﻿// ** cambia el valor de dropdownlist según class **
//Un dropdownlist que contenga "ddlTipoValor" como id, siempre llevará como class "selectpicker".
//Cuando dicho class es acompañado de "dtv1", "dtv2", "dtvX", etc... significa que ese dropdownlist está vinculado con uno o más dropdownlist (hay veces en que no está vinculado con ningún otro, o sea, opera individualmente).
//Eso aplica cuando el usuario quiere que un dropdownlist funcione a la par con otro dropdownlist.
//Por ejemplo en el módulo "estado de pagos", se encuentran 3 textbox en conjunto con un dropdownlist, al cambiar uno de éstos, cambian los 3.
$(function () {
    $('[id*=ddlTipoValor]').change(function () {
        var end = $('#' + this.id + ' option:selected').val();
        var classType = this.className.split(' ')[1];

        //verifica si el dropdownlist pertenece a una vinculación
        if (classType !== undefined) {
            $('[class*=' + classType + ']').selectpicker('val', end); //establece el mismo valor seleccionado a los demás dropdownlist vinculados
            
            //verifica si el dropdownlist pertenece a un textbox estático al cual le entregan el valor desde servidor
            var txtAttrCheck = $(this).attr("txt");

            if (txtAttrCheck !== undefined) { //aquellos dropdownlist que contienen el atributo "txt" indican que el textbox que lo acompaña está en modo enabled=false o readonly = trur
                $('[id*=ddlTipoValor][class*=' + classType + ']').each(function (i, obj) { //realiza el cálculo en el conjunto de textbox
                    var txtAttr = $(this).attr("txt"); //textbox al que se le pasará el valor calculado
                    var valorAttr = $(this).attr("valor"); //valor original del textbox
                    calculaValorACLP(this, txtAttr, valorAttr); //realiza cálculo
                });
            }
        }
    });
});

//traduce el valor total del proyecto a CLP (textbox's GridView)
//función "ConvertToNumber" se encuentra en archivo "number-format-using-numeral.js".
function calculaValorACLP(ddl, txtID, valor) {
    var txt = document.getElementById(txtID);
    var ValorItemSeleccionado = document.getElementById(ddl.id).value;
    var resultado = ConvertToNumber(valor) * ConvertToNumber(ValorItemSeleccionado);
    var ddlCountItems = $("#" + ddl.id).children().length;

    numeral.language('es');

    //si es igual a 1 NO es tipo de moneda CLP
    if (parseFloat(ValorItemSeleccionado) === 1 && ddlCountItems > 0)
        txt.value = numeral(resultado).format('0,0.[00]'); //asigna valor dependiendo del tipo de valor seleccionado
    else
        txt.value = numeral(resultado).format('0,0'); //asigna valor dependiendo del tipo de valor seleccionado

}
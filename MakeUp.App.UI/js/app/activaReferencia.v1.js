﻿//activa referencia ND
function VerificaReferencia(idDivReferencia, idLstTipoDoc, idTxtReferencia, idValidator) {
    $(idLstTipoDoc).on('changed.bs.select', function (e) {
        // do something...
        var idTipoDoc = $(this).selectpicker('val');

        //realiza lógica
        ShowReferencia(idTipoDoc, idDivReferencia, idTxtReferencia, idValidator);
    });
}

function ShowReferencia(idTipoDoc, idDivReferencia, idTxtReferencia, idValidator)
{
    //habilita referencia a aquellos documentos que son NC (nota crédito) y ND (nota débito)
    if (idTipoDoc === "KB" || idTipoDoc === "KC" || idTipoDoc === "KD" || idTipoDoc === "KE" || idTipoDoc === "KF" || idTipoDoc === "KG" || idTipoDoc === "KH" || idTipoDoc === "KI") {
        $(idDivReferencia).collapse('show');
        //document.getElementById("<%=rfvReferenciaNC.ClientID %>").enabled = true;
        $(idValidator)[0].enabled = true;

        //muestra control sin error
        removerErrorControl(idTxtReferencia);
        $(idTxtReferencia).focus();
    }
    else {
        $(idDivReferencia).collapse('hide');
        //document.getElementById("<%=rfvReferenciaNC.ClientID %>").enabled = false;
        $(idValidator)[0].enabled = false;
        $(idValidator)[0].isvalid = true;

        //limpia control
        $(idTxtReferencia).val("");
    }
}
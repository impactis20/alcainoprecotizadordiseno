﻿function getMessage(msj, codigoRespuesta) {
    var titulo = $("#messageTitulo");

    var message = $("#messageBody");
    message.html(msj);

    if (codigoRespuesta === "0") {
        titulo.html("Operación exitosa&nbsp;&nbsp;<i class='fa fa-smile-o' aria-hidden='true'></i>");
        message.removeClass("text-danger").addClass("text-success");
    }
    else {
        titulo.html("Error...&nbsp;&nbsp;<i class='fa fa-frown-o' aria-hidden='true'></i>");
        message.removeClass("text-success").addClass("text-danger");
    }

    $('#message').modal({
        backdrop: 'static',
        keyboard: true
    });
}
﻿<%@ Page Title="Login Make Up" Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="MakeUp.App.UI.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>    
    <link href="~/css/bootstrap.min.css" rel="stylesheet" />
    <link href="~/css/font-awesome.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
            <div id="loginbox" style="margin-top: 50px;" class="mainbox col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title"><h4>Ingreso a Make Up</h4></div>
                    </div>

                    <div style="padding-top: 30px" class="panel-body">
                        <div id="error" runat="server" class="alert alert-danger col-sm-12" Visible="false"></div>

                        <div id="loginform" class="form-horizontal" role="form">

                            <div class="form-group">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control" placeholder="Ingrese su usuario..." autocomplete="off"></asp:TextBox>
                                    </div>
                                    <asp:RequiredFieldValidator ID="rfvUsername" runat="server" ErrorMessage="Ingrese su <b>username</b>..." ControlToValidate="txtUsername" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Entrar" />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                        <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" TextMode="Password" placeholder="Ingrese su password..."></asp:TextBox>
                                    </div>
                                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ErrorMessage="Ingrese su <b>password</b>..." ControlToValidate="txtPassword" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Entrar" />
                                </div>
                            </div>

                            <div class="input-group">
                                <div class="checkbox">
                                    <label>
                                        <asp:CheckBox ID="cbMantenerSesionIniciada" runat="server" Text="Mantener sesión iniciada" CssClass="checkbox" />
                                    </label>
                                </div>
                            </div>

                            <div style="margin-top: 30px" class="form-group">
                                <!-- Button -->

                                <div class="col-sm-12 controls">
                                    <asp:Button ID="btnEntrar" runat="server" Text="Entrar" CssClass="btn btn-primary btn-block" OnClick="btnEntrar_Click" ValidationGroup="Entrar" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
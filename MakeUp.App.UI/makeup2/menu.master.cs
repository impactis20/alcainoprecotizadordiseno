﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.SignalR;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2
{
    public partial class menu : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                BindTipoValor();

            string pageName = this.contenido.Page.GetType().FullName;
            //List<Entities.ReleaseNoteEntity> Lst = ReleaseNoteBL.GetRelease();
            
        }


        private void BindTipoValor()
        {
            List<Entities.TipoValorEntity> Lst = TipoValorBL.GetTipoValor().Where(x => x.Valor != 1).ToList(); //filtra para eliminar el tipo de valor CLP
            gvIndicadores.DataSource = Lst;
            gvIndicadores.DataBind();

            if (Lst.Count > 0)
            {
                gvIndicadores.UseAccessibleHeader = true;
                gvIndicadores.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            fechaActualizacionIndicadores.InnerText = "Actualizado " + Lst[0].FechaUltimaActualizacion.ToShortDateString();
        }

        protected void gvIndicadores_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.TipoValorEntity Item = (Entities.TipoValorEntity)r.DataItem;

                HtmlGenericControl tipoMoneda = (r.Controls[0].FindControl("tipoMoneda") as HtmlGenericControl);
                Label lblValor = (r.Controls[0].FindControl("lblValor") as Label);

                tipoMoneda.InnerText = Item.Sigla;
                lblValor.Text = Util.Funcion.FormatoNumerico(Item.Valor);
            }
        }
    }
}
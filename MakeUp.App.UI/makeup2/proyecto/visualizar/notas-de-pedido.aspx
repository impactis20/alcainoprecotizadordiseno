﻿<%@ Page Title="Notas de pedido del proyecto" Language="C#" MasterPageFile="~/makeup2/proyecto/visualizar/proyecto.master" AutoEventWireup="true" CodeBehind="notas-de-pedido.aspx.cs" Inherits="MakeUp.App.UI.makeup2.proyecto.visualizar.notas_de_pedido" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headProyecto" runat="server">
    <link href="../../../css/bootstrap-datetimepicker.css" rel="stylesheet" />
    <link href="../../../css/bootstrap-select.css" rel="stylesheet" />
    <link href="../../../css/personalizado.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenidoProyecto" runat="server">
    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-datetimepicker.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/languages/es.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/number-format-using-numeral.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/dropdown-igualitario.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/count-character-textbox.js") %>'></script>

    <script>
        $(function () {
            combo();
            //guarda el estado del acordión
            var paneName = $("[id*=hfAcordion]").val() != "" ? $("[id*=hfAcordion]").val() : "collapseOne";
            //Remove the previous selected Pane.
            //$("[id*=accordion] .in").removeClass("in");
            //Set the selected Pane.
            //$("#" + paneName).collapse("show");
            //When Pane is clicked, save the ID to the Hidden Field.
            $(".panel-heading a").click(function () {
                $("[id*=hfAcordion]").val($(this).attr("href").replace("#", ""));
            });
            //habilita calendario
            $('[id*=dFechaEntrega]').datetimepicker({
                format: 'DD-MM-YYYY',
                maxDate: 'now',
                showClear: true,
                ignoreReadonly: true
            });

            //$("[id*=btnEditar]").click(function () {
            //    var collapsedId = $(this).attr('href');
            //    var isVisible = $(collapsedId + ' .panel-body').is(':visible');

            //    if (isVisible == true) {
            //        $(collapsedId).collapse('show');
            //        return false;
            //    }
            //});

            //$("[id*=btnEditar]").click(function () {
            //    //guarda el estado del acordión    
            //$("[id*=accordionEdicion] .in").addClass("in");
            //    $("#" + paneName).collapse("hide");
            //});

            //cuando al modal "Agregar NP" se cierra...
            $("[id*=x3]").on('hidden.bs.modal', function () {
                var ValorTotalProyecto = document.getElementById('<%= hfValorTotalProyecto.ClientID %>').value;

                //limpia formulario "Agregar NP"
                document.getElementById('<%= txtCantidad.ClientID %>').value = "";
                document.getElementById('<%= txtDescripcionNP.ClientID %>').value = "";
                document.getElementById('<%= txtValorUnitario.ClientID %>').value = "";
                document.getElementById('<%= txtValorTotalNP.ClientID %>').value = "0";

                //restablece valor total proyecto en formulario "Agregar NP"
                document.getElementById('<%= txtValorTotalProyecto.ClientID %>').value = ValorTotalProyecto;
            })

            //imprime informe resumen de NP
            $("[id*=btnImprimirResumenNP").click(function () {
                $('[id*=resumenNP]').modal('hide');

                var contents = $("[id*=printResumenNP").html();
                var frame1 = $('<iframe />');
                frame1[0].name = "frame1";
                frame1.css({ "position": "absolute", "top": "-1000000px" });
                $("body").append(frame1);
                var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
                frameDoc.document.open();
                //Create a new HTML document.
                frameDoc.document.write('<html><head><title>Resumen de NP</title>');
                frameDoc.document.write('</head><body>');
                //Append the external CSS file.
                frameDoc.document.write('<link rel="stylesheet" href="../../../css/print-from-javascript/chrome.css" type="text/css" media="print" />');
                frameDoc.document.write('<link href="../../../css/bootstrap.min.css" rel="stylesheet" />');
                frameDoc.document.write('<link href="../../../css/font-awesome.min.css" rel="stylesheet" type="text/css" />');
                //Append the DIV contents.
                frameDoc.document.write(contents);
                frameDoc.document.write('</body></html>');
                frameDoc.document.close();
                setTimeout(function () {
                    window.frames["frame1"].focus();
                    window.frames["frame1"].print();
                    frame1.remove();
                }, 500);
            });

            //verifica si el usuario puede Ajustar una NP, cuando el usuario
            //no tiene el privilegio, los controles del recuadro Ajustar se deben deshabilitar

            $('[id*=estadoNP]').each(function (i, obj) {
                var estadoNP = parseInt(this.value.split(',')[0]);
                var idFooterAjuste = this.value.split(',')[1];
                if (estadoNP === 1 && !($('#' + idFooterAjuste).length)) {
                    $('[id*=txtCantidadA').attr('disabled', 'disabled');
                    $('[id*=txtDescripcionA').attr('disabled', 'disabled');
                    $('[id*=txtFechaRealEntregaA').attr('disabled', 'disabled');
                    $('[id*=btnCopiarCA').attr('disabled', 'disabled');
                }
            });
        });
    </script>

    <script type="text/javascript">
        //función que copia la creación al ajuste
        function copiarCreacionParaAjuste(txt1Copy, txt1, validator1, txt2Copy, txt2, validator2) {
            txt1.value = txt1Copy.value;
            txt2.value = txt2Copy.value;

            validator1.setAttribute("style", "display: none;");
            validator2.setAttribute("style", "display: none;");
        }

        //función que copia el ajuste a la valorizar
        function copiarAjusteParaValorizacion(txt1Copy, txt1, validator1, txt2Copy, txt2, validator2, txt3, validator3, txt4, txt5) {
            txt1.value = txt1Copy.value;
            txt2.value = txt2Copy.value;
            txt3.value = '';
            txt4.value = '';
            txt5.value = '';

            validator1.setAttribute("style", "display: none;");
            validator2.setAttribute("style", "display: none;");
            validator3.setAttribute("style", "display: none;");
        }

        //función que copia creación a la valorizar
        function copiarCreacionParaValorizacion(txt1Copy, txt1, validator1, txt2Copy, txt2, validator2, txt3Copy, txt3, validator3, txt4Copy, txt4, txt5Copy, txt5) {
            txt1.value = txt1Copy.value;
            txt2.value = txt2Copy.value;
            txt3.value = txt3Copy.value;
            txt4.value = txt4Copy.value;
            txt5.value = txt5Copy.value;

            validator1.setAttribute("style", "display: none;");
            validator2.setAttribute("style", "display: none;");
            validator3.setAttribute("style", "display: none;");
        }

        //realiza cálculo de valores de una NP al agregar o editar, ya sea ingresando cantidad o valor unitario de una NP
        function ValorTotalNP(txtCantidad, txtValorUnitario, txtTotalNP, ddlTipoValor1, txtTotalProyecto, ddlTipovalor2, editaNP) {
            var Cantidad = ConvertToNumber(document.getElementById(txtCantidad).value);
            var Unitario = ConvertToNumber(document.getElementById(txtValorUnitario).value);
            var ddlCountItems = $("#" + ddlTipoValor1).children().length;

            if (Cantidad === "" || Unitario === "") {
                document.getElementById(txtTotalNP).value = "0";
            }
            else {
                var TotalNP = Cantidad * Unitario;// Math.round(Cantidad * Unitario);
                var ActualTotalProyecto = ConvertToNumber('<%= hfValorTotalProyecto.Value %>');
                var NuevoTotalProyecto = 0;

                if (editaNP === "True") {
                    NuevoTotalProyecto = ActualTotalProyecto - ConvertToNumber('<%= hfValorNPAntesDeSerEditada.Value %>');
                    NuevoTotalProyecto += TotalNP;
                }
                else {
                    //cuando agrega una nueva NP
                    NuevoTotalProyecto = parseFloat(ActualTotalProyecto) + TotalNP;
                }

                var valorItemSeleccionadoTotalNP = parseFloat(ConvertToNumber(document.getElementById(ddlTipoValor1).value));
                var valorItemSeleccionadoTotalProyecto = ConvertToNumber(document.getElementById(ddlTipovalor2).value);

                numeral.language('es');

                //si es igual a 1 NO es tipo de moneda CLP
                if (valorItemSeleccionadoTotalNP === 1 && ddlCountItems > 1) {
                    document.getElementById(txtTotalNP).value = numeral(TotalNP * valorItemSeleccionadoTotalNP).format('0,0.[00]'); //asigna valor total NP dependiendo del tipo de valor seleccionado
                    document.getElementById(txtTotalProyecto).value = numeral(NuevoTotalProyecto * valorItemSeleccionadoTotalProyecto).format('0,0.[00]'); //asigna valor total Proyecto dependiendo del tipo de valor seleccionadoo de valor seleccionado
                }
                else {
                    document.getElementById(txtTotalNP).value = numeral(TotalNP * valorItemSeleccionadoTotalNP).format('0,0'); //asigna valor total NP dependiendo del tipo de valor seleccionado
                    document.getElementById(txtTotalProyecto).value = numeral(NuevoTotalProyecto * valorItemSeleccionadoTotalProyecto).format('0,0'); //asigna valor total Proyecto dependiendo del tip
                }
            }
        }

        //calcula valores de una NP al valorizar
        function ValorTotalValorizarNP(txtCantidad, txtValorUnitario, txtTotalNP, txtTotalProyecto, txtTotalNPCreacion, tipoValorProyecto) {
            var Cantidad = ConvertToNumber(document.getElementById(txtCantidad).value);
            var Unitario = ConvertToNumber(document.getElementById(txtValorUnitario).value);
            var TotalNPCreacion = ConvertToNumber(document.getElementById(txtTotalNPCreacion).value);
            var resultadoTotalNP = 0;
            var resultadoTotalProyecto = parseFloat(ConvertToNumber(('<%= hfValorTotalProyecto.Value %>'))) - parseFloat(TotalNPCreacion);

            numeral.language('es');

            if (Cantidad !== "" || Unitario !== "") {
                resultadoTotalNP = Math.round(Cantidad * Unitario)
                resultadoTotalProyecto += resultadoTotalNP;
            }

            if (tipoValorProyecto > 0) {
                document.getElementById(txtTotalNP).value = numeral(resultadoTotalNP).format('0,0.[00]'); //asigna valor total NP
                document.getElementById(txtTotalProyecto).value = numeral(resultadoTotalProyecto).format('0,0.[00]'); //asigna valor total Proyecto
            }
            else {
                document.getElementById(txtTotalNP).value = numeral(resultadoTotalNP).format('0,0'); //asigna valor total NP
                document.getElementById(txtTotalProyecto).value = numeral(resultadoTotalProyecto).format('0,0'); //asigna valor total Proyecto
            }
        }

        //traduce valores de una NP a pesos chilenos cuando agrega una NP o edita una NP.
        //Para que este evento se ejecute se debe cambiar el dropdownlist que va acompañado de los textboxs "Valor Total NP" o "Valor Total Proyecto"
        function NPValoresACLP(ddl, txtCantidad, txtValorUnitario, txtValorTotalNP, txtValorTotalProyecto, editaNP) {
            var valorItemSeleccionado = parseFloat(ConvertToNumber(document.getElementById(ddl.id).value));
            var ddlCountItems = $("#" + ddl.id).children().length;
            var ValorTotalProyecto = ConvertToNumber('<%= hfValorTotalProyecto.Value %>');

            var Cantidad = ConvertToNumber(document.getElementById(txtCantidad).value);
            var Unitario = ConvertToNumber(document.getElementById(txtValorUnitario).value);
            var ValorTotalNP = Cantidad * Unitario; //cálculo para adicionar el valor total del proyecto + los valores de txtCantidad y txtValorUnitarioNP

            numeral.language('es');

            if (editaNP === "True")
                ValorTotalProyecto = parseFloat(ValorTotalProyecto) - parseFloat(ConvertToNumber('<%= hfValorNPAntesDeSerEditada.Value %>'));

            ValorTotalProyecto = parseFloat(ValorTotalProyecto) + ValorTotalNP;

            //si es igual a 1 NO es tipo de moneda CLP
            if (valorItemSeleccionado === 1 && ddlCountItems > 1) {
                document.getElementById(txtValorTotalNP).value = numeral(ValorTotalNP * valorItemSeleccionado).format('0,0.[00]'); //Asigna valor a txtValorTotalNP
                document.getElementById(txtValorTotalProyecto).value = numeral(ValorTotalProyecto * valorItemSeleccionado).format('0,0.[00]'); //Asigna valor a txtValorTotalProyecto
            }
            else {
                document.getElementById(txtValorTotalNP).value = numeral(ValorTotalNP * valorItemSeleccionado).format('0,0'); //Asigna valor a txtValorTotalNP
                document.getElementById(txtValorTotalProyecto).value = numeral(ValorTotalProyecto * valorItemSeleccionado).format('0,0'); //Asigna valor a txtValorTotalProyecto
            }
        }
    </script>

    <script type="text/javascript">
        //inicia combobox por defecto
        function combo() {
            $('.seleccione').selectpicker({
                title: 'Seleccione...',
                showTick: true
            });
        }
     </script> 

    <asp:HiddenField ID="hfAcordion" runat="server" Value="" />
    <asp:HiddenField ID="hfIdProyecto" runat="server" />
    <asp:HiddenField ID="hfTipoMoneda" runat="server" />
    <asp:HiddenField ID="hfValorTotalProyecto" runat="server" />
    <asp:HiddenField ID="hfValorNPAntesDeSerEditada" runat="server" />

    <div id="formularioAgregarNP" runat="server" class="form-group">
        <div class="row">
            <div class="col-lg-12">
                Actualmente este proyecto tiene <asp:Label id="lblContNP" runat="server"></asp:Label> notas de pedido. <asp:Label ID="lblAgregarNP" runat="server">Si desea agregar una nueva nota de pedido haga <a href="#" data-toggle="modal" data-target="#x34"><strong>CLICK AQUÍ</strong></a>.</asp:Label>
                <%--<asp:Button ID="btnResumenNP" runat="server" Text="Resumen NP" CssClass="btn btn-default pull-right" OnClick="btnResumenNP_Click" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Muestra un resumen completo de los estados (creación, ajuste y valorización) de todas las NP." />--%>

                <div id="opciones" runat="server" class="pull-right">
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="padding: 0px 0px; min-height: 0px">
                                Informes <i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li>
                                    <asp:LinkButton ID="btnInformeGlobalProyecto" runat="server" Text="<i class='fa fa-newspaper-o fa-fw'></i>&nbsp;Informe global" OnClick="btnInformeGlobalProyecto_Click" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Muestra informe global del proyecto."></asp:LinkButton>
                                </li>
                                <li id="eliminarProyecto" runat="server">
                                    <asp:LinkButton ID="btnResumenNP" runat="server" Text="<i class='fa fa-list-alt fa-fw'></i>&nbsp;Estatus NP" OnClick="btnResumenNP_Click" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Muestra un resumen completo de los estados (creación, ajuste y valorización) de todas las NP."></asp:LinkButton>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                
                <div id="informeGlobal" runat="server" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" style="width:95%">
                        <div class="modal-content">
                            <div id="printInformeGlobal">
                                <div class="modal-header">
                                    <h2 class="modal-title">Informe global proyecto: <var id="tituloProyectoInformeNP" runat="server"></var></h2>
                                </div>
                                <div class="modal-body">
                                    <div class="form-horizontal">
                                        <div id="padre" runat="server" class="form-group">
                                            <label class="col-lg-2">Proyecto padre:</label>
                                            <div class="col-lg-4">
                                                <asp:Label ID="lblPadre" runat="server" CssClass="form-control-static"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2">Cliente <small>(nombre fantasía)</small>:</label>
                                            <div class="col-lg-4">
                                                <asp:Label ID="lblCliente" runat="server" CssClass="form-control-static"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2">Fecha de ingreso:</label>
                                            <div class="col-lg-4">
                                                <asp:Label ID="lblFechaIngreso" runat="server" CssClass="form-control-static"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2">Fecha planificada:</label>
                                            <div class="col-lg-4">
                                                <asp:Label ID="lblFechaPlanificada" runat="server" CssClass="form-control-static"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2">Ejecutivo comercial:</label>
                                            <div class="col-lg-4">
                                                <asp:Label ID="lblEjecutivo" runat="server" CssClass="form-control-static"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2">Dirección:</label>
                                            <div class="col-lg-4">
                                                <asp:Label ID="lblDireccion" runat="server" CssClass="form-control-static"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                    
                                    <div class="table-responsive">
                                        <asp:GridView ID="gvNPInformeGlobal" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None" ShowFooter ="true"
                                            OnDataBound="gvNPInformeGlobal_DataBound" OnRowDataBound="gvNPInformeGlobal_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="" HeaderStyle-Width="35px">
                                                    <ItemTemplate>
                                                        <asp:Label id="lblColor" runat="server" Text="&nbsp;&nbsp;"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="NP" HeaderStyle-Width="32px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIDNP" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cant." HeaderStyle-Width="58px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCant" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Uni." HeaderStyle-Width="35px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUnidad" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Descripción">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDesc" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--Columna "Valor Unitario" se maneja mediante roles, su posición es 5--%>
                                                <asp:TemplateField HeaderText="Valor Unitario" HeaderStyle-CssClass="" ItemStyle-CssClass="text-right" HeaderStyle-Width="125px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSiglaTipoValor" runat="server" CssClass="pull-left"></asp:Label>
                                                        <asp:Label ID="lblValorUnitario" runat="server" CssClass="pull-right"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--Columna "Valor Unitario" se maneja mediante roles, su posición es 6--%>
                                                <asp:TemplateField HeaderText="Valor Total" HeaderStyle-Width="140px" HeaderStyle-CssClass="text-right" FooterStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblValorTotal" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblValorProyecto" runat="server" CssClass="form-control-static"></asp:Label>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Valor Total (CLP)" HeaderStyle-Width="140px" HeaderStyle-CssClass="col-lg-1 text-right" FooterStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblValorTotalCLP" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblValorProyectoCLP" runat="server" CssClass="form-control-static"></asp:Label>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="button" id="btnImprimirInformeGlobal" value="Imprimir" class="btn btn-primary" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="resumenNP" runat="server" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" style="width:95%">
                        <div  class="modal-content">
                            <div id="printResumenNP">
                                <div class="modal-header">
                                    <h2 class="modal-title">Resumen NP proyecto: <var id="tituloProyectoResumenNP" runat="server"></var></h2>
                                </div>
                                <div class="modal-body">
                                    <div class="table-responsive">
                                        <p><i class="fa fa-asterisk text-danger" aria-hidden="true"></i><strong><var> Indica que el valor ha cambiado respecto al último estado.</var></strong></p>
                                        <p><var>Valores en negrita indican que tuvieron variaciones en la creación.</var></p>
                                        <h3>Notas de Pedido</h3>

                                        <style type="text/css">
                                            .gvHeader th:first-child {
                                                display: none;
                                            }

                                            .gvRow td:first-child {
                                                display: none;
                                            }

                                            .gvAltRow td:first-child {
                                                display: none;
                                            }
                                        </style>

                                        <asp:GridView ID="gvResumenNP" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered gvRow" HeaderStyle-CssClass="gvHeader" AlternatingRowStyle-CssClass="gvAltRow" GridLines="None" ShowFooter="true"
                                            OnDataBound="gvResumenNP_DataBound" OnRowDataBound="gvResumenNP_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <tr class="gvHeader">
                                                            <th style="width:0px"></th>
                                                            <th style="width:1%;" rowspan="2">NP</th>
                                                            <th style="background-color: #f2dede !important" colspan="5">CREADO</th>
                                                            <th style="background-color: #fcf8e3 !important" colspan="2">AJUSTADO</th>
                                                            <th style="background-color: #dff0d8 !important" colspan="4">VALORIZADO</th>
                                                        </tr>
                                                        <tr class="gvHeader">
                                                            <th></th>
                                                            <th style="width:42px;background-color: #f2dede !important">Cant</th>
                                                            <th style="width:42px;background-color: #f2dede !important">Uni</th>
                                                            <th style="background-color: #f2dede !important">Descripción</th>
                                                            <th style="width:120px;background-color: #f2dede !important">Valor Unitario</th>
                                                            <th style="width:120px;background-color: #f2dede !important">Total</th>

                                                            <th style="width:45px;background-color: #fcf8e3 !important">Cant</th>
                                                            <th style="background-color: #fcf8e3 !important">Descripción</th>

                                                            <th style="width:42px;background-color: #dff0d8 !important">Cant</th>
                                                            <th style="background-color: #dff0d8 !important">Descripción</th>
                                                            <th style="width:120px;background-color: #dff0d8 !important">Valor Unitario</th>
                                                            <th style="width:120px;background-color: #dff0d8 !important">Total</th>
                                                        </tr>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td></td>
                                                            <td><asp:Label ID="lblIDNPCompuesta" runat="server"></asp:Label></td>
                                                            <td><asp:Label ID="lblCantC" runat="server"></asp:Label></td>
                                                            <td><div class="tooltip-wrapper" data-title='<%# Eval("Creacion.TipoUnidad.Nombre") %>'><%# Eval("Creacion.TipoUnidad.Sigla") %></div></td>
                                                            <td><asp:Label ID="lblDescC" runat="server"></asp:Label></td>
                                                            <td><asp:Label ID="lblVUC" runat="server"></asp:Label></td>
                                                            <td><asp:Label ID="lblVTC" runat="server"></asp:Label></td>

                                                            <td><asp:Label ID="lblCantA" runat="server"></asp:Label></td>
                                                            <td><asp:Label ID="lblDescA" runat="server"></asp:Label></td>

                                                            <td><asp:Label ID="lblCantV" runat="server"></asp:Label></td>
                                                            <td><asp:Label ID="lblDescV" runat="server"></asp:Label></td>
                                                            <td><asp:Label ID="lblVUV" runat="server"></asp:Label></td>
                                                            <td><asp:Label ID="lblVTV" runat="server"></asp:Label></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <div id="totalProyecto" runat="server">
                                                            <td class="text-right" colspan="11"><strong>Total Proyecto</strong></td>
                                                            <td><asp:Label ID="lblVTP" runat="server"></asp:Label></td>
                                                        </div>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="button" id="btnImprimirResumenNP" value="Imprimir" class="btn btn-primary" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="x34" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="xxxx">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class="modal-title">Agregar nota de pedido</h2>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <p>Los campos con asterisco (*) son obligatorios.</p>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-2">
                                                        <div class="form-group">
                                                            <label>ID</label>
                                                            <asp:TextBox ID="txtIDNP" runat="server" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <div class="form-group">
                                                            <label>* Cant</label>
                                                            <asp:TextBox ID="txtCantidad" runat="server" CssClass="form-control input-sm" onkeyup="FormatoNumerico(this, 1)" autocomplete="off" ValidationGroup="AgregarNP"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rfvCantidad" runat="server" ErrorMessage="<b>Obligatorio</b>..." ControlToValidate="txtCantidad" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarNP" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <div class="form-group">
                                                            <asp:UpdatePanel ID="upTipoUnidad" runat="server">
                                                                <ContentTemplate>
                                                                    <label>* Unidad</label>
                                                                    <button id="btnActualizarTU" runat="server" class="btn btn-link btn-xs" type="button" onserverclick="btnActualizarTU_Click">
                                                                        <span class="fa fa-refresh"></span>
                                                                    </button>
                                                                    <asp:DropDownList ID="ddlTipoUnidad" runat="server" CssClass="selectpicker" DataValueField="ID" DataTextField="sigla" data-width="100%"></asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="rfvTipoUnidad" runat="server" ErrorMessage="<b>Obligatorio</b>..." ControlToValidate="ddlTipoUnidad" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarNP" />
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <div class="form-group">
                                                            <label>* Tipo NP</label>
                                                            <asp:DropDownList ID="ddlTipoNP" runat="server" CssClass="seleccione" DataValueField="ID" DataTextField="nombre" data-width="100%"></asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="rfvTipoNP" runat="server" ErrorMessage="<b>Obligatorio</b>..." ControlToValidate="ddlTipoNP" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarNP" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            <label>Vincular OC de Proyecto</label><br />
                                                            <asp:DropDownList ID="ddlOC" runat="server" CssClass="selectpicker" DataValueField="ID" DataTextField="Descripcion" data-width="auto"></asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="rfvOC" runat="server" ErrorMessage="Seleccione al menos una <b>Orden de compra</b>..." ControlToValidate="ddlOC" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarNP" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label>* Descripción</label>
                                                        <asp:TextBox ID="txtDescripcionNP" runat="server" CssClass="form-control input-sm" TextMode="MultiLine" Rows="4" ValidationGroup="AgregarNP"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvDescripcion" runat="server" ErrorMessage="Campo <b>Descripción</b> es obligatorio..." ControlToValidate="txtDescripcionNP" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarNP" />
                                                        <asp:Label ID="lblContDescNP" runat="server" CssClass="help-block pull-right"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            <label>* Valor unitario</label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <asp:Label ID="lblMonedaSeleccionada1" runat="server"></asp:Label></span>
                                                                <asp:TextBox ID="txtValorUnitario" runat="server" CssClass="form-control input-sm" autocomplete="off" ValidationGroup="AgregarNP"></asp:TextBox>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="rfvValorUnitario" runat="server" ErrorMessage="Campo <b>Valor unitario</b> es obligatorio..." ControlToValidate="txtValorUnitario" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarNP" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            <label>Valor total <abbr class="initialism" title="Nota de pedido">NP</abbr></label>
                                                            <div class="input-group">
                                                                <div class="input-group-btn">
                                                                    <asp:DropDownList id="ddlTipoValor1" runat="server" CssClass="selectpicker dtv1"  data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:TextBox ID="txtValorTotalNP" runat="server" Text="0" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            <label>Valor proyecto</label>
                                                            <div class="input-group">
                                                                <div class="input-group-btn">
                                                                    <asp:DropDownList id="ddlTipoValor2" runat="server" CssClass="selectpicker dtv1" ResponseTextBoxValue="txtValorTotalProyecto" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:TextBox ID="txtValorTotalProyecto" runat="server" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="pull-right">
                                            <button id="btnAgregarNP" runat="server" class="btn btn-primary" type="button" onserverclick="btnAgregarNP_ServerClick" validationgroup="AgregarNP">
                                                <span class="fa fa-calendar-plus-o"></span>&nbsp;&nbsp;Agregar nota de pedido
                                            </button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <div id="trazabilidadNP" runat="server" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" style="width:95%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 id="tituloTrazado" runat="server" class="modal-title"></h2>
                            </div>
                            <div class="modal-body">
                                <div class="table-responsive">
                                    <span><i class="fa fa-asterisk text-danger" aria-hidden="true"></i> <strong><var>Indica que el valor ha cambiado respecto al último estado.</var></strong></span>
                                    <h3>Creación</h3>
                                    <asp:GridView ID="gvTrazaC" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None"
                                        OnDataBound="gvTrazaC_DataBound" OnRowDataBound="gvTrazaC_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Evento" HeaderStyle-Width="65px">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Cant." HeaderStyle-Width="50px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCant" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Uni." HeaderStyle-Width="55px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUnidad" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- Columna Valor Unitario se maneja mediante roles, su posición es 3 --%>
                                            <asp:TemplateField HeaderText="Valor Unitario" HeaderStyle-Width="125px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblValorUnitarioCreacion" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- Columna botón Valor Total se maneja mediante roles, su posición es 4 --%>
                                            <asp:TemplateField HeaderText="Valor Total" HeaderStyle-Width="125px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblValorTotalCreacion" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Descripción" HeaderStyle-CssClass="col-lg-5">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDesc" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Usuario" HeaderStyle-Width="200px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUsuario" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Fecha" HeaderStyle-Width="135px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFecha" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <%-- ajusteTrazabilidad no forma parte de un control restringido --%>
                                    <div id="ajusteTrazabilidad" runat="server" visible="false">
                                        <h3>Ajuste</h3>
                                        <asp:GridView ID="gvTrazaA" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None"
                                            OnDataBound="gvTrazaA_DataBound" OnRowDataBound="gvTrazaA_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Evento" HeaderStyle-Width="65px">
                                                    <ItemTemplate>
                                                        <%# gvTrazaC.Rows.Count+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cant." HeaderStyle-Width="50px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCant" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Uni." HeaderStyle-Width="55px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUnidad" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Valor Unitario" HeaderStyle-Width="125px">
                                                    <ItemTemplate>
                                                        -
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Valor Total" HeaderStyle-Width="125px">
                                                    <ItemTemplate>
                                                        -
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Descripción" HeaderStyle-CssClass="col-lg-5">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDesc" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Usuario" HeaderStyle-Width="200px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUsuario" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Fecha" HeaderStyle-Width="135px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFecha" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <%-- valorizacionTrazabilidad no forma parte de un control restringido --%>
                                    <div id="valorizacionTrazabilidad" runat="server" visible="false"> 
                                        <h3>Valorización</h3>
                                        <asp:GridView ID="gvTrazaV" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None"
                                            OnDataBound="gvTrazaV_DataBound" OnRowDataBound="gvTrazaV_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Evento" HeaderStyle-Width="65px">
                                                    <ItemTemplate>
                                                        <%# gvTrazaC.Rows.Count+2 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cant." HeaderStyle-Width="50px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCant" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Uni." HeaderStyle-Width="55px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUnidad" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- Columna Valor Unitario se maneja mediante roles, su posición es 3 --%>
                                                <asp:TemplateField HeaderText="Valor Unitario" HeaderStyle-Width="125px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblValorUnitarioValorizacion" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- Columna Valor Total se maneja mediante roles, su posición es 4 --%>
                                                <asp:TemplateField HeaderText="Valor Total" HeaderStyle-Width="125px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblValorTotalValorizacion" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Descripción" HeaderStyle-CssClass="col-lg-5">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDesc" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Usuario" HeaderStyle-Width="200px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUsuario" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Fecha" HeaderStyle-Width="135px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFecha" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <%--Mensaje que despliega al usuario para confirmar una acción--%>
                <div class="modal fade" id="ConfirmaAccion" tabindex="-1" role="dialog" aria-labelledby="xxxx">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 id="TituloMsj" runat="server" class="modal-title"></h2>
                            </div>
                            <div class="modal-body">
                                <asp:Label ID="lblContenidoMsj" runat="server"></asp:Label>
                            </div>
                            <div class="modal-footer">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="pull-right">
                                            <asp:Button ID="btnEliminarNPConfirmar" runat="server" Text="Confirmar" CssClass="btn btn-danger" OnClick="btnEliminarNPConfirmar_Click" />
                                            <asp:Button ID="btnAjustarNPConfirmar" runat="server" Text="Ajustar" CssClass="btn btn-warning" OnClick="btnAjustarNPConfirmar_Click" />
                                            <asp:Button ID="btnValorizarNPConfirmar" runat="server" Text="Valorizar" CssClass="btn btn-success" OnClick="btnValorizarNPConfirmar_Click" />
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <%--Mensaje que despliega al usuario cuando un EP está abierto--%>
                <div class="modal fade" id="EPAbierto" tabindex="-1" role="dialog" aria-labelledby="xxxx">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 id="TituloEPAbierto" runat="server" class="modal-title"></h2>
                            </div>
                            <div class="modal-body">
                                <div class="panel panel-danger" visible="false">
                                    <div class="panel-heading">
                                        Existe un Estado de Pago <b>no facturado</b>...
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label>¿Desea modificar el Monto?</label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="ddlOperacionEPAbierto" runat="server" CssClass="selectpicker" DataValueField="ID" DataTextField="nombre" data-width="65px">
                                                        <asp:ListItem Value="monto" Text="Si"></asp:ListItem>
                                                        <asp:ListItem Value="porce" Text="No"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-3">
                                                <label>Fecha Ingreso</label><br />
                                                <asp:Label ID="lblFechaIngresoEP" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Porcentaje</label><br />
                                                <asp:Label ID="lblPorcentajeEP" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Monto</label><br />
                                                <asp:Label ID="lblMontoEP" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Glosa</label><br />
                                                <asp:Label ID="lblGlosaEP" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div id="xddd" runat="server" class="pull-right">
                                            <asp:Button ID="btnEditarNPConEPAbierto" runat="server" Text="Confirmar" CssClass="btn btn-info" OnClick="btnEditarNPConEPAbierto_Click" Visible="false"/>
                                            <asp:Button ID="btnAgregarNPConEPAbierto" runat="server" Text="Confirmar" CssClass="btn btn-primary" OnClick="btnAgregarNPConEPAbierto_Click" Visible="false"/>
                                            <asp:Button ID="btnEliminarNPConEPAbierto" runat="server" Text="Confirmar" CssClass="btn btn-danger" OnClick="btnEliminarNPConEPAbierto_Click" Visible="false"/>
                                            <asp:Button ID="btnValorizarConEPAbierto" runat="server" Text="Confirmar" CssClass="btn btn-success" OnClick="btnValorizarConEPAbierto_Click" Visible="false"/>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="accordion" runat="server" class="panel-group" role="tablist" aria-multiselectable="true">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="form-group">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <ul class="list-inline" style="margin-bottom:0px">
                                            <li><label class="label label-danger">&nbsp;&nbsp;</label> Creada</li>
                                            <li><label class="label label-warning">&nbsp;&nbsp;</label> Ajustada</li>
                                            <li><label class="label label-success">&nbsp;&nbsp;</label> Valorizada</li>
                                            <li><label class="label label-primary">&nbsp;&nbsp;</label> Facturada</li>
                                            <li><i class="fa fa-trash text-danger" style="margin-left: 4px;margin-right: 4px;"></i> NP eliminada</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <style type="text/css">
                        .table-np{
                            width: 100%;
                            max-width: 100%;
                            margin-bottom: 20px;
                        }

                        .table-np>tbody>tr>td, .table-np>tbody>tr>th, .table-np>tfoot>tr>td, .table-np>tfoot>tr>th, .table-np>thead>tr>td, .table-np>thead>tr>th {
                            padding: 5px 0px 5px 0px;
                            line-height: 1.42857143;
                            vertical-align: top;
                            /*border-top: 1px solid #ddd;*/
                        }
                    </style>
                    <asp:GridView ID="gvNP" runat="server" AutoGenerateColumns="false" CssClass="table" GridLines="None" ShowHeader="false"
                        OnDataBound="gvNP_DataBound" OnRowDataBound="gvNP_RowDataBound" OnRowEditing="gvNP_RowEditing" OnRowUpdating="gvNP_RowUpdating" OnRowCancelingEdit="gvNP_RowCancelingEdit" OnRowDeleting="gvNP_RowDeleting" OnRowCommand="gvNP_RowCommand">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField ID="estadoNP" runat="server" />
                                    <div id="rowNP" runat="server" class="panel panel-default">
                                        <div id="heading" runat="server" class="panel-heading" role="tab">
                                            <h3 class="panel-title">
                                                <asp:Label id="lblColor" runat="server" Text="&nbsp;&nbsp;"></asp:Label>
                                                <b>
                                                    <asp:HyperLink ID="tituloNP" runat="server" NavigateUrl="#" data-toggle="collapse" aria-expanded="false"></asp:HyperLink>
                                                </b>
                                                <i class="fa fa-comment" style="margin-left: 10px"></i>&nbsp;&nbsp;<strong><%# string.Format("{00,0}",Eval("Creacion.Cantidad")) %></strong></i>&nbsp;&nbsp;<span id="InicioDescripcion" runat="server" class="textoc"><%# Eval("Creacion.Descripcion") %></span>
                                               <%--<div class="pull-right"><strong><%# Eval("NotaPedido.NombreTipoNP") %></strong></div>--%>
                                                <div id="opcion" runat="server" class="pull-right">
                                                    <strong><%# Eval("NotaPedido.Nombre") %></strong>&nbsp;&nbsp;
                                                    <ul class="nav navbar-top-links navbar-right">
                                                        <li>
                                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="padding: 0px 0px; min-height: 0px">
                                                                <i class="fa fa-cogs fa-fw"></i><i class="fa fa-caret-down"></i>
                                                            </a>
                                                            <ul class="dropdown-menu dropdown-user">
                                                                <li>
                                                                    <asp:LinkButton ID="btnTrazaNP" runat="server" Text="<i class='fa fa-list-alt fa-fw'></i>&nbsp;Trazabilidad" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Muestra la trazabilidad de la nota de pedido (historial)" CommandName="Trazabilidad" CommandArgument='<%# Container.DataItemIndex %>'></asp:LinkButton>
                                                                </li>
                                                                <li id="eliminarNP" runat="server">
                                                                    <asp:LinkButton ID="btnEliminar" runat="server" Text="<i class='fa fa-remove fa-fw'></i>&nbsp;Eliminar" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Eliminar nota de pedido" CommandName="Delete"></asp:LinkButton>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </h3>
                                        </div>
                                        <div id="np" runat="server" class="panel-collapse collapse" role="tabpanel">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-4" style="margin-bottom:20px">
                                                        <div class="panel panel-danger">
                                                            <div class="panel-heading">
                                                                Creación <i id="asteriscoC" runat="server" class="fa fa-asterisk text-danger" aria-hidden="true" visible="false"></i>
                                                                <div id="editarC" runat="server" class="pull-right">
                                                                    <asp:Button ID="btnEditar" runat="server" Text="Editar" CssClass="btn btn-info btn-xs" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Editar nota de pedido" CommandName="Edit" ValidationGroup='<%# "Editar" + Container.DataItemIndex %>' />
                                                                </div>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label>Cantidad</label>
                                                                            <div class="input-group input-group-sm">
                                                                                <asp:TextBox ID="txtCantidadC" runat="server" CssClass="form-control input-sm" Enabled="false" MaxLength="5" autocomplete="off"></asp:TextBox>
                                                                                <span class="input-group-addon">
                                                                                    <div class="tooltip-wrapper" data-title='<%# Eval("Creacion.TipoUnidad.Nombre") %>'>
                                                                                        <%# Eval("Creacion.TipoUnidad.Sigla") %>
                                                                                    </div>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label>Usuario</label>
                                                                            <asp:TextBox ID="txtUsuarioC" runat="server" Text='<%# string.Format("{0} {1}", Eval("Creacion.Usuario.Nombre"), Eval("Creacion.Usuario.ApellidoPaterno")) %>' CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="form-group">
                                                                            <label>Descripción</label>
                                                                            <asp:TextBox ID="txtDescripcionC" runat="server" Text='<%# Eval("Creacion.Descripcion") %>' CssClass="form-control input-sm" Enabled="false" TextMode="MultiLine" Rows="4" MaxLength="500"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label>Fecha Creación</label>
                                                                            <div class="tooltip-wrapper" data-title='<%# Eval("Creacion.Fecha") %>'>
                                                                                <asp:TextBox ID="txtFechaC" runat="server" Text='<%# string.Format("{0:dd/MM/yyyy}", Eval("Creacion.Fecha")) %>' CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="ultimaModificacionC" runat="server" class="col-lg-6" visible="false">
                                                                        <div class="form-group">
                                                                            <label>Última mod.</label>
                                                                            <div class="tooltip-wrapper" data-title='<%# Eval("Creacion.FechaUltimaModificacion") %>'>
                                                                                <asp:TextBox ID="txtFechaUltimaC" runat="server" Text='<%# string.Format("{0:dd/MM/yyyy}", Eval("Creacion.FechaUltimaModificacion")) %>' CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="valoresCreacion" runat="server" class="panel-footer">
                                                                <div class="form-group">
                                                                    <label>Valor unitario</label>
                                                                    <div class="input-group">
                                                                        <div class="input-group-btn">
                                                                            <asp:DropDownList id="ddlTipoValor1" runat="server" CssClass="selectpicker" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <asp:TextBox ID="txtVUC" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Valor total</label>
                                                                    <div class="input-group">
                                                                        <div class="input-group-btn">
                                                                            <asp:DropDownList id="ddlTipoValor2" runat="server" CssClass="selectpicker" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <asp:TextBox ID="txtVTC" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Valor proyecto</label>
                                                                    <div class="input-group">
                                                                        <div class="input-group-btn">
                                                                            <asp:DropDownList id="ddlTipoValor3" runat="server" CssClass="selectpicker" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <asp:TextBox ID="txtVPC" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4" style="margin-bottom:20px">
                                                        <div id="ajuste" runat="server" class="panel panel-warning">
                                                            <div class="panel-heading">
                                                                Ajuste <i id="asteriscoA" runat="server" class="fa fa-asterisk text-danger" aria-hidden="true" visible="false"></i>
                                                                <div id="copiarA" runat="server" class="pull-right" visible="false">
                                                                    <button id="btnCopiarCA" runat="server" class="btn btn-danger btn-xs" type="button" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Copiar datos desde la Creación">
                                                                        <i class="fa fa-files-o" aria-hidden="true"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div id="bodyAjuste" class="panel-body">  
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label>* Cantidad</label> <i id="asteriscoAC" runat="server" class="fa fa-asterisk text-danger" aria-hidden="true" visible="false"></i>
                                                                            <div class="input-group input-group-sm">
                                                                                <asp:TextBox ID="txtCantidadA" runat="server" CssClass="form-control input-sm" Enabled="false" MaxLength="6" autocomplete="off" ValidationGroup='<%# "Ajuste" + Container.DataItemIndex %>'></asp:TextBox>
                                                                                <span class="input-group-addon">
                                                                                    <div class="tooltip-wrapper" data-title='<%# Eval("Creacion.TipoUnidad.Nombre") %>'>
                                                                                        <%# Eval("Creacion.TipoUnidad.Sigla")%>
                                                                                    </div>
                                                                                </span>
                                                                            </div>
                                                                            <asp:RequiredFieldValidator ID="rfvCantidadA" runat="server" ErrorMessage="Obligatorio..." ControlToValidate="txtCantidadA" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup='<%# "Ajuste" + Container.DataItemIndex %>' />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <div id="usuarioA" runat="server" class="form-group" visible="false">
                                                                            <label>Usuario</label><br />
                                                                            <asp:TextBox ID="txtUsuarioA" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="form-group">
                                                                            <label>* Descripción</label> <i id="asteriscoAD" runat="server" class="fa fa-asterisk text-danger" aria-hidden="true" visible="false"></i>
                                                                            <asp:TextBox ID="txtDescripcionA" runat="server" CssClass="form-control input-sm" Enabled="false" TextMode="MultiLine" Rows="4" MaxLength="500" ValidationGroup='<%# "Ajuste" + Container.DataItemIndex %>'></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="rfvDescripcionA" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtDescripcionA" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" ValidationGroup='<%# "Ajuste" + Container.DataItemIndex %>' />
                                                                            <asp:Label ID="lblContDescA" runat="server" CssClass="help-block pull-right"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                </div> 
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="form-group">
                                                                            <label>* Fecha de entrega real</label>
                                                                            <div id="dFechaEntregaA" runat="server" class="input-group date input-group">
                                                                                <asp:TextBox ID="txtFechaRealEntregaA" runat="server" CssClass="form-control input-sm" Enabled="false" autocomplete="off" ValidationGroup='<%# "Ajuste" + Container.DataItemIndex %>'></asp:TextBox>
                                                                                <span id="spanUnoA" runat="server" class="input-group-addon">
                                                                                    <span id="spanDosA" runat="server" class="fa fa-calendar"></span>
                                                                                </span>
                                                                            </div>
                                                                            <asp:RequiredFieldValidator ID="rfvFechaRealEntregaA" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtFechaRealEntregaA" SetFocusOnError="true" CssClass="alert-danger" ValidationGroup='<%# "Ajuste" + Container.DataItemIndex %>' />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="footerAjuste" runat="server"> <%-- div usado para para mostrar u ocultar el contenido dependiendo de si el usuario tiene o no acceso --%>
                                                                <div id="footerA" runat="server" class="panel-footer">  <%-- div usado para mostrar u ocultar btn dependiendo del estado de la NP --%>
                                                                    <asp:Button ID="btnAjustar" runat="server" Text="Ajustar" CssClass="btn btn-warning btn-lg btn-block" Visible="false" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Ajustar nota de pedido" CommandName="Ajustar" CommandArgument='<%# Container.DataItemIndex %>' ValidationGroup='<%# "Ajuste" + Container.DataItemIndex %>' />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4" style="margin-bottom:20px">
                                                        <div id="contenidoValorizacion" runat="server" class="panel panel-success">
                                                            <div class="panel-heading">
                                                                Valorización <i id="asteriscoV" runat="server" class="fa fa-asterisk text-danger" aria-hidden="true" visible="false"></i>
                                                                    <div id="copiarV" runat="server" class="pull-right">
                                                                        <button id="btnCopiarCV" runat="server" class="btn btn-danger btn-xs" type="button" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Copiar datos desde la Creación">
                                                                            <i class="fa fa-files-o" aria-hidden="true"></i>
                                                                        </button>
                                                                        <button id="btnCopiarAV" runat="server" class="btn btn-warning btn-xs" type="button" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Copiar datos desde el Ajuste">
                                                                            <i class="fa fa-files-o" aria-hidden="true"></i>
                                                                        </button>
                                                                    </div>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label>* Cantidad</label> <i id="asteriscoVC" runat="server" class="fa fa-asterisk text-danger" aria-hidden="true" visible="false"></i>
                                                                            <div class="input-group input-group-sm">
                                                                                <asp:TextBox ID="txtCantidadV" runat="server" CssClass="form-control input-sm" Enabled="false" MaxLength="6" autocomplete="off" ValidationGroup='<%# "Valorizacion" + Container.DataItemIndex %>'></asp:TextBox>
                                                                                <span class="input-group-addon">
                                                                                    <div class="tooltip-wrapper" data-title='<%# Eval("Creacion.TipoUnidad.Nombre") %>'>
                                                                                        <%# Eval("Creacion.TipoUnidad.Sigla") %>
                                                                                    </div>
                                                                                </span>
                                                                            </div>
                                                                            <asp:RequiredFieldValidator ID="rfvCantidadV" runat="server" ErrorMessage="Obligatorio..." ControlToValidate="txtCantidadV" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup='<%# "Valorizacion" + Container.DataItemIndex %>' />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <div id="usuarioV" runat="server" class="form-group" visible="false">
                                                                            <label>Usuario</label>
                                                                            <asp:TextBox ID="txtUsuarioV" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="form-group">
                                                                            <label>* Descripción</label> <i id="asteriscoVD" runat="server" class="fa fa-asterisk text-danger" aria-hidden="true" visible="false"></i>
                                                                            <asp:TextBox ID="txtDescripcionV" runat="server" CssClass="form-control input-sm" Enabled="false" TextMode="MultiLine" Rows="4" MaxLength="500" autocomplete="off" ValidationGroup='<%# "Valorizacion" + Container.DataItemIndex %>'></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="rfvDescripcionV" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtDescripcionV" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup='<%# "Valorizacion" + Container.DataItemIndex %>' />
                                                                            <asp:Label ID="lblContDescV" runat="server" CssClass="help-block pull-right"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div id="fechaV" runat="server" class="form-group">
                                                                            <label>Fecha Valorización</label>
                                                                            <div class="tooltip-wrapper" data-title='<%# Eval("Valorizacion.Fecha") %>'>
                                                                                <asp:TextBox ID="txtFechaV" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="valoresValorizacion" runat="server" class="panel-footer">
                                                                <div class="form-group">
                                                                    <label>Valor unitario</label> <i id="asteriscoVU" runat="server" class="fa fa-asterisk text-danger" aria-hidden="true" visible="false"></i>
                                                                    <div class="input-group">
                                                                        <div class="input-group-btn">
                                                                            <asp:DropDownList id="ddlTipoValor4" runat="server" CssClass="selectpicker" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <asp:TextBox ID="txtVUV" runat="server" CssClass="form-control input-sm" Enabled="false" autocomplete="off" ValidationGroup='<%# "Valorizacion" + Container.DataItemIndex %>' ></asp:TextBox>
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="rfvVUV" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtVUV" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup='<%# "Valorizacion" + Container.DataItemIndex %>' />
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Valor total</label>
                                                                    <div class="input-group">
                                                                        <div class="input-group-btn">
                                                                            <asp:DropDownList id="ddlTipoValor5" runat="server" CssClass="selectpicker" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <asp:TextBox ID="txtVTV" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Valor proyecto</label>
                                                                    <div class="input-group">
                                                                        <div class="input-group-btn">
                                                                            <asp:DropDownList id="ddlTipoValor6" runat="server" CssClass="selectpicker" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <asp:TextBox ID="txtVPV" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <asp:Label ID="lblVariacion" runat="server" Text="* El ajuste tuvo una variación" CssClass="text-danger" Visible="false"></asp:Label>
                                                                    <asp:Button ID="btnValorizar" runat="server" Text="Valorizar" CssClass="btn btn-success btn-lg btn-block" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Valorizar nota de pedido" CommandName="Valorizar" CommandArgument='<%# Container.DataItemIndex %>' ValidationGroup='<%# "Valorizacion" + Container.DataItemIndex %>' />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab">
                                            <h3 class="panel-title">
                                                <label class="label label-danger">&nbsp;&nbsp;</label>
                                                <b>
                                                    <asp:HyperLink ID="tituloNP" runat="server" data-toggle="collapse" data-parent="#accordion" aria-expanded="false"></asp:HyperLink>
                                                </b>
                                                <i class="fa fa-comment" style="margin-left: 10px"></i>&nbsp;&nbsp;<strong><%# string.Format("{00,0}",Eval("Creacion.Cantidad")) %></strong></i>&nbsp;&nbsp;<span id="InicioDescripcion" runat="server" class="textoc"><%# Eval("Creacion.Descripcion") %></span>
                                                <div class="pull-right"><strong><%# Eval("NotaPedido.Nombre") %></strong></div>
                                            </h3>
                                        </div>
                                        <div id="np" runat="server" class="panel-collapse collapse in" role="tabpanel">
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <div class="panel panel-danger">
                                                        <div class="panel-heading">
                                                            Creación
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <div class="form-group">
                                                                        <label>* Cant</label>
                                                                        <asp:TextBox ID="txtCantidadC" runat="server" CssClass="form-control input-sm" onkeyup="FormatoNumerico(this, 1)" autocomplete="off"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rfvCantidad" runat="server" ErrorMessage="Obligatorio..." ControlToValidate="txtCantidadC" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="EditarNP" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <div class="form-group">
                                                                        <label>* Unidad</label>
                                                                        <asp:DropDownList ID="ddlTipoUnidadC" runat="server" CssClass="selectpicker" DataValueField="ID" DataTextField="sigla" data-width="100%"></asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <div class="form-group">
                                                                        <label>* Tipo NP</label>
                                                                        <asp:DropDownList ID="ddlTipoNP" runat="server" CssClass="seleccione" DataValueField="ID" DataTextField="nombre" data-width="100%"></asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <div class="form-group">
                                                                        <label>Vincular <abbr class="initialism" title="Orden de Compra">OC</abbr> de Proyecto</label><br />
                                                                        <asp:DropDownList ID="ddlOCC" runat="server" CssClass="selectpicker" DataValueField="ID" DataTextField="Descripcion" data-width="auto"></asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="rfvOCSeleccionada" runat="server" ErrorMessage="Obligatorio..." ControlToValidate="ddlOCC" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="EditarNP" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="form-group">
                                                                        <label>* Descripción</label>
                                                                        <asp:TextBox ID="txtDescripcionC" runat="server" CssClass="form-control input-sm" Text='<%# Eval("Creacion.Descripcion") %>' TextMode="MultiLine" Rows="4"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rfvDescripcionA" runat="server" ErrorMessage="Obligatorio..." ControlToValidate="txtDescripcionC" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="EditarNP" />
                                                                        <asp:Label ID="lblContDescC" runat="server" CssClass="help-block pull-right"></asp:Label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="valoresCreacion" runat="server" class="row">
                                                                <div class="col-lg-4">
                                                                    <div class="form-group">
                                                                        <label>* Valor unitario</label>
                                                                        <div class="input-group">
                                                                            <asp:Label id="lblMonedaSeleccionada" runat="server" CssClass="input-group-addon"></asp:Label>
                                                                            <asp:TextBox ID="txtVUC" runat="server" CssClass="form-control input-sm" onkeyup="FormatoNumerico(this, 1)" autocomplete="off"></asp:TextBox>
                                                                        </div>
                                                                        <asp:RequiredFieldValidator ID="rfvValorUnitario" runat="server" ErrorMessage="Obligatorio..." ControlToValidate="txtVUC" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="EditarNP" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <div class="form-group">
                                                                        <label>Valor total</label>
                                                                        <div class="input-group">
                                                                            <div class="input-group-btn">
                                                                                <asp:DropDownList id="ddlTipoValor1" runat="server" CssClass="selectpicker" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <asp:TextBox ID="txtVTC" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <div class="form-group">
                                                                        <label>Valor proyecto</label>
                                                                        <div class="input-group">
                                                                            <div class="input-group-btn">
                                                                                <asp:DropDownList id="ddlTipoValor2" runat="server" CssClass="selectpicker" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <asp:TextBox ID="txtVPC" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>                                                                                
                                                </div>
                                            </div>
                                            <div class="panel-footer">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="pull-right">
                                                            <asp:Button ID="btnEditar" runat="server" Text="Guardar" CssClass="btn btn-primary btn-xs" type="button" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Editar nota de pedido" CommandName="Update" ValidationGroup="EditarNP" />
                                                            <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn btn-default btn-xs" type="button" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Eliminar nota de pedido" CommandName="Cancel" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        //Va y abre la NP cuando entra desde una URL
        $(window).on('load', function () {
            //Abre una NP basada en una URL
            var url = document.location.toString();
            if (url.match('#')) {
                var linkHeader = $("a:contains('" + url.split('#')[1] + "')[id*=tituloNP]");
                goToByScroll(linkHeader);
            }
        });

        //Va y abre la NP cuando se abre de la bandeja de notificación
        $(document).on('click', "a.notificacion,a.no-vista", function () {
            window.location = "#" + $(this).attr("href").split('#')[1];
            window.location.reload(true);
        });

        function goToByScroll(link) {
            var idAcordeon = $(link).attr("href");
            $(idAcordeon).collapse('show');

            //busca ID del div Padre
            var id = "#" + $(idAcordeon).parent().attr("id");

            var alturaMenu = 0;
            var resp = $(document).width() + 17; //suma 17 para que quede acorde width real de navegador
            if (resp < 768)
                alturaMenu = $("#menu").height();

            // Scroll
            $('body').animate({
                //scrollTop: $("#" + id).offset().top - alturaMenu
                scrollTop: $(id).offset().top - alturaMenu
            },
                'slow');
        }
    </script>
</asp:Content>
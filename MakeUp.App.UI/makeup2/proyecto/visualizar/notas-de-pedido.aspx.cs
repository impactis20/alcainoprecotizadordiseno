﻿using MakeUp.BL;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MakeUp.App.UI.makeup2.proyecto.visualizar
{
    public partial class notas_de_pedido : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //disponibiliza controles según autorizaciones del administrador
            CheckControlEnabled();

            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    CargaInicial(Convert.ToInt16(Request.QueryString["id"].ToString()));
                }
                else
                {
                    Page.Title = "Proyecto no encontrado...";
                }
            }
        }

        private void CheckControlEnabled()
        {
            //oculta controles restringidos
            lblAgregarNP.Visible = false;
            gvTrazaC.Columns[3].Visible = false;
            gvTrazaC.Columns[4].Visible = false;
            gvTrazaV.Columns[3].Visible = false;
            gvTrazaV.Columns[4].Visible = false;
            gvNPInformeGlobal.Columns[5].Visible = false; //Precio valor unitario NP
            gvNPInformeGlobal.Columns[6].Visible = false; //Valor total NP

            App_Code.Seguridad.GetControlAutorizado(Page, Request.Path);
        }

        private void CargaInicial(int IDProyecto)
        {
            Entities.ProyectoEntity Proyecto = ProyectoBL.GetCabeceraProyecto(IDProyecto);

            if (Proyecto != null)
            {
                hfIdProyecto.Value = Proyecto.ID.ToString();
                hfTipoMoneda.Value = Proyecto.TipoValor.Sigla;

                lblMonedaSeleccionada1.Text = Proyecto.TipoValor.Sigla;

                //asigna función de cálculo al agregar una NP en textbox's cantidad y valor unitario
                string ArmaFuncionValorTotalNP = string.Format("ValorTotalNP('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')", txtCantidad.ClientID, txtValorUnitario.ClientID, txtValorTotalNP.ClientID, ddlTipoValor1.ClientID, txtValorTotalProyecto.ClientID, ddlTipoValor2.ClientID, false);
                txtCantidad.Attributes.Add("onblur", ArmaFuncionValorTotalNP);
                txtValorUnitario.Attributes.Add("onblur", ArmaFuncionValorTotalNP);

                //asigna contador de caracteres
                txtDescripcionNP.Attributes.Add("onkeyup", "countChar(this, '" + lblContDescNP.ClientID + "', 500)");

                //Agrega item CLP para traducir el valor de la caja de texto
                AddFunctionValorTotalProyectoCLP(ref ddlTipoValor1, Proyecto.TipoValor.Sigla, Proyecto.TipoValor.Valor);
                AddFunctionValorTotalProyectoCLP(ref ddlTipoValor2, Proyecto.TipoValor.Sigla, Proyecto.TipoValor.Valor);

                if (Proyecto.TipoValor.Valor > 1)
                {
                    //Almacena el valor del tipo de valor
                    ViewState.Add("ValorTipoValor", Proyecto.TipoValor.Valor);

                    //determina cantidad de decimales que se podrá ingresar en el valor de una NP (ingreso y edición)
                    txtValorUnitario.Attributes.Add("onkeyup", "FormatoNumerico(this, 2);");
                }
                else
                {
                    //determina cantidad de decimales que se podrá ingresar en el valor de una NP (ingreso y edición)
                    txtValorUnitario.Attributes.Add("onkeyup", "FormatoNumerico(this, 0);");
                }

                //Agrega onchange a txtValorTotalNP y txtValorTotalProyecto (Agregar NP)
                string onChangeDropDownList = string.Format("NPValoresACLP(this, '{0}', '{1}', '{2}', '{3}', '{4}')", txtCantidad.ClientID, txtValorUnitario.ClientID, txtValorTotalNP.ClientID, txtValorTotalProyecto.ClientID, false);
                ddlTipoValor1.Attributes.Add("onchange", onChangeDropDownList);
                ddlTipoValor2.Attributes.Add("onchange", onChangeDropDownList);

                BindTipoUnidad(ddlTipoUnidad);
                BindOCProyecto(ddlOC);
                BindDataNP(Proyecto.ID);
                BindTipoNP(ddlTipoNP);
            }
        }

        //Agrega item CLP para traducir el valor de la caja de texto
        public static void AddFunctionValorTotalProyectoCLP(ref DropDownList ddl, string Sigla, decimal Valor)
        {
            ddl.Items.Add(new ListItem(Sigla, "1"));

            if (Valor > 1)
                ddl.Items.Add(new ListItem("CLP", Valor.ToString())); //calculará a pesos chilenos el tipo de valor seleccionado

            ddl.DataBind();
        }

        protected void btnActualizarTU_Click(object sender, EventArgs e)
        {
            BindTipoUnidad(ddlTipoUnidad);
            ScriptManager.RegisterStartupScript(this, GetType(), "UpdateddlTU", "$('.selectpicker').selectpicker('refresh');", true);
        }

        protected void btnAgregarNP_ServerClick(object sender, EventArgs e)
        {
            int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);

            try
            {
                //Agrega NP y consulta si tiene EP abiertos (No facturados) para confirmación
                if (EstadoPagoBL.GetTieneEPAbiertos(IDProyecto))
                {
                    CargaEPNoFacturado(IDProyecto, "Proceda para Agregar NP", btnAgregarNPConEPAbierto);
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "$('#EPAbierto').modal('show');", true);
                }
                else
                {
                    //Al agregar una nueva NP significa que el valor del proyecto va a variar, por ende
                    //hay que consultar si el proyecto tiene EP emitidos, de ser así, hay que actualizar el porcentaje
                    //de dicho(s) EP

                    if (EstadoPagoBL.GetTieneEPEmitidos(IDProyecto))
                        AgregarNP("porce");
                    else
                        AgregarNP(string.Empty);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        #region Informe Global del proyecto
        protected void btnInformeGlobalProyecto_Click(object sender, EventArgs e)
        {
            int IDProyecto = Convert.ToInt16(Request.QueryString["id"].ToString());

            Entities.ProyectoEntity Proyecto = ProyectoBL.GetCabeceraProyecto(IDProyecto);
            tituloProyectoInformeNP.InnerText = string.Format("{0} - {1}", Proyecto.ID.ToString(), Proyecto.Nombre);

            if (Proyecto.Padre != null)
            {
                padre.Visible = true;
                lblPadre.Text = string.Format("{0} - {1}", Proyecto.Padre.ID, Proyecto.Padre.Nombre);
            }
            else
                padre.Visible = false;

            lblCliente.Text = Proyecto.Cliente.NombreFantasia;
            lblFechaIngreso.Text = Proyecto.FechaIngreso.ToShortDateString();
            lblFechaPlanificada.Text = Proyecto.FechaPlanificada.ToShortDateString();
            lblEjecutivo.Text = string.Format("{0} {1} {2} <var>({3})</var>", Proyecto.EjecutivoComercial.Nombre, Proyecto.EjecutivoComercial.ApellidoPaterno, Proyecto.EjecutivoComercial.ApellidoMaterno, Proyecto.EjecutivoComercial.Email);
            lblDireccion.Text = string.Format("{0}, {1}", Proyecto.Direccion, Proyecto.Comuna);

            if (Proyecto.TipoValor.Valor > 1)
            {
                ViewState.Add("TotalProyecto", string.Format("{0} {1}", Proyecto.TipoValor.Sigla, Util.Funcion.FormatoNumerico(Proyecto.ValorTotal)));
                //ViewState.Add("TotalProyectoCLP", string.Format("CLP {0}", Util.Funcion.FormatoNumericoEntero(Proyecto.ValorTotal * Proyecto.TipoValor.Valor)));
                ViewState.Add("TotalProyectoCLP", 0); //valor total del proyecto en CLP se calculará mediante la sumatoria del valor total de cada NP, de forma de obtener valores aproximados.
            }
            else
            {
                ViewState.Add("TotalProyecto", string.Format("{0} {1}", Proyecto.TipoValor.Sigla, Util.Funcion.FormatoNumericoEntero(Proyecto.ValorTotal)));
                ViewState.Add("TotalProyectoCLP", null);
            }

            //obtiene data gridview
            BindDataInformeGlobalNP(IDProyecto);

            ScriptManager.RegisterStartupScript(this, GetType(), "informeGlobal", "$('#" + informeGlobal.ClientID + "').modal({backdrop: 'static',keyboard: true});", true);
        }

        private void BindDataInformeGlobalNP(int IDProyecto)
        {
            try
            {
                List<Entities.NotaPedidoEntity> Lst = NotaPedidoBL.GetNP(IDProyecto).Where(x => x.Estado == true).ToList();
                gvNPInformeGlobal.DataKeyNames = new string[1] { "ID" }; //PK
                gvNPInformeGlobal.DataSource = Lst;
                gvNPInformeGlobal.DataBind();

                if (Lst.Count > 0)
                {
                    gvNPInformeGlobal.UseAccessibleHeader = true;
                    gvNPInformeGlobal.HeaderRow.TableSection = TableRowSection.TableHeader;

                    //determina si muestra columna traductora a CLP en informe global
                    //Recordar que la columna 7 no pertenece al grupo de permisos. Ésta se muestra si el tipo de moneda no es CLP y si el usuario tiene acceso a ver los valores del informe global
                    gvNPInformeGlobal.Columns[7].Visible = ViewState["TotalProyectoCLP"] != null && gvNPInformeGlobal.Columns[6].Visible ? true : false;
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        protected void gvNPInformeGlobal_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.NotaPedidoEntity NP = (Entities.NotaPedidoEntity)r.DataItem;

                Label lblColor = (r.Controls[0].FindControl("lblColor") as Label);
                lblColor.CssClass = "label";

                switch (NP.IDEstadoNP)
                {
                    case 1:
                        lblColor.Attributes.Add("style", "background-color:#d9534f !important"); // rojo
                        SetRowValues(r, NP.IDCompuesta, NP.Creacion.Cantidad, NP.Creacion.TipoUnidad.Sigla, NP.Creacion.Descripcion, NP.Creacion.ValorUnitario);
                        break;
                    case 2:
                        lblColor.Attributes.Add("style", "background-color:#f0ad4e !important"); // amarillo-naranjo
                        SetRowValues(r, NP.IDCompuesta, NP.Creacion.Cantidad, NP.Creacion.TipoUnidad.Sigla, NP.Ajuste.Descripcion, NP.Creacion.ValorUnitario);
                        break;
                    case 3:
                        lblColor.Attributes.Add("style", "background-color:#5cb85c !important"); // verde
                        SetRowValues(r, NP.IDCompuesta, NP.Valorizacion.Cantidad, NP.Creacion.TipoUnidad.Sigla, NP.Valorizacion.Descripcion, NP.Valorizacion.ValorUnitario);
                        break;
                    case 4:
                        lblColor.Attributes.Add("style", "background-color:#337ab7 !important"); // azul
                        SetRowValues(r, NP.IDCompuesta, NP.Valorizacion.Cantidad, NP.Creacion.TipoUnidad.Sigla, NP.Valorizacion.Descripcion, NP.Valorizacion.ValorUnitario);
                        break;
                }
            }

            if (r.RowType == DataControlRowType.Footer)
            {
                Label lblValorProyecto = (r.Controls[0].FindControl("lblValorProyecto") as Label);
                lblValorProyecto.Text = string.Format("<strong>{0}</strong>", ViewState["TotalProyecto"].ToString());

                if (ViewState["TotalProyectoCLP"] != null)
                {
                    Label lblValorProyectoCLP = (r.Controls[0].FindControl("lblValorProyectoCLP") as Label);
                    lblValorProyectoCLP.Text = string.Format("<strong>CLP {0}</strong>", Util.Funcion.FormatoNumericoEntero(ViewState["TotalProyectoCLP"].ToString()));
                }
            }
        }

        private void SetRowValues(GridViewRow r, string IDCompuesta, decimal Cantidad, string TipoUnidad, string Descripcion, decimal ValorUnitario)
        {
            Label lblColor = (r.Controls[0].FindControl("lblColor") as Label);
            Label lblIDNP = (r.Controls[0].FindControl("lblIDNP") as Label);
            Label lblCant = (r.Controls[0].FindControl("lblCant") as Label);
            Label lblUnidad = (r.Controls[0].FindControl("lblUnidad") as Label);
            Label lblDesc = (r.Controls[0].FindControl("lblDesc") as Label);
            Label lblSiglaTipoValor = (r.Controls[0].FindControl("lblSiglaTipoValor") as Label);
            Label lblValorUnitario = (r.Controls[0].FindControl("lblValorUnitario") as Label);
            Label lblValorTotal = (r.Controls[0].FindControl("lblValorTotal") as Label);

            string TipoValor = ViewState["TotalProyecto"].ToString().Split(' ')[0];
            decimal ValorNP = ValorUnitario * Cantidad;

            lblIDNP.Text = IDCompuesta.Split('-')[1];
            lblCant.Text = Util.Funcion.FormatoNumerico(Cantidad);
            lblUnidad.Text = TipoUnidad;
            lblDesc.Text = Descripcion;
            lblSiglaTipoValor.Text = TipoValor;

            if (ViewState["ValorTipoValor"] != null)
            {
                decimal ValorNPCLP = ValorNP * Convert.ToDecimal(ViewState["ValorTipoValor"]);
                lblValorUnitario.Text = Util.Funcion.FormatoNumerico(ValorUnitario);
                lblValorTotal.Text = Util.Funcion.FormatoNumerico(ValorNP);
                //Label aparecerá cuando el proyecto no sea CLP
                Label lblValorTotalCLP = (r.Controls[0].FindControl("lblValorTotalCLP") as Label);
                lblValorTotalCLP.Text = Util.Funcion.FormatoNumericoEntero(ValorNPCLP);

                ViewState["TotalProyectoCLP"] = Convert.ToInt32(ViewState["TotalProyectoCLP"]) + Convert.ToInt32(ValorNPCLP); //convierte valores a enteros. Ej: 1.9 = 2, 1.4 = 1
            }
            else
            {
                lblValorUnitario.Text = Util.Funcion.FormatoNumericoEntero(ValorUnitario);
                lblValorTotal.Text = Util.Funcion.FormatoNumericoEntero(ValorNP);
            }
        }

        protected void gvNPInformeGlobal_DataBound(object sender, EventArgs e)
        {
            App_Code.Seguridad.GetControlGridviewAutorizado(Page, Request.Path, gvNPInformeGlobal.ID);
        }
        #endregion

        #region Informe Resumen de NP
        protected void btnResumenNP_Click(object sender, EventArgs e)
        {
            tituloProyectoResumenNP.InnerText = (Page.Master.Master.Master.FindControl("menu").FindControl("contenido").FindControl("titulo") as HtmlGenericControl).InnerText; //copia texto del titulo padre

            List<Entities.NotaPedidoEntity> Lst = NotaPedidoBL.GetResumenNPProyecto(Convert.ToInt16(hfIdProyecto.Value));
            gvResumenNP.DataKeyNames = new string[1] { "ID" }; //PK
            gvResumenNP.DataSource = Lst.Where(x => x.Estado == true);
            gvResumenNP.DataBind();

            if (Lst.Count > 0)
            {
                gvResumenNP.UseAccessibleHeader = true;
                gvResumenNP.HeaderRow.TableSection = TableRowSection.TableHeader;

                ScriptManager.RegisterStartupScript(this, GetType(), "ResumenNP", "$('#" + resumenNP.ClientID + "').modal({backdrop: 'static',keyboard: true});", true);
            }
        }

        protected void gvResumenNP_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.NotaPedidoEntity NP = (Entities.NotaPedidoEntity)r.DataItem;

                (r.Controls[0].FindControl("lblIDNPCompuesta") as Label).Text = NP.IDCompuesta.Split('-')[1];

                //La creación nunca llevará un asterisco
                decimal TotalCreacion = NP.Creacion.Cantidad * NP.Creacion.ValorUnitario;
                Label lblCantC = (r.Controls[0].FindControl("lblCantC") as Label);
                Label lblDescC = (r.Controls[0].FindControl("lblDescC") as Label);
                Label lblVUC = (r.Controls[0].FindControl("lblVUC") as Label);
                Label lblVTC = (r.Controls[0].FindControl("lblVTC") as Label);

                //oculta controles restringidos
                lblVUC.Visible = false;
                lblVTC.Visible = false;

                lblCantC.Text = Util.Funcion.FormatoNumerico(NP.Creacion.Cantidad);
                lblDescC.Text = NP.Creacion.Descripcion;

                //si es mayor a 1 es porque el proyecto no es CLP
                if (ddlTipoValor1.Items.Count > 1)
                {
                    lblVUC.Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumerico(NP.Creacion.ValorUnitario));
                    lblVTC.Text = Util.Funcion.FormatoNumerico(TotalCreacion);
                }
                else
                {
                    lblVUC.Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumericoEntero(NP.Creacion.ValorUnitario));
                    lblVTC.Text = Util.Funcion.FormatoNumericoEntero(TotalCreacion);
                }

                //verifica si en la creación la cantidad, descripcion y valor unitario, fueron modificados en el tiempo. De ser así se marca en negrita
                if (NP.Creacion.CantidadModificada)
                    lblCantC.Text = string.Format("<strong>{0}</strong>", lblCantC.Text);

                if (NP.Creacion.DescripcionModificada)
                    lblDescC.Text = string.Format("<strong>{0}</strong>", lblDescC.Text);

                if (NP.Creacion.ValorUnitarioModificado)
                    lblVUC.Text = string.Format("<strong>{0}</strong>", lblVUC.Text);


                if (NP.IDEstadoNP >= 2)
                {
                    Label lblCantA = (r.Controls[0].FindControl("lblCantA") as Label);
                    Label lblDescA = (r.Controls[0].FindControl("lblDescA") as Label);

                    lblCantA.Text = Util.Funcion.FormatoNumerico(NP.Ajuste.Cantidad);
                    lblDescA.Text = NP.Ajuste.Descripcion;

                    string Asterisco = "<i class='fa fa-asterisk text-danger' style='margin-left:5px' aria-hidden='true'></i>";

                    if (NP.Creacion.Cantidad != NP.Ajuste.Cantidad) //verifica si la cantidad del Ajuste varió respecto a la cantidad de la Creación
                        lblCantA.Text += Asterisco;

                    if (NP.Creacion.Descripcion != NP.Ajuste.Descripcion) //verifica si la descripción del Ajuste varió respecto a la descripción de la Creación
                        lblDescA.Text += Asterisco;

                    if (NP.IDEstadoNP >= 3)
                    {
                        decimal TotalValorizacion = NP.Valorizacion.ValorUnitario * NP.Valorizacion.Cantidad;
                        Label lblCantV = (r.Controls[0].FindControl("lblCantV") as Label);
                        Label lblDescV = (r.Controls[0].FindControl("lblDescV") as Label);
                        Label lblVUV = (r.Controls[0].FindControl("lblVUV") as Label);
                        Label lblVTV = (r.Controls[0].FindControl("lblVTV") as Label);

                        //oculta controles restringidos
                        lblVUV.Visible = false;
                        lblVTV.Visible = false;

                        lblCantV.Text = Util.Funcion.FormatoNumerico(NP.Valorizacion.Cantidad);
                        lblDescV.Text = NP.Valorizacion.Descripcion;

                        //si es mayor a 1 es porque el proyecto no es CLP
                        if (ddlTipoValor1.Items.Count > 1)
                        {
                            lblVUV.Text = Util.Funcion.FormatoNumerico(NP.Valorizacion.ValorUnitario);
                            lblVTV.Text = Util.Funcion.FormatoNumerico(TotalValorizacion);
                        }
                        else
                        {
                            lblVUV.Text = Util.Funcion.FormatoNumericoEntero(NP.Valorizacion.ValorUnitario);
                            lblVTV.Text = Util.Funcion.FormatoNumericoEntero(TotalValorizacion);
                        }

                        if (NP.Ajuste.Cantidad != NP.Valorizacion.Cantidad)
                            lblCantV.Text += Asterisco;

                        if (NP.Ajuste.Descripcion != NP.Valorizacion.Descripcion)
                            lblDescV.Text += Asterisco;

                        if (NP.Creacion.ValorUnitario != NP.Valorizacion.ValorUnitario)
                            lblVUV.Text += Asterisco;

                        if (TotalCreacion != TotalValorizacion)
                            lblVTV.Text += Asterisco;
                    }
                }
            }

            if (r.RowType == DataControlRowType.Footer)
            {
                short IDProyecto = Convert.ToInt16(Request.QueryString["id"].ToString());
                double ValorTotalProyecto = ProyectoBL.GetValorTotalProyecto(IDProyecto);
                Label lblVTP = (r.Controls[0].FindControl("lblVTP") as Label);
                HtmlGenericControl totalProyecto = (r.Controls[0].FindControl("totalProyecto") as HtmlGenericControl);

                //oculta controles restringidos
                totalProyecto.Visible = false;

                lblVTP.Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumerico(ValorTotalProyecto));
            }
        }

        protected void gvResumenNP_DataBound(object sender, EventArgs e)
        {
            App_Code.Seguridad.GetControlGridviewAutorizado(Page, Request.Path, gvResumenNP.ID);
        }
        #endregion

        #region Informe de trazabalidad de NP
        protected void gvTrazaC_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.NotaPedidoEntity NP = (Entities.NotaPedidoEntity)r.DataItem;

                Label lblCant = (r.Controls[0].FindControl("lblCant") as Label);
                Label lblUnidad = (r.Controls[0].FindControl("lblUnidad") as Label);
                Label lblValorUnitario = (r.Controls[0].FindControl("lblValorUnitarioCreacion") as Label);
                Label lblDesc = (r.Controls[0].FindControl("lblDesc") as Label);
                Label lblFecha = (r.Controls[0].FindControl("lblFecha") as Label);
                Label lblUsuario = (r.Controls[0].FindControl("lblUsuario") as Label);

                if (Session["NP"] == null)
                {
                    Session.Add("NP", NP); //Guarda NP para compararla con la siguiente NP

                    lblCant.Text = Util.Funcion.FormatoNumerico(NP.Creacion.Cantidad);
                    lblUnidad.Text = NP.Creacion.TipoUnidad.Sigla;
                    lblValorUnitario.Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumerico(NP.Creacion.ValorUnitario));
                    lblDesc.Text = NP.Creacion.Descripcion;
                    lblFecha.Text = string.Format("{0:dd/MM/yyyy HH:mm}", NP.Creacion.Fecha);
                    lblUsuario.Text = string.Format("{0} {1}", NP.Creacion.Usuario.Nombre, NP.Creacion.Usuario.ApellidoPaterno);
                }
                else
                {
                    Entities.NotaPedidoEntity NPAnterior = (Entities.NotaPedidoEntity)Session["NP"];
                    string Asterisco = "<i class='fa fa-asterisk text-danger' style='margin-left:5px' aria-hidden='true'></i>";

                    //Verifica qué campos variaron según la versión anterior
                    if (NPAnterior.Creacion.Cantidad != NP.Creacion.Cantidad)
                        lblCant.Text = Util.Funcion.FormatoNumerico(NP.Creacion.Cantidad) + Asterisco;
                    else
                        lblCant.Text = Util.Funcion.FormatoNumerico(NP.Creacion.Cantidad);

                    if (NPAnterior.Creacion.TipoUnidad.Sigla != NP.Creacion.TipoUnidad.Sigla)
                        lblUnidad.Text = NP.Creacion.TipoUnidad.Sigla + Asterisco;
                    else
                        lblUnidad.Text = NP.Creacion.TipoUnidad.Sigla;

                    if (NPAnterior.Creacion.ValorUnitario != NP.Creacion.ValorUnitario)
                        lblValorUnitario.Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumerico(NP.Creacion.ValorUnitario) + Asterisco);
                    else
                        lblValorUnitario.Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumerico(NP.Creacion.ValorUnitario));

                    if (NPAnterior.Creacion.Descripcion != NP.Creacion.Descripcion)
                        lblDesc.Text = NP.Creacion.Descripcion + Asterisco;
                    else
                        lblDesc.Text = NP.Creacion.Descripcion;

                    string UsuarioAnterior = string.Format("{0} {1}", NPAnterior.Creacion.Usuario.Nombre, NPAnterior.Creacion.Usuario.ApellidoPaterno);
                    string Usuario = string.Format("{0} {1}", NP.Creacion.Usuario.Nombre, NP.Creacion.Usuario.ApellidoPaterno);

                    if (UsuarioAnterior != Usuario)
                        lblUsuario.Text = Usuario + Asterisco;
                    else
                        lblUsuario.Text = Usuario;

                    lblFecha.Text = string.Format("{0:dd/MM/yyyy HH:mm}", NP.Creacion.Fecha);

                    Session["NP"] = NP; //actualiza NP para luego ser comparada con la siguiente
                }

                (r.Controls[0].FindControl("lblValorTotalCreacion") as Label).Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumerico(Math.Round(NP.Creacion.Cantidad * NP.Creacion.ValorUnitario, 0)));
            }
        }

        protected void gvTrazaA_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.NotaPedidoEntity NP = (Entities.NotaPedidoEntity)r.DataItem;

                Label lblCant = (r.Controls[0].FindControl("lblCant") as Label);
                Label lblUnidad = (r.Controls[0].FindControl("lblUnidad") as Label);
                //Label lblValorUnitario = (r.Controls[0].FindControl("lblValorUnitarioAjuste") as Label);
                Label lblDesc = (r.Controls[0].FindControl("lblDesc") as Label);
                Label lblFecha = (r.Controls[0].FindControl("lblFecha") as Label);
                Label lblUsuario = (r.Controls[0].FindControl("lblUsuario") as Label);

                Entities.NotaPedidoEntity NPAnterior = (Entities.NotaPedidoEntity)Session["NP"];
                string Asterisco = "<i class='fa fa-asterisk text-danger' style='margin-left:5px' aria-hidden='true'></i>";

                //Verifica qué campos variaron según la versión anterior
                if (NPAnterior.Creacion.Cantidad != NP.Ajuste.Cantidad)
                    lblCant.Text = Util.Funcion.FormatoNumerico(NP.Ajuste.Cantidad) + Asterisco;
                else
                    lblCant.Text = Util.Funcion.FormatoNumerico(NP.Ajuste.Cantidad);

                lblUnidad.Text = NP.Creacion.TipoUnidad.Sigla; //Tipo de unidad nunca variará en el Ajuste
                //lblValorUnitario.Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumerico(NP.Creacion.ValorUnitario)); //Valor unitario nunca variará en el Ajuste

                if (NPAnterior.Creacion.Descripcion != NP.Ajuste.Descripcion)
                    lblDesc.Text = NP.Ajuste.Descripcion + Asterisco;
                else
                    lblDesc.Text = NP.Ajuste.Descripcion;

                string UsuarioAnterior = string.Format("{0} {1}", NPAnterior.Creacion.Usuario.Nombre, NPAnterior.Creacion.Usuario.ApellidoPaterno);
                string Usuario = string.Format("{0} {1}", NP.Ajuste.Usuario.Nombre, NP.Ajuste.Usuario.ApellidoPaterno);

                if (UsuarioAnterior != Usuario)
                    lblUsuario.Text = Usuario + Asterisco;
                else
                    lblUsuario.Text = Usuario;

                lblFecha.Text = string.Format("{0:dd/MM/yyyy HH:mm}", NP.Ajuste.Fecha);

                Session["NP"] = NP; //actualiza NP para luego ser comparada con la siguiente
            }
        }

        protected void gvTrazaV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.NotaPedidoEntity NP = (Entities.NotaPedidoEntity)r.DataItem;

                Label lblCant = (r.Controls[0].FindControl("lblCant") as Label);
                Label lblUnidad = (r.Controls[0].FindControl("lblUnidad") as Label);
                Label lblValorUnitario = (r.Controls[0].FindControl("lblValorUnitarioValorizacion") as Label);
                Label lblDesc = (r.Controls[0].FindControl("lblDesc") as Label);
                Label lblFecha = (r.Controls[0].FindControl("lblFecha") as Label);
                Label lblUsuario = (r.Controls[0].FindControl("lblUsuario") as Label);

                Entities.NotaPedidoEntity NPAnterior = (Entities.NotaPedidoEntity)Session["NP"];
                string Asterisco = "<i class='fa fa-asterisk text-danger' style='margin-left:5px' aria-hidden='true'></i>";

                //Verifica qué campos variaron según la versión anterior
                if (NPAnterior.Ajuste.Cantidad != NP.Valorizacion.Cantidad)
                    lblCant.Text = Util.Funcion.FormatoNumerico(NP.Valorizacion.Cantidad) + Asterisco;
                else
                    lblCant.Text = Util.Funcion.FormatoNumerico(NP.Valorizacion.Cantidad);

                lblUnidad.Text = NP.Creacion.TipoUnidad.Sigla;

                if (NPAnterior.Creacion.ValorUnitario != NP.Valorizacion.ValorUnitario)
                    lblValorUnitario.Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumerico(NP.Valorizacion.ValorUnitario) + Asterisco);
                else
                    lblValorUnitario.Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumerico(NP.Valorizacion.ValorUnitario));

                if (NPAnterior.Ajuste.Descripcion != NP.Valorizacion.Descripcion)
                    lblDesc.Text = NP.Valorizacion.Descripcion + Asterisco;
                else
                    lblDesc.Text = NP.Valorizacion.Descripcion;

                string UsuarioAnterior = string.Format("{0} {1}", NPAnterior.Ajuste.Usuario.Nombre, NPAnterior.Ajuste.Usuario.ApellidoPaterno);
                string Usuario = string.Format("{0} {1}", NP.Valorizacion.Usuario.Nombre, NP.Valorizacion.Usuario.ApellidoPaterno);

                if (UsuarioAnterior != Usuario)
                    lblUsuario.Text = Usuario + Asterisco;
                else
                    lblUsuario.Text = Usuario;

                (r.Controls[0].FindControl("lblValorTotalValorizacion") as Label).Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumerico(Math.Round(NP.Valorizacion.Cantidad * NP.Valorizacion.ValorUnitario, 0)));

                lblFecha.Text = string.Format("{0:dd/MM/yyyy HH:mm}", NP.Valorizacion.Fecha);
            }
        }

        protected void gvTrazaC_DataBound(object sender, EventArgs e)
        {
            App_Code.Seguridad.GetControlGridviewAutorizado(Page, Request.Path, gvTrazaC.ID);
        }

        protected void gvTrazaA_DataBound(object sender, EventArgs e)
        {
            if (gvTrazaC.Columns[3].Visible)
            {
                gvTrazaA.Columns[3].Visible = true;
                gvTrazaA.Columns[4].Visible = true;
            }
            else
            {
                gvTrazaA.Columns[3].Visible = false;
                gvTrazaA.Columns[4].Visible = false;
            }
        }

        protected void gvTrazaV_DataBound(object sender, EventArgs e)
        {
            App_Code.Seguridad.GetControlGridviewAutorizado(Page, Request.Path, gvTrazaV.ID);
        }
        #endregion

        #region Gridview NP eventos
        protected void gvNP_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.NotaPedidoEntity NP = (Entities.NotaPedidoEntity)r.DataItem;

                HyperLink tituloNP = (r.Controls[0].FindControl("tituloNP") as HyperLink);
                tituloNP.Text = NP.IDCompuesta;

                Label lblColor = (r.Controls[0].FindControl("lblColor") as Label);
                HtmlGenericControl InicioDescripcion = (r.Controls[0].FindControl("InicioDescripcion") as HtmlGenericControl);

                //obtiene items de ddl tipos de valor
                ListItem[] items = new ListItem[ddlTipoValor1.Items.Count];
                ddlTipoValor1.Items.CopyTo(items, 0);

                if (NP.Estado == true) //verifica si la NP está eliminada
                {
                    //asigna url del div
                    string IDEtiquetaNP = (r.Controls[0].FindControl("np") as HtmlGenericControl).ClientID;
                    tituloNP.Attributes.Add("data-parent", string.Format("#{0}", accordion.ClientID));
                    tituloNP.NavigateUrl = string.Format("#{0}", IDEtiquetaNP);
                    tituloNP.Attributes["aria-controls"] = IDEtiquetaNP;
                    tituloNP.Text += string.Format(" de {0}", ViewState["TotalNPValidas"].ToString());

                    if ((r.RowState & DataControlRowState.Edit) == DataControlRowState.Edit)
                    {
                        //oculta controles restringidos
                        (r.Controls[0].FindControl("valoresCreacion") as HtmlGenericControl).Visible = false;

                        //Bind desplegable con Tipo de unidad
                        DropDownList ddlTipoUnidad = (r.Controls[0].FindControl("ddlTipoUnidadC") as DropDownList);
                        BindTipoUnidad(ddlTipoUnidad);
                        ddlTipoUnidad.SelectedValue = NP.Creacion.TipoUnidad.ID.ToString();

                        //Bind desplegable con Tipo de NP
                        DropDownList ddlTipoNP = (r.Controls[0].FindControl("ddlTipoNP") as DropDownList);
                        BindTipoNP(ddlTipoNP);
                        ddlTipoNP.SelectedValue = NP.Creacion.TipoNP.ID.ToString();
                        ddlTipoNP.Enabled = PaginaBL.GetHabilitarControlPagina(28, IDUsuario);

                        //Bind desplegable con OC seleccionada
                        DropDownList ddlOC = (r.Controls[0].FindControl("ddlOCC") as DropDownList);
                        BindOCProyecto(ddlOC);
                        ddlOC.SelectedValue = NP.Creacion.OrdenCompra.ID.ToString();

                        //establece tipo de valor
                        (r.Controls[0].FindControl("lblMonedaSeleccionada") as Label).Text = lblMonedaSeleccionada1.Text;

                        TextBox txtCantidadC = (r.Controls[0].FindControl("txtCantidadC") as TextBox);
                        TextBox txtVUC = (r.Controls[0].FindControl("txtVUC") as TextBox);
                        DropDownList ddlTipoValor1 = (r.Controls[0].FindControl("ddlTipoValor1") as DropDownList);
                        TextBox txtVTC = (r.Controls[0].FindControl("txtVTC") as TextBox);
                        DropDownList ddlTipoValor2 = (r.Controls[0].FindControl("ddlTipoValor2") as DropDownList);
                        TextBox txtVPC = (r.Controls[0].FindControl("txtVPC") as TextBox);

                        //asigna javascript autocálculo al agregar una NP a textboxs Cantidad y Valor Unitario
                        string ArmaFuncionValorTotalNP = string.Format("ValorTotalNP('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')", txtCantidadC.ClientID, txtVUC.ClientID, txtVTC.ClientID, ddlTipoValor1.ClientID, txtVPC.ClientID, ddlTipoValor2.ClientID, true);
                        txtCantidadC.Attributes.Add("onblur", ArmaFuncionValorTotalNP);
                        txtVUC.Attributes.Add("onblur", ArmaFuncionValorTotalNP);

                        //almacena valores originales de la NP (para poder realizar cálculo correctamente)
                        hfValorNPAntesDeSerEditada.Value = (NP.Creacion.Cantidad * NP.Creacion.ValorUnitario).ToString();

                        //copia items tipos de valor
                        ddlTipoValor1.Items.AddRange(items);
                        ddlTipoValor2.Items.AddRange(items);
                        ddlTipoValor1.DataBind();
                        ddlTipoValor2.DataBind();

                        if (ddlTipoValor1.Items.Count == 1)
                        {
                            //asigna valor total NP y valor total proyecto
                            txtCantidadC.Text = Util.Funcion.FormatoNumerico(NP.Creacion.Cantidad);
                            txtVUC.Text = Util.Funcion.FormatoNumericoEntero(NP.Creacion.ValorUnitario);
                            txtVTC.Text = Util.Funcion.FormatoNumericoEntero(hfValorNPAntesDeSerEditada.Value);
                            txtVPC.Text = Util.Funcion.FormatoNumericoEntero(hfValorTotalProyecto.Value);

                            //determina cantidad de decimales que se podrá ingresar en el valor de una NP (edición)
                            txtVUC.Attributes.Add("onkeyup", "FormatoNumerico(this, 0);");
                        }
                        else
                        {
                            //asigna valor total NP y valor total proyecto
                            txtCantidadC.Text = Util.Funcion.FormatoNumerico(NP.Creacion.Cantidad);
                            txtVUC.Text = Util.Funcion.FormatoNumerico(NP.Creacion.ValorUnitario);
                            txtVTC.Text = Util.Funcion.FormatoNumerico(hfValorNPAntesDeSerEditada.Value);
                            txtVPC.Text = Util.Funcion.FormatoNumerico(hfValorTotalProyecto.Value);

                            //determina cantidad de decimales que se podrá ingresar en el valor de una NP (edición)
                            txtVUC.Attributes.Add("onkeyup", "FormatoNumerico(this, 2);");
                        }

                        //Agrega función cliente onchange para calcular el valor del proyecto si se cambia el tipo de valor
                        string onChangeDropDownList = string.Format("NPValoresACLP(this, '{0}', '{1}', '{2}', '{3}', '{4}')", txtCantidadC.ClientID, txtVUC.ClientID, txtVTC.ClientID, txtVPC.ClientID, true);
                        ddlTipoValor1.Attributes.Add("onchange", onChangeDropDownList);
                        ddlTipoValor2.Attributes.Add("onchange", onChangeDropDownList);

                        string VinculaDropDownList = string.Format(" dtvgvNPEditing{0}", r.RowIndex);
                        ddlTipoValor1.CssClass += VinculaDropDownList;
                        ddlTipoValor2.CssClass += VinculaDropDownList;


                        //asigna contador de caracteres
                        (r.Controls[0].FindControl("txtDescripcionC") as TextBox).Attributes.Add("onkeyup", "countChar(this, '" + (r.Controls[0].FindControl("lblContDescC") as Label).ClientID + "', 500)");

                        //Almacena valores de la NP antes de la edición
                        ViewState.Add("NPAntesDeSerEditada", new Entities.CreacionEntity(NP.Creacion.TipoUnidad.ID, NP.Creacion.OrdenCompra.ID, NP.Creacion.ValorUnitario, NP.Creacion.Cantidad, NP.Creacion.Descripcion, NP.Creacion.TipoNP.ID));
                    }
                    else
                    {
                        //almacena estado de la NP (1, 2, 3 o 4) y clientID de div "footerAjuste"
                        //lo siguiente se realiza con el fin de concretar requerimiento:
                        //"si el usuario no puede ajustar una NP, entonces se debe ocultar el boton y
                        //los controles visibles en el recuadro de ajuste (btnCopiarCA, txtCantidadA, txtDescripcionA y txtFechaRealEntregaA)."
                        (r.Controls[0].FindControl("estadoNP") as HiddenField).Value = string.Format("{0},{1}", NP.IDEstadoNP.ToString(), (r.Controls[0].FindControl("footerAjuste") as HtmlGenericControl).ClientID);

                        //Asigna valores dependiendo del estado en el que está la NP
                        if (NP.IDEstadoNP >= 1)
                        {
                            //oculta controles restringidos
                            (r.Controls[0].FindControl("btnEliminar") as LinkButton).Visible = false;
                            (r.Controls[0].FindControl("btnEditar") as Button).Visible = false;
                            (r.Controls[0].FindControl("valoresCreacion") as HtmlGenericControl).Visible = false;
                            (r.Controls[0].FindControl("footerAjuste") as HtmlGenericControl).Visible = false;

                            DropDownList ddlTipoValor1 = (r.Controls[0].FindControl("ddlTipoValor1") as DropDownList);
                            TextBox txtVUC = (r.Controls[0].FindControl("txtVUC") as TextBox);
                            DropDownList ddlTipoValor2 = (r.Controls[0].FindControl("ddlTipoValor2") as DropDownList);
                            TextBox txtVTC = (r.Controls[0].FindControl("txtVTC") as TextBox);
                            DropDownList ddlTipoValor3 = (r.Controls[0].FindControl("ddlTipoValor3") as DropDownList);
                            TextBox txtVPC = (r.Controls[0].FindControl("txtVPC") as TextBox);

                            //Valores NP creación
                            (r.Controls[0].FindControl("txtCantidadC") as TextBox).Text = Util.Funcion.FormatoNumerico(NP.Creacion.Cantidad);

                            //verifica si la NP está valorizada o facturada
                            //decimal ValorTotalProyectoCreacion = Convert.ToDecimal(hfValorTotalProyecto.Value);

                            //if (NP.IDEstadoNP >= 3)
                            //{
                            //    //Al valor total del proyecto se le resta la valorización de cada NP según corresponda, con el fin de que en la ventana "Creación" el Total proyecto sea correcto
                            //    ValorTotalProyectoCreacion -= (NP.Valorizacion.Cantidad * NP.Valorizacion.ValorUnitario); //Resta valorización
                            //    ValorTotalProyectoCreacion += (NP.Creacion.Cantidad * NP.Creacion.ValorUnitario); //Suma el valor de la creación
                            //}

                            string ValorTotalProyectoCreacion = hfValorTotalProyecto.Value.ToString();

                            //si tiene 1 item, es CLP
                            if (this.ddlTipoValor1.Items.Count == 1)
                            {
                                txtVUC.Text = Util.Funcion.FormatoNumericoEntero(NP.Creacion.ValorUnitario);
                                txtVTC.Text = Util.Funcion.FormatoNumericoEntero(NP.Creacion.Cantidad * NP.Creacion.ValorUnitario);
                                txtVPC.Text = Util.Funcion.FormatoNumericoEntero(ValorTotalProyectoCreacion);
                            }
                            else
                            {
                                txtVUC.Text = Util.Funcion.FormatoNumerico(NP.Creacion.ValorUnitario);
                                txtVTC.Text = Util.Funcion.FormatoNumerico(NP.Creacion.Cantidad * NP.Creacion.ValorUnitario);
                                txtVPC.Text = Util.Funcion.FormatoNumerico(ValorTotalProyectoCreacion);
                            }

                            //copia items tipos de valor
                            ddlTipoValor1.Items.AddRange(items);
                            ddlTipoValor2.Items.AddRange(items);
                            ddlTipoValor3.Items.AddRange(items);
                            ddlTipoValor1.DataBind();
                            ddlTipoValor2.DataBind();
                            ddlTipoValor3.DataBind();

                            //Agrega atributos de datos (textbox y valor) para generar cálculo de onchange en ddlTipoValor
                            ddlTipoValor1.Attributes.Add("txt", txtVUC.ClientID);
                            ddlTipoValor1.Attributes.Add("valor", NP.Creacion.ValorUnitario.ToString());
                            ddlTipoValor2.Attributes.Add("txt", txtVTC.ClientID);
                            ddlTipoValor2.Attributes.Add("valor", (NP.Creacion.Cantidad * NP.Creacion.ValorUnitario).ToString());
                            ddlTipoValor3.Attributes.Add("txt", txtVPC.ClientID);
                            ddlTipoValor3.Attributes.Add("valor", hfValorTotalProyecto.Value);

                            //Agrega función cliente onchange para calcular el valor del proyecto si se cambia el tipo de valor
                            string VinculaDropDownList = string.Format(" dtvgvNPCreacion{0}", r.RowIndex);
                            ddlTipoValor1.CssClass += (VinculaDropDownList);
                            ddlTipoValor2.CssClass += (VinculaDropDownList);
                            ddlTipoValor3.CssClass += (VinculaDropDownList);

                            if (NP.Creacion.FechaUltimaModificacion != NP.Creacion.Fecha)
                            {
                                //Muestra el control, asigna fecha de modificación y muestra asterisco
                                (r.Controls[0].FindControl("ultimaModificacionC") as HtmlGenericControl).Visible = true;
                                (r.Controls[0].FindControl("asteriscoC") as HtmlGenericControl).Visible = true;
                            }

                            if (NP.IDEstadoNP == 1)
                            {
                                //Oculta panel de valorización
                                (r.Controls[0].FindControl("contenidoValorizacion") as HtmlGenericControl).InnerHtml = string.Empty;
                                (r.Controls[0].FindControl("contenidoValorizacion") as HtmlGenericControl).Attributes.Add("style", "height:565px");

                                lblColor.CssClass = "label label-danger"; //Creada
                                InicioDescripcion.InnerText = NP.Creacion.Descripcion;

                                //habilita formulario de ajuste
                                (r.Controls[0].FindControl("copiarA") as HtmlGenericControl).Visible = true;
                                (r.Controls[0].FindControl("txtCantidadA") as TextBox).Enabled = true;
                                (r.Controls[0].FindControl("txtDescripcionA") as TextBox).Enabled = true;
                                (r.Controls[0].FindControl("txtDescripcionA") as TextBox).Attributes.Add("onkeyup", "countChar(this, '" + (r.Controls[0].FindControl("lblContDescA") as Label).ClientID + "', 500)"); //asigna contador de caracteres
                                (r.Controls[0].FindControl("txtFechaRealEntregaA") as TextBox).Enabled = true;
                                (r.Controls[0].FindControl("btnAjustar") as Button).Visible = true;
                                (r.Controls[0].FindControl("btnCopiarCA") as HtmlButton).Attributes.Add("onclick", string.Format("copiarCreacionParaAjuste({0}, {1}, {2}, {3}, {4}, {5})", (r.Controls[0].FindControl("txtCantidadC") as TextBox).ClientID, (r.Controls[0].FindControl("txtCantidadA") as TextBox).ClientID, (r.Controls[0].FindControl("rfvCantidadA") as RequiredFieldValidator).ClientID, (r.Controls[0].FindControl("txtDescripcionC") as TextBox).ClientID, (r.Controls[0].FindControl("txtDescripcionA") as TextBox).ClientID, (r.Controls[0].FindControl("rfvDescripcionA") as RequiredFieldValidator).ClientID)); //habilita botón copiar desde creación btnCopiarCA
                                (r.Controls[0].FindControl("footerA") as HtmlGenericControl).Visible = true; //disponibiliza botón de ajuste
                            }
                        }

                        //Controles de panel de valorización
                        DropDownList ddlTipoValor4 = (r.Controls[0].FindControl("ddlTipoValor4") as DropDownList);
                        TextBox txtVUV = (r.Controls[0].FindControl("txtVUV") as TextBox);
                        DropDownList ddlTipoValor5 = (r.Controls[0].FindControl("ddlTipoValor5") as DropDownList);
                        TextBox txtVTV = (r.Controls[0].FindControl("txtVTV") as TextBox);
                        DropDownList ddlTipoValor6 = (r.Controls[0].FindControl("ddlTipoValor6") as DropDownList);
                        TextBox txtVPV = (r.Controls[0].FindControl("txtVPV") as TextBox);

                        if (NP.IDEstadoNP >= 2)
                        {
                            (r.Controls[0].FindControl("editarC") as HtmlGenericControl).Visible = false; //Oculta botón editar (creación)
                            (r.Controls[0].FindControl("footerA") as HtmlGenericControl).Visible = false; //Oculta footer del ajuste
                            (r.Controls[0].FindControl("eliminarNP") as HtmlGenericControl).Visible = false; //Oculta botón eliminar NP. Esta opción aparece cuando la NP tiene estado 1
                            (r.Controls[0].FindControl("contenidoValorizacion") as HtmlGenericControl).Visible = false;
                            (r.Controls[0].FindControl("valoresValorizacion") as HtmlGenericControl).Visible = false;

                            //Llena el ajuste con los valores ingresados
                            (r.Controls[0].FindControl("usuarioA") as HtmlGenericControl).Visible = true;
                            (r.Controls[0].FindControl("txtUsuarioA") as TextBox).Text = string.Format("{0} {1}", NP.Ajuste.Usuario.Nombre, NP.Ajuste.Usuario.ApellidoPaterno);
                            (r.Controls[0].FindControl("txtCantidadA") as TextBox).Text = Util.Funcion.FormatoNumerico(NP.Ajuste.Cantidad);
                            (r.Controls[0].FindControl("txtDescripcionA") as TextBox).Text = NP.Ajuste.Descripcion;
                            (r.Controls[0].FindControl("txtFechaRealEntregaA") as TextBox).Text = NP.Ajuste.FechaRealEntrega.ToShortDateString();

                            //Regulariza string para quitar caracteres \n, \r, etc... con el fin de sólo comprar contenido
                            string DescCreacion = Regex.Replace(NP.Creacion.Descripcion, @"\s+", string.Empty);
                            string DescAjuste = Regex.Replace(NP.Ajuste.Descripcion, @"\s+", string.Empty);

                            //marca con asterisco si la cantidad y descripción es distinta entre la creación y el ajuste
                            if (NP.Creacion.Cantidad != NP.Ajuste.Cantidad || !string.Equals(DescCreacion, DescAjuste, StringComparison.OrdinalIgnoreCase))
                                (r.Controls[0].FindControl("asteriscoA") as HtmlGenericControl).Visible = true;

                            if (NP.Creacion.Cantidad != NP.Ajuste.Cantidad)
                                (r.Controls[0].FindControl("asteriscoAC") as HtmlGenericControl).Visible = true;

                            if (!string.Equals(DescCreacion, DescAjuste, StringComparison.OrdinalIgnoreCase))
                                (r.Controls[0].FindControl("asteriscoAD") as HtmlGenericControl).Visible = true;

                            //copia items tipos de valor
                            ddlTipoValor4.Items.AddRange(items);
                            ddlTipoValor5.Items.AddRange(items);
                            ddlTipoValor6.Items.AddRange(items);
                            ddlTipoValor4.DataBind();
                            ddlTipoValor5.DataBind();
                            ddlTipoValor6.DataBind();

                            if (NP.IDEstadoNP == 2)
                            {
                                //oculta controles restringidos
                                (r.Controls[0].FindControl("valoresValorizacion") as HtmlGenericControl).Visible = false;
                                (r.Controls[0].FindControl("contenidoValorizacion") as HtmlGenericControl).Visible = false;

                                lblColor.CssClass = "label label-warning"; //Ajustada
                                InicioDescripcion.InnerText = NP.Ajuste.Descripcion;

                                //habilita formulario de valorización
                                (r.Controls[0].FindControl("copiarV") as HtmlGenericControl).Visible = true;
                                (r.Controls[0].FindControl("txtCantidadV") as TextBox).Enabled = true;
                                (r.Controls[0].FindControl("txtDescripcionV") as TextBox).Enabled = true;
                                (r.Controls[0].FindControl("txtDescripcionV") as TextBox).Attributes.Add("onkeyup", "countChar(this, '" + (r.Controls[0].FindControl("lblContDescV") as Label).ClientID + "', 500)"); //asigna contador de caracteres
                                (r.Controls[0].FindControl("txtFechaV") as TextBox).Text = DateTime.Now.ToShortDateString(); //Establece fecha de hoy
                                txtVUV.Enabled = true;
                                (r.Controls[0].FindControl("btnValorizar") as Button).Visible = true;
                                (r.Controls[0].FindControl("btnCopiarCV") as HtmlButton).Attributes.Add("onclick", string.Format("copiarCreacionParaValorizacion({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12})", (r.Controls[0].FindControl("txtCantidadC") as TextBox).ClientID, (r.Controls[0].FindControl("txtCantidadV") as TextBox).ClientID, (r.Controls[0].FindControl("rfvCantidadV") as RequiredFieldValidator).ClientID, (r.Controls[0].FindControl("txtDescripcionC") as TextBox).ClientID, (r.Controls[0].FindControl("txtDescripcionV") as TextBox).ClientID, (r.Controls[0].FindControl("rfvDescripcionV") as RequiredFieldValidator).ClientID, (r.Controls[0].FindControl("txtVUC") as TextBox).ClientID, txtVUV.ClientID, (r.Controls[0].FindControl("rfvVUV") as RequiredFieldValidator).ClientID, (r.Controls[0].FindControl("txtVTC") as TextBox).ClientID, txtVTV.ClientID, (r.Controls[0].FindControl("txtVPC") as TextBox).ClientID, txtVPV.ClientID)); //habilita botón copiar desde creación btnCopiarCV
                                (r.Controls[0].FindControl("btnCopiarAV") as HtmlButton).Attributes.Add("onclick", string.Format("copiarAjusteParaValorizacion({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9})", (r.Controls[0].FindControl("txtCantidadA") as TextBox).ClientID, (r.Controls[0].FindControl("txtCantidadV") as TextBox).ClientID, (r.Controls[0].FindControl("rfvCantidadV") as RequiredFieldValidator).ClientID, (r.Controls[0].FindControl("txtDescripcionA") as TextBox).ClientID, (r.Controls[0].FindControl("txtDescripcionV") as TextBox).ClientID, (r.Controls[0].FindControl("rfvDescripcionV") as RequiredFieldValidator).ClientID, txtVUV.ClientID, (r.Controls[0].FindControl("rfvVUV") as RequiredFieldValidator).ClientID, txtVTV.ClientID, txtVPV.ClientID)); //habilita botón copiar desde Ajuste btnCopiarAV

                                //verifica si el tipo de valor corresponde
                                int CantDecimal = this.ddlTipoValor1.Items.Count > 1 ? 2 : 0;

                                //crea función javascript autocálculo al agregar la Cantidad y Valor Unitario a los textboxs de la valorización
                                string ArmaFuncionValorTotalNP = string.Format("ValorTotalValorizarNP('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')", (r.Controls[0].FindControl("txtCantidadV") as TextBox).ClientID, txtVUV.ClientID, txtVTV.ClientID, txtVPV.ClientID, (r.Controls[0].FindControl("txtVTC") as TextBox).ClientID, CantDecimal);

                                //asigna función de autocálculo
                                (r.Controls[0].FindControl("txtCantidadV") as TextBox).Attributes.Add("onblur", ArmaFuncionValorTotalNP);
                                txtVUV.Attributes.Add("onblur", ArmaFuncionValorTotalNP);

                                //asigna función de formato numérico
                                (r.Controls[0].FindControl("txtCantidadV") as TextBox).Attributes.Add("onkeyup", "FormatoNumerico(this, 1)"); //según requerimiento usuario puede ingresar 1 decimal en la Cantidad
                                txtVUV.Attributes.Add("onkeyup", string.Format("FormatoNumerico(this, {0})", CantDecimal)); //usuario ingresa valor unitario valorización según tipo de valor del proyecto

                                //al valorizar el usuario no puede cambiar el tipo de valor
                                ddlTipoValor4.Enabled = false;
                                ddlTipoValor5.Enabled = false;
                                ddlTipoValor6.Enabled = false;
                            }
                        }

                        if (NP.IDEstadoNP >= 3)
                        {
                            //Agrega atributos de datos (textbox y valor) para generar cálculo de onchange en ddlTipoValor
                            ddlTipoValor4.Attributes.Add("txt", txtVUV.ClientID);
                            ddlTipoValor4.Attributes.Add("valor", NP.Valorizacion.ValorUnitario.ToString());
                            ddlTipoValor5.Attributes.Add("txt", txtVTV.ClientID);
                            ddlTipoValor5.Attributes.Add("valor", (NP.Valorizacion.Cantidad * NP.Valorizacion.ValorUnitario).ToString());
                            ddlTipoValor6.Attributes.Add("txt", txtVPV.ClientID);
                            ddlTipoValor6.Attributes.Add("valor", hfValorTotalProyecto.Value);

                            //Agrega función cliente onchange para calcular el valor del proyecto si se cambia el tipo de valor
                            string VinculaDropDownList = string.Format(" dtvgvNPValorizacion{0}", r.RowIndex);
                            ddlTipoValor4.CssClass += (VinculaDropDownList);
                            ddlTipoValor5.CssClass += (VinculaDropDownList);
                            ddlTipoValor6.CssClass += (VinculaDropDownList);

                            InicioDescripcion.InnerText = NP.Valorizacion.Descripcion;

                            //Llena la valorización con los valores ingresados
                            (r.Controls[0].FindControl("usuarioV") as HtmlGenericControl).Visible = true;
                            (r.Controls[0].FindControl("txtUsuarioV") as TextBox).Text = string.Format("{0} {1}", NP.Valorizacion.Usuario.Nombre, NP.Valorizacion.Usuario.ApellidoPaterno);
                            (r.Controls[0].FindControl("txtCantidadV") as TextBox).Text = Util.Funcion.FormatoNumerico(NP.Valorizacion.Cantidad);
                            (r.Controls[0].FindControl("txtDescripcionV") as TextBox).Text = NP.Valorizacion.Descripcion;
                            (r.Controls[0].FindControl("fechaV") as HtmlGenericControl).Visible = true;
                            (r.Controls[0].FindControl("txtFechaV") as TextBox).Text = NP.Valorizacion.Fecha.ToShortDateString();

                            //Regulariza string para quitar caracteres \n, \r, etc... con el fin de sólo comprar contenido
                            string DescAjuste = Regex.Replace(NP.Ajuste.Descripcion, @"\s+", string.Empty);
                            string DescValorizacion = Regex.Replace(NP.Valorizacion.Descripcion, @"\s+", string.Empty);

                            //marca con asterisco si la cantidad y descripción es distinta entre el ajuste y la valorización
                            if (NP.Ajuste.Cantidad != NP.Valorizacion.Cantidad || !string.Equals(DescAjuste, DescValorizacion, StringComparison.OrdinalIgnoreCase) || NP.Creacion.ValorUnitario != NP.Valorizacion.ValorUnitario)
                                (r.Controls[0].FindControl("asteriscoV") as HtmlGenericControl).Visible = true;

                            if (NP.Ajuste.Cantidad != NP.Valorizacion.Cantidad)
                                (r.Controls[0].FindControl("asteriscoVC") as HtmlGenericControl).Visible = true;

                            if (!string.Equals(DescAjuste, DescValorizacion, StringComparison.OrdinalIgnoreCase))
                                (r.Controls[0].FindControl("asteriscoVD") as HtmlGenericControl).Visible = true;

                            if (NP.Creacion.ValorUnitario != NP.Valorizacion.ValorUnitario)
                                (r.Controls[0].FindControl("asteriscoVU") as HtmlGenericControl).Visible = true;

                            //si tiene 1 item, es CLP
                            if (this.ddlTipoValor1.Items.Count == 1)
                            {
                                txtVUV.Text = Util.Funcion.FormatoNumericoEntero(NP.Valorizacion.ValorUnitario);
                                txtVTV.Text = Util.Funcion.FormatoNumericoEntero(NP.Valorizacion.Cantidad * NP.Valorizacion.ValorUnitario);
                                txtVPV.Text = Util.Funcion.FormatoNumericoEntero(hfValorTotalProyecto.Value);
                            }
                            else
                            {
                                txtVUV.Text = Util.Funcion.FormatoNumerico(NP.Valorizacion.ValorUnitario);
                                txtVTV.Text = Util.Funcion.FormatoNumerico(NP.Valorizacion.Cantidad * NP.Valorizacion.ValorUnitario);
                                txtVPV.Text = Util.Funcion.FormatoNumerico(hfValorTotalProyecto.Value);
                            }

                            (r.Controls[0].FindControl("btnValorizar") as Button).Visible = false;

                            //Cierra la NP
                            if (NP.IDEstadoNP == 3)
                                lblColor.CssClass = "label label-success"; //Valorizada
                        }

                        if (NP.IDEstadoNP >= 4)
                            lblColor.CssClass = "label label-primary"; //Facturada
                    }
                }
                else //NP Eliminada
                {
                    (r.Controls[0].FindControl("rowNP") as HtmlGenericControl).Attributes["class"] = "panel panel-danger";
                    (r.Controls[0].FindControl("opcion") as HtmlGenericControl).Visible = false;
                    lblColor.CssClass = string.Empty;
                    lblColor.Text = "<i class=\"fa fa-trash\" style=\"margin-left: 4px;margin-right: 4px;\"></i>";
                    InicioDescripcion.InnerText = NP.Creacion.Descripcion;
                }
            }
        }

        protected void gvNP_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.CommandArgument.ToString()))
            {
                int rowIndex = Convert.ToInt32(e.CommandArgument);
                GridViewRow r = gvNP.Rows[rowIndex];
                int IDNP = Convert.ToInt16(gvNP.DataKeys[rowIndex].Value);
                string IDCompuestaNP = (r.FindControl("tituloNP") as HyperLink).Text.Split(' ')[0];
                int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);
                Entities.NotaPedidoEntity NPCreacion = new Entities.NotaPedidoEntity(IDNP);


                switch (e.CommandName)
                {
                    case "Ajustar":
                        //El usuario lo identifica en la función CargaConfirmarEvento
                        Entities.NotaPedidoEntity NPAjuste = new Entities.NotaPedidoEntity(IDNP);
                        //NPCreacion.Creacion = new Entities.CreacionEntity(Convert.ToInt16((r.FindControl("txtCantidadC") as TextBox).Text), (r.FindControl("txtDescripcionC") as TextBox).Text.Trim());
                        NPAjuste.Ajuste = new Entities.AjusteEntity(Convert.ToDecimal((r.FindControl("txtCantidadA") as TextBox).Text), (r.FindControl("txtDescripcionA") as TextBox).Text.Trim(), Convert.ToDateTime((r.FindControl("txtFechaRealEntregaA") as TextBox).Text));
                        ViewState.Add("AjustarNP", NPAjuste);

                        NPCreacion.Creacion = new Entities.CreacionEntity(Convert.ToDecimal((r.FindControl("txtCantidadC") as TextBox).Text), Convert.ToDecimal((r.FindControl("txtVUC") as TextBox).Text), (r.FindControl("txtDescripcionC") as TextBox).Text.Trim());
                        ViewState.Add("CreacionNP", NPCreacion);
                        //ViewState.Add("CreacionNP", descripcionC);

                        string TituloAjuste = string.Format("Ajustar NP {0}", IDCompuestaNP);
                        string ContenidoAjuste = string.Format("¿Está seguro de ajustar la NP {0}?", IDCompuestaNP);

                        //despliega popup de confirmación de valorización
                        CargaConfirmarEvento(TituloAjuste, ContenidoAjuste, btnAjustarNPConfirmar);
                        break;
                    case "Valorizar":
                        //El usuario lo identifica en la función CargaConfirmarEvento
                        Entities.NotaPedidoEntity NPValorizacion = new Entities.NotaPedidoEntity(IDNP);
                        NPValorizacion.Valorizacion = new Entities.ValorizacionEntity(Convert.ToDecimal((r.FindControl("txtCantidadV") as TextBox).Text), (r.FindControl("txtDescripcionV") as TextBox).Text.Trim(), Convert.ToDecimal((r.FindControl("txtVUV") as TextBox).Text));
                        ViewState.Add("ValorizarNP", NPValorizacion);
                        ViewState.Add("ValorTotalCreacion", Convert.ToDecimal((r.FindControl("txtCantidadC") as TextBox).Text) * Convert.ToDecimal((r.FindControl("txtVUC") as TextBox).Text));

                        string TituloValorizacion = string.Format("Valorizar NP {0}", IDCompuestaNP);
                        string ContenidoValorizacion = string.Format("¿Está seguro de valorizar la NP {0}?", IDCompuestaNP);

                        //despliega popup de confirmación de valorización
                        CargaConfirmarEvento(TituloValorizacion, ContenidoValorizacion, btnValorizarNPConfirmar);
                        break;
                    case "Trazabilidad":
                        tituloTrazado.InnerText = string.Format("Trazado NP {0}", IDCompuestaNP);

                        List<Entities.NotaPedidoEntity> Lst = NotaPedidoBL.GetTrazabilidadNP(IDNP);

                        gvTrazaC.DataSource = Lst.Where(x => x.IDEstadoNP == 1); //Filtra la creación y sus ediciones
                        gvTrazaC.DataBind();

                        gvTrazaC.UseAccessibleHeader = true;
                        gvTrazaC.HeaderRow.TableSection = TableRowSection.TableHeader;

                        List<Entities.NotaPedidoEntity> LstAjuste = Lst.Where(x => x.IDEstadoNP == 2).ToList(); //Filtra ajuste

                        valorizacionTrazabilidad.Visible = false;
                        ajusteTrazabilidad.Visible = false;

                        if (LstAjuste.Count > 0)
                        {
                            gvTrazaA.DataSource = LstAjuste;
                            gvTrazaA.DataBind();

                            gvTrazaA.UseAccessibleHeader = true;
                            gvTrazaA.HeaderRow.TableSection = TableRowSection.TableHeader;
                            ajusteTrazabilidad.Visible = true;

                            List<Entities.NotaPedidoEntity> LstValorizacion = Lst.Where(x => x.IDEstadoNP == 3).ToList(); //Filtra valorización

                            if (LstValorizacion.Count > 0)
                            {
                                gvTrazaV.DataSource = LstValorizacion;
                                gvTrazaV.DataBind();

                                gvTrazaV.UseAccessibleHeader = true;
                                gvTrazaV.HeaderRow.TableSection = TableRowSection.TableHeader;
                                valorizacionTrazabilidad.Visible = true;
                            }
                        }


                        ScriptManager.RegisterStartupScript(this, GetType(), "TrazabilidadNP", "$('#" + trazabilidadNP.ClientID + "').modal({backdrop: 'static',keyboard: true});", true);
                        break;
                }

                Session.Remove("NP"); //Destruye NP almacenada
                BindDataNP(IDProyecto);
            }
        }

        protected void gvNP_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvNP.EditIndex = e.NewEditIndex;

            int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);
            BindDataNP(IDProyecto);

            Util.DeshabilitaDIV(formularioAgregarNP);
        }

        protected void gvNP_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvNP.EditIndex = -1;

            int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);
            BindDataNP(IDProyecto);

            //Remueve ViewState de edición
            ViewState.Remove("EditarNP");
            ViewState.Remove("CampoAModificar");

            Util.HabilitaDIV(formularioAgregarNP);
        }

        protected void gvNP_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow r = gvNP.Rows[e.RowIndex];

            int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);

            //obtiene NP a editar
            Entities.NotaPedidoEntity NP = new Entities.NotaPedidoEntity(Convert.ToInt16(e.Keys["ID"]));
            int prueba = Convert.ToInt16((r.FindControl("ddlTipoNP") as DropDownList).SelectedItem.Value);
            NP.Creacion = new Entities.CreacionEntity(Convert.ToInt16((r.FindControl("ddlTipoUnidadC") as DropDownList).SelectedItem.Value), Convert.ToInt16((r.FindControl("ddlOCC") as DropDownList).SelectedItem.Value), Convert.ToDecimal((r.FindControl("txtVUC") as TextBox).Text), Convert.ToDecimal((r.FindControl("txtCantidadC") as TextBox).Text), (r.FindControl("txtDescripcionC") as TextBox).Text, prueba);

            try
            {
                Entities.CreacionEntity NPAntesDeSerEditada = (Entities.CreacionEntity)ViewState["NPAntesDeSerEditada"];

                //Verifica si hubo cambios en la edición...
                if ((NP.Creacion.Cantidad != NPAntesDeSerEditada.Cantidad) || (NP.Creacion.Descripcion != NPAntesDeSerEditada.Descripcion) || (NP.Creacion.ValorUnitario != NPAntesDeSerEditada.ValorUnitario) || (NP.Creacion.OrdenCompra.ID != NPAntesDeSerEditada.OrdenCompra.ID) || (NP.Creacion.TipoUnidad.ID != NPAntesDeSerEditada.TipoUnidad.ID) || (NP.Creacion.TipoNP.ID != NPAntesDeSerEditada.TipoNP.ID))
                {
                    string CampoModifica = string.Empty;
                    bool EditaCantidadOValorUnitario = false;

                    if (NP.Creacion.Cantidad != NPAntesDeSerEditada.Cantidad)
                    {
                        CampoModifica = "<strong>Cantidad</strong>";

                        //Indica que edita Cantidad
                        EditaCantidadOValorUnitario = true;
                    }

                    if (NP.Creacion.Descripcion != NPAntesDeSerEditada.Descripcion)
                    {
                        if (CampoModifica != string.Empty)
                            CampoModifica += ", ";

                        CampoModifica += "<strong>Descripción</strong>";
                    }

                    if (NP.Creacion.ValorUnitario != NPAntesDeSerEditada.ValorUnitario)
                    {
                        if (CampoModifica != string.Empty)
                            CampoModifica += ", ";

                        CampoModifica += "<strong>Precio Unitario</strong>";

                        //Indica que edita Valor Unitario
                        EditaCantidadOValorUnitario = true;
                    }

                    if (NP.Creacion.TipoUnidad.ID != NPAntesDeSerEditada.TipoUnidad.ID)
                    {
                        if (CampoModifica != string.Empty)
                            CampoModifica += ", ";

                        CampoModifica += "<strong>Unidad de medida</strong>";
                    }

                    if (NP.Creacion.OrdenCompra.ID != NPAntesDeSerEditada.OrdenCompra.ID)
                    {
                        if (CampoModifica != string.Empty)
                            CampoModifica += ", ";

                        CampoModifica += "<strong>Orden de Compra</strong>";
                    }

                    if (NP.Creacion.TipoNP.ID != NPAntesDeSerEditada.TipoNP.ID)
                    {
                        if (CampoModifica != string.Empty)
                            CampoModifica += ", ";

                        CampoModifica += "<strong>Tipo NP</strong>";
                    }

                    ViewState.Add("EditarNP", NP); //Almacena NP a editar
                    ViewState.Add("CampoAModificar", CampoModifica); //Almacena campos que se van a modificar para el historial

                    //Agrega NP y consulta si tiene EP abiertos (No facturados) para confirmación
                    if (EstadoPagoBL.GetTieneEPAbiertos(IDProyecto) && EditaCantidadOValorUnitario)
                    {
                        CargaEPNoFacturado(IDProyecto, "Proceda para Editar NP", btnEditarNPConEPAbierto);
                        ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "$('#EPAbierto').modal('show');", true);
                    }
                    else
                    {
                        //Si EditaCantidadOValorUnitario es True, significa que el valor del proyecto va a variar, por ende
                        //hay que consultar si el proyecto tiene EP emitidos, de ser ambas verdad, hay que actualizar el porcentaje
                        //de dicho(s) EP
                        if (EstadoPagoBL.GetTieneEPEmitidos(IDProyecto) && EditaCantidadOValorUnitario)
                            EditarNP("porce");
                        else
                            EditarNP(string.Empty);
                    }
                }
                else
                {
                    //Cierra la edición
                    gvNP.EditIndex = -1;

                    //actualiza gridview con np
                    BindDataNP(IDProyecto);

                    Util.HabilitaDIV(formularioAgregarNP);

                    //No se aplicó ningún cambio
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("warning", "Sin cambios...", "No se aplicó ningún cambio a la nota de pedido..."), true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        protected void gvNP_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);
            ViewState.Add("ConfirmarEliminarNP", Convert.ToInt16(e.Keys["ID"]));

            //Mensaje de confirmacion para eliminar la NP
            string Titulo = "Confirmar Eliminar NP";
            string Contenido = string.Format("¿Seguro que desea eliminar la NP {0}?", (gvNP.Rows[e.RowIndex].FindControl("tituloNP") as HyperLink).Text.Split(' ')[0]);

            CargaConfirmarEvento(Titulo, Contenido, btnEliminarNPConfirmar);
        }

        protected void gvNP_DataBound(object sender, EventArgs e)
        {
            App_Code.Seguridad.GetControlGridviewAutorizado(Page, Request.Path, gvNP.ID);
        }

        #region Eventos de Agregar, Editar, Eliminar, Ajustar, Valorizar NP cuando un Estado de Pago está abierto (no facturado)
        protected void btnEditarNPConEPAbierto_Click(object sender, EventArgs e)
        {
            EditarNP(ddlOperacionEPAbierto.SelectedItem.Value);
        }

        protected void btnAgregarNPConEPAbierto_Click(object sender, EventArgs e)
        {
            AgregarNP(ddlOperacionEPAbierto.SelectedItem.Value);
        }

        protected void btnEliminarNPConEPAbierto_Click(object sender, EventArgs e)
        {
            EliminarNP(ddlOperacionEPAbierto.SelectedItem.Value);
        }

        protected void btnValorizarConEPAbierto_Click(object sender, EventArgs e)
        {
            ValorizarNP(ddlOperacionEPAbierto.SelectedItem.Value);
        }
        #endregion

        #region Solicita confirmar eventos de Eliminar NP, Ajustar o Valorizar una NP
        protected void btnEliminarNPConfirmar_Click(object sender, EventArgs e)
        {
            int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);

            //Vuelve a verificar si tiene EP abiertos (No facturados) para confirmación
            if (EstadoPagoBL.GetTieneEPAbiertos(IDProyecto))
            {
                //CargaEPNoFacturado(IDProyecto, "Proceda para Eliminar NP", btnEliminarNPConEPAbierto);
                //ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "$('#EPAbierto').modal('show');", true);
                EliminarNP("monto");
            }
            else
                EliminarNP("porce");
        }

        protected void btnAjustarNPConfirmar_Click(object sender, EventArgs e)
        {
            //El ajuste de una NP no modifica los estados de pago
            AjustarNP(string.Empty);
        }

        protected void btnValorizarNPConfirmar_Click(object sender, EventArgs e)
        {
            int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);

            Entities.NotaPedidoEntity NPValorizar = (Entities.NotaPedidoEntity)ViewState["ValorizarNP"];
            decimal ValorTotalCreacion = Convert.ToDecimal(ViewState["ValorTotalCreacion"]);

            //Vuelve a verificar si tiene EP abiertos (No facturados) para confirmación
            if (EstadoPagoBL.GetTieneEPAbiertos(IDProyecto) && ValorTotalCreacion != (NPValorizar.Valorizacion.Cantidad * NPValorizar.Valorizacion.ValorUnitario))
            {
                CargaEPNoFacturado(IDProyecto, "Proceda para Valorizar NP", btnValorizarConEPAbierto);
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "$('#EPAbierto').modal('show');", true);
            }
            else
            {
                ValorizarNP("porce");
            }
        }
        #endregion

        #region Eventos de Agregar, Editar, Eliminar, Ajustar y Valorizar una NP
        private void AgregarNP(string DecisionEP)
        {
            int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
            int IDNotificacionApp = 0;

            //Agrega la nota de pedido. Ademas envía decisión sobre el EP abierto
            //NotaPedidoBL.AddNPProyecto(IDProyecto, IDUsuario, Convert.ToInt16(ddlTipoUnidad.SelectedItem.Value), Convert.ToInt16(ddlOC.SelectedItem.Value), Convert.ToDecimal(txtValorUnitario.Text), Convert.ToDecimal(txtCantidad.Text), txtDescripcionNP.Text.Trim(), DateTime.Now, DecisionEP, ref IDNotificacionApp);
            NotaPedidoBL.AddNPProyecto(IDProyecto, IDUsuario, Convert.ToInt16(ddlTipoUnidad.SelectedItem.Value), Convert.ToInt16(ddlTipoNP.SelectedItem.Value), Convert.ToInt16(ddlOC.SelectedItem.Value), Convert.ToDecimal(txtValorUnitario.Text), Convert.ToDecimal(txtCantidad.Text), txtDescripcionNP.Text.Trim(), DateTime.Now, DecisionEP, ref IDNotificacionApp);


            //NOTIFICACIÓN (12): Notifica a los usuarios que hay una nueva nota de pedido en un proyecto
            hubs.NotificacionAppHub.EnviaNotificacionesUsuariosConectados(GlobalHost.ConnectionManager.GetHubContext<hubs.NotificacionAppHub>());

            //default formulario
            txtCantidad.Text = string.Empty;
            txtDescripcionNP.Text = string.Empty;
            txtValorUnitario.Text = string.Empty;
            ddlTipoNP.SelectedIndex = -1;

            //actualiza gridview con np
            BindDataNP(IDProyecto);

            //Despliega mensaje de éxito
        }

        private void EliminarNP(string DecisionEP)
        {
            int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
            int IDNotificacionApp = 0;

            //Elimina lógicamente la NP.
            NotaPedidoBL.DelNP(Convert.ToInt16(ViewState["ConfirmarEliminarNP"].ToString()), IDUsuario, DecisionEP, ref IDNotificacionApp);
            BindDataNP(IDProyecto);

            //NOTIFICACIÓN(11): Notifica a los usuarios que se eliminó una NP
            hubs.NotificacionAppHub.EnviaNotificacionesUsuariosConectados(GlobalHost.ConnectionManager.GetHubContext<hubs.NotificacionAppHub>());

            ViewState.Remove("ConfirmarEliminarNP");

            //Despliega mensaje de éxito

        }

        private void EditarNP(string DecisionEP)
        {
            int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
            int IDNotificacionApp = 0;

            //obtiene NP a editar
            Entities.NotaPedidoEntity NP = (Entities.NotaPedidoEntity)ViewState["EditarNP"];
            string CamposAModificar = ViewState["CampoAModificar"].ToString();

            //Agrega nueva NP con fecha más reciente y datos actualizados. Ademas envía decisión sobre el EP abierto
            NotaPedidoBL.SetNP(NP.ID, IDUsuario, NP.Creacion.TipoUnidad.ID, NP.Creacion.TipoNP.ID, NP.Creacion.OrdenCompra.ID, NP.Creacion.ValorUnitario, NP.Creacion.Cantidad, NP.Creacion.Descripcion, CamposAModificar, DateTime.Now, DecisionEP, ref IDNotificacionApp);
            gvNP.EditIndex = -1;
            BindDataNP(IDProyecto);

            //NOTIFICACIÓN(10): Notifica a los usuarios que se editó una NP
            hubs.NotificacionAppHub.EnviaNotificacionesUsuariosConectados(GlobalHost.ConnectionManager.GetHubContext<hubs.NotificacionAppHub>());


            //Remueve ViewState para edición
            ViewState.Remove("NPAntesDeSerEditada");
            ViewState.Remove("EditarNP");
            ViewState.Remove("CampoAModificar");

            Util.HabilitaDIV(formularioAgregarNP);

            //Despliega mensaje de éxito
            ScriptManager.RegisterStartupScript(this, GetType(), "Pop126", Util.GetMensajeAlerta("success", "Operacion realizada...", "La NP se ha editado correctamente..."), true);
        }

        private void AjustarNP(string DecisionEP)
        {
            int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
            int IDNotificacionApp = 0;
            //List<Entities.NotaPedidoEntity> Lst = NotaPedidoBL.GetNP(IDProyecto);

            Entities.NotaPedidoEntity NP = (Entities.NotaPedidoEntity)ViewState["AjustarNP"];
            Entities.NotaPedidoEntity NPC = (Entities.NotaPedidoEntity)ViewState["CreacionNP"];

            //Entities.CreacionEntity NPC = (Entities.CreacionEntity)ViewState["EditarNP"];
            ViewState.Remove("AjustarNP");
            //ViewState.Remove("CreancionNP");

            //Pasar a Facturada NP de un proyecto por Regularizar que tenga los datos igual que en la creacion
            List<Entities.NotaPedidoEntity> NP_Reg = NotaPedidoBL.GetNPRegularizar(IDProyecto, NP.ID).ToList();


            if (NP_Reg.Count > 0)
            {
                if (NPC.Creacion.Cantidad == NP.Ajuste.Cantidad && NPC.Creacion.Descripcion == NP.Ajuste.Descripcion)
                {
                    NotaPedidoBL.AddAjuste(NP.ID, IDUsuario, NP.Ajuste.Cantidad, NP.Ajuste.Descripcion, NP.Ajuste.FechaRealEntrega, DateTime.Now, DecisionEP, ref IDNotificacionApp);
                    NotaPedidoBL.AddValorizacion(NP.ID, IDUsuario, NP.Ajuste.Cantidad, NPC.Creacion.ValorUnitario, NP.Ajuste.Descripcion, DateTime.Now, DecisionEP, ref IDNotificacionApp);
                    BindDataNP(IDProyecto);

                    //NOTIFICACIÓN (5): Notifica a los usuarios que hay que facturar una NP valorizada
                    hubs.NotificacionAppHub.EnviaNotificacionesUsuariosConectados(GlobalHost.ConnectionManager.GetHubContext<hubs.NotificacionAppHub>());
                }
                else
                {
                    NotaPedidoBL.AddAjuste(NP.ID, IDUsuario, NP.Ajuste.Cantidad, NP.Ajuste.Descripcion, NP.Ajuste.FechaRealEntrega, DateTime.Now, DecisionEP, ref IDNotificacionApp);
                    BindDataNP(IDProyecto);

                    //NOTIFICACIÓN(3): Notifica a los usuarios que hay que valorizar una NP
                    hubs.NotificacionAppHub.EnviaNotificacionesUsuariosConectados(GlobalHost.ConnectionManager.GetHubContext<hubs.NotificacionAppHub>());
                }

            }
            else
            {
                NotaPedidoBL.AddAjuste(NP.ID, IDUsuario, NP.Ajuste.Cantidad, NP.Ajuste.Descripcion, NP.Ajuste.FechaRealEntrega, DateTime.Now, DecisionEP, ref IDNotificacionApp);
                BindDataNP(IDProyecto);

                //NOTIFICACIÓN(3): Notifica a los usuarios que hay que valorizar una NP
                hubs.NotificacionAppHub.EnviaNotificacionesUsuariosConectados(GlobalHost.ConnectionManager.GetHubContext<hubs.NotificacionAppHub>());
            }

            //envía mensaje de éxito
        }

        private void ValorizarNP(string DecisionEP)
        {
            int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
            int IDNotificacionApp = 0;


            Entities.NotaPedidoEntity NP = (Entities.NotaPedidoEntity)ViewState["ValorizarNP"];
            ViewState.Remove("ValorizarNP");

            NotaPedidoBL.AddValorizacion(NP.ID, IDUsuario, NP.Valorizacion.Cantidad, NP.Valorizacion.ValorUnitario, NP.Valorizacion.Descripcion, DateTime.Now, DecisionEP, ref IDNotificacionApp);
            BindDataNP(IDProyecto);

            //NOTIFICACIÓN (5): Notifica a los usuarios que hay que facturar una NP valorizada
            hubs.NotificacionAppHub.EnviaNotificacionesUsuariosConectados(GlobalHost.ConnectionManager.GetHubContext<hubs.NotificacionAppHub>());

            //envía mensaje de éxito
        }
        #endregion

        private void CargaEPNoFacturado(int IDProyecto, string Titulo, Button btnConfirmar)
        {
            TituloEPAbierto.InnerHtml = Titulo;

            Entities.EstadoPagoEntity EPNoFacturado = EstadoPagoBL.GetEstadoPago(IDProyecto).Where(x => x.FacturaEmitida == false).ToList()[0];

            lblFechaIngresoEP.Text = EPNoFacturado.FechaCreacion.ToShortDateString();
            lblPorcentajeEP.Text = Util.Funcion.FormatoNumerico(EPNoFacturado.Porcentaje) + "%";
            lblMontoEP.Text = Util.Funcion.FormatoNumerico(EPNoFacturado.Monto); //no es necesario redondear el valor si es CLP, ya que al agregar un EP el monto viene validado (entero o decimal) según tipo de moneda
            lblGlosaEP.Text = EPNoFacturado.Glosa;

            btnEliminarNPConEPAbierto.Visible = false;
            btnAgregarNPConEPAbierto.Visible = false;
            btnEditarNPConEPAbierto.Visible = false;

            //habilita btn para confirmar eliminación
            btnConfirmar.Visible = true;
        }

        private void CargaConfirmarEvento(string TituloMsj, string ContenidoMsj, Button btnConfirmar)
        {
            this.TituloMsj.InnerHtml = TituloMsj;
            lblContenidoMsj.Text = ContenidoMsj;

            btnEliminarNPConfirmar.Visible = false;
            btnAjustarNPConfirmar.Visible = false;
            btnValorizarNPConfirmar.Visible = false;

            btnConfirmar.Visible = true;

            ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "$('#ConfirmaAccion').modal('show');", true);
        }

        private void BindDataNP(int IDProyecto)
        {
            try
            {
                List<Entities.NotaPedidoEntity> Lst = NotaPedidoBL.GetNP(IDProyecto);
                string CantidadNPValida = Lst.Where(x => x.Estado == true).ToList().Count.ToString();

                //obtiene número total de NP válidas del proyecto
                ViewState.Add("TotalNPValidas", CantidadNPValida);

                gvNP.DataKeyNames = new string[1] { "ID" }; //PK
                gvNP.DataSource = Lst;

                txtIDNP.Text = string.Format("{0}-{1}", IDProyecto, Lst.Count + 1);

                //sumatoria total valor proyecto
                double ValorTotalProyecto = ProyectoBL.GetValorTotalProyecto(IDProyecto);
                hfValorTotalProyecto.Value = ValorTotalProyecto.ToString();

                //true: tipo de valor NO es CLP
                if (ddlTipoValor1.Items.Count > 1)
                    txtValorTotalProyecto.Text = Util.Funcion.FormatoNumerico(ValorTotalProyecto);
                else
                    txtValorTotalProyecto.Text = Util.Funcion.FormatoNumericoEntero(ValorTotalProyecto);

                lblContNP.Text = CantidadNPValida;

                gvNP.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }
        #endregion

        private void BindOCProyecto(DropDownList ddlOC)
        {
            List<Entities.DocumentoComercialEntity> Lst = ProyectoBL.GetOCProyecto(Convert.ToInt16(hfIdProyecto.Value));
            ddlOC.DataSource = Lst;
            ddlOC.DataBind();
        }

        private void BindTipoUnidad(DropDownList ddlTipoUnidad)
        {
            ddlTipoUnidad.DataSource = TipoUnidadBL.GetTipoUnidad(0, string.Empty, string.Empty, false);
            ddlTipoUnidad.DataBind();
        }


        private void BindTipoNP(DropDownList ddlTipoNP)
        {

            List<Entities.TipoNPEntity> lstData = NotaPedidoBL.GetTipoNP();
            ddlTipoNP.DataTextField = "nombre";
            ddlTipoNP.DataValueField = "id";
            ddlTipoNP.DataSource = lstData;
            ddlTipoNP.DataBind();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Microsoft.AspNet.SignalR;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.proyecto.visualizar
{
    public partial class datos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    CargaInicial(Convert.ToInt16(Request.QueryString["id"].ToString()));
                }
                else
                {
                    Page.Title = "Proyecto no encontrado...";
                }
            }
        }

        protected override void OnInitComplete(EventArgs e)
        {
            base.OnInitComplete(e);

            //disponibiliza controles según autorizaciones del administrador
            CheckControlEnabled();
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
        }

        private void BindListJefePRoyecto()
        {
            //lvJefeProyecto.Items.Clear();
            int IDProyecto = Convert.ToInt16(Request.QueryString["id"].ToString());
            List<Entities.UsuarioEntity> LstJefes = ProyectoBL.GetJefeProyecto(IDProyecto).Where(x => x.EsJefeProyecto == true).ToList();


            if (LstJefes.Count > 0)
            {
                foreach (Entities.UsuarioEntity item in LstJefes)
                    item.Nombre = string.Format("<br><i class='fa fa-check' aria-hidden='true'></i>&nbsp;{0}", item.Nombre);
            }
            else
            {
                LstJefes.Add(new Entities.UsuarioEntity("<br><span class='text-danger'><i class='fa fa-times' aria-hidden='true'></i>&nbsp;Sin Jefe de Proyecto asignado</span>", string.Empty));
            }


            lvJefeProyecto.DataSource = LstJefes;
            lvJefeProyecto.DataBind();
        }


        private void BindListBoxJefeProyecto()
        {
            lstJefeProyecto.Items.Clear();

            int IDProyecto = Convert.ToInt16(Request.QueryString["id"].ToString());
            List<Entities.UsuarioEntity> LstJefes = ProyectoBL.GetJefeProyecto(IDProyecto);

            foreach (Entities.UsuarioEntity Usuario in LstJefes)
            {
                string Nombre = string.Format("{0} {1} {2}", Usuario.Nombre, Usuario.ApellidoPaterno, Usuario.ApellidoMaterno);
                ListItem Item = new ListItem(Nombre, Usuario.ID.ToString());
                Item.Selected = Usuario.EsJefeProyecto;

                lstJefeProyecto.Items.Add(Item);
            }
        }


        private void CheckControlEnabled()
        {
            //oculta controles restringidos
            proyectoPadre.Visible = false;
            fechaComprometida.Visible = false;
            fechaPlanificada.Visible = false;
            ejecutivoComercial.Visible = false;
            valorProyecto.Visible = false;
            direccion.Visible = false;
            comuna.Visible = false;
            btnEditar.Visible = false;
            gvDocComercial.Columns[4].Visible = false; //botón eliminar documento comercial
            gvDocComercial.Columns[3].Visible = false; //botón editar documento comercial

            App_Code.Seguridad.GetControlAutorizado(Page, Request.Path);
        }

        private void CargaInicial(int IDProyecto)
        {
            Entities.ProyectoEntity Proyecto = ProyectoBL.GetCabeceraProyecto(IDProyecto);

            if (Proyecto != null)
            {
                txtIDProyecto.Text = Proyecto.ID.ToString();
                hfIdProyecto.Value = Proyecto.ID.ToString(); //almacena Id de proyecto

                if (Proyecto.Padre != null)
                {
                    txtPadre.Text = Proyecto.Padre.Nombre;
                    hfPadre.Value = Proyecto.Padre.ID.ToString();
                }

                //Como siempre va a la BD a consultar le coloca al value dos espacion en blanco y para el autocomplete no sirve, entonces si no trajo dato de la BD le coloco el espacio que necesito
                if (Proyecto.SupervisorMontaje.Nombre == "")
                {
                    txtSupervisor.Text = "";
                }
                else
                {
                    txtSupervisor.Text = string.Format("{0} {1} {2}", Proyecto.SupervisorMontaje.Nombre, Proyecto.SupervisorMontaje.ApellidoPaterno, Proyecto.SupervisorMontaje.ApellidoMaterno);
                }

                txtNombre.Text = Proyecto.Nombre;
                txtCliente.Text = Proyecto.Cliente.NombreFantasia;
                hfCliente.Value = Proyecto.Cliente.ID.ToString();
                txtFechaIngreso.Text = Proyecto.FechaIngreso.ToShortDateString();
                txtFechaPlanificada.Text = Proyecto.FechaPlanificada.ToShortDateString();
                txtFechaComprometida.Text = Proyecto.FechaComprometida.ToShortDateString();
                txtEjecutivo.Text = string.Format("{0} {1} {2}", Proyecto.EjecutivoComercial.Nombre, Proyecto.EjecutivoComercial.ApellidoPaterno, Proyecto.EjecutivoComercial.ApellidoMaterno);
                //txtSupervisor.Text = string.Format("{0} {1} {2}", Proyecto.SupervisorMontaje.Nombre, Proyecto.SupervisorMontaje.ApellidoPaterno, Proyecto.SupervisorMontaje.ApellidoMaterno);
                hfEjecutivo.Value = Proyecto.EjecutivoComercial.ID.ToString();
                hfSupervisor.Value = Proyecto.SupervisorMontaje.ID.ToString();
                BindListJefePRoyecto();
                lstJefeProyecto.Visible = false; //control se muestra al presionar Editar
                txtTiempoDiseño.Enabled = false;
                txtTiempoDiseño.Text = Proyecto.Tiempo.ToString();
                txtSupervisor.Enabled = false;
                txtDireccion.Text = Proyecto.Direccion;
                txtComuna.Text = Proyecto.Comuna;
                //lblTipoMoneda.Text = Proyecto.TipoValor.Sigla;
                lblDirectorioDC.Text = string.Format("<b>{0}</b>", Proyecto.Directorio);

                //crea contador de caracteres
                txtDescripcionDoc.Attributes.Add("onkeyup", "countChar(this, '" + lblContDescDoc.ClientID + "', 30)");
                txtDescripcionArchivo.Attributes.Add("onkeyup", "countChar(this, '" + lblContDA.ClientID + "', 200)");

                //bind's
                BindTipoDocumento();
                BindDataDocComercial(IDProyecto);
                BindDataDocProyecto(IDProyecto);

                //carga total proyecto
                double ValorTotalProyecto = ProyectoBL.GetValorTotalProyecto(IDProyecto); //almacena valor total proyecto para realizar cálculo 

                //Agrega tipo de valor del proyecto
                ddlTipoValor.Items.Add(Proyecto.TipoValor.Sigla); //bloqueado por requerimiento
                notas_de_pedido.AddFunctionValorTotalProyectoCLP(ref ddlTipoValor1, Proyecto.TipoValor.Sigla, Proyecto.TipoValor.Valor); //valor total proyecto (datos)

                if (Proyecto.TipoValor.Valor > 1)
                {
                    txtValorProyecto.Text = Util.Funcion.FormatoNumerico(ValorTotalProyecto);

                    //Agrega atributos de datos (textbox y valor) para generar cálculo de onchange en ddlTipoValor
                    ddlTipoValor1.Attributes.Add("txt", txtValorProyecto.ClientID);
                    ddlTipoValor1.Attributes.Add("valor", ValorTotalProyecto.ToString());
                }
                else
                    txtValorProyecto.Text = Util.Funcion.FormatoNumericoEntero(ValorTotalProyecto);
            }
        }

        protected void btnAgregarDocComercial_ServerClick(object sender, EventArgs e)
        {
            int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);
            int IDTipoDoc = Convert.ToInt16(ddlTipoDocumento.SelectedItem.Value);
            string Desc = txtDescripcionDoc.Text.Trim();
            string NombreArchivo = fArchivo.FileName;
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();

            //inserta registro y retorna directorio del proyecto
            string Directorio = ProyectoBL.AddDocumentoComercial(IDUsuario, Convert.ToInt16(ddlTipoDocumento.SelectedItem.Value), IDProyecto, NombreArchivo, DateTime.Now, txtDescripcionDoc.Text);

            try
            {
                string RutaArchivo = string.Format("{0}\\{1}", Directorio, fArchivo.FileName);

                //servidor de archivos compartidos
                string ServidorArchivosProyecto = ConfigurationManager.AppSettings["servidorArchivosProyecto"].ToString();
                //obtiene credenciales para ingresar a la carpeta compartida
                string UsuarioCarpetaCompartidaServidorArchivosProyecto = ConfigurationManager.AppSettings["usuarioCarpetaCompartidaServidorArchivosProyecto"].ToString();
                string PasswordCarpetaCompartidaServidorArchivosProyecto = ConfigurationManager.AppSettings["passwordCarpetaCompartidaServidorArchivosProyecto"].ToString();

                using (App_Code.Seguridad.NetworkShareAccesser.Acceso(ServidorArchivosProyecto, UsuarioCarpetaCompartidaServidorArchivosProyecto, PasswordCarpetaCompartidaServidorArchivosProyecto))
                {
                    //crea el directorio si es que no existe
                    if (!Directory.Exists(Directorio))
                        Directory.CreateDirectory(Directorio);

                    fArchivo.SaveAs(RutaArchivo); //guarda archivo
                }

                BindDataDocComercial(IDProyecto);
                lblDirectorioDC.Text = string.Format("<b>{0}</b>", Directorio);

                //default formulario
                txtDescripcionDoc.Text = string.Empty;
            }
            catch /*(Exception ex)*/
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", "No se pudo subir el archivo al directorio."), true);
            }
        }

        protected void gvDocComercial_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                //cuando edita debe cargar el desplegable tipo documento...
                if ((r.RowState & DataControlRowState.Edit) == DataControlRowState.Edit)
                {
                    //asigna contador de caracteres
                    (r.Controls[0].FindControl("txtDesc") as TextBox).Attributes.Add("onkeyup", "countChar(this, '" + (r.Controls[0].FindControl("lblContDesc") as Label).ClientID + "', 30)");

                    DropDownList ddlTipoDoc = (r.Controls[0].FindControl("ddlTipoDocumento") as DropDownList);
                    ddlTipoDoc.DataSource = ProyectoBL.GetTipoDocumento();
                    ddlTipoDoc.SelectedValue = ((Entities.DocumentoComercialEntity)r.DataItem).TipoDocumento.ID.ToString();
                    ddlTipoDoc.DataBind();
                }
            }
        }

        protected void gvDocComercial_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvDocComercial.EditIndex = e.NewEditIndex;
            BindDataDocComercial(Convert.ToInt16(hfIdProyecto.Value));
            Util.DeshabilitaDIV(formDocComercial);
        }

        protected void gvDocComercial_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            Entities.DocumentoComercialEntity DocComercial = new Entities.DocumentoComercialEntity(Convert.ToInt16(e.Keys["ID"].ToString()), (gvDocComercial.Rows[e.RowIndex].FindControl("txtDesc") as TextBox).Text.Trim(), Convert.ToInt16((gvDocComercial.Rows[e.RowIndex].FindControl("ddlTipoDocumento") as DropDownList).SelectedItem.Value));

            try
            {
                //actualiza valores
                ProyectoBL.SetDocumentoComercial(DocComercial.ID, DocComercial.TipoDocumento.ID, DocComercial.Descripcion);

                //actualiza la tabla con nuevo valores
                gvDocComercial.EditIndex = -1;
                BindDataDocComercial(Convert.ToInt16(hfIdProyecto.Value));

                //finalizada la acción habilita nuevamente el formulario de búsqueda
                Util.HabilitaDIV(formDocComercial);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        protected void gvDocComercial_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvDocComercial.EditIndex = -1;
            BindDataDocComercial(Convert.ToInt16(hfIdProyecto.Value));
            Util.HabilitaDIV(formDocComercial);
        }

        protected void gvDocComercial_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                //cambia estado en la BD
                string RutaArchivo = ProyectoBL.DelDocumentoComercial((Convert.ToInt16(e.Keys["ID"].ToString())));

                //Elimina el archivo físicamente del directorio
                if (File.Exists(RutaArchivo))
                {
                    File.Delete(RutaArchivo);
                }

                //quita y descuenta el contador
                BindDataDocComercial(Convert.ToInt16(hfIdProyecto.Value));
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }

            ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("success", "Operación satisfactoria", "El registro fue eliminado."), true);
        }

        protected void btnAgregarDocProyecto_ServerClick(object sender, EventArgs e)
        {
            try
            {
                int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);
                ProyectoBL.AddDocumentoProyecto(IDProyecto, txtDescripcionArchivo.Text.Trim(), txtRutaArchivo.Text.Trim(), DateTime.Now);
                BindDataDocProyecto(IDProyecto);

                //default formulario
                txtDescripcionArchivo.Text = string.Empty;
                txtRutaArchivo.Text = string.Empty;
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        protected void gvDocProyecto_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                //cambia estado en la BD
                ProyectoBL.DelDocumentoProyecto((Convert.ToInt16(e.Keys["ID"].ToString())));

                //quita y descuenta el contador
                int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);
                BindDataDocProyecto(IDProyecto);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }

            ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("success", "Operación satisfactoria", "El registro fue eliminado."), true);
        }

        private void BindDataDocComercial(int IDProyecto)
        {
            try
            {
                List<Entities.DocumentoComercialEntity> Lst = ProyectoBL.GetDocumentoComercial(IDProyecto);
                gvDocComercial.DataKeyNames = new string[1] { "ID" }; //PK
                gvDocComercial.DataSource = Lst;
                gvDocComercial.DataBind();

                if (Lst.Count > 0)
                {
                    gvDocComercial.UseAccessibleHeader = true;
                    gvDocComercial.HeaderRow.TableSection = TableRowSection.TableHeader;

                    lblRegistroDC.Text = string.Format("<b>{0}</b> documentos en el directorio del proyecto ", Lst.Count);
                    lblRegistroDC.CssClass = "alert-info";
                }
                else
                {
                    lblRegistroDC.Text = "No hay documentos comerciales para este proyecto en ";
                    lblRegistroDC.CssClass = "alert-danger";
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        private void BindDataDocProyecto(int IDProyecto)
        {
            try
            {
                List<Entities.DocumentoProyectoEntity> Lst = ProyectoBL.GetDocumentoProyecto(IDProyecto);
                gvDocProyecto.DataKeyNames = new string[1] { "ID" };
                gvDocProyecto.DataSource = Lst;
                gvDocProyecto.DataBind();

                if (Lst.Count > 0)
                {
                    gvDocProyecto.UseAccessibleHeader = true;
                    gvDocProyecto.HeaderRow.TableSection = TableRowSection.TableHeader;

                    lblRegistroDP.Text = string.Format("<b>{0}</b> documentos en el directorio del proyecto ", Lst.Count);
                    lblRegistroDP.CssClass = "alert-info";
                }
                else
                {
                    lblRegistroDP.Text = "No hay archivos o directorios compartidos para este proyecto";
                    lblRegistroDP.CssClass = "alert-danger";
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                return;
            }
        }

        private void BindTipoDocumento()
        {
            ddlTipoDocumento.DataSource = ProyectoBL.GetTipoDocumento().OrderByDescending(x => x.VinculaOC); //OrderByDescending hace que traiga Orden de compra primero
            ddlTipoDocumento.DataBind();
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            HabilitaEdicionCabecera(true);
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                hfPadre.Value = txtPadre.Text == string.Empty ? string.Empty : hfPadre.Value; // validación necesaria para cuando el usuario confirma un proyecto y luego vuelve atrás en el navegador

                //if (((!string.IsNullOrEmpty(txtPadre.Text) && !string.IsNullOrEmpty(hfPadre.Value)) || (string.IsNullOrEmpty(txtPadre.Text) && string.IsNullOrEmpty(hfPadre.Value))) && !string.IsNullOrEmpty(hfEjecutivo.Value) || txtSupervisor.Text != "  ")

                if (((!string.IsNullOrEmpty(txtPadre.Text) && !string.IsNullOrEmpty(hfPadre.Value)) || (string.IsNullOrEmpty(txtPadre.Text) && string.IsNullOrEmpty(hfPadre.Value))) && !string.IsNullOrEmpty(hfEjecutivo.Value))
                {
                    int IDProyecto = Convert.ToInt16(txtIDProyecto.Text);
                    int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
                    string Nombre = txtNombre.Text.Trim();
                    DateTime FechaPlanificada = Convert.ToDateTime(txtFechaPlanificada.Text);
                    DateTime FechaComprometida = Convert.ToDateTime(txtFechaComprometida.Text);
                    int IDEjecutivo = Convert.ToInt16(hfEjecutivo.Value);
                    int IDSupervisor = 0;
                    int IDCliente = Convert.ToInt16(hfCliente.Value);

                    if (txtSupervisor.Text != "")
                    {
                        IDSupervisor = Convert.ToInt16(hfSupervisor.Value);
                    }
                    string Direccion = txtDireccion.Text.Trim();
                    string Comuna = txtComuna.Text.Trim();

                    int IDNotificacion1 = 0; //Retorna el ID de notificación de actualización de datos de proyecto (nombre, comuna o dirección).
                    int IDNotificacion2 = 0; //Retorna el ID de notificación de actualización de fecha comprometida del proyecto.
                    int IDNotificacion3 = 0; //Retorna el ID de notificación de actualización de fecha planificada del proyecto.
                    int IDNotificacion4 = 0; //Retorna el ID de notificación de cambio de jefe(s) de proyecto.
                    decimal Tiempo = 0;
                    if (txtTiempoDiseño.Text != "")
                    {
                        Tiempo = Convert.ToDecimal(txtTiempoDiseño.Text);
                    }
                    decimal TiempoDiseño = Tiempo;
                    //if (lstJefeProyecto.SelectedIndex == -1)
                    //{
                    //    TiempoDiseño = 0;
                    //}
                    bool validar = PaginaBL.GetHabilitarControlPagina(26, IDUsuario);


                    bool Edita = false;


                    if (validar == false)
                    {
                        List<Entities.UsuarioEntity> LstJefeProyecto = ProyectoBL.GetJefeProyecto(IDProyecto).Where(x => x.EsJefeProyecto == true).ToList();
                        if (LstJefeProyecto.Count > 0)
                        {
                            foreach (ListItem Usuario in lstJefeProyecto.Items)
                                LstJefeProyecto.Add(new Entities.UsuarioEntity { ID = Convert.ToInt16(Usuario.Value) });
                        }

                        Edita = ProyectoBL.SetCabeceraProyecto(IDProyecto, IDUsuario, Nombre, FechaComprometida, FechaPlanificada, IDEjecutivo, Direccion, Comuna, LstJefeProyecto, ref IDNotificacion1, ref IDNotificacion2, ref IDNotificacion3, ref IDNotificacion4, IDSupervisor, IDCliente, TiempoDiseño);

                    }
                    else
                    {
                        List<Entities.UsuarioEntity> LstJefeProyecto = new List<Entities.UsuarioEntity>();
                        //List<Entities.UsuarioEntity> LstJefes = ProyectoBL.GetJefeProyecto(IDProyecto).Where(x => x.EsJefeProyecto == true).ToList();

                        foreach (ListItem Usuario in lstJefeProyecto.Items)
                        {
                            if (Usuario.Selected)
                            {
                                LstJefeProyecto.Add(new Entities.UsuarioEntity { ID = Convert.ToInt16(Usuario.Value) });
                            }

                        }
                        Edita = ProyectoBL.SetCabeceraProyecto(IDProyecto, IDUsuario, Nombre, FechaComprometida, FechaPlanificada, IDEjecutivo, Direccion, Comuna, LstJefeProyecto, ref IDNotificacion1, ref IDNotificacion2, ref IDNotificacion3, ref IDNotificacion4, IDSupervisor, IDCliente, TiempoDiseño);

                    }

                    //bool Edita = false;
                    //Edita = ProyectoBL.SetCabeceraProyecto(IDProyecto, IDUsuario, Nombre, FechaComprometida, FechaPlanificada, IDEjecutivo, Direccion, Comuna, LstJefeProyecto, ref IDNotificacion1, ref IDNotificacion2, ref IDNotificacion3, ref IDNotificacion4);

                    if (Edita)
                    {
                        if (IDNotificacion1 != 0)
                        {
                            //cambia nombre del proyecto en el título
                            (Page.Master.Master.Master.FindControl("menu").FindControl("contenido").FindControl("titulo") as HtmlGenericControl).InnerText = string.Format("{0} - {1}", IDProyecto, Nombre);

                            //NOTIFICACIÓN (7): Notifica a los usuarios que se modificó el encabezado del proyecto (nombre, comuna o dirección)
                            //...
                        }

                        if (IDNotificacion2 != 0)
                        {
                            //NOTIFICACIÓN (8): Notifica a los usuarios que se modificó la fecha comprometida del proyecto
                            //...
                        }

                        if (IDNotificacion3 != 0)
                        {
                            //NOTIFICACIÓN (9): Notifica a los usuarios que se modificó la fecha planificada del proyecto
                            //...
                        }

                        if (IDNotificacion4 != 0)
                        {
                            //NOTIFICACIÓN (2): Notifica a los usuarios sobre asignación de jefe de proyecto
                            //...
                        }



                        //Envía las notificaciones a los usuarios conectados...
                        hubs.NotificacionAppHub.EnviaNotificacionesUsuariosConectados(GlobalHost.ConnectionManager.GetHubContext<hubs.NotificacionAppHub>());

                        ScriptManager.RegisterStartupScript(this, GetType(), "Edición exitosa", Util.GetMensajeAlerta("success", "Operación satisfactoria", "Los datos del proyecto fueron actualizados correctamente."), true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Sin cambios", Util.GetMensajeAlerta("warning", "No hubo modificaciones", "No se realizó ninguna actualización en el proyecto."), true);
                    }

                    //Desabilita los controles del formulario
                    HabilitaEdicionCabecera(false);
                }
                else
                {
                    if (string.IsNullOrEmpty(hfEjecutivo.Value)) rfvEjecutivo.IsValid = false;
                    if (!string.IsNullOrEmpty(txtPadre.Text) && string.IsNullOrEmpty(hfPadre.Value)) lblPadre.Visible = true;
                }

                BindListJefePRoyecto();
                //BindSuperMontaje();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Error edición", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            HabilitaEdicionCabecera(false);
            lblPadre.Visible = false; //(por si queda activo)
            CargaInicial(Convert.ToInt16(Request.QueryString["id"].ToString()));
        }

        private void HabilitaEdicionCabecera(bool R)
        {
            txtPadre.Enabled = R;
            txtNombre.Enabled = R;
            txtEjecutivo.Enabled = R;
            txtDireccion.Enabled = R;
            txtComuna.Enabled = R;
            txtSupervisor.Enabled = R;
            txtCliente.Enabled = R;
            txtTiempoDiseño.Enabled = R;
            btnEditar.Visible = !R;
            btnGuardar.Visible = R;
            btnCancelar.Visible = R;

            //verifica si rol tiene activada las opciones de disponibilizar txtFechaComprometida y txtFechaPlanificada 24 y 25
            //24: Permite disponibilizar Fecha comprometida (comercial) para ser editada (para conceder debe estar seleccionada la opción "Permite editar los datos del proyecto.").
            //25: Permite disponibilizar Fecha planificada (producción) para ser editada (para conceder debe estar seleccionada la opción "Permite editar los datos del proyecto.").
            if (R)
            {
                int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();

                txtFechaComprometida.Enabled = PaginaBL.GetHabilitarControlPagina(24, IDUsuario);
                txtFechaPlanificada.Enabled = PaginaBL.GetHabilitarControlPagina(25, IDUsuario);

                lstJefeProyecto.Enabled = PaginaBL.GetHabilitarControlPagina(26, IDUsuario);
                txtSupervisor.Enabled = PaginaBL.GetHabilitarControlPagina(27, IDUsuario);
                txtCliente.Enabled = true;
                

                //txtSupervisor.Attributes.Remove("disabled");
                //txtSupervisor.Visible = R;
                //BindListBoxSuperMontaje();

                if (lstJefeProyecto.Enabled)
                {
                    lstJefeProyecto.Attributes.Remove("disabled");
                    lstJefeProyecto.Visible = R;
                    BindListBoxJefeProyecto();
                    txtTiempoDiseño.Enabled = true;
                   

                }
                else
                {
                    BindListJefePRoyecto();
                    //BindSuperMontaje();
                }
            }
            else
            {
                txtFechaComprometida.Enabled = R;
                txtFechaPlanificada.Enabled = R;
                lstJefeProyecto.Attributes.Add("disabled", "disabled");
                lstJefeProyecto.Visible = R;
                BindListJefePRoyecto();
                //BindSuperMontaje();
            }
        }

        protected void gvDocComercial_DataBound(object sender, EventArgs e)
        {
            App_Code.Seguridad.GetControlGridviewAutorizado(Page, Request.Path, gvDocComercial.ID);
        }

        [WebMethod]
        public static string[] GetCliente(string prefix)
        {
            return ClienteBL.GetClienteJSON(prefix).ToArray();
        }

        [WebMethod]
        public static List<string> GetProyectoPadre(string prefix)
        {
            return ProyectoBL.GetProyectoPadreJSON(prefix);
        }

        [WebMethod]
        public static string[] GetEjecutivoComercial(string prefix)
        {
            return UsuarioBL.GetEjecutivoComercialJSON(prefix).ToArray();
        }

        [WebMethod]
        public static string[] GetSupervisorMontaje(string prefix)
        {
            return UsuarioBL.GetSupervisorMontajeJSON(prefix).ToArray();
        }
    }
}
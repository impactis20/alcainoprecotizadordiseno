﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MakeUp.BL;


namespace MakeUp.App.UI.makeup2.proyecto.visualizar
{
    public partial class facturacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    BindDataGrid(Convert.ToInt16(Request.QueryString["id"].ToString()));
                }
                else
                {
                    Page.Title = "Proyecto no encontrado...";
                }
            }
        }

        private void BindDataGrid(int IDProyecto)
        {
            hfIdProyecto.Value = IDProyecto.ToString();
            List<Entities.FacturaEntity> Lst = FacturacionBL.GetFacturaProyecto(IDProyecto);
            gvFactura.DataSource = Lst;
            gvFactura.DataBind();

            if (Lst.Count > 0)
            {
                gvFactura.UseAccessibleHeader = true;
                gvFactura.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else {
                lblBuscar.Text = "El Proyecto no tiene Facturas asociadas";
                lblBuscar.CssClass = "alert-danger";
            }

        }

        protected void gvFactura_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.FacturaEntity Item = (Entities.FacturaEntity)r.DataItem;
                Label lblMontoFactura = (r.Controls[0].FindControl("lblMontoFactura") as Label);
                Label lblColor = (r.Controls[0].FindControl("lblColor") as Label);
                Label lblGlosa = (r.Controls[0].FindControl("lblGlosa") as Label);
                Label lblNroFactura = (r.Controls[0].FindControl("lblNroFactura") as Label); 
                HyperLink urlDoc = (r.Controls[0].FindControl("doc") as HyperLink);
                TextBox txtNroFact = (r.Controls[0].FindControl("lblNroFactura") as TextBox);
                urlDoc.NavigateUrl = string.Format("archivo.ashx?id={0}", Util.Funcion.Encriptar(Item.UrlDoc)); //asigna url del documento encriptada


                lblColor.Attributes.Add("style", "margin-left: 4px;margin-right: 4px;");
                lblColor.Attributes.Add("rel", "tooltip");
                lblColor.Attributes.Add("data-placement", "top");
                lblColor.Attributes.Add("data-toggle", "tooltip");

                switch (Item.IDEstadoFactura)
                {
                    case 1:
                        //lblColor.Attributes.Add("style", "background-color:#d9534f !important"); // rojo
                        lblColor.CssClass = "fa fa-clock-o fa-2x text-danger";
                        lblColor.Attributes.Add("title", "Factura pendiente de despacho");
                        break;
                    case 2:
                        //lblColor.Attributes.Add("style", "background-color:#5cb85c !important"); // verde
                        lblColor.CssClass = "fa fa-check fa-2x text-success";
                        lblColor.Attributes.Add("title", "Factura despachada");
                        //lblColor.Attributes.Add("style", "background-color:#337ab7 !important"); // azul
                        break;
                }

                if (Item.Proyecto.TipoValor.ID == 1 || Item.Proyecto.TipoValor.ID == 2)
                {
                    //cual el nro de factura es nulo el valor es -1
                    if (Item.NroFactura != -1)
                    {
                        //Enlace se habilita cuando la factura está enviada al SII
                        if (Item.EnviadoSII)
                        {
                            urlDoc.Visible = true;
                        }
                        else
                        {
                            lblNroFactura.Text = Item.NroFactura.ToString();
                            urlDoc.Visible = false;
                        }
                    }
                    else
                    {
                        lblNroFactura.Text = "-";
                    }

                    lblMontoFactura.Text = string.Format("CLP {0}", Util.Funcion.FormatoNumericoEntero(Item.Monto));
                }
                else if (Item.Proyecto.TipoValor.ID == 3 || Item.Proyecto.TipoValor.ID == 4)
                {
                    if (Item.NroFactura == -1)
                    {
                        lblNroFactura.Text = "0";
                        lblMontoFactura.Text = string.Format("{0} {1}", Item.Proyecto.TipoValor.Sigla, Item.Monto);

                    }
                    else
                    {

                        lblNroFactura.Text = Item.NroFactura.ToString();
                        lblMontoFactura.Text = string.Format("{0} {1}", Item.Proyecto.TipoValor.Sigla, Item.Monto);
                        urlDoc.Visible = true;
                    }
                }

            }

            //lblMontoFactura.Text = string.Format("CLP {0}", Util.Funcion.FormatoNumericoEntero(Item.Monto));
        }
    }
}
﻿<%@ Page Title="Notas de pedido del proyecto (eliminado)" Language="C#" MasterPageFile="~/makeup2/proyecto/eliminado/proyecto.master" AutoEventWireup="true" CodeBehind="notas-de-pedido.aspx.cs" Inherits="MakeUp.App.UI.makeup2.proyecto.eliminado.notas_de_pedido" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headProyecto" runat="server">
    <link href="../../../css/bootstrap-select.css" rel="stylesheet" />
    <link href="../../../css/personalizado.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenidoProyecto" runat="server">
    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/languages/es.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/number-format-using-numeral.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/dropdown-igualitario.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/count-character-textbox.js") %>'></script>

    <script>
        //guarda el estado del acordión
        $(function () {
            var paneName = $("[id*=hfAcordion]").val() != "" ? $("[id*=hfAcordion]").val() : "collapseOne";             
            //Remove the previous selected Pane.
            $("[id*=accordion] .in").removeClass("in");             
            //Set the selected Pane.
            $("#" + paneName).collapse("show");             
            //When Pane is clicked, save the ID to the Hidden Field.
            $(".panel-heading a").click(function () {
                $("[id*=hfAcordion]").val($(this).attr("href").replace("#", ""));
            });
            
            //imprime informe resumen de NP
            $("[id*=btnImprimirResumenNP").click(function () {
                $('[id*=resumenNP]').modal('hide');

                var contents = $("[id*=printResumenNP").html();
                var frame1 = $('<iframe />');
                frame1[0].name = "frame1";
                frame1.css({ "position": "absolute", "top": "-1000000px" });
                $("body").append(frame1);
                var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
                frameDoc.document.open();
                //Create a new HTML document.
                frameDoc.document.write('<html><head><title>Resumen de NP</title>');
                frameDoc.document.write('</head><body>');
                //Append the external CSS file.
                frameDoc.document.write('<link rel="stylesheet" href="../../../css/print-from-javascript/chrome.css" type="text/css" media="print" />');
                frameDoc.document.write('<link href="../../../css/bootstrap.min.css" rel="stylesheet" />');
                frameDoc.document.write('<link href="../../../css/font-awesome.min.css" rel="stylesheet" type="text/css" />');
                //Append the DIV contents.
                frameDoc.document.write(contents);
                frameDoc.document.write('</body></html>');
                frameDoc.document.close();
                setTimeout(function () {
                    window.frames["frame1"].focus();
                    window.frames["frame1"].print();
                    frame1.remove();
                }, 500);
            });
        });
    </script>

    <script type="text/javascript">
        //calcula valores de una NP al valorizar
        function ValorTotalValorizarNP(txtCantidad, txtValorUnitario, txtTotalNP, txtTotalProyecto, txtTotalNPCreacion, tipoValorProyecto) {
            var Cantidad = ConvertToNumber(document.getElementById(txtCantidad).value);
            var Unitario = ConvertToNumber(document.getElementById(txtValorUnitario).value);
            var TotalNPCreacion = ConvertToNumber(document.getElementById(txtTotalNPCreacion).value);
            var resultadoTotalNP = 0;
            var resultadoTotalProyecto = parseFloat(ConvertToNumber(('<%= hfValorTotalProyecto.Value %>'))) - parseFloat(TotalNPCreacion);

            numeral.language('es');

            if (Cantidad !== "" || Unitario !== "") {
                resultadoTotalNP = Math.round(Cantidad * Unitario)
                resultadoTotalProyecto += resultadoTotalNP;
            }

            if (tipoValorProyecto > 0) {
                document.getElementById(txtTotalNP).value = numeral(resultadoTotalNP).format('0,0.[00]'); //asigna valor total NP
                document.getElementById(txtTotalProyecto).value = numeral(resultadoTotalProyecto).format('0,0.[00]'); //asigna valor total Proyecto
            }
            else {
                document.getElementById(txtTotalNP).value = numeral(resultadoTotalNP).format('0,0'); //asigna valor total NP
                document.getElementById(txtTotalProyecto).value = numeral(resultadoTotalProyecto).format('0,0'); //asigna valor total Proyecto
            }
        }

        //traduce valores de una NP a pesos chilenos cuando agrega una NP o edita una NP.
        //Para que este evento se ejecute se debe cambiar el dropdownlist que va acompañado de los textboxs "Valor Total NP" o "Valor Total Proyecto"
        function NPValoresACLP(ddl, txtCantidad, txtValorUnitario, txtValorTotalNP, txtValorTotalProyecto, editaNP) {
            var valorItemSeleccionado = ConvertToNumber(document.getElementById(ddl.id).value);
            var ValorTotalProyecto = ConvertToNumber('<%= hfValorTotalProyecto.Value %>');
            
            var Cantidad = ConvertToNumber(document.getElementById(txtCantidad).value);
            var Unitario = ConvertToNumber(document.getElementById(txtValorUnitario).value);
            var ValorTotalNP = Cantidad * Unitario; //cálculo para adicionar el valor total del proyecto + los valores de txtCantidad y txtValorUnitarioNP

            numeral.language('es');

            if (editaNP === "True")
                ValorTotalProyecto = parseFloat(ValorTotalProyecto) - parseFloat(ConvertToNumber('0'));

            ValorTotalProyecto = parseFloat(ValorTotalProyecto) + ValorTotalNP;

            //si es igual a 1 NO es tipo de moneda CLP
            if (valorItemSeleccionado == 1){
                document.getElementById(txtValorTotalNP).value = numeral(ValorTotalNP * valorItemSeleccionado).format('0,0.[00]'); //Asigna valor a txtValorTotalNP
                document.getElementById(txtValorTotalProyecto).value = numeral(ValorTotalProyecto * valorItemSeleccionado).format('0,0.[00]'); //Asigna valor a txtValorTotalProyecto
            }
            else {
                document.getElementById(txtValorTotalNP).value = numeral(ValorTotalNP * valorItemSeleccionado).format('0,0'); //Asigna valor a txtValorTotalNP
                document.getElementById(txtValorTotalProyecto).value = numeral(ValorTotalProyecto * valorItemSeleccionado).format('0,0'); //Asigna valor a txtValorTotalProyecto
            }
        }
    </script>

    <asp:HiddenField ID="hfAcordion" runat="server" Value="" />
    <asp:HiddenField ID="hfIdProyecto" runat="server" />
    <asp:HiddenField ID="hfTipoMoneda" runat="server" />
    <asp:HiddenField ID="hfValorTotalProyecto" runat="server" />

    <asp:DropDownList id="ddlTipoValor1" runat="server" CssClass="selectpicker dtv1" Visible="false" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
    </asp:DropDownList>

    <div id="formularioAgregarNP" runat="server" class="form-group">
        <div class="row">
            <div class="col-lg-12">
                A este proyecto se agregaron <asp:Label id="lblContNP" runat="server"></asp:Label> notas de pedido.
                <div id="opciones" runat="server" class="pull-right">
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="padding: 0px 0px; min-height: 0px">
                                Informes <i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li>
                                    <asp:LinkButton ID="btnInformeGlobalProyecto" runat="server" Text="<i class='fa fa-newspaper-o fa-fw'></i>&nbsp;Informe global" OnClick="btnInformeGlobalProyecto_Click" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Muestra informe global del proyecto."></asp:LinkButton>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                
                <div id="informeGlobal" runat="server" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" style="width:95%">
                        <div class="modal-content">
                            <div id="printInformeGlobal">
                                <div class="modal-header">
                                    <h2 class="modal-title">Informe global proyecto: <var id="tituloProyectoInformeNP" runat="server"></var></h2>
                                </div>
                                <div class="modal-body">
                                    <div class="form-horizontal">
                                        <div id="padre" runat="server" class="form-group">
                                            <label class="col-lg-2">Proyecto padre:</label>
                                            <div class="col-lg-4">
                                                <asp:Label ID="lblPadre" runat="server" CssClass="form-control-static"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2">Cliente <small>(nombre fantasía)</small>:</label>
                                            <div class="col-lg-4">
                                                <asp:Label ID="lblCliente" runat="server" CssClass="form-control-static"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2">Fecha de ingreso:</label>
                                            <div class="col-lg-4">
                                                <asp:Label ID="lblFechaIngreso" runat="server" CssClass="form-control-static"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2">Fecha planificada:</label>
                                            <div class="col-lg-4">
                                                <asp:Label ID="lblFechaPlanificada" runat="server" CssClass="form-control-static"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2">Ejecutivo comercial:</label>
                                            <div class="col-lg-4">
                                                <asp:Label ID="lblEjecutivo" runat="server" CssClass="form-control-static"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2">Dirección:</label>
                                            <div class="col-lg-4">
                                                <asp:Label ID="lblDireccion" runat="server" CssClass="form-control-static"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                    
                                    <div class="table-responsive">
                                        <asp:GridView ID="gvNPInformeGlobal" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None" ShowFooter ="true"
                                            OnDataBound="gvNPInformeGlobal_DataBound" OnRowDataBound="gvNPInformeGlobal_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="" HeaderStyle-Width="35px">
                                                    <ItemTemplate>
                                                        <asp:Label id="lblColor" runat="server" Text="&nbsp;&nbsp;"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="NP" HeaderStyle-Width="32px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIDNP" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cant." HeaderStyle-Width="58px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCant" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Uni." HeaderStyle-Width="35px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUnidad" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Descripción">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDesc" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--Columna "Valor Unitario" se maneja mediante roles, su posición es 5--%>
                                                <asp:TemplateField HeaderText="Valor Unitario" HeaderStyle-CssClass="" ItemStyle-CssClass="text-right" HeaderStyle-Width="125px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSiglaTipoValor" runat="server" CssClass="pull-left"></asp:Label>
                                                        <asp:Label ID="lblValorUnitario" runat="server" CssClass="pull-right"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--Columna "Valor Unitario" se maneja mediante roles, su posición es 6--%>
                                                <asp:TemplateField HeaderText="Valor Total" HeaderStyle-Width="145px" HeaderStyle-CssClass="text-right" FooterStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblValorTotal" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblValorProyecto" runat="server" CssClass="form-control-static"></asp:Label>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Valor Total (CLP)" HeaderStyle-Width="145px" HeaderStyle-CssClass="col-lg-1 text-right" FooterStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblValorTotalCLP" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblValorProyectoCLP" runat="server" CssClass="form-control-static"></asp:Label>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="button" id="btnImprimirInformeGlobal" value="Imprimir" class="btn btn-primary" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="resumenNP" runat="server" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" style="width:95%">
                        <div  class="modal-content">
                            <div id="printResumenNP">
                                <div class="modal-header">
                                    <h2 class="modal-title">Resumen NP proyecto: <var id="tituloProyectoResumenNP" runat="server"></var></h2>
                                </div>
                                <div class="modal-body">
                                    <div class="table-responsive">
                                        <p><i class="fa fa-asterisk text-danger" aria-hidden="true"></i><strong><var> Indica que el valor ha cambiado respecto al último estado.</var></strong></p>
                                        <p><var>Valores en negrita indican que tuvieron variaciones en la creación.</var></p>
                                        <h3>Notas de Pedido</h3>

                                        <style type="text/css">
                                            .gvHeader th:first-child {
                                                display: none;
                                            }

                                            .gvRow td:first-child {
                                                display: none;
                                            }

                                            .gvAltRow td:first-child {
                                                display: none;
                                            }
                                        </style>

                                        <asp:GridView ID="gvResumenNP" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered gvRow" HeaderStyle-CssClass="gvHeader" AlternatingRowStyle-CssClass="gvAltRow" GridLines="None" ShowFooter="true"
                                            OnDataBound="gvResumenNP_DataBound" OnRowDataBound="gvResumenNP_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <tr class="gvHeader">
                                                            <th style="width:0px"></th>
                                                            <th style="width:1%;" rowspan="2">NP</th>
                                                            <th style="background-color: #f2dede !important" colspan="5">CREADO</th>
                                                            <th style="background-color: #fcf8e3 !important" colspan="2">AJUSTADO</th>
                                                            <th style="background-color: #dff0d8 !important" colspan="4">VALORIZADO</th>
                                                        </tr>
                                                        <tr class="gvHeader">
                                                            <th></th>
                                                            <th style="width:42px;background-color: #f2dede !important">Cant</th>
                                                            <th style="width:42px;background-color: #f2dede !important">Uni</th>
                                                            <th style="background-color: #f2dede !important">Descripción</th>
                                                            <th style="width:120px;background-color: #f2dede !important">Valor Unitario</th>
                                                            <th style="width:120px;background-color: #f2dede !important">Total</th>

                                                            <th style="width:45px;background-color: #fcf8e3 !important">Cant</th>
                                                            <th style="background-color: #fcf8e3 !important">Descripción</th>

                                                            <th style="width:42px;background-color: #dff0d8 !important">Cant</th>
                                                            <th style="background-color: #dff0d8 !important">Descripción</th>
                                                            <th style="width:120px;background-color: #dff0d8 !important">Valor Unitario</th>
                                                            <th style="width:120px;background-color: #dff0d8 !important">Total</th>
                                                        </tr>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td></td>
                                                            <td><asp:Label ID="lblIDNPCompuesta" runat="server"></asp:Label></td>
                                                            <td><asp:Label ID="lblCantC" runat="server"></asp:Label></td>
                                                            <td><div class="tooltip-wrapper" data-title='<%# Eval("Creacion.TipoUnidad.Nombre") %>'><%# Eval("Creacion.TipoUnidad.Sigla") %></div></td>
                                                            <td><asp:Label ID="lblDescC" runat="server"></asp:Label></td>
                                                            <td><asp:Label ID="lblVUC" runat="server"></asp:Label></td>
                                                            <td><asp:Label ID="lblVTC" runat="server"></asp:Label></td>

                                                            <td><asp:Label ID="lblCantA" runat="server"></asp:Label></td>
                                                            <td><asp:Label ID="lblDescA" runat="server"></asp:Label></td>

                                                            <td><asp:Label ID="lblCantV" runat="server"></asp:Label></td>
                                                            <td><asp:Label ID="lblDescV" runat="server"></asp:Label></td>
                                                            <td><asp:Label ID="lblVUV" runat="server"></asp:Label></td>
                                                            <td><asp:Label ID="lblVTV" runat="server"></asp:Label></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <div id="totalProyecto" runat="server">
                                                            <td class="text-right" colspan="11"><strong>Total Proyecto</strong></td>
                                                            <td><asp:Label ID="lblVTP" runat="server"></asp:Label></td>
                                                        </div>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="button" id="btnImprimirResumenNP" value="Imprimir" class="btn btn-primary" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <div id="trazabilidadNP" runat="server" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" style="width:95%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 id="tituloTrazado" runat="server" class="modal-title"></h2>
                            </div>
                            <div class="modal-body">
                                <div class="table-responsive">
                                    <span><i class="fa fa-asterisk text-danger" aria-hidden="true"></i> <strong><var>Indica que el valor ha cambiado respecto al último estado.</var></strong></span>
                                    <h3>Creación</h3>
                                    <asp:GridView ID="gvTrazaC" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None"
                                        OnDataBound="gvTrazaC_DataBound" OnRowDataBound="gvTrazaC_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Evento" HeaderStyle-Width="65px">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Cant." HeaderStyle-Width="50px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCant" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Uni." HeaderStyle-Width="55px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUnidad" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- Columna Valor Unitario se maneja mediante roles, su posición es 3 --%>
                                            <asp:TemplateField HeaderText="Valor Unitario" HeaderStyle-Width="125px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblValorUnitarioCreacion" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- Columna botón Valor Total se maneja mediante roles, su posición es 4 --%>
                                            <asp:TemplateField HeaderText="Valor Total" HeaderStyle-Width="125px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblValorTotalCreacion" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Descripción" HeaderStyle-CssClass="col-lg-5">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDesc" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Usuario" HeaderStyle-Width="200px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUsuario" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Fecha" HeaderStyle-Width="135px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFecha" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <%-- ajusteTrazabilidad no forma parte de un control restringido --%>
                                    <div id="ajusteTrazabilidad" runat="server" visible="false">
                                        <h3>Ajuste</h3>
                                        <asp:GridView ID="gvTrazaA" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None"
                                            OnDataBound="gvTrazaA_DataBound" OnRowDataBound="gvTrazaA_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Evento" HeaderStyle-Width="65px">
                                                    <ItemTemplate>
                                                        <%# gvTrazaC.Rows.Count+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cantidad" HeaderStyle-Width="50px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCant" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Unidad" HeaderStyle-Width="55px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUnidad" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Valor Unitario" HeaderStyle-Width="125px">
                                                    <ItemTemplate>
                                                        -
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Valor Total" HeaderStyle-Width="125px">
                                                    <ItemTemplate>
                                                        -
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Descripción" HeaderStyle-CssClass="col-lg-5">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDesc" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Usuario" HeaderStyle-Width="200px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUsuario" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Fecha" HeaderStyle-Width="135px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFecha" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <%-- valorizacionTrazabilidad no forma parte de un control restringido --%>
                                    <div id="valorizacionTrazabilidad" runat="server" visible="false"> 
                                        <h3>Valorización</h3>
                                        <asp:GridView ID="gvTrazaV" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None"
                                            OnDataBound="gvTrazaV_DataBound" OnRowDataBound="gvTrazaV_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Evento" HeaderStyle-Width="65px">
                                                    <ItemTemplate>
                                                        <%# gvTrazaC.Rows.Count+2 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cantidad" HeaderStyle-Width="50px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCant" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Unidad" HeaderStyle-Width="55px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUnidad" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- Columna Valor Unitario se maneja mediante roles, su posición es 3 --%>
                                                <asp:TemplateField HeaderText="Valor Unitario" HeaderStyle-Width="125px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblValorUnitarioValorizacion" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- Columna Valor Total se maneja mediante roles, su posición es 4 --%>
                                                <asp:TemplateField HeaderText="Valor Total" HeaderStyle-Width="125px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblValorTotalValorizacion" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Descripción" HeaderStyle-CssClass="col-lg-5">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDesc" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Usuario" HeaderStyle-Width="200px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUsuario" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Fecha" HeaderStyle-Width="135px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFecha" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
                

                <div id="accordion" runat="server" class="panel-group" role="tablist" aria-multiselectable="true">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="form-group">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <ul class="list-inline" style="margin-bottom:0px">
                                            <li><label class="label label-danger">&nbsp;&nbsp;</label> Creada</li>
                                            <li><label class="label label-warning">&nbsp;&nbsp;</label> Ajustada</li>
                                            <li><label class="label label-success">&nbsp;&nbsp;</label> Valorizada</li>
                                            <li><label class="label label-primary">&nbsp;&nbsp;</label> Facturada</li>
                                            <li><i class="fa fa-trash text-danger" style="margin-left: 4px;margin-right: 4px;"></i> NP eliminada</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <style type="text/css">
                        .table-np{
                            width: 100%;
                            max-width: 100%;
                            margin-bottom: 20px;
                        }

                        .table-np>tbody>tr>td, .table-np>tbody>tr>th, .table-np>tfoot>tr>td, .table-np>tfoot>tr>th, .table-np>thead>tr>td, .table-np>thead>tr>th {
                            padding: 5px 0px 5px 0px;
                            line-height: 1.42857143;
                            vertical-align: top;
                            /*border-top: 1px solid #ddd;*/
                        }
                    </style>
                    <asp:GridView ID="gvNP" runat="server" AutoGenerateColumns="false" CssClass="table" GridLines="None" ShowHeader="false"
                        OnDataBound="gvNP_DataBound" OnRowDataBound="gvNP_RowDataBound" OnRowCommand="gvNP_RowCommand">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <div id="rowNP" runat="server" class="panel panel-default">
                                        <div id="heading" runat="server" class="panel-heading" role="tab">
                                            <h3 class="panel-title">
                                                <asp:Label id="lblColor" runat="server" Text="&nbsp;&nbsp;"></asp:Label>
                                                <b>
                                                    <asp:HyperLink ID="tituloNP" runat="server" NavigateUrl="#" data-toggle="collapse" aria-expanded="false"></asp:HyperLink>
                                                </b>
                                                <i class="fa fa-comment" style="margin-left: 10px"></i>&nbsp;&nbsp;<strong><%# string.Format("{00,0}",Eval("Creacion.Cantidad")) %></strong></i>&nbsp;&nbsp;<span id="InicioDescripcion" runat="server" class="textoc"><%# Eval("Creacion.Descripcion") %></span>
                                                <div id="opcion" runat="server" class="pull-right">
                                                    <strong><%# Eval("NotaPedido.Nombre") %></strong>&nbsp;&nbsp;
                                                    <ul class="nav navbar-top-links navbar-right">
                                                        <li>
                                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="padding: 0px 0px; min-height: 0px">
                                                                <i class="fa fa-cogs fa-fw"></i><i class="fa fa-caret-down"></i>
                                                            </a>
                                                            <ul class="dropdown-menu dropdown-user">
                                                                <li>
                                                                    <asp:LinkButton ID="btnTrazaNP" runat="server" Text="<i class='fa fa-list-alt fa-fw'></i>&nbsp;Trazabilidad" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Muestra la trazabilidad de la nota de pedido (historial)" CommandName="Trazabilidad" CommandArgument='<%# Container.DataItemIndex %>'></asp:LinkButton>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </h3>
                                        </div>
                                        <div id="np" runat="server" class="panel-collapse collapse" role="tabpanel">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-4" style="margin-bottom:20px">
                                                        <div class="panel panel-danger">
                                                            <div class="panel-heading">
                                                                Creación <i id="asteriscoC" runat="server" class="fa fa-asterisk text-danger" aria-hidden="true" visible="false"></i>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label>Cantidad</label>
                                                                            <div class="input-group input-group-sm">
                                                                                <asp:TextBox ID="txtCantidadC" runat="server" CssClass="form-control input-sm" Enabled="false" MaxLength="5" autocomplete="off"></asp:TextBox>
                                                                                <span class="input-group-addon">
                                                                                    <div class="tooltip-wrapper" data-title='<%# Eval("Creacion.TipoUnidad.Nombre") %>'>
                                                                                        <%# Eval("Creacion.TipoUnidad.Sigla") %>
                                                                                    </div>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label>Usuario</label>
                                                                            <asp:TextBox ID="txtUsuarioC" runat="server" Text='<%# string.Format("{0} {1}", Eval("Creacion.Usuario.Nombre"), Eval("Creacion.Usuario.ApellidoPaterno")) %>' CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="form-group">
                                                                            <label>Descripción</label>
                                                                            <asp:TextBox ID="txtDescripcionC" runat="server" Text='<%# Eval("Creacion.Descripcion") %>' CssClass="form-control input-sm" Enabled="false" TextMode="MultiLine" Rows="4" MaxLength="500"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label>Fecha Creación</label>
                                                                            <div class="tooltip-wrapper" data-title='<%# Eval("Creacion.Fecha") %>'>
                                                                                <asp:TextBox ID="txtFechaC" runat="server" Text='<%# string.Format("{0:dd/MM/yyyy}", Eval("Creacion.Fecha")) %>' CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="ultimaModificacionC" runat="server" class="col-lg-6" visible="false">
                                                                        <div class="form-group">
                                                                            <label>Última mod.</label>
                                                                            <div class="tooltip-wrapper" data-title='<%# Eval("Creacion.FechaUltimaModificacion") %>'>
                                                                                <asp:TextBox ID="txtFechaUltimaC" runat="server" Text='<%# string.Format("{0:dd/MM/yyyy}", Eval("Creacion.FechaUltimaModificacion")) %>' CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="valoresCreacion" runat="server" class="panel-footer">
                                                                <div class="form-group">
                                                                    <label>Valor unitario</label>
                                                                    <div class="input-group">
                                                                        <div class="input-group-btn">
                                                                            <asp:DropDownList id="ddlTipoValor1" runat="server" CssClass="selectpicker" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <asp:TextBox ID="txtVUC" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Valor total</label>
                                                                    <div class="input-group">
                                                                        <div class="input-group-btn">
                                                                            <asp:DropDownList id="ddlTipoValor2" runat="server" CssClass="selectpicker" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <asp:TextBox ID="txtVTC" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Valor proyecto</label>
                                                                    <div class="input-group">
                                                                        <div class="input-group-btn">
                                                                            <asp:DropDownList id="ddlTipoValor3" runat="server" CssClass="selectpicker" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <asp:TextBox ID="txtVPC" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4" style="margin-bottom:20px">
                                                        <div id="ajuste" runat="server" class="panel panel-warning">
                                                            <div class="panel-heading">
                                                                Ajuste <i id="asteriscoA" runat="server" class="fa fa-asterisk text-danger" aria-hidden="true" visible="false"></i>
                                                            </div>
                                                            <div id="bodyAjuste" class="panel-body">  
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label>* Cantidad</label> <i id="asteriscoAC" runat="server" class="fa fa-asterisk text-danger" aria-hidden="true" visible="false"></i>
                                                                            <div class="input-group input-group-sm">
                                                                                <asp:TextBox ID="txtCantidadA" runat="server" CssClass="form-control input-sm" Enabled="false" MaxLength="6" autocomplete="off" ValidationGroup='<%# "Ajuste" + Container.DataItemIndex %>'></asp:TextBox>
                                                                                <span class="input-group-addon">
                                                                                    <div class="tooltip-wrapper" data-title='<%# Eval("Creacion.TipoUnidad.Nombre") %>'>
                                                                                        <%# Eval("Creacion.TipoUnidad.Sigla") %>
                                                                                    </div>
                                                                                </span>
                                                                            </div>
                                                                            <asp:RequiredFieldValidator ID="rfvCantidadA" runat="server" ErrorMessage="Obligatorio..." ControlToValidate="txtCantidadA" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup='<%# "Ajuste" + Container.DataItemIndex %>' />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <div id="usuarioA" runat="server" class="form-group" visible="false">
                                                                            <label>Usuario</label><br />
                                                                            <asp:TextBox ID="txtUsuarioA" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="form-group">
                                                                            <label>* Descripción</label> <i id="asteriscoAD" runat="server" class="fa fa-asterisk text-danger" aria-hidden="true" visible="false"></i>
                                                                            <asp:TextBox ID="txtDescripcionA" runat="server" CssClass="form-control input-sm" Enabled="false" TextMode="MultiLine" Rows="4" MaxLength="500" ValidationGroup='<%# "Ajuste" + Container.DataItemIndex %>'></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="rfvDescripcionA" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtDescripcionA" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" ValidationGroup='<%# "Ajuste" + Container.DataItemIndex %>' />
                                                                            <asp:Label ID="lblContDescA" runat="server" CssClass="help-block pull-right"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                </div> 
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="form-group">
                                                                            <label>* Fecha de entrega real</label>
                                                                            <div id="dFechaEntregaA" runat="server" class="input-group date input-group">
                                                                                <asp:TextBox ID="txtFechaRealEntregaA" runat="server" CssClass="form-control input-sm" Enabled="false" autocomplete="off" ValidationGroup='<%# "Ajuste" + Container.DataItemIndex %>'></asp:TextBox>
                                                                                <span id="spanUnoA" runat="server" class="input-group-addon">
                                                                                    <span id="spanDosA" runat="server" class="fa fa-calendar"></span>
                                                                                </span>
                                                                            </div>
                                                                            <asp:RequiredFieldValidator ID="rfvFechaRealEntregaA" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtFechaRealEntregaA" SetFocusOnError="true" CssClass="alert-danger" ValidationGroup='<%# "Ajuste" + Container.DataItemIndex %>' />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4" style="margin-bottom:20px">
                                                        <div id="contenidoValorizacion" runat="server" class="panel panel-success">
                                                            <div class="panel-heading">
                                                                Valorización <i id="asteriscoV" runat="server" class="fa fa-asterisk text-danger" aria-hidden="true" visible="false"></i>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label>* Cantidad</label> <i id="asteriscoVC" runat="server" class="fa fa-asterisk text-danger" aria-hidden="true" visible="false"></i>
                                                                            <div class="input-group input-group-sm">
                                                                                <asp:TextBox ID="txtCantidadV" runat="server" CssClass="form-control input-sm" Enabled="false" MaxLength="6" autocomplete="off" ValidationGroup='<%# "Valorizacion" + Container.DataItemIndex %>'></asp:TextBox>
                                                                                <span class="input-group-addon">
                                                                                    <div class="tooltip-wrapper" data-title='<%# Eval("Creacion.TipoUnidad.Nombre") %>'>
                                                                                        <%# Eval("Creacion.TipoUnidad.Sigla") %>
                                                                                    </div>
                                                                                </span>
                                                                            </div>
                                                                            <asp:RequiredFieldValidator ID="rfvCantidadV" runat="server" ErrorMessage="Obligatorio..." ControlToValidate="txtCantidadV" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup='<%# "Valorizacion" + Container.DataItemIndex %>' />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <div id="usuarioV" runat="server" class="form-group" visible="false">
                                                                            <label>Usuario</label>
                                                                            <asp:TextBox ID="txtUsuarioV" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="form-group">
                                                                            <label>* Descripción</label> <i id="asteriscoVD" runat="server" class="fa fa-asterisk text-danger" aria-hidden="true" visible="false"></i>
                                                                            <asp:TextBox ID="txtDescripcionV" runat="server" CssClass="form-control input-sm" Enabled="false" TextMode="MultiLine" Rows="4" MaxLength="500" autocomplete="off" ValidationGroup='<%# "Valorizacion" + Container.DataItemIndex %>'></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="rfvDescripcionV" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtDescripcionV" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup='<%# "Valorizacion" + Container.DataItemIndex %>' />
                                                                            <asp:Label ID="lblContDescV" runat="server" CssClass="help-block pull-right"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div id="fechaV" runat="server" class="form-group">
                                                                            <label>Fecha Valorización</label>
                                                                            <div class="tooltip-wrapper" data-title='<%# Eval("Valorizacion.Fecha") %>'>
                                                                                <asp:TextBox ID="txtFechaV" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="valoresValorizacion" runat="server" class="panel-footer">
                                                                <div class="form-group">
                                                                    <label>Valor unitario</label> <i id="asteriscoVU" runat="server" class="fa fa-asterisk text-danger" aria-hidden="true" visible="false"></i>
                                                                    <div class="input-group">
                                                                        <div class="input-group-btn">
                                                                            <asp:DropDownList id="ddlTipoValor4" runat="server" CssClass="selectpicker" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <asp:TextBox ID="txtVUV" runat="server" CssClass="form-control input-sm" Enabled="false" autocomplete="off" ValidationGroup='<%# "Valorizacion" + Container.DataItemIndex %>' ></asp:TextBox>
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="rfvVUV" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtVUV" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup='<%# "Valorizacion" + Container.DataItemIndex %>' />
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Valor total</label>
                                                                    <div class="input-group">
                                                                        <div class="input-group-btn">
                                                                            <asp:DropDownList id="ddlTipoValor5" runat="server" CssClass="selectpicker" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <asp:TextBox ID="txtVTV" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Valor proyecto</label>
                                                                    <div class="input-group">
                                                                        <div class="input-group-btn">
                                                                            <asp:DropDownList id="ddlTipoValor6" runat="server" CssClass="selectpicker" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <asp:TextBox ID="txtVPV" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
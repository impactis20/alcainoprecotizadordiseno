﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MakeUp.BL;


namespace MakeUp.App.UI.makeup2.proyecto.eliminado
{
    public partial class proyecto : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int IDProyecto = Convert.ToInt16(Request.QueryString["id"].ToString());

            if (!IsPostBack)
            {
                CargaInicial(IDProyecto);
            }

            EstableceActiveTab(IDProyecto);
        }

        private void CargaInicial(int IDProyecto)
        {
            Entities.ProyectoEntity Proyecto = ProyectoBL.GetCabeceraProyectoEliminado(IDProyecto);

            if (Proyecto != null)
            {
                string Titulo = string.Format("{0} - {1}", Proyecto.ID.ToString(), Proyecto.Nombre);
                Page.Title = Titulo;
                titulo.InnerText = Titulo;
            }
            else
            {
                titulo.InnerHtml = "No existe... <i class=\"fa fa-frown-o\" aria-hidden=\"true\"></i>";
                opciones.Visible = false;
                msjGeneral.Visible = false;
                tabs.Visible = false;
                contenido.Visible = false;
            }
        }

        private void EstableceActiveTab(int IDProyecto)
        {
            for (int x = 0; x < tabs.Controls.Count; x++)
            {
                if (x % 2 == 1)
                {
                    string Url = Request.Url.AbsolutePath;
                    HyperLink link = (HyperLink)tabs.Controls[x];
                    link.NavigateUrl = string.Format("{0}={1}", link.NavigateUrl.Split('=')[0], IDProyecto);

                    LiteralControl li = tabs.Controls[x - 1] as LiteralControl;

                    if (Url.Contains(link.NavigateUrl.Replace("~", string.Empty).Split('?')[0]))
                        li.Text = li.Text.Replace("<li>", "<li class='active'>");
                    else
                        li.Text = li.Text.Replace("<li class='active'>", "<li>");
                }
            }
        }

        protected void btnInformeGlobalProyecto_Click(object sender, EventArgs e)
        {
            int IDProyecto = Convert.ToInt16(Request.QueryString["id"].ToString());

            Entities.ProyectoEntity Proyecto = ProyectoBL.GetCabeceraProyectoEliminado(IDProyecto);
            tituloProyectoResumenNP.InnerText = string.Format("{0} - {1}", Proyecto.ID.ToString(), Proyecto.Nombre);

            if (Proyecto.Padre != null)
            {
                padre.Visible = true;
                lblPadre.Text = string.Format("{0} - {1}", Proyecto.Padre.ID, Proyecto.Padre.Nombre);
            }
            else
                padre.Visible = false;

            lblCliente.Text = Proyecto.Cliente.NombreFantasia;
            lblFechaIngreso.Text = Proyecto.FechaIngreso.ToShortDateString();
            lblFechaPlanificada.Text = Proyecto.FechaPlanificada.ToShortDateString();
            lblEjecutivo.Text = string.Format("{0} {1} {2} <var>({3})</var>", Proyecto.EjecutivoComercial.Nombre, Proyecto.EjecutivoComercial.ApellidoPaterno, Proyecto.EjecutivoComercial.ApellidoMaterno, Proyecto.EjecutivoComercial.Email);
            lblDireccion.Text = string.Format("{0}, {1}", Proyecto.Direccion, Proyecto.Comuna);
            ViewState.Add("TotalProyecto", string.Format("{0} {1}", Proyecto.TipoValor.Sigla, Util.Funcion.FormatoNumerico(Proyecto.ValorTotal)));

            if (Proyecto.TipoValor.Valor > 1)
            {
                ViewState.Add("ValorTipoValor", Proyecto.TipoValor.Valor);
                ViewState.Add("TotalProyectoCLP", string.Format("CLP {0}", Util.Funcion.FormatoNumerico(Math.Round(Proyecto.ValorTotal * Proyecto.TipoValor.Valor, 0))));
            }
            else
                gvNPInformeGlobal.Columns[7].Visible = false;

            BindDataNP(IDProyecto);

            ScriptManager.RegisterStartupScript(this, GetType(), "ResumenNP", "$('#" + informeGlobal.ClientID + "').modal({backdrop: 'static',keyboard: true});", true);
        }

        private void BindDataNP(int IDProyecto)
        {
            try
            {
                List<Entities.NotaPedidoEntity> Lst = NotaPedidoBL.GetNP(IDProyecto);
                gvNPInformeGlobal.DataKeyNames = new string[1] { "ID" }; //PK
                gvNPInformeGlobal.DataSource = Lst;
                gvNPInformeGlobal.DataBind();

                if (Lst.Count > 0)
                {
                    gvNPInformeGlobal.UseAccessibleHeader = true;
                    gvNPInformeGlobal.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        protected void gvNPInformeGlobal_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.NotaPedidoEntity NP = (Entities.NotaPedidoEntity)r.DataItem;

                Label lblColor = (r.Controls[0].FindControl("lblColor") as Label);
                Label lblIDNP = (r.Controls[0].FindControl("lblIDNP") as Label);
                Label lblCant = (r.Controls[0].FindControl("lblCant") as Label);
                Label lblUnidad = (r.Controls[0].FindControl("lblUnidad") as Label);
                Label lblDesc = (r.Controls[0].FindControl("lblDesc") as Label);
                Label lblValorUnitario = (r.Controls[0].FindControl("lblValorUnitario") as Label);
                Label lblValorTotal = (r.Controls[0].FindControl("lblValorTotal") as Label);

                switch (NP.IDEstadoNP)
                {
                    case 1:
                        lblColor.CssClass = "label";
                        lblColor.Attributes.Add("style", "background-color:#d9534f !important");
                        SetRowValues(r, NP.IDCompuesta, NP.Creacion.Cantidad, NP.Creacion.TipoUnidad.Sigla, NP.Creacion.Descripcion, NP.Creacion.ValorUnitario);
                        break;
                    case 2:
                        lblColor.CssClass = "label";
                        lblColor.Attributes.Add("style", "background-color:#f0ad4e !important");
                        SetRowValues(r, NP.IDCompuesta, NP.Creacion.Cantidad, NP.Creacion.TipoUnidad.Sigla, NP.Ajuste.Descripcion, NP.Creacion.ValorUnitario);
                        break;
                    case 3:
                        lblColor.CssClass = "label";
                        lblColor.Attributes.Add("style", "background-color:#5cb85c !important");
                        SetRowValues(r, NP.IDCompuesta, NP.Valorizacion.Cantidad, NP.Creacion.TipoUnidad.Sigla, NP.Valorizacion.Descripcion, NP.Valorizacion.ValorUnitario);
                        break;
                    case 4:
                        lblColor.CssClass = "label";
                        lblColor.Attributes.Add("style", "background-color:#337ab7 !important");
                        SetRowValues(r, NP.IDCompuesta, NP.Valorizacion.Cantidad, NP.Creacion.TipoUnidad.Sigla, NP.Valorizacion.Descripcion, NP.Valorizacion.ValorUnitario);
                        break;
                }
            }

            if (r.RowType == DataControlRowType.Footer)
            {
                Label lblValorProyecto = (r.Controls[0].FindControl("lblValorProyecto") as Label);
                lblValorProyecto.Text = string.Format("<strong>{0}</strong>", ViewState["TotalProyecto"].ToString());

                if (ViewState["TotalProyectoCLP"] != null)
                {
                    Label lblValorProyectoCLP = (r.Controls[0].FindControl("lblValorProyectoCLP") as Label);
                    lblValorProyectoCLP.Text = string.Format("<strong>{0}</strong>", ViewState["TotalProyectoCLP"].ToString());
                }
            }
        }

        private void SetRowValues(GridViewRow r, string IDCompuesta, decimal Cantidad, string TipoUnidad, string Descripcion, decimal ValorUnitario)
        {
            Label lblColor = (r.Controls[0].FindControl("lblColor") as Label);
            Label lblIDNP = (r.Controls[0].FindControl("lblIDNP") as Label);
            Label lblCant = (r.Controls[0].FindControl("lblCant") as Label);
            Label lblUnidad = (r.Controls[0].FindControl("lblUnidad") as Label);
            Label lblDesc = (r.Controls[0].FindControl("lblDesc") as Label);
            Label lblValorUnitario = (r.Controls[0].FindControl("lblValorUnitario") as Label);
            Label lblValorTotal = (r.Controls[0].FindControl("lblValorTotal") as Label);
            Label lblValorTotalCLP = (r.Controls[0].FindControl("lblValorTotalCLP") as Label);

            string TipoValor = ViewState["TotalProyecto"].ToString().Split(' ')[0];
            decimal ValorTotal = ValorUnitario * Cantidad;

            lblIDNP.Text = IDCompuesta.Split('-')[1];
            lblCant.Text = Util.Funcion.FormatoNumerico(Cantidad);
            lblUnidad.Text = TipoUnidad;
            lblDesc.Text = Descripcion;
            lblValorUnitario.Text = string.Format("{0} {1}", TipoValor, Util.Funcion.FormatoNumerico(ValorUnitario));
            lblValorTotal.Text = string.Format("{0} {1}", TipoValor, Util.Funcion.FormatoNumerico(ValorTotal));

            if (ViewState["ValorTipoValor"] != null)
                lblValorTotalCLP.Text = string.Format("CLP {0}", Util.Funcion.FormatoNumerico(Math.Round(ValorTotal * Convert.ToDecimal(ViewState["ValorTipoValor"]), 0)));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.proyecto.eliminado
{
    public partial class datos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    CargaInicial(Convert.ToInt16(Request.QueryString["id"].ToString()));
                }
                else
                {
                    Page.Title = "Proyecto no encontrado...";
                }
            }
        }

        private void CargaInicial(int IDProyecto)
        {
            Entities.ProyectoEntity Proyecto = ProyectoBL.GetCabeceraProyectoEliminado(IDProyecto);

            if (Proyecto != null)
            {
                txtIDProyecto.Text = Proyecto.ID.ToString();
                hfIdProyecto.Value = Proyecto.ID.ToString(); //almacena Id de proyecto

                if (Proyecto.Padre != null)
                {
                    txtPadre.Text = Proyecto.Padre.Nombre;
                    hfPadre.Value = Proyecto.Padre.ID.ToString();
                }

                txtNombre.Text = Proyecto.Nombre;
                txtCliente.Text = Proyecto.Cliente.NombreFantasia;
                txtFechaIngreso.Text = Proyecto.FechaIngreso.ToShortDateString();
                txtFechaPlanificada.Text = Proyecto.FechaPlanificada.ToShortDateString();
                txtFechaComprometida.Text = Proyecto.FechaComprometida.ToShortDateString();
                txtEjecutivo.Text = string.Format("{0} {1} {2}", Proyecto.EjecutivoComercial.Nombre, Proyecto.EjecutivoComercial.ApellidoPaterno, Proyecto.EjecutivoComercial.ApellidoMaterno);
                hfEjecutivo.Value = Proyecto.EjecutivoComercial.ID.ToString();
                txtDireccion.Text = Proyecto.Direccion;
                txtComuna.Text = Proyecto.Comuna;
                txtmotivo.Text = Proyecto.Motivo;
                //lblTipoMoneda.Text = Proyecto.TipoValor.Sigla;
                lblDirectorioDC.Text = string.Format("<b>{0}</b>", Proyecto.Directorio);

                //bind's
                BindDataDocComercial(IDProyecto);
                BindDataDocProyecto(IDProyecto);

                //carga total proyecto
                double ValorTotalProyecto = ProyectoBL.GetValorTotalProyecto(IDProyecto); //almacena valor total proyecto para realizar cálculo 

                //Agrega tipo de valor del proyecto
                ddlTipoValor.Items.Add(Proyecto.TipoValor.Sigla); //bloqueado por requerimiento
                visualizar.notas_de_pedido.AddFunctionValorTotalProyectoCLP(ref ddlTipoValor1, Proyecto.TipoValor.Sigla, Proyecto.TipoValor.Valor); //valor total proyecto (datos)

                if (Proyecto.TipoValor.Valor > 1)
                {
                    txtValorProyecto.Text = Util.Funcion.FormatoNumerico(ValorTotalProyecto);

                    //Agrega traductor de tipo de cambio
                    ddlTipoValor1.Attributes.Add("txt", txtValorProyecto.ClientID);
                    ddlTipoValor1.Attributes.Add("valor", ValorTotalProyecto.ToString());
                }
                else
                    txtValorProyecto.Text = Util.Funcion.FormatoNumericoEntero(ValorTotalProyecto);
            }
        }

        protected void gvDocComercial_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                //cuando edita debe cargar el desplegable tipo documento...
                if ((r.RowState & DataControlRowState.Edit) == DataControlRowState.Edit)
                {
                    //asigna contador de caracteres
                    (r.Controls[0].FindControl("txtDesc") as TextBox).Attributes.Add("onkeyup", "countChar(this, '" + (r.Controls[0].FindControl("lblContDesc") as Label).ClientID + "', 30)");

                    DropDownList ddlTipoDoc = (r.Controls[0].FindControl("ddlTipoDocumento") as DropDownList);
                    ddlTipoDoc.DataSource = ProyectoBL.GetTipoDocumento();
                    ddlTipoDoc.SelectedValue = ((Entities.DocumentoComercialEntity)r.DataItem).TipoDocumento.ID.ToString();
                    ddlTipoDoc.DataBind();
                }
            }
        }

        private void BindDataDocComercial(int IDProyecto)
        {
            try
            {
                List<Entities.DocumentoComercialEntity> Lst = ProyectoBL.GetDocumentoComercial(IDProyecto);
                gvDocComercial.DataKeyNames = new string[1] { "ID" }; //PK
                gvDocComercial.DataSource = Lst;
                gvDocComercial.DataBind();

                if (Lst.Count > 0)
                {
                    gvDocComercial.UseAccessibleHeader = true;
                    gvDocComercial.HeaderRow.TableSection = TableRowSection.TableHeader;

                    lblRegistroDC.Text = string.Format("<b>{0}</b> documentos en el directorio del proyecto ", Lst.Count);
                    lblRegistroDC.CssClass = "alert-info";
                }
                else
                {
                    lblRegistroDC.Text = "No hay documentos comerciales para este proyecto en ";
                    lblRegistroDC.CssClass = "alert-danger";
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                return;
            }
        }

        private void BindDataDocProyecto(int IDProyecto)
        {
            try
            {
                List<Entities.DocumentoProyectoEntity> Lst = ProyectoBL.GetDocumentoProyecto(IDProyecto);
                gvDocProyecto.DataKeyNames = new string[1] { "ID" };
                gvDocProyecto.DataSource = Lst;
                gvDocProyecto.DataBind();

                if (Lst.Count > 0)
                {
                    gvDocProyecto.UseAccessibleHeader = true;
                    gvDocProyecto.HeaderRow.TableSection = TableRowSection.TableHeader;

                    lblRegistroDP.Text = string.Format("<b>{0}</b> documentos en el directorio del proyecto ", Lst.Count);
                    lblRegistroDP.CssClass = "alert-info";
                }
                else
                {
                    lblRegistroDP.Text = "No hay archivos o directorios compartidos para este proyecto";
                    lblRegistroDP.CssClass = "alert-danger";
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                return;
            }
        }
    }
}
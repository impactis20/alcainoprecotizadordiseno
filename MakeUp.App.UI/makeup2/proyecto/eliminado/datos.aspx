﻿

<%@ Page Title="Datos de proyecto (eliminado)" Language="C#" MasterPageFile="~/makeup2/proyecto/eliminado/proyecto.master" AutoEventWireup="true" CodeBehind="datos.aspx.cs" Inherits="MakeUp.App.UI.makeup2.proyecto.eliminado.datos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headProyecto" runat="server">
    <link href="../../../css/bootstrap-datetimepicker.css" rel="stylesheet" />
    <link href="../../../css/bootstrap-select.css" rel="stylesheet" />
    <link href="../../../css/fileinput.min.css" rel="stylesheet" />
    <link href="../../../css/bootstrap-typeahead.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenidoProyecto" runat="server">
    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-datetimepicker.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/fileinput.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/autocomplete-query.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap3-typeahead.min.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/languages/es.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/count-character-textbox.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/dropdown-igualitario.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/number-format-using-numeral.js") %>'></script>

    <script type="text/javascript">
        $(function () {
            $('[class*=calendario]').datetimepicker({
                format: 'DD-MM-YYYY',
                showClear: true,
                ignoreReadonly: true
            });
        });
    </script>

    <asp:HiddenField ID="hfIdProyecto" runat="server" />

    <div class="form-group">
        <p>Los campos con asterisco (*) son obligatorios.</p>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>ID Proyecto</label>
                    <asp:TextBox ID="txtIDProyecto" runat="server" data-toggle="tooltip" data-placement="top" title="Habilita formulario para editar datos de cabecera del proyecto" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                </div>
            </div>
            <div class="col-lg-6">
                <div id="proyectoPadre" runat="server" class="form-group">
                    <label>Proyecto padre</label>
                    <asp:TextBox ID="txtPadre" runat="server" CssClass="form-control input-sm" Enabled="false" placeholder="Ingrese ID del proyecto padre..." autocomplete="off"></asp:TextBox>
                    <asp:Label ID="lblPadre" runat="server" CssClass="alert-danger" Text="Seleccione un <b>Proyecto</b> de la lista..." Visible="false"></asp:Label>
                    <asp:HiddenField ID="hfPadre" runat="server" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div id="nombreProyecto" runat="server" class="form-group">
                    <label>* Nombre proyecto</label>
                    <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvNombre" runat="server" ErrorMessage="Campo <b>Nombre proyecto</b> es obligatorio..." ControlToValidate="txtNombre" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Editar" />
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label>* Cliente <small>(nombre fantasía)</small></label>
                    <asp:TextBox ID="txtCliente" runat="server" CssClass="form-control input-sm" Enabled="false" autocomplete="off"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Fecha de ingreso</label>
                    <asp:TextBox ID="txtFechaIngreso" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div id="fechaComprometida" runat="server" class="col-lg-6">
                        <div class="form-group">
                            <label>* Fecha comprometida <small>(Comercial)</small></label>
                            <div class="input-group date input-group calendario">
                                <asp:TextBox ID="txtFechaComprometida" runat="server" CssClass="form-control input-sm disabled" Enabled="false"></asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            </div>
                            <asp:RequiredFieldValidator ID="rfvFechaComprometida" runat="server" ErrorMessage="Campo <b>Fecha Comprometida</b> es obligatorio..." ControlToValidate="txtFechaComprometida" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Editar" />
                        </div>
                    </div>
                    <div id="fechaPlanificada" runat="server" class="col-lg-6">
                        <div class="form-group">
                            <label>Fecha planificada <small>(Producción)</small></label>
                            <div class="input-group date input-group calendario">
                                <asp:TextBox ID="txtFechaPlanificada" runat="server" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="ejecutivoComercial" runat="server" class="col-lg-6">
                <div class="form-group">
                    <label>* Ejecutivo comercial</label>
                    <asp:TextBox ID="txtEjecutivo" runat="server" CssClass="form-control input-sm" Enabled="false" autocomplete="off"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEjecutivo" runat="server" ErrorMessage="Seleccione un <b>Ejecutivo comercial</b> de la lista..." ControlToValidate="txtEjecutivo" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Editar" />
                    <asp:HiddenField ID="hfEjecutivo" runat="server" />
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label>Divisa o tipo de valor</label>
                    <asp:DropDownList ID="ddlTipoValor" runat="server" CssClass="selectpicker" Enabled="false" DataValueField="ID" DataTextField="sigla" data-width="100%"></asp:DropDownList>
                </div>
            </div>
            <div id="valorProyecto" runat="server" class="col-lg-3">
                <div class="form-group">
                    <label>Valor proyecto</label>
                    <div class="input-group">
                        <div class="input-group-btn">
                            <asp:DropDownList id="ddlTipoValor1" runat="server" CssClass="selectpicker dtv1" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                            </asp:DropDownList>
                        </div>
                        <asp:TextBox ID="txtValorProyecto" runat="server" Text="0" CssClass="form-control input-sm" Enabled="false"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div id="direccion" runat="server" class="form-group">
                    <label>* Dirección del proyecto</label>
                    <asp:TextBox ID="txtDireccion" runat="server" CssClass="form-control input-sm" Enabled="false" MaxLength="200"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvDireccion" runat="server" ErrorMessage="Campo <b>Dirección</b> es obligatorio..." ControlToValidate="txtDireccion" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Editar" />
                </div>
            </div>
            <div id="comuna" runat="server" class="col-lg-3">
                <div class="form-group">
                    <label>* Comuna</label>
                    <asp:TextBox ID="txtComuna" runat="server" CssClass="form-control input-sm" Enabled="false" MaxLength="100" autocomplete="off"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvComuna" runat="server" ErrorMessage="Campo <b>Comuna</b> es obligatorio..." ControlToValidate="txtComuna" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Editar" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div id="Div1" runat="server" class="form-group">
                    <label>* Motivo de Eliminación</label>
                    <asp:TextBox ID="txtmotivo" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control input-sm" Enabled="false" MaxLength="200"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>

    <div id="DocComercial" runat="server" class="panel panel-default">
        <div class="panel-heading">
            <b class="text-uppercase">* Adjuntar documentos comerciales (Obligatorio)</b>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <div class="col-lg-12">
                    <div class="row">
                        <asp:Label ID="lblRegistroDC" runat="server"></asp:Label><asp:Label ID="lblDirectorioDC" runat="server" class="alert-info"></asp:Label>
                        <div class="table-responsive">
                            <asp:GridView ID="gvDocComercial" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None"
                                OnRowDataBound="gvDocComercial_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Tipo archivo" ItemStyle-CssClass="col-lg-2">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTipoArchivo" runat="server" Text='<%# Eval("TipoDocumento.Nombre") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlTipoDocumento" runat="server" CssClass="selectpicker" DataValueField="ID" DataTextField="Nombre" data-width="100%"></asp:DropDownList>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Descripción" ItemStyle-CssClass="col-lg-4">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDesc" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtDesc" runat="server" Text='<%# Eval("Descripcion") %>' CssClass="form-control input-sm" TextMode="MultiLine" Rows="1" MaxLength="30" placeholder="Ingrese descripción del documento..."></asp:TextBox>
                                            <asp:Label ID="lblContDesc" runat="server" CssClass="help-block pull-right"></asp:Label>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Archivo" ItemStyle-CssClass="col-lg-4">
                                        <ItemTemplate>
                                            <asp:Label ID="lblArchivo" runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="DocProyecto" runat="server" class="panel panel-default">
        <div class="panel-heading">
            <b class="text-uppercase">Acceso a documentos de proyecto</b>
        </div>
        <div class="panel-body">
            <div class="col-lg-12">
                <div class="row">
                    <asp:Label ID="lblRegistroDP" runat="server"></asp:Label>
                    <div class="table-responsive">
                        <asp:GridView ID="gvDocProyecto" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None">
                            <Columns>
                                <asp:TemplateField HeaderText="Ruta">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRuta" runat="server" Text='<%# Eval("Url") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Descripción">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDesc" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-CssClass="col-lg-2 right">
                                    <ItemTemplate>
                                        <div class="pull-right">
                                            <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" CssClass="btn btn-danger btn-xs" CommandName="Delete" OnClientClick='<%# string.Format("return confirm(\"¿Está seguro que desea eliminar la ruta a documentos {0}?\");", Eval("Url")) %>' />
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
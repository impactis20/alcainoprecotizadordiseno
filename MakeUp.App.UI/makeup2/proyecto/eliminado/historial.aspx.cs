﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.proyecto.eliminado
{
    public partial class historial : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    CargaInicial(Convert.ToInt16(Request.QueryString["id"].ToString()));
                }
                else
                {
                    Page.Title = "Proyecto no encontrado...";
                }
            }
        }

        private void CargaInicial(int IDProyecto)
        {
            hfIdProyecto.Value = IDProyecto.ToString();

            List<Entities.HistorialProyectoEntity> Lst = ProyectoBL.GetHistorial(IDProyecto, ddlHistorial.SelectedItem.Value);
            gvHistorial.DataSource = Lst;
            gvHistorial.DataBind();

            if (Lst.Count > 0)
            {
                gvHistorial.UseAccessibleHeader = true;
                gvHistorial.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void ddlHistorial_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataBindHistorial(Convert.ToInt16(hfIdProyecto.Value));
        }

        protected void gvHistorial_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.HistorialProyectoEntity HP = (Entities.HistorialProyectoEntity)r.DataItem;

                (r.Controls[0].FindControl("lblEvento") as Label).Text = string.Format("<i class='fa {0}'></i>&nbsp;&nbsp;<b>{1} {2}</b> {3}.", HP.Icono, HP.Usuario.Nombre, HP.Usuario.ApellidoPaterno, HP.Descripcion);
                (r.Controls[0].FindControl("lblFecha") as Label).Text = string.Format("{0:dd-MM-yyyy H:mm}", HP.Fecha);
            }
        }

        private void DataBindHistorial(int IDProyecto)
        {
            List<Entities.HistorialProyectoEntity> Lst = ProyectoBL.GetHistorial(IDProyecto, ddlHistorial.SelectedItem.Value);
            gvHistorial.DataSource = Lst;
            gvHistorial.DataBind();

            if (Lst.Count > 0)
            {
                gvHistorial.UseAccessibleHeader = true;
                gvHistorial.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
    }
}
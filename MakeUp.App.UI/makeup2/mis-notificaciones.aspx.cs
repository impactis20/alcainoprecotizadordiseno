﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2
{
    public partial class mis_notificaciones : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindData(ddlOrden.SelectedItem.Value);
            }
        }

        private void BindData(string orden)
        {
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
            int NotificacionSinVer = 0;
            List<Entities.NotificacionAppEntity> Lst = NotificacionBL.GetNotificacionesAppUsuario(IDUsuario, 9999999, ref NotificacionSinVer);

            gvNotificacion.DataKeyNames = new string[1] { "ID" };

            if (orden == "asc")
            {
                if(cbLeidas.Checked)
                    gvNotificacion.DataSource = Lst.OrderBy(x => x.FechaEvento).Where(z => z.Ingreso == !cbLeidas.Checked);
                else
                    gvNotificacion.DataSource = Lst.OrderBy(x => x.FechaEvento);
            }
            else
            {
                if (cbLeidas.Checked)
                    gvNotificacion.DataSource = Lst.OrderByDescending(x => x.FechaEvento).Where(z => z.Ingreso == !cbLeidas.Checked);
                else
                    gvNotificacion.DataSource = Lst.OrderByDescending(x => x.FechaEvento);
            }

            gvNotificacion.DataBind();

            if (gvNotificacion.Rows.Count > 0)
            {
                gvNotificacion.UseAccessibleHeader = true;
                gvNotificacion.HeaderRow.TableSection = TableRowSection.TableHeader;
                lblInfo.CssClass = "alert-info";
                lblInfo.Text = string.Format("Usted tiene {0} notificaciones no leídas.", Lst.Where(x => x.Ingreso == false).Count());
            }
            else
            {
                lblInfo.CssClass = "alert-danger";
                lblInfo.Text = "No se encontraron notificaciones.";
            }
        }

        protected void gvNotificacion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.NotificacionAppEntity Noti = (Entities.NotificacionAppEntity)r.DataItem;

                //HyperLink hlNotificacion = r.Controls[0].FindControl("hlNotificacion") as HyperLink;linkNotificacion
                LinkButton linkIcono = r.Controls[0].FindControl("linkIcono") as LinkButton;
                LinkButton linkNotificacion = r.Controls[0].FindControl("linkNotificacion") as LinkButton;
                //HtmlGenericControl icono = r.Controls[0].FindControl("icono") as HtmlGenericControl;

                linkNotificacion.Text = string.Format("{0} {1}{2}", Noti.UsuarioResponsable.Nombre, Noti.UsuarioResponsable.ApellidoPaterno, Noti.MensajeNotificado);
                //linkNotificacion.PostBackUrl = Noti.Url;
                linkNotificacion.Attributes.Add("data-url-notificacion", Noti.Url);

                if (!Noti.Ingreso)
                {
                    r.CssClass = "active";
                    linkIcono.Attributes.Add("class", "fa fa-eye-slash");
                }
                else
                {
                    linkIcono.Attributes.Add("class", "fa fa-eye");
                    linkIcono.Enabled = false;
                }
            }
        }

        protected void gvNotificacion_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "LeeNotificacion")
            {
                int rowIndex = Convert.ToInt32(e.CommandArgument);
                //Obtiene ID notificación
                int IDNotificacion = Convert.ToInt16(gvNotificacion.DataKeys[rowIndex].Value);
                int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();

                //Deja la notificación en visto
                NotificacionBL.SetIngresoNotificacion(IDNotificacion, IDUsuario, true);

                //Obtiene url de notificación
                string UrlNotificacion = (gvNotificacion.Rows[rowIndex].FindControl("linkNotificacion") as LinkButton).Attributes["data-url-notificacion"];

                Response.Redirect(UrlNotificacion);
            }

            if (e.CommandName == "MarcaNotificacion")
            {
                int rowIndex = Convert.ToInt32(e.CommandArgument);
                //Obtiene ID notificación
                int IDNotificacion = Convert.ToInt16(gvNotificacion.DataKeys[rowIndex].Value);
                int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();

                //Deja la notificación en visto
                NotificacionBL.SetIngresoNotificacion(IDNotificacion, IDUsuario, true);
                BindData(ddlOrden.SelectedItem.Value);
            }
        }

        protected void ddlOrden_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData(ddlOrden.SelectedItem.Value);
        }

        protected void cbLeidas_CheckedChanged(object sender, EventArgs e)
        {
            BindData(ddlOrden.SelectedItem.Value);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.facturacion.estado_de_pago
{
    public partial class pendiente : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindGridView();
        }

        private void BindGridView()
        {
            List<Entities.ProyectoEntity> Lst = FacturacionBL.GetEPxFacturar();
            gvEstadoPago.DataSource = Lst;
            gvEstadoPago.DataKeyNames = new string[1] { "ID" }; //PK
            gvEstadoPago.DataBind();

            if (Lst.Count > 0)
            {
                lblContador.Text = string.Format("<b>{0}</b> proyectos pendientes de facturación.", Lst.Count);
                lblContador.CssClass = "alert-info";

                gvEstadoPago.UseAccessibleHeader = true;
                gvEstadoPago.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                lblContador.Text = string.Format("No hay proyectos pendientes de facturación.", Lst.Count);
                lblContador.CssClass = "alert-danger";
            }
        }

        protected void gvEstadoPago_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.ProyectoEntity Item = (Entities.ProyectoEntity)r.DataItem;

                Label lblEPPendiente = (r.Controls[0].FindControl("lblEPPendiente") as Label);

                string ValorEPPentiente = string.Empty;

                //verifica si valor es CLP (número entero) u otro tipo de valor (decimales)
                if (Item.TipoValor.Valor > 1)
                    ValorEPPentiente = Util.Funcion.FormatoNumerico(Item.EstadoPagoPendiente);
                else
                    ValorEPPentiente = Util.Funcion.FormatoNumericoEntero(Item.EstadoPagoPendiente);

                lblEPPendiente.Text = string.Format("{0} {1}", Item.TipoValor.Sigla, ValorEPPentiente);
            }
        }
    }
}
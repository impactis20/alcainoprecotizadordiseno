﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.SignalR;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.facturacion.estado_de_pago
{
    public partial class detalle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    CargaInicial(Convert.ToInt16(Request.QueryString["id"].ToString()));
                }
            }
        }

        private void CargaInicial(int ID)
        {
            Entities.ProyectoEntity Proyecto = ProyectoBL.GetCabeceraProyecto(ID);
            lblID.Text = Proyecto.ID.ToString();
            hfIdCliente.Value = Proyecto.Cliente.ID.ToString();
            string razonSocial = Proyecto.Cliente.RazonSocial.ToString();
            string rut = Proyecto.Cliente.rutCliente.ToString();

            if (Proyecto.Padre != null)
            {
                padre.Visible = true;
                lblPadre.Text = string.Format("{0} - {1}", Proyecto.Padre.ID, Proyecto.Padre.Nombre);
            }
            else
                padre.Visible = false;

            lblNombreProyecto.Text = Proyecto.Nombre;
            lblCliente.Text = Proyecto.Cliente.NombreFantasia;
            lblFechaIngreso.Text = Proyecto.FechaIngreso.ToShortDateString();
            lblFechaPlanificada.Text = Proyecto.FechaPlanificada.ToShortDateString();
            lblEjecutivo.Text = string.Format("{0} {1} {2}", Proyecto.EjecutivoComercial.Nombre, Proyecto.EjecutivoComercial.ApellidoPaterno, Proyecto.EjecutivoComercial.ApellidoMaterno);
            lblTotalNP.Text = Proyecto.CantidadTotalNP.ToString();
            lblTipoValor.Text = Proyecto.TipoValor.Sigla;
            hfIDTipoValor.Value = Proyecto.TipoValor.ID.ToString();
            lblRazonSocial.Text = razonSocial;
            lblRut.Text = Util.Funcion.FormatoRut(rut);

            //almacenada para determinar si valores llevan decimal
            ViewState.Add("ValorTipoValor", Proyecto.TipoValor.Valor);

            lblValorProyecto.Text = Util.Funcion.FormatoNumerico(Proyecto.ValorTotal);

            //actualiza Gridview con Estados de pago
            BindData(ID);

            nombreProyecto.InnerText = Proyecto.Nombre;
            txtGlosa.Attributes.Add("onkeyup", "countChar(this, '" + lblGlosa.ClientID + "', 80)");
        }

        private void BindData(int ID)
        {
            List<Entities.EstadoPagoEntity> Lst = EstadoPagoBL.GetEstadoPago(ID);
            gvEP.DataKeyNames = new string[1] { "ID" }; //PK
            gvEP.DataSource = Lst;
            gvEP.DataBind();

            if (Lst.Count > 0)
            {
                gvEP.UseAccessibleHeader = true;
                gvEP.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gvEP_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.EstadoPagoEntity EP = (Entities.EstadoPagoEntity)r.DataItem;

                Label lblEP = (r.Controls[0].FindControl("lblEP") as Label);
                Label lblPorcentaje = (r.Controls[0].FindControl("lblPorcentaje") as Label);
                Label lblMonto = (r.Controls[0].FindControl("lblMonto") as Label);
                //HyperLink detalleEP = (r.Controls[0].FindControl("detalleEP") as HyperLink);
                Label lblGlosa = (r.Controls[0].FindControl("lblGlosa") as Label);
                Label lblFechaIngreso = (r.Controls[0].FindControl("lblFechaIngreso") as Label);

                Label lblFechaPreFacturado = (r.Controls[0].FindControl("lblFechaPreFacturado") as Label);
                Button btnFacturarEP = (r.Controls[0].FindControl("btnFacturarEP") as Button);

                if (EP.FacturaEmitida)
                {
                    btnFacturarEP.Visible = false;
                    lblFechaPreFacturado.Text = string.Format("{0:dd-MM-yyyy HH:mm}", EP.FechaPreFacturado);
                }
                else
                {
                    btnFacturarEP.Visible = true;
                    lblFechaPreFacturado.Text = "-";
                }

                lblEP.Text = (r.RowIndex + 1).ToString();
                lblPorcentaje.Text = string.Format("{0}%", Util.Funcion.FormatoNumerico(EP.Porcentaje));
                //detalleEP.Text = EP.Glosa;
                //detalleEP.Attributes.Add("data-content", EP.Glosa);
                lblGlosa.Text = EP.Glosa;
                lblFechaIngreso.Text = string.Format("{0:dd-MM-yyyy HH:mm}", EP.FechaCreacion);

                if (Convert.ToInt32(ViewState["ValorTipoValor"]) > 1)
                    lblMonto.Text = string.Format("{0} {1}", lblTipoValor.Text, Util.Funcion.FormatoNumerico(EP.Monto));
                else
                    lblMonto.Text = string.Format("{0} {1}", lblTipoValor.Text, Util.Funcion.FormatoNumericoEntero(EP.Monto));
            }
        }

        protected void gvEP_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.CommandArgument.ToString()))
            {
                int rowIndex = Convert.ToInt32(e.CommandArgument);
                int IDEP = Convert.ToInt16(gvEP.DataKeys[rowIndex].Value);
                GridViewRow r = gvEP.Rows[rowIndex];

                if (e.CommandName == "FacturarEP")
                {
                    int IDProyecto = Convert.ToInt16(Request.QueryString["id"].ToString());
                    int IDTipoValor = Convert.ToInt16(hfIDTipoValor.Value);

                    ViewState.Add("IDEPFacturar", IDEP);
                    nombreProyecto.InnerText = lblNombreProyecto.Text;
                    porcentaje.InnerText = (r.FindControl("lblPorcentaje") as Label).Text;

                    //obtiene EP actualizado (porcentaje, valor)
                    Entities.EstadoPagoEntity EP = EstadoPagoBL.GetEstadoPago(IDProyecto).Where(x => x.ID == IDEP).ToList()[0];

                    if (Convert.ToInt32(ViewState["ValorTipoValor"]) > 1)
                        lblEPxFacturar.Text = string.Format("{0}% ({1} {2})", Util.Funcion.FormatoNumerico(EP.Porcentaje), lblTipoValor.Text, Util.Funcion.FormatoNumerico(EP.Monto));
                    else
                        lblEPxFacturar.Text = string.Format("{0}% ({1} {2})", Util.Funcion.FormatoNumerico(EP.Porcentaje), lblTipoValor.Text, Util.Funcion.FormatoNumericoEntero(EP.Monto));

                    hfValorFacturacion.Value = EP.Monto.ToString(); //almacena valor a facturar

                    //2 es UF. Cuando es tipo de valor UF se muestra el calendario para seleccionar la fecha con valor a facturar
                    if (IDTipoValor == 2)
                    {
                        lblInfoFechaFacturacion.Visible = true;
                        calendarioFechaFacturacion.Visible = true;
                        rfvFechaFacturacion.ValidationGroup = rfvFechaVencimiento.ValidationGroup;
                        rfvFechaFacturacion.DataBind();
                    }

                    ScriptManager.RegisterStartupScript(this, GetType(), "Facturacion", "$('#" + facturar.ClientID + "').modal({backdrop: 'static',keyboard: true});", true);
                }
            }
        }

        private void Facturar()
        {
            int IDProyecto = Convert.ToInt16(Request.QueryString["id"].ToString());
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
            int IDEP = Convert.ToInt16(ViewState["IDEPFacturar"].ToString());
            int IDTipoValor = Convert.ToInt16(hfIDTipoValor.Value);
            int IDCliente = Convert.ToInt16(hfIdCliente.Value);
            string CondicionPago = ddlCondicionPago.SelectedItem.Value;
            DateTime FechaVencimiento = Convert.ToDateTime(txtFechaVencimiento.Text);
            string NroOC = txtNroOC.Text;
            DateTime FechaOC = string.IsNullOrEmpty(txtFechaOC.Text) ? DateTime.MinValue : Convert.ToDateTime(txtFechaOC.Text);
            string NroHes = txtNroHes.Text;
            DateTime FechaHes = string.IsNullOrEmpty(txtFechaHes.Text) ? DateTime.MinValue : Convert.ToDateTime(txtFechaHes.Text);
            string Glosa = txtGlosa.Text.Trim();
            decimal ValorTotalFactura = IDTipoValor == 2 ? Convert.ToDecimal(txtTotalFacturacion.Text) : Convert.ToDecimal(hfValorFacturacion.Value);

            int IDNotificacionApp = 0;

            //Respuesta: Devuelve 0 cuando el estado de pago ya fue facturado,
            //           Devuelve - 1 cuando el proyecto está en USD o Euro
            //           De lo contrario devuelve el nro de la factura que genera Manager
            int Respuesta = FacturacionBL.AddFacturacionxEP(IDProyecto, IDUsuario, IDCliente, IDEP, ValorTotalFactura, CondicionPago, FechaVencimiento, NroOC, FechaOC, NroHes, FechaHes, Glosa, ref IDNotificacionApp);

            if (Respuesta == 0)
            {
                //Otro usuario ya facturó el EP
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error al generar la factura...", "La factura del Estado de Pago ya fue generada."), true);
            }
            else
            {
                //NOTIFICACIÓN (4): Notifica a los usuarios que hay un Estado de pago facturado
                hubs.NotificacionAppHub.EnviaNotificacionesUsuariosConectados(GlobalHost.ConnectionManager.GetHubContext<hubs.NotificacionAppHub>());

                //La facturación se genero correctamente
                Entities.ProyectoEntity Proyecto = ProyectoBL.GetCabeceraProyecto(IDProyecto);

                string NombreProyecto = string.Format("{0} - {1}", IDProyecto, Proyecto.Nombre);
                string NombreUsuario = UsuarioBL.GetUsuario(IDUsuario, string.Empty, string.Empty, string.Empty, null, false)[0].Nombre;

                //notifica vía correo electrónico al usuario facturador de manager
                string UrlProyecto = string.Format("http://{0}/makeup2/proyecto/visualizar/datos.aspx?id={1}", Request.Url.Authority, IDProyecto);
                string Para = ConfigurationManager.AppSettings["emailsOrdenFacturacion"];
                string Asunto = string.Format("Orden de Facturación Proyecto: {0}", NombreProyecto);
                string Cuerpo = string.Format("<span style=\"color:#1f497d\">Estimada <b>Luisa Uribe</b>,<br><br>" +
                    "Mediante el presente le informamos a usted que con fecha <b>{0}</b> el ejecutivo comercial <b>{1}</b> ha dado orden de facturación al proyecto <b style=\"text-decoration:none\"><a href={2} target=\"_blank\">{3}</a></b> del cliente <b>{4}</b> por un monto de <b>$ {5}</b>.<br><br>" +
                    "</span>",
                    DateTime.Now.ToShortDateString(), //fecha de envío
                    NombreUsuario, //nombre de usuario
                    Util.GetComillas(UrlProyecto), //url del proyecto
                    NombreProyecto, //nombre del proyecto
                    Proyecto.Cliente.RazonSocial, //nombre de fantasía
                    Util.Funcion.FormatoNumericoEntero(ValorTotalFactura)); //valor de la factura

                //cuando el proyecto es CLP o UF se incluye el nro de la factura de Manager en el correo
                if (Respuesta != -1) //Respuesta tiene valor -1 cuando el proyecto es USD o Euro
                    Cuerpo += string.Format("<span style=\"color:#1f497d\">El correlativo asignado en Manager para esta factura es: <b>{0}</b></span>.", Respuesta);

                Cuerpo += "<span style=\"color:#1f497d\"><br><br>Si tiene dudas o consultas diríjase a su administrador.<br><br>Atte.<br>Sistema Make Up.</span>";

                //credenciales correo de envío MakeUp
                string Remitente = ConfigurationManager.AppSettings["remitenteMakeUp"];
                string De = ConfigurationManager.AppSettings["emailMakeUp"];
                string Ps = ConfigurationManager.AppSettings["passwordMakeUp"];

                string RespuestaWS = App_Code.WebService.Email.EnviarEmail(De, Ps, De, Para, string.Empty, string.Empty, Remitente, Asunto, Cuerpo);

                bool CorreoEnviado = Convert.ToBoolean(Convert.ToInt16(RespuestaWS.Split('.')[0]));

                string Mensaje = "Se generó correctamente la factura del Estado de Pago.";
                string IconoEmail = "<br><i class=\"fa fa-envelope-o\" aria-hidden=\"true\" style=\"margin-left: 30px; margin-right: 4px\">&nbsp;&nbsp;</i>";
                string EmailNotificacion = string.Format("{0}{1}", IconoEmail, Para.Replace(",", IconoEmail));

                if (CorreoEnviado)
                    Mensaje += string.Format("<br><br><i class=\"fa fa-check text-success\" aria-hidden=\"true\" style=\"margin-eft: 4px;margin-right: 4px\">&nbsp;&nbsp;</i> Notificación enviada correctamente a:{0}", EmailNotificacion);
                else
                    Mensaje += string.Format("<br><br><i class=\"fa fa-exclamation-triangle text-danger\" aria-hidden=\"true\" style=\"margin-eft: 4px;margin-right: 4px\">&nbsp;&nbsp;</i>Por un error interno de servicio la notificación de correo electrónico no pudo ser enviada a:{0}", EmailNotificacion);

                ScriptManager.RegisterStartupScript(this, GetType(), "Pop307", Util.GetMensajeAlerta("success", "Factura generada correctamente...", Mensaje), true);
            }

            //Limpia formulario
            ddlCondicionPago.SelectedIndex = -1;
            txtGlosa.Text = string.Empty;

            //actualiza gridview con estados de pago
            BindData(IDProyecto);


        }

        protected void btnVolver_ServerClick(object sender, EventArgs e)
        {
            lblinfoNHES.Text = "";
            lblinfoNOC.Text = "";
            lblFOC.Text = "";
            lblFHES.Text = "";

            ScriptManager.RegisterStartupScript(this, GetType(), "Facturacion", "$('#" + facturar.ClientID + "').modal({backdrop: 'static',keyboard: true});", true);

            txtFechaFacturacion.Text = hfFechaFacturación.Value;
            DateTime fechaFact = Convert.ToDateTime(hfFechaFacturación.Value);
            double ValorDia = TipoValorBL.GetValorTipoValor(2, fechaFact);
            string valorDiaF = Convert.ToString(string.Format("{0:#,0.##}", ValorDia));
            string moneda = "CLP";

            txtFechaVencimiento.Text = hfFechaVencimiento.Value;
            txtNroOC.Text = hfNOC.Value;
            txtFechaOC.Text = hfFechaOC.Value;
            txtNroHes.Text = hfNHes.Value;
            txtFechaHes.Text = hfFNHes.Value;
            txtGlosa.Text = hfGlosa.Value;
            txtFechaFacturacion.Text = hfFechaFacturación.Value;
            lblValorDia.Text = string.Format("{0} {1}", moneda, valorDiaF);
            txtTotalFacturacion.Text = hfTotalFacturar.Value;
            ddlCondicionPago.SelectedItem.Value = hfCondicionPago.Value;
        }

        protected void Continuar_ServerClick(object sender, EventArgs e)
        {
            Facturar();
        }

        protected void btnConfirmaFacturacion_ServerClick(object sender, EventArgs e)
        {
            hfFechaVencimiento.Value = txtFechaVencimiento.Text;
            hfNOC.Value = txtNroOC.Text;
            hfFechaOC.Value = txtFechaOC.Text;
            hfNHes.Value = txtNroHes.Text;
            hfFNHes.Value = txtFechaHes.Text;
            hfGlosa.Value = txtGlosa.Text;
            hfFechaFacturación.Value = txtFechaFacturacion.Text;
            hfValorDia.Value = lblValorDia.Text;
            hfTotalFacturar.Value = txtTotalFacturacion.Text;
            hfCondicionPago.Value = ddlCondicionPago.SelectedItem.Value;


            if (txtNroOC.Text == "" && txtFechaOC.Text == "" && txtNroHes.Text == "" && txtFechaHes.Text == "")
            {
                lblmensaje.CssClass = "alert-danger";
                lblmensaje.Text = "No ha ingresado datos para Nro/Fecha OC, Nro/Fecha HES, ¿Desea continuar?";
                ClientScript.RegisterStartupScript(this.GetType(), "myScript", "Mensaje();", true);

            }

            else if (txtNroOC.Text != "" && txtFechaOC.Text != "" && txtNroHes.Text == "" && txtFechaHes.Text == "")
            {
                lblmensaje.CssClass = "alert-danger";
                lblmensaje.Text = "Ha ingresado Nro/fecha OC sin ingresar Nro/Fecha HES, ¿Desea continuar?";
                ClientScript.RegisterStartupScript(this.GetType(), "myScript", "Mensaje();", true);

            }
            else if (txtNroOC.Text == "" && txtFechaOC.Text == "" && txtNroHes.Text != "" && txtFechaHes.Text != "")
            {
                lblmensaje1.CssClass = "alert-danger";
                lblmensaje1.Text = "Debe ingresar Nro/Fecha OC.";
                ClientScript.RegisterStartupScript(this.GetType(), "myScript", "Mensaje1();", true);

            }
            else if (txtNroOC.Text != "" && txtFechaOC.Text == "")
            {
                lblFHES.Text = "";
                lblinfoNOC.Text = "";
                lblinfoNHES.Text = "";
                //lblinfoNOC.Text = "Debe ingresar Fecha OC para Numero OC.";
                lblFOC.Text = "Debe ingresar Fecha OC para Numero OC.";
                ScriptManager.RegisterStartupScript(this, GetType(), "Facturacion", "$('#" + facturar.ClientID + "').modal({backdrop: 'static',keyboard: true});", true);
            }
            else if (txtNroOC.Text == "" && txtFechaOC.Text != "")
            {
                lblFHES.Text = "";
                lblinfoNHES.Text = "";
                lblFOC.Text = "";
                //lblFOC.Text = "Debe ingresar Numero OC para Fecha OC.";
                lblinfoNOC.Text = "Debe ingresar Numero OC para Fecha OC.";
                ScriptManager.RegisterStartupScript(this, GetType(), "Facturacion", "$('#" + facturar.ClientID + "').modal({backdrop: 'static',keyboard: true});", true);
            }
            else if (txtNroHes.Text != "" && txtFechaHes.Text == "")
            {
                lblinfoNHES.Text = "";
                lblFOC.Text = "";
                lblinfoNOC.Text = "";
                //lblinfoNHES.Text = "Debe ingresar Fecha HES para Numero HES.";
                lblFHES.Text = "Debe ingresar Fecha HES para Numero HES.";
                ScriptManager.RegisterStartupScript(this, GetType(), "Facturacion", "$('#" + facturar.ClientID + "').modal({backdrop: 'static',keyboard: true});", true);

            }
            else if (txtNroHes.Text == "" && txtFechaHes.Text != "")
            {
                lblFOC.Text = "";
                lblFHES.Text = "";
                lblinfoNOC.Text = "";
                lblinfoNHES.Text = "Debe ingresar Fecha HES para Numero HES.";
                //lblFHES.Text = "Debe ingresar Fecha HES para Numero HES.";
                ScriptManager.RegisterStartupScript(this, GetType(), "Facturacion", "$('#" + facturar.ClientID + "').modal({backdrop: 'static',keyboard: true});", true);
            }
            else if (txtNroOC.Text != "" && txtFechaOC.Text != "" && txtNroHes.Text != "" && txtFechaHes.Text != "")
            {
                Facturar();
            }
        }

        [WebMethod]
        public static float GetValorUF(string date)
        {
            DateTime Fecha = Convert.ToDateTime(date);

            return TipoValorBL.GetValorTipoValor(2, Fecha);
        }
    }
}
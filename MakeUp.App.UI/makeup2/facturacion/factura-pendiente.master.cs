﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MakeUp.App.UI.makeup2.facturacion
{
    public partial class factura_pendiente : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            EstableceActiveTab();
        }

        private void EstableceActiveTab()
        {
            for (int x = 0; x < tabs.Controls.Count; x++)
            {
                if (x % 2 == 1)
                {
                    string Url = Request.Url.AbsolutePath;
                    HyperLink link = (HyperLink)tabs.Controls[x];

                    LiteralControl li = tabs.Controls[x - 1] as LiteralControl;

                    if (Url.Contains(link.NavigateUrl.Replace("~", string.Empty).Split('?')[0]))
                        li.Text = li.Text.Replace("<li>", "<li class='active'>");
                    else
                        li.Text = li.Text.Replace("<li class='active'>", "<li>");
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Services;
using System.Web.UI.WebControls;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.proyecto
{
    public partial class buscar : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
            Entities.RolPredeterminadoEntity rolPredeterminado = UsuarioBL.GetRolUsuario(IDUsuario);
            int rolID = rolPredeterminado.ID;
            if (!Page.IsPostBack)
            {

                //Agregado por Gloria Molina, 
                if (rolID == 3 || rolID == 6)
                {
                    rdTodos.Checked = false;
                    rdMisProyectos.Checked = true;
                    rdTodos.Visible = true;
                    rdMisProyectos.Visible = true;
                    int controlUsuario = 1;
                    BindEstadoNP();
                    BuscarProyecto(controlUsuario, rolID, IDUsuario);
                }
                else {
                        int controlUsuario = 0;
                        BindEstadoNP();
                        BuscarProyecto(controlUsuario, rolID, IDUsuario);
                        rdTodos.Visible = false;
                        rdMisProyectos.Visible = false;
                }


                //BindEstadoNP();
                //BuscarProyecto();
    
            }
        }

        private void BindEstadoNP()
        {
            lstEstadoNP.DataTextField = "Descripcion";
            lstEstadoNP.DataValueField = "ID";

            lstEstadoNP.DataSource = NotaPedidoBL.GetEstadoNP();
            lstEstadoNP.DataBind();


            //por defecto todas seleccionadas
            foreach (ListItem item in lstEstadoNP.Items)
            {
                int value = Convert.ToInt16(item.Value);
                if (value != 4)
                {
                    item.Selected = true;
                }
            }
        }

        protected void btnBuscar_ServerClick(object sender, EventArgs e)
        {

            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
            Entities.RolPredeterminadoEntity rolPredeterminado = UsuarioBL.GetRolUsuario(IDUsuario);
            int rolID = rolPredeterminado.ID;
            eliminado.Visible = false;
            if (rolID == 3 || rolID == 6)
            {
                if (rdMisProyectos.Checked)
                {
                    rdTodos.Visible = true;
                    rdMisProyectos.Visible = true;
                    int controlUsuario = 1;
                    BuscarProyecto(controlUsuario, rolID, IDUsuario);
                }
                else
                {
                    rdTodos.Visible = true;
                    rdMisProyectos.Visible = true;
                    int controlUsuario = 0;
                    BuscarProyecto(controlUsuario, rolID, IDUsuario);
                }

            }
            else
            {
                int controlUsuario = 0;
                BuscarProyecto(controlUsuario, rolID, IDUsuario);
                rdTodos.Visible = false;
                rdMisProyectos.Visible = false;
            }

        }

       
     
        private void BuscarProyecto(int controlUsuario, int rolID, int IDUsuario)
        {

           
            int IDProyecto = txtID.Text.Trim() == string.Empty ? 0 : Convert.ToInt16(txtID.Text.Trim());
            string NombreProyecto = txtNombre.Text;
            string NombreFantasia = txtCliente.Text;
            List<Entities.EstadoNPEntity> LstFiltro = new List<Entities.EstadoNPEntity>();

            foreach (ListItem Item in lstEstadoNP.Items)
            {
                int value = Convert.ToInt16(Item.Value);

                    if (Item.Selected)
                        LstFiltro.Add(new Entities.EstadoNPEntity(value));
            }

            List<Entities.ProyectoEntity> Lst = ProyectoBL.GetBuscarProyecto(IDProyecto, NombreProyecto, NombreFantasia, LstFiltro, controlUsuario, rolID, IDUsuario);
            gvProyecto.DataSource = Lst;
            gvProyecto.DataBind();


            int CantidadProyectoPorRegularizar = Lst.Where(x => x.PorcentajeTotalEP == 100 && (x.CantidadNPPorAjustar > 0 || x.CantidadNPPorValorizar > 0)).ToList().Count;

            //consulta si tiene proyectos por regularizar
            if (CantidadProyectoPorRegularizar > 0)
                porRegularizar.Visible = true;
            else
                porRegularizar.Visible = false;

            if (Lst.Count > 0)
            {
                gvProyecto.UseAccessibleHeader = true;
                gvProyecto.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gvProyecto_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.ProyectoEntity Proyecto = (Entities.ProyectoEntity)r.DataItem;
                HyperLink lnkProyecto = (r.Controls[0].FindControl("lnkProyecto") as HyperLink);
                Label lblPorAjustar = (r.Controls[0].FindControl("lblPorAjustar") as Label);
                Label lblPorValorizar = (r.Controls[0].FindControl("lblPorValorizar") as Label);
                Label lblPorFacturar = (r.Controls[0].FindControl("lblPorFacturar") as Label);
                Label lblFacturado = (r.Controls[0].FindControl("lblFacturado") as Label);
                
                (r.Controls[0].FindControl("lblFechaPlanificada") as Label).Text = Proyecto.FechaPlanificada.ToShortDateString();
                (r.Controls[0].FindControl("lblFechaCreacion") as Label).Text = Proyecto.FechaIngreso.ToShortDateString();

                //verifica si es proyecto por regularizar, eliminado o proyecto vigente
                if (Proyecto.Estado)
                {
                    //pregunta si es un proyecto por regularizar
                    if (Proyecto.PorcentajeTotalEP == 100 && (Proyecto.CantidadNPPorAjustar > 0 || Proyecto.CantidadNPPorValorizar > 0))
                        lnkProyecto.Text = "<i class='fa fa-exclamation-triangle text-warning' style='margin-right: 4px'></i> ";                        

                    lnkProyecto.NavigateUrl = string.Format("~/makeup2/proyecto/visualizar/datos.aspx?id={0}", Proyecto.ID);
                    lnkProyecto.Text += Proyecto.Nombre;
                }
                else
                {
                    lnkProyecto.NavigateUrl = string.Format("~/makeup2/proyecto/eliminado/datos.aspx?id={0}", Proyecto.ID);
                    lnkProyecto.Text = string.Format("<i class='fa fa-trash text-danger' style='margin-right: 4px'></i> {0}", Proyecto.Nombre);
                }

                //Agrega ejecutivo comercial
                (r.Controls[0].FindControl("lblEjecutivo") as Label).Text = string.Format("{0} {1}", Proyecto.EjecutivoComercial.Nombre, Proyecto.EjecutivoComercial.ApellidoPaterno);

                //Ordena los Estados de NP
                SetLabelEstadoNP(ref lblPorAjustar, Proyecto.CantidadNPPorAjustar, "label-danger", "Por ajustar");
                SetLabelEstadoNP(ref lblPorValorizar, Proyecto.CantidadNPPorValorizar, "label-warning", "Por valorizar");
                SetLabelEstadoNP(ref lblPorFacturar, Proyecto.CantidadNPPorFacturar, "label-success", "Por factura");
                SetLabelEstadoNP(ref lblFacturado, Proyecto.CantidadNPFacturada, "label-primary", "Facturadas");
            }
        }

        private void SetLabelEstadoNP(ref Label lbl, int Valor, string CssClass, string TituloToolTip)
        {
            if (Valor > 0)
            {
                lbl.Text = Valor.ToString();
                lbl.CssClass += string.Format(" {0}", CssClass);
                lbl.Attributes.Add("title", TituloToolTip);
            }
            else {
                lbl.Text = "&nbsp;&nbsp;";
            }
        }

        protected void lnkPorRegularizar_Click(object sender, EventArgs e)
        {
            eliminado.Visible = false;
            porRegularizar.Visible = true;

            int IDProyecto = txtID.Text.Trim() == string.Empty ? 0 : Convert.ToInt16(txtID.Text.Trim());
            string NombreProyecto = txtNombre.Text;
            string NombreFantasia = txtCliente.Text;

            List<Entities.ProyectoEntity> Lst = ProyectoBL.GetBuscarProyectoPorRegularizar(IDProyecto, NombreProyecto, NombreFantasia);
            gvProyecto.DataSource = Lst;
            gvProyecto.DataBind();

            if (Lst.Count > 0)
            {
                gvProyecto.UseAccessibleHeader = true;
                gvProyecto.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void lnkEliminados_Click(object sender, EventArgs e)
        {
            eliminado.Visible = true;
            porRegularizar.Visible = true;

            int IDProyecto = txtID.Text.Trim() == string.Empty ? 0 : Convert.ToInt16(txtID.Text.Trim());
            string NombreProyecto = txtNombre.Text;
            string NombreFantasia = txtCliente.Text;

            List<Entities.ProyectoEntity> Lst = ProyectoBL.GetBuscarProyectoEliminado(IDProyecto, NombreProyecto, NombreFantasia);
            gvProyecto.DataSource = Lst;
            gvProyecto.DataBind();

            if (Lst.Count > 0)
            {
                gvProyecto.UseAccessibleHeader = true;
                gvProyecto.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        //private void myCombo_Loaded(object sender, System.Windows.Routed e)
        //{
        //    ControlTemplate ct = this.lstEstadoNP.Template;
        //    Popup pop = ct.FindName("PART_Popup", this.lstEstadoNP) as Popup;
        //    pop.Placement = PlacementMode.Top;
        //}

        [WebMethod]
        public static string[] GetNombreFantasia(string prefix)
        {
            return GenericoBL.GetAutocompletar(prefix, "cliente", "nombre_fantasia").ToArray();
        }

        [WebMethod]
        public static string[] GetNombreProyecto(string prefix)
        {
            return GenericoBL.GetAutocompletar(prefix, "proyecto", "nombre").ToArray();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.proyecto.eliminado
{
    public partial class notas_de_pedido : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //disponibiliza controles según autorizaciones del administrador
            CheckControlEnabled();

            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    CargaInicial(Convert.ToInt16(Request.QueryString["id"].ToString()));
                }
                else
                {
                    Page.Title = "Proyecto no encontrado...";
                }
            }
        }

        private void CheckControlEnabled()
        {
            //RECORDAR: Este es un módulo de NP eliminados y quien tenga acceso podrá ver todo, valorización, precios, valores totales, etc...
            //oculta controles restringidos

            App_Code.Seguridad.GetControlAutorizado(Page, Request.Path);
        }

        private void CargaInicial(int IDProyecto)
        {
            Entities.ProyectoEntity Proyecto = ProyectoBL.GetCabeceraProyectoEliminado(IDProyecto);

            if (Proyecto != null)
            {
                hfIdProyecto.Value = Proyecto.ID.ToString();
                hfTipoMoneda.Value = Proyecto.TipoValor.Sigla;

                //Agrega item CLP para traducir el valor de la caja de texto
                AddFunctionValorTotalProyectoCLP(ref ddlTipoValor1, Proyecto.TipoValor.Sigla, Proyecto.TipoValor.Valor);

                if (Proyecto.TipoValor.Valor > 1)
                {
                    //Almacena el valor del tipo de valor
                    ViewState.Add("ValorTipoValor", Proyecto.TipoValor.Valor);
                }

                BindDataNP(Proyecto.ID);
            }
        }

        //Agrega item CLP para traducir el valor de la caja de texto
        public static void AddFunctionValorTotalProyectoCLP(ref DropDownList ddl, string Sigla, decimal Valor)
        {
            ddl.Items.Add(new ListItem(Sigla, "1"));

            if (Valor > 1)
                ddl.Items.Add(new ListItem("CLP", Valor.ToString())); //calculará a pesos chilenos el tipo de valor seleccionado

            ddl.DataBind();
        }
        
        #region Informe Global del proyecto
        protected void btnInformeGlobalProyecto_Click(object sender, EventArgs e)
        {
            int IDProyecto = Convert.ToInt16(Request.QueryString["id"].ToString());

            Entities.ProyectoEntity Proyecto = ProyectoBL.GetCabeceraProyectoEliminado(IDProyecto);
            tituloProyectoInformeNP.InnerText = string.Format("{0} - {1}", Proyecto.ID.ToString(), Proyecto.Nombre);

            if (Proyecto.Padre != null)
            {
                padre.Visible = true;
                lblPadre.Text = string.Format("{0} - {1}", Proyecto.Padre.ID, Proyecto.Padre.Nombre);
            }
            else
                padre.Visible = false;

            lblCliente.Text = Proyecto.Cliente.NombreFantasia;
            lblFechaIngreso.Text = Proyecto.FechaIngreso.ToShortDateString();
            lblFechaPlanificada.Text = Proyecto.FechaPlanificada.ToShortDateString();
            lblEjecutivo.Text = string.Format("{0} {1} {2} <var>({3})</var>", Proyecto.EjecutivoComercial.Nombre, Proyecto.EjecutivoComercial.ApellidoPaterno, Proyecto.EjecutivoComercial.ApellidoMaterno, Proyecto.EjecutivoComercial.Email);
            lblDireccion.Text = string.Format("{0}, {1}", Proyecto.Direccion, Proyecto.Comuna);

            if (Proyecto.TipoValor.Valor > 1)
            {
                ViewState.Add("TotalProyecto", string.Format("{0} {1}", Proyecto.TipoValor.Sigla, Util.Funcion.FormatoNumerico(Proyecto.ValorTotal)));
                //ViewState.Add("TotalProyectoCLP", string.Format("CLP {0}", Util.Funcion.FormatoNumericoEntero(Proyecto.ValorTotal * Proyecto.TipoValor.Valor)));
                ViewState.Add("TotalProyectoCLP", 0); //valor total del proyecto en CLP se calculará mediante la sumatoria del valor total de cada NP, de forma de obtener valores aproximados.
            }
            else
            {
                ViewState.Add("TotalProyecto", string.Format("{0} {1}", Proyecto.TipoValor.Sigla, Util.Funcion.FormatoNumericoEntero(Proyecto.ValorTotal)));
                ViewState.Add("TotalProyectoCLP", null);
            }

            //obtiene data gridview
            BindDataInformeGlobalNP(IDProyecto);

            ScriptManager.RegisterStartupScript(this, GetType(), "informeGlobal", "$('#" + informeGlobal.ClientID + "').modal({backdrop: 'static',keyboard: true});", true);
        }

        private void BindDataInformeGlobalNP(int IDProyecto)
        {
            try
            {
                List<Entities.NotaPedidoEntity> Lst = NotaPedidoBL.GetNP(IDProyecto);
                gvNPInformeGlobal.DataKeyNames = new string[1] { "ID" }; //PK
                gvNPInformeGlobal.DataSource = Lst;
                gvNPInformeGlobal.DataBind();

                if (Lst.Count > 0)
                {
                    gvNPInformeGlobal.UseAccessibleHeader = true;
                    gvNPInformeGlobal.HeaderRow.TableSection = TableRowSection.TableHeader;

                    //determina si muestra columna traductora a CLP en informe global
                    //Recordar que la columna 7 no pertenece al grupo de permisos. Ésta se muestra si el tipo de moneda no es CLP y si el usuario tiene acceso a ver los valores del informe global
                    //cuando el proyecto está eliminado, la columna 6 estará habilitada
                    gvNPInformeGlobal.Columns[7].Visible = ViewState["TotalProyectoCLP"] != null ? true : false;
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        protected void gvNPInformeGlobal_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.NotaPedidoEntity NP = (Entities.NotaPedidoEntity)r.DataItem;

                Label lblColor = (r.Controls[0].FindControl("lblColor") as Label);

                switch (NP.IDEstadoNP)
                {
                    case 1:
                        lblColor.CssClass = "label";
                        lblColor.Attributes.Add("style", "background-color:#d9534f !important");
                        SetRowValues(r, NP.IDCompuesta, NP.Creacion.Cantidad, NP.Creacion.TipoUnidad.Sigla, NP.Creacion.Descripcion, NP.Creacion.ValorUnitario);
                        break;
                    case 2:
                        lblColor.CssClass = "label";
                        lblColor.Attributes.Add("style", "background-color:#f0ad4e !important");
                        SetRowValues(r, NP.IDCompuesta, NP.Creacion.Cantidad, NP.Creacion.TipoUnidad.Sigla, NP.Ajuste.Descripcion, NP.Creacion.ValorUnitario);
                        break;
                    case 3:
                        lblColor.CssClass = "label";
                        lblColor.Attributes.Add("style", "background-color:#5cb85c !important");
                        SetRowValues(r, NP.IDCompuesta, NP.Valorizacion.Cantidad, NP.Creacion.TipoUnidad.Sigla, NP.Valorizacion.Descripcion, NP.Valorizacion.ValorUnitario);
                        break;
                    case 4:
                        lblColor.CssClass = "label";
                        lblColor.Attributes.Add("style", "background-color:#337ab7 !important");
                        SetRowValues(r, NP.IDCompuesta, NP.Valorizacion.Cantidad, NP.Creacion.TipoUnidad.Sigla, NP.Valorizacion.Descripcion, NP.Valorizacion.ValorUnitario);
                        break;
                }
            }

            if (r.RowType == DataControlRowType.Footer)
            {
                Label lblValorProyecto = (r.Controls[0].FindControl("lblValorProyecto") as Label);
                lblValorProyecto.Text = string.Format("<strong>{0}</strong>", ViewState["TotalProyecto"].ToString());

                if (ViewState["TotalProyectoCLP"] != null)
                {
                    Label lblValorProyectoCLP = (r.Controls[0].FindControl("lblValorProyectoCLP") as Label);
                    lblValorProyectoCLP.Text = string.Format("<strong>CLP {0}</strong>", Util.Funcion.FormatoNumericoEntero(ViewState["TotalProyectoCLP"].ToString()));
                }
            }
        }

        private void SetRowValues(GridViewRow r, string IDCompuesta, decimal Cantidad, string TipoUnidad, string Descripcion, decimal ValorUnitario)
        {
            Label lblColor = (r.Controls[0].FindControl("lblColor") as Label);
            Label lblIDNP = (r.Controls[0].FindControl("lblIDNP") as Label);
            Label lblCant = (r.Controls[0].FindControl("lblCant") as Label);
            Label lblUnidad = (r.Controls[0].FindControl("lblUnidad") as Label);
            Label lblDesc = (r.Controls[0].FindControl("lblDesc") as Label);
            Label lblSiglaTipoValor = (r.Controls[0].FindControl("lblSiglaTipoValor") as Label);
            Label lblValorUnitario = (r.Controls[0].FindControl("lblValorUnitario") as Label);
            Label lblValorTotal = (r.Controls[0].FindControl("lblValorTotal") as Label);
            

            string TipoValor = ViewState["TotalProyecto"].ToString().Split(' ')[0];
            decimal ValorNP = ValorUnitario * Cantidad;

            lblIDNP.Text = IDCompuesta.Split('-')[1];
            lblCant.Text = Util.Funcion.FormatoNumerico(Cantidad);
            lblUnidad.Text = TipoUnidad;
            lblDesc.Text = Descripcion;
            lblSiglaTipoValor.Text = TipoValor;

            if (ViewState["ValorTipoValor"] != null)
            {
                decimal ValorNPCLP = ValorNP * Convert.ToDecimal(ViewState["ValorTipoValor"]);
                lblValorUnitario.Text = Util.Funcion.FormatoNumerico(ValorUnitario);
                lblValorTotal.Text = Util.Funcion.FormatoNumerico(ValorNP);
                //Label aparecerá cuando el proyecto no sea CLP
                Label lblValorTotalCLP = (r.Controls[0].FindControl("lblValorTotalCLP") as Label);
                lblValorTotalCLP.Text = Util.Funcion.FormatoNumericoEntero(ValorNPCLP);

                ViewState["TotalProyectoCLP"] = Convert.ToInt32(ViewState["TotalProyectoCLP"]) + Convert.ToInt32(ValorNPCLP); //convierte valores a enteros. Ej: 1.9 = 2, 1.4 = 1
            }
            else
            {
                lblValorUnitario.Text = Util.Funcion.FormatoNumericoEntero(ValorUnitario);
                lblValorTotal.Text = Util.Funcion.FormatoNumericoEntero(ValorNP);
            }
        }

        protected void gvNPInformeGlobal_DataBound(object sender, EventArgs e)
        {
            App_Code.Seguridad.GetControlGridviewAutorizado(Page, Request.Path, gvNPInformeGlobal.ID);
        }
        #endregion

        #region Informe Resumen de NP
        protected void btnResumenNP_Click(object sender, EventArgs e)
        {
            tituloProyectoResumenNP.InnerText = (Page.Master.Master.Master.FindControl("menu").FindControl("contenido").FindControl("titulo") as HtmlGenericControl).InnerText; //copia texto del titulo padre

            List<Entities.NotaPedidoEntity> Lst = NotaPedidoBL.GetResumenNPProyecto(Convert.ToInt16(hfIdProyecto.Value));
            gvResumenNP.DataKeyNames = new string[1] { "ID" }; //PK
            gvResumenNP.DataSource = Lst.Where(x => x.Estado == true);
            gvResumenNP.DataBind();

            if (Lst.Count > 0)
            {
                gvResumenNP.UseAccessibleHeader = true;
                gvResumenNP.HeaderRow.TableSection = TableRowSection.TableHeader;

                ScriptManager.RegisterStartupScript(this, GetType(), "ResumenNP", "$('#" + resumenNP.ClientID + "').modal({backdrop: 'static',keyboard: true});", true);
            }
        }

        protected void gvResumenNP_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.NotaPedidoEntity NP = (Entities.NotaPedidoEntity)r.DataItem;

                (r.Controls[0].FindControl("lblIDNPCompuesta") as Label).Text = NP.IDCompuesta.Split('-')[1];

                //La creación nunca llevará un asterisco
                decimal TotalCreacion = NP.Creacion.Cantidad * NP.Creacion.ValorUnitario;
                Label lblCantC = (r.Controls[0].FindControl("lblCantC") as Label);
                Label lblDescC = (r.Controls[0].FindControl("lblDescC") as Label);
                Label lblVUC = (r.Controls[0].FindControl("lblVUC") as Label);
                Label lblVTC = (r.Controls[0].FindControl("lblVTC") as Label);

                //oculta controles restringidos
                lblVUC.Visible = false;
                lblVTC.Visible = false;

                lblCantC.Text = Util.Funcion.FormatoNumerico(NP.Creacion.Cantidad);
                lblDescC.Text = NP.Creacion.Descripcion;

                //si es mayor a 1 es porque el proyecto no es CLP
                if (ViewState["ValorTipoValor"] != null)
                {
                    lblVUC.Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumerico(NP.Creacion.ValorUnitario));
                    lblVTC.Text = Util.Funcion.FormatoNumerico(TotalCreacion);
                }
                else
                {
                    lblVUC.Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumericoEntero(NP.Creacion.ValorUnitario));
                    lblVTC.Text = Util.Funcion.FormatoNumericoEntero(TotalCreacion);
                }

                //verifica si en la creación la cantidad, descripcion y valor unitario, fueron modificados en el tiempo. De ser así se marca en negrita
                if (NP.Creacion.CantidadModificada)
                    lblCantC.Text = string.Format("<strong>{0}</strong>", lblCantC.Text);

                if (NP.Creacion.DescripcionModificada)
                    lblDescC.Text = string.Format("<strong>{0}</strong>", lblDescC.Text);

                if (NP.Creacion.ValorUnitarioModificado)
                    lblVUC.Text = string.Format("<strong>{0}</strong>", lblVUC.Text);


                if (NP.IDEstadoNP >= 2)
                {
                    Label lblCantA = (r.Controls[0].FindControl("lblCantA") as Label);
                    Label lblDescA = (r.Controls[0].FindControl("lblDescA") as Label);

                    lblCantA.Text = Util.Funcion.FormatoNumerico(NP.Ajuste.Cantidad);
                    lblDescA.Text = NP.Ajuste.Descripcion;

                    string Asterisco = "<i class='fa fa-asterisk text-danger' style='margin-left:5px' aria-hidden='true'></i>";

                    if (NP.Creacion.Cantidad != NP.Ajuste.Cantidad) //verifica si la cantidad del Ajuste varió respecto a la cantidad de la Creación
                        lblCantA.Text += Asterisco;

                    if (NP.Creacion.Descripcion != NP.Ajuste.Descripcion) //verifica si la descripción del Ajuste varió respecto a la descripción de la Creación
                        lblDescA.Text += Asterisco;

                    if (NP.IDEstadoNP >= 3)
                    {
                        decimal TotalValorizacion = NP.Valorizacion.ValorUnitario * NP.Valorizacion.Cantidad;
                        Label lblCantV = (r.Controls[0].FindControl("lblCantV") as Label);
                        Label lblDescV = (r.Controls[0].FindControl("lblDescV") as Label);
                        Label lblVUV = (r.Controls[0].FindControl("lblVUV") as Label);
                        Label lblVTV = (r.Controls[0].FindControl("lblVTV") as Label);

                        //oculta controles restringidos
                        lblVUV.Visible = false;
                        lblVTV.Visible = false;

                        lblCantV.Text = Util.Funcion.FormatoNumerico(NP.Valorizacion.Cantidad);
                        lblDescV.Text = NP.Valorizacion.Descripcion;

                        //si es mayor a 1 es porque el proyecto no es CLP
                        if (ViewState["ValorTipoValor"] != null)
                        {
                            lblVUV.Text = Util.Funcion.FormatoNumerico(NP.Valorizacion.ValorUnitario);
                            lblVTV.Text = Util.Funcion.FormatoNumerico(TotalValorizacion);
                        }
                        else
                        {
                            lblVUV.Text = Util.Funcion.FormatoNumericoEntero(NP.Valorizacion.ValorUnitario);
                            lblVTV.Text = Util.Funcion.FormatoNumericoEntero(TotalValorizacion);
                        }

                        if (NP.Ajuste.Cantidad != NP.Valorizacion.Cantidad)
                            lblCantV.Text += Asterisco;

                        if (NP.Ajuste.Descripcion != NP.Valorizacion.Descripcion)
                            lblDescV.Text += Asterisco;

                        if (NP.Creacion.ValorUnitario != NP.Valorizacion.ValorUnitario)
                            lblVUV.Text += Asterisco;

                        if (TotalCreacion != TotalValorizacion)
                            lblVTV.Text += Asterisco;
                    }
                }
            }

            if (r.RowType == DataControlRowType.Footer)
            {
                short IDProyecto = Convert.ToInt16(Request.QueryString["id"].ToString());
                double ValorTotalProyecto = ProyectoBL.GetValorTotalProyecto(IDProyecto);
                Label lblVTP = (r.Controls[0].FindControl("lblVTP") as Label);
                HtmlGenericControl totalProyecto = (r.Controls[0].FindControl("totalProyecto") as HtmlGenericControl);

                //oculta controles restringidos
                totalProyecto.Visible = false;

                lblVTP.Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumerico(ValorTotalProyecto));
            }
        }

        protected void gvResumenNP_DataBound(object sender, EventArgs e)
        {
            App_Code.Seguridad.GetControlGridviewAutorizado(Page, Request.Path, gvResumenNP.ID);
        }
        #endregion

        #region Informe de trazabalidad de NP
        protected void gvTrazaC_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.NotaPedidoEntity NP = (Entities.NotaPedidoEntity)r.DataItem;

                Label lblCant = (r.Controls[0].FindControl("lblCant") as Label);
                Label lblUnidad = (r.Controls[0].FindControl("lblUnidad") as Label);
                Label lblValorUnitario = (r.Controls[0].FindControl("lblValorUnitarioCreacion") as Label);
                Label lblDesc = (r.Controls[0].FindControl("lblDesc") as Label);
                Label lblFecha = (r.Controls[0].FindControl("lblFecha") as Label);
                Label lblUsuario = (r.Controls[0].FindControl("lblUsuario") as Label);

                if (Session["NP"] == null)
                {
                    Session.Add("NP", NP); //Guarda NP para compararla con la siguiente NP

                    lblCant.Text = Util.Funcion.FormatoNumerico(NP.Creacion.Cantidad);
                    lblUnidad.Text = NP.Creacion.TipoUnidad.Sigla;
                    lblValorUnitario.Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumerico(NP.Creacion.ValorUnitario));
                    lblDesc.Text = NP.Creacion.Descripcion;
                    lblFecha.Text = string.Format("{0:dd/MM/yyyy HH:mm}", NP.Creacion.Fecha);
                    lblUsuario.Text = string.Format("{0} {1}", NP.Creacion.Usuario.Nombre, NP.Creacion.Usuario.ApellidoPaterno);
                }
                else
                {
                    Entities.NotaPedidoEntity NPAnterior = (Entities.NotaPedidoEntity)Session["NP"];
                    string Asterisco = "<i class='fa fa-asterisk text-danger' style='margin-left:5px' aria-hidden='true'></i>";

                    //Verifica qué campos variaron según la versión anterior
                    if (NPAnterior.Creacion.Cantidad != NP.Creacion.Cantidad)
                        lblCant.Text = Util.Funcion.FormatoNumerico(NP.Creacion.Cantidad) + Asterisco;
                    else
                        lblCant.Text = Util.Funcion.FormatoNumerico(NP.Creacion.Cantidad);

                    if (NPAnterior.Creacion.TipoUnidad.Sigla != NP.Creacion.TipoUnidad.Sigla)
                        lblUnidad.Text = NP.Creacion.TipoUnidad.Sigla + Asterisco;
                    else
                        lblUnidad.Text = NP.Creacion.TipoUnidad.Sigla;

                    if (NPAnterior.Creacion.ValorUnitario != NP.Creacion.ValorUnitario)
                        lblValorUnitario.Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumerico(NP.Creacion.ValorUnitario) + Asterisco);
                    else
                        lblValorUnitario.Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumerico(NP.Creacion.ValorUnitario));

                    if (NPAnterior.Creacion.Descripcion != NP.Creacion.Descripcion)
                        lblDesc.Text = NP.Creacion.Descripcion + Asterisco;
                    else
                        lblDesc.Text = NP.Creacion.Descripcion;

                    string UsuarioAnterior = string.Format("{0} {1}", NPAnterior.Creacion.Usuario.Nombre, NPAnterior.Creacion.Usuario.ApellidoPaterno);
                    string Usuario = string.Format("{0} {1}", NP.Creacion.Usuario.Nombre, NP.Creacion.Usuario.ApellidoPaterno);

                    if (UsuarioAnterior != Usuario)
                        lblUsuario.Text = Usuario + Asterisco;
                    else
                        lblUsuario.Text = Usuario;

                    lblFecha.Text = string.Format("{0:dd/MM/yyyy HH:mm}", NP.Creacion.Fecha);

                    Session["NP"] = NP; //actualiza NP para luego ser comparada con la siguiente
                }

                (r.Controls[0].FindControl("lblValorTotalCreacion") as Label).Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumerico(Math.Round(NP.Creacion.Cantidad * NP.Creacion.ValorUnitario, 0)));
            }
        }

        protected void gvTrazaA_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.NotaPedidoEntity NP = (Entities.NotaPedidoEntity)r.DataItem;

                Label lblCant = (r.Controls[0].FindControl("lblCant") as Label);
                Label lblUnidad = (r.Controls[0].FindControl("lblUnidad") as Label);
                //Label lblValorUnitario = (r.Controls[0].FindControl("lblValorUnitarioAjuste") as Label);
                Label lblDesc = (r.Controls[0].FindControl("lblDesc") as Label);
                Label lblFecha = (r.Controls[0].FindControl("lblFecha") as Label);
                Label lblUsuario = (r.Controls[0].FindControl("lblUsuario") as Label);

                Entities.NotaPedidoEntity NPAnterior = (Entities.NotaPedidoEntity)Session["NP"];
                string Asterisco = "<i class='fa fa-asterisk text-danger' style='margin-left:5px' aria-hidden='true'></i>";

                //Verifica qué campos variaron según la versión anterior
                if (NPAnterior.Creacion.Cantidad != NP.Ajuste.Cantidad)
                    lblCant.Text = Util.Funcion.FormatoNumerico(NP.Ajuste.Cantidad) + Asterisco;
                else
                    lblCant.Text = Util.Funcion.FormatoNumerico(NP.Ajuste.Cantidad);

                lblUnidad.Text = NP.Creacion.TipoUnidad.Sigla; //Tipo de unidad nunca variará en el Ajuste
                //lblValorUnitario.Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumerico(NP.Creacion.ValorUnitario)); //Valor unitario nunca variará en el Ajuste

                if (NPAnterior.Creacion.Descripcion != NP.Ajuste.Descripcion)
                    lblDesc.Text = NP.Ajuste.Descripcion + Asterisco;
                else
                    lblDesc.Text = NP.Ajuste.Descripcion;

                string UsuarioAnterior = string.Format("{0} {1}", NPAnterior.Creacion.Usuario.Nombre, NPAnterior.Creacion.Usuario.ApellidoPaterno);
                string Usuario = string.Format("{0} {1}", NP.Ajuste.Usuario.Nombre, NP.Ajuste.Usuario.ApellidoPaterno);

                if (UsuarioAnterior != Usuario)
                    lblUsuario.Text = Usuario + Asterisco;
                else
                    lblUsuario.Text = Usuario;

                lblFecha.Text = string.Format("{0:dd/MM/yyyy HH:mm}", NP.Ajuste.Fecha);

                Session["NP"] = NP; //actualiza NP para luego ser comparada con la siguiente
            }
        }

        protected void gvTrazaV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.NotaPedidoEntity NP = (Entities.NotaPedidoEntity)r.DataItem;

                Label lblCant = (r.Controls[0].FindControl("lblCant") as Label);
                Label lblUnidad = (r.Controls[0].FindControl("lblUnidad") as Label);
                Label lblValorUnitario = (r.Controls[0].FindControl("lblValorUnitarioValorizacion") as Label);
                Label lblDesc = (r.Controls[0].FindControl("lblDesc") as Label);
                Label lblFecha = (r.Controls[0].FindControl("lblFecha") as Label);
                Label lblUsuario = (r.Controls[0].FindControl("lblUsuario") as Label);

                Entities.NotaPedidoEntity NPAnterior = (Entities.NotaPedidoEntity)Session["NP"];
                string Asterisco = "<i class='fa fa-asterisk text-danger' style='margin-left:5px' aria-hidden='true'></i>";

                //Verifica qué campos variaron según la versión anterior
                if (NPAnterior.Ajuste.Cantidad != NP.Valorizacion.Cantidad)
                    lblCant.Text = Util.Funcion.FormatoNumerico(NP.Valorizacion.Cantidad) + Asterisco;
                else
                    lblCant.Text = Util.Funcion.FormatoNumerico(NP.Valorizacion.Cantidad);

                lblUnidad.Text = NP.Creacion.TipoUnidad.Sigla;

                if (NPAnterior.Creacion.ValorUnitario != NP.Valorizacion.ValorUnitario)
                    lblValorUnitario.Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumerico(NP.Valorizacion.ValorUnitario) + Asterisco);
                else
                    lblValorUnitario.Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumerico(NP.Valorizacion.ValorUnitario));

                if (NPAnterior.Ajuste.Descripcion != NP.Valorizacion.Descripcion)
                    lblDesc.Text = NP.Valorizacion.Descripcion + Asterisco;
                else
                    lblDesc.Text = NP.Valorizacion.Descripcion;

                string UsuarioAnterior = string.Format("{0} {1}", NPAnterior.Ajuste.Usuario.Nombre, NPAnterior.Ajuste.Usuario.ApellidoPaterno);
                string Usuario = string.Format("{0} {1}", NP.Valorizacion.Usuario.Nombre, NP.Valorizacion.Usuario.ApellidoPaterno);

                if (UsuarioAnterior != Usuario)
                    lblUsuario.Text = Usuario + Asterisco;
                else
                    lblUsuario.Text = Usuario;

                (r.Controls[0].FindControl("lblValorTotalValorizacion") as Label).Text = string.Format("{0} {1}", hfTipoMoneda.Value, Util.Funcion.FormatoNumerico(Math.Round(NP.Valorizacion.Cantidad * NP.Valorizacion.ValorUnitario, 0)));

                lblFecha.Text = string.Format("{0:dd/MM/yyyy HH:mm}", NP.Valorizacion.Fecha);
            }
        }

        protected void gvTrazaC_DataBound(object sender, EventArgs e)
        {
            App_Code.Seguridad.GetControlGridviewAutorizado(Page, Request.Path, gvTrazaC.ID);
        }

        protected void gvTrazaA_DataBound(object sender, EventArgs e)
        {
            if (gvTrazaC.Columns[3].Visible)
            {
                gvTrazaA.Columns[3].Visible = true;
                gvTrazaA.Columns[4].Visible = true;
            }
            else
            {
                gvTrazaA.Columns[3].Visible = false;
                gvTrazaA.Columns[4].Visible = false;
            }
        }

        protected void gvTrazaV_DataBound(object sender, EventArgs e)
        {
            App_Code.Seguridad.GetControlGridviewAutorizado(Page, Request.Path, gvTrazaV.ID);
        }
        #endregion

        #region Gridview NP eventos
        protected void gvNP_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.NotaPedidoEntity NP = (Entities.NotaPedidoEntity)r.DataItem;

                HyperLink tituloNP = (r.Controls[0].FindControl("tituloNP") as HyperLink);
                tituloNP.Text = NP.IDCompuesta;

                Label lblColor = (r.Controls[0].FindControl("lblColor") as Label);
                HtmlGenericControl InicioDescripcion = (r.Controls[0].FindControl("InicioDescripcion") as HtmlGenericControl);

                //obtiene items de ddl tipos de valor
                ListItem[] items = new ListItem[ddlTipoValor1.Items.Count];
                ddlTipoValor1.Items.CopyTo(items, 0);

                if (NP.Estado) //verifica si la NP está eliminada
                {
                    //asigna url del div
                    string IDEtiquetaNP = (r.Controls[0].FindControl("np") as HtmlGenericControl).ClientID;
                    tituloNP.Attributes.Add("data-parent", string.Format("#{0}", accordion.ClientID));
                    tituloNP.NavigateUrl = string.Format("#{0}", IDEtiquetaNP);
                    tituloNP.Attributes["aria-controls"] = IDEtiquetaNP;
                    tituloNP.Text += string.Format(" de {0}", ViewState["TotalNPValidas"].ToString());

                   //Asigna valores dependiendo del estado en el que está la NP
                   if (NP.IDEstadoNP >= 1)
                   {
                       DropDownList ddlTipoValor1 = (r.Controls[0].FindControl("ddlTipoValor1") as DropDownList);
                       TextBox txtVUC = (r.Controls[0].FindControl("txtVUC") as TextBox);
                       DropDownList ddlTipoValor2 = (r.Controls[0].FindControl("ddlTipoValor2") as DropDownList);
                       TextBox txtVTC = (r.Controls[0].FindControl("txtVTC") as TextBox);
                       DropDownList ddlTipoValor3 = (r.Controls[0].FindControl("ddlTipoValor3") as DropDownList);
                       TextBox txtVPC = (r.Controls[0].FindControl("txtVPC") as TextBox);

                       //Valores NP creación
                       (r.Controls[0].FindControl("txtCantidadC") as TextBox).Text = Util.Funcion.FormatoNumerico(NP.Creacion.Cantidad);

                       //verifica si la NP está valorizada o facturada
                       //decimal ValorTotalProyectoCreacion = Convert.ToDecimal(hfValorTotalProyecto.Value);

                       //if (NP.IDEstadoNP >= 3)
                       //{
                       //    //Al valor total del proyecto se le resta la valorización de cada NP según corresponda, con el fin de que en la ventana "Creación" el Total proyecto sea correcto
                       //    ValorTotalProyectoCreacion -= (NP.Valorizacion.Cantidad * NP.Valorizacion.ValorUnitario); //Resta valorización
                       //    ValorTotalProyectoCreacion += (NP.Creacion.Cantidad * NP.Creacion.ValorUnitario); //Suma el valor de la creación
                       //}

                       string ValorTotalProyectoCreacion = hfValorTotalProyecto.Value.ToString();

                       //si tiene 1 item, es CLP
                       if (ddlTipoValor1.Items.Count == 1)
                       {
                           txtVUC.Text = Util.Funcion.FormatoNumericoEntero(NP.Creacion.ValorUnitario);
                           txtVTC.Text = Util.Funcion.FormatoNumericoEntero(NP.Creacion.Cantidad * NP.Creacion.ValorUnitario);
                           txtVPC.Text = Util.Funcion.FormatoNumericoEntero(ValorTotalProyectoCreacion);
                       }
                       else
                       {
                           txtVUC.Text = Util.Funcion.FormatoNumerico(NP.Creacion.ValorUnitario);
                           txtVTC.Text = Util.Funcion.FormatoNumerico(NP.Creacion.Cantidad * NP.Creacion.ValorUnitario);
                           txtVPC.Text = Util.Funcion.FormatoNumerico(ValorTotalProyectoCreacion);
                       }

                       //copia items tipos de valor
                       ddlTipoValor1.Items.AddRange(items);
                       ddlTipoValor2.Items.AddRange(items);
                       ddlTipoValor3.Items.AddRange(items);
                       ddlTipoValor1.DataBind();
                       ddlTipoValor2.DataBind();
                       ddlTipoValor3.DataBind();

                       //Agrega atributos de datos (textbox y valor) para generar cálculo de onchange en ddlTipoValor
                       ddlTipoValor1.Attributes.Add("txt", txtVUC.ClientID);
                       ddlTipoValor1.Attributes.Add("valor", NP.Creacion.ValorUnitario.ToString());
                       ddlTipoValor2.Attributes.Add("txt", txtVTC.ClientID);
                       ddlTipoValor2.Attributes.Add("valor", (NP.Creacion.Cantidad * NP.Creacion.ValorUnitario).ToString());
                       ddlTipoValor3.Attributes.Add("txt", txtVPC.ClientID);
                       ddlTipoValor3.Attributes.Add("valor", hfValorTotalProyecto.Value);

                       //Agrega función cliente onchange para calcular el valor del proyecto si se cambia el tipo de valor
                       string VinculaDropDownList = string.Format(" dtvgvNPCreacion{0}", r.RowIndex);
                       ddlTipoValor1.CssClass += (VinculaDropDownList);
                       ddlTipoValor2.CssClass += (VinculaDropDownList);
                       ddlTipoValor3.CssClass += (VinculaDropDownList);

                       if (NP.Creacion.FechaUltimaModificacion != NP.Creacion.Fecha)
                       {
                           //Muestra el control, asigna fecha de modificación y muestra asterisco
                           (r.Controls[0].FindControl("ultimaModificacionC") as HtmlGenericControl).Visible = true;
                           (r.Controls[0].FindControl("asteriscoC") as HtmlGenericControl).Visible = true;
                       }

                       if (NP.IDEstadoNP == 1)
                       {
                           //Oculta panel de valorización
                           (r.Controls[0].FindControl("contenidoValorizacion") as HtmlGenericControl).InnerHtml = string.Empty;
                           (r.Controls[0].FindControl("contenidoValorizacion") as HtmlGenericControl).Attributes.Add("style", "height:565px");

                           lblColor.CssClass = "label label-danger"; //Creada
                           InicioDescripcion.InnerText = NP.Creacion.Descripcion;
                       }
                   }

                   //Controles de panel de valorización
                   DropDownList ddlTipoValor4 = (r.Controls[0].FindControl("ddlTipoValor4") as DropDownList);
                   TextBox txtVUV = (r.Controls[0].FindControl("txtVUV") as TextBox);
                   DropDownList ddlTipoValor5 = (r.Controls[0].FindControl("ddlTipoValor5") as DropDownList);
                   TextBox txtVTV = (r.Controls[0].FindControl("txtVTV") as TextBox);
                   DropDownList ddlTipoValor6 = (r.Controls[0].FindControl("ddlTipoValor6") as DropDownList);
                   TextBox txtVPV = (r.Controls[0].FindControl("txtVPV") as TextBox);

                    if (NP.IDEstadoNP >= 2)
                    {
                        //Llena el ajuste con los valores ingresados
                        (r.Controls[0].FindControl("usuarioA") as HtmlGenericControl).Visible = true;
                        (r.Controls[0].FindControl("txtUsuarioA") as TextBox).Text = string.Format("{0} {1}", NP.Ajuste.Usuario.Nombre, NP.Ajuste.Usuario.ApellidoPaterno);
                        (r.Controls[0].FindControl("txtCantidadA") as TextBox).Text = Util.Funcion.FormatoNumerico(NP.Ajuste.Cantidad);
                        (r.Controls[0].FindControl("txtDescripcionA") as TextBox).Text = NP.Ajuste.Descripcion;
                        (r.Controls[0].FindControl("txtFechaRealEntregaA") as TextBox).Text = NP.Ajuste.FechaRealEntrega.ToShortDateString();

                        //Regulariza string para quitar caracteres \n, \r, etc... con el fin de sólo comprar contenido
                        string DescCreacion = Regex.Replace(NP.Creacion.Descripcion, @"\s+", string.Empty);
                        string DescAjuste = Regex.Replace(NP.Ajuste.Descripcion, @"\s+", string.Empty);

                        //marca con asterisco si la cantidad y descripción es distinta entre la creación y el ajuste
                        if (NP.Creacion.Cantidad != NP.Ajuste.Cantidad || !string.Equals(DescCreacion, DescAjuste, StringComparison.OrdinalIgnoreCase))
                            (r.Controls[0].FindControl("asteriscoA") as HtmlGenericControl).Visible = true;

                        if (NP.Creacion.Cantidad != NP.Ajuste.Cantidad)
                            (r.Controls[0].FindControl("asteriscoAC") as HtmlGenericControl).Visible = true;

                        if (!string.Equals(DescCreacion, DescAjuste, StringComparison.OrdinalIgnoreCase))
                            (r.Controls[0].FindControl("asteriscoAD") as HtmlGenericControl).Visible = true;

                        //copia items tipos de valor
                        ddlTipoValor4.Items.AddRange(items);
                        ddlTipoValor5.Items.AddRange(items);
                        ddlTipoValor6.Items.AddRange(items);
                        ddlTipoValor4.DataBind();
                        ddlTipoValor5.DataBind();
                        ddlTipoValor6.DataBind();

                        if (NP.IDEstadoNP == 2)
                        {
                            lblColor.CssClass = "label label-warning"; //Ajustada
                            InicioDescripcion.InnerText = NP.Ajuste.Descripcion;

                            //habilita formulario de valorización
                            (r.Controls[0].FindControl("txtFechaV") as TextBox).Text = DateTime.Now.ToShortDateString(); //Establece fecha de hoy

                            //verifica si el tipo de valor corresponde
                            int CantDecimal = ddlTipoValor1.Items.Count > 1 ? 2 : 0;

                            //crea función javascript autocálculo al agregar la Cantidad y Valor Unitario a los textboxs de la valorización
                            string ArmaFuncionValorTotalNP = string.Format("ValorTotalValorizarNP('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')", (r.Controls[0].FindControl("txtCantidadV") as TextBox).ClientID, txtVUV.ClientID, txtVTV.ClientID, txtVPV.ClientID, (r.Controls[0].FindControl("txtVTC") as TextBox).ClientID, CantDecimal);

                            //asigna función de autocálculo
                            (r.Controls[0].FindControl("txtCantidadV") as TextBox).Attributes.Add("onblur", ArmaFuncionValorTotalNP);
                            txtVUV.Attributes.Add("onblur", ArmaFuncionValorTotalNP);

                            //asigna función de formato numérico
                            (r.Controls[0].FindControl("txtCantidadV") as TextBox).Attributes.Add("onkeyup", "FormatoNumerico(this, 1)"); //según requerimiento usuario puede ingresar 1 decimal en la Cantidad
                            txtVUV.Attributes.Add("onkeyup", string.Format("FormatoNumerico(this, {0})", CantDecimal)); //usuario ingresa valor unitario valorización según tipo de valor del proyecto

                            //al valorizar el usuario no puede cambiar el tipo de valor
                            ddlTipoValor4.Enabled = false;
                            ddlTipoValor5.Enabled = false;
                            ddlTipoValor6.Enabled = false;
                        }
                    }

                    if (NP.IDEstadoNP >= 3)
                    {
                        //Agrega atributos de datos (textbox y valor) para generar cálculo de onchange en ddlTipoValor
                        ddlTipoValor4.Attributes.Add("txt", txtVUV.ClientID);
                        ddlTipoValor4.Attributes.Add("valor", NP.Valorizacion.ValorUnitario.ToString());
                        ddlTipoValor5.Attributes.Add("txt", txtVTV.ClientID);
                        ddlTipoValor5.Attributes.Add("valor", (NP.Valorizacion.Cantidad * NP.Valorizacion.ValorUnitario).ToString());
                        ddlTipoValor6.Attributes.Add("txt", txtVPV.ClientID);
                        ddlTipoValor6.Attributes.Add("valor", hfValorTotalProyecto.Value);

                        //Agrega función cliente onchange para calcular el valor del proyecto si se cambia el tipo de valor
                        string VinculaDropDownList = string.Format(" dtvgvNPValorizacion{0}", r.RowIndex);
                        ddlTipoValor4.CssClass += (VinculaDropDownList);
                        ddlTipoValor5.CssClass += (VinculaDropDownList);
                        ddlTipoValor6.CssClass += (VinculaDropDownList);

                        InicioDescripcion.InnerText = NP.Valorizacion.Descripcion;

                        //Llena la valorización con los valores ingresados
                        (r.Controls[0].FindControl("usuarioV") as HtmlGenericControl).Visible = true;
                        (r.Controls[0].FindControl("txtUsuarioV") as TextBox).Text = string.Format("{0} {1}", NP.Valorizacion.Usuario.Nombre, NP.Valorizacion.Usuario.ApellidoPaterno);
                        (r.Controls[0].FindControl("txtCantidadV") as TextBox).Text = Util.Funcion.FormatoNumerico(NP.Valorizacion.Cantidad);
                        (r.Controls[0].FindControl("txtDescripcionV") as TextBox).Text = NP.Valorizacion.Descripcion;
                        (r.Controls[0].FindControl("fechaV") as HtmlGenericControl).Visible = true;
                        (r.Controls[0].FindControl("txtFechaV") as TextBox).Text = NP.Valorizacion.Fecha.ToShortDateString();

                        //Regulariza string para quitar caracteres \n, \r, etc... con el fin de sólo comprar contenido
                        string DescAjuste = Regex.Replace(NP.Ajuste.Descripcion, @"\s+", string.Empty);
                        string DescValorizacion = Regex.Replace(NP.Valorizacion.Descripcion, @"\s+", string.Empty);

                        //marca con asterisco si la cantidad y descripción es distinta entre el ajuste y la valorización
                        if (NP.Ajuste.Cantidad != NP.Valorizacion.Cantidad || !string.Equals(DescAjuste, DescValorizacion, StringComparison.OrdinalIgnoreCase) || NP.Creacion.ValorUnitario != NP.Valorizacion.ValorUnitario)
                            (r.Controls[0].FindControl("asteriscoV") as HtmlGenericControl).Visible = true;

                        if (NP.Ajuste.Cantidad != NP.Valorizacion.Cantidad)
                            (r.Controls[0].FindControl("asteriscoVC") as HtmlGenericControl).Visible = true;

                        if (!string.Equals(DescAjuste, DescValorizacion, StringComparison.OrdinalIgnoreCase))
                            (r.Controls[0].FindControl("asteriscoVD") as HtmlGenericControl).Visible = true;

                        if (NP.Creacion.ValorUnitario != NP.Valorizacion.ValorUnitario)
                            (r.Controls[0].FindControl("asteriscoVU") as HtmlGenericControl).Visible = true;

                        //si tiene 1 item, es CLP
                        if (ddlTipoValor1.Items.Count == 1)
                        {
                            txtVUV.Text = Util.Funcion.FormatoNumericoEntero(NP.Valorizacion.ValorUnitario);
                            txtVTV.Text = Util.Funcion.FormatoNumericoEntero(NP.Valorizacion.Cantidad * NP.Valorizacion.ValorUnitario);
                            txtVPV.Text = Util.Funcion.FormatoNumericoEntero(hfValorTotalProyecto.Value);
                        }
                        else
                        {
                            txtVUV.Text = Util.Funcion.FormatoNumerico(NP.Valorizacion.ValorUnitario);
                            txtVTV.Text = Util.Funcion.FormatoNumerico(NP.Valorizacion.Cantidad * NP.Valorizacion.ValorUnitario);
                            txtVPV.Text = Util.Funcion.FormatoNumerico(hfValorTotalProyecto.Value);
                        }
                        
                        //Cierra la NP
                        if (NP.IDEstadoNP == 3)
                            lblColor.CssClass = "label label-success"; //Valorizada
                    }

                    if (NP.IDEstadoNP >= 4)
                        lblColor.CssClass = "label label-primary"; //Facturada
                    
                }
                else //NP Eliminada
                {
                    (r.Controls[0].FindControl("rowNP") as HtmlGenericControl).Attributes["class"] = "panel panel-danger";
                    (r.Controls[0].FindControl("opcion") as HtmlGenericControl).Visible = false;
                    lblColor.CssClass = string.Empty;
                    lblColor.Text = "<i class=\"fa fa-trash\" style=\"margin-left: 4px;margin-right: 4px;\"></i>";
                    InicioDescripcion.InnerText = NP.Creacion.Descripcion;
                }
            }
        }

        protected void gvNP_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.CommandArgument.ToString()))
            {
                int rowIndex = Convert.ToInt32(e.CommandArgument);
                GridViewRow r = gvNP.Rows[rowIndex];
                int IDNP = Convert.ToInt16(gvNP.DataKeys[rowIndex].Value);
                string IDCompuestaNP = (r.FindControl("tituloNP") as HyperLink).Text.Split(' ')[0];
                int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);

                switch (e.CommandName)
                {
                    case "Trazabilidad":
                        tituloTrazado.InnerText = string.Format("Trazado NP {0}", IDCompuestaNP);

                        List<Entities.NotaPedidoEntity> Lst = NotaPedidoBL.GetTrazabilidadNP(IDNP);

                        gvTrazaC.DataSource = Lst.Where(x => x.IDEstadoNP == 1); //Filtra la creación y sus ediciones
                        gvTrazaC.DataBind();

                        gvTrazaC.UseAccessibleHeader = true;
                        gvTrazaC.HeaderRow.TableSection = TableRowSection.TableHeader;

                        List<Entities.NotaPedidoEntity> LstAjuste = Lst.Where(x => x.IDEstadoNP == 2).ToList(); //Filtra ajuste

                        valorizacionTrazabilidad.Visible = false;
                        ajusteTrazabilidad.Visible = false;

                        if (LstAjuste.Count > 0)
                        {
                            gvTrazaA.DataSource = LstAjuste;
                            gvTrazaA.DataBind();

                            gvTrazaA.UseAccessibleHeader = true;
                            gvTrazaA.HeaderRow.TableSection = TableRowSection.TableHeader;
                            ajusteTrazabilidad.Visible = true;

                            List<Entities.NotaPedidoEntity> LstValorizacion = Lst.Where(x => x.IDEstadoNP == 3).ToList(); //Filtra valorización

                            if (LstValorizacion.Count > 0)
                            {
                                gvTrazaV.DataSource = LstValorizacion;
                                gvTrazaV.DataBind();

                                gvTrazaV.UseAccessibleHeader = true;
                                gvTrazaV.HeaderRow.TableSection = TableRowSection.TableHeader;
                                valorizacionTrazabilidad.Visible = true;
                            }
                        }


                        ScriptManager.RegisterStartupScript(this, GetType(), "TrazabilidadNP", "$('#" + trazabilidadNP.ClientID + "').modal({backdrop: 'static',keyboard: true});", true);
                        break;
                }

                Session.Remove("NP"); //Destruye NP almacenada
                BindDataNP(IDProyecto);
            }
        }

        protected void gvNP_DataBound(object sender, EventArgs e)
        {
            App_Code.Seguridad.GetControlGridviewAutorizado(Page, Request.Path, gvNP.ID);
        }

        private void BindDataNP(int IDProyecto)
        {
            try
            {
                List<Entities.NotaPedidoEntity> Lst = NotaPedidoBL.GetNP(IDProyecto);
                string CantidadNPValida = Lst.Where(x => x.Estado == true).ToList().Count.ToString();

                //obtiene número total de NP válidas del proyecto
                ViewState.Add("TotalNPValidas", CantidadNPValida);

                gvNP.DataKeyNames = new string[1] { "ID" }; //PK
                gvNP.DataSource = Lst;
                
                //sumatoria total valor proyecto
                double ValorTotalProyecto = ProyectoBL.GetValorTotalProyecto(IDProyecto);
                hfValorTotalProyecto.Value = ValorTotalProyecto.ToString();

                lblContNP.Text = CantidadNPValida;

                gvNP.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }
        #endregion

        private void BindOCProyecto(DropDownList ddlOC)
        {
            List<Entities.DocumentoComercialEntity> Lst = ProyectoBL.GetOCProyecto(Convert.ToInt16(hfIdProyecto.Value));
            ddlOC.DataSource = Lst;
            ddlOC.DataBind();
        }

        private void BindTipoUnidad(DropDownList ddlTipoUnidad)
        {
            ddlTipoUnidad.DataSource = TipoUnidadBL.GetTipoUnidad(0, string.Empty, string.Empty, false);
            ddlTipoUnidad.DataBind();
        }
    }
}
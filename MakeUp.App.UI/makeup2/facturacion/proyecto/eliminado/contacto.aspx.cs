﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.proyecto.eliminado
{
    public partial class contacto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    CargaInicial(Convert.ToInt16(Request.QueryString["id"].ToString()));
                }
                else
                {
                    Page.Title = "Proyecto no encontrado...";
                }
            }
        }

        private void CargaInicial(int IDProyecto)
        {
            Entities.ProyectoEntity Proyecto = ProyectoBL.GetCabeceraProyectoEliminado(IDProyecto);

            if (Proyecto != null)
            {
                //verifica si el usuario puedes ver contactos compartidos
                int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
                Entities.UsuarioEntity Usuario = UsuarioBL.GetUsuario(IDUsuario, string.Empty, string.Empty, string.Empty, null, false)[0];
                bool PuedeVerContactosCompartidos = Usuario.VisualizaTodoContacto == true ? true : false; //Iguala a true porque "Usuario.VisualizaTodoContacto" puede recibir valor null

                //Obtiene lista de contactos
                List<Entities.ClienteEntity.ContactoEntity> Lst = ContactoBL.GetContactoProyecto(IDProyecto, Proyecto.Cliente.ID);

                //filtra si puede visualizar todos los contactos
                if (!PuedeVerContactosCompartidos)
                    Lst = Lst.Where(x => x.ContactoCompartido == false).ToList();

                gvContacto.DataKeyNames = new string[1] { "ID" }; //PK
                gvContacto.DataSource = Lst;
                gvContacto.DataBind();

                if (Lst.Count > 0)
                {
                    gvContacto.UseAccessibleHeader = true;
                    gvContacto.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
        }

        protected void gvContacto_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int IDProyecto = Convert.ToInt16(Convert.ToInt16(Request.QueryString["id"].ToString()));
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
            if (e.CommandName == "GuardarSeleccion")
            {
                foreach (GridViewRow row in gvContacto.Rows)
                    if (row.RowType == DataControlRowType.DataRow)
                        ContactoBL.SetVincularContactoProyecto(IDProyecto, Convert.ToInt16(gvContacto.DataKeys[row.RowIndex].Value.ToString()), IDUsuario, (row.Cells[0].FindControl("cbContacto") as CheckBox).Checked);
            }
        }
    }
}
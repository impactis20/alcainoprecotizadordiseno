﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.proyecto.eliminado
{
    public partial class estado_de_pago : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    CargaInicial(Convert.ToInt16(Request.QueryString["id"].ToString()));
                }
                else
                {
                    Page.Title = "Proyecto no encontrado...";
                }
            }
        }

        private void CargaInicial(int IDProyecto)
        {
            Entities.ProyectoEntity Proyecto = ProyectoBL.GetCabeceraProyectoEliminado(IDProyecto);

            if (Proyecto != null)
            {
                hfIdProyecto.Value = IDProyecto.ToString();

                double SaldoPorFacturar = ProyectoBL.GetSaldoPorFacturarProyecto(IDProyecto);
                double ValorTotalProyecto = ProyectoBL.GetValorTotalProyecto(IDProyecto);
                double ValorFacturado = ValorTotalProyecto - SaldoPorFacturar;

                txtValorProyecto.Text = Util.Funcion.FormatoNumerico(ValorTotalProyecto);
                txtSaldoPorFacturar.Text = Util.Funcion.FormatoNumerico(SaldoPorFacturar);
                txtValorFacturado.Text = Util.Funcion.FormatoNumerico(ValorFacturado);

                hfValorTipoValor.Value = string.Format("{0}|{1}", Proyecto.TipoValor.Sigla, Util.Funcion.FormatoNumerico(Proyecto.TipoValor.Valor));

                //Obtiene estados de pago
                BindDataEstadoPago(IDProyecto);

                //Agrega tipo de valor del proyecto
                visualizar.notas_de_pedido.AddFunctionValorTotalProyectoCLP(ref ddlTipoValor1, Proyecto.TipoValor.Sigla, Proyecto.TipoValor.Valor);
                visualizar.notas_de_pedido.AddFunctionValorTotalProyectoCLP(ref ddlTipoValor2, Proyecto.TipoValor.Sigla, Proyecto.TipoValor.Valor);
                visualizar.notas_de_pedido.AddFunctionValorTotalProyectoCLP(ref ddlTipoValor3, Proyecto.TipoValor.Sigla, Proyecto.TipoValor.Valor);

                if (Proyecto.TipoValor.Valor > 1)
                {
                    txtValorProyecto.Text = Util.Funcion.FormatoNumerico(ValorTotalProyecto);
                    txtSaldoPorFacturar.Text = Util.Funcion.FormatoNumerico(SaldoPorFacturar);
                    txtValorFacturado.Text = Util.Funcion.FormatoNumerico(ValorFacturado);

                    //Agrega atributos de datos (textbox y valor) para generar cálculo de onchange en ddlTipoValor
                    ddlTipoValor1.Attributes.Add("txt", txtSaldoPorFacturar.ClientID);
                    ddlTipoValor1.Attributes.Add("valor", SaldoPorFacturar.ToString());
                    ddlTipoValor2.Attributes.Add("txt", txtValorFacturado.ClientID);
                    ddlTipoValor2.Attributes.Add("valor", ValorFacturado.ToString());
                    ddlTipoValor3.Attributes.Add("txt", txtValorProyecto.ClientID);
                    ddlTipoValor3.Attributes.Add("valor", ValorTotalProyecto.ToString());
                }
                else {

                    txtValorProyecto.Text = Util.Funcion.FormatoNumericoEntero(ValorTotalProyecto);
                    txtSaldoPorFacturar.Text = Util.Funcion.FormatoNumericoEntero(SaldoPorFacturar);
                    txtValorFacturado.Text = Util.Funcion.FormatoNumericoEntero(ValorFacturado);
                }
            }
        }

        protected void gvEstadoPago_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.EstadoPagoEntity EP = (Entities.EstadoPagoEntity)r.DataItem;
                Label lblPorcentaje = (r.Controls[0].FindControl("lblPorcentaje") as Label);
                Label lblMonto = (r.Controls[0].FindControl("lblMonto") as Label);
                Label lblGlosa = (r.Controls[0].FindControl("lblGlosa") as Label);
                Label lblFechaPreFacturado = (r.Controls[0].FindControl("lblFechaPreFacturado") as Label);
                Label lblFechaCreacion = (r.Controls[0].FindControl("lblFechaCreacion") as Label);

                lblPorcentaje.Text = Util.Funcion.FormatoNumerico(EP.Porcentaje) + "%";

                if (Convert.ToDouble(hfValorTipoValor.Value.Split('|')[1]) > 1)
                    lblMonto.Text = string.Format("{0} {1}", hfValorTipoValor.Value.Split('|')[0], Util.Funcion.FormatoNumerico(EP.Monto));
                else
                    lblMonto.Text = string.Format("{0} {1}", hfValorTipoValor.Value.Split('|')[0], Util.Funcion.FormatoNumericoEntero(EP.Monto));

                lblGlosa.Text = EP.Glosa;
                lblFechaCreacion.Text = string.Format("{0:dd-MM-yyyy HH:mm}", EP.FechaCreacion);

                if (EP.FacturaEmitida)
                    lblFechaPreFacturado.Text = string.Format("{0:dd-MM-yyyy HH:mm}", EP.FechaPreFacturado);
                else
                    lblFechaPreFacturado.Text = "-";
            }
        }

        private void BindDataEstadoPago(int IDProyecto)
        {
            List<Entities.EstadoPagoEntity> Lst = EstadoPagoBL.GetEstadoPago(IDProyecto);
            gvEstadoPago.DataSource = Lst;
            gvEstadoPago.DataKeyNames = new string[1] { "ID" }; //PK
            gvEstadoPago.DataBind();

            if (Lst.Count > 0)
            {
                gvEstadoPago.UseAccessibleHeader = true;
                gvEstadoPago.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
    }
}
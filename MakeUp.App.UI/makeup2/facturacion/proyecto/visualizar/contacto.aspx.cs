﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.SignalR;
using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.proyecto.visualizar
{
    public partial class contacto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    CargaInicial(Convert.ToInt16(Request.QueryString["id"].ToString()));
                }
                else
                {
                    Page.Title = "Proyecto no encontrado...";
                }
            }
        }

        protected void btnAgregarContacto_ServerClick(object sender, EventArgs e)
        {
            int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);
            int IDCliente = Convert.ToInt16(hfIdCliente.Value);
            string Nombre = txtNombreContacto.Text.Trim();
            string Email = txtEmail.Text.Trim();
            string DescContacto = txtDescripcionContacto.Text.Trim();
            string Telefono = txtTelefono.Text.Trim();
            string Movil = txtMovil.Text.Trim();

            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
            //falta validación de usuario (ver contactos de comercial o contactos de producción)
            //se está enviando un 1 como tipo de contacto. El ejemplo asume que el usuario con ID 1 es comercial
            //agrega contacto a cliente
            int IDNotificacionApp = 0;
            ContactoBL.AddContacto(IDProyecto, IDCliente, IDUsuario, Nombre, Email, DescContacto, Telefono, Movil, cbCompartirContacto.Checked, ref IDNotificacionApp);

            //NOTIFICACIÓN(6): Notifica a los usuarios que se agregó un contacto a un cliente
            hubs.NotificacionAppHub.EnviaNotificacionesUsuariosConectados(GlobalHost.ConnectionManager.GetHubContext<hubs.NotificacionAppHub>());

            //limpia formulario
            txtNombreContacto.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtDescripcionContacto.Text = string.Empty;
            txtTelefono.Text = string.Empty;
            txtMovil.Text = string.Empty;
            cbCompartirContacto.Checked = false;

            //actualiza gridview de contactos
            BindDataContato(IDCliente);
        }

        protected void gvContacto_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
            if (e.CommandName == "GuardarSeleccion")
            {
                foreach (GridViewRow row in gvContacto.Rows)
                    if (row.RowType == DataControlRowType.DataRow)
                        ContactoBL.SetVincularContactoProyecto(IDProyecto, Convert.ToInt16(gvContacto.DataKeys[row.RowIndex].Value.ToString()), IDUsuario, (row.Cells[0].FindControl("cbContacto") as CheckBox).Checked);
            }
        }

        protected void gvContacto_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //cambia estado en la BD
            int IDContacto = Convert.ToInt16(e.Keys["ID"].ToString());
            ContactoBL.DelContacto(IDContacto);

            //actualiza gridview de contactos
            BindDataContato(Convert.ToInt16(hfIdCliente.Value));
        }

        private void BindDataContato(int IDCliente)
        {
            int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);

            //verifica si el usuario puedes ver contactos compartidos
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
            Entities.UsuarioEntity Usuario = UsuarioBL.GetUsuario(IDUsuario, string.Empty, string.Empty, string.Empty, null, false)[0];
            bool PuedeVerContactosCompartidos = Usuario.VisualizaTodoContacto == true ? true : false; //Iguala a true porque "Usuario.VisualizaTodoContacto" puede recibir valor null

            //Obtiene lista de contactos
            List<Entities.ClienteEntity.ContactoEntity> Lst = ContactoBL.GetContactoProyecto(IDProyecto, IDCliente);

            //filtra si puede visualizar todos los contactos
            if (!PuedeVerContactosCompartidos)
            {
                Lst = Lst.Where(x => x.ContactoCompartido == false).ToList();
                compartirContacto.Visible = false; //oculta opción de compartir contacto 
            }
            else
                compartirContacto.Visible = true; //muestra opción de compartir contacto 

            gvContacto.DataKeyNames = new string[1] { "ID" }; //PK
            gvContacto.DataSource = Lst;
            gvContacto.DataBind();

            if (Lst.Count > 0)
            {
                gvContacto.UseAccessibleHeader = true;
                gvContacto.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        private void CargaInicial(int IDProyecto)
        {
            Entities.ProyectoEntity Proyecto = ProyectoBL.GetCabeceraProyecto(IDProyecto);

            if (Proyecto != null)
            {
                hfIdProyecto.Value = IDProyecto.ToString();
                hfIdCliente.Value = Proyecto.Cliente.ID.ToString();
                hfNombreFantasiaCliente.Value = Proyecto.Cliente.NombreFantasia;

                BindDataContato(Proyecto.Cliente.ID);
            }
        }

        protected void gvContacto_RowEditing(object sender, GridViewEditEventArgs e)
        {
            int IDProyecto = Convert.ToInt16(Request.QueryString["id"].ToString());
            int IDCliente = Convert.ToInt16(hfIdCliente.Value);

            gvContacto.EditIndex = e.NewEditIndex;
            //Obtiene lista de contactos
            BindDataContato(IDCliente);
        }

        protected void gvContacto_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow r = gvContacto.Rows[e.RowIndex];
            int IDProyecto = Convert.ToInt16(Request.QueryString["id"].ToString());
            int IDCliente = Convert.ToInt16(hfIdCliente.Value);

            try
            {
                ContactoBL.SetContacto(Convert.ToInt16(e.Keys["ID"]), (r.FindControl("txtNombreContacto") as TextBox).Text, (r.FindControl("txtEmail") as TextBox).Text, (r.FindControl("txtDesc") as TextBox).Text, (r.FindControl("txtTelefono") as TextBox).Text, (r.FindControl("txtMovil") as TextBox).Text, (r.FindControl("cbTipo") as CheckBox).Checked);
                gvContacto.EditIndex = -1;
                BindDataContato(IDCliente);

                ScriptManager.RegisterStartupScript(this, GetType(), "Pop2", Util.GetMensajeAlerta("success", "Contacto actualizado...", "El contacto ha sido actualizado."), true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop2", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        protected void gvContacto_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvContacto.EditIndex = -1;
            CargaInicial(Convert.ToInt16(Request.QueryString["id"].ToString()));
        }
    }
}
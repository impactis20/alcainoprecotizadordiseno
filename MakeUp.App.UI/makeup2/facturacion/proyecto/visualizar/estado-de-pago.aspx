﻿<%@ Page Title="Estados de pago del proyecto" Language="C#" MasterPageFile="~/makeup2/proyecto/visualizar/proyecto.master" AutoEventWireup="true" CodeBehind="estado-de-pago.aspx.cs" Inherits="MakeUp.App.UI.makeup2.proyecto.visualizar.estado_de_pago" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headProyecto" runat="server">
    <link href="../../../css/bootstrap-select.css" rel="stylesheet" />
    <link href="../../../css/personalizado.css" rel="stylesheet" />
    <link href="../../../css/bootstrap-datetimepicker.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenidoProyecto" runat="server">
     <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-datetimepicker.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/languages/es.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/count-character-textbox.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/number-format-using-numeral.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/dropdown-igualitario.js") %>'></script>

    <script type="text/javascript">
        $(function () {
            $('[id*=dFechaFacturacion]').datetimepicker({
                useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                format: 'DD-MM-YYYY',
                maxDate: moment().add(1, 'days'), //permite hasta la fecha de hoy
                showClear: true,
                ignoreReadonly: true
            });

            $('[id*=dFechaVencimiento]').datetimepicker({
                useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                format: 'DD-MM-YYYY',
                minDate: 'now',
                showClear: true,
                ignoreReadonly: true
            });

            $('[id*=dFechaOC]').datetimepicker({
                useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                format: 'DD-MM-YYYY',
                maxDate: 'now',
                showClear: true,
                ignoreReadonly: true
            });

            $('[id*=dFechaHes]').datetimepicker({
                useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                format: 'DD-MM-YYYY',
                maxDate: 'now',
                showClear: true,
                ignoreReadonly: true
            });

            $('[id*=dFechaFacturacion]').on("dp.change", function (e) {
                var fechaSeleccionada = e.date.format('DD-MM-YYYY');
                var urlValorUF = '<%= ResolveUrl(string.Format("{0}/GetValorUF", Request.Url.AbsolutePath)) %>';
                $.ajax({
                    url: urlValorUF,
                    data: "{ 'date': '" + fechaSeleccionada + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        numeral.language('es'); //asigna idioma a numeral
                        var valorUFDia = data.d;
                        var tipoMoneda = 'CLP ';
                        var valorFacturacion = parseFloat(ConvertToNumber($('#<%= hfValorFacturacion.ClientID %>').val())); //valor en UF
                        $('#<%= lblValorDia.ClientID %>').text(tipoMoneda + numeral(valorUFDia).format('0,0.[00]'));
                        $('#<%= txtTotalFacturacion.ClientID %>').val(numeral(valorUFDia * valorFacturacion).format('0,0')); //conversión a CLP no lleva decimales (los decimales los aproxima)
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            });
         });

        //calcula el monto del EP en base a un porcentaje
        function CalculaMontoEP(Porcentaje) {
            if (Porcentaje !== '') {
                var txtPorcentaje = document.getElementById("<%= txtPorcentaje.ClientID %>");
                var txtMonto = document.getElementById("<%= txtMonto.ClientID %>");
                var ValorTotalProyecto = parseFloat(ConvertToNumber('<%= hfValorTotalProyecto.Value %>'));

                Porcentaje = parseFloat(ConvertToNumber(Porcentaje));

                if (Porcentaje > 100 || Porcentaje === 0) {
                    alerta('Porcentaje inválido...', 'El porcentaje no puede exceder el máximo permitido (100%), tampoco puede ser igual a 0.', 'text-danger');
                    txtPorcentaje.value = "";
                    txtMonto.value = "";
                    return;
                }
                else {
                    numeral.language('es');
                    var ValorMonto = ValorTotalProyecto * Porcentaje / 100;
                    txtMonto.value = numeral(ValorMonto).format('0,0' + getCantidadDecimal()); //[00] dos decimales
                    document.getElementById('<%= hfMonto.ClientID %>').value = ValorMonto; //almacena monto
                    document.getElementById('<%= hfPorcentaje.ClientID %>').value = Porcentaje; //almacena monto
                }
            }
        }

        //calcula el porcentaje del EP en base a un monto
        function CalculaPorcentajeEP(Monto) {
            if (Monto !== '') {
                var txtPorcentaje = document.getElementById("<%= txtPorcentaje.ClientID %>");
                var txtMonto = document.getElementById("<%= txtMonto.ClientID %>");
                var ValorTotalProyecto = parseFloat(ConvertToNumber('<%= hfValorTotalProyecto.Value %>'));

                Monto = parseFloat(ConvertToNumber(Monto));

                if (Monto > ValorTotalProyecto || Monto === 0) {
                    alerta('Monto inválido...', 'El monto no puede exceder el Saldo del proyecto, tampoco puede ser igual a 0.', 'text-danger');
                    txtPorcentaje.value = "";
                    txtMonto.value = "";
                    return;
                }
                else {
                    numeral.language('es');
                    var ValorPorcentaje = Monto / ValorTotalProyecto * 100;



                    txtPorcentaje.value = numeral(ValorPorcentaje).format('0,0.[00]'); //porcentaje debe basarse en 2 decimales
                    document.getElementById('<%= hfMonto.ClientID %>').value = Monto; //almacena monto
                    document.getElementById('<%= hfPorcentaje.ClientID %>').value = ValorPorcentaje; //almacena porcentaje
                }
            }
        }

        function getCantidadDecimal() {
            var valorTipoValor = parseInt(document.getElementById('<%= hfValorTipoValor.ClientID %>').value);
            if (valorTipoValor > 1)
                return '.[00]'; //[00] dos decimales

            return '';
        }
    </script>
    <script type="text/javascript">
        //function pageLoad(sender, args) {

        //Habilita y deshabilita textbox        
        $(function () {
            $("input[type=radio][name*=TipoEP]").change(function () {
                if (this.value == 'rbPorcentaje') {
                    $('[id*=txtPorcentaje]').removeAttr('disabled');
                    $('[id*=txtMonto]').attr('disabled', 'disabled');

                        //$('[id*=<%= txtPorcentaje.ClientID %>]').focus();
                        document.getElementById("<%= txtPorcentaje.ClientID %>").focus();
                    }
                    else {
                        $('[id*=txtMonto]').removeAttr('disabled');
                        $('[id*=txtPorcentaje]').attr('disabled', 'disabled');

                        //$('[id*=<%= txtMonto.ClientID %>]').focus();
                        document.getElementById("<%= txtMonto.ClientID %>").focus();
                }

                $('[id*=txtPorcentaje]').val('');
                $('[id*=txtMonto]').val('');
            })
        });
        //}
    </script>
     <script type="text/javascript">
         $(document).ready(function () {
             $('[data-toggle="popover"]').popover();
         });
            </script>

            <script type="text/javascript">
                //Funcion que sirve para cerrar popover desde el boton "x"
                $(document).on("click", ".popover .close", function () {
                    $(this).parents(".popover").popover('hide');
                });
            </script>

            <style>
                .radio-ff > input {
                    margin-top:0px;
                    margin-right:24px
                }
                .radio-ff > label {
                    margin:0px;
                    cursor:pointer
                }

                .hand-point{
                    cursor:pointer
                }

                .ff {
                    display: inline-block;
                    white-space: nowrap;
                    overflow: hidden;
                    text-overflow: ellipsis;     /** IE6+, Firefox 7+, Opera 11+, Chrome, Safari **/
                    -o-text-overflow: ellipsis;  /** Opera 9 & 10 **/
                    width: 200px; /* note that this width will have to be smaller to see the effect */
                }
            </style>

    <asp:HiddenField ID="hfIdProyecto" runat="server" />
    <asp:HiddenField ID="hfValorTotalProyecto" runat="server" />
    <asp:HiddenField ID="hfPorcentaje" runat="server" />
    <asp:HiddenField ID="hfMonto" runat="server" />
    <asp:HiddenField ID="hfValorTipoValor" runat="server" />
    <asp:HiddenField ID="hfIdCliente" runat="server" />
    <asp:HiddenField ID="hfNombreProyecto" runat="server" />
    
    <div class="form-group">
        <p>Ingrese el monto o el porcentaje para generar un estado de pago. El cálculo se realiza en base al saldo del proyecto.</p>
        <p>
            <label class="label label-danger"><i class="fa fa-info-circle" aria-hidden="true"></i> Importante</label>
            los estados de pagos se aplican a todas las notas de pedidos creadas o ajustadas.
        </p>
        <div class="row">
            <div class="form-group">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group">
                                <asp:RadioButton ID="rbPorcentaje" runat="server" Text="&nbsp;Porcentaje" GroupName="TipoEP" Checked="true" />
                                <div class="input-group">
                                    <asp:TextBox ID="txtPorcentaje" runat="server" CssClass="form-control input-sm text-right" onblur="CalculaMontoEP(this.value)" ViewStateMode="Enabled" MaxLength="5" autocomplete="off" onkeyup="FormatoNumerico(this, 2);"></asp:TextBox>
                                    <span class="input-group-addon">%</span>
                                </div>
                                <asp:RequiredFieldValidator ID="rfvPorcentaje" runat="server" ControlToValidate="txtPorcentaje" ErrorMessage="Campo obligatorio..." SetFocusOnError="true" Display="Dynamic" CssClass="alert-danger" ValidationGroup="AgregarEstadoPago" />
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="form-group">
                                <asp:RadioButton ID="rbMonto" runat="server" Text="&nbsp;Monto" GroupName="TipoEP" />
                                <div class="input-group">
                                    <span class="input-group-addon"><asp:Label ID="lblMonedaSeleccionadaEP" runat="server"></asp:Label></span>
                                    <asp:TextBox ID="txtMonto" runat="server" CssClass="form-control input-sm valortotal-format" onblur="CalculaPorcentajeEP(this.value)" ViewStateMode="Enabled" Enabled="false" autocomplete="off"></asp:TextBox>
                                </div>
                                <asp:RequiredFieldValidator ID="rfvMonto" runat="server" ControlToValidate="txtMonto" ErrorMessage="Campo obligatorio..." SetFocusOnError="true" Display="Dynamic" CssClass="alert-danger" ValidationGroup="AgregarEstadoPago" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-10">
                            <div class="form-group">
                                <label>* Glosa Estado de Pago</label>
                                <asp:TextBox ID="txtGlosa" runat="server" placeholder="Ingrese texto..." CssClass="form-control input-sm" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvGlosa" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtGlosa" SetFocusOnError="true" Display="Dynamic" CssClass="alert-danger" ValidationGroup="AgregarEstadoPago" />
                                <asp:Label ID="lblContG" runat="server" CssClass="help-block pull-right"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-10">
                            <div class="form-group">
                                <div class="pull-right">
                                    <asp:Button ID="btnAgregarEstadoPago" runat="server" Text="Agregar" CssClass="btn btn-default" OnClick="btnAgregarEstadoPago_Click" rel="tooltip" data-toggle="tooltip" data-placement="top" title="Agregar estado de pago" ValidationGroup="AgregarEstadoPago" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-6">
                    <div class="col-lg-12">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-6 control-label">Valor Proyecto</label>
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <asp:DropDownList id="ddlTipoValor3" runat="server" CssClass="selectpicker dtv1" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:TextBox ID="txtValorProyecto" runat="server" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">Valor Facturado</label>
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <asp:DropDownList id="ddlTipoValor2" runat="server" CssClass="selectpicker dtv1" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:TextBox ID="txtValorFacturado" runat="server" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">Saldo</label>
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <asp:DropDownList id="ddlTipoValor1" runat="server" CssClass="selectpicker dtv1" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:TextBox ID="txtSaldoPorFacturar" runat="server" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ConfirmaEliminarEP" tabindex="-1" role="dialog" aria-labelledby="xxxx">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Confirmar Eliminar Estado de Pago</h2>
                </div>
                <div class="modal-body">
                    <p>¿Está seguro de eliminar El estado de pago que representa el <asp:Label ID="lblEstadoPagoEliminar" runat="server"></asp:Label>% del proyecto?</p>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right">
                                <button id="btnConfirmarEliminarEP" runat="server" class="btn btn-primary" type="button" onserverclick="btnConfirmarEliminarEP_ServerClick">
                                    <span class="fa fa-remove"></span>&nbsp;&nbsp;Confirmar Eliminación
                                </button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="table-responsive">
            <asp:GridView ID="gvEstadoPago" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None"
                OnRowDataBound="gvEstadoPago_RowDataBound" OnRowDeleting="gvEstadoPago_RowDeleting" OnRowCommand="gvEstadoPago_RowCommand">
                <Columns>
                    <asp:TemplateField HeaderText="#" HeaderStyle-Width="28px">
                        <ItemTemplate>
                            <asp:Label ID="lblN" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Porcentaje" HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Label ID="lblPorcentaje" runat="server"></asp:Label>
                            <asp:HiddenField ID="hfPorcentaje" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Monto" HeaderStyle-Width="128px">
                        <ItemTemplate>
                            <asp:Label ID="lblMonto" runat="server"></asp:Label>
                            <asp:HiddenField ID="hfMonto" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Glosa Estado de Pago">
                        <ItemTemplate>
                            <asp:Label ID="lblGlosa" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Fecha Creación" HeaderStyle-Width="140px">
                        <ItemTemplate>
                            <asp:Label ID="lblFechaCreacion" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Facturación MU" HeaderStyle-Width="140px">
                        <ItemTemplate>
                            <asp:Label ID="lblFechaPreFacturado" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="40px">
                        <ItemTemplate>
                        <asp:Button ID="btnFacturar" runat="server" Text="Facturar" CssClass="btn btn-primary btn-xs" CommandName="Facturar" visible="false"/>
<%--                            <asp:Button ID="btnFacturar" runat="server" Text="Facturar" CssClass="btn btn-primary btn-xs" Visible="false" CommandName="FacturarEP" CommandArgument='<%# Container.DataItemIndex %>' />--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="40px">
                        <ItemTemplate>
                            <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" CssClass="btn btn-danger btn-xs" CommandName="Delete" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>

        <div id="facturar" runat="server" class="modal fade facturacion" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>Genere facturación de Estado de Pago...</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Está seguro(a) que desea realizar la facturación de estado de pago correspondiente al <b id="porcentaje" runat="server"></b> del proyecto <b id="nombreProyecto" runat="server"></b>?. A continuación, revise el siguiente detalle de facturación:</p>
                        <div class="row">
                            <div class="col-lg-6">
                                <label>Estado de pago a facturar</label>
                                <br />
                                <asp:Label ID="lblEPxFacturar" runat="server"></asp:Label>
                                <asp:HiddenField ID="hfValorFacturacion" runat="server" />
                            </div>
                        </div>
                        <br />
                        <p>Para terminar la facturación, agregue una<asp:Label ID="lblInfoFechaFacturacion" runat="server" Text=" fecha de facturación," Visible="false"></asp:Label> condición de pago y glosa (obligatorio).</p>

                        <div id="calendarioFechaFacturacion" runat="server" class="row" visible="false">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>* Fecha de facturación</label>
                                    <div class="input-group date input-group" id="dFechaFacturacion">
                                        <asp:TextBox ID="txtFechaFacturacion" runat="server" CssClass="form-control input-sm disabled" autocomplete="off"></asp:TextBox>
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>
                                    <asp:RequiredFieldValidator ID="rfvFechaFacturacion" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtFechaFacturacion" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" />
                                    <asp:Label ID="lblValorDivisa" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Valor día</label>
                                    <br />
                                    <asp:Label ID="lblValorDia" runat="server" Text="CLP 0"></asp:Label>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>Total a facturar</label>
                                    <br />
                                    <div class="input-group">
                                        <span class="input-group-addon">CLP</span>
                                        <asp:TextBox ID="txtTotalFacturacion" runat="server" Text="0" CssClass="form-control input-sm" onkeyup="FormatoNumerico(this, 0);"></asp:TextBox>
                                    </div>
                                    <asp:HiddenField ID="HiddenField1" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Condición de pago:</label><br />
                                    <%--<asp:TextBox ID="txtCondicionPago" runat="server" CssClass="form-control input-sm" MaxLength="200" autocomplete="off"></asp:TextBox>--%>
                                    <asp:DropDownList ID="ddlCondicionPago" runat="server" CssClass="selectpicker" DataValueField="ID" DataTextField="sigla" data-width="auto">
                                        <asp:ListItem Text="Contado" Value="Contado"></asp:ListItem>
                                        <asp:ListItem Text="Crédito 7 días" Value="Credito 7 dias"></asp:ListItem>
                                        <asp:ListItem Text="Crédito 15 días" Value="Credito 15 dias"></asp:ListItem>
                                        <asp:ListItem Text="Crédito 30 días" Value="Credito 30 dias" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                                    <%--<asp:RequiredFieldValidator ID="rfvCondicionPago" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtCondicionPago" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="ConfirmaFacturacion" />--%>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>* Fecha de vencimiento:</label>
                                    <div class="input-group date input-group" id="dFechaVencimiento">
                                        <asp:TextBox ID="txtFechaVencimiento" runat="server" CssClass="form-control input-sm disabled" autocomplete="off"></asp:TextBox>
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>
                                    <asp:RequiredFieldValidator ID="rfvFechaVencimiento" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtFechaVencimiento" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="ConfirmaFacturacion" />
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>N° de OC.:</label><br />
                                    <asp:TextBox ID="txtNroOC" runat="server" CssClass="form-control input-sm" MaxLength="30" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Fecha OC.:</label><br />
                                    <div class="input-group date input-group" id="dFechaOC">
                                        <asp:TextBox ID="txtFechaOC" runat="server" CssClass="form-control input-sm disabled" autocomplete="off"></asp:TextBox>
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>N° de HES.:</label><br />
                                    <asp:TextBox ID="txtNroHes" runat="server" CssClass="form-control input-sm" MaxLength="30" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Fecha HES.:</label><br />
                                    <div class="input-group date input-group" id="dFechaHes">
                                        <asp:TextBox ID="txtFechaHes" runat="server" CssClass="form-control input-sm disabled" autocomplete="off"></asp:TextBox>
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>* Glosa Estado de Pago:</label><br />
                                    <asp:TextBox ID="txtGlosaFac" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3" MaxLength="80" autocomplete="off"></asp:TextBox>
                                    <asp:Label ID="lblGlosa" runat="server" CssClass="help-block pull-right"></asp:Label>
                                    <asp:RequiredFieldValidator ID="rfvGlosaFac" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtGlosaFac" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="ConfirmaFacturacion" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btnConfirmaFacturacion" runat="server" class="btn btn-primary" type="button" onserverclick="btnConfirmaFacturacion_ServerClick" validationgroup="ConfirmaFacturacion">
                            <span class="fa fa-check"></span>&nbsp;&nbsp;Aceptar
                        </button>
                        <button class="btn btn-default" type="button" data-dismiss="modal">
                            <span class="fa fa-remove"></span>&nbsp;&nbsp;Cancelar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
</asp:Content>
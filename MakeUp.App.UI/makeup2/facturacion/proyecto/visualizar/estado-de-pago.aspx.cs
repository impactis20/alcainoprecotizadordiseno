﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Services;
using System.Web.UI.WebControls;
using Microsoft.AspNet.SignalR;


using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.proyecto.visualizar
{
    public partial class estado_de_pago : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    CargaInicial(Convert.ToInt16(Request.QueryString["id"].ToString()));
                }
                else
                {
                    Page.Title = "Proyecto no encontrado...";
                }
            }
        }

        private void CargaInicial(int IDProyecto)
        {
            Entities.ProyectoEntity Proyecto = ProyectoBL.GetCabeceraProyecto(IDProyecto);

            if (Proyecto != null)
            {
                hfIdProyecto.Value = IDProyecto.ToString();

                double SaldoPorFacturar = ProyectoBL.GetSaldoPorFacturarProyecto(IDProyecto);
                double ValorTotalProyecto = ProyectoBL.GetValorTotalProyecto(IDProyecto);
                double ValorFacturado = ValorTotalProyecto - SaldoPorFacturar;

                hfValorTotalProyecto.Value = ValorTotalProyecto.ToString();

                lblMonedaSeleccionadaEP.Text = Proyecto.TipoValor.Sigla;

                //asigna valor del tipo de valor
                hfValorTipoValor.Value = Util.Funcion.FormatoNumerico(Proyecto.TipoValor.Valor);

                //Obtiene estados de pago
                BindDataEstadoPago(IDProyecto);

                //Agrega tipo de valor del proyecto
                notas_de_pedido.AddFunctionValorTotalProyectoCLP(ref ddlTipoValor1, Proyecto.TipoValor.Sigla, Proyecto.TipoValor.Valor);
                notas_de_pedido.AddFunctionValorTotalProyectoCLP(ref ddlTipoValor2, Proyecto.TipoValor.Sigla, Proyecto.TipoValor.Valor);
                notas_de_pedido.AddFunctionValorTotalProyectoCLP(ref ddlTipoValor3, Proyecto.TipoValor.Sigla, Proyecto.TipoValor.Valor);

                if (Proyecto.TipoValor.Valor > 1)
                {
                    txtValorProyecto.Text = Util.Funcion.FormatoNumerico(ValorTotalProyecto);
                    txtSaldoPorFacturar.Text = Util.Funcion.FormatoNumerico(SaldoPorFacturar);
                    txtValorFacturado.Text = Util.Funcion.FormatoNumerico(ValorFacturado);

                    //Agrega atributos de datos (textbox y valor) para generar cálculo de onchange en ddlTipoValor
                    ddlTipoValor1.Attributes.Add("txt", txtSaldoPorFacturar.ClientID);
                    ddlTipoValor1.Attributes.Add("valor", SaldoPorFacturar.ToString());
                    ddlTipoValor2.Attributes.Add("txt", txtValorFacturado.ClientID);
                    ddlTipoValor2.Attributes.Add("valor", ValorFacturado.ToString());
                    ddlTipoValor3.Attributes.Add("txt", txtValorProyecto.ClientID);
                    ddlTipoValor3.Attributes.Add("valor", ValorTotalProyecto.ToString());

                    //determina cuantos decimales se podrá ingresar en el monto del estado de pago
                    txtMonto.Attributes.Add("onkeyup", "FormatoNumerico(this, 2);");
                }
                else
                {

                    txtValorProyecto.Text = Util.Funcion.FormatoNumericoEntero(ValorTotalProyecto);
                    txtSaldoPorFacturar.Text = Util.Funcion.FormatoNumericoEntero(SaldoPorFacturar);
                    txtValorFacturado.Text = Util.Funcion.FormatoNumericoEntero(ValorFacturado);

                    //determina cuantos decimales se podrá ingresar en el monto del estado de pago
                    //tipo de valor CLP no permite decimales
                    txtMonto.Attributes.Add("onkeyup", "FormatoNumerico(this, 0);");
                }

                //asigna contador de caracteres
                txtGlosa.Attributes.Add("onkeyup", "countChar(this, '" + lblContG.ClientID + "', 250)");
            }
        }

        protected void btnAgregarEstadoPago_Click(object sender, EventArgs e)
        {
            int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);
            decimal Porcentaje = Convert.ToDecimal(hfPorcentaje.Value.Replace(".", ","));
            decimal Monto = Convert.ToDecimal(hfMonto.Value.Replace(".", ","));

            string Glosa = txtGlosa.Text.Trim();

            txtPorcentaje.Text = Util.Funcion.FormatoNumerico(Porcentaje);

            //si es mayor a 1, el proyecto no es CLP
            if (Convert.ToDouble(hfValorTipoValor.Value) > 1)
                txtMonto.Text = Util.Funcion.FormatoNumerico(Monto);
            else
                txtMonto.Text = Util.Funcion.FormatoNumericoEntero(Monto);

            rfvPorcentaje.IsValid = string.IsNullOrEmpty(txtPorcentaje.Text) ? false : true;
            rfvMonto.IsValid = string.IsNullOrEmpty(txtMonto.Text) ? false : true;

            if (Porcentaje > 100)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("warning", "Porcentaje de estado de pago excedido...", "Ha excedido el porcentaje máximo permitido (100%)."), true);
            }
            else if (Porcentaje == 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("warning", "Porcentaje de estado de pago inválido...", "El porcentaje de un Estado de Pago no puede ser 0%."), true);
            }
            else if (Monto == 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("warning", "Estado de pago inválido...", "No puede crear un Estado de Pago mientras el valor del proyecto sea 0."), true);
            }
            else
            {
                //Verifica si hay un EP sin facturar
                if (EstadoPagoBL.GetEstadoPago(IDProyecto).Where(x => x.FacturaEmitida == false).ToList().Count == 0)
                {
                    //cuando el tipo de valor es CLP, el monto debe aproximarse
                    //no es necesario redondear con dos decimales cuando el tipo de valor es usd, uf o euro, porque
                    //la variable monto en la bd es un numeric(11,2) (se redondea solo)
                    if (Convert.ToDouble(hfValorTipoValor.Value) == 1)
                        Monto = Math.Round(Monto, 0);

                    //Intenta agregar estado pago
                    int IDUsuarioCreador = App_Code.Seguridad.GetCurrentIDUSuario();

                    bool Resp = EstadoPagoBL.AddEstadoPago(IDProyecto, Porcentaje, Monto, Glosa, IDUsuarioCreador, DateTime.Now);

                    rfvMonto.IsValid = true;
                    rfvPorcentaje.IsValid = true;

                    //Cuando Resp es falso, indica que la sumatoria de estados de pagos excedio el valor total del proyecto
                    if (Resp)
                    {
                        //refresca gridview de estados de pago
                        BindDataEstadoPago(IDProyecto);

                        //limpia formulario
                        LimpiaFormularioEstadoPago();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("warning", "Valor total del proyecto excedido...", "La sumatoria de los estados de pagos (monto), no puede exceder el valor total del proyecto."), true);
                    }
                }
                else
                {
                    //limpia formulario
                    LimpiaFormularioEstadoPago();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("warning", "Estado de pago sin facturar...", "El proyecto no puede tener más de un Estado de Pago sin facturar."), true);
                }
            }

            //corrige qué textbox habilitar
            txtPorcentaje.Enabled = rbPorcentaje.Checked;
            txtMonto.Enabled = rbMonto.Checked;
        }

        private void LimpiaFormularioEstadoPago()
        {
            txtPorcentaje.Text = string.Empty;
            txtMonto.Text = string.Empty;
            txtGlosa.Text = string.Empty;
        }

        protected void btnConfirmarEliminarEP_ServerClick(object sender, EventArgs e)
        {
            int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);
            Entities.EstadoPagoEntity EPEliminar = (Entities.EstadoPagoEntity)ViewState["EPEliminar"];

            //Elimina viewState de EP a eliminar
            ViewState.Remove("EPEliminar");

            //Elimina lógicamente el estado de pago
            int Respuesta = EstadoPagoBL.DelEstadoPago(EPEliminar.ID);

            if (Respuesta == 1)
            {
                //El estado de pago fue eliminado
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("success", "Estado de Pago eliminado", "El Estado de Pago fue eliminado del proyecto."), true);
            }
            else
            {
                //El estado de pago no puede ser eliminado porque fue emitido a facturacion
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", "El Estado de Pago no puede ser eliminado porque fue emitido a facturación."), true);
            }

            BindDataEstadoPago(IDProyecto);

            //corrige qué textbox habilitar
            txtPorcentaje.Enabled = rbPorcentaje.Checked;
            txtMonto.Enabled = rbMonto.Checked;
        }

        protected void gvEstadoPago_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.EstadoPagoEntity EP = (Entities.EstadoPagoEntity)r.DataItem;
                Button btnEliminar = (r.Controls[0].FindControl("btnEliminar") as Button);
                Label lblPorcentaje = (r.Controls[0].FindControl("lblPorcentaje") as Label);
                Label lblMonto = (r.Controls[0].FindControl("lblMonto") as Label);
                Label lblGlosa = (r.Controls[0].FindControl("lblGlosa") as Label);
                Label lblFechaPreFacturado = (r.Controls[0].FindControl("lblFechaPreFacturado") as Label);
                Label lblFechaCreacion = (r.Controls[0].FindControl("lblFechaCreacion") as Label);
                Button btnFacturar = (r.Controls[0].FindControl("btnFacturar") as Button);

                lblPorcentaje.Text = Util.Funcion.FormatoNumerico(EP.Porcentaje) + "%";

                if (Convert.ToDouble(hfValorTipoValor.Value) > 1)
                    lblMonto.Text = string.Format("{0} {1}", lblMonedaSeleccionadaEP.Text, Util.Funcion.FormatoNumerico(EP.Monto));
                else
                    lblMonto.Text = string.Format("{0} {1}", lblMonedaSeleccionadaEP.Text, Util.Funcion.FormatoNumericoEntero(EP.Monto));

                lblGlosa.Text = EP.Glosa;
                lblFechaCreacion.Text = string.Format("{0:dd-MM-yyyy HH:mm}", EP.FechaCreacion);

                if (EP.FacturaEmitida)
                {
                    lblFechaPreFacturado.Text = string.Format("{0:dd-MM-yyyy HH:mm}", EP.FechaPreFacturado);
                }
                else
                {
                    lblFechaPreFacturado.Text = "-";
                    btnFacturar.Visible = true;
                }

                btnEliminar.Visible = !EP.FacturaEmitida;
            }
        }

        protected void gvEstadoPago_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow r = gvEstadoPago.Rows[e.RowIndex];

            Entities.EstadoPagoEntity EPEliminar = new Entities.EstadoPagoEntity();
            EPEliminar.ID = Convert.ToInt16(e.Keys["ID"].ToString());
            EPEliminar.Porcentaje = Convert.ToDecimal((r.Controls[0].FindControl("lblPorcentaje") as Label).Text.Replace("%", string.Empty));

            ViewState.Add("EPEliminar", EPEliminar);

            lblEstadoPagoEliminar.Text = Util.Funcion.FormatoNumerico(EPEliminar.Porcentaje);

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "$(function () { $('#ConfirmaEliminarEP').modal('show'); });", true);
        }

        private void BindDataEstadoPago(int IDProyecto)
        {
            List<Entities.EstadoPagoEntity> Lst = EstadoPagoBL.GetEstadoPago(IDProyecto);
            gvEstadoPago.DataSource = Lst;
            gvEstadoPago.DataKeyNames = new string[1] { "ID" }; //PK
            gvEstadoPago.DataBind();

            if (Lst.Count > 0)
            {
                gvEstadoPago.UseAccessibleHeader = true;
                gvEstadoPago.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gvEstadoPago_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //int rowIndex = Convert.ToInt32(e.CommandArgument);
            //int IDEP = Convert.ToInt16(gvEstadoPago.DataKeys[rowIndex].Value);
            //GridViewRow r = gvEstadoPago.Rows[rowIndex];

            int IDProyecto = Convert.ToInt16(hfIdProyecto.Value);
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();

            //Entities.ProyectoEntity Proyecto = ProyectoBL.GetCabeceraProyecto(IDProyecto);
            //hfIdCliente.Value = Proyecto.Cliente.ID.ToString();
            //hfNombreProyecto.Value = Proyecto.Nombre.ToString();

            if (e.CommandName == "Facturar")
            {
                Response.Redirect("~/makeup2/facturacion/estado-de-pago/detalle.aspx?id=" + IDProyecto);
            }

            //if (e.CommandName == "FacturarEP")
            //{

            //    Double IDTipoValor = Convert.ToDouble(hfValorTipoValor.Value);

            //    ViewState.Add("IDEPFacturar", IDEP);
            //    nombreProyecto.InnerText = "";
            //    porcentaje.InnerText = (r.FindControl("lblPorcentaje") as Label).Text;

            //    //obtiene EP actualizado (porcentaje, valor)
            //    Entities.EstadoPagoEntity EP = EstadoPagoBL.GetEstadoPago(IDProyecto).Where(x => x.ID == IDEP).ToList()[0];

            //    if (Convert.ToInt32(ViewState["ValorTipoValor"]) > 1)
            //        lblEPxFacturar.Text = string.Format("{0}% ({1} {2})", Util.Funcion.FormatoNumerico(EP.Porcentaje), lblMonedaSeleccionadaEP.Text, Util.Funcion.FormatoNumerico(EP.Monto));
            //    else
            //        lblEPxFacturar.Text = string.Format("{0}% ({1} {2})", Util.Funcion.FormatoNumerico(EP.Porcentaje), lblMonedaSeleccionadaEP.Text, Util.Funcion.FormatoNumerico(EP.Monto));

            //    hfValorFacturacion.Value = EP.Monto.ToString(); //almacena valor a facturar

            //    //2 es UF. Cuando es tipo de valor UF se muestra el calendario para seleccionar la fecha con valor a facturar
            //    if (IDTipoValor == 2)
            //    {
            //        lblInfoFechaFacturacion.Visible = true;
            //        calendarioFechaFacturacion.Visible = true;
            //        rfvFechaFacturacion.ValidationGroup = rfvFechaVencimiento.ValidationGroup;
            //        rfvFechaFacturacion.DataBind();
            //    }

            //    ScriptManager.RegisterStartupScript(this, GetType(), "Facturacion", "$('#" + facturar.ClientID + "').modal({backdrop: 'static',keyboard: true});", true);
            //}
        }

        protected void btnConfirmaFacturacion_ServerClick(object sender, EventArgs e)
        {
            int IDProyecto = Convert.ToInt16(Request.QueryString["id"].ToString());
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
            int IDEP = Convert.ToInt16(ViewState["IDEPFacturar"].ToString());
            int IDTipoValor = Convert.ToInt16(hfValorTipoValor.Value);
            int IDCliente = Convert.ToInt16(hfIdCliente.Value);
            string CondicionPago = ddlCondicionPago.SelectedItem.Value;
            DateTime FechaVencimiento = Convert.ToDateTime(txtFechaVencimiento.Text);
            string NroOC = txtNroOC.Text;
            DateTime FechaOC = string.IsNullOrEmpty(txtFechaOC.Text) ? DateTime.MinValue : Convert.ToDateTime(txtFechaOC.Text);
            string NroHes = txtNroHes.Text;
            DateTime FechaHes = string.IsNullOrEmpty(txtFechaHes.Text) ? DateTime.MinValue : Convert.ToDateTime(txtFechaHes.Text);
            string Glosa = txtGlosaFac.Text.Trim();
            decimal ValorTotalFactura = IDTipoValor == 2 ? Convert.ToDecimal(txtTotalFacturacion.Text) : Convert.ToDecimal(hfValorFacturacion.Value);

            int IDNotificacionApp = 0;

            //Respuesta: Devuelve 0 cuando el estado de pago ya fue facturado,
            //           Devuelve - 1 cuando el proyecto está en USD o Euro
            //           De lo contrario devuelve el nro de la factura que genera Manager
            int Respuesta = FacturacionBL.AddFacturacionxEP(IDProyecto, IDUsuario, IDCliente, IDEP, ValorTotalFactura, CondicionPago, FechaVencimiento, NroOC, FechaOC, NroHes, FechaHes, Glosa, ref IDNotificacionApp);

            if (Respuesta == 0)
            {
                //Otro usuario ya facturó el EP
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error al generar la factura...", "La factura del Estado de Pago ya fue generada."), true);
            }
            else
            {
                //NOTIFICACIÓN (4): Notifica a los usuarios que hay un Estado de pago facturado
                hubs.NotificacionAppHub.EnviaNotificacionesUsuariosConectados(GlobalHost.ConnectionManager.GetHubContext<hubs.NotificacionAppHub>());

                //La facturación se genero correctamente
                Entities.ProyectoEntity Proyecto = ProyectoBL.GetCabeceraProyecto(IDProyecto);

                string NombreProyecto = string.Format("{0} - {1}", IDProyecto, Proyecto.Nombre);
                string NombreUsuario = UsuarioBL.GetUsuario(IDUsuario, string.Empty, string.Empty, string.Empty, null, false)[0].Nombre;

                //notifica vía correo electrónico al usuario facturador de manager
                string UrlProyecto = string.Format("http://{0}/makeup2/proyecto/visualizar/datos.aspx?id={1}", Request.Url.Authority, IDProyecto);
                string Para = ConfigurationManager.AppSettings["emailsOrdenFacturacion"];
                string Asunto = string.Format("Orden de Facturación Proyecto: {0}", NombreProyecto);
                string Cuerpo = string.Format("<span style=\"color:#1f497d\">Estimada <b>Luisa Uribe</b>,<br><br>" +
                    "Mediante el presente le informamos a usted que con fecha <b>{0}</b> el ejecutivo comercial <b>{1}</b> ha dado orden de facturación al proyecto <b style=\"text-decoration:none\"><a href={2} target=\"_blank\">{3}</a></b> del cliente <b>{4}</b> por un monto de <b>$ {5}</b>.<br><br>" +
                    "</span>",
                    DateTime.Now.ToShortDateString(), //fecha de envío
                    NombreUsuario, //nombre de usuario
                    Util.GetComillas(UrlProyecto), //url del proyecto
                    NombreProyecto, //nombre del proyecto
                    Proyecto.Cliente.RazonSocial, //nombre de fantasía
                    Util.Funcion.FormatoNumericoEntero(ValorTotalFactura)); //valor de la factura

                //cuando el proyecto es CLP o UF se incluye el nro de la factura de Manager en el correo
                if (Respuesta != -1) //Respuesta tiene valor -1 cuando el proyecto es USD o Euro
                    Cuerpo += string.Format("<span style=\"color:#1f497d\">El correlativo asignado en Manager para esta factura es: <b>{0}</b></span>.", Respuesta);

                Cuerpo += "<span style=\"color:#1f497d\"><br><br>Si tiene dudas o consultas diríjase a su administrador.<br><br>Atte.<br>Sistema Make Up.</span>";

                //credenciales correo de envío MakeUp
                string Remitente = ConfigurationManager.AppSettings["remitenteMakeUp"];
                string De = ConfigurationManager.AppSettings["emailMakeUp"];
                string Ps = ConfigurationManager.AppSettings["passwordMakeUp"];

                string RespuestaWS = App_Code.WebService.Email.EnviarEmail(De, Ps, De, Para, string.Empty, string.Empty, Remitente, Asunto, Cuerpo);

                bool CorreoEnviado = Convert.ToBoolean(Convert.ToInt16(RespuestaWS.Split('.')[0]));

                string Mensaje = "Se generó correctamente la factura del Estado de Pago.";
                string IconoEmail = "<br><i class=\"fa fa-envelope-o\" aria-hidden=\"true\" style=\"margin-left: 30px; margin-right: 4px\">&nbsp;&nbsp;</i>";
                string EmailNotificacion = string.Format("{0}{1}", IconoEmail, Para.Replace(",", IconoEmail));

                if (CorreoEnviado)
                    Mensaje += string.Format("<br><br><i class=\"fa fa-check text-success\" aria-hidden=\"true\" style=\"margin-eft: 4px;margin-right: 4px\">&nbsp;&nbsp;</i> Notificación enviada correctamente a:{0}", EmailNotificacion);
                else
                    Mensaje += string.Format("<br><br><i class=\"fa fa-exclamation-triangle text-danger\" aria-hidden=\"true\" style=\"margin-eft: 4px;margin-right: 4px\">&nbsp;&nbsp;</i>Por un error interno de servicio la notificación de correo electrónico no pudo ser enviada a:{0}", EmailNotificacion);

                ScriptManager.RegisterStartupScript(this, GetType(), "Pop307", Util.GetMensajeAlerta("success", "Factura generada correctamente...", Mensaje), true);
            }

            //Limpia formulario
            ddlCondicionPago.SelectedIndex = -1;
            txtGlosaFac.Text = string.Empty;

            //actualiza gridview con estados de pago
            BindDataEstadoPago(IDProyecto);
        }

        [WebMethod]
        public static float GetValorUF(string date)
        {
            DateTime Fecha = Convert.ToDateTime(date);

            return TipoValorBL.GetValorTipoValor(2, Fecha);
        }
    }
}
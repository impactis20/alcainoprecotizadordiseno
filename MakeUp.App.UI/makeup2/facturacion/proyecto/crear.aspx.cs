﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.SignalR;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.proyecto
{
    public partial class crear : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PaneName.Value = Request.Form[PaneName.UniqueID];

                int IDProyecto = ProyectoBL.GetNuevoIDProyecto(); //Obtiene "posible" nuevo ID de proyecto
                titulo.InnerText = IDProyecto.ToString();
                txtIDProyecto.Text = IDProyecto.ToString();
                txtFechaIngreso.Text = DateTime.Today.ToShortDateString();

                BindTipoValor();
                BindTipoUnidad(ddlTipoUnidad);
                BindTipoNP(ddlTipoNP);

                //crea contador de caracteres
                txtDescripcionNP.Attributes.Add("onkeyup", "countChar(this, '" + lblContDescNP.ClientID + "', 500)");
                txtDescripcionDoc.Attributes.Add("onkeyup", "countChar(this, '" + lblContDescDoc.ClientID + "', 30)");
                txtDescripcionArchivo.Attributes.Add("onkeyup", "countChar(this, '" + lblContDA.ClientID + "', 200)");
                txtGlosa.Attributes.Add("onkeyup", "countChar(this, '" + lblContG.ClientID + "', 250)");

                ClearViewState();

                txtFechaPlanificada.Text = DateTime.Now.AddDays(1).ToShortDateString();
                txtFechaComprometida.Text = DateTime.Now.AddDays(1).ToShortDateString();
            }
        }

        private void ClearViewState()
        {
            ViewState.Remove("TotalPorcentajeEP");
        }

        protected void btnConfirmarProyecto_ServerClick(object sender, EventArgs e)
        {
            try
            {
                hfPadre.Value = txtPadre.Text == string.Empty ? string.Empty : hfPadre.Value; // validación necesaria para cuando el usuario confirma un proyecto y luego vuelve atrás en el navegador

                if (((!string.IsNullOrEmpty(txtPadre.Text) && !string.IsNullOrEmpty(hfPadre.Value)) || (string.IsNullOrEmpty(txtPadre.Text) && string.IsNullOrEmpty(hfPadre.Value))) && !string.IsNullOrEmpty(hfCliente.Value) && !string.IsNullOrEmpty(hfEjecutivo.Value))
                {
                    if (hfPadre.Value == string.Empty)
                    {
                        txtFechaPlanificada.Text = txtFechaComprometida.Text;
                        ScriptManager.RegisterStartupScript(this, GetType(), "ConfirmarCrearProyecto", "$('#" + confirmarCrearProyecto.ClientID + "').modal({backdrop: 'static',keyboard: true});", true);
                    }
                    else
                        ConfirmarProyecto();
                }
                else
                {
                    if (string.IsNullOrEmpty(hfCliente.Value)) rfvCliente.IsValid = false;
                    if (string.IsNullOrEmpty(hfEjecutivo.Value)) rfvEjecutivo.IsValid = false;
                    if (!string.IsNullOrEmpty(txtPadre.Text) && string.IsNullOrEmpty(hfPadre.Value)) lblPadre.Visible = true;
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        protected void btnReconfirmarProyecto_Click(object sender, EventArgs e)
        {
            try
            {
                ConfirmarProyecto();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        private void ConfirmarProyecto()
        {


            int IDProyecto = Convert.ToInt16(txtIDProyecto.Text);
            string Nombre = txtNombre.Text.Trim();
            DateTime FechaComprometida = Convert.ToDateTime(txtFechaComprometida.Text);
            //int IDTipoValor = Convert.ToInt16(ddlTipoValor.SelectedItem != null ? ddlTipoValor.SelectedItem.Value : string.Empty);
            string TipoValor = ddlTipoValor.SelectedItem != null ? ddlTipoValor.SelectedItem.Value : string.Empty;
            int IDTipoValor = Convert.ToInt16(TipoValor);
            string Direccion = txtDireccion.Text.Trim();
            string Comuna = txtComuna.Text.Trim();
            lblPadre.Visible = false;
            int IDCliente = Convert.ToInt16(hfCliente.Value);
            int IDUsuarioCreador = App_Code.Seguridad.GetCurrentIDUSuario();
            int IDEjecutivo = Convert.ToInt16(hfEjecutivo.Value);
            string ServidorArchivosProyecto = ConfigurationManager.AppSettings["servidorArchivosProyecto"].ToString();
            string CarpetaCompartidaServidorArchivosProyecto = ConfigurationManager.AppSettings["carpetaCompartidaServidorArchivosProyecto"].ToString();
            string Directorio = string.Format(@"\\{0}\{1}\id_proyecto\documentos comerciales", ServidorArchivosProyecto, CarpetaCompartidaServidorArchivosProyecto); //el directorio se almacena mediante un update luego de tener su ID real
            int IDRealProyecto = ProyectoBL.AddProyecto(hfPadre.Value, IDCliente, IDUsuarioCreador, IDEjecutivo, IDTipoValor, Nombre, DateTime.Now, FechaComprometida, Direccion, Comuna, Directorio);

            //obtiene credenciales para ingresar a la carpeta compartida
            string UsuarioCarpetaCompartidaServidorArchivosProyecto = ConfigurationManager.AppSettings["usuarioCarpetaCompartidaServidorArchivosProyecto"].ToString();
            string PasswordCarpetaCompartidaServidorArchivosProyecto = ConfigurationManager.AppSettings["passwordCarpetaCompartidaServidorArchivosProyecto"].ToString();

            using (App_Code.Seguridad.NetworkShareAccesser.Acceso(ServidorArchivosProyecto, UsuarioCarpetaCompartidaServidorArchivosProyecto, PasswordCarpetaCompartidaServidorArchivosProyecto))
            {
                Directory.CreateDirectory(Directorio.Replace("id_proyecto", IDRealProyecto.ToString())); //crea directorio con ID real del proyecto
            }

            btnConfirmarProyecto.Visible = false;
            datosTotalProyecto.Visible = true;

            //Habilita paneles y pestaña contactos del cliente
            DocComercial.Visible = true;
            DocProyecto.Visible = true;
            tabContacto.Visible = true;

            //almacena ID real del proyecto
            hfIDProyecto.Value = IDRealProyecto.ToString();
            titulo.InnerText = string.Format("{0} - {1}", IDRealProyecto.ToString(), Nombre);
            txtIDProyecto.Text = IDRealProyecto.ToString();

            if (IDProyecto != IDRealProyecto)
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("warning", "Confirmación de proyecto...", string.Format("Para su información, el <b>ID de proyecto</b> desplegado inicialmente ha variado debedido a que fue asignado a otro proyecto. La causa se genera por una creación de proyecto paralela.<br><br>El ID real asignado a este proyecto es <b>{0}</b>.", IDRealProyecto)), true);

            //Establece tipo de moneda
            lblMonedaSeleccionada.Text = ddlTipoValor.SelectedItem.Text;
            lblMonedaSeleccionadaEP.Text = ddlTipoValor.SelectedItem.Text;

            //asigna función de cálculo al agregar una NP en textbox's cantidad y valor unitario
            string ArmaFuncionValorTotalNP = string.Format("ValorTotalNP('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')", txtCantidad.ClientID, txtValorUnitario.ClientID, txtValorTotalNP.ClientID, ddlTipoValor2.ClientID, txtValorProyecto1.ClientID, ddlTipoValor3.ClientID, false);
            txtCantidad.Attributes.Add("onblur", ArmaFuncionValorTotalNP);
            txtValorUnitario.Attributes.Add("onblur", ArmaFuncionValorTotalNP);

            decimal ValorTipoValorSeleccionado = TipoValorBL.GetValorTipoValor(Convert.ToInt16(ddlTipoValor.SelectedItem.Value));

            //asigna valor del tipo de valor
            hfValorTipoValor.Value = Util.Funcion.FormatoNumerico(ValorTipoValorSeleccionado);

            //Agrega item CLP para traducir el valor de la caja de texto
            visualizar.notas_de_pedido.AddFunctionValorTotalProyectoCLP(ref ddlTipoValor1, ddlTipoValor.SelectedItem.Text, ValorTipoValorSeleccionado); //valor total proyecto (datos)
            visualizar.notas_de_pedido.AddFunctionValorTotalProyectoCLP(ref ddlTipoValor2, ddlTipoValor.SelectedItem.Text, ValorTipoValorSeleccionado); //valor total NP (agregar NP)
            visualizar.notas_de_pedido.AddFunctionValorTotalProyectoCLP(ref ddlTipoValor3, ddlTipoValor.SelectedItem.Text, ValorTipoValorSeleccionado); //valor total proyecto (agregar NP)
            visualizar.notas_de_pedido.AddFunctionValorTotalProyectoCLP(ref ddlTipoValor4, ddlTipoValor.SelectedItem.Text, ValorTipoValorSeleccionado); //valor total proyecto (estado de pago)

            //Agrega onchange a txtValorTotalNP y txtValorTotalProyecto (Agregar NP)
            string onChangeDropDownList = string.Format("NPValoresACLP(this, '{0}', '{1}', '{2}', '{3}', '{4}')", txtCantidad.ClientID, txtValorUnitario.ClientID, txtValorTotalNP.ClientID, txtValorProyecto1.ClientID, false);
            ddlTipoValor2.Attributes.Add("onchange", onChangeDropDownList); //ddl Valor total NP
            ddlTipoValor3.Attributes.Add("onchange", onChangeDropDownList); //ddl Valor proyecto (agregar NP)

            if (ValorTipoValorSeleccionado > 1)
            {
                //Almacena el valor del tipo de valor
                ViewState.Add("ValorTipoValor", ValorTipoValorSeleccionado);

                //determina cantidad de decimales que se porá ingresar en el valor de una NP (ingreso y edición)
                txtValorUnitario.Attributes.Add("onkeyup", "FormatoNumerico(this, 2);");

                //determina cantidad de decimales que se podrá ingresar en el monto del estado de pago
                txtMonto.Attributes.Add("onkeyup", "FormatoNumerico(this, 2);");
            }
            else
            {
                //determina cantidad de decimales que se porá ingresar en el valor de una NP (ingreso y edición)
                txtValorUnitario.Attributes.Add("onkeyup", "FormatoNumerico(this, 0);");

                //determina cuantos decimales se podrá ingresar en el monto del estado de pago
                //tipo de valor CLP no lleva permite decimales
                txtMonto.Attributes.Add("onkeyup", "FormatoNumerico(this, 0);");
            }

            //activa desplegable tipo documento
            BindTipoDocumento();

            //Carga contactos del cliente
            BindDataContato(IDCliente);

            //Carga Tipos de Nota de Pedido
            BindTipoNP(ddlTipoNP);

            //asigna "ID" primera nota de pedido
            txtIDNP.Text = string.Format("{0}-1", IDRealProyecto);

            //inhabilita cambiar cliente y datos de cabecera
            txtCliente.Enabled = false;
            ddlTipoValor.Enabled = false;
            txtPadre.Enabled = false;
            txtNombre.Enabled = false;
            txtEjecutivo.Enabled = false;
            txtDireccion.Enabled = false;
            txtComuna.Enabled = false;
            txtFechaComprometida.Enabled = false;
        }

        private void BindTipoValor()
        {
            ddlTipoValor.DataSource = TipoValorBL.GetTipoValor();
            ddlTipoValor.DataTextField = "sigla";
            ddlTipoValor.DataValueField = "id";
            ddlTipoValor.DataBind();
        }

        private void BindTipoNP(DropDownList ddlTipoNP)
        {

            List<Entities.TipoNPEntity> lstData = NotaPedidoBL.GetTipoNP();
            ddlTipoNP.DataTextField = "nombre";
            ddlTipoNP.DataValueField = "id";
            ddlTipoNP.DataSource = lstData;
            ddlTipoNP.DataBind();
        }

        private void BindTipoUnidad(DropDownList ddlTipoUnidad)
        {
            ddlTipoUnidad.DataSource = TipoUnidadBL.GetTipoUnidad(0, string.Empty, string.Empty, false);
            ddlTipoUnidad.DataBind();
        }

        private void BindTipoDocumento()
        {
            ddlTipoDocumento.DataSource = ProyectoBL.GetTipoDocumento().OrderByDescending(x => x.VinculaOC); //OrderByDescending hace que traiga Orden de compra primero
            ddlTipoDocumento.DataBind();
        }

        private void BindOCProyecto(DropDownList ddlOC)
        {
            List<Entities.DocumentoComercialEntity> Lst = ProyectoBL.GetOCProyecto(Convert.ToInt16(hfIDProyecto.Value));
            ddlOC.DataSource = Lst;
            ddlOC.DataBind();

            //Si es mayor a 0, habilito la pestaña Notas de pedido
            if (Lst.Count > 0) tabNP.Visible = true;
            else tabNP.Visible = false;
        }

        private void BindDataDocComercial(int IDProyecto)
        {
            try
            {
                List<Entities.DocumentoComercialEntity> Lst = ProyectoBL.GetDocumentoComercial(IDProyecto);
                gvDocComercial.DataKeyNames = new string[1] { "ID" }; //PK
                gvDocComercial.DataSource = Lst;
                gvDocComercial.DataBind();

                if (Lst.Count > 0)
                {
                    gvDocComercial.UseAccessibleHeader = true;
                    gvDocComercial.HeaderRow.TableSection = TableRowSection.TableHeader;

                    lblRegistroDC.Text = string.Format("<b>{0}</b> documentos en el directorio del proyecto ", Lst.Count);
                    lblRegistroDC.CssClass = "alert-info";
                }
                else
                {
                    lblRegistroDC.Text = "No hay documentos comerciales para este proyecto en ";
                    lblRegistroDC.CssClass = "alert-danger";
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                return;
            }
        }

        protected void btnAgregarDocComercial_ServerClick(object sender, EventArgs e)
        {
            int IDProyecto = Convert.ToInt16(hfIDProyecto.Value);
            int IDTipoDoc = Convert.ToInt16(ddlTipoDocumento.SelectedItem.Value);
            string Desc = txtDescripcionDoc.Text.Trim();
            string NombreArchivo = fArchivo.FileName;
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();

            try
            {
                ////inserta registro y retorna directorio del proyecto
                //string Directorio = ProyectoBL.AddDocumentoComercial(IDUsuario, Convert.ToInt16(ddlTipoDocumento.SelectedItem.Value), IDProyecto, NombreArchivo, DateTime.Now, txtDescripcionDoc.Text);
                //string RutaArchivo = string.Format("{0}\\{1}", Directorio, fArchivo.FileName);
                //fArchivo.SaveAs(RutaArchivo); //guarda archivo

                //obtiene credenciales para ingresar a la carpeta compartida
                string ServidorArchivosProyecto = ConfigurationManager.AppSettings["servidorArchivosProyecto"].ToString();
                string UsuarioCarpetaCompartidaServidorArchivosProyecto = ConfigurationManager.AppSettings["usuarioCarpetaCompartidaServidorArchivosProyecto"].ToString();
                string PasswordCarpetaCompartidaServidorArchivosProyecto = ConfigurationManager.AppSettings["passwordCarpetaCompartidaServidorArchivosProyecto"].ToString();

                using (App_Code.Seguridad.NetworkShareAccesser.Acceso(ServidorArchivosProyecto, UsuarioCarpetaCompartidaServidorArchivosProyecto, PasswordCarpetaCompartidaServidorArchivosProyecto))
                {
                    //inserta registro y retorna directorio del proyecto
                    string Directorio = ProyectoBL.AddDocumentoComercial(IDUsuario, Convert.ToInt16(ddlTipoDocumento.SelectedItem.Value), IDProyecto, NombreArchivo, DateTime.Now, txtDescripcionDoc.Text);
                    string RutaArchivo = string.Format(@"\\{0}\{1}", Directorio, fArchivo.FileName);
                    fArchivo.SaveAs(RutaArchivo); //guarda archivo

                    //carga desplegable con órdenes de compra
                    BindOCProyecto(ddlOC);

                    BindDataDocComercial(IDProyecto);
                    lblDirectorioDC.Text = string.Format("<b>{0}</b>", Directorio);

                    //default formulario
                    txtDescripcionDoc.Text = string.Empty;
                }
            }
            catch /*(Exception ex)*/
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop23", Util.GetMensajeAlerta("error", "Error...", Util.GetError("Ha ocurrido un error al intentar adjuntar un documento. Favor de intentar más tarde.")), true);
            }
        }

        protected void gvDocComercial_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                //cuando edita debe cargar el desplegable tipo documento...
                if ((r.RowState & DataControlRowState.Edit) == DataControlRowState.Edit)
                {
                    //asigna contador de caracteres
                    (r.Controls[0].FindControl("txtDesc") as TextBox).Attributes.Add("onkeyup", "countChar(this, '" + (r.Controls[0].FindControl("lblContDesc") as Label).ClientID + "', 30)");

                    DropDownList ddlTipoDoc = (r.Controls[0].FindControl("ddlTipoDocumento") as DropDownList);
                    ddlTipoDoc.DataSource = ProyectoBL.GetTipoDocumento();
                    ddlTipoDoc.SelectedValue = ((Entities.DocumentoComercialEntity)r.DataItem).TipoDocumento.ID.ToString();
                    ddlTipoDoc.DataBind();
                }
            }
        }

        protected void gvDocComercial_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvDocComercial.EditIndex = e.NewEditIndex;
            BindDataDocComercial(Convert.ToInt16(hfIDProyecto.Value));
            Util.DeshabilitaDIV(formDocComercial);
        }

        protected void gvDocComercial_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            Entities.DocumentoComercialEntity DocComercial = new Entities.DocumentoComercialEntity(Convert.ToInt16(e.Keys["ID"].ToString()), (gvDocComercial.Rows[e.RowIndex].FindControl("txtDesc") as TextBox).Text.Trim(), Convert.ToInt16((gvDocComercial.Rows[e.RowIndex].FindControl("ddlTipoDocumento") as DropDownList).SelectedItem.Value));

            try
            {
                //actualiza valores
                ProyectoBL.SetDocumentoComercial(DocComercial.ID, DocComercial.TipoDocumento.ID, DocComercial.Descripcion);

                //actualiza la tabla con nuevo valores
                gvDocComercial.EditIndex = -1;
                BindDataDocComercial(Convert.ToInt16(hfIDProyecto.Value));

                //carga desplegable con órdenes de compra
                BindOCProyecto(ddlOC);

                //finalizada la acción habilita nuevamente el formulario de búsqueda
                Util.HabilitaDIV(formDocComercial);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        protected void gvDocComercial_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvDocComercial.EditIndex = -1;
            BindDataDocComercial(Convert.ToInt16(hfIDProyecto.Value));
            Util.HabilitaDIV(formDocComercial);
        }

        protected void gvDocComercial_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                //cambia estado en la BD
                string RutaArchivo = ProyectoBL.DelDocumentoComercial((Convert.ToInt16(e.Keys["ID"].ToString())));

                //Elimina el archivo físicamente del directorio
                if (File.Exists(RutaArchivo))
                {
                    File.Delete(RutaArchivo);
                }

                //quita y descuenta el contador
                BindDataDocComercial(Convert.ToInt16(hfIDProyecto.Value));

                //carga desplegable con órdenes de compra
                BindOCProyecto(ddlOC);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }

            ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("success", "Operación satisfactoria", "El registro fue eliminado."), true);
        }

        protected void gvDocProyecto_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                //cambia estado en la BD
                ProyectoBL.DelDocumentoProyecto((Convert.ToInt16(e.Keys["ID"].ToString())));

                //quita y descuenta el contador
                int IDProyecto = Convert.ToInt16(hfIDProyecto.Value);
                BindDataDocProyecto(IDProyecto);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }

            ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("success", "Operación satisfactoria", "El registro fue eliminado."), true);
        }

        protected void btnAgregarDocProyecto_ServerClick(object sender, EventArgs e)
        {
            try
            {
                int IDProyecto = Convert.ToInt16(hfIDProyecto.Value);
                ProyectoBL.AddDocumentoProyecto(IDProyecto, txtDescripcionArchivo.Text.Trim(), txtRutaArchivo.Text.Trim(), DateTime.Now);
                BindDataDocProyecto(IDProyecto);

                //default formulario
                txtDescripcionArchivo.Text = string.Empty;
                txtRutaArchivo.Text = string.Empty;
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        private void BindDataDocProyecto(int IDProyecto)
        {
            try
            {
                List<Entities.DocumentoProyectoEntity> Lst = ProyectoBL.GetDocumentoProyecto(IDProyecto);
                gvDocProyecto.DataKeyNames = new string[1] { "ID" };
                gvDocProyecto.DataSource = Lst;
                gvDocProyecto.DataBind();

                if (Lst.Count > 0)
                {
                    gvDocProyecto.UseAccessibleHeader = true;
                    gvDocProyecto.HeaderRow.TableSection = TableRowSection.TableHeader;

                    lblRegistroDP.Text = string.Format("<b>{0}</b> documentos en el directorio del proyecto ", Lst.Count);
                    lblRegistroDP.CssClass = "alert-info";
                }
                else
                {
                    lblRegistroDP.Text = "No hay archivos o directorios compartidos para este proyecto";
                    lblRegistroDP.CssClass = "alert-danger";
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                return;
            }
        }

        protected void btnActualizarTP_Click(object sender, EventArgs e)
        {
            BindTipoUnidad(ddlTipoUnidad);
            ScriptManager.RegisterStartupScript(this, GetType(), "UpdateddlTP", "$('.selectpicker').selectpicker('refresh');", true);
        }

        protected void btnEPConfirmarAgregarNP_ServerClick(object sender, EventArgs e)
        {
            int IDProyecto = Convert.ToInt16(hfIDProyecto.Value);
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
            int IDNotificacionApp = 0;

            //NotaPedidoBL.AddNPProyecto(IDProyecto, IDUsuario, Convert.ToInt16(ddlTipoUnidad.SelectedItem.Value), Convert.ToInt16(ddlOC.SelectedItem.Value), Convert.ToDecimal(txtValorUnitario.Text), Convert.ToDecimal(txtCantidad.Text), txtDescripcionNP.Text.Trim(), DateTime.Now, "", ref IDNotificacionApp);
            NotaPedidoBL.AddNPProyecto(IDProyecto, IDUsuario, Convert.ToInt16(ddlTipoUnidad.SelectedItem.Value), Convert.ToInt16(ddlTipoNP.SelectedItem.Value), Convert.ToInt16(ddlOC.SelectedItem.Value), Convert.ToDecimal(txtValorUnitario.Text), Convert.ToDecimal(txtCantidad.Text), txtDescripcionNP.Text.Trim(), DateTime.Now, "", ref IDNotificacionApp);

            //Modifica el monto o porcentaje (según haya selecionado el usuario) de los estados de pago abiertos (no facturados)
            EstadoPagoBL.SetEstadoPago(IDProyecto, ddlOperacionEPAgregar.SelectedItem.Value);

            //default formulario
            txtIDNP.Text = string.Format("{0}-{1}", IDProyecto, Convert.ToInt16(txtIDNP.Text.Split('-')[1]) + 1);
            txtCantidad.Text = string.Empty;
            txtDescripcionNP.Text = string.Empty;
            txtValorUnitario.Text = string.Empty;

            //actualiza gridview con np
            BindDataNP(IDProyecto);

            //actualiza estados de pago
            BindDataEstadoPago(IDProyecto);

            //valida si es posible crear el proyecto
            btnFinalizarCreacion.Visible = ProyectoBL.GetValidaCreacionProyecto(IDProyecto);
        }

        protected void btnAgregarNP_ServerClick(object sender, EventArgs e)
        {
            int IDProyecto = Convert.ToInt16(hfIDProyecto.Value);
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();

            try
            {
                //Agrega NP y consulta si tiene EP abiertos (No facturados) para confirmación
                if (EstadoPagoBL.GetTieneEPAbiertos(IDProyecto))
                {
                    Entities.EstadoPagoEntity EPNoFacturado = EstadoPagoBL.GetEstadoPago(IDProyecto).Where(x => x.Facturado == false).ToList()[0];

                    lblFechaIngresoEP.Text = EPNoFacturado.FechaCreacion.ToShortDateString();
                    lblPorcentajeEP.Text = EPNoFacturado.Porcentaje.ToString() + "%";
                    lblMontoEP.Text = EPNoFacturado.Monto.ToString();
                    lblGlosaEP.Text = EPNoFacturado.Glosa;

                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "$('#EPConfirmaAgregarNP').modal('show');", true);
                }
                else
                {
                    int IDNotificacionApp = 0;
                    //NotaPedidoBL.AddNPProyecto(IDProyecto, IDUsuario, Convert.ToInt16(ddlTipoUnidad.SelectedItem.Value), Convert.ToInt16(ddlOC.SelectedItem.Value), Convert.ToDecimal(txtValorUnitario.Text), Convert.ToDecimal(txtCantidad.Text), txtDescripcionNP.Text.Trim(), DateTime.Now, "", ref IDNotificacionApp);
                    NotaPedidoBL.AddNPProyecto(IDProyecto, IDUsuario, Convert.ToInt16(ddlTipoUnidad.SelectedItem.Value), Convert.ToInt16(ddlTipoNP.SelectedItem.Value), Convert.ToInt16(ddlOC.SelectedItem.Value), Convert.ToDecimal(txtValorUnitario.Text), Convert.ToDecimal(txtCantidad.Text), txtDescripcionNP.Text.Trim(), DateTime.Now, "", ref IDNotificacionApp);

                    //default formulario
                    txtIDNP.Text = string.Format("{0}-{1}", IDProyecto, Convert.ToInt16(txtIDNP.Text.Split('-')[1]) + 1);
                    txtCantidad.Text = string.Empty;
                    txtDescripcionNP.Text = string.Empty;
                    txtValorUnitario.Text = string.Empty;
                    ddlTipoNP.SelectedIndex = -1;
                    ddlTipoValor2.SelectedIndex = -1;
                    ddlTipoValor3.SelectedIndex = -1;

                    //actualiza gridview con np
                    BindDataNP(IDProyecto);

                    //actualiza estados de pago
                    BindDataEstadoPago(IDProyecto);

                    //limpia formulario de agregar estado de pago
                    txtPorcentaje.Text = string.Empty;
                    txtMonto.Text = string.Empty;
                    txtGlosa.Text = string.Empty;

                    //valida si es posible crear el proyecto
                    btnFinalizarCreacion.Visible = ProyectoBL.GetValidaCreacionProyecto(IDProyecto);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        private void BindDataNP(int IDProyecto)
        {
            try
            {
                List<Entities.NotaPedidoEntity> Lst = NotaPedidoBL.GetNPCreada(IDProyecto);

                //sumatoria total valor proyecto sólo con NP's en estado de Creación
                double ValorTotalProyectoCreacion = ProyectoBL.GetValorTotalProyecto(IDProyecto);
                hfValorTotalProyecto.Value = ValorTotalProyectoCreacion.ToString();

                //si el nro de notas de pedido es mayor a 0, habilito la pestaña estado de pago
                if (Lst.Count > 0) tabEP.Visible = true;

                gvNP.DataKeyNames = new string[1] { "ID" }; //PK
                gvNP.DataSource = Lst;
                gvNP.DataBind();

                txtValorProyecto.Text = Util.Funcion.FormatoNumerico(ValorTotalProyectoCreacion);
                txtValorProyecto1.Text = txtValorProyecto.Text;
                txtValorProyectoEP.Text = txtValorProyecto.Text;

                //Agrega atributos de datos (textbox y valor) para generar cálculo de onchange en ddlTipoValor
                ddlTipoValor1.Attributes.Add("txt", txtValorProyecto.ClientID);
                ddlTipoValor1.Attributes.Add("valor", ValorTotalProyectoCreacion.ToString());
                ddlTipoValor4.Attributes.Add("txt", txtValorProyectoEP.ClientID);
                ddlTipoValor4.Attributes.Add("valor", ValorTotalProyectoCreacion.ToString());
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        protected void gvNP_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                int IDProyecto = Convert.ToInt16(hfIDProyecto.Value.ToString());
                Entities.NotaPedidoEntity NP = (Entities.NotaPedidoEntity)r.DataItem;

                HyperLink RowNP = (r.Controls[0].FindControl("rowNP") as HyperLink);
                RowNP.Attributes.Add("data-parent", string.Format("#{0}", accordion.ClientID));
                RowNP.Text = string.Format("{0} de {1}", NP.IDCompuesta, r.RowIndex + 1);

                //crea variable de controles
                TextBox txtCantidad = (r.Controls[0].FindControl("txtCantidad") as TextBox);
                TextBox txtValorUnitario = (r.Controls[0].FindControl("txtValorUnitario") as TextBox);
                TextBox txtValorTotalNP = (r.Controls[0].FindControl("txtValorTotalNP") as TextBox);
                TextBox txtValorProyecto = (r.Controls[0].FindControl("txtValorProyecto") as TextBox);

                //asigna url del div
                string IDEtiquetaNP = (r.Controls[0].FindControl("np") as HtmlGenericControl).ClientID;
                RowNP.NavigateUrl = string.Format("#{0}", IDEtiquetaNP);
                RowNP.Attributes["aria-controls"] = IDEtiquetaNP;

                //obtiene items de ddl tipos de valor
                DropDownList ddlTipoValor2 = (r.Controls[0].FindControl("ddlTipoValor2") as DropDownList); //valor total NP
                DropDownList ddlTipoValor3 = (r.Controls[0].FindControl("ddlTipoValor3") as DropDownList); //valor total proyecto NP
                ListItem[] items = new ListItem[ddlTipoValor1.Items.Count];
                ddlTipoValor1.Items.CopyTo(items, 0); //obtiene items de ddlTipoValor1 y se copian a variable "items"

                //copia items tipos de valor
                ddlTipoValor2.Items.AddRange(items);
                ddlTipoValor3.Items.AddRange(items);
                ddlTipoValor2.DataBind();
                ddlTipoValor3.DataBind();

                //valores NP creación
                txtCantidad.Text = Util.Funcion.FormatoNumerico(NP.Creacion.Cantidad);

                //si tiene más de un item, NO es tipo de valor CLP
                if (ddlTipoValor1.Items.Count > 1)
                {
                    //asigna valores
                    txtValorUnitario.Text = Util.Funcion.FormatoNumerico(NP.Creacion.ValorUnitario);
                    txtValorTotalNP.Text = Util.Funcion.FormatoNumerico(NP.Creacion.Cantidad * NP.Creacion.ValorUnitario);
                    txtValorProyecto.Text = Util.Funcion.FormatoNumerico(hfValorTotalProyecto.Value);
                }
                else
                {
                    //asigna valores
                    txtValorUnitario.Text = Util.Funcion.FormatoNumericoEntero(NP.Creacion.ValorUnitario);
                    txtValorTotalNP.Text = Util.Funcion.FormatoNumericoEntero(NP.Creacion.Cantidad * NP.Creacion.ValorUnitario);
                    txtValorProyecto.Text = Util.Funcion.FormatoNumericoEntero(hfValorTotalProyecto.Value);
                }

                //Bind desplegable con Tipo de unidad
                DropDownList ddlTipoUnidad = (r.Controls[0].FindControl("ddlTipoUnidad") as DropDownList);
                BindTipoUnidad(ddlTipoUnidad);
                ddlTipoUnidad.SelectedValue = NP.Creacion.TipoUnidad.ID.ToString();

                //Bind desplegable con Tipo de NP
                DropDownList ddlTipoNP = (r.Controls[0].FindControl("ddlTipoNP") as DropDownList);
                BindTipoNP(ddlTipoNP);
                ddlTipoNP.SelectedValue = NP.Creacion.TipoNP.ID.ToString();

                //Bind desplegable con OC seleccionada
                DropDownList ddlOC = (r.Controls[0].FindControl("ddlOC") as DropDownList);
                BindOCProyecto(ddlOC);
                ddlOC.SelectedValue = NP.Creacion.OrdenCompra.ID.ToString();

                if ((r.RowState & DataControlRowState.Edit) == DataControlRowState.Edit)
                {
                    //almacena valores originales de la NP (para poder realizar cálculo correctamente)
                    hfValorNPAntesDeSerEditada.Value = (NP.Creacion.Cantidad * NP.Creacion.ValorUnitario).ToString();

                    //habilita todos los item (modo edición)
                    foreach (ListItem TipoUnidad in ddlTipoUnidad.Items)
                        TipoUnidad.Attributes.Remove("disabled");

                    foreach (ListItem TipoNP in ddlTipoNP.Items)
                        TipoNP.Attributes.Remove("disabled");

                    //establece el tipo de valor seleccionado
                    (r.Controls[0].FindControl("lblMonedaSeleccionada") as Label).Text = ddlTipoValor.SelectedItem.Text; //*Valor unitario

                    //asigna javascript autocálculo al agregar una NP a textboxs Cantidad y Valor Unitario
                    string ArmaFuncionValorTotalNP = string.Format("ValorTotalNP('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')", txtCantidad.ClientID, txtValorUnitario.ClientID, txtValorTotalNP.ClientID, ddlTipoValor2.ClientID, txtValorProyecto.ClientID, ddlTipoValor3.ClientID, true);
                    txtCantidad.Attributes.Add("onblur", ArmaFuncionValorTotalNP);
                    txtValorUnitario.Attributes.Add("onblur", ArmaFuncionValorTotalNP);

                    //determina cantidad de decimales que se podrá ingresar en el valor de una NP (edición)
                    if (ddlTipoValor1.Items.Count == 1)
                        txtValorUnitario.Attributes.Add("onkeyup", "FormatoNumerico(this, 0);");
                    else
                        txtValorUnitario.Attributes.Add("onkeyup", "FormatoNumerico(this, 2);");

                    //asigna valor total NP antes de ser modificada
                    hfActualValorTotalNP.Value = (NP.Creacion.Cantidad * NP.Creacion.ValorUnitario).ToString();

                    //Agrega función cliente onchange para calcular el valor del proyecto si se cambia el tipo de valor
                    string onChangeDropDownList = string.Format("NPValoresACLP(this, '{0}', '{1}', '{2}', '{3}', '{4}')", txtCantidad.ClientID, txtValorUnitario.ClientID, txtValorTotalNP.ClientID, txtValorProyecto.ClientID, true);
                    ddlTipoValor2.Attributes.Add("onchange", onChangeDropDownList);
                    ddlTipoValor3.Attributes.Add("onchange", onChangeDropDownList);

                    string VinculaDropDownList = string.Format(" dtvgvNPEditing{0}", r.RowIndex);
                    ddlTipoValor2.CssClass += VinculaDropDownList;
                    ddlTipoValor3.CssClass += VinculaDropDownList;

                    //Establece ambos ddl al tipo valor ingresado por el proyecto
                    ddlTipoValor2.SelectedIndex = -1;
                    ddlTipoValor3.SelectedIndex = -1;

                    //asigna contador de caracteres
                    (r.Controls[0].FindControl("txtDescripcionNP") as TextBox).Attributes.Add("onkeyup", "countChar(this, '" + (r.Controls[0].FindControl("lblContDescNP") as Label).ClientID + "', 500)");

                    //Almacena valores de la NP antes de la edición
                    ViewState.Add("NPAntesDeSerEditada", new Entities.CreacionEntity(NP.Creacion.TipoUnidad.ID, NP.Creacion.OrdenCompra.ID, NP.Creacion.ValorUnitario, NP.Creacion.Cantidad, NP.Creacion.Descripcion, NP.Creacion.TipoNP.ID));

                    //habilita todos los item (modo edición)
                    foreach (ListItem OC in ddlOC.Items)
                        OC.Attributes.Remove("disabled");
                }
                else
                {
                    //deshabilita todos los item (modo visualización)
                    foreach (ListItem TipoUnidad in ddlTipoUnidad.Items)
                        TipoUnidad.Attributes.Add("disabled", "");

                    foreach (ListItem TipoNP in ddlTipoNP.Items)
                        TipoNP.Attributes.Add("disabled", "");

                    DropDownList ddlTipoValor1 = (r.Controls[0].FindControl("ddlTipoValor1") as DropDownList); //valor total NP

                    //copia items tipos de valor
                    ddlTipoValor1.Items.AddRange(items);
                    ddlTipoValor1.DataBind();

                    //Agrega atributos de datos (textbox y valor) para generar cálculo de onchange en ddlTipoValor
                    ddlTipoValor1.Attributes.Add("txt", txtValorUnitario.ClientID);
                    ddlTipoValor1.Attributes.Add("valor", NP.Creacion.ValorUnitario.ToString());
                    ddlTipoValor2.Attributes.Add("txt", txtValorTotalNP.ClientID);
                    ddlTipoValor2.Attributes.Add("valor", (NP.Creacion.Cantidad * NP.Creacion.ValorUnitario).ToString());
                    ddlTipoValor3.Attributes.Add("txt", txtValorProyecto.ClientID);
                    ddlTipoValor3.Attributes.Add("valor", hfValorTotalProyecto.Value);

                    //Agrega función cliente onchange para calcular el valor del proyecto si se cambia el tipo de valor
                    string VinculaDropDownList = string.Format(" dtvgvNPCreacion{0}", r.RowIndex);
                    ddlTipoValor1.CssClass += VinculaDropDownList;
                    ddlTipoValor2.CssClass += VinculaDropDownList;
                    ddlTipoValor3.CssClass += VinculaDropDownList;

                    //deshabilita todos los item (modo visualización)
                    foreach (ListItem OC in ddlOC.Items)
                        OC.Attributes.Add("disabled", "");
                }
            }
        }

        protected void gvNP_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvNP.EditIndex = e.NewEditIndex;

            int IDProyecto = Convert.ToInt16(hfIDProyecto.Value);
            BindDataNP(IDProyecto);

            Util.DeshabilitaDIV(formularioAgregarNP);
        }

        protected void gvNP_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvNP.EditIndex = -1;

            int IDProyecto = Convert.ToInt16(hfIDProyecto.Value);
            BindDataNP(IDProyecto);

            Util.HabilitaDIV(formularioAgregarNP);
        }

        protected void btnEPConfirmarEditarNP_ServerClick(object sender, EventArgs e)
        {
            int IDProyecto = Convert.ToInt16(hfIDProyecto.Value);
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();

            Entities.NotaPedidoEntity NP = (Entities.NotaPedidoEntity)ViewState["EditarNP"];
            ViewState.Remove("EditarNP");

            //Agrega nueva NP con fecha más reciente y datos actualizados
            NotaPedidoBL.SetNPCreada(IDProyecto, NP.ID, IDUsuario, NP.Creacion.TipoUnidad.ID, NP.Creacion.OrdenCompra.ID, NP.Creacion.ValorUnitario, NP.Creacion.Cantidad, NP.Creacion.Descripcion, DateTime.Now, ddlOperacionEPEditar.SelectedItem.Value, NP.Creacion.TipoNP.ID);

            //Modifica el monto o porcentaje (según haya selecionado el usuario) de los estados de pago abiertos (no facturados)
            //EstadoPagoBL.SetEstadoPago(IDProyecto, ddlOperacionEPEditar.SelectedItem.Value);

            gvNP.EditIndex = -1;
            //actualiza gridview con np
            BindDataNP(IDProyecto);

            //actualiza estados de pago
            BindDataEstadoPago(IDProyecto);

            //limpia formulario de agregar estado de pago
            txtPorcentaje.Text = string.Empty;
            txtMonto.Text = string.Empty;
            txtGlosa.Text = string.Empty;

            Util.HabilitaDIV(formularioAgregarNP);
        }

        protected void gvNP_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow r = gvNP.Rows[e.RowIndex];

            int IDProyecto = Convert.ToInt16(hfIDProyecto.Value);
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();

            //obtiene NP a editar
            Entities.NotaPedidoEntity NP = new Entities.NotaPedidoEntity(Convert.ToInt16(e.Keys["ID"]));
            NP.Creacion = new Entities.CreacionEntity(Convert.ToInt16((r.FindControl("ddlTipoUnidad") as DropDownList).SelectedItem.Value), Convert.ToInt16((r.FindControl("ddlOC") as DropDownList).SelectedItem.Value), Convert.ToDecimal((r.FindControl("txtValorUnitario") as TextBox).Text), Convert.ToDecimal((r.FindControl("txtCantidad") as TextBox).Text), (r.FindControl("txtDescripcionNP") as TextBox).Text, Convert.ToInt16((r.FindControl("ddlTipoNP") as DropDownList).SelectedItem.Value));

            try
            {
                Entities.CreacionEntity NPAntesDeSerEditada = (Entities.CreacionEntity)ViewState["NPAntesDeSerEditada"];
                ViewState.Remove("NPAntesDeSerEditada");

                //Verifica si hubo cambios en la edición...
                if ((NP.Creacion.Cantidad != NPAntesDeSerEditada.Cantidad) || (NP.Creacion.Descripcion != NPAntesDeSerEditada.Descripcion) || (NP.Creacion.ValorUnitario != NPAntesDeSerEditada.ValorUnitario) || (NP.Creacion.OrdenCompra.ID != NPAntesDeSerEditada.OrdenCompra.ID) || (NP.Creacion.TipoUnidad.ID != NPAntesDeSerEditada.TipoUnidad.ID) || (NP.Creacion.TipoNP.ID != NPAntesDeSerEditada.TipoNP.ID))
                {
                    //Agrega NP y consulta si tiene EP abiertos (No facturados) para confirmación
                    if (EstadoPagoBL.GetTieneEPAbiertos(IDProyecto))
                    {
                        ViewState.Add("EditarNP", NP);

                        Entities.EstadoPagoEntity EPNoFacturado = EstadoPagoBL.GetEstadoPago(IDProyecto).Where(x => x.Facturado == false).ToList()[0];

                        lblFechaIngresoEP3.Text = EPNoFacturado.FechaCreacion.ToShortDateString();
                        lblPorcentajeEP3.Text = EPNoFacturado.Porcentaje.ToString() + "%";

                        if (ProyectoBL.GetProyectoEsCLP(IDProyecto))
                            lblMontoEP3.Text = string.Format("{0} {1}", lblMonedaSeleccionadaEP.Text, Util.Funcion.FormatoNumericoEntero(EPNoFacturado.Monto.ToString()));
                        else
                            lblMontoEP3.Text = string.Format("{0} {1}", lblMonedaSeleccionadaEP.Text, Util.Funcion.FormatoNumerico(EPNoFacturado.Monto.ToString()));

                        lblGlosaEP3.Text = EPNoFacturado.Glosa;

                        ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "$('#EPConfirmaEditarNP').modal('show');", true);
                    }
                    else
                    {
                        //Agrega nueva NP con fecha más reciente y datos actualizados
                        NotaPedidoBL.SetNPCreada(IDProyecto, NP.ID, IDUsuario, NP.Creacion.TipoUnidad.ID, NP.Creacion.OrdenCompra.ID, NP.Creacion.ValorUnitario, NP.Creacion.Cantidad, NP.Creacion.Descripcion, DateTime.Now, string.Empty, NP.Creacion.TipoNP.ID);

                        gvNP.EditIndex = -1;
                        //actualiza gridview con np
                        BindDataNP(IDProyecto);

                        //actualiza estados de pago
                        BindDataEstadoPago(IDProyecto);

                        //limpia formulario de agregar estado de pago
                        txtPorcentaje.Text = string.Empty;
                        txtMonto.Text = string.Empty;
                        txtGlosa.Text = string.Empty;

                        Util.HabilitaDIV(formularioAgregarNP);
                    }
                }
                else
                {
                    //Cierra la edición
                    gvNP.EditIndex = -1;

                    //actualiza gridview con np
                    BindDataNP(IDProyecto);

                    Util.HabilitaDIV(formularioAgregarNP);

                    //No se aplicó ningún cambio
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("warning", "Sin cambios...", "No se aplicó ningún cambio a la nota de pedido..."), true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        protected void btnEPConfirmaEliminarNP_ServerClick(object sender, EventArgs e)
        {
            int IDProyecto = Convert.ToInt16(hfIDProyecto.Value);

            //Al eliminar una NP cuando el proyecto está en estado de creación, no se almacena el usuario que hizo la acción
            //Elimina lógicamente la NP
            //Además DelNP() Modifica el monto o porcentaje (según haya selecionado el usuario) de los estados de pago abiertos (no facturados)
            NotaPedidoBL.DelNPCreada(Convert.ToInt16(ViewState["EPConfirmaEliminarNP"].ToString()), ddlOperacionEPEliminar.SelectedItem.Value); //envía tipo de acción a realizar en los estados de pago creados

            txtIDNP.Text = string.Format("{0}-{1}", IDProyecto, Convert.ToInt16(txtIDNP.Text.Split('-')[1]) - 1);
            BindDataNP(IDProyecto);

            //actualiza y bindea gridview con estados de pago
            BindDataEstadoPago(IDProyecto);

            //limpia formulario de agregar estado de pago
            txtPorcentaje.Text = string.Empty;
            txtMonto.Text = string.Empty;
            txtGlosa.Text = string.Empty;

            //valida si es posible crear el proyecto
            btnFinalizarCreacion.Visible = ProyectoBL.GetValidaCreacionProyecto(IDProyecto);
        }

        protected void btnConfirmarEliminarNP_ServerClick(object sender, EventArgs e)
        {
            int IDProyecto = Convert.ToInt16(hfIDProyecto.Value.ToString());
            int IDNP = Convert.ToInt16(ViewState["ConfirmarEliminarNP"].ToString());

            if (EstadoPagoBL.GetTieneEPAbiertos(IDProyecto))
            {
                //ViewState.Add("EPConfirmaEliminarNP", IDNP);

                //Entities.EstadoPagoEntity EPNoFacturado = EstadoPagoBL.GetEstadoPago(IDProyecto).Where(x => x.Facturado == false).ToList()[0];

                //lblFechaIngresoEP2.Text = EPNoFacturado.FechaCreacion.ToShortDateString();
                //lblPorcentajeEP2.Text = EPNoFacturado.Porcentaje.ToString() + "%";
                //lblMontoEP2.Text = EPNoFacturado.Monto.ToString();
                //lblGlosaEP2.Text = EPNoFacturado.Glosa;

                //ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "$('#EPConfirmaEliminarNP').modal('show');", true);
                NotaPedidoBL.DelNPCreada(IDNP, "monto"); //Envía vacío porque no tiene EP creados
            }
            else
            {
                //Al eliminar una NP cuando el proyecto está en estado de creación, no se almacena el usuario que hizo la acción
                //Elimina la NP lógicamente
                NotaPedidoBL.DelNPCreada(IDNP, string.Empty); //Envía vacío porque no tiene EP creados

                txtIDNP.Text = string.Format("{0}-{1}", IDProyecto, Convert.ToInt16(txtIDNP.Text.Split('-')[1]) - 1);
                BindDataNP(IDProyecto);

                //actualiza y bindea gridview con estados de pago
                BindDataEstadoPago(IDProyecto);

                //limpia formulario de agregar estado de pago
                txtPorcentaje.Text = string.Empty;
                txtMonto.Text = string.Empty;
                txtGlosa.Text = string.Empty;

                //valida si es posible crear el proyecto
                btnFinalizarCreacion.Visible = ProyectoBL.GetValidaCreacionProyecto(IDProyecto);
            }
        }

        protected void gvNP_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ViewState.Add("ConfirmarEliminarNP", Convert.ToInt16(e.Keys["ID"]));
            lblNPAEliminar.Text = (gvNP.Rows[e.RowIndex].FindControl("rowNP") as HyperLink).Text.Split(' ')[0];
            ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "$('#ConfirmaEliminarNP').modal('show');", true);
        }

        protected void btnAgregarEstadoPago_ServerClick(object sender, EventArgs e)
        {
            int IDProyecto = Convert.ToInt16(hfIDProyecto.Value.ToString());
            decimal Porcentaje = Convert.ToDecimal(hfPorcentaje.Value.Replace(".", ","));
            decimal Monto = Convert.ToDecimal(hfMonto.Value.Replace(".", ","));

            string Glosa = txtGlosa.Text.Trim();

            txtPorcentaje.Text = Util.Funcion.FormatoNumerico(Porcentaje);

            //si es mayor a 1, el proyecto no es CLP
            if (Convert.ToDouble(hfValorTipoValor.Value) > 1)
                txtMonto.Text = Util.Funcion.FormatoNumerico(Monto);
            else
                txtMonto.Text = Util.Funcion.FormatoNumericoEntero(Monto);

            rfvPorcentaje.IsValid = string.IsNullOrEmpty(txtPorcentaje.Text) ? false : true;
            rfvMonto.IsValid = string.IsNullOrEmpty(txtMonto.Text) ? false : true;

            if (Porcentaje > 100)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("warning", "Porcentaje de estado de pago excedido...", "El porcentaje máximo de un Estado de Pago es 100%."), true);
            }
            else if (Porcentaje == 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("warning", "Porcentaje de estado de pago inválido...", "El porcentaje de un Estado de Pago no puede ser 0%."), true);
            }
            else if (Monto == 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("warning", "Estado de pago inválido...", "No puede crear un Estado de Pago mientras el valor del proyecto sea 0."), true);
            }
            else
            {
                //Verifica si hay un EP sin facturar
                if (EstadoPagoBL.GetEstadoPago(IDProyecto).Where(x => x.Facturado == false).ToList().Count == 0)
                {
                    //cuando el tipo de valor es CLP, el monto debe aproximarse
                    //no es necesario redondear con dos decimales cuando el tipo de valor es usd, uf o euro, porque
                    //la variable monto en la bd es un numeric(11,2) (se redondea solo)
                    if (Convert.ToDouble(hfValorTipoValor.Value) == 1)
                        Monto = Math.Round(Monto, 0);

                    //Intenta agregar estado pago
                    int IDUsuarioCreador = App_Code.Seguridad.GetCurrentIDUSuario();

                    bool Resp = EstadoPagoBL.AddEstadoPago(IDProyecto, Porcentaje, Monto, Glosa, IDUsuarioCreador, DateTime.Now);

                    //Cuando Resp es falso, indica que la sumatoria de estados de pagos excedio el valor total del proyecto
                    if (Resp)
                    {
                        //refresca gridview de estados de pago
                        BindDataEstadoPago(IDProyecto);

                        //limpia formulario de agregar estado de pago
                        LimpiaFormularioEstadoPago();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("warning", "Valor total del proyecto excedido...", "La sumatoria de los estados de pagos (monto), no puede exceder el valor total del proyecto."), true);
                    }
                }
                else
                {
                    //limpia formulario de agregar estado de pago
                    LimpiaFormularioEstadoPago();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("warning", "Estado de pago sin facturar...", "El proyecto no puede tener más de un Estado de Pago sin facturar."), true);
                }
            }

            //corrige qué textbox habilitar
            txtPorcentaje.Enabled = rbPorcentaje.Checked;
            txtMonto.Enabled = rbMonto.Checked;
        }

        private void LimpiaFormularioEstadoPago()
        {
            txtPorcentaje.Text = string.Empty;
            txtMonto.Text = string.Empty;
            txtGlosa.Text = string.Empty;
        }

        private void BindDataEstadoPago(int IDProyecto)
        {
            //El monto de los estados de pago se actualizan automáticamente cuando una nota de pedido es agregada o editada (posible cambio de cantidad o valor unitario)
            List<Entities.EstadoPagoEntity> Lst = EstadoPagoBL.GetEstadoPago(IDProyecto);
            gvEstadoPago.DataSource = Lst;
            gvEstadoPago.DataKeyNames = new string[1] { "ID" }; //PK
            gvEstadoPago.DataBind();

            if (Lst.Count > 0)
            {
                gvEstadoPago.UseAccessibleHeader = true;
                gvEstadoPago.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void btnConfirmarEliminarEP_ServerClick(object sender, EventArgs e)
        {
            int IDProyecto = Convert.ToInt16(hfIDProyecto.Value.ToString());
            Entities.EstadoPagoEntity EPEliminar = (Entities.EstadoPagoEntity)ViewState["EPEliminar"];

            //Elimina viewState de EP a eliminar
            ViewState.Remove("EPEliminar");

            //Elimina lógicamente el estado de pago
            int Respuesta = EstadoPagoBL.DelEstadoPago(EPEliminar.ID);

            if (Respuesta == 1)
            {
                //El estado de pago fue eliminado
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("success", "Estado de Pago eliminado", "El Estado de Pago fue eliminado del proyecto."), true);
            }
            else
            {
                //El estado de pago no puede ser eliminado porque fue emitido a facturacion
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", "El Estado de Pago no puede ser eliminado porque fue emitido a facturación."), true);
            }

            BindDataEstadoPago(IDProyecto);

            //corrige qué textbox habilitar
            txtPorcentaje.Enabled = rbPorcentaje.Checked;
            txtMonto.Enabled = rbMonto.Checked;
        }

        protected void gvEstadoPago_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow r = gvEstadoPago.Rows[e.RowIndex];

            Entities.EstadoPagoEntity EPEliminar = new Entities.EstadoPagoEntity();
            EPEliminar.ID = Convert.ToInt16(e.Keys["ID"].ToString());
            EPEliminar.Porcentaje = Convert.ToDecimal((r.Controls[0].FindControl("lblPorcentaje") as Label).Text.Replace("%", string.Empty));

            ViewState.Add("EPEliminar", EPEliminar);

            lblEstadoPagoEliminar.Text = EPEliminar.Porcentaje.ToString();
            ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "$('#ConfirmaEliminarEP').modal('show');", true);
        }

        protected void btnAgregarContacto_ServerClick(object sender, EventArgs e)
        {
            int IDProyecto = Convert.ToInt16(hfIDProyecto.Value.ToString());
            int IDCliente = Convert.ToInt16(hfCliente.Value);
            string Nombre = txtNombreContacto.Text.Trim();
            string Email = txtEmail.Text.Trim();
            string DescContacto = txtDescripcionContacto.Text.Trim();
            string Telefono = txtTelefono.Text.Trim();
            string Movil = txtMovil.Text.Trim();

            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
            //falta validación de usuario (si ve o no ve contactos compartidos)
            //se está enviando un 1 como tipo de contacto. El ejemplo asume que el usuario con ID 1 es compartido
            //agrega contacto a cliente
            int IDNotificacionApp = 0;
            ContactoBL.AddContacto(IDProyecto, IDCliente, IDUsuario, Nombre, Email, DescContacto, Telefono, Movil, cbCompartirContacto.Checked, ref IDNotificacionApp);

            // NOTIFICACIÓN (6): Notifica a los usuarios que se agregó un contacto a un cliente
            hubs.NotificacionAppHub.EnviaNotificacionesUsuariosConectados(GlobalHost.ConnectionManager.GetHubContext<hubs.NotificacionAppHub>());

            //limpia formulario
            txtNombreContacto.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtDescripcionContacto.Text = string.Empty;
            txtTelefono.Text = string.Empty;
            txtMovil.Text = string.Empty;
            cbCompartirContacto.Checked = false;

            //actualiza gridview de contactos
            BindDataContato(IDCliente);

            //valida si es posible crear el proyecto
            btnFinalizarCreacion.Visible = ProyectoBL.GetValidaCreacionProyecto(IDProyecto);
        }

        protected void gvContacto_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //cambia estado en la BD
            int IDContacto = Convert.ToInt16(e.Keys["ID"].ToString());
            ContactoBL.DelContacto(IDContacto);

            //actualiza gridview de contactos
            BindDataContato(Convert.ToInt16(hfCliente.Value));

            //valida si es posible crear el proyecto
            int IDProyecto = Convert.ToInt16(hfIDProyecto.Value.ToString());
            btnFinalizarCreacion.Visible = ProyectoBL.GetValidaCreacionProyecto(IDProyecto);
        }

        private void BindDataContato(int IDCliente)
        {
            int IDProyecto = Convert.ToInt16(hfIDProyecto.Value.ToString());
            List<Entities.ClienteEntity.ContactoEntity> Lst = ContactoBL.GetContactoProyecto(IDProyecto, IDCliente);
            gvContacto.DataKeyNames = new string[1] { "ID" }; //PK
            gvContacto.DataSource = Lst;
            gvContacto.DataBind();
        }

        //protected void gvContacto_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    GridViewRow r = e.Row;

        //    if (r.RowType == DataControlRowType.DataRow)
        //    {
        //        Entities.NotaPedidoEntity Item = (Entities.NotaPedidoEntity)r.DataItem;
        //        CheckBox cbContacto
        //    }
        //}

        protected void gvContacto_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int IDProyecto = Convert.ToInt16(txtIDProyecto.Text);
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();

            if (e.CommandName == "GuardarSeleccion")
            {
                foreach (GridViewRow row in gvContacto.Rows)
                    if (row.RowType == DataControlRowType.DataRow)
                        ContactoBL.SetVincularContactoProyecto(IDProyecto, Convert.ToInt16(gvContacto.DataKeys[row.RowIndex].Value.ToString()), IDUsuario, (row.Cells[0].FindControl("cbContacto") as CheckBox).Checked);

                btnFinalizarCreacion.Visible = ProyectoBL.GetValidaCreacionProyecto(IDProyecto);
            }
        }

        protected void btnFinalizarCreacion_ServerClick(object sender, EventArgs e)
        {
            ClearViewState();
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
            int IDNotificacionApp = 0;

            try
            {
                int IDProyecto = Convert.ToInt16(hfIDProyecto.Value);
                //Agrega el o los estados de pago a las notas de pedido creadas
                //Prepara y deja los estados de pago listo para ser facturados
                //Deja el proyecto como activo para ser buscado
                //Todas las notas de pedido quedan como estado "Por ajustar" ID: 1
                ProyectoBL.AddFinalizarCreacion(IDProyecto, IDUsuario, ref IDNotificacionApp);

                //NOTIFICACIÓN (1): Notifica a los usuarios que se creó un proyecto
                hubs.NotificacionAppHub.EnviaNotificacionesUsuariosConectados(GlobalHost.ConnectionManager.GetHubContext<hubs.NotificacionAppHub>());

                //una vez creado el proyecto se redirecciona al mismo 
                Response.Redirect("visualizar/notas-de-pedido.aspx?id=" + IDProyecto);

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        protected void txtPorcentaje_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPorcentaje.Text.Trim()))
            {
                int IDProyecto = Convert.ToInt16(hfIDProyecto.Value);
                double ValorTotalProyecto = ProyectoBL.GetValorTotalProyecto(IDProyecto);

                txtMonto.Text = (ValorTotalProyecto * (Convert.ToDouble(txtPorcentaje.Text) / 100)).ToString();

                BindDataNP(IDProyecto);

                txtMonto.Enabled = false;
                txtPorcentaje.Enabled = true;
            }
        }

        protected void txtMonto_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtMonto.Text.Trim()))
            {
                int IDProyecto = Convert.ToInt16(hfIDProyecto.Value);
                double ValorTotalProyecto = ProyectoBL.GetValorTotalProyecto(IDProyecto);
                double Monto = Convert.ToDouble(txtMonto.Text.Trim());

                txtPorcentaje.Text = ((Monto / ValorTotalProyecto) * 100).ToString();
                txtMonto.Text = txtMonto.Text.Replace(".", "");

                BindDataNP(IDProyecto);

                txtMonto.Enabled = true;
                txtPorcentaje.Enabled = false;
            }
        }

        [WebMethod]
        public static string[] GetCliente(string prefix)
        {
            return ClienteBL.GetClienteJSON(prefix).ToArray();
        }

        [WebMethod]
        public static List<string> GetProyectoPadre(string prefix)
        {
            return ProyectoBL.GetProyectoPadreJSON(prefix);
        }

        [WebMethod]
        public static string[] GetEjecutivoComercial(string prefix)
        {
            return UsuarioBL.GetEjecutivoComercialJSON(prefix).ToArray();
        }
    }
}
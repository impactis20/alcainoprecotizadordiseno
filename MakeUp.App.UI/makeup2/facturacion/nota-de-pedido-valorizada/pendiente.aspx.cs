﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.facturacion.nota_de_pedido_valorizada
{
    public partial class pendiente : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindGridView();
        }

        private void BindGridView()
        {
            List<Entities.ProyectoEntity> Lst = FacturacionBL.GetNPxFacturar();
            gvNPValorizada.DataSource = Lst;
            gvNPValorizada.DataKeyNames = new string[1] { "ID" }; //PK
            gvNPValorizada.DataBind();

            if (Lst.Count > 0)
            {
                lblContador.Text = string.Format("<b>{0}</b> proyectos pendientes de facturación.", Lst.Count);
                lblContador.CssClass = "alert-info";

                gvNPValorizada.UseAccessibleHeader = true;
                gvNPValorizada.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                lblContador.Text = string.Format("No hay proyectos pendientes de facturación.", Lst.Count);
                lblContador.CssClass = "alert-danger";
            }
        }

        protected void gvNPValorizada_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.ProyectoEntity Item = (Entities.ProyectoEntity)r.DataItem;

                Label lblPendiente = (r.Controls[0].FindControl("lblPendiente") as Label);
                lblPendiente.Text = string.Format("{0} {1}", Item.TipoValor.Sigla, Util.Funcion.FormatoNumerico(Item.ValorizadoPendiente));
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.SignalR;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.facturacion.nota_de_pedido_valorizada
{
    public partial class detalle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    CargaInicial(Convert.ToInt16(Request.QueryString["id"].ToString()));



                }
            }
        }


        private void CargaInicial(int ID)
        {
            Entities.ProyectoEntity Proyecto = ProyectoBL.GetCabeceraProyecto(ID);
            lblID.Text = Proyecto.ID.ToString();
            hfIdCliente.Value = Proyecto.Cliente.ID.ToString();
            string razonSocial = Proyecto.Cliente.RazonSocial.ToString();
            string rut = Proyecto.Cliente.rutCliente.ToString();

            if (Proyecto.Padre != null)
            {
                padre.Visible = true;
                lblPadre.Text = string.Format("{0} - {1}", Proyecto.Padre.ID, Proyecto.Padre.Nombre);
            }
            else
                padre.Visible = false;

            lblNombreProyecto.Text = Proyecto.Nombre;
            lblCliente.Text = Proyecto.Cliente.NombreFantasia;
            lblFechaIngreso.Text = Proyecto.FechaIngreso.ToShortDateString();
            lblFechaPlanificada.Text = Proyecto.FechaPlanificada.ToShortDateString();
            lblEjecutivo.Text = string.Format("{0} {1} {2}", Proyecto.EjecutivoComercial.Nombre, Proyecto.EjecutivoComercial.ApellidoPaterno, Proyecto.EjecutivoComercial.ApellidoMaterno);
            lblTotalNP.Text = Proyecto.CantidadTotalNP.ToString();
            lblTipoValor.Text = Proyecto.TipoValor.Sigla;
            hfIDTipoValor.Value = Proyecto.TipoValor.ID.ToString();
            lblRazonSocial.Text = razonSocial;
            lblRut.Text = Util.Funcion.FormatoRut(rut);

            //verifica el tipo de valor para convertir número (con decimales o sin decimal)
            if (Proyecto.TipoValor.Valor > 1)
            {
                lblValorProyecto.Text = Util.Funcion.FormatoNumerico(Proyecto.ValorTotal);
                ViewState.Add("ValorTipoValor", Proyecto.TipoValor.Valor);
            }
            else
                lblValorProyecto.Text = Util.Funcion.FormatoNumericoEntero(Proyecto.ValorTotal);

            //actualiza Gridview con NP 
            BindData(ID);

            nombreProyecto.InnerText = Proyecto.Nombre;
            txtGlosa.Attributes.Add("onkeyup", "countChar(this, '" + lblGlosa.ClientID + "', 80)");
        }

        private void BindData(int ID)
        {
            List<Entities.NotaPedidoEntity> Lst = FacturacionBL.GetNPParaFacturar(ID);
            gvNP.DataKeyNames = new string[1] { "ID" }; //PK
            gvNP.DataSource = Lst;
            gvNP.DataBind();

            if (Lst.Count > 0)
            {
                gvNP.UseAccessibleHeader = true;
                gvNP.HeaderRow.TableSection = TableRowSection.TableHeader;

                //El botón facturar se visualiza si tiene notas de pedido por facturar
                int ContadorNPxFacturar = Lst.Where(x => x.IDEstadoNP == 3).Count();
                if (ContadorNPxFacturar > 0)
                    btnFacturar.Visible = true;
                else
                    btnFacturar.Visible = false;
            }
        }

        protected void gvNP_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;


            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.NotaPedidoEntity NP = (Entities.NotaPedidoEntity)r.DataItem;

                Label lblColor = (r.Controls[0].FindControl("lblColor") as Label);
                CheckBox cbFacturar = (r.Controls[0].FindControl("cbFacturar") as CheckBox);
                Label lblValorXFacturar = (r.Controls[0].FindControl("lblValorXFacturar") as Label);
                HyperLink detalleNP = (r.Controls[0].FindControl("detalleNP") as HyperLink);
                Label lblValorizador = (r.Controls[0].FindControl("lblValorizador") as Label);
                Label lblFechaEntrega = (r.Controls[0].FindControl("lblFechaEntrega") as Label);

                cbFacturar.Text = NP.IDCompuesta;

                switch (NP.IDEstadoNP)
                {
                    case 1:
                        lblColor.CssClass = "label label-danger";
                        cbFacturar.Checked = false; cbFacturar.Enabled = false;
                        detalleNP.Text = NP.Creacion.Descripcion;
                        detalleNP.Attributes.Add("data-content", string.Format("<b>Cantidad:</b> {0}<br><b>Unidad:</b> {1}<br><b>Valor Unitario:</b> {2} {3}<br><b>Descripción:</b> {4}<br><br><var>Creado por {5} {6}</var>", Util.Funcion.FormatoNumerico(NP.Creacion.Cantidad), NP.Creacion.TipoUnidad.Nombre, lblTipoValor.Text, Util.Funcion.FormatoNumerico(NP.Creacion.ValorUnitario), NP.Creacion.Descripcion, NP.Creacion.Usuario.Nombre, NP.Creacion.Usuario.ApellidoPaterno));
                        break;
                    case 2:
                        lblColor.CssClass = "label label-warning";
                        cbFacturar.Checked = false; cbFacturar.Enabled = false;
                        detalleNP.Text = NP.Ajuste.Descripcion;
                        detalleNP.Attributes.Add("data-content", string.Format("<b>Cantidad:</b> {0}<br><b>Unidad:</b> {1}<br><b>Valor Unitario:</b> {2} {3}<br><b>Descripción:</b> {4}<br><br><var>Creado por {5} {6}</var>", Util.Funcion.FormatoNumerico(NP.Ajuste.Cantidad), NP.Creacion.TipoUnidad.Nombre, lblTipoValor.Text, Util.Funcion.FormatoNumerico(NP.Creacion.ValorUnitario), NP.Ajuste.Descripcion, NP.Creacion.Usuario.Nombre, NP.Creacion.Usuario.ApellidoPaterno));
                        lblFechaEntrega.Text = NP.Ajuste.FechaRealEntrega.ToShortDateString();
                        break;
                    default:
                        lblColor.CssClass = NP.IDEstadoNP == 3 ? "label label-success" : "label label-primary";

                        if (NP.IDEstadoNP == 3)
                        {
                            cbFacturar.Checked = false;
                            cbFacturar.Enabled = true;
                        }
                        else
                        {
                            cbFacturar.Checked = true;
                            cbFacturar.Enabled = false;
                        }

                        detalleNP.Text = NP.Valorizacion.Descripcion;
                        detalleNP.Attributes.Add("data-content", string.Format("<b>Cantidad:</b> {0}<br><b>Unidad:</b> {1}<br><b>Valor Unitario:</b> {2} {3}<br><b>Descripción:</b> {4}<br><br><var>Creado por {5} {6}</var>", Util.Funcion.FormatoNumerico(NP.Valorizacion.Cantidad), NP.Creacion.TipoUnidad.Nombre, lblTipoValor.Text, Util.Funcion.FormatoNumerico(NP.Valorizacion.ValorUnitario), NP.Valorizacion.Descripcion, NP.Creacion.Usuario.Nombre, NP.Creacion.Usuario.ApellidoPaterno));

                        string ValorNP = string.Empty;

                        if (ViewState["ValorTipoValor"] != null)
                            ValorNP = Util.Funcion.FormatoNumerico(NP.Valorizacion.ValorPorFacturar);
                        else
                            ValorNP = Util.Funcion.FormatoNumericoEntero(NP.Valorizacion.ValorPorFacturar);


                        //si la nota de pedido esta en Estatus 4 es porque se cobro, por lo que debe mostrarse en Valor por facturar Monto 0. Gloria Molina
                        string valorPago = "00,00";

                        if (NP.IDEstadoNP == 4)
                        {
                            lblValorXFacturar.Text = string.Format("{0} {1}", lblTipoValor.Text, valorPago);
                        }
                        else
                        {
                            lblValorXFacturar.Text = string.Format("{0} {1}", lblTipoValor.Text, ValorNP);
                        }

                        //lblValorXFacturar.Text = string.Format("{0} {1}", lblTipoValor.Text, ValorNP);

                        lblValorizador.Text = string.Format("{0} {1}", NP.Valorizacion.Usuario.Nombre, NP.Valorizacion.Usuario.ApellidoPaterno);
                        lblFechaEntrega.Text = NP.Ajuste.FechaRealEntrega.ToShortDateString();
                        break;
                }
                //data-content='<b>Cantidad:</b> 2<br><b>Unidad:</b> Kilogramo<br><b>Valor Unitario:</b> USD 86,69527896995708<br><b>Descripción:</b> Somos una compañía dedicada a la comercialización, diseño, fabricación e instalación de letreros luminosos, estructuras publicitarias y elementos de imagen...<br><br><var>Creado por Patricio Silva</var>'
            }
        }

        protected void btnFacturar_Click(object sender, EventArgs e)
        {
            txtNroOC.Text = "";
            lblinfoNOC.Text = "";
            lblinfoNHES.Text = "";
            txtNroHes.Text = "";
            txtFechaOC.Text = "";
            txtFechaHes.Text = "";
            lblFHES.Text = "";
            lblFOC.Text = "";
            int IDProyecto = Convert.ToInt16(Request.QueryString["id"].ToString());
            int ContNPSeleccionada = 0;
            decimal ValorTotalFacturacion = 0;
            List<Entities.NotaPedidoEntity> NPSeleccionada = new List<Entities.NotaPedidoEntity>();
            List<Entities.NotaPedidoEntity> Lst = FacturacionBL.GetNPParaFacturar(IDProyecto);

            //valida que hayan NP seleccionadas
            foreach (GridViewRow r in gvNP.Rows)
            {
                if (r.RowType == DataControlRowType.DataRow)
                {
                    CheckBox cbFacturar = (r.Controls[0].FindControl("cbFacturar") as CheckBox);

                    if (cbFacturar.Enabled && cbFacturar.Checked)
                    {
                        int IDNPSeleccionada = Convert.ToInt16(gvNP.DataKeys[r.RowIndex].Value);
                        Entities.NotaPedidoEntity NP = new Entities.NotaPedidoEntity();
                        NP.IDCompuesta = cbFacturar.Text;
                        NP.Valorizacion.ValorPorFacturar = Lst.Where(x => x.ID == IDNPSeleccionada).ToList()[0].Valorizacion.ValorPorFacturar;
                        ValorTotalFacturacion += NP.Valorizacion.ValorPorFacturar;

                        NPSeleccionada.Add(NP);

                        ContNPSeleccionada++;
                    }
                }
            }

            if (ContNPSeleccionada > 0)
            {
                nroNPSeleccionada.InnerText = string.Format("{0} Nota(s) de Pedido", ContNPSeleccionada);

                gvDetalle.DataSource = NPSeleccionada;
                gvDetalle.DataBind();

                //asigna valor total a facturar (Footer gridview gvDetalle)
                (gvDetalle.FooterRow.FindControl("lblValorTotalFacturacion") as Label).Text = string.Format("{0} {1}", lblTipoValor.Text, Util.Funcion.FormatoNumerico(ValorTotalFacturacion));

                if (NPSeleccionada.Count > 0)
                {
                    gvDetalle.UseAccessibleHeader = true;
                    gvDetalle.HeaderRow.TableSection = TableRowSection.TableHeader;
                }

                int IDTipoValor = Convert.ToInt16(hfIDTipoValor.Value);
                hfValorFacturacion.Value = ValorTotalFacturacion.ToString();

                //2 es UF. Cuando es tipo de valor UF se muestra el calendario para seleccionar la fecha con valor a facturar
                if (IDTipoValor == 2)
                {
                    lblInfoFechaFacturacion.Visible = true;
                    calendarioFechaFacturacion.Visible = true;
                    rfvFechaFacturacion.ValidationGroup = rfvFechaVencimiento.ValidationGroup;
                    rfvFechaFacturacion.DataBind();
                }


                ScriptManager.RegisterStartupScript(this, GetType(), "Facturacion", "$('#" + facturar.ClientID + "').modal({backdrop: 'static',keyboard: true});", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop234", Util.GetMensajeAlerta("warning", "Facturación incorrecta...", "Para facturar debe seleccionar al menos una nota de pedido valorizada."), true);
            }
        }

        protected void Check_Changed(Object sender, EventArgs e)
        {

            foreach (GridViewRow r in gvNP.Rows)
            {
                CheckBox cbFacturar = (r.Controls[0].FindControl("cbFacturar") as CheckBox);
                CheckBox checkAll = (CheckBox)sender;
                if (checkAll.Checked && cbFacturar.Enabled)
                {
                    cbFacturar.Checked = true;
                }
                else if (!checkAll.Checked)
                {
                    cbFacturar.Checked = false;
                }
            }
        }


        protected void gvDetalle_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.NotaPedidoEntity item = (Entities.NotaPedidoEntity)r.DataItem;

                Label lblNPSeleccionada = (r.Controls[0].FindControl("lblNPSeleccionada") as Label);
                Label lblValor = (r.Controls[0].FindControl("lblValor") as Label);

                lblNPSeleccionada.Text = item.IDCompuesta;
                lblValor.Text = string.Format("{0} {1}", lblTipoValor.Text, Util.Funcion.FormatoNumerico(item.Valorizacion.ValorPorFacturar));
            }
        }

        private void Facturar()
        {

            int IDProyecto = Convert.ToInt16(Request.QueryString["id"].ToString());
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
            List<Entities.NotaPedidoEntity> Lst = new List<Entities.NotaPedidoEntity>();
            int IDTipoValor = Convert.ToInt16(hfIDTipoValor.Value);
            int IDCliente = Convert.ToInt16(hfIdCliente.Value);
            string CondicionPago = ddlCondicionPago.SelectedItem.Value;
            DateTime FechaVencimiento = Convert.ToDateTime(txtFechaVencimiento.Text);
            string NroOC = txtNroOC.Text;
            DateTime FechaOC = string.IsNullOrEmpty(txtFechaOC.Text) ? DateTime.MinValue : Convert.ToDateTime(txtFechaOC.Text);
            string NroHes = txtNroHes.Text;
            DateTime FechaHes = string.IsNullOrEmpty(txtFechaHes.Text) ? DateTime.MinValue : Convert.ToDateTime(txtFechaHes.Text);
            string Glosa = txtGlosa.Text.Trim();
            decimal ValorTotalFactura = IDTipoValor == 2 ? Convert.ToDecimal(txtTotalFacturacion.Text) : Convert.ToDecimal(hfValorFacturacion.Value);
            int IDNotificacionApp = 0;

            try
            {

                foreach (GridViewRow r in gvNP.Rows)
                {

                    CheckBox cbFacturar = (r.Controls[0].FindControl("cbFacturar") as CheckBox);

                    if (cbFacturar.Enabled && cbFacturar.Checked)
                    {
                        int IDNP = Convert.ToInt16(gvNP.DataKeys[r.RowIndex].Value);
                        Lst.Add(new Entities.NotaPedidoEntity(IDNP));
                    }

                }


                //Respuesta: Devuelve 0 cuando la facturación no se pudo realizar porque una de las NP seleccionadas ya fue facturada,
                //           Devuelve - 1 cuando el proyecto está en USD o Euro
                //           De lo contrario devuelve el nro de la factura que genera Manager
                int Respuesta = FacturacionBL.AddFacturacionxNP(IDProyecto, IDUsuario, IDCliente, ValorTotalFactura, CondicionPago, FechaVencimiento, NroOC, FechaOC, NroHes, FechaHes, Glosa, Lst, ref IDNotificacionApp);

                if (Respuesta == 0)
                {
                    //Otro usuario ya facturó alguna NP seleccionada
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error al generar la factura...", "Una de las NP seleccionadas ya fue facturadas. Reingrese y vuelva a intentar."), true);
                }
                else
                {
                    //La facturación se genero correctamente

                    //NOTIFICACIÓN(14): Notifica a los usuarios que se facturaron(1 a n) notas de pedido
                    hubs.NotificacionAppHub.EnviaNotificacionesUsuariosConectados(GlobalHost.ConnectionManager.GetHubContext<hubs.NotificacionAppHub>());

                    Entities.ProyectoEntity Proyecto = ProyectoBL.GetCabeceraProyecto(IDProyecto);

                    string NombreProyecto = string.Format("{0} - {1}", IDProyecto, Proyecto.Nombre);
                    string NombreUsuario = UsuarioBL.GetUsuario(IDUsuario, string.Empty, string.Empty, string.Empty, null, false)[0].Nombre;

                    //notifica vía correo electrónico al usuario facturador de manager
                    string UrlProyecto = string.Format("http://{0}/makeup2/proyecto/visualizar/datos.aspx?id={1}", Request.Url.Authority, IDProyecto);
                    string Para = ConfigurationManager.AppSettings["emailsOrdenFacturacion"];
                    string Asunto = string.Format("Orden de Facturación Proyecto: {0}", NombreProyecto);
                    string Cuerpo = string.Format("<span style=\"color:#1f497d\">Estimada <b>Luisa Uribe</b>,<br><br>" +
                    "Mediante el presente le informamos a usted que con fecha <b>{0}</b> el ejecutivo comercial <b>{1}</b> ha dado orden de facturación al proyecto <b style=\"text-decoration:none\"><a href={2} target=\"_blank\">{3}</a></b> del cliente <b>{4}</b> por un monto de <b>$ {5}</b>.<br><br>" +
                    "</span>",
                    DateTime.Now.ToShortDateString(), //fecha de envío
                    NombreUsuario, //nombre de usuario
                    Util.GetComillas(UrlProyecto), //url del proyecto
                    NombreProyecto, //nombre del proyecto
                    Proyecto.Cliente.RazonSocial, //nombre de fantasía
                    Util.Funcion.FormatoNumericoEntero(ValorTotalFactura)); //valor de la factura

                    //cuando el proyecto es CLP o UF se incluye el nro de la factura de Manager en el correo
                    if (Respuesta != -1) //Respuesta tiene valor -1 cuando el proyecto es USD o Euro
                        Cuerpo += string.Format("<span style=\"color:#1f497d\">El correlativo asignado en Manager para esta factura es: <b>{0}</b></span>.", Respuesta);

                    Cuerpo += "<span style=\"color:#1f497d\"><br><br>Si tiene dudas o consultas diríjase a su administrador.<br><br>Atte.<br>Sistema Make Up.</span>";

                    //credenciales correo de envío MakeUp
                    string Remitente = ConfigurationManager.AppSettings["remitenteMakeUp"];
                    string De = ConfigurationManager.AppSettings["emailMakeUp"];
                    string Ps = ConfigurationManager.AppSettings["passwordMakeUp"];

                    string RespuestaWS = App_Code.WebService.Email.EnviarEmail(De, Ps, De, Para, string.Empty, string.Empty, Remitente, Asunto, Cuerpo);

                    bool CorreoEnviado = Convert.ToBoolean(Convert.ToInt16(RespuestaWS.Split('.')[0]));

                    string Mensaje = "Se generó correctamente la facturación mediante notas de pedido valorizadas.";
                    string IconoEmail = "<br><i class=\"fa fa-envelope-o\" aria-hidden=\"true\" style=\"margin-left: 30px; margin-right: 4px\">&nbsp;&nbsp;</i>";
                    string EmailNotificacion = string.Format("{0}{1}", IconoEmail, Para.Replace(",", IconoEmail));

                    if (CorreoEnviado)
                        Mensaje += string.Format("<br><br><i class=\"fa fa-check text-success\" aria-hidden=\"true\" style=\"margin-eft: 4px;margin-right: 4px\">&nbsp;&nbsp;</i> Notificación enviada correctamente a:{0}", EmailNotificacion);
                    else
                        Mensaje += string.Format("<br><br><i class=\"fa fa-exclamation-triangle text-danger\" aria-hidden=\"true\" style=\"margin-eft: 4px;margin-right: 4px\">&nbsp;&nbsp;</i>Por un error interno de servicio la notificación de correo electrónico no pudo ser enviada a:{0}", EmailNotificacion);

                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop307", Util.GetMensajeAlerta("success", "Factura generada correctamente...", Mensaje), true);
                }

                //limpia formulario (condición de pago y glosa
                ddlCondicionPago.SelectedIndex = -1;
                txtGlosa.Text = string.Empty;

                //actualiza gridview para bloquear las NP que se facturaron
                BindData(IDProyecto);

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop55", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }


        }

        protected void Continuar_ServerClick(object sender, EventArgs e)
        {
            Facturar();
        }
        protected void btnConfirmaFacturacion_ServerClick(object sender, EventArgs e)
        {

            hfFechaVencimiento.Value = txtFechaVencimiento.Text;
            hfNOC.Value = txtNroOC.Text;

            hfFechaOC.Value = txtFechaOC.Text;
            hfNHes.Value = txtNroHes.Text;
            hfFNHes.Value = txtFechaHes.Text;
            hfGlosa.Value = txtGlosa.Text;
            hfFechaFacturación.Value = txtFechaFacturacion.Text;
            hfValorDia.Value = lblValorDia.Text;
            hfTotalFacturar.Value = txtTotalFacturacion.Text;
            hfCondicionPago.Value = ddlCondicionPago.SelectedItem.Value;

            if (txtNroOC.Text == "" && txtFechaOC.Text == "" && txtNroHes.Text == "" && txtFechaHes.Text == "")
            {
                lblmensaje.CssClass = "alert-danger";
                lblmensaje.Text = "No ha ingresado datos para Nro/Fecha OC, Nro/Fecha HES, ¿Desea continuar?";
                ClientScript.RegisterStartupScript(this.GetType(), "myScript", "Mensaje();", true);

            }

            else if (txtNroOC.Text != "" && txtFechaOC.Text != "" && txtNroHes.Text == "" && txtFechaHes.Text == "")
            {
                lblmensaje.CssClass = "alert-danger";
                lblmensaje.Text = "Ha ingresado Nro/fecha OC sin ingresar Nro/Fecha HES, ¿Desea continuar?";
                ClientScript.RegisterStartupScript(this.GetType(), "myScript", "Mensaje();", true);

            }
            else if (txtNroOC.Text == "" && txtFechaOC.Text == "" && txtNroHes.Text != "" && txtFechaHes.Text != "")
            {
                lblmensaje1.CssClass = "alert-danger";
                lblmensaje1.Text = "Debe ingresar Nro/Fecha OC.";
                ClientScript.RegisterStartupScript(this.GetType(), "myScript", "Mensaje1();", true);

            }
            else if (txtNroOC.Text != "" && txtFechaOC.Text == "")
            {
                lblFHES.Text = "";
                lblinfoNOC.Text = "";
                lblinfoNHES.Text = "";
                //lblinfoNOC.Text = "Debe ingresar Fecha OC para Numero OC.";
                lblFOC.Text = "Debe ingresar Fecha OC para Numero OC.";
                ScriptManager.RegisterStartupScript(this, GetType(), "Facturacion", "$('#" + facturar.ClientID + "').modal({backdrop: 'static',keyboard: true});", true);
            }
            else if (txtNroOC.Text == "" && txtFechaOC.Text != "")
            {
                lblFHES.Text = "";
                lblinfoNHES.Text = "";
                lblFOC.Text = "";
                //lblFOC.Text = "Debe ingresar Numero OC para Fecha OC.";
                lblinfoNOC.Text = "Debe ingresar Numero OC para Fecha OC.";
                ScriptManager.RegisterStartupScript(this, GetType(), "Facturacion", "$('#" + facturar.ClientID + "').modal({backdrop: 'static',keyboard: true});", true);
            }
            else if (txtNroHes.Text != "" && txtFechaHes.Text == "")
            {
                lblinfoNHES.Text = "";
                lblFOC.Text = "";
                lblinfoNOC.Text = "";
                //lblinfoNHES.Text = "Debe ingresar Fecha HES para Numero HES.";
                lblFHES.Text = "Debe ingresar Fecha HES para Numero HES.";
                ScriptManager.RegisterStartupScript(this, GetType(), "Facturacion", "$('#" + facturar.ClientID + "').modal({backdrop: 'static',keyboard: true});", true);

            }
            else if (txtNroHes.Text == "" && txtFechaHes.Text != "")
            {
                lblFOC.Text = "";
                lblFHES.Text = "";
                lblinfoNOC.Text = "";
                lblinfoNHES.Text = "Debe ingresar Fecha HES para Numero HES.";
                //lblFHES.Text = "Debe ingresar Fecha HES para Numero HES.";
                ScriptManager.RegisterStartupScript(this, GetType(), "Facturacion", "$('#" + facturar.ClientID + "').modal({backdrop: 'static',keyboard: true});", true);
            }
            else if (txtNroOC.Text != "" && txtFechaOC.Text != "" && txtNroHes.Text != "" && txtFechaHes.Text != "")
            {
                Facturar();
            }
        }

        protected void btnVolver_ServerClick(object sender, EventArgs e)
        {
            lblinfoNHES.Text = "";
            lblinfoNOC.Text = "";
            lblFOC.Text = "";
            lblFHES.Text = "";

            ScriptManager.RegisterStartupScript(this, GetType(), "Facturacion", "$('#" + facturar.ClientID + "').modal({backdrop: 'static',keyboard: true});", true);

            txtFechaFacturacion.Text = hfFechaFacturación.Value;
            DateTime fechaFact = Convert.ToDateTime(hfFechaFacturación.Value);
            double ValorDia = TipoValorBL.GetValorTipoValor(2, fechaFact);
            string valorDiaF = Convert.ToString(string.Format("{0:#,0.##}", ValorDia));
            string moneda = "CLP";

            txtFechaVencimiento.Text = hfFechaVencimiento.Value;
            txtNroOC.Text = hfNOC.Value;
            txtFechaOC.Text = hfFechaOC.Value;
            txtNroHes.Text = hfNHes.Value;
            txtFechaHes.Text = hfFNHes.Value;
            txtGlosa.Text = hfGlosa.Value;
            txtFechaFacturacion.Text = hfFechaFacturación.Value;
            lblValorDia.Text = string.Format("{0} {1}", moneda, valorDiaF);
            txtTotalFacturacion.Text = hfTotalFacturar.Value;
            ddlCondicionPago.SelectedItem.Value = hfCondicionPago.Value;
        }


        [WebMethod]
        public static float GetValorUF(string date)
        {
            DateTime Fecha = Convert.ToDateTime(date);

            return TipoValorBL.GetValorTipoValor(2, Fecha);
        }
    }
}
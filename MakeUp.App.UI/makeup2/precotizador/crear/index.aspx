﻿<%@ Page Title="" Language="C#" MasterPageFile="~/makeup2/menu.master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="MakeUp.App.UI.makeup2.precotizador.crear.index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headMenu" runat="server">
    <link href="../../../css/bootstrap-datetimepicker.css" rel="stylesheet" />
    <link href="../../../css/bootstrap-select.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-datetimepicker.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('[id*=dFechaEntrega]').datetimepicker({
                useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                format: 'DD-MM-YYYY',
                maxDate: moment().add(1, 'days'), //permite hasta la fecha de hoy
                showClear: true,
                ignoreReadonly: true
            });
            
            //inicia combobox por defecto
            $('.selectpicker').selectpicker({
                title: 'Seleccione...',
                showTick: true
            });
        });
     
    </script>
    <div class="row">
   <div class="col-lg-12">
   <h2 class="page-header">Crear oportunidad de negocio</h2>
    <div class="form-group">
        <div class="row">
            <div class="col-lg-3">
                <div class="form-group">
                    <label>Cliente</label>
                    <asp:ListBox ID="lstCliente" runat="server" SelectionMode="Single" CssClass="selectpicker disabled" AutoPostBack="true" data-width="100%"></asp:ListBox>            
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label>Nuevo Cliente</label>
                    <asp:TextBox ID="txtNuevoCliente" runat="server" CssClass="form-control input-sm"  autocomplete="off"></asp:TextBox>
                </div>
           </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="form-group">
                    <label>Contacto</label>
                    <asp:TextBox ID="txtContacto" runat="server" CssClass="form-control input-sm"  autocomplete="off"></asp:TextBox>
                    </div>
                </div>
              <div class="col-lg-3">
                <div class="form-group">
                    <label>Teléfono</label>
                    <asp:TextBox ID="txtTelefono" runat="server" CssClass="form-control input-sm"  autocomplete="off"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="form-group">
                    <label>Email</label>
                    <div class="input-group input-group">
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control input-sm disabled"  autocomplete="off"></asp:TextBox>
                    <span class="input-group-addon">
                        <span class="fa fa-at"></span>
                    </span>
                 </div>
                </div>
           </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label>Fecha Comprometida (Cotización)</label>
                    <div class="input-group date input-group" id="dFechaEntrega">
                        <asp:TextBox ID="txtFechaComprometida" runat="server" CssClass="form-control input-sm disabled"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </span>
                    </div>
                </div>
           </div>
            </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Generador del Negocio</label>
                    <asp:ListBox ID="lstGenerador" runat="server" SelectionMode="Single" CssClass="selectpicker" AutoPostBack="true" data-width="100%"></asp:ListBox>            
                </div>
           </div>
            </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Responsable seguimiento y cierre</label>
                    <asp:ListBox ID="lstResponsable" runat="server" SelectionMode="Single" CssClass="selectpicker" AutoPostBack="true" data-width="100%"></asp:ListBox>            
                </div>
           </div>
            </div>
            <div class="row">
             <div class="col-lg-6">
                <div class="form-group">
                    <label>Título Referencial</label>
                    <asp:TextBox ID="txtTitulo" runat="server" CssClass="form-control input-sm"  autocomplete="off"></asp:TextBox>
                 </div>
                </div> 
                </div>
            <div class="row">
               <div class="col-lg-6">
                  <div class="form-group">
                    <label>Descripción Comercial</label>
                    <asp:TextBox ID="txtDescripcionComercial" runat="server" CssClass="form-control input-sm"  autocomplete="off"></asp:TextBox>
                  </div>
                </div>
            </div>
        <div class="row">
               <div class="col-lg-6">
                  <div class="form-group">
                    <label>Descripción Técnica</label>
                    <asp:TextBox ID="txtDescripcionTecnica" runat="server" CssClass="form-control input-sm"  autocomplete="off"></asp:TextBox>
                </div>
              </div>
           </div>
          <div class="row">
            <div class="col-lg-12">
                <div class="pull-left">
                    <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" CssClass="btn btn-primary"/>
                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn btn-default"/>
                </div>
              </div>
           </div>
        </div>
    </div>
</div>
</asp:Content>

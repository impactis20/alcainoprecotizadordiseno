﻿<%@ Page Language="C#" MasterPageFile="~/makeup2/precotizador/seguimiento/seguimiento.Master" AutoEventWireup="true" CodeBehind="cubicacion.aspx.cs" Inherits="MakeUp.App.UI.makeup2.precotizador.seguimiento.cubicacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headSeguimiento" runat="server">
    <link href="../../../css/bootstrap-datetimepicker.css" rel="stylesheet" />
    <link href="../../../css/bootstrap-select.css" rel="stylesheet" />
    <link href="../../../css/fileinput.min.css" rel="stylesheet" />
    <link href="../../../css/bootstrap-typeahead.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenidoSeguimiento" runat="server">
    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-datetimepicker.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/fileinput.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/autocomplete-query.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap3-typeahead.min.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/languages/es.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/count-character-textbox.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/dropdown-igualitario.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/number-format-using-numeral.js") %>'></script>


</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MakeUp.App.UI.makeup2.precotizador.seguimiento
{
    public partial class seguimiento : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargaInicial();
            }
            EstableceActiveTab();
            //CheckControlEnabled();
        }

        private void CargaInicial()
        {
                titulo.InnerHtml = "No existe... <i class=\"fa fa-frown-o\" aria-hidden=\"true\"></i>";
                msjGeneral.Visible = false;
                tabs.Visible = false;

        }

        private void CheckControlEnabled()
        {
            //oculta controles restringidos
            App_Code.Seguridad.GetControlAutorizado(Page, "/makeup2/precotizador/seguimiento");
        }

        private void EstableceActiveTab()
        {
            for (int x = 0; x < tabs.Controls.Count; x++)
            {
                if (x % 2 == 1)
                {
                    string Url = Request.Url.AbsolutePath;
                    HyperLink link = (HyperLink)tabs.Controls[x];
                    link.NavigateUrl = string.Format("{0}", link.NavigateUrl.Split('=')[0]);

                    LiteralControl li = tabs.Controls[x - 1] as LiteralControl;

                    if (Url.Contains(link.NavigateUrl.Replace("~", string.Empty).Split('?')[0]))
                        li.Text = li.Text.Replace("<li>", "<li class='active'>");
                    else
                        li.Text = li.Text.Replace("<li class='active'>", "<li>");
                }
            }
        }

    }
}
﻿using System;
using System.Web.Configuration;
using System.Web;
using System.Web.Security;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2
{
    public partial class header : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                CargarPerfilUsuario();
        }

        private void CargarPerfilUsuario()
        {
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
            Entities.UsuarioEntity Usuario = UsuarioBL.GetUsuario(IDUsuario, string.Empty, string.Empty, string.Empty, null, false)[0]; //Busca info usuario
            lblNombre.Text = Usuario.Nombre;
            lblRol.Text = Usuario.Rol.Nombre;
            lblEmail.Text = Usuario.Email;
        }

        protected void btnCerrarSesion_ServerClick(object sender, EventArgs e)
        {
            if (Request.Cookies["dReUser_"] != null)
            {
                Request.Cookies["dReUser_"].Expires = DateTime.Now;
                Response.Cookies.Add(Request.Cookies["dReUser_"]);
            }

            FormsAuthentication.SignOut();
            Session.Abandon();

            // clear authentication cookie
            HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie1.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie1);

            // clear session cookie (not necessary for your current problem but i would recommend you do it anyway)
            SessionStateSection sessionStateSection = (SessionStateSection)WebConfigurationManager.GetSection("system.web/sessionState");
            HttpCookie cookie2 = new HttpCookie(sessionStateSection.CookieName, "");
            cookie2.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie2);

            //FormsAuthentication.RedirectToLoginPage();
            Response.Redirect(FormsAuthentication.LoginUrl, false);
        }
    }
}
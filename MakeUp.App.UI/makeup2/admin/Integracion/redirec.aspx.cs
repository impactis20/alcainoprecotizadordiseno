﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MakeUp.App.UI.makeup2.admin.Integracion
{
    public partial class redirec : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Request.Params["id"]);
            if (id == 1)
            {
                Response.Redirect("detalles1.aspx?id=1");
            }
            if (id == 2)
            {
                Response.Redirect("detalles2.aspx?id=2");
            }
            if (id == 3)
            {
                Response.Redirect("detalles3.aspx?id=3");
            }
            if (id == 4)
            {
                Response.Redirect("detalles4.aspx?id=4");
            }
            if (id == 5)
            {
                Response.Redirect("detalles5.aspx?id=5");
            }
            if (id == 6)
            {
                Response.Redirect("detalles6.aspx?id=6");
            }
            if (id == 7)
            {
                Response.Redirect("detalles7.aspx?id=7");
            }
            if (id == 8)
            {
                Response.Redirect("detalles8.aspx?id=8");
            }
            if (id == 9)
            {
                Response.Redirect("detalles9.aspx?id=9");
            }
            if (id == 10)
            {
                Response.Redirect("detalles10.aspx?id=10");
            }
            if (id == 11)
            {
                Response.Redirect("detalles11.aspx?id=11");
            }
            if (id == 12)
            {
                Response.Redirect("detalles12.aspx?id=12");
            }

        }
    }
}
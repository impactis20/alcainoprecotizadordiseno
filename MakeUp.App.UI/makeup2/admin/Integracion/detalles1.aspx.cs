﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MakeUp.BL;
namespace MakeUp.App.UI.makeup2.admin.Integracion
{
    public partial class detalles1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int id = Convert.ToInt32(Request.Params["id"]);
                if (id == 1)
                {
                    Label2.Text = "Lista de estado de integracion Crea Centro de Costo";
                    BindgvDetalleIntegracion1();
                }
                if (id == 2)
                {
                    Label2.Text = "Lista de estado de integracion Crea Articulo";
                    BindgvDetalleIntegracion2();
                }
                if (id == 3)
                {
                    Label2.Text = "Lista de estado de integracion Crear Articulo de Montaje";
                    BindgvDetalleIntegracion3();
                }
                if (id == 4)
                {
                    Label2.Text = "Lista de estado de integracion Agrega Centro Costo de Garantia";
                    BindgvDetalleIntegracion4();
                }
                if (id == 5)
                {
                    Label2.Text = "Lista de estado de integracion Agrega Cliente";
                    BindgvDetalleIntegracion5();
                }
                if (id == 6)
                {
                    Label2.Text = "Lista de estado de integracion Agrega Factura";
                    BindgvDetalleIntegracion6();
                }
                if (id == 7)
                {
                    Label2.Text = "Lista de estado de integracion Agrega Nota de Venta";
                    BindgvDetalleIntegracion7();
                }
                if (id == 8)
                {
                    Label2.Text = "Lista de estado de integracion Agrega Orden de Producción";
                    BindgvDetalleIntegracion8();
                }
                if (id == 9)
                {
                    Label2.Text = "Lista de estado de integracion Agrega Orden de Producción de Montaje";
                    BindgvDetalleIntegracion9();
                }
                if (id == 10)
                {
                    Label2.Text = "Lista de estado de integracion Modifica Cliente";
                    BindgvDetalleIntegracion10();
                }
                if (id == 11)
                {
                    Label2.Text = "Lista de estado de integracion Modifica Codigo Factura";
                    BindgvDetalleIntegracion11();
                }
                if (id == 12)
                {
                    Label2.Text = "Lista de estado de integracion Modifica Nota de Venta";
                    BindgvDetalleIntegracion12();
                }
            }
        }
        private void BindgvDetalleIntegracion1()
        {
            List<Entities.IntegracionEntity> Lst = IntegracionBL.getInteg1();
            gvDetalleIntegracion1.DataKeyNames = new string[] { "Id_log1" };
            gvDetalleIntegracion1.DataSource = Lst;
            gvDetalleIntegracion1.DataBind();

            if (Lst.Count > 0)
            {
                gvDetalleIntegracion1.UseAccessibleHeader = true;
                gvDetalleIntegracion1.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvDetalleIntegracion1.UseAccessibleHeader = false;
              
            }
        }
        protected void gvDetalleIntegracion1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.IntegracionEntity inte = (Entities.IntegracionEntity)r.DataItem;
                (r.Controls[0].FindControl("idproyecto") as Label).Text = inte.Codigo.ToString();
                (r.Controls[0].FindControl("descspcont") as Label).Text = inte.desc_sp_cont.ToString();
                (r.Controls[0].FindControl("descspcall") as Label).Text = inte.desc_sp_call.ToString();
                (r.Controls[0].FindControl("estado") as Label).Text = inte.estado.ToString();
                (r.Controls[0].FindControl("excepcion") as Label).Text = inte.excepcion.ToString();
                (r.Controls[0].FindControl("fecha") as Label).Text = inte.fecha_Hora.ToString();
                HyperLink detalle = r.Controls[0].FindControl("id_log") as HyperLink;
            }
        }

        private void BindgvDetalleIntegracion2()
        {
            List<Entities.IntegracionEntity> Lst = IntegracionBL.getInteg2();
            gvDetalleIntegracion2.DataKeyNames = new string[] { "Id_log1" };
            gvDetalleIntegracion2.DataSource = Lst;
            gvDetalleIntegracion2.DataBind();

            if (Lst.Count > 0)
            {
                gvDetalleIntegracion2.UseAccessibleHeader = true;
                gvDetalleIntegracion2.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvDetalleIntegracion2.UseAccessibleHeader = false;

            }
        }
        protected void gvDetalleIntegracion2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.IntegracionEntity inte = (Entities.IntegracionEntity)r.DataItem;
                (r.Controls[0].FindControl("idproyecto") as Label).Text = inte.Codigo.ToString();
                (r.Controls[0].FindControl("descspcont") as Label).Text = inte.desc_sp_cont.ToString();
                (r.Controls[0].FindControl("descspcall") as Label).Text = inte.desc_sp_call.ToString();
                (r.Controls[0].FindControl("estado") as Label).Text = inte.estado.ToString();
                (r.Controls[0].FindControl("excepcion") as Label).Text = inte.excepcion.ToString();
                (r.Controls[0].FindControl("fecha") as Label).Text = inte.fecha_Hora.ToString();
                HyperLink detalle = r.Controls[0].FindControl("Id_log1") as HyperLink;
            }
        }

        private void BindgvDetalleIntegracion3()
        {
            List<Entities.IntegracionEntity> Lst = IntegracionBL.getInteg3();
            gvDetalleIntegracion3.DataKeyNames = new string[] { "Id_log1" };
            gvDetalleIntegracion3.DataSource = Lst;
            gvDetalleIntegracion3.DataBind();

            if (Lst.Count > 0)
            {
                gvDetalleIntegracion3.UseAccessibleHeader = true;
                gvDetalleIntegracion3.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvDetalleIntegracion3.UseAccessibleHeader = false;

            }
        }
        protected void gvDetalleIntegracion3_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.IntegracionEntity inte = (Entities.IntegracionEntity)r.DataItem;
                (r.Controls[0].FindControl("idproyecto") as Label).Text = inte.Codigo.ToString();
                (r.Controls[0].FindControl("descspcont") as Label).Text = inte.desc_sp_cont.ToString();
                (r.Controls[0].FindControl("descspcall") as Label).Text = inte.desc_sp_call.ToString();
                (r.Controls[0].FindControl("estado") as Label).Text = inte.estado.ToString();
                (r.Controls[0].FindControl("excepcion") as Label).Text = inte.excepcion.ToString();
                (r.Controls[0].FindControl("fecha") as Label).Text = inte.fecha_Hora.ToString();
                HyperLink detalle = r.Controls[0].FindControl("Id_log1") as HyperLink;
            }
        }
        private void BindgvDetalleIntegracion4()
        {
            List<Entities.IntegracionEntity> Lst = IntegracionBL.getInteg4();
            gvDetalleIntegracion4.DataKeyNames = new string[] { "Id_log1" };
            gvDetalleIntegracion4.DataSource = Lst;
            gvDetalleIntegracion4.DataBind();

            if (Lst.Count > 0)
            {
                gvDetalleIntegracion4.UseAccessibleHeader = true;
                gvDetalleIntegracion4.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvDetalleIntegracion4.UseAccessibleHeader = false;

            }
        }
        protected void gvDetalleIntegracion4_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.IntegracionEntity inte = (Entities.IntegracionEntity)r.DataItem;
                (r.Controls[0].FindControl("idproyecto") as Label).Text = inte.Codigo.ToString();
                (r.Controls[0].FindControl("descspcont") as Label).Text = inte.desc_sp_cont.ToString();
                (r.Controls[0].FindControl("descspcall") as Label).Text = inte.desc_sp_call.ToString();
                (r.Controls[0].FindControl("estado") as Label).Text = inte.estado.ToString();
                (r.Controls[0].FindControl("excepcion") as Label).Text = inte.excepcion.ToString();
                (r.Controls[0].FindControl("fecha") as Label).Text = inte.fecha_Hora.ToString();
                HyperLink detalle = r.Controls[0].FindControl("Id_log1") as HyperLink;
            }
        }

        private void BindgvDetalleIntegracion5()
        {
            List<Entities.IntegracionEntity> Lst = IntegracionBL.getInteg5();
            gvDetalleIntegracion5.DataKeyNames = new string[] { "Id_log1" };
            gvDetalleIntegracion5.DataSource = Lst;
            gvDetalleIntegracion5.DataBind();

            if (Lst.Count > 0)
            {
                gvDetalleIntegracion5.UseAccessibleHeader = true;
                gvDetalleIntegracion5.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvDetalleIntegracion5.UseAccessibleHeader = false;

            }
        }
        protected void gvDetalleIntegracion5_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.IntegracionEntity inte = (Entities.IntegracionEntity)r.DataItem;

                (r.Controls[0].FindControl("descspcont") as Label).Text = inte.desc_sp_cont.ToString();
                (r.Controls[0].FindControl("descspcall") as Label).Text = inte.desc_sp_call.ToString();
                (r.Controls[0].FindControl("estado") as Label).Text = inte.estado.ToString();
                (r.Controls[0].FindControl("excepcion") as Label).Text = inte.excepcion.ToString();
                (r.Controls[0].FindControl("fecha") as Label).Text = inte.fecha_Hora.ToString();
                HyperLink detalle = r.Controls[0].FindControl("Id_log1") as HyperLink;
            }
        }
        private void BindgvDetalleIntegracion6()
        {
            List<Entities.IntegracionEntity> Lst = IntegracionBL.getInteg6();
            gvDetalleIntegracion6.DataKeyNames = new string[] { "Id_log1" };
            gvDetalleIntegracion6.DataSource = Lst;
            gvDetalleIntegracion6.DataBind();

            if (Lst.Count > 0)
            {
                gvDetalleIntegracion6.UseAccessibleHeader = true;
                gvDetalleIntegracion6.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvDetalleIntegracion6.UseAccessibleHeader = false;

            }
        }
        protected void gvDetalleIntegracion6_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.IntegracionEntity inte = (Entities.IntegracionEntity)r.DataItem;
                (r.Controls[0].FindControl("idproyecto") as Label).Text = inte.Codigo.ToString();
                (r.Controls[0].FindControl("descspcont") as Label).Text = inte.desc_sp_cont.ToString();
                (r.Controls[0].FindControl("descspcall") as Label).Text = inte.desc_sp_call.ToString();
                (r.Controls[0].FindControl("estado") as Label).Text = inte.estado.ToString();
                (r.Controls[0].FindControl("excepcion") as Label).Text = inte.excepcion.ToString();
                (r.Controls[0].FindControl("fecha") as Label).Text = inte.fecha_Hora.ToString();
                HyperLink detalle = r.Controls[0].FindControl("Id_log1") as HyperLink;
            }
        }
        private void BindgvDetalleIntegracion7()
        {
            List<Entities.IntegracionEntity> Lst = IntegracionBL.getInteg7();
            gvDetalleIntegracion7.DataKeyNames = new string[] { "Id_log1" };
            gvDetalleIntegracion7.DataSource = Lst;
            gvDetalleIntegracion7.DataBind();

            if (Lst.Count > 0)
            {
                gvDetalleIntegracion7.UseAccessibleHeader = true;
                gvDetalleIntegracion7.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvDetalleIntegracion7.UseAccessibleHeader = false;

            }
        }
        protected void gvDetalleIntegracion7_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.IntegracionEntity inte = (Entities.IntegracionEntity)r.DataItem;

                (r.Controls[0].FindControl("descspcont") as Label).Text = inte.desc_sp_cont.ToString();
                (r.Controls[0].FindControl("descspcall") as Label).Text = inte.desc_sp_call.ToString();
                (r.Controls[0].FindControl("estado") as Label).Text = inte.estado.ToString();
                (r.Controls[0].FindControl("excepcion") as Label).Text = inte.excepcion.ToString();
                (r.Controls[0].FindControl("fecha") as Label).Text = inte.fecha_Hora.ToString();
                HyperLink detalle = r.Controls[0].FindControl("Id_log1") as HyperLink;
            }
        }

        private void BindgvDetalleIntegracion8()
        {
            List<Entities.IntegracionEntity> Lst = IntegracionBL.getInteg8();
            gvDetalleIntegracion8.DataKeyNames = new string[] { "Id_log1" };
            gvDetalleIntegracion8.DataSource = Lst;
            gvDetalleIntegracion8.DataBind();

            if (Lst.Count > 0)
            {
                gvDetalleIntegracion8.UseAccessibleHeader = true;
                gvDetalleIntegracion8.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvDetalleIntegracion8.UseAccessibleHeader = false;

            }
        }
        protected void gvDetalleIntegracion8_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.IntegracionEntity inte = (Entities.IntegracionEntity)r.DataItem;
                string CodigoProyecto = inte.Codigo.Substring(0, inte.Codigo.Length - 2);
                (r.Controls[0].FindControl("idproyecto") as Label).Text = CodigoProyecto.ToString();
                (r.Controls[0].FindControl("descspcont") as Label).Text = inte.desc_sp_cont.ToString();
                (r.Controls[0].FindControl("descspcall") as Label).Text = inte.desc_sp_call.ToString();
                (r.Controls[0].FindControl("estado") as Label).Text = inte.estado.ToString();
                (r.Controls[0].FindControl("excepcion") as Label).Text = inte.excepcion.ToString();
                (r.Controls[0].FindControl("fecha") as Label).Text = inte.fecha_Hora.ToString();
                HyperLink detalle = r.Controls[0].FindControl("Id_log1") as HyperLink;
            }
        }
        private void BindgvDetalleIntegracion9()
        {
            List<Entities.IntegracionEntity> Lst = IntegracionBL.getInteg9();
            gvDetalleIntegracion9.DataKeyNames = new string[] { "Id_log1" };
            gvDetalleIntegracion9.DataSource = Lst;
            gvDetalleIntegracion9.DataBind();

            if (Lst.Count > 0)
            {
                gvDetalleIntegracion9.UseAccessibleHeader = true;
                gvDetalleIntegracion9.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvDetalleIntegracion9.UseAccessibleHeader = false;

            }
        }
        protected void gvDetalleIntegracion9_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.IntegracionEntity inte = (Entities.IntegracionEntity)r.DataItem;
                string CodigoProyecto = inte.Codigo.Substring(0, inte.Codigo.Length - 2);
                (r.Controls[0].FindControl("idproyecto") as Label).Text = CodigoProyecto.ToString();
                (r.Controls[0].FindControl("descspcont") as Label).Text = inte.desc_sp_cont.ToString();
                (r.Controls[0].FindControl("descspcall") as Label).Text = inte.desc_sp_call.ToString();
                (r.Controls[0].FindControl("estado") as Label).Text = inte.estado.ToString();
                (r.Controls[0].FindControl("excepcion") as Label).Text = inte.excepcion.ToString();
                (r.Controls[0].FindControl("fecha") as Label).Text = inte.fecha_Hora.ToString();
                HyperLink detalle = r.Controls[0].FindControl("Id_log1") as HyperLink;
            }
        }
        private void BindgvDetalleIntegracion10()
        {
            List<Entities.IntegracionEntity> Lst = IntegracionBL.getInteg10();
            gvDetalleIntegracion10.DataKeyNames = new string[] { "Id_log1" };
            gvDetalleIntegracion10.DataSource = Lst;
            gvDetalleIntegracion10.DataBind();

            if (Lst.Count > 0)
            {
                gvDetalleIntegracion10.UseAccessibleHeader = true;
                gvDetalleIntegracion10.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvDetalleIntegracion10.UseAccessibleHeader = false;

            }
        }
        protected void gvDetalleIntegracion10_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.IntegracionEntity inte = (Entities.IntegracionEntity)r.DataItem;

                (r.Controls[0].FindControl("descspcont") as Label).Text = inte.desc_sp_cont.ToString();
                (r.Controls[0].FindControl("descspcall") as Label).Text = inte.desc_sp_call.ToString();
                (r.Controls[0].FindControl("estado") as Label).Text = inte.estado.ToString();
                (r.Controls[0].FindControl("excepcion") as Label).Text = inte.excepcion.ToString();
                (r.Controls[0].FindControl("fecha") as Label).Text = inte.fecha_Hora.ToString();
                HyperLink detalle = r.Controls[0].FindControl("Id_log1") as HyperLink;
            }
        }
        private void BindgvDetalleIntegracion11()
        {
            List<Entities.IntegracionEntity> Lst = IntegracionBL.getInteg11();
            gvDetalleIntegracion11.DataKeyNames = new string[] { "Id_log1" };
            gvDetalleIntegracion11.DataSource = Lst;
            gvDetalleIntegracion11.DataBind();

            if (Lst.Count > 0)
            {
                gvDetalleIntegracion11.UseAccessibleHeader = true;
                gvDetalleIntegracion11.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvDetalleIntegracion11.UseAccessibleHeader = false;

            }
        }
        protected void gvDetalleIntegracion11_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.IntegracionEntity inte = (Entities.IntegracionEntity)r.DataItem;
                (r.Controls[0].FindControl("idproyecto") as Label).Text = inte.Codigo.ToString();
                (r.Controls[0].FindControl("descspcont") as Label).Text = inte.desc_sp_cont.ToString();
                (r.Controls[0].FindControl("descspcall") as Label).Text = inte.desc_sp_call.ToString();
                (r.Controls[0].FindControl("estado") as Label).Text = inte.estado.ToString();
                (r.Controls[0].FindControl("excepcion") as Label).Text = inte.excepcion.ToString();
                (r.Controls[0].FindControl("fecha") as Label).Text = inte.fecha_Hora.ToString();
                HyperLink detalle = r.Controls[0].FindControl("Id_log1") as HyperLink;
            }
        }

        private void BindgvDetalleIntegracion12()
        {
            List<Entities.IntegracionEntity> Lst = IntegracionBL.getInteg12();
            gvDetalleIntegracion12.DataKeyNames = new string[] { "Id_log1" };
            gvDetalleIntegracion12.DataSource = Lst;
            gvDetalleIntegracion12.DataBind();

            if (Lst.Count > 0)
            {
                gvDetalleIntegracion12.UseAccessibleHeader = true;
                gvDetalleIntegracion12.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvDetalleIntegracion12.UseAccessibleHeader = false;

            }
        }
        protected void gvDetalleIntegracion12_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.IntegracionEntity inte = (Entities.IntegracionEntity)r.DataItem;

                (r.Controls[0].FindControl("descspcont") as Label).Text = inte.desc_sp_cont.ToString();
                (r.Controls[0].FindControl("descspcall") as Label).Text = inte.desc_sp_call.ToString();
                (r.Controls[0].FindControl("estado") as Label).Text = inte.estado.ToString();
                (r.Controls[0].FindControl("excepcion") as Label).Text = inte.excepcion.ToString();
                (r.Controls[0].FindControl("fecha") as Label).Text = inte.fecha_Hora.ToString();
                HyperLink detalle = r.Controls[0].FindControl("Id_log1") as HyperLink;
            }
        }
    }
}
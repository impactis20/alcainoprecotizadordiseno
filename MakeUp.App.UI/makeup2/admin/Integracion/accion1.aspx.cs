﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using MakeUp.DAL;
using MakeUp.BL;
using System.Drawing;

namespace MakeUp.App.UI.makeup2.admin.Integracion
{
    public partial class accion1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    int idInte = Convert.ToInt16(Request.QueryString["idInte"]);
                    if (idInte == 1)
                    {
                        CreateDataSource1();
                        DataGrid1.DataSource = dt;
                        GenerateColumns(DataGrid1);
                        DataBind();
                    }
                    else if (idInte == 2)
                    {
                        CreateDataSource2();
                        DataGrid2.DataSource = dt2;
                        GenerateColumns(DataGrid2);
                        DataBind();
                    }
                    else if (idInte == 3)
                    {
                        CreateDataSource3();
                        DataGrid3.DataSource = dt3;
                        GenerateColumns(DataGrid3);
                        DataBind();

                    }
                    else if (idInte == 4)
                    {
                        CreateDataSource4();
                        DataGrid4.DataSource = dt4;
                        GenerateColumns(DataGrid4);
                        DataBind();
                    }
                    else if (idInte == 5)
                    {
                        CreateDataSource5();
                        DataGrid5.DataSource = dt5;
                        GenerateColumns(DataGrid5);
                        DataBind();
                    }
                    else if (idInte == 6)
                    {
                        CreateDataSource6();
                        DataGrid6.DataSource = dt6;
                        GenerateColumns(DataGrid6);
                        DataBind();
                    }
                    else if (idInte == 7)
                    {
                        CreateDataSource7();
                        DataGrid7.DataSource = dt7;
                        GenerateColumns(DataGrid7);
                        DataBind();
                    }
                    else if (idInte == 8)
                    {
                        CreateDataSource8();
                        DataGrid8.DataSource = dt8;
                        GenerateColumns(DataGrid8);
                        DataBind();
                    }
                    else if (idInte == 9)
                    {
                        CreateDataSource9();
                        DataGrid9.DataSource = dt9;
                        GenerateColumns(DataGrid9);
                        DataBind();
                    }
                    else if (idInte == 10)
                    {
                        CreateDataSource10();
                        DataGrid10.DataSource = dt10;
                        GenerateColumns(DataGrid10);
                        DataBind();
                    }
                    else if (idInte == 11)
                    {
                        CreateDataSource11();
                        DataGrid11.DataSource = dt11;
                        GenerateColumns(DataGrid11);
                        DataBind();
                    }
                    else if (idInte == 12) {
                        CreateDataSource12();
                        DataGrid12.DataSource = dt12;
                        GenerateColumns(DataGrid12);
                        DataBind();
                    }
                    //Label1.Text = "Ejecutar Procedimiento";
                    //Label1.CssClass = "alert-info";

                }
            }


        }

        protected DataTable dt = new DataTable("LOGS");
        protected DataTable dt2 = new DataTable("LOGS");
        protected DataTable dt3 = new DataTable("LOGS");
        protected DataTable dt4 = new DataTable("LOGS");
        protected DataTable dt5 = new DataTable("LOGS");
        protected DataTable dt6 = new DataTable("LOGS");
        protected DataTable dt7 = new DataTable("LOGS");
        protected DataTable dt8 = new DataTable("LOGS");
        protected DataTable dt9 = new DataTable("LOGS");
        protected DataTable dt10 = new DataTable("LOGS");
        protected DataTable dt11 = new DataTable("LOGS");
        protected DataTable dt12 = new DataTable("LOGS");

        private void CreateDataSource1()
        {
            try
            {
                DataGrid1.HeaderStyle.Font.Bold =true;
                DataColumn col = null;
                col = dt.Columns.Add("PARAMETROS", typeof(string));
            
                col.Caption = "Parametros";
                

                //dt.DefaultView.BackColor = Color.Red;

                col = dt.Columns.Add("VALORES", typeof(string));
                col.Caption = "Valores";

                int id_log1 = Convert.ToInt32(Request.Params["id"]);
                Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion1(id_log1);

                // Fill data table
                dt.Rows.Add(new object[] { "id_log", data.Id_log1 });
                dt.Rows.Add(new object[] { "desc_sp_cont",data.desc_sp_cont });
                dt.Rows.Add(new object[] { "desc_sp_call", data.desc_sp_call });
                dt.Rows.Add(new object[] { "desc_call", data.desc_call });
                dt.Rows.Add(new object[] { "estado", data.estado });
                dt.Rows.Add(new object[] { "excepcion", data.excepcion });
                dt.Rows.Add(new object[] { "fecha_Hora", data.fecha_Hora });
                dt.Rows.Add(new object[] { "p_codigocc", data.Codigo });
                dt.Rows.Add(new object[] { "p_yy", data.Yy });
                dt.Rows.Add(new object[] { "p_nreguistCC", data.Nreguist });
                dt.Rows.Add(new object[] { "p_nivel2", data.Nivel2 });
                dt.Rows.Add(new object[] { "p_clase1", data.Clase1 });
                dt.Rows.Add(new object[] { "p_nombreproyecto", data.Nombre });
                Button1.Enabled = true;
            }
            catch(Exception ex)
            {
                Label1.Text = "Proceso con error";
                Label1.CssClass = "alert-danger";
                Button1.Enabled = false;
            }

        }
        private void CreateDataSource2()
        {
            try
            {
                DataGrid2.HeaderStyle.Font.Bold = true;
                DataColumn col = null;
                col = dt2.Columns.Add("PARAMETROS", typeof(string));
                col.Caption = "Parametros";

                col = dt2.Columns.Add("VALORES", typeof(string));
                col.Caption = "Valores";
                int id_log1 = Convert.ToInt32(Request.Params["id"]);
                Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion2(id_log1);

                // Fill data table
                dt2.Rows.Add(new object[] { "id_log", data.Id_log1 });
                dt2.Rows.Add(new object[] { "desc_sp_cont", data.desc_sp_cont });
                dt2.Rows.Add(new object[] { "desc_sp_call", data.desc_sp_call });
                dt2.Rows.Add(new object[] { "desc_call", data.desc_call });
                dt2.Rows.Add(new object[] { "estado", data.estado });
                dt2.Rows.Add(new object[] { "excepcion", data.excepcion });
                dt2.Rows.Add(new object[] { "fecha_Hora", data.fecha_Hora });
                dt2.Rows.Add(new object[] { "p_nombreproyecto", data.Nombre });
                dt2.Rows.Add(new object[] { "p_idarticulomanager", data.articulo });
                dt2.Rows.Add(new object[] { "p_fechacreacion", data.fecha });
                dt2.Rows.Add(new object[] { "p_codigocc", data.Codigo });
                Button1.Enabled = true;
            }
            catch (Exception ex)
            {
                Label1.Text = "Proceso con error";
                Label1.CssClass = "alert-danger";
                Button1.Enabled = false;
            }
        }

        private void CreateDataSource3()
        {
            try
            {
                DataGrid3.HeaderStyle.Font.Bold = true;
                DataColumn col = null;
                col = dt3.Columns.Add("PARAMETROS", typeof(string));
                col.Caption = "Parametros";

                col = dt3.Columns.Add("VALORES", typeof(string));
                col.Caption = "Valores";
                int id_log1 = Convert.ToInt32(Request.Params["id"]);
                Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion3(id_log1);

                // Fill data table
                dt3.Rows.Add(new object[] { "id_log", data.Id_log1 });
                dt3.Rows.Add(new object[] { "desc_sp_cont", data.desc_sp_cont });
                dt3.Rows.Add(new object[] { "desc_sp_call", data.desc_sp_call });
                dt3.Rows.Add(new object[] { "desc_call", data.desc_call });
                dt3.Rows.Add(new object[] { "estado", data.estado });
                dt3.Rows.Add(new object[] { "excepcion", data.excepcion });
                dt3.Rows.Add(new object[] { "fecha_Hora", data.fecha_Hora });
                dt3.Rows.Add(new object[] { "p_nombreproyecto", data.Nombre });
                dt3.Rows.Add(new object[] { "p_idarticulomanagermon", data.articulo });
                dt3.Rows.Add(new object[] { "p_fechacreacion", data.fecha });
                dt3.Rows.Add(new object[] { "p_codigocc", data.Codigo });
                Button1.Enabled = true;
            }
            catch (Exception ex)
            {
                Label1.Text = "Proceso con error";
                Label1.CssClass = "alert-danger";
                Button1.Enabled = false;
            }
        }
        private void CreateDataSource4()
        {
            try
            {
                DataGrid4.HeaderStyle.Font.Bold = true;
                DataColumn col = null;
                col = dt4.Columns.Add("PARAMETROS", typeof(string));
                col.Caption = "Parametros";

                col = dt4.Columns.Add("VALORES", typeof(string));
                col.Caption = "Valores";
                int id_log1 = Convert.ToInt32(Request.Params["id"]);
                Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion4(id_log1);

                // Fill data table
                dt4.Rows.Add(new object[] { "id_log", data.Id_log1 });
                dt4.Rows.Add(new object[] { "desc_sp_cont", data.desc_sp_cont });
                dt4.Rows.Add(new object[] { "desc_sp_call", data.desc_sp_call });
                dt4.Rows.Add(new object[] { "desc_call", data.desc_call });
                dt4.Rows.Add(new object[] { "estado", data.estado });
                dt4.Rows.Add(new object[] { "excepcion", data.excepcion });
                dt4.Rows.Add(new object[] { "fecha_Hora", data.fecha_Hora });
                dt4.Rows.Add(new object[] { "p_idproyecto", data.IdProyecto });

                Button1.Enabled = true;
            }
            catch (Exception ex)
            {
                Label1.Text = "Proceso con error";
                Label1.CssClass = "alert-danger";
                Button1.Enabled = false;
            }
        }
        private void CreateDataSource5()
        {
            try
            {
                DataGrid5.HeaderStyle.Font.Bold = true;
                DataColumn col = null;
                col = dt5.Columns.Add("PARAMETROS", typeof(string));
                col.Caption = "Parametros";

                col = dt5.Columns.Add("VALORES", typeof(string));
                col.Caption = "Valores";
                int id_log1 = Convert.ToInt32(Request.Params["id"]);
                Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion5(id_log1);

                // Fill data table
                dt5.Rows.Add(new object[] { "id_log", data.Id_log1 });
                dt5.Rows.Add(new object[] { "desc_sp_cont", data.desc_sp_cont });
                dt5.Rows.Add(new object[] { "desc_sp_call", data.desc_sp_call });
                dt5.Rows.Add(new object[] { "desc_call", data.desc_call });
                dt5.Rows.Add(new object[] { "estado", data.estado });
                dt5.Rows.Add(new object[] { "excepcion", data.excepcion });
                dt5.Rows.Add(new object[] { "fecha_Hora", data.fecha_Hora });
                dt5.Rows.Add(new object[] { "p_nombreFantasia", data.Nombre });
                dt5.Rows.Add(new object[] { "p_rut", data.rutCliente });
                dt5.Rows.Add(new object[] { "p_digito_v", data.DigitoV });
                dt5.Rows.Add(new object[] { "p_razon_social", data.Razon });
                dt5.Rows.Add(new object[] { "p_giro", data.Giro });
                dt5.Rows.Add(new object[] { "p_direccion", data.Direccion });
                dt5.Rows.Add(new object[] { "p_telefono", data.Telefono });
                dt5.Rows.Add(new object[] { "p_comuna", data.Comuna });
                dt5.Rows.Add(new object[] { "p_ciudad", data.Ciudad });
                dt5.Rows.Add(new object[] { "p_nreguist", data.Nreguist });
                Button1.Enabled = true;
            }
            catch (Exception ex)
            {
                Label1.Text = "Proceso con error";
                Label1.CssClass = "alert-danger";
                Button1.Enabled = false;
            }
        }
        private void CreateDataSource6()
        {
            try
            {
                DataGrid6.HeaderStyle.Font.Bold = true;
                DataColumn col = null;
                col = dt6.Columns.Add("PARAMETROS", typeof(string));
                col.Caption = "Parametros";

                col = dt6.Columns.Add("VALORES", typeof(string));
                col.Caption = "Valores";
                int id_log1 = Convert.ToInt32(Request.Params["id"]);
                Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion6(id_log1);

                // Fill data table
                dt6.Rows.Add(new object[] { "id_log", data.Id_log1 });
                dt6.Rows.Add(new object[] { "desc_sp_cont", data.desc_sp_cont });
                dt6.Rows.Add(new object[] { "desc_sp_call", data.desc_sp_call });
                dt6.Rows.Add(new object[] { "desc_call", data.desc_call });
                dt6.Rows.Add(new object[] { "estado", data.estado });
                dt6.Rows.Add(new object[] { "excepcion", data.excepcion });
                dt6.Rows.Add(new object[] { "fecha_Hora", data.fecha_Hora });
                dt6.Rows.Add(new object[] { "p_idproyecto", data.IdProyecto });
                dt6.Rows.Add(new object[] { "p_idcliente", data.IdCliente });
                dt6.Rows.Add(new object[] { "p_fechacreacion", data.fechaCreacion });
                dt6.Rows.Add(new object[] { "p_condicionpago", data.condicionPago });
                dt6.Rows.Add(new object[] { "p_fechavencimiento", data.fechaVencimiento });
                dt6.Rows.Add(new object[] { "p_nrooc", data.nroOC });
                dt6.Rows.Add(new object[] { "p_fechaoc", data.fechaOC });
                dt6.Rows.Add(new object[] { "p_nrohes", data.nroOC });
                dt6.Rows.Add(new object[] { "p_fechahes", data.fechaHes });
                dt6.Rows.Add(new object[] { "p_valortotalfactura", data.valorTotalNeto });
                dt6.Rows.Add(new object[] { "p_glosa", data.glosa });
                dt6.Rows.Add(new object[] { "p_idfacturamanager", data.idFacturaManager });
                Button1.Enabled = true;
            }
            catch (Exception ex)
            {
                Label1.Text = "Proceso con error";
                Label1.CssClass = "alert-danger";
                Button1.Enabled = false;
            }
        }
        private void CreateDataSource7()
        {
            try
            {
                DataGrid7.HeaderStyle.Font.Bold = true;
                DataColumn col = null;
                col = dt7.Columns.Add("PARAMETROS", typeof(string));
                col.Caption = "Parametros";

                col = dt7.Columns.Add("VALORES", typeof(string));
                col.Caption = "Valores";
                int id_log1 = Convert.ToInt32(Request.Params["id"]);
                Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion7(id_log1);

                // Fill data table
                dt7.Rows.Add(new object[] { "id_log", data.Id_log1 });
                dt7.Rows.Add(new object[] { "desc_sp_cont", data.desc_sp_cont });
                dt7.Rows.Add(new object[] { "desc_sp_call", data.desc_sp_call });
                dt7.Rows.Add(new object[] { "desc_call", data.desc_call });
                dt7.Rows.Add(new object[] { "estado", data.estado });
                dt7.Rows.Add(new object[] { "excepcion", data.excepcion });
                dt7.Rows.Add(new object[] { "fecha_Hora", data.fecha_Hora });
                dt7.Rows.Add(new object[] { "p_idproyecto", data.IdProyecto });
                dt7.Rows.Add(new object[] { "p_idnotaventamanager", data.idNotaVentaManager });
                dt7.Rows.Add(new object[] { "p_valortotalproyecto", data.valorTotalProyecto });
                dt7.Rows.Add(new object[] { "p_rutcliente", data.rutCliente });
                dt7.Rows.Add(new object[] { "p_idclientemanager", data.IdCliente });
                dt7.Rows.Add(new object[] { "p_fechacreacion", data.fechaCreacion });
                dt7.Rows.Add(new object[] { "p_idtipovalor", data.idTipoValor });
                Button1.Enabled = true;
            }
            catch (Exception ex)
            {
                Label1.Text = "Proceso con error";
                Label1.CssClass = "alert-danger";
                Button1.Enabled = false;
            }
        }

        private void CreateDataSource8()
        {
            try
            {
                DataGrid8.HeaderStyle.Font.Bold = true;
                DataColumn col = null;
                col = dt8.Columns.Add("PARAMETROS", typeof(string));
                col.Caption = "Parametros";

                col = dt8.Columns.Add("VALORES", typeof(string));
                col.Caption = "Valores";
                int id_log1 = Convert.ToInt32(Request.Params["id"]);
                Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion8(id_log1);

                // Fill data table
                dt8.Rows.Add(new object[] { "id_log", data.Id_log1 });
                dt8.Rows.Add(new object[] { "desc_sp_cont", data.desc_sp_cont });
                dt8.Rows.Add(new object[] { "desc_sp_call", data.desc_sp_call });
                dt8.Rows.Add(new object[] { "desc_call", data.desc_call });
                dt8.Rows.Add(new object[] { "estado", data.estado });
                dt8.Rows.Add(new object[] { "excepcion", data.excepcion });
                dt8.Rows.Add(new object[] { "fecha_Hora", data.fecha_Hora });
                dt8.Rows.Add(new object[] { "p_ordenprd", data.OrdenPRD });
                dt8.Rows.Add(new object[] { "p_nombreproyecto", data.Nombre });
                dt8.Rows.Add(new object[] { "p_nreguistcc", data.nreguist_art });
                dt8.Rows.Add(new object[] { "p_idclientemanager", data.IdCliente });
                dt8.Rows.Add(new object[] { "p_fechacomprometida", data.fechaComprometida });
                dt8.Rows.Add(new object[] { "p_fechacreacion", data.fechaCreacion });
                dt8.Rows.Add(new object[] { "p_idarticulomanager", data.articulo });
                Button1.Enabled = true;
            }
            catch (Exception ex)
            {
                Label1.Text = "Proceso con error";
                Label1.CssClass = "alert-danger";
                Button1.Enabled = false;
            }
        }

        private void CreateDataSource9()
        {
            try
            {
                DataGrid9.HeaderStyle.Font.Bold = true;
                DataColumn col = null;
                col = dt9.Columns.Add("PARAMETROS", typeof(string));
                col.Caption = "Parametros";

                col = dt9.Columns.Add("VALORES", typeof(string));
                col.Caption = "Valores";
                int id_log1 = Convert.ToInt32(Request.Params["id"]);
                Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion9(id_log1);

                // Fill data table
                dt9.Rows.Add(new object[] { "id_log", data.Id_log1 });
                dt9.Rows.Add(new object[] { "desc_sp_cont", data.desc_sp_cont });
                dt9.Rows.Add(new object[] { "desc_sp_call", data.desc_sp_call });
                dt9.Rows.Add(new object[] { "desc_call", data.desc_call });
                dt9.Rows.Add(new object[] { "estado", data.estado });
                dt9.Rows.Add(new object[] { "excepcion", data.excepcion });
                dt9.Rows.Add(new object[] { "fecha_Hora", data.fecha_Hora });
                dt9.Rows.Add(new object[] { "p_ordenprdm", data.OrdenPRD });
                dt9.Rows.Add(new object[] { "p_nombrem", data.Nombre });
                dt9.Rows.Add(new object[] { "p_nreguistcc", data.Nreguist });
                dt9.Rows.Add(new object[] { "p_idclientemanager", data.IdCliente });
                dt9.Rows.Add(new object[] { "p_fechacomprometida", data.fechaComprometida });
                dt9.Rows.Add(new object[] { "p_fechacreacion", data.fechaCreacion });
                dt9.Rows.Add(new object[] { "p_idarticulomanagermon", data.articulo });
                Button1.Enabled = true;
            }
            catch (Exception ex)
            {
                Label1.Text = "Proceso con error";
                Label1.CssClass = "alert-danger";
                Button1.Enabled = false;
            }
        }

        private void CreateDataSource10()
        {
            try
            {
                DataGrid10.HeaderStyle.Font.Bold = true;
                DataColumn col = null;
                col = dt10.Columns.Add("PARAMETROS", typeof(string));
                col.Caption = "Parametros";

                col = dt10.Columns.Add("VALORES", typeof(string));
                col.Caption = "Valores";
                int id_log1 = Convert.ToInt32(Request.Params["id"]);
                Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion10(id_log1);

                // Fill data table
                dt10.Rows.Add(new object[] { "id_log", data.Id_log1 });
                dt10.Rows.Add(new object[] { "desc_sp_cont", data.desc_sp_cont });
                dt10.Rows.Add(new object[] { "desc_sp_call", data.desc_sp_call });
                dt10.Rows.Add(new object[] { "desc_call", data.desc_call });
                dt10.Rows.Add(new object[] { "estado", data.estado });
                dt10.Rows.Add(new object[] { "excepcion", data.excepcion });
                dt10.Rows.Add(new object[] { "fecha_Hora", data.fecha_Hora });
                dt10.Rows.Add(new object[] { "p_nombrefantasia", data.NombreFantasia });
                dt10.Rows.Add(new object[] { "p_rut", data.Rut });
                dt10.Rows.Add(new object[] { "p_dv", data.DigitoV });
                dt10.Rows.Add(new object[] { "p_razonsocial", data.Razon });
                dt10.Rows.Add(new object[] { "p_giro", data.Giro });
                dt10.Rows.Add(new object[] { "p_direccion", data.Direccion });
                dt10.Rows.Add(new object[] { "p_telefono", data.Telefono });
                dt10.Rows.Add(new object[] { "p_comuna", data.Comuna });
                dt10.Rows.Add(new object[] { "p_ciudad", data.Ciudad });
                dt10.Rows.Add(new object[] { "p_nreguist", data.Nreguist });
                Button1.Enabled = true;
            }
            catch (Exception ex)
            {
                Label1.Text = "Proceso con error";
                Label1.CssClass = "alert-danger";
                Button1.Enabled = false;
            }
        }

        private void CreateDataSource11()
        {
            try
            {
                DataGrid11.HeaderStyle.Font.Bold = true;
                DataColumn col = null;
                col = dt11.Columns.Add("PARAMETROS", typeof(string));
                col.Caption = "Parametros";

                col = dt11.Columns.Add("VALORES", typeof(string));
                col.Caption = "Valores";
                int id_log1 = Convert.ToInt32(Request.Params["id"]);
                Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion11(id_log1);

                // Fill data table
                dt11.Rows.Add(new object[] { "id_log", data.Id_log1 });
                dt11.Rows.Add(new object[] { "desc_sp_cont", data.desc_sp_cont });
                dt11.Rows.Add(new object[] { "desc_sp_call", data.desc_sp_call });
                dt11.Rows.Add(new object[] { "desc_call", data.desc_call });
                dt11.Rows.Add(new object[] { "estado", data.estado });
                dt11.Rows.Add(new object[] { "excepcion", data.excepcion });
                dt11.Rows.Add(new object[] { "fecha_Hora", data.fecha_Hora });
                dt11.Rows.Add(new object[] { "p_idproyecto", data.Codigo });

                Button1.Enabled = true;
            }
            catch (Exception ex)
            {
                Label1.Text = "Proceso con error";
                Label1.CssClass = "alert-danger";
                Button1.Enabled = false;
            }
        }

        private void CreateDataSource12()
        {
            try
            {
                DataGrid12.HeaderStyle.Font.Bold = true;
                DataColumn col = null;
                col = dt12.Columns.Add("PARAMETROS", typeof(string));
                col.Caption = "Parametros";

                col = dt12.Columns.Add("VALORES", typeof(string));
                col.Caption = "Valores";
                int id_log1 = Convert.ToInt32(Request.Params["id"]);
                Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion12(id_log1);

                // Fill data table
                dt12.Rows.Add(new object[] { "id_log", data.Id_log1 });
                dt12.Rows.Add(new object[] { "desc_sp_cont", data.desc_sp_cont });
                dt12.Rows.Add(new object[] { "desc_sp_call", data.desc_sp_call });
                dt12.Rows.Add(new object[] { "desc_call", data.desc_call });
                dt12.Rows.Add(new object[] { "estado", data.estado });
                dt12.Rows.Add(new object[] { "excepcion", data.excepcion });
                dt12.Rows.Add(new object[] { "fecha_Hora", data.fecha_Hora });
                dt12.Rows.Add(new object[] { "p_idproyecto", data.IdProyecto });
            }
            catch (Exception ex)
            {
                Label1.Text = "Proceso con error";
                Label1.CssClass = "alert-danger";
                Button1.Enabled = false;
            }
        }
        private void GenerateColumns(DataGrid grid)
        {

            grid.AutoGenerateColumns = false;
            int idInte = Convert.ToInt16(Request.QueryString["idInte"]);
            if (idInte == 1)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    BoundColumn dc = new BoundColumn();
                    dc.DataField = col.ColumnName;
                    dc.HeaderText = col.Caption;
                    grid.Columns.Add(dc);
                }
            }
            else if (idInte == 2) {
                foreach (DataColumn col in dt2.Columns)
                {
                    BoundColumn dc = new BoundColumn();
                    dc.DataField = col.ColumnName;
                    dc.HeaderText = col.Caption;
                    grid.Columns.Add(dc);
                }

            }
            else if (idInte == 3)
            {
                foreach (DataColumn col in dt3.Columns)
                {
                    BoundColumn dc = new BoundColumn();
                    dc.DataField = col.ColumnName;
                    dc.HeaderText = col.Caption;
                    grid.Columns.Add(dc);
                }

            }
            else if (idInte == 4)
            {
                foreach (DataColumn col in dt4.Columns)
                {
                    BoundColumn dc = new BoundColumn();
                    dc.DataField = col.ColumnName;
                    dc.HeaderText = col.Caption;
                    grid.Columns.Add(dc);
                }

            }
            else if (idInte == 5)
            {
                foreach (DataColumn col in dt5.Columns)
                {
                    BoundColumn dc = new BoundColumn();
                    dc.DataField = col.ColumnName;
                    dc.HeaderText = col.Caption;
                    grid.Columns.Add(dc);
                }

            }
            else if (idInte == 6)
            {
                foreach (DataColumn col in dt6.Columns)
                {
                    BoundColumn dc = new BoundColumn();
                    dc.DataField = col.ColumnName;
                    dc.HeaderText = col.Caption;
                    grid.Columns.Add(dc);
                }

            }
            else if (idInte == 7)
            {
                foreach (DataColumn col in dt7.Columns)
                {
                    BoundColumn dc = new BoundColumn();
                    dc.DataField = col.ColumnName;
                    dc.HeaderText = col.Caption;
                    grid.Columns.Add(dc);
                }

            }
            else if (idInte == 8)
            {
                foreach (DataColumn col in dt8.Columns)
                {
                    BoundColumn dc = new BoundColumn();
                    dc.DataField = col.ColumnName;
                    dc.HeaderText = col.Caption;
                    grid.Columns.Add(dc);
                }

            }
            else if (idInte == 9)
            {
                foreach (DataColumn col in dt9.Columns)
                {
                    BoundColumn dc = new BoundColumn();
                    dc.DataField = col.ColumnName;
                    dc.HeaderText = col.Caption;
                    grid.Columns.Add(dc);
                }

            }
            else if (idInte == 10)
            {
                foreach (DataColumn col in dt10.Columns)
                {
                    BoundColumn dc = new BoundColumn();
                    dc.DataField = col.ColumnName;
                    dc.HeaderText = col.Caption;
                    grid.Columns.Add(dc);
                }

            }
            else if (idInte ==11)
            {
                foreach (DataColumn col in dt11.Columns)
                {
                    BoundColumn dc = new BoundColumn();
                    dc.DataField = col.ColumnName;
                    dc.HeaderText = col.Caption;
                    grid.Columns.Add(dc);
                }


            }
            else if (idInte == 12)
            {
                foreach (DataColumn col in dt12.Columns)
                {
                    BoundColumn dc = new BoundColumn();
                    dc.DataField = col.ColumnName;
                    dc.HeaderText = col.Caption;
                    grid.Columns.Add(dc);
                }

            }
        }
       
        protected void Button1_Click(object sender, EventArgs e)
        {
            int idInte = Convert.ToInt16(Request.QueryString["idInte"]);
            if (idInte == 1)
            {
                try
                {
                    int id_log1 = Convert.ToInt32(Request.Params["id"]);
                    Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion1(id_log1);
                    string p1 = data.Codigo;
                    string p2 = data.Nombre;
                    string p3 = data.Clase1;
                    int p4 = data.Nivel2;
                    int p5 = data.Nreguist;
                    decimal p6 = data.Yy;
                    IntegracionBL.setIntegracion1(p1, p2, p3, p4, p5, p6, id_log1);
                    //IntegracionBL.setIntegracion1(id_log1);
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop867", Util.GetMensajeAlerta("success", "...", "Reprocesado exitosamente"), true);
                    Label1.Text = "No hay repocesos Pendientes";
                    Label1.CssClass = "alert-info";
                    Button1.Visible = false;
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                    CreateDataSource1();
                    DataGrid1.DataSource = dt;
                    GenerateColumns(DataGrid1);
                    DataBind();
                }

            }
            else if (idInte == 2)
            {
                try
                {
                    int id_log1 = Convert.ToInt32(Request.Params["id"]);
                    Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion2(id_log1);
                    string p1 = data.Nombre;
                    string p2 = data.Codigo;
                    DateTime p3 = data.fecha;
                    string p4 = data.articulo;
                    IntegracionBL.setIntegracion2(p1, p2, p3, p4, id_log1);
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop867", Util.GetMensajeAlerta("success", "...", "Reprocesado exitosamente"), true);
                    Label1.Text = "No hay repocesos Pendientes";
                    Label1.CssClass = "alert-info";
                    Button1.Visible = false;
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                    CreateDataSource2();
                    DataGrid2.DataSource = dt2;
                    GenerateColumns(DataGrid2);
                    DataBind();
                }

            }
            else if (idInte == 3)
            {
                try
                {
                    int id_log1 = Convert.ToInt32(Request.Params["id"]);
                    Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion3(id_log1);
                    string p1 = data.Nombre;
                    string p2 = data.Codigo;
                    DateTime p3 = data.fecha;
                    string p4 = data.articulo;
                    IntegracionBL.setIntegracion3(p1, p2, p3, p4, id_log1);
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop867", Util.GetMensajeAlerta("success", "...", "Reprocesado exitosamente"), true);
                    Label1.Text = "No hay repocesos Pendientes";
                    Label1.CssClass = "alert-info";
                    Button1.Visible = false;
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                    CreateDataSource3();
                    DataGrid3.DataSource = dt3;
                    GenerateColumns(DataGrid3);
                    DataBind();
                }

            }
            else if (idInte == 4)
            {
                try
                {
                    int id_log1 = Convert.ToInt32(Request.Params["id"]);
                    Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion4(id_log1);
                    int p1 = data.IdProyecto;
                    IntegracionBL.setIntegracion4(p1, id_log1);
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop867", Util.GetMensajeAlerta("success", "...", "Reprocesado exitosamente"), true);
                    Label1.Text = "No hay repocesos Pendientes";
                    Label1.CssClass = "alert-info";
                    Button1.Visible = false;
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                    CreateDataSource4();
                    DataGrid4.DataSource = dt4;
                    GenerateColumns(DataGrid4);
                    DataBind();
                }
            }
            else if (idInte == 5)
            {
                try
                {
                    int id_log1 = Convert.ToInt32(Request.Params["id"]);
                    Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion5(id_log1);
                    string p1 = data.Nombre;
                    int p2 = data.Rut;
                    string p3 = data.DigitoV;
                    string p4 = data.Razon;
                    string p5 = data.Giro;
                    string p6 = data.Direccion;
                    string p7 = data.Telefono;
                    string p8 = data.Comuna;
                    string p9 = data.Ciudad;
                    int p10 = data.Nreguist;
                    IntegracionBL.setIntegracion5(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, id_log1);
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop867", Util.GetMensajeAlerta("success", "...", "Reprocesado exitosamente"), true);
                    Label1.Text = "No hay repocesos Pendientes";
                    Label1.CssClass = "alert-info";
                    Button1.Visible = false;
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                    CreateDataSource5();
                    DataGrid5.DataSource = dt5;
                    GenerateColumns(DataGrid5);
                    DataBind();
                }

            }
            else if (idInte == 6)
            {
                try
                {
                    int id_log1 = Convert.ToInt32(Request.Params["id"]);
                    Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion6(id_log1);
                    int p1 = data.IdProyecto;
                    int p2 = data.IdCliente;
                    DateTime p3 = data.fechaCreacion;
                    string p4 = data.condicionPago;
                    DateTime p5 = data.fechaVencimiento;
                    string p6 = data.nroOC;
                    DateTime p7 = data.fechaOC;
                    string p8 = data.nroHes;
                    DateTime p9 = data.fechaHes;
                    double p10 = data.valorTotalNeto;
                    string p11 =data.glosa;
                    int p12 =data.idFacturaManager;
                    IntegracionBL.setIntegracion6(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, id_log1);
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop867", Util.GetMensajeAlerta("success", "...", "Reprocesado exitosamente"), true);
                    Label1.Text = "No hay repocesos Pendientes";
                    Label1.CssClass = "alert-info";
                    Button1.Visible = false;
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                    CreateDataSource6();
                    DataGrid6.DataSource = dt6;
                    GenerateColumns(DataGrid6);
                    DataBind();
                }
            }
            else if (idInte == 7)
            {
                try
                {
                    int id_log1 = Convert.ToInt32(Request.Params["id"]);
                    Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion7(id_log1);
                    int p1 = data.IdProyecto;
                    int p2 = data.idTipoValor;
                    string p3 = data.fechaIngreso;
                    int p4 = data.IdCliente;
                    string p5 = data.rutCliente;
                    int p6 = data.valorTotalProyecto;
                    int p7 = data.idNotaVentaManager;
                    IntegracionBL.setIntegracion7(p1, p2, p3, p4, p5, p6, p7, id_log1);
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop867", Util.GetMensajeAlerta("success", "...", "Reprocesado exitosamente"), true);
                    Label1.Text = "No hay repocesos Pendientes";
                    Label1.CssClass = "alert-info";
                    Button1.Visible = false;
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                    CreateDataSource7();
                    DataGrid7.DataSource = dt7;
                    GenerateColumns(DataGrid7);
                    DataBind();
                }
            }
            else if (idInte == 8)
            {
                try
                {
                    int id_log1 = Convert.ToInt32(Request.Params["id"]);
                    Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion8(id_log1);
                    string p1 = data.OrdenPRD;
                    string p2 = data.nreguist_art;
                    DateTime p3 = data.fechaCreacion;
                    DateTime p4 = data.fechaComprometida;
                    int p5 = data.IdCliente;
                    string p6 = data.Nombre;
                    IntegracionBL.setIntegracion8(p1, p2, p3, p4, p5, p6, id_log1);
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop867", Util.GetMensajeAlerta("success", "...", "Reprocesado exitosamente"), true);
                    Label1.Text = "No hay repocesos Pendientes";
                    Label1.CssClass = "alert-info";
                    Button1.Visible = false;
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                    CreateDataSource8();
                    DataGrid8.DataSource = dt8;
                    GenerateColumns(DataGrid8);
                    DataBind();
                }
            }
            else if (idInte == 9)
            {
                try
                {
                    int id_log1 = Convert.ToInt32(Request.Params["id"]);
                    Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion9(id_log1);
                    string p1 = data.OrdenPRD;
                    string p2 = data.nreguist_art;
                    DateTime p3 = data.fechaCreacion;
                    DateTime p4 = data.fechaComprometida;
                    int p5 = data.IdCliente;
                    IntegracionBL.setIntegracion9(p1, p2, p3, p4, p5, id_log1);
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop867", Util.GetMensajeAlerta("success", "...", "Reprocesado exitosamente"), true);
                    Label1.Text = "No hay repocesos Pendientes";
                    Label1.CssClass = "alert-info";
                    Button1.Visible = false;
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                    CreateDataSource9();
                    DataGrid9.DataSource = dt9;
                    GenerateColumns(DataGrid9);
                    DataBind();
                }
            }
            else if (idInte == 10)
            {
                try
                {
                    int id_log1 = Convert.ToInt32(Request.Params["id"]);
                    Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion10(id_log1);
                    string p1 = data.NombreFantasia;
                    int p2 = data.Rut;
                    string p3 = data.DigitoV;
                    string p4 = data.Razon;
                    string p5 = data.Giro;
                    string p6 = data.Direccion;
                    string p7 = data.Telefono;
                    string p8 = data.Comuna;
                    string p9 = data.Ciudad;
                    int p10 = data.Nreguist;
                    IntegracionBL.setIntegracion10(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, id_log1);
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop867", Util.GetMensajeAlerta("success", "...", "Reprocesado exitosamente"), true);
                    Label1.Text = "No hay repocesos Pendientes";
                    Label1.CssClass = "alert-info";
                    Button1.Visible = false;
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                    CreateDataSource10();
                    DataGrid10.DataSource = dt10;
                    GenerateColumns(DataGrid10);
                    DataBind();
                }
            }
            else if (idInte == 11)
            {
                try
                {
                    int id_log1 = Convert.ToInt32(Request.Params["id"]);
                    Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion11(id_log1);
                    int p1 = data.IdProyecto;
                    IntegracionBL.setIntegracion11(p1,id_log1);
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop867", Util.GetMensajeAlerta("success", "...", "Reprocesado exitosamente"), true);
                    Label1.Text = "No hay repocesos Pendientes";
                    Label1.CssClass = "alert-info";
                    Button1.Visible = false;
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                    CreateDataSource11();
                    DataGrid11.DataSource = dt11;
                    GenerateColumns(DataGrid11);
                    DataBind();
                }
            }
            else if (idInte == 12) {
                try
                {
                    int id_log1 = Convert.ToInt32(Request.Params["id"]);
                    Entities.IntegracionEntity data = IntegracionBL.getDetalleIntegracion12(id_log1);
                    int p1 = data.IdProyecto;
                    IntegracionBL.setIntegracion12(p1, id_log1);
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop867", Util.GetMensajeAlerta("success", "...", "Reprocesado exitosamente"), true);
                    Label1.Text = "No hay repocesos Pendientes";
                    Label1.CssClass = "alert-info";
                    Button1.Visible = false;
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                    CreateDataSource12();
                    DataGrid12.DataSource = dt12;
                    GenerateColumns(DataGrid12);
                    DataBind();
                }
            }

        }

    }
}
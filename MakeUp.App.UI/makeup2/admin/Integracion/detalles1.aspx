﻿<%@ Page Language="C#" MasterPageFile="~/makeup2/menu.master" AutoEventWireup="true" CodeBehind="detalles1.aspx.cs" Inherits="MakeUp.App.UI.makeup2.admin.Integracion.detalles1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headMenu" runat="server">
    <link href="../../../css/bootstrap-typeahead.css" rel="stylesheet" />
    <link rel="stylesheet" href="../../../css/bootstrap-select.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/autocomplete-query.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap3-typeahead.min.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/numeral.min.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/languages/es.js") %>' type="text/javascript"></script>
    <h2 class="page-header">Logs de Procedimientos de Integración</h2>
    <%--<p>Lista de estado de integraciones procedimiento <strong>z_ma_addCentroCosto</strong> &nbsp <a href="javascript:history.back(-1);" title="Ir la página anterior">Volver</a></p>--%>
    <div class="form-group">    
       <div class="col-lg-12">
           <div class="row">       
                <h4><asp:Label ID="Label2" runat="server" Text="" ForeColor="ControlDark"></asp:Label></h4>
            <%--</div>
            <div class="col-lg-7">
                <div class="pull-left">--%>
                    <a href="javascript:history.back(-1);" title="Ir la página anterior"><strong>Volver</strong></a>
               <%-- </div>--%>
            </div>
        </div>
    </div>
        <div class="form-group">
            <div class="col-lg-12">
                <div class="row">
                    <asp:Label id="lblBuscarBaja" runat="server"></asp:Label>
                   <div class="table-responsive">
                    <asp:GridView ID="gvDetalleIntegracion1" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-responsive" GridLines="None" OnRowDataBound="gvDetalleIntegracion1_RowDataBound" > 
                        <Columns>
                             <asp:TemplateField HeaderText="Id Proyecto">
                                <ItemTemplate>
                                    <asp:Label ID="idproyecto" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                              <asp:TemplateField HeaderText="Desc Sp Cont">
                                <ItemTemplate>
                                    <asp:Label ID="descspcont" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:TemplateField HeaderText="Desc Sp Call">
                                <ItemTemplate>
                                    <asp:Label ID="descspcall" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="estado" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Excepcion">
                                <ItemTemplate>
                                    <asp:Label ID="excepcion" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Fecha/ Hora">
                                <ItemTemplate>
                                    <asp:Label ID="fecha" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                         <asp:HyperLinkField  DataNavigateUrlFields="Id_log1" DataNavigateUrlFormatString="accion1.aspx?id={0}&idInte=1"  DataTextField="Id_log1" DataTextFormatString="Detalle" HeaderText="Acción"/>
                        </Columns>
                    </asp:GridView>
                     <asp:GridView ID="gvDetalleIntegracion2" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-responsive" GridLines="None" OnRowDataBound="gvDetalleIntegracion2_RowDataBound" > 
                        <Columns>
                            <asp:TemplateField HeaderText="Id Proyecto">
                                <ItemTemplate>
                                    <asp:Label ID="idproyecto" runat="server"></asp:Label>
                                </ItemTemplate>
                                </asp:TemplateField>
                              <asp:TemplateField HeaderText="Desc Sp Cont">
                                <ItemTemplate>
                                    <asp:Label ID="descspcont" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:TemplateField HeaderText="Desc Sp Call">
                                <ItemTemplate>
                                    <asp:Label ID="descspcall" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="estado" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Excepcion">
                                <ItemTemplate>
                                    <asp:Label ID="excepcion" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Fecha/ Hora">
                                <ItemTemplate>
                                    <asp:Label ID="fecha" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                         <asp:HyperLinkField  DataNavigateUrlFields="Id_log1" DataNavigateUrlFormatString="accion1.aspx?id={0}&idInte=2"  DataTextField="Id_log1" DataTextFormatString="Detalle" HeaderText="Acción"/>
                        </Columns>
                    </asp:GridView>
                    <asp:GridView ID="gvDetalleIntegracion3" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-responsive" GridLines="None" OnRowDataBound="gvDetalleIntegracion3_RowDataBound" > 
                        <Columns>
                             <asp:TemplateField HeaderText="Id Proyecto">
                                <ItemTemplate>
                                    <asp:Label ID="idproyecto" runat="server"></asp:Label>
                                </ItemTemplate>
                                </asp:TemplateField>
                              <asp:TemplateField HeaderText="Desc Sp Cont">
                                <ItemTemplate>
                                    <asp:Label ID="descspcont" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:TemplateField HeaderText="Desc Sp Call">
                                <ItemTemplate>
                                    <asp:Label ID="descspcall" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="estado" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Excepcion">
                                <ItemTemplate>
                                    <asp:Label ID="excepcion" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Fecha/ Hora">
                                <ItemTemplate>
                                    <asp:Label ID="fecha" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                         <asp:HyperLinkField  DataNavigateUrlFields="Id_log1" DataNavigateUrlFormatString="accion1.aspx?id={0}&idInte=3"  DataTextField="Id_log1" DataTextFormatString="Detalle" HeaderText="Acción"/>
                        </Columns>
                    </asp:GridView>
                       <asp:GridView ID="gvDetalleIntegracion4" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-responsive" GridLines="None" OnRowDataBound="gvDetalleIntegracion4_RowDataBound" > 
                        <Columns>
                            <asp:TemplateField HeaderText="Id Proyecto">
                                <ItemTemplate>
                                    <asp:Label ID="idproyecto" runat="server"></asp:Label>
                                </ItemTemplate>
                                </asp:TemplateField>
                              <asp:TemplateField HeaderText="Desc Sp Cont">
                                <ItemTemplate>
                                    <asp:Label ID="descspcont" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:TemplateField HeaderText="Desc Sp Call">
                                <ItemTemplate>
                                    <asp:Label ID="descspcall" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="estado" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Excepcion">
                                <ItemTemplate>
                                    <asp:Label ID="excepcion" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Fecha/ Hora">
                                <ItemTemplate>
                                    <asp:Label ID="fecha" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                         <asp:HyperLinkField  DataNavigateUrlFields="Id_log1" DataNavigateUrlFormatString="accion1.aspx?id={0}&idInte=4"  DataTextField="Id_log1" DataTextFormatString="Detalle" HeaderText="Acción"/>
                        </Columns>
                    </asp:GridView>
                       <asp:GridView ID="gvDetalleIntegracion5" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-responsive" GridLines="None" OnRowDataBound="gvDetalleIntegracion5_RowDataBound" > 
                        <Columns>
                              <asp:TemplateField HeaderText="Desc Sp Cont">
                                <ItemTemplate>
                                    <asp:Label ID="descspcont" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:TemplateField HeaderText="Desc Sp Call">
                                <ItemTemplate>
                                    <asp:Label ID="descspcall" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="estado" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Excepcion">
                                <ItemTemplate>
                                    <asp:Label ID="excepcion" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Fecha/ Hora">
                                <ItemTemplate>
                                    <asp:Label ID="fecha" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                         <asp:HyperLinkField  DataNavigateUrlFields="Id_log1" DataNavigateUrlFormatString="accion1.aspx?id={0}&idInte=5"  DataTextField="Id_log1" DataTextFormatString="Detalle" HeaderText="Acción"/>
                        </Columns>
                    </asp:GridView>
                       <asp:GridView ID="gvDetalleIntegracion6" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-responsive" GridLines="None" OnRowDataBound="gvDetalleIntegracion6_RowDataBound" > 
                        <Columns>
                             <asp:TemplateField HeaderText="Id Proyecto">
                                <ItemTemplate>
                                    <asp:Label ID="idproyecto" runat="server"></asp:Label>
                                </ItemTemplate>
                                </asp:TemplateField>
                              <asp:TemplateField HeaderText="Desc Sp Cont">
                                <ItemTemplate>
                                    <asp:Label ID="descspcont" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:TemplateField HeaderText="Desc Sp Call">
                                <ItemTemplate>
                                    <asp:Label ID="descspcall" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="estado" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Excepcion">
                                <ItemTemplate>
                                    <asp:Label ID="excepcion" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Fecha/ Hora">
                                <ItemTemplate>
                                    <asp:Label ID="fecha" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                         <asp:HyperLinkField  DataNavigateUrlFields="Id_log1" DataNavigateUrlFormatString="accion1.aspx?id={0}&idInte=6"  DataTextField="Id_log1" DataTextFormatString="Detalle" HeaderText="Acción"/>
                        </Columns>
                    </asp:GridView>
                       <asp:GridView ID="gvDetalleIntegracion7" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-responsive" GridLines="None" OnRowDataBound="gvDetalleIntegracion7_RowDataBound" > 
                        <Columns>
                              <asp:TemplateField HeaderText="Desc Sp Cont">
                                <ItemTemplate>
                                    <asp:Label ID="descspcont" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:TemplateField HeaderText="Desc Sp Call">
                                <ItemTemplate>
                                    <asp:Label ID="descspcall" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="estado" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Excepcion">
                                <ItemTemplate>
                                    <asp:Label ID="excepcion" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Fecha/ Hora">
                                <ItemTemplate>
                                    <asp:Label ID="fecha" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                         <asp:HyperLinkField  DataNavigateUrlFields="Id_log1" DataNavigateUrlFormatString="accion1.aspx?id={0}&idInte=7"  DataTextField="Id_log1" DataTextFormatString="Detalle" HeaderText="Acción"/>
                        </Columns>
                    </asp:GridView>
                       <asp:GridView ID="gvDetalleIntegracion8" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-responsive" GridLines="None" OnRowDataBound="gvDetalleIntegracion8_RowDataBound" > 
                        <Columns>
                            <asp:TemplateField HeaderText="Id Proyecto">
                                <ItemTemplate>
                                    <asp:Label ID="idproyecto" runat="server"></asp:Label>
                                </ItemTemplate>
                                </asp:TemplateField>
                              <asp:TemplateField HeaderText="Desc Sp Cont">
                                <ItemTemplate>
                                    <asp:Label ID="descspcont" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:TemplateField HeaderText="Desc Sp Call">
                                <ItemTemplate>
                                    <asp:Label ID="descspcall" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="estado" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Excepcion">
                                <ItemTemplate>
                                    <asp:Label ID="excepcion" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Fecha/ Hora">
                                <ItemTemplate>
                                    <asp:Label ID="fecha" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                         <asp:HyperLinkField  DataNavigateUrlFields="Id_log1" DataNavigateUrlFormatString="accion1.aspx?id={0}&idInte=8"  DataTextField="Id_log1" DataTextFormatString="Detalle" HeaderText="Acción"/>
                        </Columns>
                    </asp:GridView>
                       <asp:GridView ID="gvDetalleIntegracion9" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-responsive" GridLines="None" OnRowDataBound="gvDetalleIntegracion9_RowDataBound" > 
                        <Columns>
                            <asp:TemplateField HeaderText="Id Proyecto">
                                <ItemTemplate>
                                    <asp:Label ID="idproyecto" runat="server"></asp:Label>
                                </ItemTemplate>
                                </asp:TemplateField>
                              <asp:TemplateField HeaderText="Desc Sp Cont">
                                <ItemTemplate>
                                    <asp:Label ID="descspcont" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:TemplateField HeaderText="Desc Sp Call">
                                <ItemTemplate>
                                    <asp:Label ID="descspcall" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="estado" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Excepcion">
                                <ItemTemplate>
                                    <asp:Label ID="excepcion" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Fecha/ Hora">
                                <ItemTemplate>
                                    <asp:Label ID="fecha" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                         <asp:HyperLinkField  DataNavigateUrlFields="Id_log1" DataNavigateUrlFormatString="accion1.aspx?id={0}&idInte=9"  DataTextField="Id_log1" DataTextFormatString="Detalle" HeaderText="Acción"/>
                        </Columns>
                    </asp:GridView>
                        <asp:GridView ID="gvDetalleIntegracion10" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-responsive" GridLines="None" OnRowDataBound="gvDetalleIntegracion10_RowDataBound" > 
                        <Columns>
                              <asp:TemplateField HeaderText="Desc Sp Cont">
                                <ItemTemplate>
                                    <asp:Label ID="descspcont" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:TemplateField HeaderText="Desc Sp Call">
                                <ItemTemplate>
                                    <asp:Label ID="descspcall" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="estado" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Excepcion">
                                <ItemTemplate>
                                    <asp:Label ID="excepcion" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Fecha/ Hora">
                                <ItemTemplate>
                                    <asp:Label ID="fecha" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                         <asp:HyperLinkField  DataNavigateUrlFields="Id_log1" DataNavigateUrlFormatString="accion1.aspx?id={0}&idInte=10"  DataTextField="Id_log1" DataTextFormatString="Detalle" HeaderText="Acción"/>
                        </Columns>
                    </asp:GridView>
                       <asp:GridView ID="gvDetalleIntegracion11" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-responsive" GridLines="None" OnRowDataBound="gvDetalleIntegracion11_RowDataBound" > 
                        <Columns>
                               <asp:TemplateField HeaderText="Id Proyecto">
                                <ItemTemplate>
                                    <asp:Label ID="idproyecto" runat="server"></asp:Label>
                                </ItemTemplate>
                                </asp:TemplateField>
                              <asp:TemplateField HeaderText="Desc Sp Cont">
                                <ItemTemplate>
                                    <asp:Label ID="descspcont" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:TemplateField HeaderText="Desc Sp Call">
                                <ItemTemplate>
                                    <asp:Label ID="descspcall" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="estado" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Excepcion">
                                <ItemTemplate>
                                    <asp:Label ID="excepcion" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Fecha/ Hora">
                                <ItemTemplate>
                                    <asp:Label ID="fecha" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                         <asp:HyperLinkField  DataNavigateUrlFields="Id_log1" DataNavigateUrlFormatString="accion1.aspx?id={0}&idInte=11"  DataTextField="Id_log1" DataTextFormatString="Detalle" HeaderText="Acción"/>
                        </Columns>
                    </asp:GridView>
                       <asp:GridView ID="gvDetalleIntegracion12" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-responsive" GridLines="None" OnRowDataBound="gvDetalleIntegracion12_RowDataBound" > 
                        <Columns>
                              <asp:TemplateField HeaderText="Desc Sp Cont">
                                <ItemTemplate>
                                    <asp:Label ID="descspcont" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:TemplateField HeaderText="Desc Sp Call">
                                <ItemTemplate>
                                    <asp:Label ID="descspcall" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="estado" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Excepcion">
                                <ItemTemplate>
                                    <asp:Label ID="excepcion" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Fecha/ Hora">
                                <ItemTemplate>
                                    <asp:Label ID="fecha" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                         <asp:HyperLinkField  DataNavigateUrlFields="Id_log1" DataNavigateUrlFormatString="accion1.aspx?id={0}&idInte=12"  DataTextField="Id_log1" DataTextFormatString="Detalle" HeaderText="Acción"/>
                        </Columns>
                    </asp:GridView>
                </div>
                </div>
            </div>
        </div>
</asp:Content>

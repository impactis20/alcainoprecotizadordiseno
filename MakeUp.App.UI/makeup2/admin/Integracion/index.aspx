﻿<%@ Page Language="C#" MasterPageFile="~/makeup2/menu.master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="MakeUp.App.UI.makeup2.admin.Integracion.index" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headMenu" runat="server">
    <link href="../../../css/bootstrap-typeahead.css" rel="stylesheet" />
    <link rel="stylesheet" href="../../../css/bootstrap-select.css" />
    <link href="../../../css/jquery.dataTables.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/autocomplete-query.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap3-typeahead.min.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/numeral.min.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/languages/es.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/jquery.dataTables.min.js") %>' type="text/javascript"></script>
    
    <h2 class="page-header">Logs de Procedimientos de Integración</h2>
    <div class="form-group">
        <div class="form-group">
            <div class="col-lg-12">
                <div class="row">
                    <asp:Label id="lblBuscarBaja" runat="server"></asp:Label>
                    <div class="table-responsive">
                        <asp:GridView ID="gvIndex" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-responsive" GridLines="None" OnRowDataBound="gvIndex_RowDataBound" > 
                        <Columns>
                            <asp:TemplateField HeaderText="Id">
                                <ItemTemplate>
                                    <asp:Label ID="lblId" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Descripcion">
                                <ItemTemplate>
                                    <asp:Label ID="lblDescripcion" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
<%--                            <asp:TemplateField HeaderText="Procedimiento">
                                <ItemTemplate>
                                    <asp:Label ID="lblProcedimiento" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="lblEstado" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Errores">
                                <ItemTemplate>
                                    <asp:Label ID="lblErrores" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                              <asp:HyperLinkField DataNavigateUrlFields="id" DataNavigateUrlFormatString="detalles1.aspx?id={0}" DataTextField="id" DataTextFormatString="Ver" HeaderText="Acción"/>  
     
                        </Columns>
                    </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
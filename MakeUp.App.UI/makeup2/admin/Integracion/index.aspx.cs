﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.admin.Integracion
{
    public partial class index : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            ActualizaContador();

            if (!IsPostBack)
            {
                BindIndex();
            }
        }

        private void BindIndex()
        {
            List<Entities.Index> Lst = IntegracionBL.getDetalleIndex();
            gvIndex.DataSource = Lst;
            gvIndex.DataBind();

            if (Lst.Count > 0)
            {
                gvIndex.UseAccessibleHeader = true;
                gvIndex.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvIndex.UseAccessibleHeader = false;
                gvIndex.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
        protected void gvIndex_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.Index index = (Entities.Index)r.DataItem;

                (r.Controls[0].FindControl("lblId") as Label).Text = index.id.ToString();
                (r.Controls[0].FindControl("lblDescripcion") as Label).Text = index.descripcion.ToString();
                //(r.Controls[0].FindControl("lblProcedimiento") as Label).Text = index.procedimiento.ToString();
                (r.Controls[0].FindControl("lblEstado") as Label).Text = index.estado.ToString();
                (r.Controls[0].FindControl("lblErrores") as Label).Text = index.errores.ToString();
                HyperLink detalle = r.Controls[0].FindControl("detalle") as HyperLink;


            }
        }

        private void ActualizaContador() {

            int Repuesta = IntegracionBL.setActualizaContador();
        }
    }
}
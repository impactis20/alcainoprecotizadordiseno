﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.admin.rol_predeterminado
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDataListRolPredeterminado(RolPredeterminadoBL.GetListadoRolPredeterminadoCreado());
                BindDataAddModulo();

                txtAddDescripcion.Attributes.Add("onkeyup", "countChar(this, '" + lblAddDescripcion.ClientID + "', " + txtAddDescripcion.MaxLength + ")");
                txtDescripcion.Attributes.Add("onkeyup", "countChar(this, '" + lblDescripcion.ClientID + "', " + txtDescripcion.MaxLength + ")");
            }
        }

        private void BindDataListRolPredeterminado(List<Entities.RolPredeterminadoEntity> Lst)
        {
            lstRolPredeterminado.DataTextField = "Nombre";
            lstRolPredeterminado.DataValueField = "ID";

            lstRolPredeterminado.DataSource = Lst;
            lstRolPredeterminado.DataBind();

            if (Lst.Count > 0)
            {
                GetRolPredeterminado(lstRolPredeterminado.Items[0].Value);
                btnEditar.Visible = true;
                btnEliminar.Visible = true;
                controles.Visible = true;
                desc.Visible = true;
            }
            else
            {
                btnEditar.Visible = false;
                btnEliminar.Visible = false;
                controles.Visible = false;
                desc.Visible = false;
            }
        }

        private void BindDataAddModulo()
        {
            List<Entities.PaginaEntity> Lst = PaginaBL.GetAllPaginaControles();

            BindDataGV(gvAddModulo, Lst);
        }

        private void BindDataGV(GridView gv, List<Entities.PaginaEntity> Lst)
        {
            gv.DataKeyNames = new string[1] { "ID" }; //PK
            gv.DataSource = Lst;
            gv.DataBind();

            if (Lst.Count > 0)
            {
                gv.UseAccessibleHeader = true;
                gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gvAddModulo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.PaginaEntity Item = (Entities.PaginaEntity)r.DataItem;

                CheckBox cbModulo = (r.Controls[0].FindControl("cbModulo") as CheckBox);
                cbModulo.Text = string.Format("<b>{0}</b>", Item.Nombre);

                CheckBoxList cblControl = (r.Controls[0].FindControl("cblControl") as CheckBoxList);
                cblControl.CssClass += " " + cbModulo.ClientID;

                if(Item.PermitidoPorDefecto)
                {
                    cbModulo.Checked = true;
                    cbModulo.Enabled = false;
                }

                if (Item.Controles.Count == 0)
                    (r.Controls[0].FindControl("addControles") as HtmlGenericControl).Visible = false;

                cblControl.DataValueField = "ID";
                cblControl.DataTextField = "Descripcion";
                cblControl.DataSource = Item.Controles;
                cblControl.DataBind();
            }
        }

        protected void btnCrearRol_Click(object sender, EventArgs e)
        {
            try
            {
                List<Entities.PaginaEntity> LstPagina = new List<Entities.PaginaEntity>();
                List<Entities.PaginaEntity.Control> LstControl = new List<Entities.PaginaEntity.Control>();

                foreach (GridViewRow r in gvAddModulo.Rows)
                {
                    CheckBox cbModulo = (r.Controls[0].FindControl("cbModulo") as CheckBox);

                    if (cbModulo.Checked)
                    {
                        LstPagina.Add(new Entities.PaginaEntity(Convert.ToInt16(gvAddModulo.DataKeys[r.RowIndex].Value.ToString()))); //Agrega ID de la página (módulo) seleccionado

                        foreach (ListItem cb in (r.Controls[0].FindControl("cblControl") as CheckBoxList).Items)
                            if (cb.Selected)
                                LstControl.Add(new Entities.PaginaEntity.Control(Convert.ToInt16(cb.Value))); //Agrega a la lista el ID de los controles seleccionados
                    }
                }

                BindDataAddModulo();

                //Inserta lista de páginas (módulo) y de controles seleccionados
                RolPredeterminadoBL.AddRolPredeterminado(txtAddNombre.Text.Trim(), txtAddDescripcion.Text.Trim(), LstPagina, LstControl);

                //limpia formulario
                txtAddNombre.Text = string.Empty;
                txtAddDescripcion.Text = string.Empty;

                BindDataListRolPredeterminado(RolPredeterminadoBL.GetListadoRolPredeterminadoCreado());

                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("success", "Operación satisfactoria", "El rol fue creado exitosamente."), true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop87", Util.GetMensajeAlerta("error", ex.Source, ex.Message), true);
            }
        }

        protected void gvModulo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.PaginaEntity Item = (Entities.PaginaEntity)r.DataItem;

                CheckBox cbModulo = (r.Controls[0].FindControl("cbModulo") as CheckBox);
                cbModulo.Text = string.Format("<b>{0}</b>", Item.Nombre);
                cbModulo.Checked = Item.Seleccionado;

                CheckBoxList cblControl = (r.Controls[0].FindControl("cblControl") as CheckBoxList);
                cblControl.CssClass += " " + cbModulo.ClientID;

                if (Item.PermitidoPorDefecto)
                {
                    cbModulo.Checked = true;
                    cbModulo.Enabled = false;
                }

                if (Item.Controles.Count == 0)
                    (r.Controls[0].FindControl("controles") as HtmlGenericControl).Visible = false;
                else
                {
                    foreach (Entities.PaginaEntity.Control Control in Item.Controles)
                    {
                        ListItem item = new ListItem(Control.Descripcion, Control.ID.ToString());
                        item.Selected = Control.Seleccionado;
                        cblControl.Items.Add(item);
                    }

                    cblControl.DataBind();
                }
            }
        }

        protected void lstRolPredeterminado_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetRolPredeterminado(lstRolPredeterminado.SelectedItem.Value);
        }

        private void GetRolPredeterminado(string ID)
        {
            int intID = Convert.ToInt16(ID);
            Entities.RolPredeterminadoEntity RolPredeterminado = RolPredeterminadoBL.GetRolPredeterminado(intID);
            txtNombre.Text = RolPredeterminado.Nombre;
            txtDescripcion.Text = RolPredeterminado.Descripcion;
            BindDataGV(gvModulo, RolPredeterminado.Modulo);

            //Obtienes la lista de notificaciones para desplegarlas y poder filtrar el rol
            gvFiltroRolNotificacion.DataKeyNames = new string[1] { "ID" }; //PK
            gvFiltroRolNotificacion.DataSource = NotificacionBL.GetFiltrarRolEnNotificacion(intID);
            gvFiltroRolNotificacion.DataBind();
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            lstRolPredeterminado.Visible = false;
            txtNombre.Visible = true;
            txtNombre.Enabled = true;
            txtDescripcion.Enabled = true;
            controles.Enabled = true; //desbloquea Gridview

            btnEditar.Visible = false;
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
            btnEliminar.Visible = false;
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            int IDRolPredeterminado = Convert.ToInt16(lstRolPredeterminado.SelectedItem.Value);
            lstRolPredeterminado.Visible = true;
            txtNombre.Visible = false;
            txtNombre.Enabled = false;
            txtDescripcion.Enabled = false;
            controles.Enabled = false;//bloquea Gridview

            List<Entities.PaginaEntity> LstPagina = new List<Entities.PaginaEntity>();
            List<Entities.PaginaEntity.Control> LstControl = new List<Entities.PaginaEntity.Control>();

            //obtienes las páginas (módulos) y controles seleccionados
            foreach (GridViewRow r in gvModulo.Rows)
            {
                CheckBox cbModulo = (r.Controls[0].FindControl("cbModulo") as CheckBox);

                if (cbModulo.Checked)
                {
                    LstPagina.Add(new Entities.PaginaEntity(Convert.ToInt16(gvModulo.DataKeys[r.RowIndex].Value.ToString()))); //Agrega ID de la página (módulo) seleccionado

                    foreach (ListItem cb in (r.Controls[0].FindControl("cblControl") as CheckBoxList).Items)
                        if (cb.Selected)
                            LstControl.Add(new Entities.PaginaEntity.Control(Convert.ToInt16(cb.Value))); //Agrega a la lista el ID de los controles seleccionados
                }
            }

            //Elimina todas las páginas y controles del rol
            RolPredeterminadoBL.SetRolPredeterminado(IDRolPredeterminado, txtNombre.Text.Trim(), txtDescripcion.Text.Trim(), LstPagina, LstControl);

            #region Permite configurar qué roles
            List<Entities.NotificacionEntity> LstNotificacion = new List<Entities.NotificacionEntity>();
            //obtienes las notificaciones en las que el rol se puede filtrar
            foreach (GridViewRow r in gvFiltroRolNotificacion.Rows)
            {
                CheckBox cbModulo = (r.Controls[0].FindControl("cbPermite") as CheckBox);
                if (cbModulo.Checked)
                    LstNotificacion.Add(new Entities.NotificacionEntity(Convert.ToInt16(gvFiltroRolNotificacion.DataKeys[r.RowIndex].Value.ToString()))); //Agrega ID de la página (módulo) seleccionado
            }

            //Vincula las notificaciones al rol para poder filtrar en el módulo notificaciones
            NotificacionBL.AddPermiteFiltrarRolEnNotificacion(IDRolPredeterminado, LstNotificacion);
            #endregion

            //Verifica si el nombre del rol fue modificado
            if (lstRolPredeterminado.SelectedItem.Text != txtNombre.Text)
            {
                BindDataListRolPredeterminado(RolPredeterminadoBL.GetListadoRolPredeterminadoCreado()); //refresca lista de roles predeterminado
                lstRolPredeterminado.SelectedValue = IDRolPredeterminado.ToString();
            }

            GetRolPredeterminado(lstRolPredeterminado.SelectedItem.Value); //Refresca nuevamente las páginas y controles

            ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("success", "Operación satisfactoria", "El rol fue actualizado correctamente."), true);

            btnEditar.Visible = true;
            btnGuardar.Visible = false;
            btnCancelar.Visible = false;
            btnEliminar.Visible = true;
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            lstRolPredeterminado.Visible = true;
            txtNombre.Visible = false;
            txtNombre.Enabled = false;
            txtDescripcion.Enabled = false;
            controles.Enabled = false; //bloquea Gridview

            GetRolPredeterminado(lstRolPredeterminado.SelectedItem.Value); //Refresca nuevamente las páginas y controles del rol seleccionado

            btnEditar.Visible = true;
            btnGuardar.Visible = false;
            btnCancelar.Visible = false;
            btnEliminar.Visible = true;
        }

        protected void btnConfirmarEliminar_ServerClick(object sender, EventArgs e)
        {
            int IDRolSeleccionado = Convert.ToInt16(lstRolPredeterminado.SelectedItem.Value);
            BindDataListRolPredeterminado(RolPredeterminadoBL.DelRolPredeterminado(IDRolSeleccionado));

            ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("success", "Operación satisfactoria", "El rol fue eliminado."), true);
        }
    }
}
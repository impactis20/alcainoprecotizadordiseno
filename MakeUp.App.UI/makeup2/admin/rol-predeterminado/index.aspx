﻿<%@ Page Title="Roles predeterminados" Language="C#" MasterPageFile="~/makeup2/menu.master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="MakeUp.App.UI.makeup2.admin.rol_predeterminado.index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headMenu" runat="server">
    <link rel="stylesheet" href="../../../css/bootstrap-select.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">    
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/personalizado/count-character-textbox.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/checkbox-roles.js") %>'></script>

    <script>
        function alertaEliminar() {
            $(function () {
                $('#<%= nombreRolEliminar.ClientID %>').text($("#<%= lstRolPredeterminado.ClientID %> option:selected").text());

                //levanta popup
                $('#<%= Eliminar.ClientID %>').modal({
                    backdrop: 'static',
                    keyboard: true
                });
            });
        }
    </script>
    <div class="modal fade" id="x3" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Agregar rol predeterminado</h2>
                </div>
                <div class="modal-body">
                    <p>Cree un rol predeterminado ingresando el nombre y seleccionando los módulos y controles a los cual desea conceder el acceso. Para finalizar haga clic en <label class="label label-primary">Crear rol</label>.</p>
                    
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12">
                                <var>Los campos con asterisco (*) son obligatorios.</var>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <h3>* Nombre</h3>
                                    <asp:TextBox ID="txtAddNombre" runat="server" CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvAddNombre" runat="server" ErrorMessage="* Campo obligatorio" ControlToValidate="txtAddNombre" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Crear" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <h3>* Descripción</h3>
                                    <asp:TextBox ID="txtAddDescripcion" runat="server" CssClass="form-control input-sm" TextMode="MultiLine" Rows="3" MaxLength="150" autocomplete="off"></asp:TextBox>
                                    <asp:Label ID="lblAddDescripcion" runat="server" CssClass="help-block pull-right"></asp:Label>
                                    <asp:RequiredFieldValidator ID="rfvAddDescripcion" runat="server" ErrorMessage="* Campo obligatorio" ControlToValidate="txtAddDescripcion" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Crear" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="gvAddModulo" runat="server" CssClass="col-lg-12" AutoGenerateColumns="false" GridLines="None" ShowHeader="false"
                                        OnRowDataBound="gvAddModulo_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-CssClass="row">
                                                <ItemTemplate>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <div class="form-group">
                                                                <div class="col-lg-12">
                                                                    <asp:CheckBox ID="cbModulo" runat="server" CssClass="checkbox panel-title" action="add">
                                                                
                                                                    </asp:CheckBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="addControles" runat="server" class="panel-body">
                                                            <div class="col-lg-12">
                                                                <asp:CheckBoxList ID="cblControl" runat="server" CssClass="checkbox">
                                                                </asp:CheckBoxList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-lg-12">
                        <div class="pull-right">
                            <asp:Button ID="btnCrearRol" runat="server" Text="Crear Rol" CssClass="btn btn-primary" OnClick="btnCrearRol_Click" ValidationGroup="Crear" />
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h2 class="page-header">Crear rol predeterminado</h2>
    <p>Seleccione un rol predeterminado para visualizarlo o editarlo. Para agregar un rol predeterminado haga <a href="#" data-toggle="modal" data-target="#x3">clic aquí</a>.</p>
    
    <div class="form-group">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-inline">
                    <h3>Rol predeterminado</h3>

                    <asp:ListBox ID="lstRolPredeterminado" runat="server" SelectionMode="Single" CssClass="selectpicker" AutoPostBack="true" OnSelectedIndexChanged="lstRolPredeterminado_SelectedIndexChanged"  data-width="auto">
                    </asp:ListBox>
                    <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control input-sm" Enabled="false" MaxLength="25" Visible="false" autocomplete="off" Width="200px"></asp:TextBox>
                    <asp:Button ID="btnEditar" runat="server" Text="Editar" CssClass="btn btn-info btn-sm" OnClick="btnEditar_Click" />
                    <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn btn-primary btn-sm" Visible="false" OnClick="btnGuardar_Click" ValidationGroup="Editar"/>
                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn btn-default btn-sm" Visible="false" OnClick="btnCancelar_Click"/>
                    <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" CssClass="btn btn-danger btn-sm" OnClientClick="javascript:alertaEliminar(); return false;" />
                    <asp:RequiredFieldValidator ID="rfvNombre" runat="server" ErrorMessage="Seleccione para buscar..." ControlToValidate="txtNombre" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Editar" />
                </div>
            </div>
        </div>
        <div id="desc" runat="server" class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <h3>Descripción</h3>
                    <asp:TextBox ID="txtDescripcion" runat="server" CssClass="form-control input-sm" Enabled="false" TextMode="MultiLine" Rows="3" MaxLength="150" autocomplete="off"></asp:TextBox>
                    <asp:Label ID="lblDescripcion" runat="server" CssClass="help-block pull-right"></asp:Label>
                    <asp:RequiredFieldValidator ID="rfvDescripcion" runat="server" ErrorMessage="* Campo obligatorio" ControlToValidate="txtDescripcion" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Editar" />
                </div>
            </div>
        </div>
        <asp:Panel ID="controles" runat="server" Enabled="false">
            <div class="row">
                <div class="col-lg-12">
                    <h3>Módulos y controles</h3>
                    <div class="table-responsive">
                        <asp:GridView ID="gvModulo" runat="server" CssClass="col-lg-12" AutoGenerateColumns="false" GridLines="None" ShowHeader="false"
                            OnRowDataBound="gvModulo_RowDataBound">
                            <Columns>
                                <asp:TemplateField ItemStyle-CssClass="row">
                                    <ItemTemplate>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <div class="form-group">
                                                    <div class="col-lg-12">
                                                        <asp:CheckBox ID="cbModulo" runat="server" CssClass="checkbox panel-title">
                                                        </asp:CheckBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="controles" runat="server" class="panel-body">
                                                <div class="col-lg-12">
                                                    <asp:CheckBoxList ID="cblControl" runat="server" CssClass="checkbox">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h3>Filtrar rol en notificación</h3>
                    <div class="table-responsive">
                        <asp:GridView ID="gvFiltroRolNotificacion" runat="server" CssClass="col-lg-12" AutoGenerateColumns="false" GridLines="None" ShowHeader="false">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbPermite" runat="server" Text='<%# Eval("NOMBRE") %>' Checked='<%# Eval("ESTADO") %>' CssClass="checkbox" style="margin-left:30px"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>

    <div id="Eliminar" runat="server" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Eliminar rol predeterminado</h4>
                </div>
                <div class="modal-body">
                    <p>¿Está seguro(a) que desea eliminar el rol predeterminado "<b id="nombreRolEliminar" runat="server"></b>"?.</p>
                </div>
                <div class="modal-footer">
                    <button id="btnConfirmarEliminar" runat="server" class="btn btn-primary" type="button" onserverclick="btnConfirmarEliminar_ServerClick">
                        <span class="fa fa-check"></span>&nbsp;&nbsp;Aceptar
                    </button>
                    <button class="btn btn-default" type="button" data-dismiss="modal">
                        <span class="fa fa-remove"></span>&nbsp;&nbsp;Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
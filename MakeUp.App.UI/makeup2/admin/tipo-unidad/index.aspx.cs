﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.admin.tipo_unidad
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    int ID = Convert.ToInt16(Request.QueryString["id"].ToString());
                    BindData(ID, string.Empty, string.Empty, false);
                }
                else
                {
                    BindData(0, txtNombre.Text, txtSigla.Text, false);
                }

                ViewState.Add("Nombre", string.Empty);
                ViewState.Add("Sigla", string.Empty);
            }
        }

        protected void btnBuscar_ServerClick(object sender, EventArgs e)
        {
            string Nombre = txtNombre.Text.Trim();
            string Sigla = txtSigla.Text.Trim();

            ViewState["Nombre"] = Nombre;
            ViewState["Sigla"]= Sigla;

            BindData(0, Nombre, Sigla, false);
        }

        protected void gvTipoUnidad_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvTipoUnidad.EditIndex = e.NewEditIndex;
            int IDTipoUnidad = Convert.ToInt16(gvTipoUnidad.DataKeys[e.NewEditIndex].Value);
            BindData(0, ViewState["Nombre"].ToString(), ViewState["Sigla"].ToString(), true);

            //Deshabilita formulario de búsqueda
            Util.DeshabilitaDIV(formulario);
        }

        protected void gvTipoUnidad_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int IDTipoUnidad = Convert.ToInt16(gvTipoUnidad.DataKeys[e.RowIndex].Value);//Convert.ToInt16(e.Keys["ID"].ToString());
            string Nombre = (gvTipoUnidad.Rows[e.RowIndex].FindControl("txtNombre") as TextBox).Text.Trim();
            string Sigla = (gvTipoUnidad.Rows[e.RowIndex].FindControl("txtSigla") as TextBox).Text.Trim();

            try
            {
                //actualiza valores
                TipoUnidadBL.SetTipoUnidad(IDTipoUnidad, Nombre, Sigla);

                ViewState["Nombre"] = Nombre;
                ViewState["Sigla"] = Sigla;

                gvTipoUnidad.EditIndex = -1;
                //actualiza el registro
                BindData(IDTipoUnidad, txtNombre.Text.Trim(), txtSigla.Text.Trim(), false);

                //finalizada la acción habilita nuevamente el formulario de búsqueda
                Util.HabilitaDIV(formulario);

                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("success", "Operación satisfactoria", "El registro fue actualizado."), true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        protected void gvTipoUnidad_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvTipoUnidad.EditIndex = -1;
            int IDTipoUnidad = Convert.ToInt16(gvTipoUnidad.DataKeys[e.RowIndex].Value);
            BindData(0, txtNombre.Text.Trim(), txtSigla.Text.Trim(), false);

            //finalizada la acción habilita nuevamente el formulario de búsqueda
            Util.HabilitaDIV(formulario);
        }

        private void BindData(int ID, string Nombre, string Sigla, bool EditaRegistro)
        {
            try
            {
                List<Entities.TipoUnidadEntity> lstTipoUnidad = TipoUnidadBL.GetTipoUnidad(ID, Nombre, Sigla, EditaRegistro);
                gvTipoUnidad.DataKeyNames = new string[1] { "ID" }; //PK
                gvTipoUnidad.DataSource = lstTipoUnidad;
                gvTipoUnidad.DataBind();

                if (lstTipoUnidad.Count > 0)
                {
                    gvTipoUnidad.UseAccessibleHeader = true;
                    gvTipoUnidad.HeaderRow.TableSection = TableRowSection.TableHeader;
                    lblBuscar.Text = string.Format("Resultados encontrados: <b>{0}</b>", lstTipoUnidad.Count);
                    lblBuscar.CssClass = "alert-info";
                }
                else
                {
                    lblBuscar.Text = "No se encontraron coincidencias para los parámetros ingresados";
                    lblBuscar.CssClass = "alert-danger";
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                return;
            }
        }

        protected void gvTipoUnidad_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                int IDTipoUnidad = Convert.ToInt16(gvTipoUnidad.DataKeys[e.RowIndex].Value);
                TipoUnidadBL.DelTipoUnidad(IDTipoUnidad);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }

            //quita y descuenta el contador
            gvTipoUnidad.Rows[e.RowIndex].Visible = false;
            lblBuscar.Text = string.Format("Resultados encontrados: <b>{0}</b>", Convert.ToInt16(lblBuscar.Text.Split('>')[1].Split('<')[0]) - 1);

            ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("success", "Operación satisfactoria", "El registro fue eliminado."), true);
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            string Nombre = txtAddNombre.Text.Trim();
            string Sigla = txtAddSigla.Text.Trim();

            try
            {
                int ID = TipoUnidadBL.AddTipoUnidad(Nombre, Sigla);

                //Limpia formulario
                txtAddNombre.Text = string.Empty;
                txtAddSigla.Text = string.Empty;

                //actualiza gridview con unidades
                BindData(0, string.Empty, string.Empty, false);

                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("success", "Operación satisfactoria", "El tipo de unidad ha sido agregado correctamente al sistema."), true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        [WebMethod]
        public static string[] GetNombre(string prefix)
        {
            return GenericoBL.GetAutocompletar(prefix, "tipo_unidad", "nombre").ToArray();
        }

        [WebMethod]
        public static string[] GetSigla(string prefix)
        {
            return GenericoBL.GetAutocompletar(prefix, "tipo_unidad", "sigla").ToArray();
        }
    }
}
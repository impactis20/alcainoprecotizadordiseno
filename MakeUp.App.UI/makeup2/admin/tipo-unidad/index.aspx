﻿<%@ Page Title="Tipos de unidad" Language="C#" MasterPageFile="~/makeup2/menu.master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="MakeUp.App.UI.makeup2.admin.tipo_unidad.index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headMenu" runat="server">
    <link href="../../../css/bootstrap-typeahead.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/autocomplete-query.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap3-typeahead.min.js") %>' type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            AutoComplete("<%= txtNombre.ClientID %>", '<%= ResolveUrl(string.Format("{0}/GetNombre", Request.Url.AbsolutePath)) %>');
            AutoComplete("<%= txtSigla.ClientID %>", '<%= ResolveUrl(string.Format("{0}/GetSigla", Request.Url.AbsolutePath)) %>');
        });
    </script>

    <div class="modal fade" id="x3" tabindex="-1" role="dialog" aria-labelledby="xxxx">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Agregar tipo de unidad</h2>
                </div>
                <div class="modal-body">
                    <p>Complete el formulario para agregar un Tipo de Unidad, completado el formulario presione el botón <span class="label label-primary">Agregar tipo de unidad</span>.</p>
                    
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12">
                                <var>Los campos con asterisco (*) son obligatorios.</var>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">* Nombre</label>
                                    <asp:TextBox ID="txtAddNombre" runat="server" CssClass="form-control input-sm" MaxLength="25" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtNombre" runat="server" ErrorMessage="Campo <b>Nombre</b> es obligatorio..." ControlToValidate="txtAddNombre" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarTU" />
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">* Sigla</label>
                                    <asp:TextBox ID="txtAddSigla" runat="server" CssClass="form-control input-sm" MaxLength="25" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtSigla" runat="server" ErrorMessage="Campo <b>sigla</b> es obligatorio..." ControlToValidate="txtAddSigla" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarTU" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right">
                                <asp:Button ID="btnAgregar" runat="server" Text="Agregar tipo de unidad" CssClass="btn btn-primary" OnClick="btnAgregar_Click" ValidationGroup="AgregarTU" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h2 class="page-header">Buscar tipo de unidad</h2>
    <p>Busque un tipo de unidad filtrando por nombre y/o sigla. Para agregar un nuevo Tipo de Unidad haga <a href="#" data-toggle="modal" data-target="#x3">clic aquí</a>.</p>
    <div class="form-group">
        <div id="formulario" runat="server">
            <div class="row">
                <div class="col-lg-2">
                    <div class="form-group">
                        <label>Nombre</label>
                        <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control input-sm" autocomplete="off" placeholder="Ingrese un nombre..."></asp:TextBox>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label>Sigla</label>
                        <asp:TextBox ID="txtSigla" runat="server" CssClass="form-control input-sm" autocomplete="off" placeholder="Ingrese una sigla..."></asp:TextBox>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="pull-right">
                        <br />
                        <button id="btnBuscar" class="btn btn-default" type="button" runat="server" onserverclick="btnBuscar_ServerClick">
                            <span class="fa fa-search"></span>&nbsp;&nbsp;Buscar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-lg-12">
            <div class="row">
                <asp:Label id="lblBuscar" runat="server"></asp:Label>
                <div class="table-responsive small">
                    <asp:GridView ID="gvTipoUnidad" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None"
                        OnRowEditing="gvTipoUnidad_RowEditing" OnRowUpdating="gvTipoUnidad_RowUpdating" OnRowCancelingEdit="gvTipoUnidad_RowCancelingEdit" OnRowDeleting="gvTipoUnidad_RowDeleting">
                        <Columns>
                            <asp:TemplateField HeaderText="Nombre" ItemStyle-CssClass="col-lg-3">
                                <ItemTemplate>
                                    <asp:Label ID="lblNombre" runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtNombre" runat="server" Text='<%# Eval("Nombre") %>' CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtNombre" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtNombre" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Editar" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sigla" ItemStyle-CssClass="col-lg-3">
                                <ItemTemplate>
                                    <asp:Label ID="lblSigla" runat="server" Text='<%# Eval("Sigla") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtSigla" runat="server" Text='<%# Eval("Sigla") %>' CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtSigla" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtSigla" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Editar" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-CssClass="col-lg-6 right">
                                <ItemTemplate>
                                    <div class="pull-right">
                                        <asp:Button id="btnEditar" runat="server" Text="Editar" CssClass="btn btn-info btn-xs" CommandName="Edit" />
                                        <asp:Button id="btnEliminar" runat="server" Text="Eliminar" CssClass="btn btn-danger btn-xs" CommandName="Delete" OnClientClick='<%# string.Format("return confirm(\"¿Estas seguro que deseas eliminar {0}?\");", Eval("Nombre")) %>' />
                                    </div>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <div class="pull-right">
                                        <asp:Button id="btnEditar" runat="server" Text="Guardar" CssClass="btn btn-primary btn-xs" CommandName="Update" ValidationGroup="Editar" />
                                        <asp:Button id="btnCancelar" runat="server" Text="Cancelar" CssClass="btn btn-default btn-xs" CommandName="Cancel" />
                                    </div>
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
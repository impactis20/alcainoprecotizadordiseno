﻿<%@ Page Title="Cliente" Language="C#" MasterPageFile="~/makeup2/menu.master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="MakeUp.App.UI.makeup2.admin.cliente.index" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headMenu" runat="server">
    <link href="../../../css/bootstrap-typeahead.css" rel="stylesheet" />
    <link rel="stylesheet" href="../../../css/bootstrap-select.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/autocomplete-query.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap3-typeahead.min.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/numeral.min.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/languages/es.js") %>' type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            AutoComplete("<%= txtBuscarRut.ClientID %>", '<%= ResolveUrl(string.Format("{0}/GetRUT", Request.Url.AbsolutePath)) %>');
            AutoComplete("<%= txtBuscarNombreFantasia.ClientID %>", '<%= ResolveUrl(string.Format("{0}/GetNombreFantasia", Request.Url.AbsolutePath)) %>');
            AutoComplete("<%= txtBuscarRazonSocial.ClientID %>", '<%= ResolveUrl(string.Format("{0}/GetRazonSocial", Request.Url.AbsolutePath)) %>');
        });

        function Rut(txtRut) {
            var valor = txtRut.value;
            var parteEntera = valor.split('-')[0];
            var dv = '';

            if (valor.indexOf("-") !== -1)
                dv = '-' + valor.split('-')[1].substring(0, 1);
            //if (howManyRepeated(valor) >= 2) {
            //    valor = valor.substring(0, valor.length - 1);
            //}

            numeral.language('es');
            txtRut.value = numeral(parteEntera.replace(/\./g, '')).format('0,0') + dv;
        }

        function howManyRepeated(str) {
            try { return str.replace('-', '').length; }
            catch (e) { return 0; } // if TypeError
        }
    </script>

    <div class="modal fade" id="x3" tabindex="-1" role="dialog" aria-labelledby="xxxx">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Agregar cliente</h2>
                </div>
                <div class="modal-body">
                    <p>Complete el siguiente formulario para agregar un cliente al sistema, completado el formulario presione el botón <span class="label label-primary">Agregar cliente</span>.</p>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12">
                                <var>Los campos con asterisco (*) son obligatorios.</var>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>* Nombre Fantasía</label>
                                    <asp:TextBox ID="txtNombreFantasia" runat="server" CssClass="form-control input-sm" MaxLength="50" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Campo <b>Nombre Fantasía</b> es obligatorio..." ControlToValidate="txtNombreFantasia" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarCliente" />
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>* Rut</label>
                                    <asp:TextBox ID="txtRut" runat="server" CssClass="form-control input-sm" MaxLength="12" onkeyup="javascript:Rut(this);" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtRut" runat="server" ErrorMessage="Campo <b>Rut</b> es obligatorio..." ControlToValidate="txtRut" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarCliente" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>* Razón social</label>
                                    <asp:TextBox ID="txtRazonSocial" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtRazonSocial" runat="server" ErrorMessage="Campo <b>Razón Social</b> es obligatorio..." ControlToValidate="txtRazonSocial" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarCliente" />
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>* Giro</label>
                                    <asp:TextBox ID="txtGiro" runat="server" CssClass="form-control input-sm" MaxLength="200" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtGiro" runat="server" ErrorMessage="Campo <b>Giro</b> es obligatorio..." ControlToValidate="txtGiro" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarCliente" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>* Dirección</label>
                                    <asp:TextBox ID="txtDireccion" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvDireccion" runat="server" ErrorMessage="Campo <b>Dirección</b> es obligatorio..." ControlToValidate="txtDireccion" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarCliente" />
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>* Comuna</label>
                                    <asp:TextBox ID="txtComuna" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvComuna" runat="server" ErrorMessage="Campo <b>Comuna</b> es obligatorio..." ControlToValidate="txtComuna" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarCliente" />
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>* Ciudad</label>
                                    <asp:TextBox ID="txtCiudad" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvCiudad" runat="server" ErrorMessage="Campo <b>Ciudad</b> es obligatorio..." ControlToValidate="txtCiudad" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarCliente" />
                                </div>
                            </div>
                            <%--<div class="col-lg-3">
                                <div class="form-group">
                                    <label>* Región</label>
                                    <asp:DropDownList ID="ddlRegion" runat="server" CssClass="selectpicker" DataValueField="ID" DataTextField="nombre" data-width="100%"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvRegion" runat="server" ErrorMessage="Campo <b>Dirección</b> es obligatorio..." ControlToValidate="ddlRegion" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarCliente" />
                                </div>
                            </div>--%>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>Teléfono central empresa</label>
                                    <asp:TextBox ID="txtFonoCentral" runat="server" CssClass="form-control input-sm" MaxLength="20" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right">
                                <asp:Button ID="btnAgregar" runat="server" Text="Agregar cliente" CssClass="btn btn-primary" OnClick="btnAgregar_Click" ValidationGroup="AgregarCliente" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h2 class="page-header">Buscar cliente</h2>
    <p>Busque un cliente del sistema ingresando Nombre de Fantasía o Razón Social. Para agregar un nuevo Cliente haga <a href="#" data-toggle="modal" data-target="#x3"><strong>CLICK AQUÍ</strong></a>.</p>
    <div id="formulario" runat="server" class="form-group">
        <div class="row">
            <div class="col-lg-3">
                <div class="form-group">
                    <label>RUT</label>
                    <asp:TextBox ID="txtBuscarRut" runat="server" CssClass="form-control input-sm" autocomplete="off" placeholder="Ingrese RUT..."></asp:TextBox>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label>Nombre de Fantasía</label>
                    <asp:TextBox ID="txtBuscarNombreFantasia" runat="server" CssClass="form-control input-sm" autocomplete="off" placeholder="Ingrese nombre de fantasía..."></asp:TextBox>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label>Razón Social</label>
                    <asp:TextBox ID="txtBuscarRazonSocial" runat="server" CssClass="form-control input-sm" autocomplete="off" placeholder="Ingrese una razón social..."></asp:TextBox>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="pull-right">
                    <br />
                    <button id="btnBuscar" class="btn btn-default" type="button" runat="server" onserverclick="btnBuscar_ServerClick">
                        <span class="fa fa-search"></span>&nbsp;&nbsp;Buscar
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-12">
            <div class="row">
                <asp:Label ID="lblBuscar" runat="server"></asp:Label>
                <div class="table-responsive small">
                    <asp:GridView ID="gvCliente" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-responsive" GridLines="None"
                        OnRowDataBound="gvCliente_RowDataBound" OnRowEditing="gvCliente_RowEditing" OnRowUpdating="gvCliente_RowUpdating" OnRowCancelingEdit="gvCliente_RowCancelingEdit">
                        <Columns>
                            <asp:TemplateField HeaderText="Rut" ItemStyle-CssClass="col-lg-1">
                                <ItemTemplate>
                                    <asp:Label ID="lblRut" runat="server" Text='<%# string.Format("{0}-{1}", Eval("Rut"), Eval("DV")) %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtRut" runat="server" Text='<%# string.Format("{0}-{1}", Eval("Rut"), Eval("DV")) %>' CssClass="form-control input-sm" MaxLength="12" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvRut" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtRut" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Editar" />
                                </EditItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Nombre Fantasía" ItemStyle-CssClass="col-lg-2">
                                <ItemTemplate>
                                    <asp:Label ID="lblNombreFantasia" runat="server" Text='<%# Eval("NombreFantasia") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtNombreFantasia" runat="server" Text='<%# Eval("NombreFantasia") %>' CssClass="form-control input-sm" MaxLength="100" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtNombreFantasia" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtNombreFantasia" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Editar" />
                                </EditItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Razón Social" ItemStyle-CssClass="col-lg-1">
                                <ItemTemplate>
                                    <asp:Label ID="lblRS" runat="server" Text='<%# Eval("RazonSocial") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtRS" runat="server" Text='<%# Eval("RazonSocial") %>' CssClass="form-control input-sm" MaxLength="100" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtRS" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtRS" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Editar" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Giro" ItemStyle-CssClass="col-lg-1">
                                <ItemTemplate>
                                    <asp:Label ID="lblGiro" runat="server" Text='<%# Eval("Giro") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtGiro" runat="server" Text='<%# Eval("Giro") %>' CssClass="form-control input-sm" MaxLength="200" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtGiro" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtGiro" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Editar" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Teléfono" ItemStyle-CssClass="col-lg-1">
                                <ItemTemplate>
                                    <asp:Label ID="lblTelefono" runat="server" Text='<%# Eval("Telefono") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtTelefono" runat="server" Text='<%# Eval("Telefono") %>' CssClass="form-control input-sm" MaxLength="20" autocomplete="off"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Dirección" ItemStyle-CssClass="col-lg-1">
                                <ItemTemplate>
                                    <asp:Label ID="lblDireccion" runat="server" Text='<%# Eval("Direccion") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtDireccion" runat="server" Text='<%# Eval("Direccion") %>' CssClass="form-control input-sm" MaxLength="100" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtDireccion" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtDireccion" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Editar" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comuna" ItemStyle-CssClass="col-lg-1">
                                <ItemTemplate>
                                    <asp:Label ID="lblComuna" runat="server" Text='<%# Eval("Comuna") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtComuna" runat="server" Text='<%# Eval("Comuna") %>' CssClass="form-control input-sm" MaxLength="100" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtComuna" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtComuna" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Editar" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ciudad" ItemStyle-CssClass="col-lg-1">
                                <ItemTemplate>
                                    <asp:Label ID="lblCiudad" runat="server" Text='<%# Eval("Ciudad") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtCiudad" runat="server" Text='<%# Eval("Ciudad") %>' CssClass="form-control input-sm" MaxLength="100" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtCiudad" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtCiudad" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Editar" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="Región" ItemStyle-CssClass="col-lg-2">
                                <ItemTemplate>
                                    <asp:Label ID="lblRegion" runat="server" Text='<%# Eval("Item2.Nombre") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlRegion" runat="server" CssClass="selectpicker" DataValueField="ID" DataTextField="Nombre" data-width="100%"></asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField ItemStyle-CssClass="col-lg-2 right">
                                <ItemTemplate>
                                    <div class="pull-right">
                                        <asp:Button id="btnEditar" runat="server" Text="Editar" CssClass="btn btn-info btn-xs" CommandName="Edit" />
                                        <%--<asp:Button id="btnEliminar" runat="server" Text="Eliminar" CssClass="btn btn-danger btn-xs" CommandName="Delete" OnClientClick='<%# string.Format("return confirm(\"¿Estas seguro que deseas eliminar {0}?\");", Eval("Item1.RazonSocial")) %>' />--%>
                                    </div>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <div class="pull-right">
                                        <asp:Button id="btnEditar" runat="server" Text="Guardar" CssClass="btn btn-primary btn-xs" CommandName="Update" ValidationGroup="Editar" />
                                        <asp:Button id="btnCancelar" runat="server" Text="Cancelar" CssClass="btn btn-default btn-xs" CommandName="Cancel" />
                                    </div>
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
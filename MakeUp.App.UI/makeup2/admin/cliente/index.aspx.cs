﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.admin.cliente
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    int ID = Convert.ToInt16(Request.QueryString["id"].ToString());
                    int Rut = Convert.ToInt16(Request.QueryString["Rut"].ToString());
                    BindData(ID, string.Empty, string.Empty, Rut, false);
                }
                else
                {
                    //Carga regiones en formulario "agregar cliente"
                    //BindRegiones();

                    BindData(0, txtBuscarNombreFantasia.Text.Trim(), txtBuscarRazonSocial.Text.Trim(), 0, false);
                }

                ViewState.Add("NombreFantasia", string.Empty);
                ViewState.Add("RazonSocial", string.Empty);
                ViewState.Add("Rut", string.Empty);
            }
        }

        //private void BindRegiones()
        //{
        //    ddlRegion.DataSource = GenericoBL.GetRegion();
        //    ddlRegion.DataBind();
        //}

        protected void btnBuscar_ServerClick(object sender, EventArgs e)
        {
            string NombreFantasia = txtBuscarNombreFantasia.Text.Trim();
            string RazonSocial = txtBuscarRazonSocial.Text.Trim();
            int Rut = 0;
            if (txtBuscarRut.Text.Trim()=="")
            {
                Rut = Rut;
            }
            else {
                Rut = Convert.ToInt32(txtBuscarRut.Text.Trim());
            }
            

            ViewState["NombreFantasia"] = NombreFantasia;
            ViewState["RazonSocial"] = RazonSocial;
            ViewState["Rut"] = Rut;

            BindData(0, NombreFantasia, RazonSocial, Rut, false);
        }

        protected void gvCliente_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.ClienteEntity Fila = (Entities.ClienteEntity)r.DataItem;

                //cuando edita...
                //if ((r.RowState & DataControlRowState.Edit) == DataControlRowState.Edit)
                //{
                //    DropDownList ddlRegion = (r.Controls[0].FindControl("ddlRegion") as DropDownList);
                //    ddlRegion.DataSource = GenericoBL.GetRegion();
                //    ddlRegion.SelectedValue = Fila.ID.ToString();
                //    ddlRegion.DataBind();
                //}
            }
        }

        protected void gvCliente_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvCliente.EditIndex = e.NewEditIndex;
            int IDCliente = Convert.ToInt16(gvCliente.DataKeys[e.NewEditIndex].Value);
            BindData(0, ViewState["NombreFantasia"].ToString(), ViewState["RazonSocial"].ToString(), 0, true);
            
            //Deshabilita formulario de búsqueda
            Util.DeshabilitaDIV(formulario);
        }

        protected void gvCliente_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow r = gvCliente.Rows[e.RowIndex];

            string Rut = Util.Funcion.FormatoRut((r.FindControl("txtRut") as TextBox).Text);
            int IDCliente = Convert.ToInt16(gvCliente.DataKeys[e.RowIndex].Value);
            string NombreFantasia = (r.FindControl("txtNombreFantasia") as TextBox).Text.Trim();
            string RazonSocial = (r.FindControl("txtRS") as TextBox).Text.Trim();

            try
            {
                //valida si el rut es válido
                if (Util.Funcion.RutValido(Rut))
                {
                    //actualiza valores
                    ClienteBL.SetCliente(IDCliente, Convert.ToInt32(Rut.Split('-')[0]), Rut.Split('-')[1], NombreFantasia, RazonSocial, (r.FindControl("txtGiro") as TextBox).Text.Trim(), (r.FindControl("txtDireccion") as TextBox).Text.Trim(), (r.FindControl("txtTelefono") as TextBox).Text, (r.FindControl("txtComuna") as TextBox).Text.Trim(), (r.FindControl("txtCiudad") as TextBox).Text.Trim());
                    
                    gvCliente.EditIndex = -1;
                    int RutBuscar = 0;

                    if (txtBuscarRut.Text.Trim()=="") {
                        RutBuscar = RutBuscar;
                    }
                    else {
                        RutBuscar = Convert.ToInt32(txtBuscarRut.Text.Trim());
                    }

                    //actualiza el registro
                    BindData(IDCliente, txtBuscarNombreFantasia.Text.Trim(), txtBuscarRazonSocial.Text.Trim(), RutBuscar, false);

                    ViewState["NombreFantasia"] = NombreFantasia;
                    ViewState["RazonSocial"] = RazonSocial;
                    ViewState["Rut"] = Rut;

                    //finalizada la acción habilita nuevamente el formulario de búsqueda
                    Util.HabilitaDIV(formulario);

                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("success", "Operación satisfactoria", "El registro fue actualizado."), true);
                }
                else
                {
                    RequiredFieldValidator rfvRut = (r.FindControl("rfvRut") as RequiredFieldValidator);
                    rfvRut.Text = "<b>Rut</b> es inválido...";
                    rfvRut.IsValid = false;
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        protected void gvCliente_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvCliente.EditIndex = -1;
            int IDCliente = Convert.ToInt16(gvCliente.DataKeys[e.RowIndex].Value);
            BindData(0, txtBuscarNombreFantasia.Text.Trim(), txtBuscarRazonSocial.Text.Trim(), 0, false);

            //finalizada la acción habilita nuevamente el formulario de búsqueda
            Util.HabilitaDIV(formulario);
        }

        //se elimina Eliminación de clientes según requerimiento
        //protected void gvCliente_RowDeleting(object sender, GridViewDeleteEventArgs e)
        //{
        //    try
        //    {
        //        HiddenField IDCliente = ((HiddenField)gvCliente.Rows[e.RowIndex].FindControl("hfIDCliente"));
        //        ClienteBL.DelCliente(Convert.ToInt16(IDCliente.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
        //    }

        //    //quita y descuenta el contador
        //    gvCliente.Rows[e.RowIndex].Visible = false;
        //    lblBuscar.Text = string.Format("Resultados encontrados: <b>{0}</b>", Convert.ToInt16(lblBuscar.Text.Split('>')[1].Split('<')[0]) - 1);

        //    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("success", "Operación satisfactoria", "El registro fue eliminado."), true);
        //}

        private void BindData(int ID, string NombreFantasia, string RazonSocial, int Rut, bool EditaRegistro)
        {
            try
            {
                List<Entities.ClienteEntity> Lst = ClienteBL.GetCliente(ID, NombreFantasia, RazonSocial, Rut, EditaRegistro);
                gvCliente.DataKeyNames = new string[1] { "ID" }; //PK
                gvCliente.DataSource = Lst;
                gvCliente.DataBind();

                if (Lst.Count > 0)
                {
                    gvCliente.UseAccessibleHeader = true;
                    gvCliente.HeaderRow.TableSection = TableRowSection.TableHeader;
                    lblBuscar.Text = string.Format("Resultados encontrados: <b>{0}</b>", Lst.Count);
                    lblBuscar.CssClass = "alert-info";
                }
                else
                {
                    lblBuscar.Text = "No se encontraron coincidencias para los parámetros ingresados";
                    lblBuscar.CssClass = "alert-danger";
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                return;
            }
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            string NombreFantasia = txtNombreFantasia.Text.Trim();
            string RazonSocial = txtRazonSocial.Text.Trim();
            string Giro = txtGiro.Text.Trim();
            string Rut = txtRut.Text.Trim();
            string Direccion = txtDireccion.Text.Trim();
            string Telefono = txtFonoCentral.Text.Trim();
            //int IDRegion = Convert.ToInt16(ddlRegion.SelectedItem.Value);
            string Comuna = txtComuna.Text.Trim();
            string Ciudad = txtCiudad.Text.Trim();
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
            
            if (Util.Funcion.RutValido(Rut))
            {
                string RutConFormato = Util.Funcion.FormatoRut(Rut);
                int RutSinDv = Convert.ToInt32(RutConFormato.Split('-')[0]);
                string DV = RutConFormato.Split('-')[1];

                if (!ClienteBL.ClienteExiste(RutSinDv))
                {
                    rfvtxtRut.IsValid = true;

                    try
                    {
                        int ID = 0;
                        ID = ClienteBL.AddCliente(RutSinDv, DV, NombreFantasia, RazonSocial, Giro, Direccion, Telefono, Comuna, Ciudad, IDUsuario); //cambiar usuario

                        //Limpia formulario
                        LimpiaFormulario();

                        //actualiza gridview con clientes
                        BindData(0, string.Empty, string.Empty, 0, false);

                        ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("success", "Operación satisfactoria", "El cliente ha sido agregado correctamente al sistema."), true);
                    }
                    catch (Exception ex)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                    }
                }
                else
                {
                    LimpiaFormulario();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop33", Util.GetMensajeAlerta("warning", "El cliente ya existe", "El cliente ya existe en la base de datos del sistema."), true);
                }
            }
            else
            {
                rfvtxtRut.Text = "El <b>Rut</b> es inválido...";
                rfvtxtRut.IsValid = false;

                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", "$('#x3').modal('show');", true);
            }
        }

        private void LimpiaFormulario()
        {
            //Limpia formulario
            txtNombreFantasia.Text = string.Empty;
            txtRazonSocial.Text = string.Empty;
            txtGiro.Text = string.Empty;
            txtRut.Text = string.Empty;
            txtDireccion.Text = string.Empty;
            txtFonoCentral.Text = string.Empty;
            txtComuna.Text = string.Empty;
        }

        [WebMethod]
        public static string[] GetNombreFantasia(string prefix)
        {
            return GenericoBL.GetAutocompletar(prefix, "cliente", "nombre_fantasia").ToArray();
        }

        [WebMethod]
        public static string[] GetRazonSocial(string prefix)
        {
            return GenericoBL.GetAutocompletar(prefix, "cliente", "razon_social").ToArray();
        }

        [WebMethod]
        public static string[] GetRut(string prefix)
        {
            return GenericoBL.GetAutocompletar(prefix, "cliente", "rut").ToArray();
        }
    }
}
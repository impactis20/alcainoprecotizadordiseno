﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.admin.usuario
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    int ID = Convert.ToInt16(Request.QueryString["id"].ToString());
                    BindDataUsuario(ID, string.Empty, string.Empty, string.Empty, false);
                }
                else
                {
                    BindDataUsuario(0, txtUsername.Text, txtNombre.Text, txtEmail.Text, false);
                }

                //carga listado de roles (agregar usuario)
                BindDataListRolPredeterminado(RolPredeterminadoBL.GetListadoRolPredeterminadoCreado());

                //carga usuarios de baja
                BindDataUsuarioBaja();
            }
        }

        protected void btnBuscar_ServerClick(object sender, EventArgs e)
        {
            BindDataUsuario(0, txtUsername.Text, txtNombre.Text, txtEmail.Text, false);
        }

        //protected void gvUsuario_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //    gvUsuario.EditIndex = e.NewEditIndex;

        //    int ID = Convert.ToInt16(ViewState["BusquedaPorID"].ToString());

        //    if (ID != 0) //consulta si es una búsqueda por url ID o botón Buscar
        //        BindDataUsuario(ID, string.Empty, string.Empty, string.Empty, true);
        //    else
        //        BindDataUsuario(0, txtUsername.Text, txtNombre.Text, txtEmail.Text, true);

        //    //Deshabilita formulario de búsqueda
        //    Util.DeshabilitaDIV(formulario);
        //}

        //protected void gvUsuario_RowUpdating(object sender, GridViewUpdateEventArgs e)
        //{
        //    int IDUsuario = Convert.ToInt16(e.Keys["ID"].ToString());
        //    bool VisualizaTodoContacto = (gvUsuario.Rows[e.RowIndex].FindControl("cbVisualizaTodoContacto") as CheckBox).Checked;

        //    try
        //    {
        //        //actualiza valores
        //        UsuarioBL.SetUsuario(IDUsuario, VisualizaTodoContacto);

        //        //actualiza la tabla con nuevo valores
        //        //gvUsuario.EditIndex = -1;
        //        BindDataUsuario(0, string.Empty, string.Empty, string.Empty, false);

        //        //finalizada la acción habilita nuevamente el formulario de búsqueda
        //        Util.HabilitaDIV(formulario);

        //        ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("success", "Operación satisfactoria", "El registro fue actualizado."), true);
        //    }
        //    catch (Exception ex)
        //    {
        //        ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
        //    }
        //}

        //protected void gvUsuario_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        //{
        //    gvUsuario.EditIndex = -1;

        //    int ID = Convert.ToInt16(ViewState["BusquedaPorID"].ToString());

        //    if (ID != 0)
        //        BindDataUsuario(ID, string.Empty, string.Empty, string.Empty, false);
        //    else
        //        BindDataUsuario(0, txtUsername.Text, txtNombre.Text, txtEmail.Text, false);

        //    //finalizada la acción habilita nuevamente el formulario de búsqueda
        //    Util.HabilitaDIV(formulario);
        //}

        private void BindDataUsuario(int ID, string UserName, string Nombre, string Email, bool EditaRegistro)
        {
            bool? Filtro = null;

            if (!string.IsNullOrEmpty(ddlFiltro.SelectedItem.Value))
                Filtro = Convert.ToBoolean(ddlFiltro.SelectedItem.Value);

            try
            {
                List<Entities.UsuarioEntity> Lst = UsuarioBL.GetUsuario(ID, UserName, Nombre, Email, Filtro, EditaRegistro);
                gvUsuario.DataKeyNames = new string[1] { "ID" }; //PK
                gvUsuario.DataSource = Lst;
                gvUsuario.DataBind();

                if (Lst.Count > 0)
                {
                    gvUsuario.UseAccessibleHeader = true;
                    gvUsuario.HeaderRow.TableSection = TableRowSection.TableHeader;
                    lblBuscar.Text = string.Format("Resultados encontrados: <b>{0}</b>", Lst.Count);
                    lblBuscar.CssClass = "alert-info";

                    if (ID != 0)
                        ViewState["BusquedaPorID"] = ID; //Almacena ID para saber si la búsqueda fue por URL o botón
                    else
                        ViewState["BusquedaPorID"] = 0;
                }
                else
                {
                    lblBuscar.Text = "No se encontraron coincidencias para los parámetros ingresados";
                    lblBuscar.CssClass = "alert-danger";
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop3", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                return;
            }
        }

        protected void gvUsuario_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                UsuarioBL.DelUsuario(Convert.ToInt16(e.Keys["ID"].ToString()));

                BindDataUsuario(0, string.Empty, string.Empty, string.Empty, false);

                BindDataUsuarioBaja();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop7", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }

            ScriptManager.RegisterStartupScript(this, GetType(), "Pop1", Util.GetMensajeAlerta("success", "Operación satisfactoria", "El usuario fue dado de baja.<br><br>Para habilitar al usuario nuevamente vaya a la tabla <b>Usuarios de baja</b> y haga click en <label class='label label-success'>Dar alta</label> según corresponda."), true);
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {

            try
            {
                if (Util.Funcion.EmailValido(hfEmail.Value.Trim()))
                {
                    List<Entities.PaginaEntity> LstPagina = new List<Entities.PaginaEntity>(); //obtiene los módulos
                    List<Entities.PaginaEntity.Control> LstControl = new List<Entities.PaginaEntity.Control>(); //obtiene los controles
                    
                    foreach (GridViewRow r in gvModulo.Rows)
                    {
                        CheckBox cbModulo = (r.Controls[0].FindControl("cbModulo") as CheckBox);

                        if (cbModulo.Checked)
                        {
                            LstPagina.Add(new Entities.PaginaEntity(Convert.ToInt16(gvModulo.DataKeys[r.RowIndex].Value.ToString()))); //Agrega ID de la página (módulo) seleccionado

                            foreach (ListItem cb in (r.Controls[0].FindControl("cblControl") as CheckBoxList).Items)
                                if (cb.Selected)
                                    LstControl.Add(new Entities.PaginaEntity.Control(Convert.ToInt16(cb.Value))); //Agrega a la lista el ID de los controles seleccionados
                        }
                    }

                    int IDRol = Convert.ToInt16(lstRolPredeterminado.SelectedItem.Value);

                    int IDUsuario = UsuarioBL.AddUsuario(hfUserName.Value, hfNombre.Value, hfApellidoP.Value, hfApellidoM.Value, hfEmail.Value, cbVisualizaTodoContacto.Checked, IDRol, LstPagina, LstControl, txtCodigoManager.Text.Trim());

                    if (IDUsuario != 0)

                        ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("success", "Operación satisfactoria", "El usuario ha sido agregado correctamente al sistema."), true);
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", "Usuario ya está registrado o Email es usado por otro usuario."), true);
                    
                }
                else
                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Email inválido...", "El email es inválido, corríjalo en la ficha del usuario en el Active Directory."), true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }

            //Limpia formulario
            txtAddUserName.Text = string.Empty;
            txtAddNombre.Text = string.Empty;
            txtAddPaterno.Text = string.Empty;
            txtAddMaterno.Text = string.Empty;
            txtAddEmail.Text = string.Empty;
            cbVisualizaTodoContacto.Checked = false;
            txtCodigoManager.Text = string.Empty;

            //actualiza gridview con clientes
            ddlFiltro.SelectedItem.Value = string.Empty;
            BindDataUsuario(0, string.Empty, string.Empty, string.Empty, false);
        }

        protected void lstRolPredeterminado_SelectedIndexChanged(object sender, EventArgs e)
        {
            string ValorSeleccionado = lstRolPredeterminado.SelectedItem.Value;

            //if (ValorSeleccionado == "-1")
            //{
            //    CustomValidator1.
            //    descripcionRol.Visible = false;
            //}
            //else
                GetRolPredeterminado(ValorSeleccionado, ref gvModulo, ref txtDescripcionRol);

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "MuestraListBox();", true);
        }

        private void BindDataListRolPredeterminado(List<Entities.RolPredeterminadoEntity> Lst)
        {
            lstRolPredeterminado.DataTextField = "Nombre";
            lstRolPredeterminado.DataValueField = "ID";

            Lst.Add(new Entities.RolPredeterminadoEntity(-1, "Seleccione..."));
            Lst = Lst.OrderBy(x => x.ID).ToList(); // ordena por ID

            lstRolPredeterminado.DataSource = Lst;
            lstRolPredeterminado.DataBind();

            if (Lst.Count > 0)
                GetRolPredeterminado(lstRolPredeterminado.Items[0].Value, ref gvModulo, ref txtDescripcionRol);
        }

        private void GetRolPredeterminado(string IDRolPredeterminado, ref GridView gvModuloUpdate, ref TextBox txtDescripcionRol)
        {
            if (IDRolPredeterminado == "-1")
            {
                descripcionRol.Visible = false;
            }
            else
            {
                descripcionRol.Visible = true;
                Entities.RolPredeterminadoEntity RolPredeterminado = RolPredeterminadoBL.GetRolPredeterminado(Convert.ToInt16(IDRolPredeterminado));
                txtDescripcionRol.Text = RolPredeterminado.Descripcion;

                gvModuloUpdate.DataKeyNames = new string[1] { "ID" }; //PK
                gvModuloUpdate.DataSource = RolPredeterminado.Modulo;
                gvModuloUpdate.DataBind();

                if (RolPredeterminado.Modulo.Count > 0)
                {
                    gvModuloUpdate.UseAccessibleHeader = true;
                    gvModuloUpdate.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
        }

        protected void gvModulo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.PaginaEntity Item = (Entities.PaginaEntity)r.DataItem;

                CheckBox cbModulo = (r.Controls[0].FindControl("cbModulo") as CheckBox);
                cbModulo.Text = string.Format("<b>{0}</b>", Item.Nombre);
                cbModulo.Checked = Item.Seleccionado;

                CheckBoxList cblControl = (r.Controls[0].FindControl("cblControl") as CheckBoxList);
                cblControl.CssClass += " " + cbModulo.ClientID;

                if (Item.PermitidoPorDefecto)
                {
                    cbModulo.Checked = true;
                    cbModulo.Enabled = false;
                }

                if (Item.Controles.Count == 0)
                    (r.Controls[0].FindControl("controles") as HtmlGenericControl).Visible = false;
                else
                {
                    foreach (Entities.PaginaEntity.Control Control in Item.Controles)
                    {
                        ListItem item = new ListItem(Control.Descripcion, Control.ID.ToString());
                        item.Selected = Control.Seleccionado;
                        cblControl.Items.Add(item);
                    }

                    cblControl.DataBind();
                }
            }
        }

        // sin Utilizar!
        [WebMethod]
        public static string[] GetUserNameAD(string prefix)
        {
            return UsuarioBL.GetUserNameAD(prefix).ToArray();
        }

        [WebMethod]
        public static string[] GetUsername(string prefix)
        {
            return GenericoBL.GetAutocompletar(prefix, "usuario", "username").ToArray();
        }

        [WebMethod]
        public static string[] GetNombreUsuario(string prefix)
        {
            return UsuarioBL.GetNombreCompletoUsuarioJSON(prefix).ToArray();
        }

        [WebMethod]
        public static string[] GetEmail(string prefix)
        {
            return GenericoBL.GetAutocompletar(prefix, "usuario", "email").ToArray();
        }

        [WebMethod]
        public static List<Entities.UsuarioEntity> GetUsuarioAD(string username)
        {
            return UsuarioBL.GetUsuarioAD(username);
        }

        protected void gvUsuarioBaja_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                UsuarioBL.SetDarAltaUsuario(Convert.ToInt16(e.Keys["ID"].ToString()));

                BindDataUsuario(0, string.Empty, string.Empty, string.Empty, false);

                BindDataUsuarioBaja();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop71", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }

            ScriptManager.RegisterStartupScript(this, GetType(), "Pop13", Util.GetMensajeAlerta("success", "Operación satisfactoria", "El usuario fue dado de alta."), true);
        }

        private void BindDataUsuarioBaja()
        {
            try
            {
                List<Entities.UsuarioEntity> Lst = UsuarioBL.GetUsuarioBaja();
                gvUsuarioBaja.DataKeyNames = new string[1] { "ID" }; //PK
                gvUsuarioBaja.DataSource = Lst;
                gvUsuarioBaja.DataBind();

                if (Lst.Count > 0)
                {
                    gvUsuarioBaja.UseAccessibleHeader = true;
                    gvUsuarioBaja.HeaderRow.TableSection = TableRowSection.TableHeader;
                    lblBuscarBaja.Text = string.Format("Resultados encontrados: <b>{0}</b>", Lst.Count);
                    lblBuscarBaja.CssClass = "alert-info";
                }
                else
                {
                    lblBuscarBaja.Text = "No hay usuarios de baja.";
                    lblBuscarBaja.CssClass = "alert-danger";
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop5", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
                return;
            }
        }

        protected void gvUsuario_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if(e.CommandName == "EditarUsuario")
            {
                int rowIndex = Convert.ToInt32(e.CommandArgument);
                int IDUsuario = Convert.ToInt16(gvUsuario.DataKeys[rowIndex].Value);
                GridViewRow r = gvUsuario.Rows[rowIndex];

                //Almacena ID Usuario por si genera cambios
                ViewState.Add("IDUsuarioEditar", IDUsuario);

                //obtiene Nombre y si visualiza los contactos
                Entities.UsuarioEntity Usuario = UsuarioBL.GetUsuario(IDUsuario, string.Empty, string.Empty, string.Empty, null, false)[0]; //Busca usuario específico
                lblNombreUsuarioEditar.Text = string.Format("{0} {1} {2} ({3})", Usuario.Nombre, Usuario.ApellidoPaterno, Usuario.ApellidoMaterno, Usuario.UserName);
                cbVisualizaTodoContactoEditar.Checked = Usuario.VisualizaTodoContacto == true ? true : false; //realiza consulta debido a que el tipo de variable acepta null

                //Bindea la lista de roles predeterminados
                lstRolPredeterminadoEditar.DataTextField = "Nombre";
                lstRolPredeterminadoEditar.DataValueField = "ID";
                lstRolPredeterminadoEditar.DataSource = RolPredeterminadoBL.GetListadoRolPredeterminadoCreado();
                lstRolPredeterminadoEditar.DataBind();

                //Obtiene rol, páginas y controles actuales del usuario
                Entities.RolPredeterminadoEntity RolUsuario = UsuarioBL.GetRolUsuario(IDUsuario);
                txtDescripcionRolEditar.Text = RolUsuario.Descripcion;

                gvModuloEditar.DataKeyNames = new string[1] { "ID" }; //PK
                gvModuloEditar.DataSource = RolUsuario.Modulo;
                gvModuloEditar.DataBind();

                if (RolUsuario.Modulo.Count > 0)
                {
                    gvModuloEditar.UseAccessibleHeader = true;
                    gvModuloEditar.HeaderRow.TableSection = TableRowSection.TableHeader;
                }

                //Selecciona Rol del usuario
                lstRolPredeterminadoEditar.SelectedValue = RolUsuario.ID.ToString();

                ScriptManager.RegisterStartupScript(this, GetType(), "PopEdita", "ModalEditarUsuario();", true);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            List<Entities.PaginaEntity> LstPagina = new List<Entities.PaginaEntity>();
            List<Entities.PaginaEntity.Control> LstControl = new List<Entities.PaginaEntity.Control>();

            //obtienes las páginas (módulos) y controles seleccionados
            foreach (GridViewRow r in gvModuloEditar.Rows)
            {
                CheckBox cbModulo = (r.Controls[0].FindControl("cbModulo") as CheckBox);

                if (cbModulo.Checked)
                {
                    LstPagina.Add(new Entities.PaginaEntity(Convert.ToInt16(gvModuloEditar.DataKeys[r.RowIndex].Value.ToString()))); //Agrega ID de la página (módulo) seleccionado

                    foreach (ListItem cb in (r.Controls[0].FindControl("cblControl") as CheckBoxList).Items)
                        if (cb.Selected)
                            LstControl.Add(new Entities.PaginaEntity.Control(Convert.ToInt16(cb.Value))); //Agrega a la lista el ID de los controles seleccionados
                }
            }

            int IDUsuario = Convert.ToInt16(ViewState["IDUsuarioEditar"]);
            int IDRol = Convert.ToInt16(lstRolPredeterminadoEditar.SelectedValue);

            //actualiza usuario
            UsuarioBL.SetUsuario(IDUsuario, IDRol, cbVisualizaTodoContactoEditar.Checked, LstPagina, LstControl);

            //actualiza tabla de usuarios
            BindDataUsuario(0, string.Empty, string.Empty, string.Empty, false);
        }

        protected void lstRolPredeterminadoEditar_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetRolPredeterminado(lstRolPredeterminadoEditar.SelectedItem.Value, ref gvModuloEditar, ref txtDescripcionRolEditar);

            ScriptManager.RegisterStartupScript(this, GetType(), "editaRolUsuario", "MuestraListBox();", true);
        }

        protected void gvUsuarioBaja_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
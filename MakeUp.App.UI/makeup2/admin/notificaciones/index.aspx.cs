﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.admin.notificaciones
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                BindGridViewNotificacion();
        }

        private void BindGridViewNotificacion()
        {
            List<Entities.NotificacionEntity> Lst = NotificacionBL.GetNotificacion();
            gvNotificacion.DataKeyNames = new string[1] { "ID" };
            gvNotificacion.DataSource = Lst;
            gvNotificacion.DataBind();

            if (Lst.Count > 0)
            {
                lblContadorNotificacion.Text = string.Format("Hay <strong>{0} notificaciones</strong> para configurar en el sistema.", Lst.Count);
                lblContadorNotificacion.CssClass = "alert-info";
            }
            else
            {
                lblContadorNotificacion.Text = "No hay notificaciones en el sistema.";
                lblContadorNotificacion.CssClass = "alert-danger";
            }
        }

        protected void gvNotificacion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.NotificacionEntity Notificacion = (Entities.NotificacionEntity)r.DataItem;

                HtmlGenericControl nombreNotificacion = r.Controls[0].FindControl("nombreNotificacion") as HtmlGenericControl;
                ListBox lstRol = r.Controls[0].FindControl("lstRol") as ListBox;

                nombreNotificacion.InnerHtml = Notificacion.Nombre;
                foreach (Entities.RolPredeterminadoEntity Rol in Notificacion.RolNotificado)
                {
                    ListItem Item = new ListItem(Rol.Nombre, Rol.ID.ToString(), true);
                    Item.Selected = Rol.VinculadoANotificacion;
                    lstRol.Items.Add(Item);
                }

                lstRol.DataBind();

                //Obtiene controles Label contador y GridView que muestra los roles asociados a la notificación
                Label lblContadorRol = r.Controls[0].FindControl("lblContadorRol") as Label;
                GridView gvRolEnNotificacion = r.Controls[0].FindControl("gvRolEnNotificacion") as GridView;

                //Actualiza Gridview de los roles que están asociados a la notificación
                BindGridViewRolEnNotificacion(ref gvRolEnNotificacion, ref lblContadorRol, Notificacion.RolNotificado.Where(x => x.VinculadoANotificacion == true).ToList());
            }
        }

        private void BindGridViewRolEnNotificacion(ref GridView gvRolEnNotificacion, ref Label lblContadorRol, List<Entities.RolPredeterminadoEntity> LstData)
        {
            gvRolEnNotificacion.DataKeyNames = new string[1] { "ID" };
            gvRolEnNotificacion.DataSource = LstData;
            gvRolEnNotificacion.DataBind();

            if (LstData.Count > 0)
            {
                gvRolEnNotificacion.UseAccessibleHeader = true;
                gvRolEnNotificacion.HeaderRow.TableSection = TableRowSection.TableHeader;
                lblContadorRol.Text = string.Format("Hay <strong>{0} roles</strong> añadidos a esta notificación.", LstData.Count);
                lblContadorRol.CssClass = "alert-info";
            }
            else
            {
                lblContadorRol.Text = "No hay roles añadidos a esta notificación.";
                lblContadorRol.CssClass = "alert-danger";
            }
        }

        protected void lstRol_SelectedIndexChanged(object sender, EventArgs e)
        {
            //((ListBox)sender).Parent.FindControl("gvRolEnNotificacion").ClientID
            int FilaEvento = (((ListBox)sender).DataItemContainer as GridViewRow).DataItemIndex;
            int IDNotificacion = Convert.ToInt16(gvNotificacion.DataKeys[FilaEvento].Value);
            
            //Actualiza la lista de notificaciones
            //List<Entities.NotificacionEntity> LstNotificaciones = NotificacionBL.GetNotificacion();

            List<Entities.RolPredeterminadoEntity> LstRolSeleccionado = new List<Entities.RolPredeterminadoEntity>();
            foreach(ListItem Item in ((ListBox)sender).Items)
                if (Item.Selected)
                {
                    int IDRolSeleccionado = Convert.ToInt16(Item.Value);
                    Entities.RolPredeterminadoEntity Rol = new Entities.RolPredeterminadoEntity(IDRolSeleccionado, Item.Text);
                    bool FiltraRol = false;
                    List<Entities.FiltroNotificacionEntity> LstFiltro = NotificacionBL.GetFiltroRolNotificacion(IDNotificacion, Rol.ID, ref FiltraRol);

                    if (FiltraRol)
                        Rol.LstFiltroNotificacion = LstFiltro;

                    //Agrega a la lista el rol seleccionado, además conlleva si el rol se puede filtrar
                    LstRolSeleccionado.Add(Rol);
                }

            //Actualiza los roles a notificar según selección
            NotificacionBL.SetNotificarRol(IDNotificacion, LstRolSeleccionado);


            //Busca Label contador y GridView que muestra los roles asociados a la notificación
            GridView gvRolEnNotificacion = ((ListBox)sender).Parent.FindControl("gvRolEnNotificacion") as GridView;
            Label lblContadorRol = ((ListBox)sender).Parent.FindControl("lblContadorRol") as Label;

            //Actualiza Gridview de los roles que están asociados a la notificación
            BindGridViewRolEnNotificacion(ref gvRolEnNotificacion, ref lblContadorRol, LstRolSeleccionado);

            ScriptManager.RegisterClientScriptBlock(this, GetType(), "anything1", string.Format("mantieneAbiertoFiltro({0});", ((ListBox)sender).ClientID), true);
        }

        protected void gvRolEnNotificacion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;

            if (r.RowType == DataControlRowType.DataRow)
            {
                int FilaNotificacion = ((GridViewRow)r.Parent.Parent.Parent.Parent.Parent.Parent).RowIndex;
                int IDNotificacion = Convert.ToInt16(gvNotificacion.DataKeys[FilaNotificacion].Value);
                Entities.RolPredeterminadoEntity Rol = (Entities.RolPredeterminadoEntity)r.DataItem;

                //controles
                Label lblRol = r.Controls[0].FindControl("lblRol") as Label;
                ListBox lstFiltro = r.Controls[0].FindControl("lstFiltro") as ListBox;

                lblRol.Text = Rol.Nombre;

                bool FiltraRol = false;
                List<Entities.FiltroNotificacionEntity> LstFiltro = NotificacionBL.GetFiltroRolNotificacion(IDNotificacion, Rol.ID, ref FiltraRol);

                if (FiltraRol)
                {
                    foreach(Entities.FiltroNotificacionEntity Filtro in LstFiltro)
                    {
                        ListItem Item = new ListItem(Filtro.Nombre, Filtro.ID.ToString());
                        Item.Selected = Filtro.FiltroSeleccionado;
                        lstFiltro.Items.Add(Item);
                    }

                    //Cuando se agrega o quita roles en una notificación, éste se hace mediante updatepanel. La siguiente línea hace reaparecer el listbox de filtro
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), Guid.NewGuid().ToString(), string.Format("$('#{0}').selectpicker('toggle');", lstFiltro.ClientID), true);

                    //Habilita columna filtro
                    (r.BindingContainer as GridView).Columns[1].Visible = true;
                }
                else
                {
                    lstFiltro.Visible = false;
                    //(r.BindingContainer as GridView).Columns[1].Visible = false;
                }

            }
        }

        protected void lstFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox lstFiltro = ((ListBox)sender);
            int IDFiltroSeleccionado = Convert.ToInt16(lstFiltro.SelectedItem.Value);

            int RolFilaEvento = (lstFiltro.DataItemContainer as GridViewRow).DataItemIndex; //Posición de la fila del rol
            int IDRol = Convert.ToInt16((lstFiltro.DataItemContainer.Parent.Parent as GridView).DataKeys[RolFilaEvento].Value); //ID del rol

            int NotificacionFilaEvento = ((lstFiltro.DataItemContainer.Parent.Parent as GridView).Parent.Parent.Parent.Parent as GridViewRow).DataItemIndex; //Posición de la fila de la notificación
            int IDNotificacion = Convert.ToInt16((((lstFiltro.DataItemContainer.Parent.Parent as GridView).Parent.Parent.Parent.Parent as GridViewRow).BindingContainer as GridView).DataKeys[NotificacionFilaEvento].Value); //ID de la notificacion

            //actualiza
            NotificacionBL.SetFiltroRolNotificacion(IDRol, IDNotificacion, IDFiltroSeleccionado);


            HtmlGenericControl divSuccess = lstFiltro.Parent.FindControl("success") as HtmlGenericControl;

            string scriptExito = "$(document).ready(function(){" +
                "$('#" + divSuccess.ClientID + "').alert();" +
                "$('#" + divSuccess.ClientID + "').fadeTo(2000, 500).slideUp(500, function(){" +
                     "$('#" + divSuccess.ClientID + "').slideUp(500);" +
                 "});" +
             "});";

            //Despliega mensaje
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "desplp", scriptExito, true);

            //Hace reaparecer el filtro listbox
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "anything3", string.Format("$('#{0}').selectpicker('render');", lstFiltro.ClientID), true);
        }
    }
}
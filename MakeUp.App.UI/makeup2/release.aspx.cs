﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MakeUp.BL;

namespace MakeUp.App.UI.makeup2
{
    public partial class release : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindGridView();
            //string pageName = this.contenido.Page.GetType().FullName;

        }

        private void BindGridView()
        {

            Entities.ReleaseNoteEntity respWS = new Entities.ReleaseNoteEntity();
            //gvRelease.DataKeyNames = new string[1] { "rut" };
            gvRelease.DataSource = ReleaseNoteBL.GetRelease();
            gvRelease.DataBind();



            if (gvRelease.Rows.Count > 0)
            {
                gvRelease.UseAccessibleHeader = true;
                gvRelease.HeaderRow.TableSection = TableRowSection.TableHeader;
                gvRelease.Visible = true;
            }
        }
            //protected void gvRelease_RowDataBound(object sender, GridViewRowEventArgs e)
            //{
            //    GridViewRow r = e.Row;

            //    if (r.RowType == DataControlRowType.DataRow)
            //    {
            //        Entities.ReleaseNoteEntity Item = (Entities.ReleaseNoteEntity)r.DataItem;

            //        HtmlGenericControl fechaR = (r.Controls[0].FindControl("fecha") as HtmlGenericControl);
            //        Label lblVersion = (r.Controls[0].FindControl("lblVersion") as Label);

            //        fechaR.InnerText = Item.FECHA;
            //        lblVersion.Text = Item.DESCRIPCION;
            //    }
            //}
        }
}
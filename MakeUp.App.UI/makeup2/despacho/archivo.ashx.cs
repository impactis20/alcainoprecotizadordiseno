﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Web.UI;


namespace MakeUp.App.UI.makeup2.despacho
{
    /// <summary>
    /// Descripción breve de archivo
    /// </summary>
    public class archivo : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string ServidorArchivosProyecto = ConfigurationManager.AppSettings["ftp"].ToString();

                string UsuarioCarpetaCompartidaServidorArchivosProyecto = ConfigurationManager.AppSettings["usuarioftp"].ToString();
                string PasswordCarpetaCompartidaServidorArchivosProyecto = ConfigurationManager.AppSettings["passftp"].ToString();

                using (App_Code.Seguridad.NetworkShareAccesser.Acceso(ServidorArchivosProyecto, UsuarioCarpetaCompartidaServidorArchivosProyecto, PasswordCarpetaCompartidaServidorArchivosProyecto))
                {
                    //En este controlador tenemos que recibir la ruta del pdf... cómo? recibiéndola por parámetro de url
                    if (context.Request.QueryString["id"] != null) //verifica si trae el parámetro "id"
                    {

                        context.Response.ContentType = "application/pdf";
                        // Get the object used to communicate with the server.
                        String ruta = Util.Funcion.Desencriptar(context.Request.QueryString["id"]);

                        WebClient client = new WebClient();
                        client.Credentials = new NetworkCredential(UsuarioCarpetaCompartidaServidorArchivosProyecto, PasswordCarpetaCompartidaServidorArchivosProyecto);
                        Byte[] buffer = client.DownloadData(ruta);
                        context.Response.AddHeader("content-disposition", string.Format("inline;filename={0}", Path.GetFileName(ruta)));
                        context.Response.AddHeader("content-length", buffer.Length.ToString());
                        context.Response.BinaryWrite(buffer);
                        context.Response.Flush();
                        context.Response.Close();
                        context.Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                context.Response.Write("El archivo PDF no existe. " + ex.Message);
            }
        
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
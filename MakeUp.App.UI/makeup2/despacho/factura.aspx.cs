﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Services;
using System.Web.UI.WebControls;
using System.Net;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.despacho
{
    public partial class factura : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //carga filtro Estado Despacho de Factura
                List<Entities.EstadoDespachoFacturaEntity> Lst = FacturacionBL.GetEstadoDespachoFactura();

                lstEstadoDespachoFactura.DataTextField = "Descripcion";
                lstEstadoDespachoFactura.DataValueField = "ID";

                lstEstadoDespachoFactura.DataSource = Lst;
                lstEstadoDespachoFactura.DataBind();

                //por defecto deja todos los estados seleccionados
                foreach (ListItem item in lstEstadoDespachoFactura.Items)
                    item.Selected = true;
               

                BindDataGrid(0, string.Empty, string.Empty);
            }
        }

        private void BindDataGrid(int IDProyecto, string NombreProyecto, string NombreFantasia)
        {
            List<Entities.EstadoDespachoFacturaEntity> LstFiltroEstadoDespachoFactura = new List<Entities.EstadoDespachoFacturaEntity>();

            foreach (ListItem Item in lstEstadoDespachoFactura.Items)
                if (Item.Selected)
                    LstFiltroEstadoDespachoFactura.Add(new Entities.EstadoDespachoFacturaEntity(Convert.ToInt16(Item.Value)));

            List<Entities.FacturaEntity> Lst = FacturacionBL.GetFactura(IDProyecto, NombreProyecto, NombreFantasia, LstFiltroEstadoDespachoFactura);
            gvFactura.DataSource = Lst;
            gvFactura.DataBind();

            if (Lst.Count > 0)
            {
                gvFactura.UseAccessibleHeader = true;
                gvFactura.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            int IDProyecto = string.IsNullOrEmpty(txtID.Text.Trim()) ? 0 : Convert.ToInt32(txtID.Text.Trim());
            string NombreFantasia = txtCliente.Text.Trim();
            string NombreProyecto = txtNombre.Text.Trim();

            BindDataGrid(IDProyecto, NombreProyecto, NombreFantasia);
        }

        protected void btnGuardar_ServerClick(object sender, EventArgs e)
        {
            string IDProyecto = hfProy.Value.Trim();
            string Factura = txtNroFact.Text.Trim();
            decimal Monto = Convert.ToDecimal(hfmontofact.Value.Trim());
            string tipoValor = hftipovalor.Value.Trim();

            FacturacionBL.SetEditarFactura(IDProyecto, Factura, Monto, tipoValor);
            ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("Confirmacion", "Edición Nro Factura...", "Edición Exitosa"), true);
            BindDataGrid(0, string.Empty, string.Empty);

        }

        protected void gvFactura_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow r = e.Row;
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();

            if (r.RowType == DataControlRowType.DataRow)
            {
                Entities.FacturaEntity Item = (Entities.FacturaEntity)r.DataItem;
                Label lblMontoFactura = (r.Controls[0].FindControl("lblMontoFactura") as Label);
                Label lblColor = (r.Controls[0].FindControl("lblColor") as Label);
                HyperLink lnkNroFactura = (r.Controls[0].FindControl("lnkNroFactura") as HyperLink);
                Label lblNroFactura = (r.Controls[0].FindControl("lblNroFactura") as Label);
                HyperLink urlDoc = (r.Controls[0].FindControl("doc") as HyperLink);
                TextBox txtNroFact = (r.Controls[0].FindControl("lblNroFactura") as TextBox);
                urlDoc.NavigateUrl = string.Format("archivo.ashx?id={0}",  Util.Funcion.Encriptar(Item.UrlDoc)); //asigna url del documento encriptada
                HyperLink editarFact = (r.Controls[0].FindControl("editar") as HyperLink);
                editarFact.Enabled = PaginaBL.GetHabilitarControlPagina(29, IDUsuario); 

                lblColor.Attributes.Add("style", "margin-left: 4px;margin-right: 4px;");
                lblColor.Attributes.Add("rel", "tooltip");
                lblColor.Attributes.Add("data-placement", "top");
                lblColor.Attributes.Add("data-toggle", "tooltip");

                switch (Item.IDEstadoFactura)
                {
                    case 1:
                        //lblColor.Attributes.Add("style", "background-color:#d9534f !important"); // rojo
                        lblColor.CssClass = "fa fa-clock-o fa-2x text-danger";
                        lblColor.Attributes.Add("title", "Factura pendiente de despacho");
                        break;
                    case 2:
                        //lblColor.Attributes.Add("style", "background-color:#5cb85c !important"); // verde
                        lblColor.CssClass = "fa fa-check fa-2x text-success";
                        lblColor.Attributes.Add("title", "Factura despachada");
                        //lblColor.Attributes.Add("style", "background-color:#337ab7 !important"); // azul
                        break;
                }

                //verifica el tipo de valor del proyecto. Si es USD (valor 3) o Euro (valor 4)
                //En caso que sea USD o Euro, el valor de la factura se debe mostrar en tipo de moneda según corresponda
                if (Item.Proyecto.TipoValor.ID == 1 || Item.Proyecto.TipoValor.ID == 2 || Item.Proyecto.TipoValor.ID == 3)
                {
                    //cual el nro de factura es nulo el valor es -1
                    if (Item.NroFactura != -1)
                    {
                        //Enlace se habilita cuando la factura está enviada al SII
                        if (Item.EnviadoSII)
                        {
                            lnkNroFactura.Text = Item.NroFactura.ToString();
                            lnkNroFactura.NavigateUrl = string.Format("detalle.aspx?factura={0}", Item.NroFactura);
                            lnkNroFactura.Visible = true;
                            urlDoc.Visible = true;
                        }
                        else
                        {
                            lblNroFactura.Text = Item.NroFactura.ToString();
                            lblNroFactura.Visible = true;
                            urlDoc.Visible = false;
                        }
                    }
                    else
                    {
                        lblNroFactura.Text = "-";
                        lblNroFactura.Visible = true;
                    }

                    lblMontoFactura.Text = string.Format("CLP {0}", Util.Funcion.FormatoNumericoEntero(Item.Monto));
                }
                else if (Item.Proyecto.TipoValor.ID == 4)
                {
                    if (Item.NroFactura == -1) {
                        lblNroFactura.Text = "0";
                        lblNroFactura.Visible = true;
                        lblMontoFactura.Text = string.Format("{0} {1}", Item.Proyecto.TipoValor.Sigla, Item.Monto);

                    }
                    else {

                        lblNroFactura.Text = Item.NroFactura.ToString();
                        lblMontoFactura.Text = string.Format("{0} {1}", Item.Proyecto.TipoValor.Sigla, Item.Monto);
                        lblNroFactura.Visible = true;
                        urlDoc.Visible = true;
                     }
                }
            }

                //lblMontoFactura.Text = string.Format("CLP {0}", Util.Funcion.FormatoNumericoEntero(Item.Monto));
        }

        [WebMethod]
        public static string[] GetNombreFantasia(string prefix)
        {
            return GenericoBL.GetAutocompletar(prefix, "cliente", "nombre_fantasia").ToArray();
        }

        [WebMethod]
        public static string[] GetNombreProyecto(string prefix)
        {
            return GenericoBL.GetAutocompletar(prefix, "proyecto", "nombre").ToArray();
        }

        //[WebMethod]
        //public static string[] SetEditarFactura(string proyecto, string factura, decimal monto)
        //{
        //    return FacturacionBL.SetEditarFactura(proyecto, factura,monto);
            
        //}
    }
}
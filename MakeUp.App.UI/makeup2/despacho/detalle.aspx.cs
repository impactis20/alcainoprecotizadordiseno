﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.SignalR;

using MakeUp.BL;

namespace MakeUp.App.UI.makeup2.despacho
{
    public partial class detalle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                CargaInicial();
            }
        }

        private void BindTipoEnvio()
        {
            //carga filtro Estado Despacho de Factura
            List<Entities.TipoEnvioFacturaEntity> Lst = FacturacionBL.GetTipoEnvioFactura();

            Lst.Add(new Entities.TipoEnvioFacturaEntity(-1, "Seleccione..."));
            Lst = Lst.OrderBy(x => x.ID).ToList(); // ordena por ID

            lstMetodoEnvio.DataTextField = "Nombre";
            lstMetodoEnvio.DataValueField = "ID";

            lstMetodoEnvio.DataSource = Lst;
            lstMetodoEnvio.DataBind();
        }

        private void CargaInicial()
        {
            BindTipoEnvio();
            
            int NroFactura = Convert.ToInt32(Request.QueryString["factura"].ToString());

            Entities.FacturaEntity Factura = FacturacionBL.GetDatosDespachoFactura(NroFactura);

            Page.Title = string.Format("{0} {1}", Page.Title, NroFactura);
            titulo.InnerHtml = string.Format("{0} <var>{1}</var>", titulo.InnerText, NroFactura);

            if (Factura.TipoEnvio != null) // si no es nulo, entonces la factura además trae Entregado por y Fecha de envío
            {
                lstMetodoEnvio.Items.RemoveAt(0);
                lstMetodoEnvio.SelectedValue = Factura.TipoEnvio.ID.ToString();

                txtEntregadoPor.Text = Factura.EntregadoPor;
                txtFechaEnvio.Text = Factura.FechaEnvio.ToShortDateString();

                txtEntregadoPor.Enabled = false;
                txtFechaEnvio.Enabled = false;
                lstMetodoEnvio.Attributes.Add("disabled", "disabled");
                footerEnvio.Visible = false;




                if (Factura.RutReceptor != 0)
                    txtRutRecepcion.Text = string.Format("{0}-{1}", Factura.RutReceptor, Factura.RutReceptorDV);

                if (Factura.RecibidoPor != null)
                {
                    txtRecibidoPor.Text = Factura.RecibidoPor;
                    txtFechaRecepcion.Text = Factura.FechaRecepcion.ToShortDateString();

                    txtRecibidoPor.Enabled = false;
                    txtFechaRecepcion.Enabled = false;
                    txtRutRecepcion.Enabled = false;
                    footerRecepcion.Visible = false;
                }
                else
                {
                    //habilita formulario para guardar datos de recepción de factura
                    txtRutRecepcion.Enabled = true;
                    txtRecibidoPor.Enabled = true;
                    txtFechaRecepcion.Enabled = true;
                    btnGuardarRecepcion.Enabled = true;
                }
            }
        }

        protected void btnGuardarEnvio_Click(object sender, EventArgs e)
        {
            try
            {
                int IDFactura = Convert.ToInt32(Request.QueryString["factura"].ToString());
                int IDTipoEnvio = Convert.ToInt16(lstMetodoEnvio.SelectedItem.Value);
                string EntregadoPor = txtEntregadoPor.Text.Trim();
                DateTime FechaEnvio = Convert.ToDateTime(txtFechaEnvio.Text.Trim());

                FacturacionBL.SetDespachoEnvioFactura(IDFactura, IDTipoEnvio, EntregadoPor, FechaEnvio);

                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("success", "Operación satisfactoria", "Los datos de envío se han actualizado correctamente."), true);

                txtEntregadoPor.Enabled = false;
                txtFechaEnvio.Enabled = false;
                lstMetodoEnvio.Attributes.Add("disabled", "disabled");
                footerEnvio.Visible = false;

                //habilita formulario para guardar datos de recepción de factura
                txtRutRecepcion.Enabled = true;
                txtRecibidoPor.Enabled = true;
                txtFechaRecepcion.Enabled = true;
                btnGuardarRecepcion.Enabled = true;
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }

        protected void btnGuardarRecepcion_Click(object sender, EventArgs e)
        {
            try
            {
                int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario();
                int NroFactura = Convert.ToInt32(Request.QueryString["factura"].ToString());
                string RutRecepcion = txtRutRecepcion.Text.Trim();
                string RecibidoPor = txtRecibidoPor.Text.Trim();
                DateTime FechaRecepcion = Convert.ToDateTime(txtFechaRecepcion.Text.Trim());
                int IDNotificacionApp = 0;

                if (string.IsNullOrEmpty(RutRecepcion))
                {
                    FacturacionBL.SetDespachoRecepcionFactura(NroFactura, IDUsuario, 0, string.Empty, RecibidoPor, FechaRecepcion, ref IDNotificacionApp);

                    //NOTIFICACIÓN(13): Notifica a los usuarios que la factura está recepcionada
                    hubs.NotificacionAppHub.EnviaNotificacionesUsuariosConectados(GlobalHost.ConnectionManager.GetHubContext<hubs.NotificacionAppHub>());

                    ScriptManager.RegisterStartupScript(this, GetType(), "Pop34", Util.GetMensajeAlerta("success", "Operación satisfactoria", "Los datos de recepción se han actualizado correctamente."), true);

                    txtRecibidoPor.Enabled = false;
                    txtFechaRecepcion.Enabled = false;
                    txtRutRecepcion.Enabled = false;
                    footerRecepcion.Visible = false;
                }
                else
                {
                    if (Util.Funcion.RutValido(RutRecepcion))
                    {
                        string RutConFormato = Util.Funcion.FormatoRut(RutRecepcion);
                        int RutSinDv = Convert.ToInt32(RutConFormato.Split('-')[0]);
                        string DV = RutConFormato.Split('-')[1];

                        lblRutRecepcion.Visible = false;

                        FacturacionBL.SetDespachoRecepcionFactura(NroFactura, IDUsuario, RutSinDv, DV, RecibidoPor, FechaRecepcion, ref IDNotificacionApp);

                        //NOTIFICACIÓN(13): Notifica a los usuarios que la factura está recepcionada
                        hubs.NotificacionAppHub.EnviaNotificacionesUsuariosConectados(GlobalHost.ConnectionManager.GetHubContext<hubs.NotificacionAppHub>());

                        ScriptManager.RegisterStartupScript(this, GetType(), "Pop09", Util.GetMensajeAlerta("success", "Operación satisfactoria", "Los datos de recepción se han actualizado correctamente."), true);

                        txtRecibidoPor.Enabled = false;
                        txtFechaRecepcion.Enabled = false;
                        txtRutRecepcion.Enabled = false;
                        footerRecepcion.Visible = false;
                    }
                    else
                    {
                        lblRutRecepcion.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Pop97", Util.GetMensajeAlerta("error", "Error...", Util.GetError(ex.Message)), true);
            }
        }
    }
}
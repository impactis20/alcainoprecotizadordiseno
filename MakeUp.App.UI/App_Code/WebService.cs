﻿using System;
using System.Collections.Generic;
using System.Web;

namespace MakeUp.App.UI.App_Code
{
    public class WebService
    {
        public class Email
        {
            public static string EnviarEmail(string CorreoGenerico, string PasswordCorreoGenerico, string De, string Para, string Copia, string CCO, string Remitente, string Asunto, string Cuerpo)
            {
                string Respuesta = string.Empty;
                ws_email.enviarSoapClient client = new ws_email.enviarSoapClient();
                client.Open();
                Respuesta = client.EnviarCorreo(CorreoGenerico, PasswordCorreoGenerico, De, Para, Copia, CCO, Remitente, Asunto, Cuerpo);
                client.Close();

                return Respuesta;
                //return "1.Enviado exitosamente";
            }
        }
    }
}
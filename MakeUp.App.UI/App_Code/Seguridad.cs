﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using MakeUp.BL;

namespace MakeUp.App.UI.App_Code
{
    public class Seguridad : IHttpModule
    {
        //public Seguridad() { }

        private HttpApplication _app;

        public void Init(HttpApplication oHttpApp)
        {
            _app = oHttpApp;
            _app.AuthorizeRequest += new EventHandler(AuthorizeRequest);
            _app.AuthenticateRequest += new EventHandler(AuthenticateRequest);
        }

        public void Dispose() { }

        /// <summary>
        /// Valida que el request tenga autenticación. Además, verifica que el usuario esté de alta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AuthenticateRequest(object sender, EventArgs e)
        {
            if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated && HttpContext.Current.User.Identity is FormsIdentity)
            {
                bool UsuarioValido = UsuarioBL.GetUsuarioValido(GetCurrentIDUSuario());

                if (!UsuarioValido)
                {
                    FormsAuthentication.SignOut();
                    HttpContext.Current.Server.Transfer("~/login.aspx");
                }
            }
        }

        /// <summary>
        /// Verifica si el usuario tiene acceso a la página.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AuthorizeRequest(object sender, EventArgs e)
        {
            if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated && HttpContext.Current.User.Identity is FormsIdentity)
            {
                var request = _app.Context.Request;
                string target = request.Form["__EVENTTARGET"]; //obtiene botón que genera evento

                //cuando presione el btn salir (logout), independiente si tiene o no permisos de acceso en la página actual
                //el sistema dejará al usuario desloguearse.
                if (target != null && target.Contains("lnkSalir")) 
                    return;

                //obtiene pagina actual y procede a verificar si tiene permisos de acceso
                string Pagina = HttpContext.Current.Request.FilePath;

                if (Pagina.Contains(".aspx"))
                {
                    int IDUsuario = GetCurrentIDUSuario();
                    bool Resp = UsuarioBL.GetAccesoPagina(IDUsuario, Pagina);

                    if (!Resp && Pagina != "/no-autorizado.aspx")
                        if (Pagina == "/login.aspx")
                            HttpContext.Current.Response.Redirect("~/makeup2/index.aspx");
                        else
                            HttpContext.Current.Server.Transfer("~/no-autorizado.aspx");
                }
            }
        }

        public static int GetCurrentIDUSuario()
        {
            FormsIdentity _identity = (FormsIdentity)HttpContext.Current.User.Identity;
            FormsAuthenticationTicket ticket = _identity.Ticket;

            return Convert.ToInt16(ticket.UserData);
        }
        

        #region Valida a qué controles tiene acceso el usuario
        //verifica a qué controles de la página el usuario tiene acceso
        //no incluye controles o columnas que están dentro de un GridView
        public static void GetControlAutorizado (Page page, string Path)
        {
            List<Entities.PaginaEntity.Control> Lst = UsuarioBL.GetControlAutorizado(GetCurrentIDUSuario(), Path);
            foreach (Entities.PaginaEntity.Control Control in Lst)
                GetControlPage(page, Control.ControlID).Visible = true;
        }

        //verifica a qué controles dentro de un Gridview el usuario tiene acceso
        //verifica además si el gridview se trata de N columnas o sólo de una columna con template diseñado (N controles)
        public static void GetControlGridviewAutorizado(Page page, string Path, string IDGridview)
        {
            GridView GridView = (GridView)GetControlPage(page, IDGridview); //Crea gridview
            
            //Evalua si tiene N columnas
            if (GridView.Columns.Count > 1)
            {
                List<Entities.PaginaEntity.Control> Lst = UsuarioBL.GetControlGridviewAutorizado(GetCurrentIDUSuario(), Path, IDGridview, 1); //1 -> obtiene columnas permitidas de un gridview
                foreach (Entities.PaginaEntity.Control Control in Lst)
                {
                    string ColumnasGridView = Control.ControlID.Split(':')[0]; //Columnas a mostrar separadas por comas ","
                    string[] Separador = { "," };
                    string[] Columna = ColumnasGridView.Split(Separador, StringSplitOptions.RemoveEmptyEntries); //Obtiene el nro de la(s) columna(s)
                    for (int i = 0; i < Columna.Length; i++)
                    {
                        short Posicion = Convert.ToInt16(Columna[i]);
                        GridView.Columns[Posicion].Visible = true;
                    }
                }
            }
            else
            {
                List<Entities.PaginaEntity.Control> Lst = UsuarioBL.GetControlGridviewAutorizado(GetCurrentIDUSuario(), Path, IDGridview, 2); //2 -> obtiene controles permitidos dentro de un gridview

                foreach (Entities.PaginaEntity.Control Control in Lst)
                {
                    string IDControls = Control.ControlID.Split(':')[0];
                    string[] Separador = { "," };
                    string[] Controles = IDControls.Split(Separador, StringSplitOptions.RemoveEmptyEntries); //Obtiene el nro de la(s) columna(s)
                    foreach (GridViewRow r in GridView.Rows)
                    {
                        for (int i = 0; i < Controles.Length; i++)
                        {
                            Control control = r.FindControl(Controles[i]);
                            if (control != null)
                                control.Visible = true;
                        }
                    }
                }
            }
        }
        #endregion

        //GetControlPage se encuentra también en "Util.cs". Se replica debido a warning
        private static Control GetControlPage(Control ctrl, string controlID)
        {
            if (string.Compare(ctrl.ID, controlID, true) == 0)
            {
                // We found the control!
                return ctrl;
            }
            else
            {
                // Recurse through ctrl's Controls collections
                foreach (Control child in ctrl.Controls)
                {
                    Control lookFor = GetControlPage(child, controlID);

                    if (lookFor != null)
                        return lookFor;  // We found the control
                }

                // If we reach here, control was not found
                return null;
            }
        }

        /// <summary>
        /// Proporciona acceso a un recurso compartido de red. Usar comando como using (NetworkShareAccesser.Accesso(NombreEquipo, Dominio, UsuarioEquipoRemoto, PasswordUsuarioEquipoRemoto)) { }
        /// </summary>
        public class NetworkShareAccesser : IDisposable
        {
            private string _remoteUncName;
            private string _remoteComputerName;

            public string RemoteComputerName
            {
                get
                {
                    return this._remoteComputerName;
                }
                set
                {
                    this._remoteComputerName = value;
                    this._remoteUncName = @"\\" + this._remoteComputerName;
                }
            }

            public string UserName
            {
                get;
                set;
            }
            public string Password
            {
                get;
                set;
            }

            #region Consts

            private const int RESOURCE_CONNECTED = 0x00000001;
            private const int RESOURCE_GLOBALNET = 0x00000002;
            private const int RESOURCE_REMEMBERED = 0x00000003;

            private const int RESOURCETYPE_ANY = 0x00000000;
            private const int RESOURCETYPE_DISK = 0x00000001;
            private const int RESOURCETYPE_PRINT = 0x00000002;

            private const int RESOURCEDISPLAYTYPE_GENERIC = 0x00000000;
            private const int RESOURCEDISPLAYTYPE_DOMAIN = 0x00000001;
            private const int RESOURCEDISPLAYTYPE_SERVER = 0x00000002;
            private const int RESOURCEDISPLAYTYPE_SHARE = 0x00000003;
            private const int RESOURCEDISPLAYTYPE_FILE = 0x00000004;
            private const int RESOURCEDISPLAYTYPE_GROUP = 0x00000005;

            private const int RESOURCEUSAGE_CONNECTABLE = 0x00000001;
            private const int RESOURCEUSAGE_CONTAINER = 0x00000002;


            private const int CONNECT_INTERACTIVE = 0x00000008;
            private const int CONNECT_PROMPT = 0x00000010;
            private const int CONNECT_REDIRECT = 0x00000080;
            private const int CONNECT_UPDATE_PROFILE = 0x00000001;
            private const int CONNECT_COMMANDLINE = 0x00000800;
            private const int CONNECT_CMD_SAVECRED = 0x00001000;

            private const int CONNECT_LOCALDRIVE = 0x00000100;

            #endregion

            #region Errors

            private const int NO_ERROR = 0;

            private const int ERROR_ACCESS_DENIED = 5;
            private const int ERROR_ALREADY_ASSIGNED = 85;
            private const int ERROR_BAD_DEVICE = 1200;
            private const int ERROR_BAD_NET_NAME = 67;
            private const int ERROR_BAD_PROVIDER = 1204;
            private const int ERROR_CANCELLED = 1223;
            private const int ERROR_EXTENDED_ERROR = 1208;
            private const int ERROR_INVALID_ADDRESS = 487;
            private const int ERROR_INVALID_PARAMETER = 87;
            private const int ERROR_INVALID_PASSWORD = 1216;
            private const int ERROR_MORE_DATA = 234;
            private const int ERROR_NO_MORE_ITEMS = 259;
            private const int ERROR_NO_NET_OR_BAD_PATH = 1203;
            private const int ERROR_NO_NETWORK = 1222;

            private const int ERROR_BAD_PROFILE = 1206;
            private const int ERROR_CANNOT_OPEN_PROFILE = 1205;
            private const int ERROR_DEVICE_IN_USE = 2404;
            private const int ERROR_NOT_CONNECTED = 2250;
            private const int ERROR_OPEN_FILES = 2401;

            #endregion

            #region PInvoke Signatures

            [DllImport("Mpr.dll")]
            private static extern int WNetUseConnection(
            IntPtr hwndOwner,
            NETRESOURCE lpNetResource,
            string lpPassword,
            string lpUserID,
            int dwFlags,
            string lpAccessName,
            string lpBufferSize,
            string lpResult
            );

            [DllImport("Mpr.dll")]
            private static extern int WNetCancelConnection2(
            string lpName,
            int dwFlags,
            bool fForce
            );

            [StructLayout(LayoutKind.Sequential)]
            private class NETRESOURCE
            {
                public int dwScope = 0;
                public int dwType = 0;
                public int dwDisplayType = 0;
                public int dwUsage = 0;
                public string lpLocalName = "";
                public string lpRemoteName = "";
                public string lpComment = "";
                public string lpProvider = "";
            }

            #endregion

            /// <summary>
            /// Crea un NetworkShareAccesser para el nombre del equipo dado. El usuario se le pedirá que introduzca sus credenciales
            /// </summary>
            /// <param name="NombreEquipoRemoto">Nombre del equipo remoto o la ip del equipo.</param>
            /// <returns></returns>
            public static NetworkShareAccesser Acceso(string NombreEquipoRemoto)
            {
                return new NetworkShareAccesser(NombreEquipoRemoto);
            }

            /// <summary>
            /// Creates a NetworkShareAccesser for the given computer name using the given username (format: domainOrComputername\Username) and password
            /// </summary>
            /// <param name="NombreEquipoRemoto">Nombre del equipo remoto o la ip del equipo.</param>
            /// <param name="UsuarioEquipoRemoto">Nombre de usuario que tiene permisos para acceder a la carpeta compartida.</param>
            /// <param name="PasswordUsuarioEquipoRemoto">Contraseña del usuario.</param>
            public static NetworkShareAccesser Acceso(string NombreEquipoRemoto, string UsuarioEquipoRemoto, string PasswordUsuarioEquipoRemoto)
            {
                return new NetworkShareAccesser(NombreEquipoRemoto,
                UsuarioEquipoRemoto,
                PasswordUsuarioEquipoRemoto);
            }

            /// <summary>
            /// Crea un NetworkShareAccesser para el nombre de equipo determinado utilizando el nombre de dominio / equipo, usuario y contraseña
            /// </summary>
            /// <param name="Dominio">Dominio de la red.</param>
            /// <param name="NombreEquipoRemoto">Nombre del equipo remoto o la ip del equipo.</param>
            /// <param name="UsuarioEquipoRemoto">Nombre de usuario que tiene permisos para acceder a la carpeta compartida.</param>
            /// <param name="PasswordUsuarioEquipoRemoto">Contraseña del usuario.</param>
            public static NetworkShareAccesser Acceso(string Dominio, string NombreEquipoRemoto, string UsuarioEquipoRemoto, string PasswordUsuarioEquipoRemoto)
            {
                return new NetworkShareAccesser(NombreEquipoRemoto,
                Dominio + @"\" + UsuarioEquipoRemoto,
                PasswordUsuarioEquipoRemoto);
            }

            private NetworkShareAccesser(string remoteComputerName)
            {
                RemoteComputerName = remoteComputerName;

                this.ConnectToShare(this._remoteUncName, null, null, true);
            }

            private NetworkShareAccesser(string remoteComputerName, string userName, string password)
            {
                RemoteComputerName = remoteComputerName;
                UserName = userName;
                Password = password;

                this.ConnectToShare(this._remoteUncName, this.UserName, this.Password, false);
            }

            private void ConnectToShare(string remoteUnc, string username, string password, bool promptUser)
            {
                NETRESOURCE nr = new NETRESOURCE
                {
                    dwType = RESOURCETYPE_DISK,
                    lpRemoteName = remoteUnc
                };

                int result;
                if (promptUser)
                {
                    result = WNetUseConnection(IntPtr.Zero, nr, "", "", CONNECT_INTERACTIVE | CONNECT_PROMPT, null, null, null);
                }
                else
                {
                    result = WNetUseConnection(IntPtr.Zero, nr, password, username, 0, null, null, null);
                }

                if (result != NO_ERROR)
                {
                    //throw new Win32Exception(result);
                }
            }

            private void DisconnectFromShare(string remoteUnc)
            {
                int result = WNetCancelConnection2(remoteUnc, CONNECT_UPDATE_PROFILE, false);
                if (result != NO_ERROR)
                {
                    //throw new Win32Exception(result);
                }
            }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            /// </summary>
            /// <filterpriority>2</filterpriority>
            public void Dispose()
            {
                this.DisconnectFromShare(this._remoteUncName);
            }
        }
    }
}
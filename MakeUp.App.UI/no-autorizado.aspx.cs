﻿using System;
using System.Web.Security;

namespace MakeUp.App.UI
{
    public partial class no_autorizado : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string Mensaje = "{0} es el mejor en {1}.";
            string Persona = "Francisco Pérez";
            string Deporte = "MTB";

            string Resultado = string.Format(Mensaje, Persona, Deporte);
        }

        protected void btnGoHome_Click(object sender, EventArgs e)
        {
            Response.Redirect(FormsAuthentication.DefaultUrl, false);
        }
    }
}
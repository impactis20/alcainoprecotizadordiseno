﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;

using MakeUp.BL;

namespace MakeUp.App.UI.hubs
{
    public class NotificacionAppHub : Hub
    {
        public static List<Entities.UsuarioEntity> LstUsuarioConectado { get; set; } = new List<Entities.UsuarioEntity>();

        public void Connect()
        {
            int IDUsuario = App_Code.Seguridad.GetCurrentIDUSuario(); //Se puede utilizar GetCurrentIDUSuario() debido a que cuando ingresa a esta función, entra con un Postback
            bool UsuarioConectado = LstUsuarioConectado.Any(x => x.ID == IDUsuario);
            int CantidadNotificaciones = Convert.ToInt16(ConfigurationManager.AppSettings["cantidadNotificacionesApp"]);
            int ContNotificacionesSinVer = 0;
            //try
            //{
            //}
            //catch
            //{
            //    LstUsuarioConectado.Any(x => x.LstIDConexion.Contains(Context.ConnectionId));
            //}

            Entities.UsuarioEntity UsuarioEntrante = new Entities.UsuarioEntity();

            if (UsuarioConectado)
            {
                //Busca al usuario que ya se encuentra conectado
                foreach (Entities.UsuarioEntity UsuarioYaConectado in LstUsuarioConectado)
                {
                    if (UsuarioYaConectado.ID == IDUsuario && !UsuarioYaConectado.LstIDConexion.Contains(Context.ConnectionId))
                    {
                        //Agrega una nueva conexión del usuario
                        UsuarioYaConectado.LstIDConexion.Add(Context.ConnectionId);

                        //Copia datos de usuario
                        UsuarioEntrante = UsuarioYaConectado;
                        break;
                    }
                }
            }
            else
            {
                //Usuario Nuevo
                UsuarioEntrante = UsuarioBL.GetUsuario(IDUsuario, string.Empty, string.Empty, string.Empty, null, false)[0]; //Busca usuario específico
                UsuarioEntrante.LstIDConexion = new List<string>();
                UsuarioEntrante.LstIDConexion.Add(Context.ConnectionId); //Agrega ID de conexión

                //Agrega usuario a lista de conectados
                LstUsuarioConectado.Add(UsuarioEntrante);
            }

            //En una lista obtiene las notificaciones del usuario
            List<Entities.NotificacionAppEntity> LstNotificaciones = NotificacionBL.GetNotificacionesAppUsuario(UsuarioEntrante.ID, CantidadNotificaciones, ref ContNotificacionesSinVer);

            //Envía notificaciones al usuario entró que al sistema (y al resto de sus conexiones)
            Clients.Clients(UsuarioEntrante.LstIDConexion).cargarNotificaciones(LstNotificaciones, ContNotificacionesSinVer);
        }

        public static void EnviaNotificacionesUsuariosConectados(IHubContext Context)
        {
            int CantidadNotificaciones = Convert.ToInt16(ConfigurationManager.AppSettings["cantidadNotificacionesApp"]);
            int ContNotificacionesSinVer = 0;

            foreach (Entities.UsuarioEntity UsuarioConectado in LstUsuarioConectado)
            {
                //En una lista obtiene las notificaciones del usuario
                List<Entities.NotificacionAppEntity> LstNotificaciones = NotificacionBL.GetNotificacionesAppUsuario(UsuarioConectado.ID, CantidadNotificaciones, ref ContNotificacionesSinVer);
                Context.Clients.Clients(UsuarioConectado.LstIDConexion).cargarNotificaciones(LstNotificaciones, ContNotificacionesSinVer);
            }
        }

        public void NotificacionesVisualizadas()
        {
            Entities.UsuarioEntity UsuarioConectado = new Entities.UsuarioEntity();

            //Busca al usuario que ya se encuentra conectado para obtener su lista de conexiones
            foreach (Entities.UsuarioEntity UsuarioYaConectado in LstUsuarioConectado)
            {
                if (UsuarioYaConectado.LstIDConexion.Contains(Context.ConnectionId))
                {
                    UsuarioConectado = UsuarioYaConectado;
                    break;
                }
            }

            //Deja en "Vista" todas las notificaciones del usuario, es decir, no hay notificaciones pendientes.
            NotificacionBL.SetNotificacionesEnEstadoVista(UsuarioConectado.ID);

            //Se conecta con el cliente para ocultar el globo de notificaciones
            Clients.Clients(UsuarioConectado.LstIDConexion).ocultaGloboDeNotificaciones();
        }

        public void IngresoNotificacionApp(int IDNotificacionApp)
        {
            Entities.UsuarioEntity UsuarioConectado = new Entities.UsuarioEntity();
            int CantidadNotificaciones = Convert.ToInt16(ConfigurationManager.AppSettings["cantidadNotificacionesApp"]);
            int ContNotificacionesSinVer = 0;

            //Busca al usuario que ya se encuentra conectado para obtener su lista de conexiones
            foreach (Entities.UsuarioEntity UsuarioYaConectado in LstUsuarioConectado)
            {
                if (UsuarioYaConectado.LstIDConexion.Contains(Context.ConnectionId))
                {
                    UsuarioConectado = UsuarioYaConectado;
                    break;
                }
            }

            //Deja la notificación en visto
            NotificacionBL.SetIngresoNotificacion(IDNotificacionApp, UsuarioConectado.ID, true);

            //Se conecta con el cliente para dejar en visto la notificación

            //En una lista obtiene las notificaciones del usuario
            List<Entities.NotificacionAppEntity> LstNotificaciones = NotificacionBL.GetNotificacionesAppUsuario(UsuarioConectado.ID, CantidadNotificaciones, ref ContNotificacionesSinVer);

            //Envía notificaciones al usuario entró que al sistema (y al resto de sus conexiones)
            Clients.Clients(UsuarioConectado.LstIDConexion).cargarNotificaciones(LstNotificaciones, ContNotificacionesSinVer);
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            //Busca el usuario ya ingresado
            foreach (Entities.UsuarioEntity Usuario in LstUsuarioConectado)
            {
                bool Remover = Usuario.LstIDConexion.Remove(Context.ConnectionId);

                if (Remover)
                {
                    //Si no hay mas conexiones, entonces elimina al usuario
                    if (Usuario.LstIDConexion.Count == 0)
                        LstUsuarioConectado.Remove(Usuario);

                    break;
                }
            }

            return base.OnDisconnected(stopCalled);
        }
    }
}
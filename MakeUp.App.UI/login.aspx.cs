﻿using System;
using System.Web;
using System.Web.Security;

using MakeUp.BL;

namespace MakeUp.App.UI
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //loguea automáticamente al usuario si la última vez checkeó "No cerrar sesión" { Si usted cierra el navegador sin cerrar sesión, la próxima vez que ingrese al Portal, entrará automáticamente sin ingresar Email y Contraseña }
            if (Request.Cookies["dReUser_"] != null)
                ValidaUsuario(Util.Funcion.Desencriptar(Request.Cookies["dReUser_"].Value).Split('|')[0], Util.Funcion.Desencriptar(Request.Cookies["dReUser_"].Value).Split('|')[1], true);
        }

        protected void btnEntrar_Click(object sender, EventArgs e)
        {
            string Username = txtUsername.Text.Trim();
            string Password = txtPassword.Text.Trim();

            ValidaUsuario(Username, Password, cbMantenerSesionIniciada.Checked);
        }

        private void ValidaUsuario(string Username, string Password, bool MantieneSesionIniciada)
        {
            if (UsuarioBL.AutenticarUsuario(Username, Password))
            {
                Entities.UsuarioEntity Usuario = UsuarioBL.GetValidaUsername(Username);
                if (Usuario != null)
                {
                    if (MantieneSesionIniciada)
                        DejarSesionIniciada(Usuario.UserName, Password);

                    CreaCookieUsuario(Usuario.Nombre, Usuario.ID.ToString());

                    Response.Redirect(FormsAuthentication.GetRedirectUrl(Usuario.UserName, false));
                }
                else
                {
                    error.Visible = true;
                    error.InnerHtml = "<p><i class='fa fa-frown-o'>&nbsp;&nbsp;</i>Usted no ha sido registrado en el sistema.</p>";
                }
            }
            else
            {
                error.Visible = true;
                error.InnerHtml = "<p><i class='fa fa-exclamation-triangle'>&nbsp;&nbsp;</i>Credenciales inválidas.</p>";
            }
        }

        private void CreaCookieUsuario(string Name, string UserData)
        {
            FormsAuthenticationTicket Ticket = new FormsAuthenticationTicket(2, Name, DateTime.Now, DateTime.Now.AddDays(2), false, UserData, FormsAuthentication.FormsCookiePath);
            string TicketEncriptado = FormsAuthentication.Encrypt(Ticket);
            HttpCookie CookieUsuario = new HttpCookie(FormsAuthentication.FormsCookieName, TicketEncriptado);
            Response.Cookies.Add(CookieUsuario);
        }

        private void DejarSesionIniciada(string Username, string Password)
        {
            string DatosUsuarioValidacion = string.Format("{0}|{1}", Username, Password);
            HttpCookie Datos = new HttpCookie("dReUser_", Util.Funcion.Encriptar(DatosUsuarioValidacion));
            Datos.Expires = DateTime.Now.AddDays(7);
            Response.Cookies.Add(Datos);
        }
    }
}
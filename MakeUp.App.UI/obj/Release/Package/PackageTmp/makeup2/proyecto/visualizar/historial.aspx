﻿<%@ Page Title="Historial del proyecto" Language="C#" MasterPageFile="~/makeup2/proyecto/visualizar/proyecto.master" AutoEventWireup="true" CodeBehind="historial.aspx.cs" Inherits="MakeUp.App.UI.makeup2.proyecto.visualizar.historial" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headProyecto" runat="server">
    <link href="../../../css/bootstrap-select.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenidoProyecto" runat="server">
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <asp:HiddenField ID="hfIdProyecto" runat="server" />
    <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <asp:DropDownList ID="ddlHistorial" runat="server" CssClass="selectpicker" OnSelectedIndexChanged="ddlHistorial_SelectedIndexChanged" AutoPostBack="true" data-width="150px">
                    <asp:ListItem Text="Ascendente" Value="asc" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Descendente" Value="desc"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <asp:GridView ID="gvHistorial" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None"
                        OnRowDataBound="gvHistorial_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Evento" ItemStyle-CssClass="col-lg-10">
                                <ItemTemplate>
                                    <asp:Label ID="lblEvento" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fecha" ItemStyle-CssClass="col-lg-2">
                                <ItemTemplate>
                                    <asp:Label ID="lblFecha" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
﻿<%@ Page Title="Estados de pago del proyecto" Language="C#" MasterPageFile="~/makeup2/proyecto/visualizar/proyecto.master" AutoEventWireup="true" CodeBehind="estado-de-pago.aspx.cs" Inherits="MakeUp.App.UI.makeup2.proyecto.visualizar.estado_de_pago" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headProyecto" runat="server">
    <link href="../../../css/bootstrap-select.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenidoProyecto" runat="server">
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/languages/es.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/count-character-textbox.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/number-format-using-numeral.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/dropdown-igualitario.js") %>'></script>

    <script type="text/javascript">
        //calcula el monto del EP en base a un porcentaje
        function CalculaMontoEP(Porcentaje) {
            if (Porcentaje !== '') {
                var txtPorcentaje = document.getElementById("<%= txtPorcentaje.ClientID %>");
                var txtMonto = document.getElementById("<%= txtMonto.ClientID %>");
                var ValorTotalProyecto = parseFloat(ConvertToNumber('<%= hfValorTotalProyecto.Value %>'));

                Porcentaje = parseFloat(ConvertToNumber(Porcentaje));

                if (Porcentaje > 100 || Porcentaje === 0) {
                    alerta('Porcentaje inválido...', 'El porcentaje no puede exceder el máximo permitido (100%), tampoco puede ser igual a 0.', 'text-danger');
                    txtPorcentaje.value = "";
                    txtMonto.value = "";
                    return;
                }
                else {
                    numeral.language('es');
                    var ValorMonto = ValorTotalProyecto * Porcentaje / 100;
                    txtMonto.value = numeral(ValorMonto).format('0,0' + getCantidadDecimal()); //[00] dos decimales
                    document.getElementById('<%= hfMonto.ClientID %>').value = ValorMonto; //almacena monto
                    document.getElementById('<%= hfPorcentaje.ClientID %>').value = Porcentaje; //almacena monto
                }
            }
        }

        //calcula el porcentaje del EP en base a un monto
        function CalculaPorcentajeEP(Monto) {
            if (Monto !== '') {
                var txtPorcentaje = document.getElementById("<%= txtPorcentaje.ClientID %>");
                var txtMonto = document.getElementById("<%= txtMonto.ClientID %>");
                var ValorTotalProyecto = parseFloat(ConvertToNumber('<%= hfValorTotalProyecto.Value %>'));

                Monto = parseFloat(ConvertToNumber(Monto));

                if (Monto > ValorTotalProyecto || Monto === 0) {
                    alerta('Monto inválido...', 'El monto no puede exceder el Saldo del proyecto, tampoco puede ser igual a 0.', 'text-danger');
                    txtPorcentaje.value = "";
                    txtMonto.value = "";
                    return;
                }
                else {
                    numeral.language('es');
                    var ValorPorcentaje = Monto / ValorTotalProyecto * 100;



                    txtPorcentaje.value = numeral(ValorPorcentaje).format('0,0.[00]'); //porcentaje debe basarse en 2 decimales
                    document.getElementById('<%= hfMonto.ClientID %>').value = Monto; //almacena monto
                    document.getElementById('<%= hfPorcentaje.ClientID %>').value = ValorPorcentaje; //almacena porcentaje
                }
            }
        }

        function getCantidadDecimal() {
            var valorTipoValor = parseInt(document.getElementById('<%= hfValorTipoValor.ClientID %>').value);
            if (valorTipoValor > 1)
                return '.[00]'; //[00] dos decimales

            return '';
        }
    </script>
    <script type="text/javascript">
        //function pageLoad(sender, args) {

            //Habilita y deshabilita textbox        
            $(function () {
                $("input[type=radio][name*=TipoEP]").change(function () {
                    if (this.value == 'rbPorcentaje') {
                        $('[id*=txtPorcentaje]').removeAttr('disabled');
                        $('[id*=txtMonto]').attr('disabled', 'disabled');

                        //$('[id*=<%= txtPorcentaje.ClientID %>]').focus();
                        document.getElementById("<%= txtPorcentaje.ClientID %>").focus();
                    }
                    else {
                        $('[id*=txtMonto]').removeAttr('disabled');
                        $('[id*=txtPorcentaje]').attr('disabled', 'disabled');

                        //$('[id*=<%= txtMonto.ClientID %>]').focus();
                        document.getElementById("<%= txtMonto.ClientID %>").focus();
                    }

                    $('[id*=txtPorcentaje]').val('');
                    $('[id*=txtMonto]').val('');
                })
            });
        //}
    </script>

    <asp:HiddenField ID="hfIdProyecto" runat="server" />
    <asp:HiddenField ID="hfValorTotalProyecto" runat="server" />
    <asp:HiddenField ID="hfPorcentaje" runat="server" />
    <asp:HiddenField ID="hfMonto" runat="server" />
    <asp:HiddenField ID="hfValorTipoValor" runat="server" />
    
    <div class="form-group">
        <p>Ingrese el monto o el porcentaje para generar un estado de pago. El cálculo se realiza en base al saldo del proyecto.</p>
        <p>
            <label class="label label-danger"><i class="fa fa-info-circle" aria-hidden="true"></i> Importante</label>
            los estados de pagos se aplican a todas las notas de pedidos creadas o ajustadas.
        </p>
        <div class="row">
            <div class="form-group">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group">
                                <asp:RadioButton ID="rbPorcentaje" runat="server" Text="&nbsp;Porcentaje" GroupName="TipoEP" Checked="true" />
                                <div class="input-group">
                                    <asp:TextBox ID="txtPorcentaje" runat="server" CssClass="form-control input-sm text-right" onblur="CalculaMontoEP(this.value)" ViewStateMode="Enabled" MaxLength="5" autocomplete="off" onkeyup="FormatoNumerico(this, 2);"></asp:TextBox>
                                    <span class="input-group-addon">%</span>
                                </div>
                                <asp:RequiredFieldValidator ID="rfvPorcentaje" runat="server" ControlToValidate="txtPorcentaje" ErrorMessage="Campo obligatorio..." SetFocusOnError="true" Display="Dynamic" CssClass="alert-danger" ValidationGroup="AgregarEstadoPago" />
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="form-group">
                                <asp:RadioButton ID="rbMonto" runat="server" Text="&nbsp;Monto" GroupName="TipoEP" />
                                <div class="input-group">
                                    <span class="input-group-addon"><asp:Label ID="lblMonedaSeleccionadaEP" runat="server"></asp:Label></span>
                                    <asp:TextBox ID="txtMonto" runat="server" CssClass="form-control input-sm valortotal-format" onblur="CalculaPorcentajeEP(this.value)" ViewStateMode="Enabled" Enabled="false" autocomplete="off"></asp:TextBox>
                                </div>
                                <asp:RequiredFieldValidator ID="rfvMonto" runat="server" ControlToValidate="txtMonto" ErrorMessage="Campo obligatorio..." SetFocusOnError="true" Display="Dynamic" CssClass="alert-danger" ValidationGroup="AgregarEstadoPago" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-10">
                            <div class="form-group">
                                <label>* Glosa</label>
                                <asp:TextBox ID="txtGlosa" runat="server" placeholder="Ingrese texto..." CssClass="form-control input-sm" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvGlosa" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtGlosa" SetFocusOnError="true" Display="Dynamic" CssClass="alert-danger" ValidationGroup="AgregarEstadoPago" />
                                <asp:Label ID="lblContG" runat="server" CssClass="help-block pull-right"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-10">
                            <div class="form-group">
                                <div class="pull-right">
                                    <asp:Button ID="btnAgregarEstadoPago" runat="server" Text="Agregar" CssClass="btn btn-default" OnClick="btnAgregarEstadoPago_Click" rel="tooltip" data-toggle="tooltip" data-placement="top" title="Agregar estado de pago" ValidationGroup="AgregarEstadoPago" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-6">
                    <div class="col-lg-12">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-6 control-label">Valor Proyecto</label>
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <asp:DropDownList id="ddlTipoValor3" runat="server" CssClass="selectpicker dtv1" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:TextBox ID="txtValorProyecto" runat="server" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">Valor Facturado</label>
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <asp:DropDownList id="ddlTipoValor2" runat="server" CssClass="selectpicker dtv1" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:TextBox ID="txtValorFacturado" runat="server" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">Saldo</label>
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <asp:DropDownList id="ddlTipoValor1" runat="server" CssClass="selectpicker dtv1" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:TextBox ID="txtSaldoPorFacturar" runat="server" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ConfirmaEliminarEP" tabindex="-1" role="dialog" aria-labelledby="xxxx">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Confirmar Eliminar Estado de Pago</h2>
                </div>
                <div class="modal-body">
                    <p>
                        ¿Está seguro de eliminar El estado de pago que representa el
                            <asp:Label ID="lblEstadoPagoEliminar" runat="server"></asp:Label>
                        del proyecto?
                    </p>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right">
                                <button id="btnConfirmarEliminarEP" runat="server" class="btn btn-primary" type="button" onserverclick="btnConfirmarEliminarEP_ServerClick">
                                    <span class="fa fa-remove"></span>&nbsp;&nbsp;Confirmar Eliminación
                                </button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="table-responsive">
            <asp:GridView ID="gvEstadoPago" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None"
                OnRowDataBound="gvEstadoPago_RowDataBound" OnRowDeleting="gvEstadoPago_RowDeleting">
                <Columns>
                    <asp:TemplateField HeaderText="#">
                        <ItemTemplate>
                            <asp:Label ID="lblN" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Fecha Creación">
                        <ItemTemplate>
                            <asp:Label ID="lblFechaCreacion" runat="server" Text='<%# Eval("FechaCreacion") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Porcentaje">
                        <ItemTemplate>
                            <asp:Label ID="lblPorcentaje" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Monto">
                        <ItemTemplate>
                            <asp:Label ID="lblMonto" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Glosa">
                        <ItemTemplate>
                            <asp:Label ID="lblGlosa" runat="server" Text='<%# Eval("Glosa") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Facturado">
                        <ItemTemplate>
                            <asp:Label ID="lblFacturaEmitida" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Facturado MG" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblFacturado" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-CssClass="col-lg-2 right">
                        <ItemTemplate>
                            <div class="pull-right">
                                <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" CssClass="btn btn-danger btn-xs" CommandName="Delete" />
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
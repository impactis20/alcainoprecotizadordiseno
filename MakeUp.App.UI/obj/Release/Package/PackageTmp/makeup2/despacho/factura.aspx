﻿<%@ Page Title="Facturas" Language="C#" MasterPageFile="~/makeup2/menu.master" AutoEventWireup="true" CodeBehind="factura.aspx.cs" Inherits="MakeUp.App.UI.makeup2.despacho.factura" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headMenu" runat="server">
    <link href="../../css/bootstrap-typeahead.css" rel="stylesheet" />
    <link rel="stylesheet" href="../../css/bootstrap-select.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/autocomplete-query.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap3-typeahead.min.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            AutoComplete("<%= txtCliente.ClientID %>", '<%= ResolveUrl(string.Format("{0}/GetNombreFantasia", Request.Url.AbsolutePath)) %>');
            AutoComplete("<%= txtNombre.ClientID %>", '<%= ResolveUrl(string.Format("{0}/GetNombreProyecto", Request.Url.AbsolutePath)) %>');
        });
    </script>
    <h2 class="page-header">Facturas</h2>
    <p>Busque una factura emitida por ID de proyecto, nombre de proyecto y/o cliente. Además puede filtrar según estado de despacho.</p>
    <div class="form-group">
        <div class="row">
            <div class="col-lg-1">
                <div class="form-group">
                    <label>ID</label>
                    <asp:TextBox ID="txtID" runat="server" CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label>Nombre proyecto</label>
                    <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label>Cliente <small>(nombre fantasía)</small></label>
                    <asp:TextBox ID="txtCliente" runat="server" CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label>Filtros (*)</label><br />
                    <asp:ListBox ID="lstEstadoDespachoFactura" runat="server" SelectionMode="Multiple" CssClass="selectpicker" data-width="100%" data-selected-text-format="count > 3" data-actions-box="true">
                    </asp:ListBox>
                    <asp:RequiredFieldValidator ID="rfvEstadoDespachoFactura" runat="server" ErrorMessage="Seleccione para buscar..." ControlToValidate="lstEstadoDespachoFactura" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Buscar" />
                </div>
            </div>
            <div class="col-lg-2">
                <div class="form-group">
                    <%--<label>Nro. Factura</label><br />
                    <asp:TextBox ID="txtNroFactura" runat="server" CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>--%>
                    <div class="pull-right">
                        <br />
                        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn btn-primary" OnClick="btnBuscar_Click" ValidationGroup="Buscar" />
                    </div>
                </div>
            </div>
        </div>
        <%--<div class="row">
            <div class="col-lg-12">
                <div class="pull-right">
                    <br />
                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn btn-primary" OnClick="btnBuscar_Click" ValidationGroup="Buscar" />
                </div>
            </div>
        </div>--%>
    </div>

    <div class="form-group">
        <div class="col-lg-12">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <ul class="list-inline" style="margin-bottom:0px">
                            <li id="eliminado" runat="server" visible="false"><i class="fa fa-trash text-danger" style="margin-left: 4px;margin-right: 4px;"></i> Proyecto eliminado</li>
                            <li id="porRegularizar" runat="server" visible="false"><i class="fa fa-exclamation-triangle text-warning" style="margin-left: 4px;margin-right: 4px;"></i> Proyecto por regularizar</li>
                            <li><label class="label label-danger">&nbsp;&nbsp;</label> Facturas pendientes de despacho</li>
                            <li><label class="label label-success">&nbsp;&nbsp;</label> Facturas despachadas</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-12">
            <div class="row">
                <asp:Label ID="lblBuscar" runat="server"></asp:Label>
                <div class="table-responsive">
                    <asp:GridView ID="gvFactura" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-responsive" GridLines="None" OnRowDataBound="gvFactura_RowDataBound" >
                        <Columns>
                            <asp:TemplateField HeaderText="" HeaderStyle-Width="35px">
                                <ItemTemplate>
                                    <asp:Label id="lblColor" runat="server" Text="&nbsp;&nbsp;"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Eval("Proyecto.ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nombre Proyecto">
                                <ItemTemplate>
                                    <asp:Label ID="lnkProyecto" runat="server" Text='<%# Eval("Proyecto.Nombre") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cliente <small>(nombre fantasía)</small>">
                                <ItemTemplate>
                                    <asp:Label ID="lblCliente" runat="server" Text='<%# Eval("Cliente.NombreFantasia") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fecha facturación">
                                <ItemTemplate>
                                    <asp:Label ID="lblFechaFacturacion" runat="server" Text='<%# string.Format("{0: dd/MM/yyyy HH:mm}", Eval("FechaOrdenFacturacion")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="Glosa">
                                <ItemTemplate>
                                    <asp:Label ID="lblGlosa" runat="server" Text='<%# Eval("Glosa") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Monto factura">
                                <ItemTemplate>
                                    <asp:Label ID="lblMontoFactura" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nro. factura">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkNroFactura" runat="server"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

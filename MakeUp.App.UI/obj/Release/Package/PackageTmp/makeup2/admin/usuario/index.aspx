﻿<%@ Page Title="Usuarios del sistema" Language="C#" MasterPageFile="~/makeup2/menu.master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="MakeUp.App.UI.makeup2.admin.usuario.index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headMenu" runat="server">
    <link href="../../../css/bootstrap-select.css" rel="stylesheet" />
    <link href="../../../css/bootstrap-typeahead.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/autocomplete-query.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap3-typeahead.min.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/personalizado/checkbox-roles.js") %>'></script>

    <script type="text/javascript">
        $(function () {
            //carga autocompletar en textbox de búsqueda
            AutoComplete("<%= txtUsername.ClientID %>", '<%= ResolveUrl(string.Format("{0}/GetUsername", Request.Url.AbsolutePath)) %>');
            AutoComplete("<%= txtNombre.ClientID %>", '<%= ResolveUrl(string.Format("{0}/GetNombreUsuario", Request.Url.AbsolutePath)) %>');
            AutoComplete("<%= txtEmail.ClientID %>", '<%= ResolveUrl(string.Format("{0}/GetEmail", Request.Url.AbsolutePath)) %>');
        
            //cuando abre modal de agregar usuario...
            //Refresca el combobox "Rol predeterminado" a valor por defecto "Seleccione..."
            //Al abrir el modal obliga al usuario a escoger nuevamente un rol
            $('[id*=x3]').on('show.bs.modal', function () {
                $("#<%= lstRolPredeterminado.ClientID %>").selectpicker('val', '-1');
                $("#<%= lstRolPredeterminado.ClientID %>").selectpicker('refresh');
                $("#<%= descripcionRol.ClientID %>").hide();
            });

            //cuando cierra modal de agregar usuario...
            //limpia el formulario
            $('[id*=x3]').on('hidden.bs.modal', function () {
                $('#<%= txtAddUserName.ClientID %>').val('');
                $('#<%= txtAddNombre.ClientID %>').val('');
                $('#<%= txtAddPaterno.ClientID %>').val('');
                $('#<%= txtAddMaterno.ClientID %>').val('');
                $('#<%= txtAddEmail.ClientID %>').val('');
            });

            //autocompletar usuario a agregar mediante username
            $('[id*= <%= txtAddUserName.ClientID %> ]').typeahead({
                hint: true,
                highlight: true,
                minLength: 1,
                source: function (request, response) {
                    $.ajax({
                        url: '<%= ResolveUrl(string.Format("{0}/GetUsuarioAD", Request.Url.AbsolutePath)) %>',
                        data: "{ 'username': '" + request + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            items = [];
                            map = {};
                            $.each(data.d, function (i, item) {
                                map[this.UserName] = { Nombre: this.Nombre, ApellidoPaterno: this.ApellidoPaterno, ApellidoMaterno: this.ApellidoMaterno, Email: this.Email };
                                items.push(this.UserName);
                            });

                            $('[id*= <%= hfUserName.ClientID %> ]').val('');
                            $('[id*= <%= hfNombre.ClientID %> ]').val('');
                            $('[id*= <%= hfApellidoP.ClientID %> ]').val('');
                            $('[id*= <%= hfApellidoM.ClientID %> ]').val('');
                            $('[id*= <%= hfEmail.ClientID %> ]').val(''); 

                            $('[id*= <%= txtAddNombre.ClientID %> ]').val('');
                            $('[id*= <%= txtAddPaterno.ClientID %> ]').val('');
                            $('[id*= <%= txtAddMaterno.ClientID %> ]').val('');
                            $('[id*= <%= txtAddEmail.ClientID %> ]').val(''); 

                            response(items);
                            $(".dropdown-menu").css("height", "auto");
                        },
                        error: function (response) {
                            $('[id*= <%= hfUserName.ClientID %> ]').val('');
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            $('[id*= <%= hfUserName.ClientID %> ]').val('');
                            alert(response.responseText);
                        }
                    });
                },
                updater: function (UserName) {
                    $('[id*= <%= hfUserName.ClientID %> ]').val(UserName); //asigna valor cuando usuario selecciona valor del autocompletar
                    $('[id*= <%= hfNombre.ClientID %> ]').val(map[UserName].Nombre);
                    $('[id*= <%= hfApellidoP.ClientID %> ]').val(map[UserName].ApellidoPaterno);
                    $('[id*= <%= hfApellidoM.ClientID %> ]').val(map[UserName].ApellidoMaterno);
                    $('[id*= <%= hfEmail.ClientID %> ]').val(map[UserName].Email); 

                    $('[id*= <%= txtAddNombre.ClientID %> ]').val(map[UserName].Nombre);
                    $('[id*= <%= txtAddPaterno.ClientID %> ]').val(map[UserName].ApellidoPaterno);
                    $('[id*= <%= txtAddMaterno.ClientID %> ]').val(map[UserName].ApellidoMaterno);
                    $('[id*= <%= txtAddEmail.ClientID %> ]').val(map[UserName].Email);

                    return UserName;
                }
            });
        });

        function MuestraListBox() {
            $('.selectpicker').selectpicker();
            $('.selectpicker').selectpicker('refresh');
        }

        function ModalEditarUsuario() {
            $('#<%= editaUsuario.ClientID %>').modal({
                backdrop: 'static',
                keyboard: true
            });
        }

        function ValidateListBox(sender, args) {
            var valorSeleccionado = parseInt($("#<%= lstRolPredeterminado.ClientID %>").val());

            if (valorSeleccionado === -1) {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }       
        }
    </script>
    
    <asp:HiddenField ID="hfUserName" runat="server" />
    <asp:HiddenField ID="hfNombre" runat="server" />
    <asp:HiddenField ID="hfApellidoP" runat="server" />
    <asp:HiddenField ID="hfApellidoM" runat="server" />
    <asp:HiddenField ID="hfEmail" runat="server" />

    <div class="modal fade" id="x3" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Agregar usuario</h2>
                </div>
                <div class="modal-body">
                    <p>Ingrese un
                        <var>username</var>
                        de Active directory para buscar un usuario e incorporarlo al sistema. El usuario es válido si aparece en la lista desplegable. Una vez seleccionado el sistema autocompletará el
                        <var>nombre</var>,
                        <var>apellido paterno</var>,
                        <var>apellido materno</var>
                        y
                        <var>email</var>. Luego seleccione un Rol (al usuario le puede conceder acceso a más módulos y controles si necesario). Para terminar haga clic en <span class="label label-primary">Agregar Usuario</span>.</p>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12">
                                <var>Los campos con asterisco (*) son obligatorios.</var>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>* Username</label>
                                    <asp:TextBox ID="txtAddUserName" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvUsername" runat="server" ErrorMessage="Campo <b>Nombre</b> es obligatorio..." ControlToValidate="txtAddUserName" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Agregar" />
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <asp:TextBox ID="txtAddNombre" runat="server" CssClass="form-control input-sm" ReadOnly="true" MaxLength="100" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Apellido Paterno</label>
                                    <asp:TextBox ID="txtAddPaterno" runat="server" CssClass="form-control input-sm" ReadOnly="true" MaxLength="200" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Apellido Materno</label>
                                    <asp:TextBox ID="txtAddMaterno" runat="server" CssClass="form-control input-sm" ReadOnly="true" MaxLength="12" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <asp:TextBox ID="txtAddEmail" runat="server" CssClass="form-control input-sm" ReadOnly="true" MaxLength="100" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <div class="col-lg-12" style="margin-top: 20px">
                                            <asp:CheckBox ID="cbVisualizaTodoContacto" runat="server" Text="¿Puede visualizar todos los contactos?" CssClass="checkbox" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="up" runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>Rol predeterminado</label>
                                            <asp:ListBox ID="lstRolPredeterminado" runat="server" SelectionMode="Single"  CssClass="selectpicker" AutoPostBack="true" OnSelectedIndexChanged="lstRolPredeterminado_SelectedIndexChanged" data-width="100%"></asp:ListBox>
                                            <%--<asp:RequiredFieldValidator ID="rfvRolPredeterminado" runat="server" ErrorMessage="Seleccione un <b>rol</b>..." ControlToValidate="lstRolPredeterminado" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Agregar" />--%>
                                            <asp:CustomValidator ID="CustomValidator1" runat="server" CssClass="alert-danger" ErrorMessage="Seleccione un rol..." ClientValidationFunction="ValidateListBox" ValidationGroup="Agregar"></asp:CustomValidator>
                                        </div>
                                    </div>
                                </div>
                                <div id="descripcionRol" runat="server" visible="false">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Descripción del rol</label><br />
                                                <asp:TextBox ID="txtDescripcionRol" runat="server" CssClass="form-control input-sm" Enabled="false" TextMode="MultiLine" Rows="2" MaxLength="150" autocomplete="off"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <label>Módulos y controles</label>
                                            <div class="table-responsive">
                                                <asp:GridView ID="gvModulo" runat="server" CssClass="col-lg-12" AutoGenerateColumns="false" GridLines="None" ShowHeader="false"
                                                    OnRowDataBound="gvModulo_RowDataBound">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-CssClass="row">
                                                            <ItemTemplate>
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <div class="form-group">
                                                                            <div class="col-lg-12">
                                                                                <asp:CheckBox ID="cbModulo" runat="server" CssClass="checkbox panel-title" action="add"></asp:CheckBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="controles" runat="server" class="panel-body">
                                                                        <div class="col-lg-12">
                                                                            <asp:CheckBoxList ID="cblControl" runat="server" CssClass="checkbox">
                                                                            </asp:CheckBoxList>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right">
                                <asp:Button ID="btnAgregar" runat="server" Text="Agregar Usuario" CssClass="btn btn-primary" OnClick="btnAgregar_Click" ValidationGroup="Agregar" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editaUsuario" runat="server" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Editar usuario: <asp:Label ID="lblNombreUsuarioEditar" runat="server"></asp:Label></h2>
                </div>
                <div class="modal-body">
                    <p>Este módulo permite restablecer la visualización de contactos compartidos, el rol y el acceso a módulos y controles.</p>                        
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Contactos</label><br />
                                    <div class="col-lg-12">
                                        <div class="col-lg-12">
                                            <asp:CheckBox ID="cbVisualizaTodoContactoEditar" runat="server" Text="¿Puede visualizar todos los contactos?" CssClass="checkbox"></asp:CheckBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="upEditar" runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>Rol</label>
                                            <asp:ListBox ID="lstRolPredeterminadoEditar" runat="server" SelectionMode="Single" CssClass="selectpicker" AutoPostBack="true" OnSelectedIndexChanged="lstRolPredeterminadoEditar_SelectedIndexChanged" data-width="100%"></asp:ListBox>
                                        </div>
                                    </div>
                                    <div>
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Descripción del rol</label><br />
                                            <asp:TextBox ID="txtDescripcionRolEditar" runat="server" CssClass="form-control input-sm" Enabled="false" TextMode="MultiLine" Rows="2" MaxLength="150" autocomplete="off"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label>Módulos y controles</label>
                                        <div class="table-responsive">
                                            <asp:GridView ID="gvModuloEditar" runat="server" CssClass="col-lg-12" AutoGenerateColumns="false" GridLines="None" ShowHeader="false"
                                                OnRowDataBound="gvModulo_RowDataBound">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-CssClass="row">
                                                        <ItemTemplate>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <div class="form-group">
                                                                        <div class="col-lg-12">
                                                                            <asp:CheckBox ID="cbModulo" runat="server" CssClass="checkbox panel-title"></asp:CheckBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="controles" runat="server" class="panel-body">
                                                                    <div class="col-lg-12">
                                                                        <asp:CheckBoxList ID="cblControl" runat="server" CssClass="checkbox">
                                                                        </asp:CheckBoxList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="lstRolPredeterminado" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right">
                                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn btn-primary" OnClick="btnGuardar_Click" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h2 class="page-header">Buscar usuario</h2>
    <p>Busque un usuario filtrando por nombre, username o email. Para agregar un nuevo usuario haga <a href="#" data-toggle="modal" data-target="#x3">clic aquí</a>.</p>
    <div class="form-group">
        <div id="formulario" runat="server">
            <div class="row">
                <div class="col-lg-2">
                    <div class="form-group">
                        <label>Username</label>
                        <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control input-sm" autocomplete="off" placeholder="Ingrese username..."></asp:TextBox>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label>Nombre</label>
                        <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control input-sm" autocomplete="off" placeholder="Ingrese nombre..."></asp:TextBox>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label>Email</label>
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control input-sm" autocomplete="off" placeholder="Ingrese email..."></asp:TextBox>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label>¿Visualiza contactos compartido?</label><br />
                        <asp:ListBox ID="ddlFiltro" runat="server" CssClass="selectpicker" SelectionMode="Single" DataValueField="ID" DataTextField="Desc" data-width="175">
                            <asp:ListItem Value="true" Text="Si"></asp:ListItem>
                            <asp:ListItem Value="false" Text="No"></asp:ListItem>
                            <asp:ListItem Value="" Text="Todos" Selected="True"></asp:ListItem>
                        </asp:ListBox>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="pull-right">
                        <br />
                        <button id="btnBuscar" class="btn btn-default" type="button" runat="server" onserverclick="btnBuscar_ServerClick">
                            <span class="fa fa-search"></span>&nbsp;&nbsp;Buscar
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <asp:Label id="lblBuscar" runat="server"></asp:Label>
                <div class="table-responsive small">
                    <asp:GridView ID="gvUsuario" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None" 
                        OnRowCommand="gvUsuario_RowCommand"  OnRowDeleting="gvUsuario_RowDeleting">
                        <Columns>
                            <asp:TemplateField HeaderText="Username" ItemStyle-CssClass="col-lg-2">
                                <ItemTemplate>
                                    <asp:Label ID="lblUsername" runat="server" Text='<%# Eval("UserName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nombre" ItemStyle-CssClass="col-lg-3">
                                <ItemTemplate>
                                    <asp:Label ID="lblNombre" runat="server" Text='<%# string.Format("{0} {1} {2}", Eval("Nombre"), Eval("ApellidoPaterno"), Eval("ApellidoMaterno")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email" ItemStyle-CssClass="col-lg-2">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rol" ItemStyle-CssClass="col-lg-2">
                                <ItemTemplate>
                                    <asp:Label ID="lblRol" runat="server" Text='<%# Eval("Rol.Nombre") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="¿Compartido?" ItemStyle-CssClass="col-lg-1 text-center">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbVisualizaTodoContacto" runat="server" CssClass="checkbox" Enabled="false" Checked='<%# Convert.ToBoolean(Eval("VisualizaTodoContacto")) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-CssClass="col-lg-2 right">
                                <ItemTemplate>
                                    <div class="pull-right">
                                        <asp:Button id="btnEditar" runat="server" Text="Editar" CssClass="btn btn-info btn-xs" CommandName="EditarUsuario" CommandArgument='<%# Container.DataItemIndex %>' />
                                        <asp:Button id="btnEliminar" runat="server" Text="Dar baja" CssClass="btn btn-danger btn-xs" CommandName="Delete" OnClientClick='<%# string.Format("return confirm(\"¿Estás seguro que deseas eliminar al usuario {0} {1} {2}?\");", Eval("Nombre"), Eval("ApellidoPaterno"), Eval("ApellidoMaterno")) %>' />
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>

        <h4 class="page-header">Usuarios de baja</h4>
        <div class="form-group">
            <div class="col-lg-12">
                <div class="row">
                    <asp:Label id="lblBuscarBaja" runat="server"></asp:Label>
                    <div class="table-responsive small">
                        <asp:GridView ID="gvUsuarioBaja" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None" 
                            OnRowDeleting="gvUsuarioBaja_RowDeleting">
                            <Columns>
                                <asp:TemplateField HeaderText="Username" ItemStyle-CssClass="col-lg-2">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUsername" runat="server" Text='<%# Eval("UserName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Nombre" ItemStyle-CssClass="col-lg-3">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNombre" runat="server" Text='<%# string.Format("{0} {1} {2}", Eval("Nombre"), Eval("ApellidoPaterno"), Eval("ApellidoMaterno")) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email" ItemStyle-CssClass="col-lg-2">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-CssClass="col-lg-2 right">
                                    <ItemTemplate>
                                        <div class="pull-right">
                                            <asp:Button id="btnDarAlta" runat="server" Text="Dar alta" CssClass="btn btn-success btn-xs" CommandName="Delete" />
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
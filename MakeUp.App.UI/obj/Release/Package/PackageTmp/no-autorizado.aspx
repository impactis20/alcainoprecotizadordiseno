﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="no-autorizado.aspx.cs" Inherits="MakeUp.App.UI.no_autorizado" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>No autorizado...</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <h2 class="page-header">No autorizado... <i class="fa fa-frown-o" aria-hidden="true"></i></h2>

            <p>Usted no tiene permisos para realizar esta solicitud. Para conceder este requisito solicíteselo a su administrador.</p>
            <asp:Button id="btnGoHome" runat="server" Text="Volver al inicio..." CssClass="btn btn-primary" OnClick="btnGoHome_Click"/>
        </div>
    </form>
</body>
</html>
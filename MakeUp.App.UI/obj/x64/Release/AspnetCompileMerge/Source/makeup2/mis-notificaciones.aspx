﻿<%@ Page Title="Mis notificaciones" Language="C#" MasterPageFile="~/makeup2/menu.master" AutoEventWireup="true" CodeBehind="mis-notificaciones.aspx.cs" Inherits="MakeUp.App.UI.makeup2.mis_notificaciones" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headMenu" runat="server">
    <link href="../css/bootstrap-select.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <h2 class="page-header">Mis notificaciones</h2>
    <div class="form-group">
        <div class="col-lg-12">
            <div class="row">
                <div class="form-inline" style="margin-bottom:15px">
                    <div class="form-group">
                        <asp:CheckBox ID="cbLeidas" runat="server" Text="&nbsp;&nbsp;Visualizar no leídas" CssClass="checkbox" AutoPostBack="true" OnCheckedChanged="cbLeidas_CheckedChanged" />
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="ddlOrden" runat="server" CssClass="selectpicker btn-xs" OnSelectedIndexChanged="ddlOrden_SelectedIndexChanged" AutoPostBack="true" data-width="150px">
                            <asp:ListItem Text="Ascendente" Value="asc"></asp:ListItem>
                            <asp:ListItem Text="Descendente" Value="desc" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                    
                <asp:Label ID="lblInfo" runat="server"></asp:Label>
                <div class="table-responsive">
                    <asp:GridView ID="gvNotificacion" runat="server" AutoGenerateColumns="false" CssClass="table table-responsive" GridLines="None" 
                        OnRowCommand="gvNotificacion_RowCommand" OnRowDataBound="gvNotificacion_RowDataBound">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <i id="icono" runat="server" aria-hidden="true"></i>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-CssClass="col-lg-10" HeaderText="Notificación">
                                <ItemTemplate>
                                    <%--<asp:HyperLink ID="hlNotificacion" runat="server" ></asp:HyperLink>--%>
                                    <asp:LinkButton ID="linkNotificacion" runat="server" CommandName="LeeNotificacion" CommandArgument='<%# Container.DataItemIndex %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-CssClass="col-lg-2" HeaderText="Fecha evento">
                                <ItemTemplate>
                                    <%# string.Format("{0: dd-MM-yyy HH:mm}", Eval("FechaEvento")) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Title="Detalle despacho de factura" Language="C#" MasterPageFile="~/makeup2/menu.master" AutoEventWireup="true" CodeBehind="detalle.aspx.cs" Inherits="MakeUp.App.UI.makeup2.despacho.detalle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headMenu" runat="server">
    <link href="../../css/bootstrap-datetimepicker.css" rel="stylesheet" />
    <link rel="stylesheet" href="../../css/bootstrap-select.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-datetimepicker.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/numeral.min.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/languages/es.js") %>' type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('[id*=dFechaEntrega]').datetimepicker({
                format: 'DD-MM-YYYY',
                maxDate: moment().add(1, 'days'), //permite hasta la fecha de hoy
                showClear: true,
                ignoreReadonly: true
            });
        });

        function ValidateListBox(sender, args) {
            var valorSeleccionado = parseInt($("#<%= lstMetodoEnvio.ClientID %>").val());

            if (valorSeleccionado === -1) {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }       
        }

        function Rut(txtRut) {
            var valor = txtRut.value;
            var parteEntera = valor.split('-')[0];
            var dv = '';

            if (valor.indexOf("-") !== -1)
                dv = '-' + valor.split('-')[1].substring(0, 1);
            //if (howManyRepeated(valor) >= 2) {
            //    valor = valor.substring(0, valor.length - 1);
            //}

            numeral.language('es');
            txtRut.value = numeral(parteEntera.replace(/\./g, '')).format('0,0') + dv;
        }
    </script>

    <h2 id="titulo" runat="server" class="page-header">Detalle despacho de factura</h2>
    
    <div id="contenido" runat="server">
        <div id="envio" runat="server" class="panel panel-default">
            <div class="panel-heading">
                <strong class="text-uppercase">Envío</strong>
            </div>
            <div class="panel-body">
                <p>Los campos con asterisco (*) son obligatorios.</p>
                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label>* Método de envío</label>
                            <asp:ListBox ID="lstMetodoEnvio" runat="server" SelectionMode="Single" CssClass="selectpicker" data-width="100%">
                            </asp:ListBox>
                            <asp:CustomValidator ID="CustomValidator1" runat="server" CssClass="alert-danger" ErrorMessage="Seleccione tipo de envío..." Display="Dynamic" SetFocusOnError="true" ClientValidationFunction="ValidateListBox" ValidationGroup="AgregarDatosEnvio"></asp:CustomValidator>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label>* Entregado por</label>
                            <asp:TextBox ID="txtEntregadoPor" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvEntregadoPor" runat="server" ErrorMessage="Ingrese nombre de quien entrega..." ControlToValidate="txtEntregadoPor" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarDatosEnvio" />
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <label>* Fecha de envío</label>
                        <div class="input-group date input-group" id="dFechaEntrega">
                            <asp:TextBox id="txtFechaEnvio" runat="server" CssClass="form-control input-sm disabled" MaxLength="15"></asp:TextBox>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar"></span>
                            </span>
                        </div>
                        <asp:RequiredFieldValidator ID="rfvFechaEnvio" runat="server" ErrorMessage="Ingrese fecha de envío..." ControlToValidate="txtFechaEnvio" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarDatosEnvio" />
                    </div>
                </div>                
            </div>
            <div id="footerEnvio" runat="server" class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="pull-right">
                            <asp:Button id="btnGuardarEnvio" runat="server" CssClass="btn btn-primary" Text="Guardar" OnClick="btnGuardarEnvio_Click" rel="tooltip" data-toggle="tooltip" data-placement="top" title="Guardar datos de envío de factura." ValidationGroup="AgregarDatosEnvio" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="recepcion" runat="server" class="panel panel-default">
            <div class="panel-heading">
                <b class="text-uppercase">Recepción</b>
            </div>
            <div class="panel-body">
                <p>Los campos con asterisco (*) son obligatorios.</p>                

                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label>Rut</label>
                            <asp:TextBox ID="txtRutRecepcion" runat="server" CssClass="form-control input-sm" Enabled="false" MaxLength="15" autocomplete="off" onkeyup="javascript:Rut(this);"></asp:TextBox>
                            <asp:Label ID="lblRutRecepcion" runat="server" Text="El rut es inválido..." CssClass="alert-danger" Visible="false"></asp:Label>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label>* Recibido por</label>
                            <asp:TextBox ID="txtRecibidoPor" runat="server" CssClass="form-control input-sm" Enabled="false" MaxLength="100" autocomplete="off"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvRecibidoPor" runat="server" ErrorMessage="Ingrese fecha de envío..." ControlToValidate="txtRecibidoPor" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarDatosRecepcion" />
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <label>* Fecha de recepción</label>
                        <div class="input-group date input-group" id="dFechaEntrega1">
                            <asp:TextBox id="txtFechaRecepcion" runat="server" CssClass="form-control input-sm disabled" Enabled="false" MaxLength="15" autocomplete="off"></asp:TextBox>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar"></span>
                            </span>                            
                        </div>
                        <asp:RequiredFieldValidator ID="rfvFechaRecepcion" runat="server" ErrorMessage="Ingrese fecha de envío..." ControlToValidate="txtFechaRecepcion" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarDatosRecepcion" />
                    </div>
                </div>
            </div>
            <div id="footerRecepcion" runat="server" class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="pull-right">
                            <asp:Button id="btnGuardarRecepcion" runat="server" CssClass="btn btn-primary" Text="Guardar" Enabled="false" OnClick="btnGuardarRecepcion_Click" rel="tooltip" data-toggle="tooltip" data-placement="top" title="Guardar datos de recepción." ValidationGroup="AgregarDatosRecepcion" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="pull-right">
                    <asp:HyperLink ID="linkVolver" runat="server" Text="Volver" CssClass="btn-link btn-lg" NavigateUrl="~/makeup2/despacho/factura.aspx" rel="tooltip" data-placement="top" title="Volver al listado de facturas."></asp:HyperLink>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Title="Contactos de proyecto" Language="C#" MasterPageFile="~/makeup2/proyecto/visualizar/proyecto.master" AutoEventWireup="true" CodeBehind="contacto.aspx.cs" Inherits="MakeUp.App.UI.makeup2.proyecto.visualizar.contacto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headProyecto" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenidoProyecto" runat="server">
    <asp:HiddenField ID="hfIdProyecto" runat="server" />
    <asp:HiddenField ID="hfIdCliente" runat="server" />
    <asp:HiddenField ID="hfNombreFantasiaCliente" runat="server" />

    <div class="modal fade" id="x19" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Agregar contacto</h2>
                </div>
                <div class="modal-body">
                    <p>Los campos con asterisco (*) son obligatorios.</p>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>* Nombre Completo</label>
                                <asp:TextBox ID="txtNombreContacto" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvNombreContacto" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtNombreContacto" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarContacto" />
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>* Email</label>
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtEmail" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarContacto" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>* Descripción de contacto</label>
                                <asp:TextBox ID="txtDescripcionContacto" runat="server" CssClass="form-control input-sm" MaxLength="200" autocomplete="off"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvDescripcionContacto" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtDescripcionContacto" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarContacto" />
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>* Teléfono de contacto</label>
                                <asp:TextBox ID="txtTelefono" runat="server" CssClass="form-control input-sm" MaxLength="20" autocomplete="off"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvTelefono" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtTelefono" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarContacto" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Teléfono móvil</label>
                                <asp:TextBox ID="txtMovil" runat="server" CssClass="form-control input-sm" MaxLength="20" autocomplete="off"></asp:TextBox>
                            </div>
                        </div>
                        <div id="compartirContacto" runat="server" class="col-lg-6">
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <div class="col-lg-12" style="margin-top: 20px">
                                        <asp:CheckBox ID="cbCompartirContacto" runat="server" Text="¿Desea compartir usuario?" CssClass="checkbox" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button id="btnAgregarContacto" runat="server" class="btn btn-primary" type="button" validationgroup="AgregarContacto" onserverclick="btnAgregarContacto_ServerClick">
                        <span class="fa fa-user-plus"></span>&nbsp;&nbsp;Agregar nuevo contacto
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <p>Seleccione los contactos que se involucran en este proyecto. Si desea agregar un nuevo contacto haga <a href="#" data-toggle="modal" data-target="#x19">clic aquí</a>.</p>
    
    <%--habilita botón "vincular contactos" cuando detecta un movimiento en los checkbox del gridview gvContacto--%>
    <script type='text/javascript'>
        $(function () {
            $("[id*=cbContacto]").click(function () {
                $("[id*=btnVincularContacto]").removeAttr('disabled');
            });
        });
    </script>

    <div class="table-responsive">
        <asp:GridView ID="gvContacto" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None" ShowFooter="true"
            OnRowDeleting="gvContacto_RowDeleting" OnRowCommand="gvContacto_RowCommand" OnRowEditing="gvContacto_RowEditing" OnRowUpdating="gvContacto_RowUpdating" OnRowCancelingEdit="gvContacto_RowCancelingEdit">
            <Columns>
                <asp:TemplateField HeaderText="Nombre completo">
                    <ItemTemplate>
                        <asp:CheckBox ID="cbContacto" runat="server" CssClass="checkbox" Checked='<%# Eval("VinculadoEnProyecto") %>' Text='<%# Eval("NombreContacto") %>' Style="margin-left: 20px; margin-top: 0px" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtNombreContacto" runat="server" Text='<%# Eval("NombreContacto") %>' CssClass="form-control input-sm" MaxLength="100" autocomplete="off"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Descripción">
                    <ItemTemplate>
                        <asp:Label ID="lblDesc" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtDesc" runat="server" Text='<%# Eval("Descripcion") %>' CssClass="form-control input-sm" MaxLength="200" autocomplete="off"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Email">
                    <ItemTemplate>
                        <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtEmail" runat="server" Text='<%# Eval("Email") %>' CssClass="form-control input-sm" MaxLength="100" autocomplete="off"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Teléfono">
                    <ItemTemplate>
                        <asp:Label ID="lblTelefono" runat="server" Text='<%# Eval("Telefono") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtTelefono" runat="server" Text='<%# Eval("Telefono") %>' CssClass="form-control input-sm" MaxLength="20" autocomplete="off"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Móvil">
                    <ItemTemplate>
                        <asp:Label ID="lblMovil" runat="server" Text='<%# Eval("Movil") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtMovil" runat="server" Text='<%# Eval("Movil") %>' CssClass="form-control input-sm" MaxLength="20" autocomplete="off"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Compartido">
                    <ItemTemplate>
                        <asp:Label ID="lblTipo" runat="server" Text='<%# Convert.ToBoolean(Eval("ContactoCompartido")) == true ? "Compartido" : "No" %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:CheckBox id="cbTipo" runat="server" Checked='<%# Convert.ToBoolean(Eval("ContactoCompartido")) %>' />
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Responsable">
                    <ItemTemplate>
                        <asp:Label ID="lblResponsable" runat="server" Text='<%# string.Format("{0} {1}", Eval("Responsable.Nombre"), Eval("Responsable.ApellidoPaterno")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-CssClass="col-lg-2 right">
                    <ItemTemplate>
                        <div class="pull-right">
                            <asp:Button ID="btnEditar" runat="server" Text="Editar" CssClass="btn btn-info btn-xs" CommandName="Edit" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Editar datos contacto" />
                            <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" CssClass="btn btn-danger btn-xs" CommandName="Delete" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Cancelar operación" OnClientClick='<%# string.Format("return confirm(\"¿Está seguro que desea eliminar a {0} de cliente {1}?\");", Eval("NombreContacto"), hfNombreFantasiaCliente.Value) %>' />
                        </div>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <div class="pull-right">
                            <asp:Button ID="btnActualizar" runat="server" Text="Guardar" CssClass="btn btn-primary btn-xs" CommandName="Update" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Actualizar datos de contacto" />
                            <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn btn-default btn-xs" CommandName="Cancel" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Eliminar contacto" />
                        </div>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <div class="pull-right">
                            <asp:Button ID="btnVincularContacto" runat="server" Text="Vincular contactos" CssClass="btn btn-default" CommandName="GuardarSeleccion" disabled="disabled" />
                        </div>
                    </FooterTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
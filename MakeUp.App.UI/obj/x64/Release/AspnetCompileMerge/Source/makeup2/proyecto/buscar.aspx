﻿<%@ Page Title="Buscar proyecto" Language="C#" MasterPageFile="~/makeup2/menu.master" AutoEventWireup="true" CodeBehind="buscar.aspx.cs" Inherits="MakeUp.App.UI.makeup2.proyecto.buscar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headMenu" runat="server">
    <link href="../../css/bootstrap-typeahead.css" rel="stylesheet" />
    <link rel="stylesheet" href="../../css/bootstrap-select.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/autocomplete-query.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap3-typeahead.min.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            AutoComplete("<%= txtCliente.ClientID %>", '<%= ResolveUrl(string.Format("{0}/GetNombreFantasia", Request.Url.AbsolutePath)) %>');
            AutoComplete("<%= txtNombre.ClientID %>", '<%= ResolveUrl(string.Format("{0}/GetNombreProyecto", Request.Url.AbsolutePath)) %>');
        });
    </script>
    <h2 class="page-header">Buscar proyecto</h2>
    <p>Puede buscar un proyecto filtrando por ID, nombre de proyecto, cliente y/o estado por estado de Notas de pedido.</p>
    <div class="form-group">
        <div class="row">
            <div class="col-lg-1">
                <div class="form-group">
                    <label>ID</label>
                    <asp:TextBox ID="txtID" runat="server" CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label>Nombre proyecto</label>
                    <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label>Cliente <small>(nombre fantasía)</small></label>
                    <asp:TextBox ID="txtCliente" runat="server" CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label>Filtros (*)</label><br />
                    <asp:ListBox ID="lstEstadoNP" runat="server" SelectionMode="Multiple" CssClass="selectpicker" data-width="100%" data-selected-text-format="count > 3" data-actions-box="true">
                    </asp:ListBox>
                    <asp:RequiredFieldValidator ID="rfvEstadoNP" runat="server" ErrorMessage="Seleccione para buscar..." ControlToValidate="lstEstadoNP" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Buscar" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="pull-right">
                    <br />
                    <div class="btn-group">
                        <button id="btnBuscar" runat="server" class="btn btn-primary" type="button" onserverclick="btnBuscar_ServerClick" validationgroup="Buscar" >
                            <span class="fa fa-search"></span>&nbsp;&nbsp;Buscar (*)
                        </button>
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu">
                          <li><asp:LinkButton ID="lnkPorRegularizar" runat="server" Text="Por regularizar" OnClick="lnkPorRegularizar_Click" rel="tooltip" data-toggle="tooltip" data-placement="left" title="Busca proyectos que están facturados mediante Estado de Pago al 100% y sus nota(s) de pedido no están valorizadas."></asp:LinkButton></li>
                          <li><asp:LinkButton ID="lnkEliminados" runat="server" Text="Eliminados" OnClick="lnkEliminados_Click"  rel="tooltip" data-toggle="tooltip" data-placement="left" title="Busca proyectos que fueron eliminados."></asp:LinkButton></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-12">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <ul class="list-inline" style="margin-bottom:0px">
                            <li id="eliminado" runat="server" visible="false"><i class="fa fa-trash text-danger" style="margin-left: 4px;margin-right: 4px;"></i> Proyecto eliminado</li>
                            <li id="porRegularizar" runat="server" visible="false"><i class="fa fa-exclamation-triangle text-warning" style="margin-left: 4px;margin-right: 4px;"></i> Proyecto por regularizar</li>
                            <li><label class="label label-danger">&nbsp;&nbsp;</label> Creada</li>
                            <li><label class="label label-warning">&nbsp;&nbsp;</label> Ajustada</li>
                            <li><label class="label label-success">&nbsp;&nbsp;</label> Valorizada</li>
                            <li><label class="label label-primary">&nbsp;&nbsp;</label> Facturada</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-12">
            <div class="row">
                <asp:Label ID="lblBuscar" runat="server"></asp:Label>
                <div class="table-responsive">
                    <asp:GridView ID="gvProyecto" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-responsive" GridLines="None" OnRowDataBound="gvProyecto_RowDataBound" >
                        <Columns>
                            <asp:TemplateField HeaderText="ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NP">
                                <ItemTemplate>
                                    <asp:Label ID="lblContNP" runat="server" Text='<%# Eval("CantidadTotalNP") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nombre Proyecto">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkProyecto" runat="server"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cliente <small>(nombre fantasía)</small>">
                                <ItemTemplate>
                                    <asp:Label ID="lblCliente" runat="server" Text='<%# Eval("Cliente.NombreFantasia") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ejecutivo Comercial">
                                <ItemTemplate>
                                    <asp:Label ID="lblEjecutivo" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Estados NP">
                                <ItemTemplate>
                                    <asp:Label ID="lblPorAjustar" runat="server" CssClass="label" rel="tooltip" data-toggle="tooltip" data-placement="top"></asp:Label>
                                    <asp:Label ID="lblPorValorizar" runat="server" CssClass="label" rel="tooltip" data-toggle="tooltip" data-placement="top"></asp:Label>
                                    <asp:Label ID="lblPorFacturar" runat="server" CssClass="label" rel="tooltip" data-toggle="tooltip" data-placement="top"></asp:Label>
                                    <asp:Label ID="lblFacturado" runat="server" CssClass="label" rel="tooltip" data-toggle="tooltip" data-placement="top"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fecha Creación">
                                <ItemTemplate>
                                    <asp:Label ID="lblFechaCreacion" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fecha Planificada">
                                <ItemTemplate>
                                    <asp:Label ID="lblFechaPlanificada" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
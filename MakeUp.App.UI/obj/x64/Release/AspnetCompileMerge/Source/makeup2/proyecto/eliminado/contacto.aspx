﻿<%@ Page Title="Contactos de proyecto (eliminado)" Language="C#" MasterPageFile="~/makeup2/proyecto/eliminado/proyecto.master" AutoEventWireup="true" CodeBehind="contacto.aspx.cs" Inherits="MakeUp.App.UI.makeup2.proyecto.eliminado.contacto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headProyecto" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenidoProyecto" runat="server">
    <%--habilita botón "vincular contactos" cuando detecta un movimiento en los checkbox del gridview gvContacto--%>
    <script type='text/javascript'>
        $(function () {
            $("[id*=cbContacto]").click(function () {
                $("[id*=btnVincularContacto]").removeAttr('disabled');
            });
        });
    </script>

    <p>Contactos vinculados en el proyecto.</p>
    
    <div class="table-responsive">
        <asp:GridView ID="gvContacto" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None" ShowFooter="true" 
            OnRowCommand="gvContacto_RowCommand">
            <Columns>
                <asp:TemplateField HeaderText="Nombre completo">
                    <ItemTemplate>
                        <asp:CheckBox ID="cbContacto" runat="server" CssClass="checkbox" Checked='<%# Eval("VinculadoEnProyecto") %>' Text='<%# Eval("NombreContacto") %>' Enabled="false" Style="margin-left: 20px; margin-top: 0px" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtContacto" runat="server" CssClass="form-control input-sm" MaxLength="100"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Descripción">
                    <ItemTemplate>
                        <asp:Label ID="lblDesc" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtDesc" runat="server" CssClass="form-control input-sm" MaxLength="200"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Email">
                    <ItemTemplate>
                        <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control input-sm" MaxLength="100"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Teléfono">
                    <ItemTemplate>
                        <asp:Label ID="lblTelefono" runat="server" Text='<%# Eval("Telefono") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtTelefono" runat="server" CssClass="form-control input-sm" MaxLength="20"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Móvil">
                    <ItemTemplate>
                        <asp:Label ID="lblMovil" runat="server" Text='<%# Eval("Movil") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtMovil" runat="server" CssClass="form-control input-sm" MaxLength="20"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Compartido">
                    <ItemTemplate>
                        <asp:Label ID="lblTipo" runat="server" Text='<%# Convert.ToBoolean(Eval("ContactoCompartido")) == true ? "Compartido" : "No" %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:CheckBox ID="cbTipo" runat="server" CssClass="checkbox" Style="margin-left: 20px; margin-top: 0px" />
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Responsable">
                    <ItemTemplate>
                        <asp:Label ID="lblResponsable" runat="server" Text='<%# string.Format("{0} {1}", Eval("Responsable.Nombre"), Eval("Responsable.ApellidoPaterno")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
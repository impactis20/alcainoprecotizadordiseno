﻿<%@ Page Title="Crear Proyecto" Language="C#" MasterPageFile="~/makeup2/menu.master" AutoEventWireup="true" CodeBehind="crear.aspx.cs" Inherits="MakeUp.App.UI.makeup2.proyecto.crear" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headMenu" runat="server">
    <link href="../../css/bootstrap-datetimepicker.css" rel="stylesheet" />
    <link href="../../css/bootstrap-select.css" rel="stylesheet" />
    <link href="../../css/fileinput.min.css" rel="stylesheet" />
    <link href="../../css/bootstrap-typeahead.css" rel="stylesheet" />
    <link href="../../css/personalizado.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-datetimepicker.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/fileinput.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/autocomplete-query.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap3-typeahead.min.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/languages/es.js") %>'></script>    
    <script src='<%= ResolveUrl("~/js/personalizado/count-character-textbox.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/number-format-using-numeral.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/dropdown-igualitario.js") %>'></script>

    <script type="text/javascript">
        $(function () {
            $('[id*=dFechaEntrega]').datetimepicker({
                useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                format: 'DD-MM-YYYY',
                minDate: 'now',
                showClear: true,
                ignoreReadonly: true
            });

            //Copiar la fecha comprometida a la fecha planificada
            $("#" + "<%= txtFechaComprometida.ClientID %>").on('blur change keyup paste', function () {
                $("#" + "<%= txtFechaPlanificada.ClientID %>").val($("#" + "<%= txtFechaComprometida.ClientID %>").val());
            });

            //al cerrar el modal "Agregar contacto", verifica si quedó algún RequiredFieldValidator activado, para desactivarla
            $('[id*=x19]').on('hidden.bs.modal', function (e) {
                var allListElements = $(".alert-danger");
                $('#' + this.id).find(allListElements).each(function (i, obj) {
                    this.isvalid = true; //establece a válido cada RequiredFieldValidator
                    $('#' + this.id).hide(); //oculta cada RequiredFieldValidator
                });
            });
        });
    </script>

    <script type="text/javascript">
        //realiza cálculo de valores de una NP al agregar o editar, ya sea ingresando cantidad o valor unitario de una NP
        function ValorTotalNP(txtCantidad, txtValorUnitario, txtTotalNP, ddlTipoValor1, txtTotalProyecto, ddlTipovalor2, editaNP) {
            var Cantidad = ConvertToNumber(document.getElementById(txtCantidad).value);
            var Unitario = ConvertToNumber(document.getElementById(txtValorUnitario).value);
            var ddlCountItems = $("#" + ddlTipoValor1).children().length;

            if (Cantidad === "" || Unitario === "") {
                document.getElementById(txtTotalNP).value = "0";
            }
            else {
                var TotalNP = Cantidad * Unitario;// Math.round(Cantidad * Unitario);
                var ActualTotalProyecto = ConvertToNumber('<%= hfValorTotalProyecto.Value %>');
                var NuevoTotalProyecto = 0;

                if (editaNP === "True") {
                    NuevoTotalProyecto = ActualTotalProyecto - ConvertToNumber('<%= hfValorNPAntesDeSerEditada.Value %>');
                    NuevoTotalProyecto += TotalNP;
                }
                else {
                    //cuando agrega una nueva NP
                    NuevoTotalProyecto = parseFloat(ActualTotalProyecto) + TotalNP;
                }

                var valorItemSeleccionadoTotalNP = parseFloat(ConvertToNumber(document.getElementById(ddlTipoValor1).value));
                var valorItemSeleccionadoTotalProyecto = parseFloat(ConvertToNumber(document.getElementById(ddlTipovalor2).value));

                numeral.language('es');

                //si es igual a 1 NO es tipo de moneda CLP
                if (valorItemSeleccionadoTotalNP === 1 && ddlCountItems > 1) {
                    document.getElementById(txtTotalNP).value = numeral(TotalNP * valorItemSeleccionadoTotalNP).format('0,0.[00]'); //asigna valor total NP dependiendo del tipo de valor seleccionado
                    document.getElementById(txtTotalProyecto).value = numeral(NuevoTotalProyecto * valorItemSeleccionadoTotalProyecto).format('0,0.[00]'); //asigna valor total Proyecto dependiendo del tipo de valor seleccionadoo de valor seleccionado
                }
                else {
                    document.getElementById(txtTotalNP).value = numeral(TotalNP * valorItemSeleccionadoTotalNP).format('0,0'); //asigna valor total NP dependiendo del tipo de valor seleccionado
                    document.getElementById(txtTotalProyecto).value = numeral(NuevoTotalProyecto * valorItemSeleccionadoTotalProyecto).format('0,0'); //asigna valor total Proyecto dependiendo del tip
                }
            }
        }

        //traduce valores de una NP a pesos chilenos cuando agrega una NP o edita una NP.
        //Para que este evento se ejecute se debe cambiar el dropdownlist que va acompañado de los textboxs "Valor Total NP" o "Valor Total Proyecto"
        function NPValoresACLP(ddl, txtCantidad, txtValorUnitario, txtValorTotalNP, txtValorTotalProyecto, editaNP) {
            var valorItemSeleccionado = parseFloat(ConvertToNumber(document.getElementById(ddl.id).value));
            var ddlCountItems = $("#" + ddl.id).children().length;
            var ValorTotalProyecto = ConvertToNumber('<%= hfValorTotalProyecto.Value %>');
            
            var Cantidad = ConvertToNumber(document.getElementById(txtCantidad).value);
            var Unitario = ConvertToNumber(document.getElementById(txtValorUnitario).value);
            var ValorTotalNP = Cantidad * Unitario; //cálculo para adicionar el valor total del proyecto + los valores de txtCantidad y txtValorUnitarioNP

            numeral.language('es');

            if (editaNP === "True")
                ValorTotalProyecto = parseFloat(ValorTotalProyecto) - parseFloat(ConvertToNumber('<%= hfValorNPAntesDeSerEditada.Value %>'));            

            ValorTotalProyecto = parseFloat(ValorTotalProyecto) + ValorTotalNP;

            //si es igual a 1 NO es tipo de moneda CLP
            if (valorItemSeleccionado === 1 && ddlCountItems > 1) {
                document.getElementById(txtValorTotalNP).value = numeral(ValorTotalNP * valorItemSeleccionado).format('0,0.[00]'); //Asigna valor a txtValorTotalNP
                document.getElementById(txtValorTotalProyecto).value = numeral(ValorTotalProyecto * valorItemSeleccionado).format('0,0.[00]'); //Asigna valor a txtValorTotalProyecto
            }
            else {
                document.getElementById(txtValorTotalNP).value = numeral(ValorTotalNP * valorItemSeleccionado).format('0,0'); //Asigna valor a txtValorTotalNP
                document.getElementById(txtValorTotalProyecto).value = numeral(ValorTotalProyecto * valorItemSeleccionado).format('0,0'); //Asigna valor a txtValorTotalProyecto
            }
        }

        //calcula el monto del EP en base a un porcentaje
        function CalculaMontoEP(Porcentaje) {
            if (Porcentaje !== '') {
                var txtPorcentaje = document.getElementById("<%= txtPorcentaje.ClientID %>");
                var txtMonto = document.getElementById("<%= txtMonto.ClientID %>");
                var ValorTotalProyecto = parseFloat(ConvertToNumber('<%= hfValorTotalProyecto.Value %>'));

                Porcentaje = parseFloat(ConvertToNumber(Porcentaje));

                if (Porcentaje > 100 || Porcentaje === 0) {
                    alerta('Porcentaje inválido...', 'El porcentaje no puede exceder el máximo permitido (100%), tampoco puede ser igual a 0.', 'text-danger');
                    txtPorcentaje.value = "";
                    txtMonto.value = "";
                    return;
                }
                else {
                    numeral.language('es');
                    var ValorMonto = ValorTotalProyecto * Porcentaje / 100;
                    txtMonto.value = numeral(ValorMonto).format('0,0' + getCantidadDecimal()); //[00] dos decimales
                    document.getElementById('<%= hfMonto.ClientID %>').value = ValorMonto; //almacena monto
                    document.getElementById('<%= hfPorcentaje.ClientID %>').value = Porcentaje; //almacena monto
                }
            }
        }

        //calcula el porcentaje del EP en base a un monto
        function CalculaPorcentajeEP(Monto) {
            if (Monto !== '') {
                var txtPorcentaje = document.getElementById("<%= txtPorcentaje.ClientID %>");
                var txtMonto = document.getElementById("<%= txtMonto.ClientID %>");
                var ValorTotalProyecto = parseFloat(ConvertToNumber('<%= hfValorTotalProyecto.Value %>'));

                Monto = parseFloat(ConvertToNumber(Monto));

                if (Monto > ValorTotalProyecto || Monto === 0) {
                    alerta('Monto inválido...', 'El monto no puede exceder el Saldo del proyecto, tampoco puede ser igual a 0.', 'text-danger');
                    txtPorcentaje.value = "";
                    txtMonto.value = "";
                    return;
                }
                else {
                    numeral.language('es');
                    var ValorPorcentaje = Monto / ValorTotalProyecto * 100;



                    txtPorcentaje.value = numeral(ValorPorcentaje).format('0,0.[00]'); //porcentaje debe basarse en 2 decimales
                    document.getElementById('<%= hfMonto.ClientID %>').value = Monto; //almacena monto
                    document.getElementById('<%= hfPorcentaje.ClientID %>').value = ValorPorcentaje; //almacena porcentaje
                }
            }
        }

        function getCantidadDecimal() {
            var valorTipoValor = parseInt(document.getElementById('<%= hfValorTipoValor.ClientID %>').value);
            if (valorTipoValor > 1)
                return '.[00]'; //[00] dos decimales

            return '';
        }

    </script>

    <script type="text/javascript">
        $(function () {
            $(".btn, a").click(function () {
                var contAlert = 0;
                var contValido = 0;
                //verifica que ningún label alert-danger esté activo
                $('.alert-danger').each(function (i, obj) {
                    var esValido = this.isvalid;
                    contAlert++;
                    if(esValido){
                        contValido++;
                    }
                });
                
                //verifica cuando el evento "window.onbeforeunload" debe ser nulo
                if(contValido === contAlert && //si no hay alertas activadas
                    this.className.indexOf('btn-file') == -1 && //si no es el botón de adjuntar archivos
                    this.nodeName.toLowerCase() !== 'a' && // si no es un elemento a
                    this.getAttribute('data-dismiss') !== 'modal') //si no es un botón "Cerrar" correspondiente a un modal
                    window.onbeforeunload = null;
            });
            
            window.onbeforeunload = function (event) {
                if(isNotTxtEmpty(<%= txtNombre.ClientID %>) || 
                    isNotTxtEmpty(<%= txtPadre.ClientID %>) || 
                    isNotTxtEmpty(<%= txtCliente.ClientID %>) || 
                    isNotTxtEmpty(<%= txtEjecutivo.ClientID %>) || 
                    isNotTxtEmpty(<%= txtDireccion.ClientID %>) ||
                    isNotTxtEmpty(<%= txtComuna.ClientID %>)){
                    var message = "Si abandonas perderás todos los cambios realizados.";

                    if (typeof event == 'undefined') {
                        event = window.event;
                    }
                    if (event) {
                        event.returnValue = message;
                    }
                    return message;
                }
            };
        });

        //verifica si el textbox está vacío
        function isNotTxtEmpty(idTxt){
            var str = document.getElementById(idTxt.id).value;
            return str.replace(/^\s+/g, '').length; // boolean (`true` if field is empty)
        }
    </script>

    <script type="text/javascript">
        //almacena último estado de la pestaña del proyecto en creación
        var idLocalStorage = 'lastTab' + <%= hfIDProyecto.Value %>;

        //guarda el estado de las pestañas
        $(function () {
            // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // save the latest tab; use cookies if you like 'em better:
                sessionStorage.setItem(idLocalStorage, $(this).attr('href'));
            });

            // go to the latest tab, if it exists:
            var lastTab = sessionStorage.getItem(idLocalStorage);
            if (lastTab) {
                $('[href="' + lastTab + '"]').tab('show');
            }
        });

        //funcion llamada cuando finaliza el proyecto..
        //elimina la ultima pestaña almacenada
        function delStateTab() {
            sessionStorage.removeItem(idLocalStorage);
        }        

        //guarda el estado del acordión
        $(function () {
            var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";
             
            //Remove the previous selected Pane.
            $("[id*=accordion] .in").removeClass("in");
             
            //Set the selected Pane.
            $("#" + paneName).collapse("show");
             
            //When Pane is clicked, save the ID to the Hidden Field.
            $(".panel-heading a").click(function () {
                $("[id*=PaneName]").val($(this).attr("href").replace("#", ""));
            });
        });
    </script>

    <script type="text/javascript">
        $(function () {
            AutoCompleteConValor("<%= txtCliente.ClientID %>", "<%= hfCliente.ClientID %>", '<%= ResolveUrl(string.Format("{0}/GetCliente", Request.Url.AbsolutePath)) %>');
            AutoCompleteConValor("<%= txtPadre.ClientID %>", "<%= hfPadre.ClientID %>", '<%= ResolveUrl(string.Format("{0}/GetProyectoPadre", Request.Url.AbsolutePath)) %>');
            AutoCompleteConValor("<%= txtEjecutivo.ClientID %>", "<%= hfEjecutivo.ClientID %>", '<%= ResolveUrl(string.Format("{0}/GetEjecutivoComercial", Request.Url.AbsolutePath)) %>');
        });
    </script>
    
    <script type="text/javascript">
        //Habilita y deshabilita textbox
        
            $(document).ready(function () {
                $("input[type=radio][name*=TipoEP]").change(function () {
                    if (this.value == 'rbPorcentaje') {
                        $('[id*=<%= txtPorcentaje.ClientID %>]').removeAttr('disabled');
                        $('[id*=<%= txtMonto.ClientID %>]').attr('disabled', 'disabled');

                        //$('[id*=<%= txtPorcentaje.ClientID %>]').focus();
                        document.getElementById("<%= txtPorcentaje.ClientID %>").focus();
                    }
                    else {
                        $('[id*=<%= txtMonto.ClientID %>]').removeAttr('disabled');
                        $('[id*=<%= txtPorcentaje.ClientID %>]').attr('disabled', 'disabled');

                        //$('[id*=<%= txtMonto.ClientID %>]').focus();
                        document.getElementById("<%= txtMonto.ClientID %>").focus();
                    }

                    $('[id*=<%= txtPorcentaje.ClientID %>]').val('');
                    $('[id*=<%= txtMonto.ClientID %>]').val('');
                })
            });
        
    </script>

    <asp:HiddenField ID="hfIDProyecto" runat="server" />
    <asp:HiddenField ID="hfPorcentaje" runat="server" />
    <asp:HiddenField ID="hfMonto" runat="server" />
    <asp:HiddenField ID="hfValorNPAntesDeSerEditada" runat="server" />
    <asp:HiddenField ID="hfValorTipoValor" runat="server" />

    <h2 class="page-header">Crear proyecto: <var id="titulo" runat="server"></var></h2>
    <div id="contenido" runat="server">
        <p>Complete el siguiente formulario para crear un proyecto. Recuerde que debe ingresar una Nota de Pedido como mínimo. Completado el formulario presione el botón
        <label class="label label-primary">Crear proyecto</label>.</p>

        <div class="row">
            <div id="TabsNP" class="col-lg-12" role="tabpanel">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#DatosProyecto" aria-controls="DatosProyecto" role="tab" data-toggle="tab">Datos de proyecto</a></li>
                    <li><a id="tabNP" runat="server" href="#NotasDePedido" aria-controls="NotasDePedido" role="tab" data-toggle="tab" visible="false">Notas de pedido</a></li>
                    <li><a id="tabEP" runat="server" href="#EstadoPago" aria-controls="EstadoPago" role="tab" data-toggle="tab" visible="false">Estado de pago</a></li>
                    <li><a id="tabContacto" runat="server" href="#Contactos" aria-controls="Contactos" role="tab" data-toggle="tab" visible="false">Contactos</a></li>
                </ul>

                <asp:HiddenField ID="PaneName" runat="server" Value="" />

                <div class="tab-content">
                    <div id="DatosProyecto" role="tabpanel" class="tab-pane fade in active">

                        <div id="confirmarCrearProyecto" runat="server" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="xxxx">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h2 class="modal-title">Confirmar proyecto</h2>
                                    </div>
                                    <div class="modal-body">
                                        ¿Está seguro(a) de proceder sin asignar un proyecto padre?
                                    </div>
                                    <div class="modal-footer">
                                        <asp:Button ID="btnReconfirmarProyecto" runat="server" Text="Aceptar" CssClass="btn btn-primary" OnClick="btnReconfirmarProyecto_Click" />
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <p>Los campos con asterisco (*) son obligatorios.</p>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>ID Proyecto</label>
                                        <asp:TextBox ID="txtIDProyecto" runat="server" CssClass="form-control input-sm" ReadOnly="true" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Proyecto padre</label>
                                        <asp:TextBox ID="txtPadre" runat="server" Text="" CssClass="form-control input-sm" placeholder="Ingrese ID del proyecto padre..." autocomplete="off"></asp:TextBox>
                                        <asp:Label ID="lblPadre" runat="server" CssClass="alert-danger" Text="Seleccione un <b>Proyecto</b> de la lista..." Visible="false"></asp:Label>
                                        <asp:HiddenField ID="hfPadre" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>* Nombre proyecto</label>
                                        <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvNombre" runat="server" ErrorMessage="Campo <b>Nombre proyecto</b> es obligatorio..." ControlToValidate="txtNombre" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="DatosGenerales" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>* Cliente <small>(nombre fantasía)</small></label>
                                        <a class="pull-right" href="../admin/cliente/index.aspx" target="_blank">Agregar nuevo cliente</a>
                                        <asp:TextBox ID="txtCliente" runat="server" CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvCliente" runat="server" ErrorMessage="Seleccione un <b>Cliente</b> de la lista..." ControlToValidate="txtCliente" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="DatosGenerales" />
                                        <asp:HiddenField ID="hfCliente" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Fecha de ingreso</label>
                                        <asp:TextBox ID="txtFechaIngreso" runat="server" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>* Fecha comprometida <small>(Comercial)</small></label>
                                                <div class="input-group date input-group" id="dFechaEntrega">
                                                    <asp:TextBox ID="txtFechaComprometida" runat="server" CssClass="form-control input-sm disabled"></asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                </div>
                                                <asp:RequiredFieldValidator ID="rfvFechaComprometida" runat="server" ErrorMessage="Campo <b>Fecha Comprometida</b> es obligatorio..." ControlToValidate="txtFechaComprometida" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="DatosGenerales" />
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Fecha planificada <small>(Producción)</small></label>
                                                <asp:TextBox ID="txtFechaPlanificada" runat="server" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>* Ejecutivo comercial</label>
                                        <asp:TextBox ID="txtEjecutivo" runat="server" CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvEjecutivo" runat="server" ErrorMessage="Seleccione un <b>Ejecutivo comercial</b> de la lista..." ControlToValidate="txtEjecutivo" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="DatosGenerales" />
                                        <asp:HiddenField ID="hfEjecutivo" runat="server" />
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>Divisa o tipo de valor</label>
                                        <asp:DropDownList ID="ddlTipoValor" runat="server" CssClass="selectpicker" DataValueField="ID" DataTextField="sigla" data-width="100%">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvTipoValor" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="ddlTipoValor" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="DatosGenerales" />
                                    </div>
                                </div>
                                <div id="datosTotalProyecto" runat="server" class="col-lg-3" visible="false">
                                    <div class="form-group">
                                        <label>
                                            Valor proyecto
                                        </label>
                                        <div class="input-group">
                                            <div class="input-group-btn">
                                                <asp:DropDownList id="ddlTipoValor1" runat="server" CssClass="selectpicker dtv1" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                </asp:DropDownList>
                                            </div>
                                            <asp:TextBox ID="txtValorProyecto" runat="server" Text="0" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                            <asp:HiddenField ID="hfValorTotalProyecto" runat="server" Value="0" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>* Dirección del proyecto</label>
                                        <asp:TextBox ID="txtDireccion" runat="server" CssClass="form-control input-sm" MaxLength="200" autocomplete="off"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvDireccion" runat="server" ErrorMessage="Campo <b>Dirección</b> es obligatorio..." ControlToValidate="txtDireccion" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="DatosGenerales" />
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>* Comuna</label>
                                        <asp:TextBox ID="txtComuna" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvComuna" runat="server" ErrorMessage="Campo <b>Comuna</b> es obligatorio..." ControlToValidate="txtComuna" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="DatosGenerales" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <br />
                                        <button id="btnConfirmarProyecto" class="btn btn-default" type="button" runat="server" rel="tooltip" data-toggle="tooltip" data-placement="top" title="Confirmar datos principales del proyecto" onserverclick="btnConfirmarProyecto_ServerClick" validationgroup="DatosGenerales">
                                            <span class="fa fa-check"></span>&nbsp;&nbsp;Confirmar proyecto
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="DocComercial" runat="server" class="panel panel-default" visible="false">
                            <div class="panel-heading">
                                <b class="text-uppercase">* Adjuntar documentos comerciales (Obligatorio)</b>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div id="formDocComercial" runat="server" class="row">
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label>* Tipo <abbr class="initialism" title="documento">doc.</abbr></label>
                                                <asp:DropDownList ID="ddlTipoDocumento" runat="server" CssClass="selectpicker" DataValueField="ID" DataTextField="Nombre" data-width="100%"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div id="Div45" runat="server" class="form-group">
                                                <label>* Descripción</label>
                                                <asp:TextBox ID="txtDescripcionDoc" runat="server" CssClass="form-control input-sm" TextMode="MultiLine" Rows="1" MaxLength="30" placeholder="Ingrese descripción del documento..."></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvDescripcionDoc" runat="server" ErrorMessage="Campo <b>Descripción</b> es obligatorio..." ControlToValidate="txtDescripcionDoc" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregaDocComercial" />
                                                <asp:Label ID="lblContDescDoc" runat="server" CssClass="help-block pull-right"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>* Archivo a subir</label>
                                                <asp:FileUpload ID="fArchivo" runat="server" CssClass="file input-sm" AllowMultiple="false" data-show-upload="false" data-show-caption="true" />
                                                <asp:RequiredFieldValidator ID="rfvArchivo" runat="server" ErrorMessage="Campo <b>Archivo a subir</b> es obligatorio..." ControlToValidate="fArchivo" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregaDocComercial" />
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <div class="pull-right">
                                                    <br />
                                                    <button id="btnAgregarDocComercial" runat="server" class="btn btn-default btn-sm" type="button" style="margin-top: 5px" onserverclick="btnAgregarDocComercial_ServerClick" validationgroup="AgregaDocComercial">
                                                        <i class="fa fa-plus"></i>&nbsp;&nbsp;Agregar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <asp:Label ID="lblRegistroDC" runat="server"></asp:Label><asp:Label ID="lblDirectorioDC" runat="server" class="alert-info"></asp:Label>
                                            <div class="table-responsive">
                                                <asp:GridView ID="gvDocComercial" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None"
                                                    OnRowDataBound="gvDocComercial_RowDataBound" OnRowEditing="gvDocComercial_RowEditing" OnRowUpdating="gvDocComercial_RowUpdating" OnRowCancelingEdit="gvDocComercial_RowCancelingEdit" OnRowDeleting="gvDocComercial_RowDeleting">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Tipo archivo" ItemStyle-CssClass="col-lg-2">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTipoArchivo" runat="server" Text='<%# Eval("TipoDocumento.Nombre") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:DropDownList ID="ddlTipoDocumento" runat="server" CssClass="selectpicker" DataValueField="ID" DataTextField="Nombre" data-width="100%"></asp:DropDownList>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Descripción" ItemStyle-CssClass="col-lg-4">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDesc" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtDesc" runat="server" Text='<%# Eval("Descripcion") %>' CssClass="form-control input-sm" TextMode="MultiLine" Rows="1" MaxLength="30" placeholder="Ingrese descripción del documento..."></asp:TextBox>
                                                                <asp:Label ID="lblContDesc" runat="server" CssClass="help-block pull-right"></asp:Label>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Archivo" ItemStyle-CssClass="col-lg-4">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblArchivo" runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-CssClass="col-lg-2 right">
                                                            <ItemTemplate>
                                                                <div class="pull-right">
                                                                    <asp:Button ID="btnEditar" runat="server" Text="Editar" CssClass="btn btn-info btn-xs" CommandName="Edit" />
                                                                    <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" CssClass="btn btn-danger btn-xs" CommandName="Delete" OnClientClick='<%# string.Format("return confirm(\"¿Está seguro que desea eliminar {0}?\");", Eval("Nombre")) %>' />
                                                                </div>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <div class="pull-right">
                                                                    <asp:Button ID="btnEditar" runat="server" Text="Guardar" CssClass="btn btn-primary btn-xs" CommandName="Update" />
                                                                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn btn-default btn-xs" CommandName="Cancel" />
                                                                </div>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="DocProyecto" runat="server" class="panel panel-default" visible="false">
                            <div class="panel-heading">
                                <b class="text-uppercase">Acceso a documentos de proyecto</b>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-5">
                                            <div class="form-group">
                                                <label>* Ruta</label>
                                                <asp:TextBox ID="txtRutaArchivo" runat="server" CssClass="form-control input-sm" placeholder="Ingrese la ruta del archivo..." MaxLength="200" autocomplete="off"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvRutaArchivo" runat="server" ErrorMessage="Campo <b>Ruta</b> es obligatorio..." ControlToValidate="txtRutaArchivo" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregaDocProyecto" />
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="form-group">
                                                <label>* Descripción</label>
                                                <asp:TextBox ID="txtDescripcionArchivo" runat="server" CssClass="form-control input-sm" placeholder="Ingrese descripción del archivo..." TextMode="MultiLine" Rows="1"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvDescArchivo" runat="server" ErrorMessage="Campo <b>Descripción</b> es obligatorio..." ControlToValidate="txtDescripcionArchivo" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregaDocProyecto" />
                                                <asp:Label ID="lblContDA" runat="server" CssClass="help-block pull-right"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <div class="pull-right">
                                                    <br />
                                                    <button id="btnAgregarDocProyecto" runat="server" class="btn btn-default btn-sm" type="button" style="margin-top: 5px" onserverclick="btnAgregarDocProyecto_ServerClick" validationgroup="AgregaDocProyecto">
                                                        <i class="fa fa-plus"></i>&nbsp;&nbsp;Agregar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="row">
                                        <asp:Label ID="lblRegistroDP" runat="server"></asp:Label>
                                        <div class="table-responsive">
                                            <asp:GridView ID="gvDocProyecto" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None"
                                                OnRowDeleting="gvDocProyecto_RowDeleting">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Ruta">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRuta" runat="server" Text='<%# Eval("Url") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Descripción">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDesc" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-CssClass="col-lg-2 right">
                                                        <ItemTemplate>
                                                            <div class="pull-right">
                                                                <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" CssClass="btn btn-danger btn-xs" CommandName="Delete" OnClientClick='<%# string.Format("return confirm(\"¿Está seguro que desea eliminar la ruta a documentos {0}?\");", Eval("Url")) %>' />
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="NotasDePedido">

                        <div class="modal fade" id="EPConfirmaAgregarNP" tabindex="-1" role="dialog" aria-labelledby="xxxx">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h2 class="modal-title">Confirmar Agregar NP</h2>
                                    </div>
                                    <div class="modal-body">
                                        <div class="panel panel-danger" visible="false">
                                            <div class="panel-heading">
                                                Existe un Estado de Pago <b>no facturado</b>...
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label>¿Desea modificar el Monto?</label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="ddlOperacionEPAgregar" runat="server" CssClass="selectpicker" DataValueField="ID" DataTextField="nombre" data-width="65px">
                                                                <asp:ListItem Value="monto" Text="Si"></asp:ListItem>
                                                                <asp:ListItem Value="porce" Text="No"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-lg-3">
                                                            <label>Fecha Ingreso</label><br />
                                                            <asp:Label ID="lblFechaIngresoEP" runat="server"></asp:Label>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <label>Porcentaje</label><br />
                                                            <asp:Label ID="lblPorcentajeEP" runat="server"></asp:Label>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <label>Monto</label><br />
                                                            <asp:Label ID="lblMontoEP" runat="server"></asp:Label>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <label>Glosa</label><br />
                                                            <asp:Label ID="lblGlosaEP" runat="server"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="pull-right">
                                                    <button id="btnEPConfirmarAgregarNP" runat="server" class="btn btn-primary" type="button" onserverclick="btnEPConfirmarAgregarNP_ServerClick" validationgroup="AgregarNP">
                                                        <span class="fa fa-check"></span>&nbsp;&nbsp;Confirmar
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="EPConfirmaEditarNP" tabindex="-1" role="dialog" aria-labelledby="xxxx">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h2 class="modal-title">Confirmar Editar NP</h2>
                                    </div>
                                    <div class="modal-body">
                                        <div class="panel panel-danger" visible="false">
                                            <div class="panel-heading">
                                                Existe un Estado de Pago <b>no facturado</b>...
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label>¿Desea modificar el Monto?</label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="ddlOperacionEPEditar" runat="server" CssClass="selectpicker" DataValueField="ID" DataTextField="nombre" data-width="65px">
                                                                <asp:ListItem Value="monto" Text="Si"></asp:ListItem>
                                                                <asp:ListItem Value="porce" Text="No"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label>Fecha Ingreso</label><br />
                                                        <asp:Label ID="lblFechaIngresoEP3" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <label>Porcentaje</label><br />
                                                        <asp:Label ID="lblPorcentajeEP3" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <label>Monto</label><br />
                                                        <asp:Label ID="lblMontoEP3" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <label>Glosa</label><br />
                                                        <asp:Label ID="lblGlosaEP3" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="pull-right">
                                                    <button id="btnEPConfirmarEditarNP" runat="server" class="btn btn-primary" type="button" onserverclick="btnEPConfirmarEditarNP_ServerClick">
                                                        <span class="fa fa-check"></span>&nbsp;&nbsp;Confirmar edición
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="modal fade" id="EPConfirmaEliminarNP" tabindex="-1" role="dialog" aria-labelledby="xxxx">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h2 class="modal-title">Confirmar Eliminar NP</h2>
                                    </div>
                                    <div class="modal-body">
                                        <div class="panel panel-danger" visible="false">
                                            <div class="panel-heading">
                                                Existe un Estado de Pago <b>no facturado</b>...
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label>¿Desea modificar el Monto?</label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="ddlOperacionEPEliminar" runat="server" CssClass="selectpicker" DataValueField="ID" DataTextField="nombre" data-width="65px">
                                                                <asp:ListItem Value="monto" Text="Si"></asp:ListItem>
                                                                <asp:ListItem Value="porce" Text="No"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label>Fecha Ingreso</label><br />
                                                        <asp:Label ID="lblFechaIngresoEP2" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <label>Porcentaje</label><br />
                                                        <asp:Label ID="lblPorcentajeEP2" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <label>Monto</label><br />
                                                        <asp:Label ID="lblMontoEP2" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <label>Glosa</label><br />
                                                        <asp:Label ID="lblGlosaEP2" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="pull-right">
                                                    <button id="btnEPConfirmaEliminarNP" runat="server" class="btn btn-primary" type="button" onserverclick="btnEPConfirmaEliminarNP_ServerClick">
                                                        <span class="fa fa-remove"></span>&nbsp;&nbsp;Confirmar Eliminación
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="ConfirmaEliminarNP" tabindex="-1" role="dialog" aria-labelledby="xxxx">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h2 class="modal-title">Confirmar Eliminar NP</h2>
                                    </div>
                                    <div class="modal-body">
                                        <p>¿Estás seguro de eliminar la NP <asp:Label ID="lblNPAEliminar" runat="server"></asp:Label>?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="pull-right">
                                                    <button id="btnConfirmarEliminarNP" runat="server" class="btn btn-primary" type="button" onserverclick="btnConfirmarEliminarNP_ServerClick">
                                                        <span class="fa fa-remove"></span>&nbsp;&nbsp;Confirmar Eliminación
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <asp:Panel ID="formularioAgregarNP" runat="server" CssClass="form-group">
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>Los campos con asterisco (*) son obligatorios.</p>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label>ID</label>
                                                    <asp:TextBox ID="txtIDNP" runat="server" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label>* Cant</label>
                                                    <asp:TextBox ID="txtCantidad" runat="server" CssClass="form-control input-sm calc-add-np" onkeyup="FormatoNumerico(this, 1)" autocomplete="off"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvCantidad" runat="server" ErrorMessage="<b>Obligatorio</b>..." ControlToValidate="txtCantidad" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarNP" />
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <asp:UpdatePanel ID="upTipoUnidad" runat="server">
                                                        <ContentTemplate>
                                                            <label>* Unidad</label>
                                                            <button id="btnActualizarTP" runat="server" class="btn btn-link btn-xs" type="button" onserverclick="btnActualizarTP_Click">
                                                                <span class="fa fa-refresh"></span>
                                                            </button>
                                                            <asp:DropDownList ID="ddlTipoUnidad" runat="server" CssClass="selectpicker" DataValueField="ID" DataTextField="sigla" data-width="100%">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="rfvTipoUnidad" runat="server" ErrorMessage="<b>Obligatorio</b>..." ControlToValidate="ddlTipoUnidad" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarNP" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Vincular OC de Proyecto</label><br />
                                                    <asp:DropDownList ID="ddlOC" runat="server" CssClass="selectpicker" DataValueField="ID" DataTextField="Descripcion" data-width="auto"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvOC" runat="server" ErrorMessage="Seleccione al menos una <b>Orden de compra</b>..." ControlToValidate="ddlOC" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarNP" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label>* Descripción</label>
                                                <asp:TextBox ID="txtDescripcionNP" runat="server" CssClass="form-control input-sm" TextMode="MultiLine" Rows="4" ValidationGroup="AgregarNP"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvDescripcion" runat="server" ErrorMessage="Campo <b>Descripción</b> es obligatorio..." ControlToValidate="txtDescripcionNP" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarNP" />
                                                <asp:Label ID="lblContDescNP" runat="server" CssClass="help-block pull-right"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                <label>* Valor unitario</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><asp:Label ID="lblMonedaSeleccionada" runat="server"></asp:Label></span>
                                                    <asp:TextBox ID="txtValorUnitario" runat="server" CssClass="form-control input-sm calc-add-np" ValidationGroup="AgregarNP" autocomplete="off"></asp:TextBox>
                                                </div>
                                                <asp:RequiredFieldValidator ID="rfvValorUnitario" runat="server" ErrorMessage="Campo <b>Valor unitario</b> es obligatorio..." ControlToValidate="txtValorUnitario" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarNP" />                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label>Valor total <abbr class="initialism" title="Nota de pedido">NP</abbr></label>
                                                    <div class="input-group">
                                                        <div class="input-group-btn">
                                                            <asp:DropDownList id="ddlTipoValor2" runat="server" CssClass="selectpicker dtv2" data-width="62px">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:TextBox ID="txtValorTotalNP" runat="server" Text="0" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label>Valor proyecto</label>
                                                    <div class="input-group">
                                                        <div class="input-group-btn">
                                                            <asp:DropDownList id="ddlTipoValor3" runat="server" CssClass="selectpicker dtv2" data-width="62px">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:TextBox ID="txtValorProyecto1" runat="server" Text="0" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="pull-right">
                                                <button id="btnAgregarNP" runat="server" class="btn btn-default" type="button" rel="tooltip" data-toggle="tooltip" data-placement="top" title="Agregar nota de pedido" onserverclick="btnAgregarNP_ServerClick" validationgroup="AgregarNP">
                                                    <span class="fa fa-calendar-plus-o"></span>&nbsp;&nbsp;Agregar nota de pedido
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel-group" id="accordion" runat="server" role="tablist" aria-multiselectable="true">
                                        <asp:HiddenField ID="hfActualValorTotalNP" runat="server" />
                                        <asp:GridView ID="gvNP" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None" ShowHeader="false"
                                            OnRowDataBound="gvNP_RowDataBound" OnRowEditing="gvNP_RowEditing" OnRowUpdating="gvNP_RowUpdating" OnRowCancelingEdit="gvNP_RowCancelingEdit" OnRowDeleting="gvNP_RowDeleting">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <div id="npRow" runat="server" class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading" runat="server">
                                                                <h3 class="panel-title">
                                                                    <b>
                                                                        <asp:HyperLink ID="rowNP" runat="server" data-toggle="collapse" aria-expanded="false"></asp:HyperLink>
                                                                    </b>
                                                                    <i class="fa fa-comment" style="margin-left: 10px"></i>&nbsp;&nbsp;<span class="textoc"><%# Eval("Creacion.Descripcion") %></span>
                                                                </h3>
                                                            </div>
                                                            <div id="np" runat="server" class="panel-collapse collapse" role="tabpanel">
                                                                <div class="panel-body">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-lg-2">
                                                                                <div class="form-group">
                                                                                    <label>Cant</label>
                                                                                    <asp:TextBox ID="txtCantidad" runat="server" CssClass="form-control input-sm" onkeyup="FormatoNumerico(this,1 )" autocomplete="off" ReadOnly="true"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-2">
                                                                                <div class="form-group">
                                                                                    <label>Unidad</label>
                                                                                    <asp:DropDownList ID="ddlTipoUnidad" runat="server" CssClass="selectpicker" Enabled="false" DataValueField="ID" DataTextField="sigla" data-width="100%">
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-8">
                                                                                <div class="form-group">
                                                                                    <label><abbr class="initialism" title="Orden de Compra">OC</abbr> de Proyecto</label><br />
                                                                                    <asp:DropDownList ID="ddlOC" runat="server" CssClass="selectpicker" Enabled="false" DataValueField="ID" DataTextField="Descripcion" data-width="auto"></asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-lg-12">
                                                                                <div class="form-group">
                                                                                    <label>Descripción</label>
                                                                                    <asp:TextBox ID="txtDescripcionNP" runat="server" CssClass="form-control input-sm" Text='<%# Eval("Creacion.Descripcion") %>' TextMode="MultiLine" Rows="4" ReadOnly="true" MaxLength="500"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-lg-4">
                                                                                <div class="form-group">
                                                                                    <label>Valor unitario</label>
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-btn">
                                                                                            <asp:DropDownList id="ddlTipoValor1" runat="server" CssClass="selectpicker" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                        <asp:TextBox ID="txtValorUnitario" runat="server" CssClass="form-control input-sm" MaxLength="12" onkeyup="FormatoNumerico(this, 1)" autocomplete="off"  ReadOnly="true"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4">
                                                                                <div class="form-group">
                                                                                    <label>Valor total <abbr class="initialism" title="Nota de Pedido">NP</abbr></label>
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-btn">
                                                                                            <asp:DropDownList id="ddlTipoValor2" runat="server" CssClass="selectpicker" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                        <asp:TextBox ID="txtValorTotalNP" runat="server" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4">
                                                                                <div class="form-group">
                                                                                    <label>Valor proyecto</label>
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-btn">
                                                                                            <asp:DropDownList id="ddlTipoValor3" runat="server" CssClass="selectpicker" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                        <asp:TextBox ID="txtValorProyecto" runat="server" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="panel-footer">
                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="pull-right">
                                                                                <asp:Button ID="btnEditar" runat="server" Text="Editar" CssClass="btn btn-info btn-xs" type="button" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Editar nota de pedido" CommandName="Edit" />
                                                                                <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" CssClass="btn btn-danger btn-xs" type="button" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Eliminar nota de pedido" CommandName="Delete" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab">
                                                                <h3 class="panel-title">
                                                                    <b>
                                                                        <asp:HyperLink ID="rowNP" runat="server" data-toggle="collapse" data-parent="#accordion" aria-expanded="false"></asp:HyperLink>
                                                                    </b>
                                                                    <i class="fa fa-comment" style="margin-left: 10px"></i>&nbsp;&nbsp;<span class="textoc"><%# Eval("Creacion.Descripcion") %></span></h3>
                                                            </div>
                                                            <div id="np" runat="server" class="panel-collapse collapse" role="tabpanel">
                                                                <div class="panel-body">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-lg-2">
                                                                                <div class="form-group">
                                                                                    <label>* Cant</label>
                                                                                    <asp:TextBox ID="txtCantidad" runat="server" CssClass="form-control input-sm" onkeyup="FormatoNumerico(this, 1)" autocomplete="off"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="rfvCantidad" runat="server" ErrorMessage="Obligatorio..." ControlToValidate="txtCantidad" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="EditarNP" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-2">
                                                                                <div class="form-group">
                                                                                    <label>* Unidad</label>
                                                                                    <asp:DropDownList ID="ddlTipoUnidad" runat="server" CssClass="selectpicker" DataValueField="ID" DataTextField="sigla" data-width="100%">
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-8">
                                                                                <div class="form-group">
                                                                                    <label>Vincular <abbr class="initialism" title="Orden de Compra">OC</abbr> de Proyecto</label><br />
                                                                                    <asp:DropDownList ID="ddlOC" runat="server" CssClass="selectpicker" DataValueField="ID" DataTextField="Descripcion" data-width="auto"></asp:DropDownList>
                                                                                    <asp:RequiredFieldValidator ID="rfvOCSeleccionada" runat="server" ErrorMessage="Obligatorio..." ControlToValidate="ddlOC" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="EditarNP" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-lg-12">
                                                                                <div class="form-group">
                                                                                    <label>* Descripción</label>
                                                                                    <asp:TextBox ID="txtDescripcionNP" runat="server" CssClass="form-control input-sm" Text='<%# Eval("Creacion.Descripcion") %>' TextMode="MultiLine" Rows="4" MaxLength="500"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="rfvDescripcion" runat="server" ErrorMessage="Obligatorio..." ControlToValidate="txtDescripcionNP" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="EditarNP" />
                                                                                    <asp:Label ID="lblContDescNP" runat="server" CssClass="help-block pull-right"></asp:Label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-lg-4">
                                                                                <div class="form-group">
                                                                                    <label>* Valor unitario</label>
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon"><asp:Label ID="lblMonedaSeleccionada" runat="server"></asp:Label></span>
                                                                                        <asp:TextBox ID="txtValorUnitario" runat="server" CssClass="form-control input-sm" onkeyup="FormatoNumerico(this, 1)" autocomplete="off"></asp:TextBox>
                                                                                    </div>
                                                                                    <asp:RequiredFieldValidator ID="rfvValorUnitario" runat="server" ErrorMessage="Obligatorio..." ControlToValidate="txtValorUnitario" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="EditarNP" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4">
                                                                                <div class="form-group">
                                                                                    <label>Valor total <abbr class="initialism" title="Nota de Pedido">NP</abbr></label>
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-btn">
                                                                                            <asp:DropDownList id="ddlTipoValor2" runat="server" CssClass="selectpicker" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                        <asp:TextBox ID="txtValorTotalNP" runat="server" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-4">
                                                                                <div class="form-group">
                                                                                    <label>Valor proyecto</label>
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-btn">
                                                                                            <asp:DropDownList id="ddlTipoValor3" runat="server" CssClass="selectpicker" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                        <asp:TextBox ID="txtValorProyecto" runat="server" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="panel-footer">
                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="pull-right">
                                                                                <asp:Button ID="btnEditar" runat="server" Text="Guardar" CssClass="btn btn-primary btn-xs" type="button" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Editar nota de pedido" CommandName="Update" ValidationGroup="EditarNP" />
                                                                                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn btn-default btn-xs" type="button" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Eliminar nota de pedido" CommandName="Cancel" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="EstadoPago">
                        <div class="form-group">
                            <p>Ingrese el monto o el porcentaje para generar un estado de pago.</p>
                            <p><label class="label label-danger"><i class="fa fa-info-circle" aria-hidden="true"></i> Importante</label> los estados de pagos se aplican a todas las notas de pedidos creadas.</p>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-5">
                                                <div class="form-group">
                                                    <asp:RadioButton ID="rbPorcentaje" runat="server" Text="&nbsp;Porcentaje" GroupName="TipoEP" Checked="true" />
                                                    <div class="input-group">
                                                        <asp:TextBox ID="txtPorcentaje" runat="server" CssClass="form-control input-sm text-right" onblur="CalculaMontoEP(this.value)" ViewStateMode="Enabled" MaxLength="4" autocomplete="off" onkeyup="FormatoNumerico(this, 2);"></asp:TextBox>
                                                        <span class="input-group-addon">%</span>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="rfvPorcentaje" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtPorcentaje" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarEstadoPago" />
                                                </div>
                                            </div>
                                            <div class="col-lg-5">
                                                <div class="form-group">
                                                    <asp:RadioButton ID="rbMonto" runat="server" Text="&nbsp;Monto" GroupName="TipoEP"/>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><asp:Label ID="lblMonedaSeleccionadaEP" runat="server"></asp:Label></span>
                                                        <asp:TextBox ID="txtMonto" runat="server" CssClass="form-control input-sm valortotal-format" onblur="CalculaPorcentajeEP(this.value)" ViewStateMode="Enabled" Enabled="false" autocomplete="off"></asp:TextBox>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="rfvMonto" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtMonto" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarEstadoPago" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <div class="form-group">
                                                    <label>* Glosa</label>
                                                    <asp:TextBox ID="txtGlosa" runat="server" placeholder="Ingrese texto..." CssClass="form-control input-sm" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvGlosa" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtGlosa" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarEstadoPago" />
                                                    <asp:Label ID="lblContG" runat="server" CssClass="help-block pull-right"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <div class="form-group">
                                                    <div class="pull-right">
                                                        <button id="btnAgregarEstadoPago" class="btn btn-default" type="button" runat="server" rel="tooltip" data-toggle="tooltip" data-placement="top" title="Agregar estado de pago" onserverclick="btnAgregarEstadoPago_ServerClick" validationgroup="AgregarEstadoPago">
                                                            <span class="fa fa-plus"></span>&nbsp;&nbsp;Agregar
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-6">
                                        <div class="col-lg-12">
                                            <div class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label">Valor proyecto</label>
                                                    <div class="input-group">
                                                        <div class="input-group-btn">
                                                            <asp:DropDownList id="ddlTipoValor4" runat="server" CssClass="selectpicker dtv3" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:TextBox ID="txtValorProyectoEP" runat="server" Text="0" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="ConfirmaEliminarEP" tabindex="-1" role="dialog" aria-labelledby="xxxx">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h2 class="modal-title">Confirmar Eliminar Estado de Pago</h2>
                                    </div>
                                    <div class="modal-body">
                                        <p>¿Está seguro de eliminar El estado de pago que representa el <asp:Label ID="lblEstadoPagoEliminar" runat="server"></asp:Label>% del proyecto?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="pull-right">
                                                    <button id="btnConfirmarEliminarEP" runat="server" class="btn btn-primary" type="button" onserverclick="btnConfirmarEliminarEP_ServerClick">
                                                        <span class="fa fa-remove"></span>&nbsp;&nbsp;Confirmar Eliminación
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="table-responsive">
                                <asp:GridView ID="gvEstadoPago" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None"
                                    OnRowDeleting="gvEstadoPago_RowDeleting">
                                    <Columns>
                                        <asp:TemplateField HeaderText="#">
                                            <ItemTemplate>
                                                <asp:Label ID="lblN" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fecha Ingreso">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFechaCreacion" runat="server" Text='<%# Eval("FechaCreacion") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Porcentaje">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPorcentaje" runat="server" Text='<%# string.Format("{0:#,0.##}%", Convert.ToDecimal(Eval("Porcentaje"))) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Monto">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMonto" runat="server" Text='<%# string.Format("{0} {1:#,0.##}", lblMonedaSeleccionadaEP.Text, Convert.ToDecimal(Eval("Monto"))) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Glosa">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGlosa" runat="server" Text='<%# Eval("Glosa") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-CssClass="col-lg-2 right">
                                            <ItemTemplate>
                                                <div class="pull-right">
                                                    <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" CssClass="btn btn-danger btn-xs" CommandName="Delete" />
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="Contactos">
                        <p>Seleccione los contactos que se involucran en este proyecto o agregue un nuevo contacto:</p>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <strong>Contactos</strong>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <p>Si desea agregar un nuevo contacto comercial haga <a href="#" data-toggle="modal" data-target="#x19">clic aquí</a>.</p>

                                            <div class="modal fade" id="x19" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h2 class="modal-title">Agregar contacto</h2>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Los campos con asterisco (*) son obligatorios.</p>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <label>* Nombre Completo</label>
                                                                        <asp:TextBox ID="txtNombreContacto" runat="server" CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rfvNombreContacto" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtNombreContacto" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarContacto" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <label>* Email</label>
                                                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtEmail" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarContacto" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <label>* Descripción de contacto</label>
                                                                        <asp:TextBox ID="txtDescripcionContacto" runat="server" CssClass="form-control input-sm" MaxLength="200" autocomplete="off"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rfvDescripcionContacto" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtDescripcionContacto" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarContacto" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <label>* Teléfono de contacto</label>
                                                                        <asp:TextBox ID="txtTelefono" runat="server" CssClass="form-control input-sm" MaxLength="20" autocomplete="off"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rfvTelefono" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtTelefono" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="AgregarContacto" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <label>Teléfono móvil</label>
                                                                        <asp:TextBox ID="txtMovil" runat="server" CssClass="form-control input-sm" MaxLength="20" autocomplete="off"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <div class="col-lg-12">
                                                                            <div class="col-lg-12" style="margin-top: 20px">
                                                                                <asp:CheckBox ID="cbCompartirContacto" runat="server" Text="¿Desea compartir usuario?" CssClass="checkbox" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button id="btnAgregarContacto" runat="server" class="btn btn-primary" type="button" validationgroup="AgregarContacto" onserverclick="btnAgregarContacto_ServerClick">
                                                                <span class="fa fa-user-plus"></span>&nbsp;&nbsp;Agregar nuevo contacto
                                                            </button>
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <%--habilita botón "vincular contactos" cuando detecta un movimiento en los checkbox del gridview gvContacto--%>
                                        <script type='text/javascript'>
                                            $(function () {
                                                $("[id*=cbContacto]").click(function () {
                                                    $("[id*=btnVincularContacto]").removeAttr('disabled');
                                                });
                                            });
                                        </script>

                                        <div class="table-responsive">
                                            <asp:GridView ID="gvContacto" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None" ShowFooter="true"
                                                OnRowDeleting="gvContacto_RowDeleting" OnRowCommand="gvContacto_RowCommand">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Nombre completo">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="cbContacto" runat="server" CssClass="checkbox" Checked='<%# Eval("VinculadoEnProyecto") %>' Text='<%# Eval("NombreContacto") %>' style="margin-left: 20px;margin-top: 0px"/>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Descripción">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDesc" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Email">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Teléfono">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTelefono" runat="server" Text='<%# Eval("Telefono") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Móvil">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMovil" runat="server" Text='<%# Eval("Movil") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Compartido">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTipo" runat="server" Text='<%# Convert.ToBoolean(Eval("ContactoCompartido")) == true ? "Compartido" : "No" %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-CssClass="col-lg-2 right">
                                                        <ItemTemplate>
                                                            <div class="pull-right">
                                                                <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" CssClass="btn btn-danger btn-xs" CommandName="Delete" OnClientClick='<%# string.Format("return confirm(\"¿Está seguro que desea eliminar a {0} de cliente {1}?\");", Eval("NombreContacto"), txtCliente.Text) %>' />
                                                            </div>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <div class="pull-right">
                                                                <asp:Button ID="btnVincularContacto" runat="server" Text="Vincular contactos" CssClass="btn btn-default" CommandName="GuardarSeleccion" disabled="disabled" />
                                                            </div>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right">
                                <button id="btnFinalizarCreacion" runat="server" class="btn btn-primary" type="button" rel="tooltip" data-toggle="tooltip" data-placement="top" title="Al finalizar la creación del proyecto, usted automáticamente será redireccionado al módulo Buscar Proyecto filtrando el proyecto recién creado." onclick="javascript:delStateTab();" onserverclick="btnFinalizarCreacion_ServerClick" visible="false">
                                    <span class="fa fa fa-check"></span>&nbsp;&nbsp;Finalizar creación proyecto
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
﻿<%@ Page Title="Estado de pago (eliminado)" Language="C#" MasterPageFile="~/makeup2/proyecto/eliminado/proyecto.master" AutoEventWireup="true" CodeBehind="estado-de-pago.aspx.cs" Inherits="MakeUp.App.UI.makeup2.proyecto.eliminado.estado_de_pago" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headProyecto" runat="server">
    <link href="../../../css/bootstrap-select.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenidoProyecto" runat="server">
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/languages/es.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/count-character-textbox.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/number-format-using-numeral.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/dropdown-igualitario.js") %>'></script>
    
    <asp:HiddenField ID="hfIdProyecto" runat="server" />
    <asp:HiddenField ID="hfPorcentaje" runat="server" />
    <asp:HiddenField ID="hfMonto" runat="server" />
    <asp:HiddenField ID="hfValorTipoValor" runat="server" />

    <div class="form-group">
        <p>Estados de pago del proyecto.</p>
        <div class="row">
            <div class="form-group">
                <div class="col-lg-12">
                    <div class="col-lg-12">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-9 control-label">Valor Proyecto</label>
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <asp:DropDownList id="ddlTipoValor3" runat="server" CssClass="selectpicker dtv1" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:TextBox ID="txtValorProyecto" runat="server" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-9 control-label">Valor Facturado</label>
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <asp:DropDownList id="ddlTipoValor2" runat="server" CssClass="selectpicker dtv1" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:TextBox ID="txtValorFacturado" runat="server" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-9 control-label">Saldo</label>
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <asp:DropDownList id="ddlTipoValor1" runat="server" CssClass="selectpicker dtv1" data-width="62px"> <%--evento onchange se carga en servidor para enviar id real del textbox--%>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:TextBox ID="txtSaldoPorFacturar" runat="server" CssClass="form-control input-sm" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="table-responsive">
            <asp:GridView ID="gvEstadoPago" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None"
                OnRowDataBound="gvEstadoPago_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderText="#" HeaderStyle-Width="28px">
                        <ItemTemplate>
                            <asp:Label ID="lblN" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Porcentaje" HeaderStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Label ID="lblPorcentaje" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Monto" HeaderStyle-Width="128px">
                        <ItemTemplate>
                            <asp:Label ID="lblMonto" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Glosa Estado de Pago">
                        <ItemTemplate>
                            <asp:Label ID="lblGlosa" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Fecha Creación" HeaderStyle-Width="140px">
                        <ItemTemplate>
                            <asp:Label ID="lblFechaCreacion" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Fecha Facturado MU" HeaderStyle-Width="160px">
                        <ItemTemplate>
                            <asp:Label ID="lblFechaPreFacturado" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="80px">
                        <ItemTemplate>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
﻿function AutoComplete(textbox, urlJson) {
    $(function () {
        $('[id*=' + textbox + ']').typeahead({
            hint: true,
            highlight: true,
            minLength: 1,
            source: function (request, response) {
                $.ajax({
                    url: urlJson,
                    data: "{ 'prefix': '" + request + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        items = [];
                        map = {};
                        $.each(data.d, function (i, item) {
                            //var id = item.split('-')[1];
                            //var name = item.split('-')[0];
                            //map[name] = { id: id, name: name };

                            var name = item;
                            map[name] = { name: name };
                            items.push(name);
                        });
                        response(items);
                        $(".dropdown-menu").css("height", "auto");
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            }
        });
    });
}

function AutoCompleteConValor(textbox, hiddenField, urlJson) {
    $(function () {
        $('[id*=' + textbox + ']').typeahead({
            hint: true,
            highlight: true,
            minLength: 1,
            source: function (request, response) {
                $.ajax({
                    url: urlJson,
                    data: "{ 'prefix': '" + request + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        items = [];
                        map = {};
                        $.each(data.d, function (i, item) {
                            var id = item.split('-')[0];
                            var name = item.split('-')[1];
                            map[name] = { id: id, name: name };
                            items.push(name);
                        });

                        $('[id*=' + hiddenField + ']').val('');

                        response(items);
                        $(".dropdown-menu").css("height", "auto");
                    },
                    error: function (response) {
                        $('[id*=' + hiddenField + ']').val('');
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        $('[id*=' + hiddenField + ']').val('');
                        alert(response.responseText);
                    }
                });
            },
            updater: function (item) {
                $('[id*=' + hiddenField + ']').val(map[item].id); //asigna valor cuando usuario selecciona valor del autocompletar
                return item;
            }
        });
    });
}
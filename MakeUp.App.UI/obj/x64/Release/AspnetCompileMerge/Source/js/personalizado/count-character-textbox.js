﻿//contador de caracteres
function countChar(val, lbl, maxCaracteres) {
    var len = val.value.length;

    if (len == 0)
        document.getElementById(lbl).innerText = "";
    else if (len >= maxCaracteres + 1)
        val.value = val.value.substring(0, maxCaracteres);
    else if (len <= maxCaracteres)
        document.getElementById(lbl).innerText = "Caracteres restantes permitidos: " + (maxCaracteres - len);
}
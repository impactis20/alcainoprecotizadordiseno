﻿<%@ Page Title="Facturar Estados de Pago" Language="C#" MasterPageFile="~/makeup2/facturacion/factura-pendiente.master" AutoEventWireup="true" CodeBehind="pendiente.aspx.cs" Inherits="MakeUp.App.UI.makeup2.facturacion.estado_de_pago.pendiente" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headFacturas" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenidoFacturas" runat="server">
    <p>Listado de proyectos con <b>Estados de Pagos</b> que están pendiente de facturación.</p>
    <asp:Label ID="lblContador" runat="server"></asp:Label>
    <div class="table-responsive">
        <asp:GridView ID="gvEstadoPago" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None" 
            OnRowDataBound="gvEstadoPago_RowDataBound">
            <Columns>
                <asp:TemplateField HeaderText="ID">
                    <ItemTemplate>
                        <%# Eval("ID") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Nombre Proyecto">
                    <ItemTemplate>
                        <%# Eval("Nombre") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Cliente <small>(nombre fantasía)</small>">
                    <ItemTemplate>
                        <a href='<%# string.Format("detalle.aspx?id={0}", Eval("ID")) %>'><%# Eval("Cliente.NombreFantasia") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Facturado">
                    <ItemTemplate>
                        <%# Eval("CantidadEPEmitidaFacturacion") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Facturado MG" Visible="false">
                    <ItemTemplate>
                        <%# Eval("CantidadEPFacturado") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Total EP">
                    <ItemTemplate>
                         <%# Eval("CantidadTotalEP") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="EP Pendiente">
                    <ItemTemplate>
                        <asp:Label ID="lblEPPendiente" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
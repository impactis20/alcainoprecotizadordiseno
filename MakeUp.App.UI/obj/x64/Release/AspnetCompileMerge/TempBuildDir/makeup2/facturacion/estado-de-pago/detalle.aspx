﻿<%@ Page Title="Facturar Estados de Pago" Language="C#" MasterPageFile="~/makeup2/menu.master" AutoEventWireup="true" CodeBehind="detalle.aspx.cs" Inherits="MakeUp.App.UI.makeup2.facturacion.estado_de_pago.detalle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headMenu" runat="server">
    <link href="../../../css/bootstrap-datetimepicker.css" rel="stylesheet" />
    <link href="../../../css/bootstrap-select.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-datetimepicker.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/personalizado/count-character-textbox.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/languages/es.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/number-format-using-numeral.js") %>'></script>

    <script type="text/javascript">
        $(function () {
            $('[id*=dFechaFacturacion]').datetimepicker({
                useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                format: 'DD-MM-YYYY',
                maxDate: moment().add(1, 'days'), //permite hasta la fecha de hoy
                showClear: true,
                ignoreReadonly: true
            });

            $('[id*=dFechaVencimiento]').datetimepicker({
                useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                format: 'DD-MM-YYYY',
                minDate: 'now',
                showClear: true,
                ignoreReadonly: true
            });

            $('[id*=dFechaOC]').datetimepicker({
                useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                format: 'DD-MM-YYYY',
                maxDate: 'now',
                showClear: true,
                ignoreReadonly: true
            });

            $('[id*=dFechaHes]').datetimepicker({
                useCurrent: false, //mantiene la fecha ingresa después de realizar postback
                format: 'DD-MM-YYYY',
                maxDate: 'now',
                showClear: true,
                ignoreReadonly: true
            });

            $('[id*=dFechaFacturacion]').on("dp.change", function (e) {
                var fechaSeleccionada = e.date.format('DD-MM-YYYY');
                var urlValorUF = '<%= ResolveUrl(string.Format("{0}/GetValorUF", Request.Url.AbsolutePath)) %>';
                $.ajax({
                    url: urlValorUF,
                    data: "{ 'date': '" + fechaSeleccionada + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        numeral.language('es'); //asigna idioma a numeral
                        var valorUFDia = data.d;
                        var tipoMoneda = 'CLP ';
                        var valorFacturacion = parseFloat(ConvertToNumber($('#<%= hfValorFacturacion.ClientID %>').val())); //valor en UF
                        $('#<%= lblValorDia.ClientID %>').text(tipoMoneda + numeral(valorUFDia).format('0,0.[00]'));
                        $('#<%= txtTotalFacturacion.ClientID %>').val(numeral(valorUFDia * valorFacturacion).format('0,0')); //conversión a CLP no lleva decimales (los decimales los aproxima)
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            });
        });
    </script>

    <asp:HiddenField ID="hfIdCliente" runat="server" />

    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Facturar Proyecto (Estado de Pago)</h2>
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>ID Proyecto:</label><br />
                            <asp:Label ID="lblID" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div id="padre" runat="server" class="col-lg-6">
                        <div class="form-group">
                            <label>Proyecto padre:</label><br />
                            <asp:Label ID="lblPadre" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Nombre proyecto:</label><br />
                            <asp:Label ID="lblNombreProyecto" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Cliente <small>(nombre fantasía)</small>:</label><br />
                            <asp:Label ID="lblCliente" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Fecha de ingreso:</label><br />
                            <asp:Label ID="lblFechaIngreso" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Fecha planificada:</label><br />
                            <asp:Label ID="lblFechaPlanificada" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Ejecutivo comercial:</label><br />
                            <asp:Label ID="lblEjecutivo" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Total NP:</label><br />
                            <asp:Label ID="lblTotalNP" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">                                    
                            <label>Divisa o tipo de valor:</label><br />
                            <asp:Label ID="lblTipoValor" runat="server"></asp:Label>
                            <asp:HiddenField ID="hfIDTipoValor" runat="server" />
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Valor proyecto:</label><br />
                            <asp:Label ID="lblValorProyecto" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('[data-toggle="popover"]').popover();
                });
            </script>

            <script type="text/javascript">
                //Funcion que sirve para cerrar popover desde el boton "x"
                $(document).on("click", ".popover .close", function () {
                    $(this).parents(".popover").popover('hide');
                });
            </script>

            <style>
                .radio-ff > input {
                    margin-top:0px;
                    margin-right:24px
                }
                .radio-ff > label {
                    margin:0px;
                    cursor:pointer
                }

                .hand-point{
                    cursor:pointer
                }

                .ff {
                    display: inline-block;
                    white-space: nowrap;
                    overflow: hidden;
                    text-overflow: ellipsis;     /** IE6+, Firefox 7+, Opera 11+, Chrome, Safari **/
                    -o-text-overflow: ellipsis;  /** Opera 9 & 10 **/
                    width: 200px; /* note that this width will have to be smaller to see the effect */
                }
            </style>

            <div class="form-group">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <asp:GridView ID="gvEP" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None"
                                OnRowDataBound="gvEP_RowDataBound" OnRowCommand="gvEP_RowCommand">
                                <Columns>
                                    <asp:TemplateField HeaderText="#" HeaderStyle-Width="28px">
                                        <ItemTemplate>
                                            <asp:Label id="lblEP" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Porcentaje" HeaderStyle-Width="80px">
                                        <ItemTemplate>
                                            <asp:Label id="lblPorcentaje" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Monto" HeaderStyle-Width="128px">
                                        <ItemTemplate>
                                            <asp:Label id="lblMonto" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Glosa Estado de Pago">
                                        <ItemTemplate>
                                            <%--<asp:HyperLink id="detalleEP" runat="server" CssClass="hand-point ff" data-toggle="popover" data-html="true" title="<b>Glosa EP</b><a href=# class=close data-dismiss=alert>&times;</a>" data-container="body" data-placement="top"></asp:HyperLink>--%>
                                            <asp:Label ID="lblGlosa" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fecha Creación" HeaderStyle-Width="140px">
                                        <ItemTemplate>
                                            <asp:Label id="lblFechaIngreso" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fecha Facturado MU" HeaderStyle-Width="160px">
                                        <ItemTemplate>
                                            <asp:Label id="lblFechaPreFacturado" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-CssClass="pull-right" HeaderStyle-Width="80px">
                                        <ItemTemplate>
                                            <asp:Button ID="btnFacturarEP" runat="server" Text="Facturar" CssClass="btn btn-primary btn-xs" Visible="false" CommandName="FacturarEP" CommandArgument='<%# Container.DataItemIndex %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>

                        <div id="facturar" runat="server" class="modal fade facturacion" tabindex="-1" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4>Genere facturación de Estado de Pago...</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>¿Está seguro(a) que desea realizar la facturación de estado de pago correspondiente al <b id="porcentaje" runat="server"></b> del proyecto <b id="nombreProyecto" runat="server"></b>?. A continuación, revise el siguiente detalle de facturación:</p>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label>Estado de pago a facturar</label>
                                                <br />
                                                <asp:Label ID="lblEPxFacturar" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hfValorFacturacion" runat="server" />
                                            </div>
                                        </div>
                                        <br />
                                        <p>Para terminar la facturación, agregue una<asp:Label ID="lblInfoFechaFacturacion" runat="server" Text=" fecha de facturación," Visible="false"></asp:Label> condición de pago y glosa (obligatorio).</p>

                                        <div id="calendarioFechaFacturacion" runat="server" class="row" visible="false">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>* Fecha de facturación</label>
                                                    <div class="input-group date input-group" id="dFechaFacturacion">
                                                        <asp:TextBox ID="txtFechaFacturacion" runat="server" CssClass="form-control input-sm disabled" autocomplete="off"></asp:TextBox>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="rfvFechaFacturacion" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtFechaFacturacion" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" />
                                                    <asp:Label ID="lblValorDivisa" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label>Valor día</label>
                                                    <br />
                                                    <asp:Label ID="lblValorDia" runat="server" Text="CLP 0"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Total a facturar</label>
                                                    <br />
                                                    <div class="input-group">
                                                        <span class="input-group-addon">CLP</span>
                                                        <asp:TextBox ID="txtTotalFacturacion" runat="server" Text="0" CssClass="form-control input-sm" onkeyup="FormatoNumerico(this, 0);"></asp:TextBox>
                                                    </div>
                                                    <asp:HiddenField ID="hfValorTotalProyecto" runat="server" Value="0" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Condición de pago:</label><br />
                                                    <%--<asp:TextBox ID="txtCondicionPago" runat="server" CssClass="form-control input-sm" MaxLength="200" autocomplete="off"></asp:TextBox>--%>
                                                    <asp:DropDownList ID="ddlCondicionPago" runat="server" CssClass="selectpicker" DataValueField="ID" DataTextField="sigla" data-width="auto">
                                                        <asp:ListItem Text="Contado" Value="Contado"></asp:ListItem>
                                                        <asp:ListItem Text="Crédito 7 días" Value="Credito 7 dias"></asp:ListItem>
                                                        <asp:ListItem Text="Crédito 15 días" Value="Credito 15 dias"></asp:ListItem>
                                                        <asp:ListItem Text="Crédito 30 días" Value="Credito 30 dias" Selected="True"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <%--<asp:RequiredFieldValidator ID="rfvCondicionPago" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtCondicionPago" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="ConfirmaFacturacion" />--%>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>* Fecha de vencimiento:</label>
                                                    <div class="input-group date input-group" id="dFechaVencimiento">
                                                        <asp:TextBox ID="txtFechaVencimiento" runat="server" CssClass="form-control input-sm disabled" autocomplete="off"></asp:TextBox>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="rfvFechaVencimiento" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtFechaVencimiento" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="ConfirmaFacturacion" />
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>N° de OC.:</label><br />
                                                    <asp:TextBox ID="txtNroOC" runat="server" CssClass="form-control input-sm" MaxLength="30" autocomplete="off"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Fecha OC.:</label><br />
                                                    <div class="input-group date input-group" id="dFechaOC">
                                                        <asp:TextBox ID="txtFechaOC" runat="server" CssClass="form-control input-sm disabled" autocomplete="off"></asp:TextBox>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>N° de HES.:</label><br />
                                                    <asp:TextBox ID="txtNroHes" runat="server" CssClass="form-control input-sm" MaxLength="30" autocomplete="off"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Fecha HES.:</label><br />
                                                    <div class="input-group date input-group" id="dFechaHes">
                                                        <asp:TextBox ID="txtFechaHes" runat="server" CssClass="form-control input-sm disabled" autocomplete="off"></asp:TextBox>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>* Glosa Estado de Pago:</label><br />
                                                    <asp:TextBox ID="txtGlosa" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3" MaxLength="80" autocomplete="off"></asp:TextBox>
                                                    <asp:Label ID="lblGlosa" runat="server" CssClass="help-block pull-right"></asp:Label>
                                                    <asp:RequiredFieldValidator ID="rfvGlosa" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtGlosa" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="ConfirmaFacturacion" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button id="btnConfirmaFacturacion" runat="server" class="btn btn-primary" type="button" onserverclick="btnConfirmaFacturacion_ServerClick" validationgroup="ConfirmaFacturacion">
                                            <span class="fa fa-check"></span>&nbsp;&nbsp;Aceptar
                                        </button>
                                        <button class="btn btn-default" type="button" data-dismiss="modal">
                                            <span class="fa fa-remove"></span>&nbsp;&nbsp;Cancelar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="pull-right">
                            <asp:HyperLink ID="linkVolver" runat="server" Text="Volver" CssClass="btn-link btn-lg" NavigateUrl="~/makeup2/facturacion/estado-de-pago/pendiente.aspx" rel="tooltip" data-placement="top" title="Volver al módulo de proyectos que tienen Notas de Pedido Valorizadas pendiente de facturación"></asp:HyperLink>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function CheckOtherIsCheckedByGVID(rb) {
            var isChecked = rb.checked;
            var row = rb.parentNode.parentNode;

            var currentRdbID = rb.id;
            var parent = document.getElementById("<%= gvEP.ClientID %>");
            var items = parent.getElementsByTagName('input');

            for (i = 0; i < items.length; i++) {
                if (items[i].id != currentRdbID && items[i].type == "radio") {
                    if (items[i].checked) {
                        items[i].checked = false;
                    }
                }
            }
        };
    </script>
</asp:Content>
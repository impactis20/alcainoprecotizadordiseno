﻿/* JQuery */
$(document).ready(function () {
    $('[id*=cbModulo]').each(function (i, obj) {
        if ($(this).is(":checked")) {
            var accion = $(this).attr("action");
            if (accion === 'add')
                $('[class*=' + this.id + '] input:checkbox').removeAttr('disabled');
        }
        else {
            $('[class*=' + this.id + '] input:checkbox').attr('disabled', 'disabled');
        }
    })
});

$(function () {
    $('input[type=checkbox][name*=cbModulo]').change(function () {
        // this will contain a reference to the checkbox   
        if (this.checked) {
            // the checkbox is now checked 
            $('[class*=' + this.id + '] input:checkbox').removeAttr('disabled');
        } else {
            // the checkbox is now no longer checked
            $('[class*=' + this.id + '] input:checkbox').attr('disabled', 'disabled');
        }
    });
});
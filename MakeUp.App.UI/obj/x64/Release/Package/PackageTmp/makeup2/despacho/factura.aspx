﻿<%@ Page Title="Facturas" Language="C#" MasterPageFile="~/makeup2/menu.master" AutoEventWireup="true" CodeBehind="factura.aspx.cs" Inherits="MakeUp.App.UI.makeup2.despacho.factura" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headMenu" runat="server">
    <link href="../../css/bootstrap-typeahead.css" rel="stylesheet" />
    <link rel="stylesheet" href="../../css/bootstrap-select.css" />
    <link href="../../css/fileinput.min.4.4.2.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/autocomplete-query.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap3-typeahead.min.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/numeral.min.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/languages/es.js") %>' type="text/javascript"></script>

    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>' ></script>
    <script src='<%= ResolveUrl("~/js/fileinput.min.4.4.2.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/jquery.dataTables.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/activaReferencia.v1.js") %>'></script>

    <script type="text/javascript">
        $(function () {
            AutoComplete("<%= txtCliente.ClientID %>", '<%= ResolveUrl(string.Format("{0}/GetNombreFantasia", Request.Url.AbsolutePath)) %>');
            AutoComplete("<%= txtNombre.ClientID %>", '<%= ResolveUrl(string.Format("{0}/GetNombreProyecto", Request.Url.AbsolutePath)) %>');
        });
    </script>
    <h2 class="page-header">Facturas</h2>
    <p>Busque una factura emitida por ID de proyecto, nombre de proyecto y/o cliente. Además puede filtrar según estado de despacho.</p>
    <div id="formulario" runat="server" class="form-group">
        <div class="row">
            <div class="col-lg-1">
                <div class="form-group">
                    <label>ID</label>
                    <asp:TextBox ID="txtID" runat="server" CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label>Nombre proyecto</label>
                    <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label>Cliente <small>(nombre fantasía)</small></label>
                    <asp:TextBox ID="txtCliente" runat="server" CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label>Filtros (*)</label><br />
                    <asp:ListBox ID="lstEstadoDespachoFactura" runat="server" SelectionMode="Multiple" CssClass="selectpicker" data-width="100%" data-selected-text-format="count > 3" data-actions-box="true">
                    </asp:ListBox>
                    <asp:RequiredFieldValidator ID="rfvEstadoDespachoFactura" runat="server" ErrorMessage="Seleccione para buscar..." ControlToValidate="lstEstadoDespachoFactura" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Buscar" />
                </div>
            </div>
            <div class="col-lg-2">
                <div class="form-group">
                    <%--<label>Nro. Factura</label><br />
                    <asp:TextBox ID="txtNroFactura" runat="server" CssClass="form-control input-sm" autocomplete="off"></asp:TextBox>--%>
                    <div class="pull-right">
                        <br />
                        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn btn-primary" OnClick="btnBuscar_Click" ValidationGroup="Buscar" />
                    </div>
                </div>
            </div>
        </div>
        <%--<div class="row">
            <div class="col-lg-12">
                <div class="pull-right">
                    <br />
                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn btn-primary" OnClick="btnBuscar_Click" ValidationGroup="Buscar" />
                </div>
            </div>
        </div>--%>
    </div>

    <div class="form-group">
        <div class="col-lg-12">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <ul class="list-inline" style="margin-bottom:0px">
                            <li><label class="fa fa-clock-o fa-2x text-danger" style="margin-left: 4px;margin-right: 4px;">&nbsp;&nbsp;</label> Facturas por despachar</li>
                            <li><label class="fa fa-check fa-2x text-success" style="margin-left: 4px;margin-right: 4px;">&nbsp;&nbsp;</label> Facturas recepcionadas</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-12">
            <div class="row">
                <asp:Label ID="lblBuscar" runat="server"></asp:Label>
                <div class="table-responsive">
                    <asp:GridView ID="gvFactura" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-responsive" GridLines="None" OnRowDataBound="gvFactura_RowDataBound" > 
<%--                    <asp:GridView ID="gvFactura" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-responsive" GridLines="None" 
                        OnRowDataBound="gvFactura_RowDataBound" OnRowEditing="gvFactura_RowEditing" OnRowUpdating="gvFactura_RowUpdating" OnRowCancelingEdit="gvFactura_RowCancelingEdit">                        --%>
                        <Columns>
                            <asp:TemplateField HeaderText="" HeaderStyle-Width="35px">
                                <ItemTemplate>
                                    <asp:Label id="lblColor" runat="server" Text="&nbsp;&nbsp;"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Eval("Proyecto.ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nombre Proyecto">
                                <ItemTemplate>
                                    <asp:Label ID="lnkProyecto" runat="server" Text='<%# Eval("Proyecto.Nombre") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cliente <small>(nombre fantasía)</small>">
                                <ItemTemplate>
                                    <asp:Label ID="lblCliente" runat="server" Text='<%# Eval("Cliente.NombreFantasia") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fecha facturación">
                                <ItemTemplate>
                                    <asp:Label ID="lblFechaFacturacion" runat="server" Text='<%# string.Format("{0: dd/MM/yyyy HH:mm}", Eval("FechaOrdenFacturacion")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="Glosa">
                                <ItemTemplate>
                                    <asp:Label ID="lblGlosa" runat="server" Text='<%# Eval("Glosa") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Monto factura">
                                <ItemTemplate>
                                    <asp:Label ID="lblMontoFactura" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nro. factura">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkNroFactura" runat="server" Visible="false"></asp:HyperLink>
                                    <asp:Label ID="lblNroFactura" runat="server" Visible="false"></asp:Label>
                                </ItemTemplate>
                               <%-- <EditItemTemplate>
                                    <asp:TextBox ID="txtNroFactura" Text='<%# Eval("NroFactura") %>' runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtNroFactura" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtNroFactura" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Editar" />
                                </EditItemTemplate>--%>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PDF">
                                 <ItemTemplate>
		                            <div class="tooltip-wrapper-header-table pull-left" title="Visualizar documento">
			                            <asp:HyperLink ID="doc" runat="server" target="_blank" Text="<i class='fa fa-file-pdf-o' aria-hidden='true'></i>" Visible="false"></asp:HyperLink>
		                            </div> 
	                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
	                            <ItemTemplate>
                                    <div class="tooltip-wrapper-header-table pull-left" title="Editar N° Factura">
                                        <asp:HyperLink style="padding-right:25px;font-size:15px;" id="editar" runat="server" href="#" data-toggle="modal" data-target=".modalEditNroFact" data-proy='<%# Eval("Proyecto.ID") %>' data-factura='<%# Eval("NroFactura") %>' data-montofact='<%# Eval("Monto") %>' data-tipovalor='<%# Eval("Proyecto.TipoValor.ID") %>' class="fa fa-edit"></asp:HyperLink>
                                    </div> 
                             </ItemTemplate>
                             </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>


<%--Modal Editar usuario --%>
    <div class="modal modalEditNroFact fade" role="dialog" aria-labelledby="tituloFact" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div id="edicion" runat="server" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h2 class="modal-title" id="tituloFact"></h2>
                        <p>Actualice el número de Factura.</p>
					</div>  
					<div class="modal-body">
						<div class="row">
							<div class="form-group">
								<div class="col-lg-6">
									   <div class="form-group">
											<label class="control-label"> N° Factura</label>
											<br />
											<asp:TextBox ID="txtNroFact" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
											<asp:RequiredFieldValidator ID="rfvNroFact" runat="server" CssClass="help-block" ErrorMessage="Ingrese N° Factura..." Display="Dynamic" ControlToValidate="txtNroFact" SetFocusOnError="True" ValidationGroup="editar" />
										</div>	       
                                      <%--  <div class="form-group">
											<label class="control-label"> Fecha Modificación</label>
											<br />
											<asp:TextBox ID="txtFechaM" runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off" placeholder="Ingrese..."></asp:TextBox>
											<asp:RequiredFieldValidator ID="rfvFechaM" runat="server" CssClass="help-block" ErrorMessage="Ingrese correo..." Display="Dynamic" ControlToValidate="txtFechaM" SetFocusOnError="True" />
										</div>--%>
									</div>
								</div>
							</div>
						</div>
					</div>
                <div id="modalfooterE" runat="server" class="modal-footer">
                    <button id="btnGuardar" runat="server" type="button" onserverclick="btnGuardar_ServerClick" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-pulse fa-fw'></i><span class='sr-only'>Cargando...</span> Editando factura..." validationgroup="editar">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;&nbsp;Guardar
                    </button>
<%--                    <asp:Button ID="Button1" runat="server" Text="Buscar" CssClass="btn btn-primary" OnClick="btnBuscar_Click" ValidationGroup="Buscar" />--%>
                    <button id="btnCerrarE" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
            </div>
      </div>

    <%--Abre modal editar usuario--%>
    <script type="text/javascript">
        $(document).ready(function () {

            //carga modal al abrirlo
            $('.modalEditNroFact').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var proy = button.data('proy');
                var factura = button.data('factura');
                //Para que visualmente en el gridwiew y modal salga cero cuando no tiene número de factura
                if (factura == "-1") {
                    factura = "0";
                } else {
                    factura = factura;
                }

                var montofact = button.data('montofact');
                var tipovalor = button.data('tipovalor');
                var modal = $(this);

                //establece título
                $('#tituloFact').text('Proy- ' + proy);

                //almacena valores antes de editar el formulario
                $(<%= hfRowId.ClientID %>).val(button.context.id)
                $(<%= hfProy.ClientID %>).val(proy);
                $(<%= hfmontofact.ClientID %>).val(montofact);
                $(<%= hftipovalor.ClientID %>).val(tipovalor);

                //asigna valores al formulario
                modal.find('#<%= txtNroFact.ClientID%>').val(factura);
            });

            $('.modalMessage').on('hidden.bs.modal', function (e) {
                var accion = $(<%= hfModalAccion.ClientID %>).val();

                if (accion !== "") {
                    var rowAfectada = $("#" + $(<%= hfRowAfectada.ClientID %>).val());


                    if (accion == "Editar") {
                        $(rowAfectada).addClass("bg-info").fadeOut(200).fadeIn(1000);

                        //datos nuevos de la edición

                        var factura = $("[id*=txtNroFact]").val();
                        

                        $(rowAfectada).find('td:eq(5)').text(nombre); <%--Fecha emisión--%>
                       
                    }

                    $(<%= hfModalAccion.ClientID %>).val("");
                }
            });

            //función actualiza documento y fila de gridview
            $('#<%= btnGuardar.ClientID %>').on('click', function () {
                var $this = $(this);
                var validationGroup = $this.attr("validationgroup");
                if (Page_ClientValidate(validationGroup)) {
                    $this.button('loading');
                    //lógica
                    $.ajax({
                        type: "POST",
                        url: "factura.aspx/SetEditarFactura",
                        data: "{'proyecto': '" + $(<%= hfProy.ClientID %>).val() + "', 'factura': '" + $("#<%=txtNroFact.ClientID%>").val() + "', 'monto': '" + parseFloat(ConvertToNumber($(<%= hfmontofact.ClientID %>).val())) + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            alert("Edición Exitosa");
                        }

                    });
                }
            });

        });
   </script>

    <asp:HiddenField id="hfRowId" runat="server"/>
    <asp:HiddenField id="hfProy" runat="server"/>
    <asp:HiddenField id="hfmontofact" runat="server"/>
    <asp:HiddenField id="hfModalAccion" runat="server"/>
    <asp:HiddenField id="hftipovalor" runat="server"/> 
    <asp:HiddenField id="hfRowAfectada" runat="server"/>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-fieldvalidator.v1.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/bootstrap-showMessageModal.v1.js") %>'></script>
</asp:Content>

﻿<%@ Page Title="Bitacora de Facturas Emitidas del Proyecto" Language="C#" MasterPageFile="~/makeup2/proyecto/visualizar/proyecto.master" AutoEventWireup="true" CodeBehind="facturacion.aspx.cs" Inherits="MakeUp.App.UI.makeup2.proyecto.visualizar.facturacion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headProyecto" runat="server">
    <link href="../../../css/bootstrap-select.css" rel="stylesheet" />
    <link href="../../../css/personalizado.css" rel="stylesheet" />
    <link href="../../../css/bootstrap-datetimepicker.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenidoProyecto" runat="server">
     <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-datetimepicker.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/numeral.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/languages/es.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/count-character-textbox.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/number-format-using-numeral.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/personalizado/dropdown-igualitario.js") %>'></script>
    <asp:HiddenField ID="hfIdProyecto" runat="server" />

    <div class="form-group">
        <div class="col-lg-12">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <ul class="list-inline" style="margin-bottom:0px">
                            <li><label class="fa fa-clock-o fa-2x text-danger" style="margin-left: 4px;margin-right: 4px;">&nbsp;&nbsp;</label> Facturas por despachar</li>
                            <li><label class="fa fa-check fa-2x text-success" style="margin-left: 4px;margin-right: 4px;">&nbsp;&nbsp;</label> Facturas recepcionadas</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="form-group">
        <div class="col-lg-12">
            <div class="row">
                <asp:Label ID="lblBuscar" runat="server"></asp:Label>
                <div class="table-responsive">
                    <asp:GridView ID="gvFactura" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-responsive" GridLines="None" OnRowDataBound="gvFactura_RowDataBound" > 
                        <Columns>
                            <asp:TemplateField HeaderText="" HeaderStyle-Width="35px">
                                <ItemTemplate>
                                    <asp:Label id="lblColor" runat="server" Text="&nbsp;&nbsp;"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cliente <small>(nombre fantasía)</small>">
                                <ItemTemplate>
                                    <asp:Label ID="lblCliente" runat="server" Text='<%# Eval("Cliente.NombreFantasia") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fecha facturación">
                                <ItemTemplate>
                                    <asp:Label ID="lblFechaFacturacion" runat="server" Text='<%# string.Format("{0: dd/MM/yyyy HH:mm}", Eval("FechaOrdenFacturacion")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Glosa">
                                <ItemTemplate>
                                    <asp:Label ID="lblGlosa" runat="server" Text='<%# Eval("Glosa") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Monto factura">
                                <ItemTemplate>
                                    <asp:Label ID="lblMontoFactura" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nro. factura">
                                <ItemTemplate>
                                    <asp:Label ID="lblNroFactura" runat="server" Text='<%# Eval("NroFactura")%>'></asp:Label>
                                </ItemTemplate>
                               <%-- <EditItemTemplate>
                                    <asp:TextBox ID="txtNroFactura" Text='<%# Eval("NroFactura") %>' runat="server" CssClass="form-control input-sm" MaxLength="100" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtNroFactura" runat="server" ErrorMessage="Campo obligatorio..." ControlToValidate="txtNroFactura" Display="Dynamic" SetFocusOnError="True" CssClass="alert-danger" ValidationGroup="Editar" />
                                </EditItemTemplate>--%>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PDF">
                                 <ItemTemplate>
		                            <div class="tooltip-wrapper-header-table pull-left" title="Visualizar documento">
			                            <asp:HyperLink ID="doc" runat="server" target="_blank" Text="<i class='fa fa-file-pdf-o' aria-hidden='true'></i>" Visible="false"></asp:HyperLink>
		                            </div> 
	                            </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
 </asp:Content>
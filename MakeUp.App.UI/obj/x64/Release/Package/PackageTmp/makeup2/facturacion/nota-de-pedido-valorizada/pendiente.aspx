﻿<%@ Page Title="Facturar Notas de Pedido Valorizadas" Language="C#" MasterPageFile="~/makeup2/facturacion/factura-pendiente.master" AutoEventWireup="true" CodeBehind="pendiente.aspx.cs" Inherits="MakeUp.App.UI.makeup2.facturacion.nota_de_pedido_valorizada.pendiente" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headFacturas" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenidoFacturas" runat="server">
    <p>Listado de proyectos con <b>Notas de Pedido Valorizadas</b> que están pendiente su facturación.</p>
    <asp:Label ID="lblContador" runat="server"></asp:Label>
    <div class="table-responsive">
        <asp:GridView ID="gvNPValorizada" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None"
            OnRowDataBound="gvNPValorizada_RowDataBound">
            <Columns>
                <asp:TemplateField HeaderText="ID">
                    <ItemTemplate>
                        <%# Eval("ID") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Nombre Proyecto">
                    <ItemTemplate>
                        <%# Eval("Nombre") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Cliente <small>(nombre fantasía)</small>">
                    <ItemTemplate>
                        <a href='<%# string.Format("detalle.aspx?id={0}", Eval("ID")) %>'><%# Eval("Cliente.NombreFantasia") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="NP Facturadas / Total NP">
                    <ItemTemplate>
                        <%# Eval("CantidadNPFacturada") %> / <%# Eval("CantidadTotalNP") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Valorizado Pendiente">
                    <ItemTemplate>
                        <asp:Label ID="lblPendiente" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Fecha Planificada">
                    <ItemTemplate>
                        <%# string.Format("{0:dd/MM/yyyy}", Eval("FechaPlanificada")) %>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
﻿<%@ Page Title="Release Note" Language="C#" MasterPageFile="~/makeup2/menu.Master" AutoEventWireup="true" CodeBehind="release.aspx.cs" Inherits="MakeUp.App.UI.makeup2.release" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headMenu" runat="server">
        <link href="../css/bootstrap-select.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <h2 class="page-header">Release Notes</h2>
     <div class="table-responsive small">
        <asp:GridView ID="gvRelease" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None" >
            <Columns>
                <asp:TemplateField HeaderText="Fecha Versión">
                    <ItemTemplate>
                       <%# string.Format("{0:dd/MM/yyyy}", Eval("FECHA")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Versión">
                    <ItemTemplate>
                       <%# Eval("DESCRIPCION") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Detalle">
                    <ItemTemplate>
                       <%# Eval("OBSERVACION") %>
                    </ItemTemplate>
                </asp:TemplateField>
                </Columns>
        </asp:GridView>
    </div>
</asp:Content>
﻿<%@ Page Title="Notificaciones (Configuración)" Language="C#" MasterPageFile="~/makeup2/menu.master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="MakeUp.App.UI.makeup2.admin.notificaciones.index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headMenu" runat="server">
    <link rel="stylesheet" href="../../../css/bootstrap-select.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script type="text/javascript">
        function mantieneAbiertoFiltro(idListRol) {
            $('#' + idListRol.id).selectpicker('toggle');
        }

        function reaparecer() {
            $('.selectpicker').selectpicker('toggle');
        }

        function mostrarFiltro(idListRol) {
            $('#' + idListRol.id).selectpicker('render');
        }
    </script>

    <script>
        //Mantiene las alertas ocultas. Desde el servidor se muestra el mensaje.
        $(document).ready(function () {
            $('[id*=success]').hide();
        });
    </script>

    <h2 class="page-header">Notificaciones del sistema (configuración)</h2>
    <p>Según notificación, seleccione y agregue uno o más roles para alertar cuando se ejecute el evento.</p>

    <div class="form-group">
        <div class="col-lg-12">
            <div class="row">
                <asp:Label ID="lblContadorNotificacion" runat="server"></asp:Label>
                <div class="table-responsive" style="margin-top:15px;overflow:visible"> <%--overflow: visible hace que ListBox no se oculte detras del div --%>
                    <asp:GridView ID="gvNotificacion" runat="server" AutoGenerateColumns="false" GridLines="None" ShowHeader="false" Width="100%"
                            OnRowDataBound="gvNotificacion_RowDataBound">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 id="nombreNotificacion" runat="server" class="panel-title"></h3>
                                        </div>
                                        <asp:UpdatePanel ID="upAgregarRol" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="panel-body">
                                                    <div class="form-group">
                                                        Seleccione rol a notificar
                                                        <asp:ListBox ID="lstRol" runat="server" SelectionMode="Multiple" CssClass="selectpicker" DataValueField="ID" DataTextField="Nombre" AutoPostBack="true" OnSelectedIndexChanged="lstRol_SelectedIndexChanged" data-width="fit" data-selected-text-format="count > 3"></asp:ListBox>
                                                    </div>
                                                    <asp:Label ID="lblContadorRol" runat="server"></asp:Label>
                                                    <div class="table-responsive" style="margin-top:15px">
                                                        <asp:GridView ID="gvRolEnNotificacion" runat="server" CssClass="table table-hover" AutoGenerateColumns="false" GridLines="None"
                                                            OnRowDataBound="gvRolEnNotificacion_RowDataBound">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Roles añadidos a la notificación">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblRol" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Filtro" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:UpdatePanel ID="upFiltro" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:ListBox ID="lstFiltro" runat="server" SelectionMode="Single" CssClass="selectpicker" data-style="btn-xs" data-container="body" DataValueField="ID" DataTextField="Nombre" AutoPostBack="true" OnSelectedIndexChanged="lstFiltro_SelectedIndexChanged" data-width="auto"></asp:ListBox>
                                                                                <span id="success" runat="server" style="display:none;position:absolute;margin-left:4px">                                                                                    
                                                                                    <i class="fa fa-check alert-success"></i> Listo!
                                                                                </span>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="lstFiltro" EventName="SelectedIndexChanged" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                    <%--<asp:Button ID="btnAgregarRol" runat="server" Text="Agregar Rol" CssClass="btn btn-info btn-sm" />--%>

                                                    <%--<asp:Button ID="btnAgregarRol" runat="server" Text="Agregar Rol" CssClass="btn btn-info btn-sm" CommandName="AgregarRol" CommandArgument='<%# Container.DataItemIndex %>'/>
                                                    <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn btn-primary btn-sm" Visible="false" CommandName="GuardarRol" CommandArgument='<%# Container.DataItemIndex %>'/>
                                                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn btn-default btn-sm" Visible="false" CommandName="CancelarOperacion" CommandArgument='<%# Container.DataItemIndex %>'/>--%>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="lstRol" EventName="SelectedIndexChanged"/>
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        
                                        <asp:UpdateProgress ID="x" runat="server" AssociatedUpdatePanelID="upAgregarRol">
                                            <ProgressTemplate>
                                                    Cargando...
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

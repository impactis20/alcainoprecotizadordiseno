﻿<%@ Page Language="C#" MasterPageFile="~/makeup2/menu.master" AutoEventWireup="true" CodeBehind="accion1.aspx.cs" Inherits="MakeUp.App.UI.makeup2.admin.Integracion.accion1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headMenu" runat="server">
     <link href="../../css/bootstrap-typeahead.css" rel="stylesheet" />
    <link rel="stylesheet" href="../../css/bootstrap-select.css" />
    <link href="../../css/fileinput.min.4.4.2.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
     <script src='<%= ResolveUrl("~/js/autocomplete-query.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap3-typeahead.min.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/bootstrap-select.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/numeral.min.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/languages/es.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/moment-with-locales.js") %>' ></script>
    <script src='<%= ResolveUrl("~/js/fileinput.min.4.4.2.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/jquery.dataTables.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/js/app/activaReferencia.v1.js") %>'></script>
        <h2 class="page-header">Logs de Procedimientos de Integración</h2>
    <%--<p>Lista de estado de integraciones procedimiento <strong>z_ma_addCentroCosto</strong>   &nbsp <a href="index.aspx" title="Ir la página anterior">Volver</a></p>--%>
    <p><strong>Reproceso de Integracion</strong> &nbsp <a href="index.aspx" title="Ir la página anterior"><strong>Volver</strong></a></p>
        <div class="form-group">
           <%--<asp:GridView id="DataGrid1" ClientIDMode="Static" runat="server" AutoGenerateColumns="False"  CssClass="table table-striped table-hover table-condensed small-top-margin">
                <Columns>
                    <asp:BoundField DataField="Parametros" HeaderText="Parametros" ><HeaderStyle Width="15%" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Valores" HeaderText="Valores" />
                </Columns>
           </asp:GridView>--%>
           <asp:datagrid id="DataGrid1" runat="server" CssClass="table table-bordered">
           </asp:datagrid>
           <asp:datagrid id="DataGrid2" runat="server" CssClass="table table-bordered">
           </asp:datagrid>
           <asp:datagrid id="DataGrid3" runat="server" CssClass="table table-bordered">
           </asp:datagrid>
           <asp:datagrid id="DataGrid4" runat="server" CssClass="table table-bordered">
           </asp:datagrid>
           <asp:datagrid id="DataGrid5" runat="server" CssClass="table table-bordered">
           </asp:datagrid>
           <asp:datagrid id="DataGrid6" runat="server" CssClass="table table-bordered">
           </asp:datagrid>
           <asp:datagrid id="DataGrid7" runat="server" CssClass="table table-bordered">
           </asp:datagrid>
           <asp:datagrid id="DataGrid8" runat="server" CssClass="table table-bordered">
           </asp:datagrid>
             <asp:datagrid id="DataGrid9" runat="server" CssClass="table table-bordered">
           </asp:datagrid>
             <asp:datagrid id="DataGrid10" runat="server" CssClass="table table-bordered">
           </asp:datagrid>
             <asp:datagrid id="DataGrid11" runat="server" CssClass="table table-bordered">
           </asp:datagrid>
             <asp:datagrid id="DataGrid12" runat="server" CssClass="table table-bordered">
           </asp:datagrid>
        </div>

 <div class="col-lg-2" style="margin-top:27px;">
        <div class="form-group">
                <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary" Text="Ejecutar" OnClick="Button1_Click" />
            </div>
              <div class="form-group">
                <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            </div>
        </div>
</asp:Content>

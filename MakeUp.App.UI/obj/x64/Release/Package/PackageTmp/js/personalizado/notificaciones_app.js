﻿$(function () {
    var notificacion = $.connection.notificacionAppHub;

    notificacion.client.cargarNotificaciones = function (lstNotificaciones, contNotificacionesSinVer) {
        
        //indica el número de notificaciones sin ver
        if (contNotificacionesSinVer > 0) {
            $('#contNotificacionesPendientes').text(contNotificacionesSinVer);
            $('#contNotificacionesPendientes').show();
        }
        else
            $('#contNotificacionesPendientes').hide();
        
        //limpia las notificaciones
        $('#notificacion').empty();

        lstNotificaciones.forEach(function (notificacion) {

            var date = new Date(notificacion.FechaEvento);
            var day = date.getDate();       // yields date
            var month = date.getMonth() + 1;    // yields month (add one as '.getMonth()' is zero indexed)
            var year = date.getFullYear();  // yields year
            var hour = date.getHours();     // yields hours 
            var minute = date.getMinutes(); // yields minutes
            var classUrl = "notificacion";

            if (notificacion.Ingreso === false)
                classUrl = " no-vista";

            var notificacionHtml = '<li data-id-notificacion="' + notificacion.ID + '">' +
                                     '<a class="' + classUrl + '" href="' + notificacion.Url + '">' +
                                       '<div>' +
                                         //'<div>' +
                                         //   '<i class="fa fa-bell-o fa-fw"></i>' +
                                         //   '<i class="font-notificacion">' +
                                               
                                         //   '</i>' +
                                         //'</div>' +
                                         '<div class="font-notificacion">' +
                                            '<strong>' + notificacion.UsuarioResponsable.Nombre + ' ' + notificacion.UsuarioResponsable.ApellidoPaterno + '</strong>' + notificacion.MensajeNotificado +
                                         '</div>' +
                                         '<div class="font-hora">' +
                                            day + '-' + month + '-' + year + ' a las ' + hour + ':' + minute + ' hrs.' +
                                         '</div>' 
                                       '</div>' 
                                     '</a>' +
                                   '</li>';

            //agrega notificación
            $('#notificacion').append(notificacionHtml);

            //agrega un divisor de notificación
            var divisorNotificacionHtml = '<li class="divider"></li>';
            $('#notificacion').append(divisorNotificacionHtml);
        });

        
        var verTodasNotificacionesHtml = '<li>' +
                                           //'<div class="tooltip-wrapper" data-title="En construcción...">' +
                                             '<a class="text-center" href="/makeup2/mis-notificaciones.aspx">' +
                                               'Ver todas mis notificaciones...' +
                                               //'<i class="fa fa-angle-right"></i>' +
                                             '</a>' +
                                           //'</div>' +
                                         '</li>';
        //inserta link para visualizar todas las notificaciones 
        $('#notificacion').append(verTodasNotificacionesHtml);
    }

    //Oculta globo de notificaciones
    notificacion.client.ocultaGloboDeNotificaciones = function () {
        $('#contNotificacionesPendientes').hide();
    };

    //Al dar clic en el botón de notificaciones invoca método que señala que ya las vió
    $('#iconoNotificacion').click(function () {
        var icononotificacionVisible = $("#contNotificacionesPendientes").is(":visible");

        if (icononotificacionVisible && this.className == "dropdown") {
            $.connection.hub.start().done(function () {
                notificacion.server.notificacionesVisualizadas();
            });
        }
    });

    //Cambia el estado de la notificación a Visto (si es que está como NO visto)
    $('ul#notificacion').on('click', "li", function () {
        var idNotificicacionApp = $(this).attr('data-id-notificacion');
        //$(this).children('a').removeClass("no-vista"); //remueve el estilo "no visto"

        $.connection.hub.start().done(function () {
            notificacion.server.ingresoNotificacionApp(idNotificicacionApp);
        });
    });

    //Realiza conexión
    $.connection.hub.start().done(function () {
        notificacion.server.connect();
    });
});

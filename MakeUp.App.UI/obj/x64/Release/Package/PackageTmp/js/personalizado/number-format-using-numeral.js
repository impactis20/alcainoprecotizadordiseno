﻿//formatea textbox numéricos
function FormatoNumerico(textbox, cantidadDecimal) {
    numeral.language('es'); //asigna idioma a numeral
    var valor = textbox.value;
    var decimales = '';
    var valorEntero = valor.split(',')[0]; //obtiene parte entera
    valorEntero = numeral(valorEntero).unformat(valorEntero); //quita formato a la parte entera

    //variable almacenan posición del cursor
    //var start = textbox.selectionStart,
    //    end = textbox.selectionEnd;
    
    if (cantidadDecimal !== 0 && valor.indexOf(",") !== -1) {
        if (valor.substr(valor.length - 1) !== ',') {
            var valorDecimal = ',' + valor.split(',')[1]; //obtiene parte decimal

            //crea cantidad de decimales según parametro "cantidadDecimal"
            for (var x = 0; x < cantidadDecimal; x++)
                decimales += '0'

            if(valorDecimal === ',0')
                textbox.value = numeral(valorEntero + valorDecimal).format('0,0.0');
            else
                textbox.value = numeral(valorEntero + valorDecimal).format('0,0.[' + decimales + ']');
        }
    }
    else {
        textbox.value = numeral(valorEntero).format('0,0');
    }
    
    //contador de separador de mil
    //var preContadorPuntoIzquierda = valor.substr(0, start).split(".").length - 1;
    //var postContadorPuntoIzquierda = textbox.value.substr(0, start).split(".").length - 1

    //if (preContadorPuntoIzquierda > postContadorPuntoIzquierda)
    //    start, end -= preContadorPuntoIzquierda - postContadorPuntoIzquierda;
    //else
    //    start, end -= postContadorPuntoIzquierda - preContadorPuntoIzquierda;

    // restore from variables...

    //if(valor.length === textbox.value.length)
    //    textbox.setSelectionRange(start, end)
    //else {
    //    //if(valor.length > textbox.value.length)
    //    //    start, end -= valor.length - textbox.value.length
    //    //else
    //        start, end -= textbox.value.length - valor.length
    //}    
}

//quita el separador de miles y el decimal (,) lo cambia por un punto (.)
function ConvertToNumber(value) {
    if (value !== undefined)
        return value.replace(/\./g, "").replace(/\,/g, ".");
    else
        return '';
}
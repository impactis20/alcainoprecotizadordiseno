﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.Entities
{
    [Serializable]
    public class TipoUnidadEntity
    {
        public TipoUnidadEntity()
        {

        }

        public TipoUnidadEntity(int ID)
        {
            this.ID = ID;
        }

        public TipoUnidadEntity(string Sigla)
        {
            this.Sigla = Sigla;
        }

        public TipoUnidadEntity(int ID, string Nombre)
        {
            this.ID = ID;
            this.Nombre = Nombre;
        }

        public TipoUnidadEntity(int ID, string Nombre, string Sigla)
        {
            this.ID = ID;
            this.Nombre = Nombre;
            this.Sigla = Sigla;
        }

        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Sigla { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.Entities
{
    [Serializable]
    public class ProyectoEntity
    {
        public ProyectoEntity() { }

        public ProyectoEntity(int ID)
        {
            this.ID = ID;
        }

        public ProyectoEntity(int ID, string Nombre)
        {
            this.ID = ID;
            this.Nombre = Nombre;
        }

        public ProyectoEntity(TipoValorEntity TipoValor)
        {
            this.TipoValor = TipoValor;
        }

        public ProyectoEntity(int ID, string Nombre, string NombreFantasia, int CantidadTotalEP, int CantidadEPFacturado, decimal EstadoPagoPendiente)
        {
            this.ID = ID;
            this.Nombre = Nombre;
            Cliente = new ClienteEntity(NombreFantasia);
            this.CantidadTotalEP = CantidadTotalEP;
            this.CantidadEPFacturado = CantidadEPFacturado;
            this.EstadoPagoPendiente = EstadoPagoPendiente;
        }

        public ProyectoEntity(int ID, string Nombre, string NombreFantasia, int CantidadNPFacturada, int CantidadTotalNP, decimal ValorizadoPendiente, DateTime FechaPlanificada, decimal Tiempo)
        {
            this.ID = ID;
            this.Nombre = Nombre;
            Cliente = new ClienteEntity(NombreFantasia);
            this.CantidadNPFacturada = CantidadNPFacturada;
            this.CantidadTotalNP = CantidadTotalNP;
            this.ValorizadoPendiente = ValorizadoPendiente;
            this.FechaPlanificada = FechaPlanificada;
            this.Tiempo = Tiempo;
        }

        public ProyectoEntity(int ID, string Nombre, string NombreFantasia, int CantidadNPFacturada, int CantidadTotalNP, decimal ValorizadoPendiente, DateTime FechaPlanificada)
        {
            this.ID = ID;
            this.Nombre = Nombre;
            Cliente = new ClienteEntity(NombreFantasia);
            this.CantidadNPFacturada = CantidadNPFacturada;
            this.CantidadTotalNP = CantidadTotalNP;
            this.ValorizadoPendiente = ValorizadoPendiente;
            this.FechaPlanificada = FechaPlanificada;
        }

        public ProyectoEntity(int ID, string Nombre, string NombreFantasia, int CantidadNPFacturada, int CantidadTotalNP, decimal ValorizadoPendiente, DateTime FechaPlanificada, string Motivo)
        {
            this.ID = ID;
            this.Nombre = Nombre;
            Cliente = new ClienteEntity(NombreFantasia);
            this.CantidadNPFacturada = CantidadNPFacturada;
            this.CantidadTotalNP = CantidadTotalNP;
            this.ValorizadoPendiente = ValorizadoPendiente;
            this.FechaPlanificada = FechaPlanificada;
            this.Motivo = Motivo;
        }

        public int ID { get; set; }
        public string Nombre { get; set; }
        public decimal Tiempo { get; set; }
        public int CantidadTotalNP { get; set; }
        public int CantidadNPPorAjustar { get; set; }
        public int CantidadNPPorValorizar { get; set; }
        public int CantidadNPPorFacturar { get; set; }
        public int CantidadNPFacturada { get; set; }

        public int CantidadEPEmitidaFacturacion { get; set; } //Enviadas a manager y que aún no han sido facturadas
        public int CantidadEPFacturado { get; set; } //
        public int CantidadEPPorFacturar { get; set; }
        public int CantidadTotalEP { get; set; } //
        public float PorcentajeTotalEP { get; set; }
        
        public DateTime FechaIngreso { get; set; }
        public DateTime FechaPlanificada { get; set; }
        public DateTime FechaComprometida { get; set; }
        public string Direccion { get; set; }
        public string Comuna { get; set; }
        public string Motivo { get; set; }
        public string Directorio { get; set; }
        public ProyectoEntity Padre { get; set; }
        public TipoValorEntity TipoValor { get; set; }
        public ClienteEntity Cliente { get; set; }
        public UsuarioEntity EjecutivoComercial { get; set; }
        public UsuarioEntity SupervisorMontaje { get; set; }
        public decimal EstadoPagoPendiente { get; set; } //
        public decimal ValorizadoPendiente { get; set; }
        public decimal ValorTotal { get; set; }
        public bool Estado { get; set; }
        //falta usuario (creador)
    }

    [Serializable]
    public class DocumentoComercialEntity
    {
        public DocumentoComercialEntity() { }

        public DocumentoComercialEntity(int ID)
        {
            this.ID = ID;
        }

        public DocumentoComercialEntity(int ID, string Descripcion)
        {
            this.ID = ID;
            this.Descripcion = Descripcion;
        }

        public DocumentoComercialEntity(int ID, string Nombre, bool Seleccionado)
        {
            this.ID = ID;
            this.Nombre = Nombre;
            SeleccionadoParaNP = Seleccionado;
        }

        public DocumentoComercialEntity(int ID, string Descripcion, int IDTipoDoc)
        {
            this.ID = ID;
            this.Descripcion = Descripcion;
            TipoDocumento = new TipoDocumentoEntity(IDTipoDoc);
        }

        public int ID { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaSubida { get; set; }
        public string Descripcion { get; set; }
        public ProyectoEntity Proyecto { get; set; }
        public TipoDocumentoEntity TipoDocumento { get; set; }
        public bool SeleccionadoParaNP { get; set; }
        //falta usuarios (creador)
    }

    [Serializable]
    public class TipoDocumentoEntity
    {
        public TipoDocumentoEntity()
        {
        }

        public TipoDocumentoEntity(int ID)
        {
            this.ID = ID;
        }

        public TipoDocumentoEntity(int ID, string Nombre)
        {
            this.ID = ID;
            this.Nombre = Nombre;
        }

        public int ID { get; set; }
        public string Nombre { get; set; }
        public bool VinculaOC { get; set; }
    }

    public class DocumentoProyectoEntity
    {
        public DocumentoProyectoEntity(int ID, string Desc, string Url)
        {
            this.ID = ID;
            Descripcion = Desc;
            this.Url = Url;
        }

        public int ID { get; set; }
        public ProyectoEntity Proyecto { get; set; }
        public string Descripcion { get; set; }
        public string Url { get; set; }
        public DateTime Fecha { get; set; }
    }

    public class TipoValorEntity
    {
        public TipoValorEntity()
        {

        }

        public TipoValorEntity(int ID)
        {
            this.ID = ID;
        }

        public TipoValorEntity(int ID, string Sigla)
        {
            this.ID = ID;
            this.Sigla = Sigla;
        }

        public TipoValorEntity(int ID, string Nombre, string Sigla)
        {
            this.ID = ID;
            this.Nombre = Nombre;
            this.Sigla = Sigla;
        }

        public TipoValorEntity(int ID, string Sigla, decimal Valor)
        {
            this.ID = ID;
            this.Sigla = Sigla;
            this.Valor = Valor;
        }

        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Sigla { get; set; }
        public DateTime FechaUltimaActualizacion { get; set; }
        public decimal Valor { get; set; }
    }

    public class HistorialProyectoEntity
    {
        public HistorialProyectoEntity(string Icono, string Nombre, string ApellidoPaterno, string Descripcion, DateTime Fecha)
        {
            this.Icono = Icono;
            Usuario = new UsuarioEntity(Nombre, ApellidoPaterno);
            this.Descripcion = Descripcion;
            this.Fecha = Fecha;
        }

        public string Icono { get; set; }
        public UsuarioEntity Usuario { get; set; }
        public string Descripcion { get; set; }
        public DateTime Fecha { get; set; }
    }

    
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.Entities
{
    public class PaginaEntity
    {
        public PaginaEntity()
        {

        }

        public PaginaEntity(int ID)
        {
            this.ID = ID;
            Controles = new List<Control>();
        }

        //public PaginaEntity(int ID, string Nombre, string Uri, bool PermitidoPorDefecto)
        //{
        //    this.ID = ID;
        //    this.Nombre = Nombre;
        //    this.Uri = Uri;
        //    Controles = new List<Control>();
        //    this.PermitidoPorDefecto = PermitidoPorDefecto;
        //}

        //public PaginaEntity(int ID, string Nombre, string Uri, bool Seleccionado)
        //{
        //    this.ID = ID;
        //    this.Nombre = Nombre;
        //    this.Uri = Uri;
        //    Controles = new List<Control>();
        //    this.Seleccionado = Seleccionado;
        //}

        public PaginaEntity(int ID, string Nombre, string Uri, bool Seleccionado, bool PermitidoPorDefecto)
        {
            this.ID = ID;
            this.Nombre = Nombre;
            this.Uri = Uri;
            Controles = new List<Control>();
            this.Seleccionado = Seleccionado;
            this.PermitidoPorDefecto = PermitidoPorDefecto;
        }

        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Uri { get; set; }
        public List<Control> Controles { get; set; }
        public bool Seleccionado { get; set; } //Seleccionado según Rol predeterminado guardado
        public bool PermitidoPorDefecto { get; set; } //Utilizado para el encabezado del proyecto, ya que no es una página y cualquier usuario puede acceder

        public class Control
        {
            public Control(int ID)
            {
                this.ID = ID;
            }

            public Control(string ControlID)
            {
                this.ControlID = ControlID;
            }

            public Control(int ID, string ControlID, string Descripcion)
            {
                this.ID = ID;
                this.ControlID = ControlID;
                this.Descripcion = Descripcion;
            }

            public Control(int ID, string ControlID, string Descripcion, bool Seleccionado)
            {
                this.ID = ID;
                this.ControlID = ControlID;
                this.Descripcion = Descripcion;
                this.Seleccionado = Seleccionado;
            }

            public int ID { get; set; }
            public string ControlID { get; set; }
            public string Descripcion { get; set; }
            public bool Seleccionado { get; set; } //Seleccionado según Rol predeterminado guardado
        }   
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.Entities
{
    [Serializable]
    public class EstadoPagoEntity
    {
        public EstadoPagoEntity()
        {

        }

        public int ID { get; set; }
        public decimal Porcentaje { get; set; }
        public decimal Monto { get; set; }
        public string Glosa { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaPreFacturado { get; set; }
        public bool Facturado { get; set; }
        public bool FacturaEmitida { get; set; }
        //public UsuarioEntity UsuarioFacturador { get; set; }
    }
}

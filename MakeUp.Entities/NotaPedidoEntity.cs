﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.Entities
{
    [Serializable]
    public class NotaPedidoEntity
    {
        public NotaPedidoEntity() {
            Valorizacion = new ValorizacionEntity();
        }

        public NotaPedidoEntity(int ID)
        {
            this.ID = ID;
        }

        public NotaPedidoEntity(int ID, int IDEstadoNP)
        {
            this.ID = ID;
            this.IDEstadoNP = IDEstadoNP;
        }

        public NotaPedidoEntity(int ID, decimal Cantidad, decimal Valor) {
            this.ID = ID;
            this.Cantidad = Cantidad;
            this.Valor = Valor;

        }

        public NotaPedidoEntity(int IDNP, string IDProyecto, string NroNP)
        {
            ID = IDNP;
            IDCompuesta = IDProyecto + "-" + NroNP;
        }

        public NotaPedidoEntity(int IDNP, string IDProyecto, string NroNP, int IDTipoNP, string DescripNP)
        {
            ID = IDNP;
            IDCompuesta = IDProyecto + "-" + NroNP;
            tipoNP = IDTipoNP;
            NotaPedido = new TipoNPEntity(DescripNP);
            NotaPedido = NotaPedido;
        }


        public NotaPedidoEntity(int IDNP, string IDProyecto, string NroNP, int IDTipoNP)
        {
            ID = IDNP;
            IDCompuesta = IDProyecto + "-" + NroNP;
            tipoNP = IDTipoNP;
        }

        public NotaPedidoEntity(int IDNP, string IDProyecto, string NroNP, int IDEstadoNP, bool EstadoNP)
        {
            ID = IDNP;
            IDCompuesta = IDProyecto + "-" + NroNP;
            this.IDEstadoNP = IDEstadoNP;
            Estado = EstadoNP;
        }

        public NotaPedidoEntity(int IDNP, string IDProyecto, string NroNP, int IDEstadoNP, int IDTipoNP)
        {
            ID = IDNP;
            IDCompuesta = IDProyecto + "-" + NroNP;
            this.IDEstadoNP = IDEstadoNP;
            tipoNP = IDTipoNP;
        }

        public NotaPedidoEntity(int IDNP, string IDProyecto, string NroNP, int IDEstadoNP, bool EstadoNP, int IDTipoNP)
        {
            ID = IDNP;
            IDCompuesta = IDProyecto + "-" + NroNP;
            this.IDEstadoNP = IDEstadoNP;
            Estado = EstadoNP;
            tipoNP = IDTipoNP;
        }

        public NotaPedidoEntity(int IDNP, string IDProyecto, string NroNP, int IDEstadoNP, bool EstadoNP, string NombreTipoNP)
        {
            ID = IDNP;
            IDCompuesta = IDProyecto + "-" + NroNP;
            this.IDEstadoNP = IDEstadoNP;
            Estado = EstadoNP;
            //nombreTipoNP = NombreTipoNP;
            NotaPedido = new TipoNPEntity(NombreTipoNP);
            //NotaPedido = NotaPedido;
        }

        public NotaPedidoEntity(int IDNP, string IDProyecto, string NroNP, int IDEstadoNP, bool EstadoNP, int IDTipoNP, string NombreTipoNP)
        {
            ID = IDNP;
            IDCompuesta = IDProyecto + "-" + NroNP;
            this.IDEstadoNP = IDEstadoNP;
            Estado = EstadoNP;
            tipoNP = IDTipoNP;
            //nombreTipoNP = NombreTipoNP;
            NotaPedido = new TipoNPEntity(NombreTipoNP);
            //NotaPedido = NotaPedido;
        }

        public int ID { get; set; }
        public string IDCompuesta { get; set; }
        public ProyectoEntity Proyecto { get; set; }
        public int IDEstadoNP { get; set; } //Indica si está creada, ajustada, valorizada o facturada (1, 2, 3 o 4 según orden)
        public CreacionEntity Creacion { get; set; }
        public AjusteEntity Ajuste { get; set; }
        public ValorizacionEntity Valorizacion { get; set; }
        public List<EstadoPagoEntity> EstadoPago { get; set; }
        public bool Estado { get; set; } //indica si es una NP eliminada
        public int tipoNP { get; set; }
        public string nombreTipoNP { get; set; }
        //public string nombre { get; set; }
        public TipoNPEntity NotaPedido { get; set; }
        public decimal Cantidad { get; set; }
        public decimal Valor { get; set; }

        public static implicit operator int(NotaPedidoEntity v)
        {
            throw new NotImplementedException();
        }
    }

    public class EstadoNPEntity
    {
        public EstadoNPEntity(int ID)
        {
            this.ID = ID;
        }

        public EstadoNPEntity(int ID, string Descripcion)
        {
            this.ID = ID;
            this.Descripcion = Descripcion;
        }

        public int ID { get; set; }
        public string Descripcion { get; set; }
    }

    
    [Serializable]
    public class CreacionEntity
    {
        public CreacionEntity()
        {

        }

        public CreacionEntity(string NombreTipoUnidad)
        {
            TipoUnidad = new TipoUnidadEntity(NombreTipoUnidad);
        }

        public CreacionEntity(int IDOC, string DescOC)
        {
            OrdenCompra = new DocumentoComercialEntity(IDOC, DescOC);
        }

        public CreacionEntity(decimal Cantidad, decimal ValorUnitario, string Descripcion)
        {
            this.Cantidad = Cantidad;
            this.ValorUnitario = ValorUnitario;
            this.Descripcion = Descripcion;
        }

        public CreacionEntity(string NombreTipoUnidad, decimal ValorUnitario)
        {
            TipoUnidad = new TipoUnidadEntity(NombreTipoUnidad);
            this.ValorUnitario = ValorUnitario;
        }

        public CreacionEntity (string NombreTipoUnidad, string NombreUsuario, string ApellidoPaterno)
        {
            TipoUnidad = new TipoUnidadEntity(NombreTipoUnidad);
            Usuario = new UsuarioEntity(NombreUsuario, ApellidoPaterno);
        }

        public CreacionEntity(string NombreTipoUnidad, decimal Cantidad, decimal ValorUnitario, string Descripcion, DateTime Fecha, string NombreUsuario, string ApellidoPaterno)
        {
            TipoUnidad = new TipoUnidadEntity(NombreTipoUnidad);
            this.Cantidad = Cantidad;
            this.ValorUnitario = ValorUnitario;
            this.Descripcion = Descripcion;
            this.Fecha = Fecha;
            Usuario = new UsuarioEntity(NombreUsuario, ApellidoPaterno);
        }

        public CreacionEntity(int IDTipoUnidad,int IDOC, decimal ValorUnitario, decimal Cantidad, string Descripcion)
        {
            TipoUnidad = new TipoUnidadEntity(IDTipoUnidad);
            OrdenCompra = new DocumentoComercialEntity(IDOC);
  
            this.ValorUnitario = ValorUnitario;
            this.Cantidad = Cantidad;
            this.Descripcion = Descripcion;
        }

        public CreacionEntity(int IDTipoUnidad,int IDOC, decimal ValorUnitario, decimal Cantidad, string Descripcion, int IDTipoNP)
        {
            TipoUnidad = new TipoUnidadEntity(IDTipoUnidad);
            OrdenCompra = new DocumentoComercialEntity(IDOC);
            TipoNP = new TipoNPEntity(IDTipoNP);
  
            this.ValorUnitario = ValorUnitario;
            this.Cantidad = Cantidad;
            this.Descripcion = Descripcion;
        }

        //public CreacionEntity(int IDTipoUnidad, string NombreTipoUnidad, string SiglaTipoUnidad, int IDOC, string DescripcionOC, int IDTipoNP)
        public CreacionEntity(int IDTipoUnidad, string NombreTipoUnidad, string SiglaTipoUnidad, int IDOC, string DescripcionOC)
        {
            TipoUnidad = new TipoUnidadEntity(IDTipoUnidad, NombreTipoUnidad, SiglaTipoUnidad);
            OrdenCompra = new DocumentoComercialEntity(IDOC, DescripcionOC);
            //TipoNP = new TipoNP(IDTipoNP);
        }

        //public CreacionEntity(int IDTipoUnidad, string NombreTipoUnidad, string SiglaTipoUnidad, int IDOC, string DescripcionOC, decimal ValorUnitario, int IDTipoNP)
        public CreacionEntity(int IDTipoUnidad, string NombreTipoUnidad, string SiglaTipoUnidad, int IDOC, string DescripcionOC, decimal ValorUnitario)
        {
            TipoUnidad = new TipoUnidadEntity(IDTipoUnidad, NombreTipoUnidad, SiglaTipoUnidad);
            OrdenCompra = new DocumentoComercialEntity(IDOC, DescripcionOC);
            this.ValorUnitario = ValorUnitario;
            //TipoNP = new TipoNP(IDTipoNP);

        }

        public CreacionEntity(decimal Cantidad, string Descripcion, decimal ValorUnitario, int IDTipoUnidad, string NombreTipoUnidad, string SiglaTipoUnidad, bool CantidadModificada, bool DescripcionModificada, bool ValorUnitarioModificado)
        {
            TipoUnidad = new TipoUnidadEntity(IDTipoUnidad, NombreTipoUnidad, SiglaTipoUnidad);
            this.ValorUnitario = ValorUnitario;
            this.Cantidad = Cantidad;
            this.Descripcion = Descripcion;
            this.CantidadModificada = CantidadModificada;
            this.DescripcionModificada = DescripcionModificada;
            this.ValorUnitarioModificado = ValorUnitarioModificado;
        }

        public CreacionEntity(decimal Cantidad, string Descripcion, decimal ValorUnitario, int IDTipoUnidad, string NombreTipoUnidad, string SiglaTipoUnidad, DateTime FechaCreacion, DateTime FechaUltimaModificacion, int IDOC, string DescripcionOC, string NombreUsuario, string APaternoUsuario)
        {
            TipoUnidad = new TipoUnidadEntity(IDTipoUnidad, NombreTipoUnidad, SiglaTipoUnidad);
            OrdenCompra = new DocumentoComercialEntity(IDOC, DescripcionOC);
            Usuario = new UsuarioEntity(NombreUsuario, APaternoUsuario);
            this.ValorUnitario = ValorUnitario;
            this.Cantidad = Cantidad;
            this.Descripcion = Descripcion;
            Fecha = FechaCreacion;
            this.FechaUltimaModificacion = FechaUltimaModificacion;
        }

        public CreacionEntity(decimal Cantidad, string Descripcion, decimal ValorUnitario, int IDTipoUnidad, string NombreTipoUnidad, string SiglaTipoUnidad, DateTime FechaCreacion, DateTime FechaUltimaModificacion, int IDOC, string DescripcionOC, string NombreUsuario, string APaternoUsuario, int IDTipoNP)
        {
            TipoUnidad = new TipoUnidadEntity(IDTipoUnidad, NombreTipoUnidad, SiglaTipoUnidad);
            OrdenCompra = new DocumentoComercialEntity(IDOC, DescripcionOC);
            Usuario = new UsuarioEntity(NombreUsuario, APaternoUsuario);
            TipoNP = new TipoNPEntity(IDTipoNP);
            this.ValorUnitario = ValorUnitario;
            this.Cantidad = Cantidad;
            this.Descripcion = Descripcion;
            Fecha = FechaCreacion;
            this.FechaUltimaModificacion = FechaUltimaModificacion;
        }
        

        public decimal Cantidad { get; set; }
        public string Descripcion { get; set; }
        public decimal ValorUnitario { get; set; }


        public bool CantidadModificada { get; set; }
        public bool DescripcionModificada { get; set; }
        public bool ValorUnitarioModificado { get; set; }

        public DateTime Fecha { get; set; }
        public DateTime? FechaUltimaModificacion { get; set; } //indica que tiene historial
        public TipoUnidadEntity TipoUnidad { get; set; }
        public DocumentoComercialEntity OrdenCompra { get; set; }
        public UsuarioEntity Usuario { get; set; }
        public NotaPedidoEntity NotaPedido { get; set; }
        public TipoNPEntity TipoNP { get; set; }
    }

    [Serializable]
    public class AjusteEntity
    {
        public AjusteEntity(DateTime FechaRealEntrega)
        {
            this.FechaRealEntrega = FechaRealEntrega;
        }

        public AjusteEntity(decimal Cantidad, string Descripcion)
        {
            this.Cantidad = Cantidad;
            this.Descripcion = Descripcion;
        }

        public AjusteEntity(decimal Cantidad, string Descripcion, DateTime FechaRealEntrega)
        {
            this.Cantidad = Cantidad;
            this.Descripcion = Descripcion;
            this.FechaRealEntrega = FechaRealEntrega;
        }

        public AjusteEntity(decimal Cantidad, string Descripcion, DateTime Fecha, string NombreUsuario, string ApellidoPaterno)
        {
            this.Cantidad = Cantidad;
            this.Descripcion = Descripcion;
            this.Fecha = Fecha;
            Usuario = new UsuarioEntity(NombreUsuario, ApellidoPaterno);
        }
        
        public AjusteEntity(decimal Cantidad, string Descripcion, DateTime FechaRealEntrega, DateTime Fecha, string NombreUsuario, string APaternoUsuario)
        {
            this.Cantidad = Cantidad;
            this.Descripcion = Descripcion;
            this.FechaRealEntrega = FechaRealEntrega;
            this.Fecha = Fecha;
            Usuario = new UsuarioEntity(NombreUsuario, APaternoUsuario);
        }

        public decimal Cantidad { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaRealEntrega { get; set; }
        public UsuarioEntity Usuario { get; set; }
        public DateTime Fecha { get; set; }
    }

    [Serializable]
    public class ValorizacionEntity
    {
        public ValorizacionEntity() { }

        public ValorizacionEntity(decimal Cantidad, string Descripcion, decimal ValorUnitario)
        {
            this.Cantidad = Cantidad;
            this.Descripcion = Descripcion;
            this.ValorUnitario = ValorUnitario;
        }

        public ValorizacionEntity(decimal Cantidad, string Descripcion, decimal ValorUnitario, DateTime Fecha, string NombreUsuario, string APaternoUsuario)
        {
            this.Cantidad = Cantidad;
            this.Descripcion = Descripcion;
            this.ValorUnitario = ValorUnitario;
            this.Fecha = Fecha;
            Usuario = new UsuarioEntity(NombreUsuario, APaternoUsuario);
        }

        public ValorizacionEntity(decimal Cantidad, string Descripcion, decimal ValorUnitario, DateTime Fecha, string NombreUsuario, string APaternoUsuario, decimal ValorPorFacturar)
        {
            this.Cantidad = Cantidad;
            this.Descripcion = Descripcion;
            this.ValorUnitario = ValorUnitario;
            this.Fecha = Fecha;
            Usuario = new UsuarioEntity(NombreUsuario, APaternoUsuario);
            this.ValorPorFacturar = ValorPorFacturar;
        }

        public decimal Cantidad { get; set; }
        public string Descripcion { get; set; }
        public decimal ValorUnitario { get; set; }
        public decimal ValorPorFacturar { get; set; }
        public UsuarioEntity Usuario { get; set; }
        public DateTime Fecha { get; set; }
    }


    [Serializable]
    public class TipoNPEntity
    {
        public TipoNPEntity()
        {

        }

        public TipoNPEntity(int ID)
        {
            this.ID = ID;
        }

        public TipoNPEntity(string Nombre)
        {
            this.Nombre = Nombre;
        }

        public TipoNPEntity(int ID, string Nombre)
        {
            this.ID = ID;
            this.Nombre = Nombre;
        }

        public int ID { get; set; }
        public string Nombre { get; set; }
    }

}

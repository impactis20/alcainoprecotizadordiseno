﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.Entities
{
    public class IntegracionEntity
    {
        public IntegracionEntity()
        {
        }
        public IntegracionEntity(int IdProyecto)
        {
            this.IdProyecto = IdProyecto;
        }
        public IntegracionEntity(string Codigo, string Nombre, string Clase1, int Nivel2, int Nreguist, decimal Yy, int Id_log1)
        {
            this.Codigo = Codigo;
            this.Nombre = Nombre;
            this.Clase1 = Clase1;
            this.Nivel2 = Nivel2;
            this.Nreguist = Nreguist;
            this.Yy = Yy;
            this.Id_log1 = Id_log1;
        }
        public IntegracionEntity(int IdProyecto, int idTipoValor, string fechaIngreso, int IdCliente, string rutCliente, int valorTotalProyecto, int idNotaVentaManager, int Id_log1)
        {
            this.IdProyecto = IdProyecto;
            this.idTipoValor = idTipoValor;
            this.fechaIngreso = fechaIngreso;
            this.IdCliente = IdCliente;
            this.rutCliente = rutCliente;
            this.valorTotalProyecto = valorTotalProyecto;
            this.idNotaVentaManager = idNotaVentaManager;
            this.Id_log1 = Id_log1;
        }
        public IntegracionEntity(string OrdenPRD, string nreguist_art, DateTime fechaCreacion, DateTime fechaComprometida, int IdCliente, string Nombre)
        {
            this.OrdenPRD = OrdenPRD;
            this.nreguist_art = nreguist_art;
            this.fechaCreacion = fechaCreacion;
            this.fechaComprometida = fechaComprometida;
            this.IdCliente = IdCliente;
            this.Nombre = Nombre;
        }
        public IntegracionEntity(string Nombre, string Codigo, DateTime fecha, string articulo, int Id_log1)
        {
            this.Codigo = Codigo;
            this.Nombre = Nombre;
            this.fecha = fecha;
            this.articulo = articulo;
            this.Id_log1 = Id_log1;
        }

        public IntegracionEntity(int Id_log1, string desc_sp_cont, string desc_sp_call, string desc_call, int estado, string excepcion, DateTime fecha_Hora, string Codigo, string Nombre, string Clase1, int Nivel2, int Nreguist, decimal Yy)
        {
            this.Id_log1 = Id_log1;
            this.desc_sp_cont = desc_sp_cont;
            this.desc_sp_call = desc_sp_call;
            this.desc_call = desc_call;
            this.estado = estado;
            this.excepcion = excepcion;
            this.fecha_Hora = fecha_Hora;
            this.Codigo = Codigo;
            this.Nombre = Nombre;
            this.Clase1 = Clase1;
            this.Nivel2 = Nivel2;
            this.Nreguist = Nreguist;
            this.Yy = Yy;
        }

        public IntegracionEntity(int Id_log1, int IdProyecto, string desc_sp_cont, string desc_sp_call,int estado, string excepcion, DateTime fecha_Hora)
        {
            this.Id_log1 = Id_log1;
            this.IdProyecto = IdProyecto;
            this.desc_sp_cont = desc_sp_cont;
            this.desc_sp_call = desc_sp_call;
            this.estado = estado;
            this.excepcion = excepcion;
            this.fecha_Hora = fecha_Hora;

        }
        public IntegracionEntity(int Id_log1, string desc_sp_cont, string desc_sp_call, int estado, string excepcion, DateTime fecha_Hora)
        {
            this.Id_log1 = Id_log1;
            this.desc_sp_cont = desc_sp_cont;
            this.desc_sp_call = desc_sp_call;
            this.estado = estado;
            this.excepcion = excepcion;
            this.fecha_Hora = fecha_Hora;
        }

        public IntegracionEntity(int Id_log1, string desc_sp_cont, string desc_sp_call, string desc_call, int estado, string excepcion, DateTime fecha_Hora,int IdProyecto)
        {
            this.Id_log1 = Id_log1;
            this.desc_sp_cont = desc_sp_cont;
            this.desc_sp_call = desc_sp_call;
            this.desc_call = desc_call;
            this.estado = estado;
            this.excepcion = excepcion;
            this.fecha_Hora = fecha_Hora;
            this.IdProyecto = IdProyecto;
        }

        public IntegracionEntity(int Id_log1, string desc_sp_cont, string desc_sp_call, string desc_call, int estado, string excepcion, DateTime fecha_Hora, string Nombre, string articulo, DateTime fecha, string Codigo)
        {
            this.Id_log1 = Id_log1;
            this.desc_sp_cont = desc_sp_cont;
            this.desc_sp_call = desc_sp_call;
            this.desc_call = desc_call;
            this.estado = estado;
            this.excepcion = excepcion;
            this.fecha_Hora = fecha_Hora;
            this.Nombre = Nombre;
            this.articulo = articulo;
            this.fecha = fecha;
            this.Codigo = Codigo;

        }
        public IntegracionEntity(string desc_sp_cont, string desc_sp_call, string desc_call, int estado, string excepcion, DateTime fecha_Hora, string NombreFantasia, int Rut, string DigitoV, string Razon, string Giro, string Direccion, string Telefono, string Comuna, string Ciudad, int Nreguist, int Id_log1)
        {
            this.Id_log1 = Id_log1;
            this.desc_sp_cont = desc_sp_cont;
            this.desc_sp_call = desc_sp_call;
            this.desc_call = desc_call;
            this.estado = estado;
            this.excepcion = excepcion;
            this.fecha_Hora = fecha_Hora;
            this.NombreFantasia = NombreFantasia;
            this.Rut = Rut;
            this.DigitoV = DigitoV;
            this.Razon = Razon;
            this.Giro = Giro;
            this.Direccion = Direccion;
            this.Telefono = Telefono;
            this.Comuna = Comuna;
            this.Ciudad = Ciudad;
            this.Nreguist = Nreguist;
        }

        public IntegracionEntity(int Id_log1, string desc_sp_cont, string desc_sp_call, string desc_call, int estado, string excepcion, DateTime fecha_Hora, string NombreFantasia, int Rut, string DigitoV, string Razon, string Giro, string Direccion, string Telefono, string Comuna, string Ciudad, int Nreguist)
        {
            this.Id_log1 = Id_log1;
            this.desc_sp_cont = desc_sp_cont;
            this.desc_sp_call = desc_sp_call;
            this.desc_call = desc_call;
            this.estado = estado;
            this.excepcion = excepcion;
            this.fecha_Hora = fecha_Hora;
            this.NombreFantasia = NombreFantasia;
            this.Rut = Rut;
            this.DigitoV = DigitoV;
            this.Razon = Razon;
            this.Giro = Giro;
            this.Direccion = Direccion;
            this.Telefono = Telefono;
            this.Comuna = Comuna;
            this.Ciudad = Ciudad;
            this.Nreguist = Nreguist;
        }
        public IntegracionEntity(string NombreFantasia, int Rut, string DigitoV, string Razon, string Giro, string Direccion, string Telefono, string Comuna, string Ciudad, int Nreguist, int Id_log1)
        {
            this.Id_log1 = Id_log1;
            this.NombreFantasia = NombreFantasia;
            this.Rut = Rut;
            this.DigitoV = DigitoV;
            this.Razon = Razon;
            this.Giro = Giro;
            this.Direccion = Direccion;
            this.Telefono = Telefono;
            this.Comuna = Comuna;
            this.Ciudad = Ciudad;
            this.Nreguist = Nreguist;
        }
        public IntegracionEntity(string desc_sp_cont, string desc_sp_call, string desc_call, int estado, string excepcion, DateTime fecha_Hora, int IdProyecto, int IdCliente, DateTime fechaCreacion, string condicionPago, DateTime fechaVencimiento, string nroOC, DateTime fechaOC, string nroHes, DateTime fechaHes, double valorTotalNeto, string glosa, int Id_log1, int idFacturaManager)
        {
            this.desc_sp_cont = desc_sp_cont;
            this.desc_sp_call = desc_sp_call;
            this.desc_call = desc_call;
            this.estado = estado;
            this.excepcion = excepcion;
            this.fecha_Hora = fecha_Hora;
            this.Id_log1 = Id_log1;
            this.IdProyecto = IdProyecto;
            this.IdCliente = IdCliente;
            this.fechaCreacion = fechaCreacion;
            this.condicionPago = condicionPago;
            this.fechaVencimiento = fechaVencimiento;
            this.nroOC = nroOC;
            this.fechaOC = fechaOC;
            this.nroHes = nroHes;
            this.fechaHes = fechaHes;
            this.valorTotalNeto = valorTotalNeto;
            this.glosa = glosa;
            this.idFacturaManager = idFacturaManager;
        }
        public IntegracionEntity(int IdProyecto, int IdCliente, DateTime fechaCreacion, string condicionPago, DateTime fechaVencimiento, string nroOC, DateTime fechaOC, string nroHes, DateTime fechaHes, double valorTotalNeto, string glosa, int Id_log1, int idFacturaManager)
        {
            this.Id_log1 = Id_log1;
            this.IdProyecto = IdProyecto;
            this.IdCliente = IdCliente;
            this.fechaCreacion = fechaCreacion;
            this.condicionPago = condicionPago;
            this.fechaVencimiento = fechaVencimiento;
            this.nroOC = nroOC;
            this.fechaOC = fechaOC;
            this.nroHes = nroHes;
            this.fechaHes = fechaHes;
            this.valorTotalNeto = valorTotalNeto;
            this.glosa = glosa;
            this.idFacturaManager = idFacturaManager;
        }

        public IntegracionEntity(int Id_log1,string desc_sp_cont, string desc_sp_call, string desc_call, int estado, string excepcion, DateTime fecha_Hora, int IdProyecto, int idNotaVentaManager, int valorTotalProyecto, int Rut, int IdCliente, DateTime fechaCreacion, int idTipoValor)
        {
            this.Id_log1 = Id_log1;
            this.desc_sp_cont = desc_sp_cont;
            this.desc_sp_call = desc_sp_call;
            this.desc_call = desc_call;
            this.estado = estado;
            this.excepcion = excepcion;
            this.fecha_Hora = fecha_Hora;
            this.IdProyecto = IdProyecto;
            this.idNotaVentaManager = idNotaVentaManager;
            this.valorTotalProyecto = valorTotalProyecto;
            this.Rut = Rut;
            this.IdCliente = IdCliente;
            this.fechaCreacion = fechaCreacion;
            this.idTipoValor = idTipoValor;
        }

        public IntegracionEntity(int Id_log1, string desc_sp_cont, string desc_sp_call, string desc_call, int estado, string excepcion, DateTime fecha_Hora, int IdProyecto, int idNotaVentaManager, int valorTotalProyecto, string Rut1, int IdCliente, DateTime fechaCreacion, int idTipoValor)
        {
            this.Id_log1 = Id_log1;
            this.desc_sp_cont = desc_sp_cont;
            this.desc_sp_call = desc_sp_call;
            this.desc_call = desc_call;
            this.estado = estado;
            this.excepcion = excepcion;
            this.fecha_Hora = fecha_Hora;
            this.IdProyecto = IdProyecto;
            this.idNotaVentaManager = idNotaVentaManager;
            this.valorTotalProyecto = valorTotalProyecto;
            this.Rut1 = Rut1;
            this.IdCliente = IdCliente;
            this.fechaCreacion = fechaCreacion;
            this.idTipoValor = idTipoValor;
        }

        public IntegracionEntity(int Id_log1, string desc_sp_cont, string desc_sp_call, string desc_call, int estado, string excepcion, DateTime fecha_Hora, string OrdenPRD, string Nombre, string nreguist_art, int IdCliente, DateTime fechaComprometida, DateTime fechaCreacion, string articulo)
        {
            this.Id_log1 = Id_log1;
            this.desc_sp_cont = desc_sp_cont;
            this.desc_sp_call = desc_sp_call;
            this.desc_call = desc_call;
            this.estado = estado;
            this.excepcion = excepcion;
            this.fecha_Hora = fecha_Hora;
            this.OrdenPRD = OrdenPRD;
            this.Nombre = Nombre;
            this.nreguist_art = nreguist_art;
            this.IdCliente = IdCliente;
            this.fechaComprometida = fechaComprometida;
            this.fechaCreacion = fechaCreacion;
            this.articulo = articulo;

        }

        public IntegracionEntity(string v1, string v2, string v3, string v4, string v5, string v6, string v7, int id_log1)
        {
            
        }

        public int IdCliente { get; set; }
        public DateTime fechaCreacion { get; set; }
        public string condicionPago { get; set; }
        public DateTime fechaVencimiento { get; set; }
        public string nroOC { get; set; }
        public DateTime fechaOC { get; set; }
        public string nroHes { get; set; }
        public DateTime fechaHes { get; set; }
        public Double valorTotalNeto { get; set; }
        public string glosa { get; set; }
        public int idFacturaManager { get; set; }
        public int Id_log1 { get; set; }
        public string desc_sp_cont { get; set; }
        public string desc_sp_call { get; set; }
        public string desc_call { get; set; }
        public int estado { get; set; }
        public string excepcion { get; set; }
        public DateTime fecha_Hora { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string Clase1 { get; set; }
        public int Nivel2 { get; set; }
        public int Nreguist { get; set; }
        public decimal Yy { get; set; }
        public string articulo { get; set; }
        public DateTime fecha { get; set; }
        public int IdProyecto { get; set; }
        public int Rut { get; set; }
        public string Rut1 { get; set; }
        public string DigitoV { get; set; }
        public string Razon { get; set; }
        public string Giro { get; set; }
        public string Ciudad { get; set; }
        public string NombreFantasia { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Comuna { get; set; }
        public int idTipoValor { get; set; }
        public string fechaIngreso { get; set; }
        public string rutCliente { get; set; }
        public int valorTotalProyecto { get; set; }
        public int idNotaVentaManager { get; set; }
        public string OrdenPRD { get; set; }
        public string nreguist_art { get; set; }
        public DateTime fechaComprometida { get; set; }

    }

public class Index {
        public Index()
        { 
        }
        public Index(int id, string descripcion, string procedimiento, int estado, int erorres)
        {
            this.id = id;
            this.descripcion = descripcion;
            this.procedimiento = procedimiento;
            this.estado = estado;
            this.errores = errores;
        }
        public int id { get; set; }
        public string descripcion { get; set; }
        public string procedimiento { get; set; }
        public int estado { get; set; }
        public int errores { get; set; }
      


    }
}

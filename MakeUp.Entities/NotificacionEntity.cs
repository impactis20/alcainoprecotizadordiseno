﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.Entities
{
    public class NotificacionEntity
    {
        public NotificacionEntity() { }

        public NotificacionEntity(int ID)
        {
            this.ID = ID;
        }

        public NotificacionEntity(int ID, string Nombre, bool Estado)
        {
            this.ID = ID;
            this.Nombre = Nombre;
            this.Estado = Estado;
        }

        public NotificacionEntity(int ID, string Nombre, string Mensaje, bool Estado)
        {
            this.ID = ID;
            this.Nombre = Nombre;
            this.Mensaje = Mensaje;
            this.Estado = Estado;
        }

        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Mensaje { get; set; }
        public List<RolPredeterminadoEntity> RolNotificado { get; set; }
        public bool Estado { get; set; }
    }
    
    public class NotificacionAppEntity
    {
        public NotificacionAppEntity() { }

        public NotificacionAppEntity(int ID, int IDUsuario, string MensajeNotificado, string Url, bool Ingreso, DateTime FechaEvento, int IDResponsable, string NombreResponsable, string ApellidoParternoResponsable)
        {
            this.ID = ID;
            this.IDUsuario = IDUsuario;
            this.MensajeNotificado = MensajeNotificado;
            UsuarioResponsable = new UsuarioEntity(NombreResponsable, ApellidoParternoResponsable);
            UsuarioResponsable.ID = IDResponsable;
            this.Ingreso = Ingreso;
            this.Url = Url;
            this.FechaEvento = FechaEvento;
        }

        public int ID { get; set; }
        public int IDUsuario { get; set; }
        public string MensajeNotificado { get; set; }
        public UsuarioEntity UsuarioResponsable { get; set; } //Usuario que generó la notificación (evento)
        //public NotificacionEntity Notificacion { get; set; } //FK        
        //public ProyectoEntity Proyecto { get; set; }
        //public EstadoPagoEntity EStadoPago { get; set; }
        //public List<NotaPedidoEntity> LstNP { get; set; }
        //public List<ClienteEntity.ContactoEntity> LstContacto { get; set; }
        public bool Ingreso { get; set; }
        public string Url { get; set; }
        public DateTime FechaEvento { get; set; }
    }
}

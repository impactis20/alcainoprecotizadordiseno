﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.Entities
{
    public class GenericoEntity
    {
        public class Region
        {
            public Region(int ID, string Nombre)
            {
                this.ID = ID;
                this.Nombre = Nombre;
            }

            public int ID { get; set; }
            public string Nombre { get; set; }
        }
    }
}

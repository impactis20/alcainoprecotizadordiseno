﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.Entities
{
    public class FacturaEntity
    {
        public FacturaEntity() { }

        public FacturaEntity(int IDFactura, int IDEstadoFactura, int IDProyecto, string NombreProyecto, int IDTipoValor, string SiglaTipoValor, string NombreFantasia, string Glosa, DateTime FechaOrdenFacturacion, double Monto, int NroFactura, bool EnviadoSII, string UrlDoc)
        {
            ID = IDFactura;
            this.IDEstadoFactura = IDEstadoFactura;
            Proyecto = new ProyectoEntity(IDProyecto, NombreProyecto);
            Proyecto.TipoValor = new TipoValorEntity(IDTipoValor, SiglaTipoValor);
            Cliente = new ClienteEntity(NombreFantasia);
            this.Glosa = Glosa;
            this.FechaOrdenFacturacion = FechaOrdenFacturacion;
            this.Monto = Monto;
            this.NroFactura = NroFactura;
            this.EnviadoSII = EnviadoSII;
            this.UrlDoc = UrlDoc;
        }

        public int ID { get; set; }
        public int IDEstadoFactura { get; set; }
        public EstadoPagoEntity EstadoPago { get; set; }
        public UsuarioEntity UsuarioCreador { get; set; }
        public ProyectoEntity Proyecto { get; set; }
        public ClienteEntity Cliente { get; set; }
        public int NroFactura { get; set; }
        public bool EnviadoSII { get; set; }
        public DateTime FechaOrdenFacturacion { get; set; } //fecha de cuando se facturó en makeup y se integró en Manager
        public string Glosa { get; set; }
        public string CondicionPago { get; set; } 
        public double Monto { get; set; }
        public TipoEnvioFacturaEntity TipoEnvio { get; set; }
        public string EntregadoPor { get; set; } //persona que envía la factura
        public DateTime FechaEnvio { get; set; } //fecha en que se envía la factura
        public int RutReceptor { get; set; }
        public string RutReceptorDV { get; set; }
        public string RecibidoPor { get; set; } //persona que recibe la factura
        public DateTime FechaRecepcion { get; set; } //fecha en que el cliente recibió la factura
        public string UrlDoc { get; set; }
    }

    public class TipoEnvioFacturaEntity
    {
        public TipoEnvioFacturaEntity()
        {

        }

        public TipoEnvioFacturaEntity(int IDTipoEnvio)
        {
            ID = IDTipoEnvio;
        }

        public TipoEnvioFacturaEntity(int IDTipoEnvio, string Nombre)
        {
            ID = IDTipoEnvio;
            this.Nombre = Nombre;
        }

        public int ID { get; set; }
        public string Nombre { get; set; }
    }

    public class EstadoDespachoFacturaEntity
    {
        public EstadoDespachoFacturaEntity() { }

        public EstadoDespachoFacturaEntity(int ID)
        {
            this.ID = ID;
        }

        public EstadoDespachoFacturaEntity(int ID, string Descripcion)
        {
            this.ID = ID; this.Descripcion = Descripcion;
        }

        public int ID { get; set; }
        public string Descripcion { get; set; }
    }
}

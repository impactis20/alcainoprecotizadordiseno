﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.Entities
{
    public class ReleaseNoteEntity
    {
        public ReleaseNoteEntity()
        {
    

        }

        public ReleaseNoteEntity(int ID, DateTime Fecha, string Descrip, string Obser)
        {
            this.ID = ID;
            this.FECHA = Fecha;
            this.DESCRIPCION = Descrip;
            this.OBSERVACION = Obser;

        }

        public int ID { get; set; }
        public DateTime FECHA { get; set; }
        public string DESCRIPCION { get; set; }
        public string OBSERVACION { get; set; }
    }
}

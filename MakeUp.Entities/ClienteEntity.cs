﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.Entities
{
    public class ClienteEntity
    {
        public ClienteEntity()
        {

        }

        public ClienteEntity(int ID)
        {
            this.ID = ID;
        }

        public ClienteEntity(string NombreFantasia)
        {
            this.NombreFantasia = NombreFantasia;
        }

        public ClienteEntity(int ID, string NombreFantasia)
        {
            this.ID = ID;
            this.NombreFantasia = NombreFantasia;
        }        

        public ClienteEntity(int ID, string NombreFantasia, string RazonSocial)
        {
            this.ID = ID;
            this.NombreFantasia = NombreFantasia;
            this.RazonSocial = RazonSocial;
        }

        public ClienteEntity(int ID, string NombreFantasia, string RazonSocial, int Rut)
        {
            this.ID = ID;
            this.NombreFantasia = NombreFantasia;
            this.RazonSocial = RazonSocial;
            this.Rut = Rut;
        }

        public ClienteEntity(int ID, string NombreFantasia, string RazonSocial, string rutCliente)
        {
            this.ID = ID;
            this.NombreFantasia = NombreFantasia;
            this.RazonSocial = RazonSocial;
            this.rutCliente = rutCliente;
        }

        public ClienteEntity(int ID, int Rut, string DV, string NombreFantasia, string RazonSocial, string Giro, string Direccion, string Telefono, string Comuna, string Ciudad)
        {
            this.ID = ID;
            this.Rut = Rut;
            this.DV = DV;
            this.NombreFantasia = NombreFantasia;
            this.RazonSocial = RazonSocial;
            this.Giro = Giro;
            this.Direccion = Direccion;
            this.Telefono = Telefono;
            this.Comuna = Comuna;
            this.Ciudad = Ciudad;
        }

        public int ID { get; set; }
        public int Rut { get; set; }
        public string rutCliente { get; set; }
        public string DV { get; set; }
        public string NombreFantasia { get; set; }
        public string RazonSocial { get; set; }
        public string Giro { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Comuna { get; set; }
        public string Ciudad { get; set; }

        public class ContactoEntity
        {
            public ContactoEntity(int ID, string NombreContacto, string Email, string Descripcion, string Telefono, string Movil, bool ContactoCompartido)
            {
                this.ID = ID;
                this.NombreContacto = NombreContacto;
                this.Email = Email;
                this.Descripcion = Descripcion;
                this.Telefono = Telefono;
                this.Movil = Movil;
                this.ContactoCompartido = ContactoCompartido;
            }


            public ContactoEntity(int ID, string NombreContacto, string Email, string Descripcion, string Telefono, string Movil, bool ContactoCompartido, string NombreResponsable, string ApellidoPaternoResponsable, bool VinculadoEnProyecto)
            {
                this.ID = ID;
                this.NombreContacto = NombreContacto;
                this.Email = Email;
                this.Descripcion = Descripcion;
                this.Telefono = Telefono;
                this.Movil = Movil;
                this.ContactoCompartido = ContactoCompartido;
                this.VinculadoEnProyecto = VinculadoEnProyecto;
                Responsable = new UsuarioEntity(NombreResponsable, ApellidoPaternoResponsable); 
            }

            public ContactoEntity(int ID, string NombreContacto, string Email, string Descripcion, string Telefono, string Movil, bool ContactoCompartido, string NombreResponsable, string ApellidoPaternoResponsable)
            {
                this.ID = ID;
                this.NombreContacto = NombreContacto;
                this.Email = Email;
                this.Descripcion = Descripcion;
                this.Telefono = Telefono;
                this.Movil = Movil;
                this.ContactoCompartido = ContactoCompartido;
                Responsable = new UsuarioEntity(NombreResponsable, ApellidoPaternoResponsable);
            }

            public int ID { get; set; }
            public string NombreContacto { get; set; }
            public string Email { get; set; }
            public string Descripcion { get; set; }
            public string Telefono { get; set; }
            public string Movil { get; set; }
            public bool ContactoCompartido { get; set; }
            public bool VinculadoEnProyecto { get; set; }
            public UsuarioEntity Responsable { get; set; }
        }
    }
}

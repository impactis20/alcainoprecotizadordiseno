﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.Entities
{
    public class RolPredeterminadoEntity
    {
        public RolPredeterminadoEntity() {
        }

        public RolPredeterminadoEntity(int ID)
        {
            this.ID = ID;
        }

        public RolPredeterminadoEntity(int ID, string Nombre)
        {
            this.ID = ID;
            this.Nombre = Nombre;
        }

        public RolPredeterminadoEntity(int ID, string Nombre, bool VinculadoANotificacion)
        {
            this.ID = ID;
            this.VinculadoANotificacion = VinculadoANotificacion;
            this.Nombre = Nombre;
        }

        public RolPredeterminadoEntity(int ID, string Nombre, string Descripcion)
        {
            this.ID = ID;
            this.Nombre = Nombre;
            this.Descripcion = Descripcion;
            Modulo = new List<PaginaEntity>();
        }

        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public List<PaginaEntity> Modulo { get; set; }
        public bool VinculadoANotificacion { get; set; } //Variable usada en módulo Notificaciones para distinguir si el Rol está en alguna notificación
        public List<FiltroNotificacionEntity> LstFiltroNotificacion { get; set; } //Variable usada para saber si el rol se puede filtrar en una notificación
    }

    public class FiltroNotificacionEntity
    {
        public FiltroNotificacionEntity()
        {

        }

        public FiltroNotificacionEntity(int ID, string Nombre, bool FiltroSeleccionado)
        {
            this.ID = ID;
            this.Nombre = Nombre;
            this.FiltroSeleccionado = FiltroSeleccionado;
        }

        public int ID { get; set; }
        public string Nombre { get; set; }
        public bool FiltroSeleccionado { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.Entities
{
    [Serializable]
    public class UsuarioEntity
    {
        public UsuarioEntity() { }

        public UsuarioEntity(string Nombre, string ApellidoPaterno)
        {
            this.Nombre = Nombre;
            this.ApellidoPaterno = ApellidoPaterno;
        }

        public UsuarioEntity(int ID, string UserName, string Nombre, string Email)
        {
            this.ID = ID;
            this.UserName = UserName;
            this.Nombre = Nombre;
            this.Email = Email;
        }

        public UsuarioEntity(int ID, string Nombre, string ApellidoPaterno, string ApellidoMaterno, string Email)
        {
            this.ID = ID;
            this.Nombre = Nombre;
            this.ApellidoPaterno = ApellidoPaterno;
            this.ApellidoMaterno = ApellidoMaterno;
            this.Email = Email;
        }

        public UsuarioEntity(int ID, string UserName, string Nombre, string ApellidoPaterno, string ApellidoMaterno, string Email)
        {
            this.ID = ID;
            this.UserName = UserName;
            this.Nombre = Nombre;
            this.ApellidoPaterno = ApellidoPaterno;
            this.ApellidoMaterno = ApellidoMaterno;
            this.Email = Email;
        }

        public UsuarioEntity(int ID, string UserName, string Nombre, string Email, bool VisualizaTodoContacto)
        {
            this.ID = ID;
            this.UserName = UserName;
            this.Nombre = Nombre;
            this.Email = Email;
            this.VisualizaTodoContacto = VisualizaTodoContacto;
        }

        public UsuarioEntity(string Nombre, string ApellidoPaterno, string ApellidoMaterno, string Email, bool VisualizaTodoContacto)
        {
            this.Nombre = Nombre;
            this.ApellidoPaterno = ApellidoPaterno;
            this.ApellidoMaterno = ApellidoMaterno;
            this.Email = Email;
            this.VisualizaTodoContacto = VisualizaTodoContacto;
        }

        public int ID { get; set; }
        public List<string> LstIDConexion { get; set; }
        public string UserName { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Email { get; set; }
        public RolPredeterminadoEntity Rol { get; set; }
        public bool? VisualizaTodoContacto { get; set; }
        public bool EsJefeProyecto { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace MakeUp.DAL
{
    public class FacturacionDAL
    {
        private static DAO_Conexion Conexion = new DAO_Conexion();

        public static List<Entities.ProyectoEntity> GetNPxFacturar()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getNPxFacturar"))
                {
                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();
                    List<Entities.ProyectoEntity> Lst = new List<Entities.ProyectoEntity>();

                    while (Reader.Read())
                    {
                        Entities.ProyectoEntity Proyecto = new Entities.ProyectoEntity(Convert.ToInt16(Reader["ID_PROYECTO"].ToString()), Reader["NOMBRE_PROYECTO"].ToString(), Reader["NOMBRE_FANTASIA"].ToString(), Convert.ToInt16(Reader["CANTIDAD_NP_FACTURADA"].ToString()), Convert.ToInt16(Reader["CONT_NP"].ToString()), Convert.ToDecimal(Reader["VALORIZADO_PENDIENTE"].ToString()), Convert.ToDateTime(Reader["FECHA_PLANIFICADA"].ToString()));
                        Entities.TipoValorEntity TipoValor = new Entities.TipoValorEntity();
                        TipoValor.Sigla = Reader["SIGLA_TIPO_VALOR"].ToString();
                        Proyecto.TipoValor = TipoValor;
                        Lst.Add(Proyecto);
                    }

                    return Lst;
                }
            }
        }

        public static List<Entities.NotaPedidoEntity> GetNPParaFacturar(int IDProyecto)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getNPParaFacturar"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();
                    List<Entities.NotaPedidoEntity> Lst = new List<Entities.NotaPedidoEntity>();

                    while (Reader.Read())
                    {
                        Entities.NotaPedidoEntity NP = new Entities.NotaPedidoEntity(Convert.ToInt16(Reader["ID_NP"].ToString()), IDProyecto.ToString(), Reader["NRO_NP"].ToString(), Convert.ToInt16(Reader["ID_ESTADONP"].ToString()), true, Convert.ToInt16(Reader["TIPO_NP"].ToString()));

                        if (NP.IDEstadoNP == 1)
                        {
                            NP.Creacion = new Entities.CreacionEntity(Reader["NOMBRE_TIPO_UNIDAD"].ToString(), Convert.ToDecimal(Reader["CANTIDAD_CREACION"].ToString()), Convert.ToDecimal(Reader["VALOR_UNITARIO_CREACION"].ToString()), Reader["DESCRIPCION_CREACION"].ToString(), DateTime.Now, Reader["NOMBRE_USUARIO_CREACION"].ToString(), Reader["A_PATERNO_USUARIO_CREACION"].ToString());
                        }

                        if (NP.IDEstadoNP == 2)
                        {
                            NP.Creacion = new Entities.CreacionEntity(Reader["NOMBRE_TIPO_UNIDAD"].ToString(), Convert.ToDecimal(Reader["VALOR_UNITARIO_CREACION"].ToString()));
                            NP.Creacion.Usuario = new Entities.UsuarioEntity(Reader["NOMBRE_USUARIO_CREACION"].ToString(), Reader["A_PATERNO_USUARIO_CREACION"].ToString());
                            NP.Ajuste = new Entities.AjusteEntity(Convert.ToDecimal(Reader["CANTIDAD_AJUSTE"].ToString()), Reader["DESCRIPCION_AJUSTE"].ToString(), Convert.ToDateTime(Reader["FECHA_REAL_ENTREGA_AJUSTE"].ToString()));
                        }

                        if (NP.IDEstadoNP == 3 || NP.IDEstadoNP == 4)
                        {
                            NP.Creacion = new Entities.CreacionEntity(Reader["NOMBRE_TIPO_UNIDAD"].ToString(), Reader["NOMBRE_USUARIO_CREACION"].ToString(), Reader["A_PATERNO_USUARIO_CREACION"].ToString());
                            NP.Ajuste = new Entities.AjusteEntity(Convert.ToDateTime(Reader["FECHA_REAL_ENTREGA_AJUSTE"].ToString()));
                            NP.Valorizacion = new Entities.ValorizacionEntity(Convert.ToDecimal(Reader["CANTIDAD_VALORIZACION"].ToString()), Reader["DESCRIPCION_VALORIZACION"].ToString(), Convert.ToDecimal(Reader["VALOR_UNITARIO_VALORIZACION"].ToString()), DateTime.Now, Reader["NOMBRE_USUARIO_VALORIZACION"].ToString(), Reader["A_PATERNO_USUARIO_VALORIZACION"].ToString(), Convert.ToDecimal(Reader["VALOR_POR_FACTURAR"].ToString()));
                        }

                        Lst.Add(NP);
                    }

                    return Lst;
                }
            }
        }

        public static int AddFacturacionxNP(int IDProyecto, int IDUsuario, int IDCliente, decimal ValorTotalFactura, string CondicionPago, DateTime FechaVencimiento, string NroOC, DateTime FechaOC, string NroHes, DateTime FechaHes, string Glosa, DataTable dtNP, DateTime FechaCreacion, ref int IDNotificacionApp)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("addFacturacionxNP"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = IDUsuario;
                    Cmd.Parameters.Add("@idCliente", SqlDbType.Int).Value = IDCliente;
                    Cmd.Parameters.Add("@valorTotalFactura", SqlDbType.Decimal).Value = ValorTotalFactura;
                    Cmd.Parameters.Add("@condicionPago", SqlDbType.VarChar, 200).Value = CondicionPago;
                    Cmd.Parameters.Add("@fechaVencimiento", SqlDbType.DateTime).Value = FechaVencimiento;
                    Cmd.Parameters.Add("@nroOC", SqlDbType.Char, 30).Value = NroOC;

                    if (FechaOC == DateTime.MinValue)
                        Cmd.Parameters.Add("@fechaOC", SqlDbType.DateTime).Value = DBNull.Value;
                    else
                        Cmd.Parameters.Add("@fechaOC", SqlDbType.DateTime).Value = FechaOC;

                    Cmd.Parameters.Add("@nroHes", SqlDbType.Char, 30).Value = NroHes;

                    if (FechaHes == DateTime.MinValue)
                        Cmd.Parameters.Add("@fechaHes", SqlDbType.DateTime).Value = DBNull.Value;
                    else
                        Cmd.Parameters.Add("@fechaHes", SqlDbType.DateTime).Value = FechaHes;

                    Cmd.Parameters.Add("@glosa", SqlDbType.VarChar, 360).Value = Glosa;
                    Cmd.Parameters.Add("@listNP", SqlDbType.Structured).Value = dtNP;
                    Cmd.Parameters.Add("@fechaCreacion", SqlDbType.DateTime).Value = FechaCreacion;
                    Cmd.Parameters.Add("@respuesta", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                    SqlParameter p_IDNotificacionApp = new SqlParameter("@idNotificacionApp", SqlDbType.Int);
                    p_IDNotificacionApp.Direction = ParameterDirection.InputOutput;
                    p_IDNotificacionApp.Value = 0;
                    Cmd.Parameters.Add(p_IDNotificacionApp);

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    //obtiene valor IDNotificacion
                    IDNotificacionApp = Convert.ToInt32(p_IDNotificacionApp.Value);

                    //Respuesta: Devuelve 0 cuando la facturación no se pudo realizar porque una de las NP seleccionadas ya fue facturada,
                    //           Devuelve - 1 cuando el proyecto está en USD o Euro
                    //           De lo contrario devuelve el nro de la factura que genera Manager
                    return Convert.ToInt16(Cmd.Parameters["@respuesta"].Value);
                }
            }
        }

        public static List<Entities.ProyectoEntity> GetEPxFacturar()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getEPxFacturar"))
                {
                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();
                    List<Entities.ProyectoEntity> Lst = new List<Entities.ProyectoEntity>();

                    while (Reader.Read())
                    {
                        Entities.ProyectoEntity Proyecto = new Entities.ProyectoEntity(Convert.ToInt16(Reader["ID_PROYECTO"].ToString()), Reader["NOMBRE_PROYECTO"].ToString(), Reader["NOMBRE_FANTASIA"].ToString(), Convert.ToInt16(Reader["CONT_EP"].ToString()), Convert.ToInt16(Reader["CANTIDAD_EP_FACTURADA"].ToString()), Convert.ToDecimal(Reader["MONTO_EP_PENDIENTE"].ToString()));
                        Proyecto.CantidadEPEmitidaFacturacion = Convert.ToInt32(Reader["CANTIDAD_EP_EMITIDA_PARA_FACTUACION"]);
                        Proyecto.TipoValor = new Entities.TipoValorEntity(Convert.ToInt16(Reader["id_tipovalor"]), Reader["SIGLA_TIPO_VALOR"].ToString(), Convert.ToDecimal(Reader["VALOR_TIPO_VALOR"]));
                        Lst.Add(Proyecto);
                    }

                    return Lst;
                }
            }
        }

        public static int AddFacturacionxEP(int IDProyecto, int IDUsuario, int IDCliente, int IDEstadoPago, decimal ValorTotalFactura, string CondicionPago, DateTime FechaVencimiento, string NroOC, DateTime FechaOC, string NroHes, DateTime FechaHes, string Glosa, DateTime FechaCreacion, ref int IDNotificacionApp)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("addFacturacionxEP"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = IDUsuario;
                    Cmd.Parameters.Add("@idCliente", SqlDbType.Int).Value = IDCliente;
                    Cmd.Parameters.Add("@idEstadoPago", SqlDbType.Int).Value = IDEstadoPago;
                    Cmd.Parameters.Add("@valorTotalFactura", SqlDbType.Decimal).Value = ValorTotalFactura;
                    Cmd.Parameters.Add("@condicionPago", SqlDbType.VarChar, 200).Value = CondicionPago;
                    Cmd.Parameters.Add("@fechaVencimiento", SqlDbType.DateTime).Value = FechaVencimiento;
                    Cmd.Parameters.Add("@nroOC", SqlDbType.Char, 30).Value = NroOC;

                    if (FechaOC == DateTime.MinValue)
                        Cmd.Parameters.Add("@fechaOC", SqlDbType.DateTime).Value = DBNull.Value;
                    else
                        Cmd.Parameters.Add("@fechaOC", SqlDbType.DateTime).Value = FechaOC;

                    Cmd.Parameters.Add("@nroHes", SqlDbType.Char, 30).Value = NroHes;

                    if (FechaHes == DateTime.MinValue)
                        Cmd.Parameters.Add("@fechaHes", SqlDbType.DateTime).Value = DBNull.Value;
                    else
                        Cmd.Parameters.Add("@fechaHes", SqlDbType.DateTime).Value = FechaHes;

                    Cmd.Parameters.Add("@glosa", SqlDbType.VarChar, 360).Value = Glosa;
                    Cmd.Parameters.Add("@fechaCreacion", SqlDbType.DateTime).Value = FechaCreacion;
                    Cmd.Parameters.Add("@respuesta", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                    SqlParameter p_IDNotificacionApp = new SqlParameter("@idNotificacionApp", SqlDbType.Int);
                    p_IDNotificacionApp.Direction = ParameterDirection.InputOutput;
                    p_IDNotificacionApp.Value = 0;
                    Cmd.Parameters.Add(p_IDNotificacionApp);

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    //obtiene valor IDNotificacion
                    IDNotificacionApp = Convert.ToInt32(p_IDNotificacionApp.Value);

                    //Respuesta: Devuelve 0 cuando el estado de pago ya fue facturado,
                    //           Devuelve - 1 cuando el proyecto está en USD o Euro
                    //           De lo contrario devuelve el nro de la factura que genera Manager
                    return Convert.ToInt16(Cmd.Parameters["@respuesta"].Value);
                }
            }
        }

        public static List<Entities.FacturaEntity> GetFactura(int IDProyecto, string NombreProyecto, string NombreFantasia, DataTable dtEstadoDespachoFactura)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getFactura"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@nombreProyecto", SqlDbType.VarChar, 50).Value = NombreProyecto;
                    Cmd.Parameters.Add("@nombreFantasia", SqlDbType.VarChar, 50).Value = NombreFantasia;
                    Cmd.Parameters.Add("@listEstadoDespachoFactura", SqlDbType.Structured).Value = dtEstadoDespachoFactura;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();
                    List<Entities.FacturaEntity> Lst = new List<Entities.FacturaEntity>();

                    while (Reader.Read())
                        Lst.Add(new Entities.FacturaEntity(Convert.ToInt32(Reader["ID_FACTURA"]), Convert.ToInt32(Reader["ID_ESTADOFACTURA"]), Convert.ToInt32(Reader["ID_PROYECTO"]), Reader["NOMBRE_PROYECTO"].ToString(), Convert.ToInt32(Reader["ID_TIPOVALOR"]), Reader["SIGLA"].ToString(), Reader["NOMBRE_FANTASIA"].ToString(), Reader["GLOSA"].ToString(), Convert.ToDateTime(Reader["FECHA_ORDEN_FACTURACION"]), Convert.ToDouble(Reader["MONTO"]), Convert.ToInt32(Reader["NRO_FACTURA"]), Convert.ToBoolean(Reader["E_ENVIADO"]), Reader["RUTA_PDF"].ToString()));

                    return Lst;
                }
            }
        }

        public static List<Entities.TipoEnvioFacturaEntity> GetTipoEnvioFactura()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getTipoEnvioFactura"))
                {
                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();
                    List<Entities.TipoEnvioFacturaEntity> Lst = new List<Entities.TipoEnvioFacturaEntity>();

                    while (Reader.Read())
                        Lst.Add(new Entities.TipoEnvioFacturaEntity(Convert.ToInt32(Reader["ID_TIPOENVIO"]), Reader["NOMBRE"].ToString()));

                    return Lst;
                }
            }
        }

        public static List<Entities.EstadoDespachoFacturaEntity> GetEstadoDespachoFactura()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getEstadoDespachoFactura"))
                {
                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();
                    List<Entities.EstadoDespachoFacturaEntity> Lst = new List<Entities.EstadoDespachoFacturaEntity>();

                    while (Reader.Read())
                        Lst.Add(new Entities.EstadoDespachoFacturaEntity(Convert.ToInt32(Reader["ID_ESTADOFACTURA"]), Reader["DESCRIPCION"].ToString()));

                    return Lst;
                }
            }
        }

        public static Entities.FacturaEntity GetDatosDespachoFactura(int IDFactura)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getDatosDespachoFactura"))
                {
                    Cmd.Parameters.Add("@idFactura", SqlDbType.Int).Value = IDFactura;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();
                    Entities.FacturaEntity Factura = new Entities.FacturaEntity();

                    if (Reader.Read())
                    {
                        Factura = new Entities.FacturaEntity();

                        if (!string.IsNullOrEmpty(Reader["ID_TIPOENVIO"].ToString()) && !string.IsNullOrEmpty(Reader["ENTREGADO_POR"].ToString()) && !string.IsNullOrEmpty(Reader["FECHA_ENVIO"].ToString()))
                        {
                            Factura.TipoEnvio = new Entities.TipoEnvioFacturaEntity(Convert.ToInt16(Reader["ID_TIPOENVIO"]));
                            Factura.EntregadoPor = Reader["ENTREGADO_POR"].ToString();
                            Factura.FechaEnvio = Convert.ToDateTime(Reader["FECHA_ENVIO"]);
                        }

                        if (!string.IsNullOrEmpty(Reader["RUT_RECEPTOR"].ToString()))
                        {
                            Factura.RutReceptor = Convert.ToInt32(Reader["RUT_RECEPTOR"]);
                            Factura.RutReceptorDV = Reader["RUT_RECEPTOR_V"].ToString();
                        }
                        else
                        {
                            Factura.RutReceptor = 0;
                        }

                        if (!string.IsNullOrEmpty(Reader["RECIBIDO_POR"].ToString()) && !string.IsNullOrEmpty(Reader["FECHA_RECEPCION"].ToString()))
                        {
                            Factura.RecibidoPor = Reader["RECIBIDO_POR"].ToString();
                            Factura.FechaRecepcion = Convert.ToDateTime(Reader["FECHA_RECEPCION"]);
                        }
                    }

                    return Factura;
                }
            }
        }

        public static void SetDespachoEnvioFactura(int NroFactura, int IDTipoEnvio, string EntregadoPor, DateTime FechaEnvio)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("setDespachoEnvioFactura"))
                {
                    Cmd.Parameters.Add("@nroFactura", SqlDbType.Int).Value = NroFactura;
                    Cmd.Parameters.Add("@idTipoEnvio", SqlDbType.Int).Value = IDTipoEnvio;
                    Cmd.Parameters.Add("@entregadoPor", SqlDbType.VarChar, 100).Value = EntregadoPor;
                    Cmd.Parameters.Add("@fechaEnvio", SqlDbType.DateTime).Value = FechaEnvio;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static void SetDespachoRecepcionFactura(int NroFactura, int IDUsuarioResponsable, int RutReceptor, string RutReceptorDV, string RecibidoPor, DateTime FechaRecepcion, DateTime FechaEvento, ref int IDNotificacionApp)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("setDespachoRecepcionFactura"))
                {
                    Cmd.Parameters.Add("@nroFactura", SqlDbType.Int).Value = NroFactura;
                    Cmd.Parameters.Add("@rutReceptor", SqlDbType.Int, 8).Value = RutReceptor;
                    Cmd.Parameters.Add("@rutReceptorDV", SqlDbType.Char, 1).Value = RutReceptorDV;
                    Cmd.Parameters.Add("@recibidoPor", SqlDbType.VarChar, 100).Value = RecibidoPor;
                    Cmd.Parameters.Add("@fechaRecepcion", SqlDbType.DateTime).Value = FechaRecepcion;
                    Cmd.Parameters.Add("@fechaEvento", SqlDbType.DateTime).Value = FechaEvento;
                    Cmd.Parameters.Add("@idUsuarioResponsableNotificacion", SqlDbType.Int).Value = IDUsuarioResponsable;

                    SqlParameter p_IDNotificacionApp = new SqlParameter("@idNotificacionApp", SqlDbType.Int);
                    p_IDNotificacionApp.Direction = ParameterDirection.InputOutput;
                    p_IDNotificacionApp.Value = 0;
                    Cmd.Parameters.Add(p_IDNotificacionApp);

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    IDNotificacionApp = Convert.ToInt32(p_IDNotificacionApp.Value);
                }
            }
        }

        public static void SetEditarFactura(string Proy, string NroFactura, decimal monto, string tipoValor)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("setEditarNroFactura"))
                {
                    Cmd.Parameters.Add("@proy", SqlDbType.Int).Value = Proy;
                    Cmd.Parameters.Add("@nroFactura", SqlDbType.Int).Value = NroFactura;
                    Cmd.Parameters.Add("@monto", SqlDbType.Decimal).Value = monto;
                    Cmd.Parameters.Add("@tipoValor", SqlDbType.Int).Value = tipoValor;
                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    //List<string> Lst = new List<string>();
                    //SqlDataReader Reader = Cmd.ExecuteReader();

                    //while (Reader.Read())
                    //    Lst.Add(Reader["ID_PROYECTO"].ToString());
                    //    Lst.Add(Reader["Nro_factura"].ToString());
                    //    Lst.Add(Reader["MONTO"].ToString());

                    //return Lst;
                }
            }

        }

        public static List<Entities.FacturaEntity> GetFacturaProyecto(int IDProyecto)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getFacturaProy"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();
                    List<Entities.FacturaEntity> Lst = new List<Entities.FacturaEntity>();

                    while (Reader.Read())
                    {
                        Entities.FacturaEntity Fact = new Entities.FacturaEntity();
                        Lst.Add(new Entities.FacturaEntity(Convert.ToInt32(Reader["ID_FACTURA"]), Convert.ToInt32(Reader["ID_ESTADOFACTURA"]), Convert.ToInt32(Reader["ID_PROYECTO"]), Reader["NOMBRE_PROYECTO"].ToString(), Convert.ToInt32(Reader["ID_TIPOVALOR"]), Reader["SIGLA"].ToString(), Reader["NOMBRE_FANTASIA"].ToString(), Reader["GLOSA"].ToString(), Convert.ToDateTime(Reader["FECHA_ORDEN_FACTURACION"]), Convert.ToDouble(Reader["MONTO"]), Convert.ToInt32(Reader["NRO_FACTURA"]), Convert.ToBoolean(Reader["E_ENVIADO"]), Reader["RUTA_PDF"].ToString()));
                    }

                    return Lst;
                }
            }
        }
    }

}

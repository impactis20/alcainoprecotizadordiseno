﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.DAL
{
    public class ClienteDAL
    {
        private static DAO_Conexion Conexion = new DAO_Conexion();

        public static int AddCliente(int Rut, string DV, string NombreFantasia, string RazonSocial, string Giro, string Direccion, string Telefono, string Comuna, string Ciudad, int IDUsuario)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("addCliente"))
                {
                    int ID = 0;

                    Cmd.Parameters.Add("@nombreFantasia", SqlDbType.VarChar, 50).Value = NombreFantasia;
                    Cmd.Parameters.Add("@rut", SqlDbType.Int).Value = Rut;
                    Cmd.Parameters.Add("@dv", SqlDbType.Char, 1).Value = DV;
                    Cmd.Parameters.Add("@razonsocial", SqlDbType.VarChar, 100).Value = RazonSocial;
                    Cmd.Parameters.Add("@giro", SqlDbType.VarChar, 200).Value = Giro;
                    Cmd.Parameters.Add("@direccion", SqlDbType.VarChar, 100).Value = Direccion;
                    Cmd.Parameters.Add("@telefono", SqlDbType.VarChar, 20).Value = Telefono;
                    //Cmd.Parameters.Add("@idRegion", SqlDbType.Int).Value = IDRegion;
                    Cmd.Parameters.Add("@comuna", SqlDbType.VarChar, 100).Value = Comuna;
                    Cmd.Parameters.Add("@ciudad", SqlDbType.VarChar, 100).Value = Ciudad;
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = IDUsuario;
                    Cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int)).Direction = ParameterDirection.ReturnValue; //variable recibe ID establecida como variable de retorno
                    //Cmd.Parameters["@ID"].Direction = ParameterDirection.ReturnValue; //

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    ID = Convert.ToInt16(Cmd.Parameters["@ID"].Value);

                    return ID;
                }
            }
        }

        public static List<Entities.ClienteEntity> GetCliente(int ID, string NombreFantasia, string RazonSocial, int Rut)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getCliente"))
                {
                    Cmd.Parameters.Add("@idCliente", SqlDbType.Int).Value = ID;
                    Cmd.Parameters.Add("@nombreFantasia", SqlDbType.VarChar).Value = NombreFantasia;
                    Cmd.Parameters.Add("@razonsocial", SqlDbType.VarChar).Value = RazonSocial;
                    Cmd.Parameters.Add("@rut", SqlDbType.Int).Value = Rut;

                    Con.Open();
                    List<Entities.ClienteEntity> Lst = new List<Entities.ClienteEntity>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                    {
                        Entities.ClienteEntity Cliente = new Entities.ClienteEntity();
                        Cliente.ID = Convert.ToInt16(Reader["ID_CLIENTE"].ToString());
                        Cliente.Rut = Convert.ToInt32(Reader["RUT"].ToString());
                        Cliente.DV = Reader["DIGITO_V"].ToString();
                        Cliente.NombreFantasia = Reader["NOMBRE_FANTASIA"].ToString();
                        Cliente.RazonSocial = Reader["RAZON_SOCIAL"].ToString();
                        Cliente.Giro = Reader["GIRO"].ToString();
                        Cliente.Direccion = Reader["DIRECCION"].ToString();
                        Cliente.Telefono = Reader["TELEFONO"].ToString();
                        Cliente.Comuna = Reader["COMUNA"].ToString();
                        Cliente.Ciudad = Reader["CIUDAD"].ToString();

                        Lst.Add(Cliente);
                    }

                    return Lst;
                }
            }
        }

        public static void SetCliente(int IDCliente, int Rut, string DV, string NombreFantasia, string RazonSocial, string Giro, string Direccion, string Telefono, string Comuna, string Ciudad)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("setCliente"))
                {
                    Cmd.Parameters.Add("@idCliente", SqlDbType.Int).Value = IDCliente;
                    Cmd.Parameters.Add("@rut", SqlDbType.Int).Value = Rut;
                    Cmd.Parameters.Add("@dv", SqlDbType.Char, 1).Value = DV;
                    Cmd.Parameters.Add("@nombreFantasia", SqlDbType.VarChar, 50).Value = NombreFantasia;
                    Cmd.Parameters.Add("@razonSocial", SqlDbType.VarChar, 100).Value = RazonSocial;
                    Cmd.Parameters.Add("@giro", SqlDbType.VarChar, 200).Value = Giro;
                    Cmd.Parameters.Add("@direccion", SqlDbType.VarChar, 100).Value = Direccion;
                    Cmd.Parameters.Add("@telefono", SqlDbType.VarChar, 12).Value = Telefono;
                    //Cmd.Parameters.Add("@idRegion", SqlDbType.Int).Value = IDRegion;
                    Cmd.Parameters.Add("@comuna", SqlDbType.VarChar, 100).Value = Comuna;
                    Cmd.Parameters.Add("@ciudad", SqlDbType.VarChar, 100).Value = Ciudad;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static void DelCliente(int ID)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("delCliente"))
                {
                    Cmd.Parameters.Add("@idCliente", SqlDbType.Int).Value = ID;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static List<string> GetClienteJSON(string prefix)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getClienteJSON"))
                {
                    Cmd.Parameters.Add("@prefix", SqlDbType.VarChar).Value = prefix;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    List<string> Lst = new List<string>();

                    while (Reader.Read())
                        Lst.Add(Reader["NOMBRE_FANTASIA"].ToString());

                    return Lst;
                }
            }
        }

        public static bool ClienteExiste(int RutSinDV)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("ClienteExiste"))
                {
                    Cmd.Parameters.Add("@rutSinDV", SqlDbType.Int, 8).Value = RutSinDV;
                    Cmd.Parameters.Add("@resp", SqlDbType.Bit).Direction = ParameterDirection.ReturnValue;

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    return Convert.ToBoolean(Cmd.Parameters["@resp"].Value);
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.DAL
{
    public class TipoValorDAL
    {
        private static DAO_Conexion Conexion = new DAO_Conexion();

        public static List<string> Lst { get; private set; }

        /// <summary>
        /// Obtiene los tipos de moneda y su valor (clp, uf, usd, euro)
        /// </summary>
        /// <returns></returns>
        public static List<Entities.TipoValorEntity> GetTipoValor()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getTipoValor"))
                {
                    Con.Open();
                    List<Entities.TipoValorEntity> Lst = new List<Entities.TipoValorEntity>();
                    //Lst.Add(new Entities.TipoValorEntity() { Sigla = "Seleccione Moneda" });
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                    {
                        Entities.TipoValorEntity TipoValor = new Entities.TipoValorEntity();

                        TipoValor.ID = Convert.ToInt16(Reader["ID_TIPOVALOR"].ToString());
                        TipoValor.Nombre = Reader["NOMBRE"].ToString();
                        TipoValor.Sigla = Reader["SIGLA"].ToString();

                        if (!string.IsNullOrEmpty(Reader["valor"].ToString()) && !string.IsNullOrEmpty(Reader["fecha_ultima_actualizacion"].ToString()))
                        {
                            TipoValor.Valor = Convert.ToDecimal(Reader["valor"].ToString());
                            TipoValor.FechaUltimaActualizacion = Convert.ToDateTime(Reader["fecha_ultima_actualizacion"].ToString());
                        }
                        Lst.Add(TipoValor);
                    }

                    return Lst;
                }
            }
        }

        /// <summary>
        /// Devuelve el valor del tipo de valor o divisa del día de hoy.
        /// </summary>
        /// <param name="IDTipoValor">ID del tipo de valor o divisa.</param>
        /// <returns></returns>
        public static decimal GetValorTipoValor(int IDTipoValor)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("dbo.ValorTipoValorHoy"))
                {
                    Cmd.Parameters.Add("@idTipoValor", SqlDbType.Int).Value = IDTipoValor;
                    Cmd.Parameters.Add("@valor", SqlDbType.Decimal).Direction = ParameterDirection.ReturnValue;

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    decimal Respuesta = Convert.ToDecimal(Cmd.Parameters["@valor"].Value);

                    return Respuesta;
                }
            }
        }

        /// <summary>
        /// Devuelve el valor del tipo de valor o divisa según fecha ingresada.
        /// </summary>
        /// <param name="IDTipoValor">ID del tipo de valor o divisa.</param>
        /// <param name="Fecha">Fecha a consultar.</param>
        /// <returns></returns>
        public static float GetValorTipoValor(int IDTipoValor, DateTime Fecha)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("dbo.ValorTipoValorFecha"))
                {
                    Cmd.Parameters.Add("@idTipoValor", SqlDbType.Int).Value = IDTipoValor;
                    Cmd.Parameters.Add("@fecha", SqlDbType.Date).Value = Fecha.Date;
                    Cmd.Parameters.Add("@valor", SqlDbType.Decimal).Direction = ParameterDirection.ReturnValue;

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    string Valor = Cmd.Parameters["@valor"].Value.ToString();

                    if (string.IsNullOrEmpty(Valor))
                        return 0;
                    else
                        return float.Parse(Valor);
                }
            }
        }

       

        public static void AddActualValorTipoValor(DataTable dt)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("addActualValorTipoValor"))
                {
                    Cmd.Parameters.Add("@listHistorialTipoValor", SqlDbType.Structured).Value = dt;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }
    }
}

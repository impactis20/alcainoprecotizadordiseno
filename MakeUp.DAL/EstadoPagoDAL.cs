﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.DAL
{
    public class EstadoPagoDAL
    {
        private static DAO_Conexion Conexion = new DAO_Conexion();

        public static bool AddEstadoPago(int IDProyecto, decimal Porcentaje, decimal Monto, string Glosa, int IDUsuarioCreador, DateTime Fecha)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("addEstadoPago"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@porcentaje", SqlDbType.Decimal).Value = Porcentaje;
                    Cmd.Parameters.Add("@monto", SqlDbType.Decimal).Value = Monto;
                    Cmd.Parameters.Add("@glosa", SqlDbType.VarChar).Value = Glosa;
                    Cmd.Parameters.Add("@idUsuarioCreador", SqlDbType.Int).Value = IDUsuarioCreador;
                    Cmd.Parameters.Add("@fechaCreacion", SqlDbType.DateTime).Value = Fecha;

                    SqlParameter p_Porcentaje = new SqlParameter("@resp", SqlDbType.Bit);
                    p_Porcentaje.Direction = ParameterDirection.Output;
                    p_Porcentaje.Value = 0;
                    Cmd.Parameters.Add(p_Porcentaje);

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                    
                    return Convert.ToBoolean(p_Porcentaje.Value);
                }
            }
        }

        public static List<Entities.EstadoPagoEntity> GetEstadoPago(int IDProyecto)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getEstadoPago"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    List<Entities.EstadoPagoEntity> Lst = new List<Entities.EstadoPagoEntity>();

                    while (Reader.Read())
                    {
                        Entities.EstadoPagoEntity EstadoPago = new Entities.EstadoPagoEntity();
                        EstadoPago.ID = Convert.ToInt16(Reader["ID_ESTADOPAGO"].ToString());
                        EstadoPago.Monto = Convert.ToDecimal(Reader["monto"].ToString());
                        EstadoPago.Glosa = Reader["glosa_ep"].ToString();
                        EstadoPago.Porcentaje = Convert.ToDecimal(Reader["porcentaje"]);
                        EstadoPago.FechaCreacion = Convert.ToDateTime(Reader["FECHA_CREACION"]);
                        EstadoPago.Facturado = Convert.ToBoolean(Reader["FACTURADO"]);
                        EstadoPago.FacturaEmitida = Convert.ToBoolean(Reader["EMITIDO_FACTURACION"]);

                        if(EstadoPago.FacturaEmitida)
                            EstadoPago.FechaPreFacturado = Convert.ToDateTime(Reader["FECHA_ORDEN_FACTURACION"]);

                        Lst.Add(EstadoPago);
                    }

                    return Lst;
                }
            }
        }

        public static int DelEstadoPago(int IDEstadoPago)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("delEstadoPago"))
                {
                    Cmd.Parameters.Add("@idEstadoPago", SqlDbType.Int).Value = IDEstadoPago;
                    Cmd.Parameters.Add("@respuesta", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    //Respuestas: 0 = el estado de pago no fue eliminado porque fue emitido a facturacion
                    //            1 = estado de pago eliminado

                    return Convert.ToInt16(Cmd.Parameters["@respuesta"].Value);
                }
            }
        }

        public static void SetEstadoPago(int IDProyecto, string DecisionEP)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("setEstadoPago"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@decisionEP", SqlDbType.VarChar, 10).Value = DecisionEP;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        //public static void SetEstadoPagoFacturado(int IDProyecto)
        //{
        //    using (SqlConnection Con = Conexion.GetConexionMakeUp())
        //    {
        //        using (SqlCommand Cmd = Conexion.GetCommand("setEstadoPagoFacturado"))
        //        {
        //            Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;

        //            Con.Open();
        //            Cmd.ExecuteNonQuery();
        //        }
        //    }
        //}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.DAL
{
    public class GenericoDAL
    {
        private static DAO_Conexion Conexion = new DAO_Conexion();

        public static List<Entities.GenericoEntity.Region> GetRegion()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getRegion"))
                {
                    Con.Open();
                    List<Entities.GenericoEntity.Region> Lst = new List<Entities.GenericoEntity.Region>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                        Lst.Add(new Entities.GenericoEntity.Region(Convert.ToInt16(Reader["id_region"].ToString()), Reader["nombre"].ToString()));

                    return Lst;
                }
            }
        }

        public static List<string> GetAutocompletar(string Prefix, string Tabla, string Campo)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getAutocompletar"))
                {
                    Cmd.Parameters.Add("@prefix", SqlDbType.NVarChar, 100).Value = Prefix;
                    Cmd.Parameters.Add("@tabla", SqlDbType.NVarChar, 25).Value = Tabla;
                    Cmd.Parameters.Add("@campo", SqlDbType.NVarChar, 60).Value = Campo;

                    Con.Open();
                    List<string> Lst = new List<string>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                        Lst.Add(Reader["valor"].ToString());

                    return Lst;
                }
            }
        }
    }
}

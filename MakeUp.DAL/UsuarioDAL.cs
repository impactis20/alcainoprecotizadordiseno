﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices;

namespace MakeUp.DAL
{
    public class UsuarioDAL
    {
        private static DAO_Conexion Conexion = new DAO_Conexion();

        public static int AddUsuario(string UserName, string Nombre, string APaterno, string AMaterno, string Email, bool VisualizaTodoContacto, int IDRol, DateTime FechaCreacion, DataTable dtPagina, DataTable dtControl, string codigoManager)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("addUsuario"))
                {
                    Cmd.Parameters.Add("@idRol", SqlDbType.Int).Value = IDRol;
                    Cmd.Parameters.Add("@userName", SqlDbType.VarChar).Value = UserName;
                    Cmd.Parameters.Add("@nombre", SqlDbType.VarChar).Value = Nombre;
                    Cmd.Parameters.Add("@aPaterno", SqlDbType.VarChar).Value = APaterno;
                    Cmd.Parameters.Add("@aMaterno", SqlDbType.VarChar).Value = AMaterno;
                    Cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = Email;
                    Cmd.Parameters.Add("@visualizaTodoContacto", SqlDbType.Bit).Value = VisualizaTodoContacto;
                    Cmd.Parameters.Add("@fechaCreacion", SqlDbType.DateTime).Value = FechaCreacion;
                    Cmd.Parameters.Add("@listPagina", SqlDbType.Structured).Value = dtPagina;
                    Cmd.Parameters.Add("@listControl", SqlDbType.Structured).Value = dtControl;
                    Cmd.Parameters.Add("@codigoMana", SqlDbType.VarChar).Value = codigoManager;

                    Cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int)).Direction = ParameterDirection.ReturnValue;

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    return Convert.ToInt16(Cmd.Parameters["@ID"].Value);
                }
            }
        }

        public static List<Entities.UsuarioEntity> GetUsuarioBaja()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getUsuarioBaja"))
                {
                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    List<Entities.UsuarioEntity> Lst = new List<Entities.UsuarioEntity>();

                    while (Reader.Read())
                        Lst.Add(new Entities.UsuarioEntity(Convert.ToInt16(Reader["ID_USUARIO"].ToString()), Reader["USERNAME"].ToString(), Reader["NOMBRE"].ToString(), Reader["APELLIDO_PA"].ToString(), Reader["APELLIDO_MA"].ToString(), Reader["EMAIL"].ToString()));

                    return Lst;
                }
            }
        }

        public static List<Entities.UsuarioEntity> GetUsuario(int ID, string Username, string Nombre, string Email, bool? VisualizaTodoContacto)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getUsuario"))
                {
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = ID;
                    Cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = Username;
                    Cmd.Parameters.Add("@nombre", SqlDbType.VarChar).Value = Nombre;
                    Cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = Email;
                    Cmd.Parameters.Add("@visualizaTodoContacto", SqlDbType.Bit).Value = VisualizaTodoContacto == null ? (object)DBNull.Value : VisualizaTodoContacto;
                    
                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();
                    List<Entities.UsuarioEntity> Lst = new List<Entities.UsuarioEntity>();

                    while (Reader.Read())
                    {
                        Entities.UsuarioEntity Usuario = new Entities.UsuarioEntity(Convert.ToInt16(Reader["ID_USUARIO"].ToString()), Reader["USERNAME"].ToString(), Reader["NOMBRE"].ToString(), Reader["EMAIL"].ToString(), Convert.ToBoolean(Reader["VISUALIZA_TODO_CONTACTO"]));
                        Usuario.Rol = new Entities.RolPredeterminadoEntity(Convert.ToInt16(Reader["ID_ROL"].ToString()), Reader["NOMBRE_ROL"].ToString());

                        Lst.Add(Usuario);
                    }

                    return Lst;
                }
            }
        }

        public static Entities.UsuarioEntity GetValidaUsername(string Username)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getValidaUsername"))
                {
                    try {
                        Cmd.Parameters.Add("@username", SqlDbType.VarChar, 25).Value = Username;

                        Con.Open();
                        SqlDataReader Reader = Cmd.ExecuteReader();

                        if (Reader.Read())
                            return new Entities.UsuarioEntity(Convert.ToInt16(Reader["ID_USUARIO"].ToString()), Username, string.Format("{0} {1} {2}", Reader["NOMBRE"].ToString(), Reader["APELLIDO_PA"].ToString(), Reader["APELLIDO_MA"].ToString()), Reader["EMAIL"].ToString(), Convert.ToBoolean(Reader["VISUALIZA_TODO_CONTACTO"].ToString()));

                        return null;
                    }
                    catch (Exception ex) {

                        Console.Write("Error al recibir respuesta del Servidor de Base de datos" + ex.Message);
                        return null;
                    }
                }
            }
        }

        public static List<string> GetNombreCompletoUsuarioJSON(string prefix)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getNombreCompletoUsuarioJSON"))
                {
                    Cmd.Parameters.Add("@prefix", SqlDbType.VarChar, 50).Value = prefix;
                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();
                    List<string> Lst = new List<string>();

                    while (Reader.Read())
                        Lst.Add(Reader["NOMBRE_COMPLETO"].ToString());

                    return Lst;
                }
            }
        }

        public static void SetUsuario(int IDUsuario, int IDRol, bool VisualizaTodoContacto, DataTable dtPagina, DataTable dtControl)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("setUsuario"))
                {
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = IDUsuario;
                    Cmd.Parameters.Add("@idRol", SqlDbType.Int).Value = IDRol;
                    Cmd.Parameters.Add("@visualizaTodoContacto", SqlDbType.Bit).Value = VisualizaTodoContacto;
                    Cmd.Parameters.Add("@listPagina", SqlDbType.Structured).Value = dtPagina;
                    Cmd.Parameters.Add("@listControl", SqlDbType.Structured).Value = dtControl;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static void DelUsuario(int ID)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("delUsuario"))
                {
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = ID;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static void SetDarAltaUsuario(int ID)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("setDarAltaUsuario"))
                {
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = ID;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static bool GetEmailExiste(string Email)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("dbo.EmailExiste"))
                {
                    Cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = Email;
                    Cmd.Parameters.Add("@result", SqlDbType.Bit).Direction = ParameterDirection.ReturnValue;

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    bool Respuesta = Convert.ToBoolean(Cmd.Parameters["@result"].Value);

                    return Respuesta;
                }
            }
        }

        public static bool GetAccesoPagina(int IDUsuario, string Path)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("dbo.AccesoPagina"))
                {
                    Cmd.Parameters.Add("@IDUsuario", SqlDbType.Int).Value = IDUsuario;
                    Cmd.Parameters.Add("@path", SqlDbType.VarChar, 100).Value = Path;
                    Cmd.Parameters.Add("@respuesta", SqlDbType.Bit).Direction = ParameterDirection.ReturnValue;

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    bool Respuesta = Convert.ToBoolean(Cmd.Parameters["@respuesta"].Value);

                    return Respuesta;
                }
            }
        }

        public static List<Entities.PaginaEntity.Control> GetControlAutorizado(int IDUsuario, string Path)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getControlAutorizado"))
                {
                    Cmd.Parameters.Add("@IDUsuario", SqlDbType.Int).Value = IDUsuario;
                    Cmd.Parameters.Add("@path", SqlDbType.VarChar, 100).Value = Path;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    List<Entities.PaginaEntity.Control> Lst = new List<Entities.PaginaEntity.Control>();

                    while (Reader.Read())
                        Lst.Add(new Entities.PaginaEntity.Control(Reader["CONTROL_ID"].ToString()));

                    return Lst;
                }
            }
        }
        
        public static List<Entities.PaginaEntity.Control> GetControlGridviewAutorizado(int IDUsuario, string Path, string IDGridview, short TipoControl)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getControlGridviewAutorizado"))
                {
                    Cmd.Parameters.Add("@IDUsuario", SqlDbType.Int).Value = IDUsuario;
                    Cmd.Parameters.Add("@path", SqlDbType.VarChar, 100).Value = Path;
                    Cmd.Parameters.Add("@idGridview", SqlDbType.VarChar, 40).Value = IDGridview;
                    Cmd.Parameters.Add("@tipoControl", SqlDbType.TinyInt).Value = TipoControl;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    List<Entities.PaginaEntity.Control> Lst = new List<Entities.PaginaEntity.Control>();

                    while (Reader.Read())
                        Lst.Add(new Entities.PaginaEntity.Control(Reader["CONTROL_ID"].ToString()));

                    return Lst;
                }
            }
        }

        public static bool GetUsuarioValido(int IDUsuario)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("UsuarioValido"))
                {
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = IDUsuario;
                    Cmd.Parameters.Add("@respuesta", SqlDbType.Bit).Direction = ParameterDirection.ReturnValue;

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    bool Respuesta = Convert.ToBoolean(Cmd.Parameters["@respuesta"].Value);

                    return Respuesta;
                }
            }
        }

        public static Entities.RolPredeterminadoEntity GetRolUsuario(int IDUsuario)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getRolUsuario"))
                {
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = IDUsuario;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    Entities.RolPredeterminadoEntity Rol = new Entities.RolPredeterminadoEntity();

                    while (Reader.Read())
                        Rol = new Entities.RolPredeterminadoEntity(Convert.ToInt16(Reader["ID_ROLPREDETERMINADO"]), Reader["NOMBRE"].ToString(), Reader["DESCRIPCION"].ToString());

                    Reader.NextResult();

                    while (Reader.Read())
                        Rol.Modulo.Add(new Entities.PaginaEntity(Convert.ToInt16(Reader["ID_PAGINA"].ToString()), Reader["NOMBRE"].ToString(), Reader["URI"].ToString(), Convert.ToBoolean(Reader["SELECCIONADO"]), Convert.ToBoolean(Reader["PERMITIDO_POR_DEFECTO"])));

                    Reader.NextResult();

                    while (Reader.Read())
                        Rol.Modulo.Find(x => x.ID == Convert.ToInt16(Reader["ID_PAGINA"].ToString())).Controles.Add(new Entities.PaginaEntity.Control(Convert.ToInt16(Reader["ID_CONTROL"].ToString()), Reader["CONTROL_ID"].ToString(), Reader["DESCRIPCION"].ToString(), Convert.ToBoolean(Convert.ToInt16(Reader["SELECCIONADO"].ToString()))));

                    return Rol;
                }
            }
        }

        public static List<string> GetEjecutivoComercialJSON(string prefix) //Los ejecutivos comerciales son todos aquellos que tienen acceso a Crear un proyecto
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getEjecutivoComercialJSON"))
                {
                    Cmd.Parameters.Add("@prefix", SqlDbType.VarChar).Value = prefix;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    List<string> Lst = new List<string>();

                    while (Reader.Read())
                        Lst.Add(Reader["EJECUTIVO_COMERCIAL"].ToString());

                    return Lst;
                }
            }
        }

        public static List<string> GetSupervisorMontajeJSON(string prefix) //Los ejecutivos comerciales son todos aquellos que tienen acceso a Crear un proyecto
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getSupervisorMontajeJSON"))
                {
                    Cmd.Parameters.Add("@prefix", SqlDbType.VarChar).Value = prefix;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    List<string> Lst = new List<string>();

                    while (Reader.Read())
                        Lst.Add(Reader["SUPERVISOR_MONTAJE"].ToString());

                    return Lst;
                }
            }
        }

        public static List<string> GetUserNameAD(string prefix)
        {
            DirectoryEntry credencialAD = GetCredencialAD();
            DirectorySearcher buscarAC = new DirectorySearcher(credencialAD);
            buscarAC.PageSize = 0;
            buscarAC.SizeLimit = 8;
            buscarAC.Filter = string.Format("(&(objectCategory=person)(objectClass=user)(sAMAccountName=*)(sAMAccountName=*{0}*))", prefix);
            buscarAC.PropertiesToLoad.Add("sAMAccountName"); //Nombre de usuario

            SearchResultCollection resultado = buscarAC.FindAll();

            List<string> Lst = new List<string>();

            if (resultado != null)
                foreach (SearchResult result in resultado)
                    Lst.Add(result.Properties["sAMAccountName"][0].ToString());

            return Lst;
        }

        public static List<Entities.UsuarioEntity> GetUsuarioAD(string UserName)
        {
            DirectoryEntry credencialAD = GetCredencialAD();
            DirectorySearcher buscarAC = new DirectorySearcher(credencialAD);
            buscarAC.PageSize = 0;
            buscarAC.SizeLimit = 8;
            buscarAC.Filter = string.Format("(&(objectCategory=person)(objectClass=user)(mail=*)(sAMAccountName=*)(givenName=*)(sn=*)((sAMAccountName=*{0}*)))", UserName); //(description=*)
            //buscarAC.PropertiesToLoad.Add("description"); //descripción de usuario
            buscarAC.PropertiesToLoad.Add("sAMAccountName"); //Nombre de usuario
            buscarAC.PropertiesToLoad.Add("givenName"); //Primer nombre
            buscarAC.PropertiesToLoad.Add("sn"); //Primer apellido
            buscarAC.PropertiesToLoad.Add("mail"); //email del usuario
            //buscarAC.PropertiesToLoad.Add("cn"); //nombre y apellido

            SearchResultCollection resultado = buscarAC.FindAll();

            if (resultado != null)
            {
                List<Entities.UsuarioEntity> Lst = new List<Entities.UsuarioEntity>();

                foreach (SearchResult result in resultado)
                {
                    Entities.UsuarioEntity Usuario = new Entities.UsuarioEntity();

                    Usuario.UserName = result.Properties["sAMAccountName"][0].ToString();
                    Usuario.Nombre = result.Properties["givenName"][0].ToString();

                    if (result.Properties["SN"][0].ToString().Contains(" "))
                    {
                        Usuario.ApellidoPaterno = result.Properties["sn"][0].ToString().Split(' ')[0];
                        Usuario.ApellidoMaterno = result.Properties["sn"][0].ToString().Split(' ')[1];
                    }
                    else
                    {
                        Usuario.ApellidoPaterno = result.Properties["sn"][0].ToString();
                        Usuario.ApellidoMaterno = "-";
                    }

                    Usuario.Email = result.Properties["mail"][0].ToString();

                    Lst.Add(Usuario);
                }

                return Lst;
            }

            return null;
        }

        public static bool AutenticarUsuario(string UserName, string Password)
        {
            try
            {
                //DirectoryEntry ad = new DirectoryEntry("LDAP://alcaino.net", UserName, Password);
                //object nativeObject = ad.NativeObject;
                return true;
            }
            catch (DirectoryServicesCOMException ex)
            {
                string e = ex.Message;
                return false;
            }
        }

        private static DirectoryEntry GetCredencialAD()
        {
            return new DirectoryEntry("LDAP://alcaino.net", ConfigurationManager.AppSettings["userAD"].ToString(), ConfigurationManager.AppSettings["userPassAD"].ToString());
        }
    }
}

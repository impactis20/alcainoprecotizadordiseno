﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace MakeUp.DAL
{
    public class PaginaDAL
    {
        private static DAO_Conexion Conexion = new DAO_Conexion();

        public static List<Entities.PaginaEntity> GetAllPaginaControles()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getAllPaginaControles"))
                {
                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    List<Entities.PaginaEntity> LstPagina = new List<Entities.PaginaEntity>();

                    while (Reader.Read())
                    {
                        Entities.PaginaEntity Pagina = new Entities.PaginaEntity(Convert.ToInt16(Reader["ID_PAGINA"].ToString()));
                        Pagina.Nombre = Reader["NOMBRE"].ToString();
                        Pagina.Uri = Reader["URI"].ToString();
                        Pagina.PermitidoPorDefecto = Convert.ToBoolean(Reader["PERMITIDO_POR_DEFECTO"]);
                        LstPagina.Add(Pagina);
                    }

                    Reader.NextResult();

                    List<Entities.PaginaEntity.Control> LstControl = new List<Entities.PaginaEntity.Control>();

                    while (Reader.Read())
                        LstPagina.Find(x => x.ID == Convert.ToInt16(Reader["ID_PAGINA"].ToString())).Controles.Add(new Entities.PaginaEntity.Control(Convert.ToInt16(Reader["ID_CONTROL"].ToString()), Reader["CONTROL_ID"].ToString(), Reader["DESCRIPCION"].ToString()));

                    return LstPagina;
                }
            }
        }

        public static bool GetHabilitarControlPagina(int IDControl, int IDUsuario)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("dbo.HabilitarControlPagina"))
                {
                    Cmd.Parameters.Add("@idControl", SqlDbType.Int).Value = IDControl;
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = IDUsuario;
                    Cmd.Parameters.Add("@habilitarControl", SqlDbType.Bit).Direction = ParameterDirection.ReturnValue;

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    bool Respuesta = Convert.ToBoolean(Cmd.Parameters["@habilitarControl"].Value);

                    return Respuesta;
                }
            }
        }
    }
}

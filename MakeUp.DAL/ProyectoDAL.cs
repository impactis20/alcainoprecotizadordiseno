﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.DAL
{
    public class ProyectoDAL
    {
        private static DAO_Conexion Conexion = new DAO_Conexion();

        public static int GetNuevoIDProyecto( int IdUsuario, DateTime Fecha)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getNuevoIDProyecto"))
                {
                   int NuevoID = 0;
                    Cmd.Parameters.Add("@NuevoID", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    Cmd.Parameters.Add("@IdUsuario", SqlDbType.Int).Value = IdUsuario;
                    Cmd.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = Fecha;
                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    NuevoID = Convert.ToInt16(Cmd.Parameters["@NuevoID"].Value);

                    return NuevoID;
                }
            }
        }

        public static double GetValorTotalProyecto(int IDProyecto)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("ValorTotalProyecto"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@valorTotal", SqlDbType.Decimal).Direction = ParameterDirection.ReturnValue;

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    return Convert.ToDouble(Cmd.Parameters["@valorTotal"].Value);
                }
            }
        }

        public static double GetSaldoPorFacturarProyecto(int IDProyecto)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("SaldoPorFacturarProyecto"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@SaldoPorFacturar", SqlDbType.Decimal).Direction = ParameterDirection.ReturnValue;

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    return Convert.ToDouble(Cmd.Parameters["@SaldoPorFacturar"].Value);
                }
            }
        }

        public static int AddProyecto(int IDProyecto,int IDPadre, int IDCliente, int IDUsuarioCreador, int IDEjecutivo, int IDTipoValor, string Nombre, DateTime FechaIngreso, DateTime FechaComprometida, string Direccion, string Comuna, string Directorio)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("addDatosProyecto"))
                {
                    int NuevoID = 0;

                    if (IDPadre == 0)
                        Cmd.Parameters.Add("@idProyectoPadre", SqlDbType.Int).Value = DBNull.Value;
                    else
                        Cmd.Parameters.Add("@idProyectoPadre", SqlDbType.Int).Value = IDPadre;

                    Cmd.Parameters.Add("@idoriginal", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@nombre", SqlDbType.VarChar).Value = Nombre;
                    Cmd.Parameters.Add("@idCliente", SqlDbType.Int).Value = IDCliente;
                    Cmd.Parameters.Add("@idUsuarioCreador", SqlDbType.Int).Value = IDUsuarioCreador;
                    Cmd.Parameters.Add("@fechaIngreso", SqlDbType.DateTime).Value = FechaIngreso;
                    Cmd.Parameters.Add("@fechaComprometida", SqlDbType.DateTime).Value = FechaComprometida;
                    Cmd.Parameters.Add("@idEjecutivo", SqlDbType.Int).Value = IDEjecutivo;
                    Cmd.Parameters.Add("@idTipoValor", SqlDbType.Int).Value = IDTipoValor;
                    Cmd.Parameters.Add("@direccionProyecto", SqlDbType.VarChar).Value = Direccion;
                    Cmd.Parameters.Add("@comuna", SqlDbType.VarChar).Value = Comuna;
                    Cmd.Parameters.Add("@directorio", SqlDbType.VarChar).Value = Directorio;
                    Cmd.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    NuevoID = Convert.ToInt16(Cmd.Parameters["@ID"].Value);

                    return NuevoID;
                }
            }
        }

        public static void AddFinalizarCreacion(int IDProyecto, int IDUsuarioCreador, DateTime FechaCreacion, ref int IDNotificacionApp)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("addFinalizarCreacion"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@fechaCreacion", SqlDbType.DateTime).Value = FechaCreacion;
                    Cmd.Parameters.Add("@idUsuarioCreador", SqlDbType.Int).Value = IDUsuarioCreador;
                    SqlParameter p_IDNotificacionApp = new SqlParameter("@idNotificacionApp", SqlDbType.Int);
                    p_IDNotificacionApp.Direction = ParameterDirection.InputOutput;
                    p_IDNotificacionApp.Value = 0;
                    Cmd.Parameters.Add(p_IDNotificacionApp);

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    //obtiene valor IDNotificacion
                    IDNotificacionApp = Convert.ToInt32(p_IDNotificacionApp.Value);
                }
            }
        }

        public static List<Entities.TipoDocumentoEntity> GetTipoDocumento()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getTipoDocumento"))
                {
                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();
                    List<Entities.TipoDocumentoEntity> Lst = new List<Entities.TipoDocumentoEntity>();

                    while (Reader.Read())
                    {
                        Entities.TipoDocumentoEntity TipoDoc = new Entities.TipoDocumentoEntity();

                        TipoDoc.ID = Convert.ToInt16(Reader["ID_TIPODOCUMENTO"].ToString());
                        TipoDoc.Nombre = Reader["NOMBRE"].ToString();
                        TipoDoc.VinculaOC = Convert.ToBoolean(Reader["VINCULAR_OC_PROYECTO"].ToString());

                        Lst.Add(TipoDoc);
                    }

                    return Lst;
                }
            }
        }

        public static List<Entities.DocumentoComercialEntity> GetDocumentoComercial(int IDProyecto)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getDocComercial"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();
                    List<Entities.DocumentoComercialEntity> Lst = new List<Entities.DocumentoComercialEntity>();

                    while (Reader.Read())
                    {
                        Entities.DocumentoComercialEntity DocComercial = new Entities.DocumentoComercialEntity();

                        DocComercial.ID = Convert.ToInt16(Reader["ID_DOCUMENTOCOMERCIAL"].ToString());
                        DocComercial.TipoDocumento = new Entities.TipoDocumentoEntity(Convert.ToInt16(Reader["ID_TIPODOCUMENTO"].ToString()), Reader["nombre_doc"].ToString());
                        DocComercial.Nombre = Reader["NOMBRE"].ToString();
                        DocComercial.Descripcion = Reader["DESCRIPCION"].ToString();

                        Lst.Add(DocComercial);
                    }

                    return Lst;
                }
            }
        }

        public static string AddDocumentoComercial(int IDUsuario, int IDTipoDoc, int IDProyecto, string Nombre, DateTime FechaSubida, string Descripcion)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("addDocComercial"))
                {
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = IDUsuario;
                    Cmd.Parameters.Add("@idTipoDoc", SqlDbType.Int).Value = IDTipoDoc;
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@nombre", SqlDbType.VarChar).Value = Nombre;
                    Cmd.Parameters.Add("@fechaSubida", SqlDbType.DateTime).Value = FechaSubida;
                    Cmd.Parameters.Add("@descripcion", SqlDbType.VarChar).Value = Descripcion;
                    SqlParameter p_Directorio = new SqlParameter("@directorio", SqlDbType.VarChar, 500);
                    p_Directorio.Direction = ParameterDirection.InputOutput;
                    p_Directorio.Value = string.Empty;
                    Cmd.Parameters.Add(p_Directorio);

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    return p_Directorio.Value.ToString();
                }
            }
        }

        public static string DelDocumentoComercial(int IDDocComercial)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("delDocComercial"))
                {
                    Cmd.Parameters.Add("@idDocComercial", SqlDbType.Int).Value = IDDocComercial;
                    SqlParameter RutaArchivo = new SqlParameter("@rutaArchivo", SqlDbType.VarChar, 305);
                    RutaArchivo.Direction = ParameterDirection.InputOutput;
                    RutaArchivo.Value = string.Empty;
                    Cmd.Parameters.Add(RutaArchivo);

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    return RutaArchivo.Value.ToString();
                }
            }
        }

        public static void SetDocumentoComercial(int IDDocComercial, int IDTipoDoc, string Descripcion)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("setDocComercial"))
                {
                    Cmd.Parameters.Add("@idDocComercial", SqlDbType.Int).Value = IDDocComercial;
                    Cmd.Parameters.Add("@idTipoDoc", SqlDbType.Int).Value = IDTipoDoc;
                    Cmd.Parameters.Add("@descDoc", SqlDbType.VarChar).Value = Descripcion;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static void AddDocumentoProyecto(int IDProyecto, string Desc, string Url, DateTime Fecha)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("addDocProyecto"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@descripcion", SqlDbType.VarChar).Value = Desc;
                    Cmd.Parameters.Add("@url", SqlDbType.VarChar).Value = Url;
                    Cmd.Parameters.Add("@fecha", SqlDbType.DateTime).Value = Fecha;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static List<Entities.DocumentoProyectoEntity> GetDocumentoProyecto(int IDProyecto)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getDocProyecto"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    List<Entities.DocumentoProyectoEntity> Lst = new List<Entities.DocumentoProyectoEntity>();

                    while (Reader.Read())
                        Lst.Add(new Entities.DocumentoProyectoEntity(Convert.ToInt16(Reader["ID_DOCUMENTOPROYECTO"].ToString()), Reader["DESCRIPCION"].ToString(), Reader["URL"].ToString()));

                    return Lst;
                }
            }
        }

        public static void DelDocumentoProyecto(int IDDocProyecto)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("delDocProyecto"))
                {
                    Cmd.Parameters.Add("@idDocProyecto", SqlDbType.Int).Value = IDDocProyecto;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static List<Entities.DocumentoComercialEntity> GetOCProyecto(int IDProyecto)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getOCProyecto"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();
                    List<Entities.DocumentoComercialEntity> Lst = new List<Entities.DocumentoComercialEntity>();

                    while (Reader.Read())
                    {
                        Lst.Add(new Entities.DocumentoComercialEntity(Convert.ToInt16(Reader["ID_DOCUMENTOCOMERCIAL"].ToString()), Reader["DESCRIPCION"].ToString()));
                    }

                    return Lst;
                }
            }
        }

        public static int GetValidaCreacionProyecto(int IDProyecto)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getValidaCreacionProyecto"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.VarChar).Value = IDProyecto;
                    Cmd.Parameters.Add("@cont", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    return Convert.ToInt16(Cmd.Parameters["@cont"].Value);
                }
            }
        }

        public static List<Entities.ProyectoEntity> GetBuscarProyecto(int IDProyecto, string NombreProyecto, string NombreFantasia, DataTable dtEstadoNP, int controlUsuario, int rolID, int IDUsuario)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getBuscarProyecto"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@nombreProyecto", SqlDbType.VarChar).Value = NombreProyecto;
                    Cmd.Parameters.Add("@nombreFantasia", SqlDbType.VarChar).Value = NombreFantasia;
                    Cmd.Parameters.Add("@listEstadoNP", SqlDbType.Structured).Value = dtEstadoNP;
                    Cmd.Parameters.Add("@controlUsuario", SqlDbType.Int).Value = controlUsuario;
                    Cmd.Parameters.Add("@rolID", SqlDbType.Int).Value = rolID;
                    Cmd.Parameters.Add("@idUsu", SqlDbType.Int).Value = IDUsuario;

                    Con.Open();
                    List<Entities.ProyectoEntity> Lst = new List<Entities.ProyectoEntity>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                    {
                        Entities.ProyectoEntity Proyecto = new Entities.ProyectoEntity();
                        Proyecto.ID = Convert.ToInt16(Reader["ID_PROYECTO"].ToString());
                        Proyecto.Nombre = Reader["NOMBRE_PROYECTO"].ToString();
                        Proyecto.CantidadTotalNP = Convert.ToInt16(Reader["CONT_NP"].ToString());
                        Proyecto.CantidadNPPorAjustar = Convert.ToInt16(Reader["CANTIDAD_NP_POR_AJUSTAR"].ToString());
                        Proyecto.CantidadNPPorValorizar = Convert.ToInt16(Reader["CANTIDAD_NP_POR_VALORIZAR"].ToString());
                        Proyecto.CantidadNPPorFacturar = Convert.ToInt16(Reader["CANTIDAD_NP_POR_FACTURAR"].ToString());
                        Proyecto.CantidadNPFacturada = Convert.ToInt16(Reader["CANTIDAD_NP_FACTURADA"].ToString());
                        Proyecto.Cliente = new Entities.ClienteEntity(Reader["NOMBRE_FANTASIA"].ToString());
                        Proyecto.PorcentajeTotalEP = float.Parse(Reader["PORCENTAJE_TOTAL_EP"].ToString());
                        Proyecto.EjecutivoComercial = new Entities.UsuarioEntity(Reader["NOMBRE_EJECUTIVO"].ToString(), Reader["APELLIDO_PATERNO_EJECUTIVO"].ToString());

                        if (!string.IsNullOrEmpty(Reader["FECHA_PLANIFICADA"].ToString()))
                            Proyecto.FechaPlanificada = Convert.ToDateTime(Reader["FECHA_PLANIFICADA"].ToString());

                        Proyecto.FechaIngreso = Convert.ToDateTime(Reader["FECHA_INGRESO"].ToString());

                        Proyecto.Estado = true;

                        Lst.Add(Proyecto);
                    }

                    return Lst;
                }
            }
        }

        public static List<Entities.ProyectoEntity> GetBuscarProyectoPorRegularizar(int IDProyecto, string NombreProyecto, string NombreFantasia)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getBuscarProyectoPorRegularizar"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@nombreProyecto", SqlDbType.VarChar).Value = NombreProyecto;
                    Cmd.Parameters.Add("@nombreFantasia", SqlDbType.VarChar).Value = NombreFantasia;

                    Con.Open();
                    List<Entities.ProyectoEntity> Lst = new List<Entities.ProyectoEntity>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                    {
                        Entities.ProyectoEntity Proyecto = new Entities.ProyectoEntity();
                        Proyecto.ID = Convert.ToInt16(Reader["ID_PROYECTO"].ToString());
                        Proyecto.Nombre = Reader["NOMBRE_PROYECTO"].ToString();
                        Proyecto.CantidadTotalNP = Convert.ToInt16(Reader["CONT_NP"].ToString());
                        Proyecto.CantidadNPPorAjustar = Convert.ToInt16(Reader["CANTIDAD_NP_POR_AJUSTAR"].ToString());
                        Proyecto.CantidadNPPorValorizar = Convert.ToInt16(Reader["CANTIDAD_NP_POR_VALORIZAR"].ToString());
                        Proyecto.CantidadNPPorFacturar = Convert.ToInt16(Reader["CANTIDAD_NP_POR_FACTURAR"].ToString());
                        Proyecto.CantidadNPFacturada = Convert.ToInt16(Reader["CANTIDAD_NP_FACTURADA"].ToString());
                        Proyecto.Cliente = new Entities.ClienteEntity(Reader["NOMBRE_FANTASIA"].ToString());
                        Proyecto.PorcentajeTotalEP = float.Parse(Reader["PORCENTAJE_TOTAL_EP"].ToString());
                        Proyecto.EjecutivoComercial = new Entities.UsuarioEntity(Reader["NOMBRE_EJECUTIVO"].ToString(), Reader["APELLIDO_PATERNO_EJECUTIVO"].ToString());

                        if (!string.IsNullOrEmpty(Reader["FECHA_PLANIFICADA"].ToString()))
                            Proyecto.FechaPlanificada = Convert.ToDateTime(Reader["FECHA_PLANIFICADA"].ToString());
                        Proyecto.Estado = true;

                        Lst.Add(Proyecto);
                    }

                    return Lst;
                }
            }
        }

        public static List<Entities.ProyectoEntity> GetBuscarProyectoEliminado(int IDProyecto, string NombreProyecto, string NombreFantasia)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getBuscarProyectoEliminado"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@nombreProyecto", SqlDbType.VarChar).Value = NombreProyecto;
                    Cmd.Parameters.Add("@nombreFantasia", SqlDbType.VarChar).Value = NombreFantasia;

                    Con.Open();
                    List<Entities.ProyectoEntity> Lst = new List<Entities.ProyectoEntity>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                    {
                        Entities.ProyectoEntity Proyecto = new Entities.ProyectoEntity();
                        Proyecto.ID = Convert.ToInt16(Reader["ID_PROYECTO"].ToString());
                        Proyecto.Nombre = Reader["NOMBRE_PROYECTO"].ToString();
                        Proyecto.CantidadTotalNP = Convert.ToInt16(Reader["CONT_NP"].ToString());
                        Proyecto.CantidadNPPorAjustar = Convert.ToInt16(Reader["CANTIDAD_NP_POR_AJUSTAR"].ToString());
                        Proyecto.CantidadNPPorValorizar = Convert.ToInt16(Reader["CANTIDAD_NP_POR_VALORIZAR"].ToString());
                        Proyecto.CantidadNPPorFacturar = Convert.ToInt16(Reader["CANTIDAD_NP_POR_FACTURAR"].ToString());
                        Proyecto.CantidadNPFacturada = Convert.ToInt16(Reader["CANTIDAD_NP_FACTURADA"].ToString());
                        Proyecto.Cliente = new Entities.ClienteEntity(Reader["NOMBRE_FANTASIA"].ToString());
                        Proyecto.EjecutivoComercial = new Entities.UsuarioEntity(Reader["NOMBRE_EJECUTIVO"].ToString(), Reader["APELLIDO_PATERNO_EJECUTIVO"].ToString());

                        if (!string.IsNullOrEmpty(Reader["FECHA_PLANIFICADA"].ToString()))
                            Proyecto.FechaPlanificada = Convert.ToDateTime(Reader["FECHA_PLANIFICADA"].ToString());
                        Proyecto.Estado = false;

                        Lst.Add(Proyecto);
                    }

                    return Lst;
                }
            }
        }

        public static List<string> GetProyectoPadreJSON(string prefix)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getProyectoPadreJSON"))
                {
                    Cmd.Parameters.Add("@prefix", SqlDbType.VarChar).Value = prefix;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    List<string> Lst = new List<string>();

                    while (Reader.Read())
                        Lst.Add(Reader["PROYECTO"].ToString());

                    return Lst;
                }
            }
        }

        public static Entities.ProyectoEntity GetCabeceraProyecto(int IDProyecto)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getCabeceraProyecto"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    Entities.ProyectoEntity Proyecto = null;

                    while (Reader.Read())
                    {
                        Proyecto = new Entities.ProyectoEntity();
                        Proyecto.ID = Convert.ToInt16(Reader["ID_PROYECTO"].ToString());
                        Proyecto.CantidadTotalNP = Convert.ToInt16(Reader["CONT_NP"].ToString());
                        Proyecto.Nombre = Reader["NOMBRE_PROYECTO"].ToString();
                        Proyecto.Tiempo = Convert.ToDecimal(Reader["TIEMPO"].ToString());

                        if (!string.IsNullOrEmpty(Reader["PADRE_ID_PROYECTO"].ToString()))
                            Proyecto.Padre = new Entities.ProyectoEntity(Convert.ToInt16(Reader["PADRE_ID_PROYECTO"].ToString()), Reader["NOMBRE_PROYECTO_PADRE"].ToString());

                        Proyecto.EjecutivoComercial = new Entities.UsuarioEntity(Convert.ToInt16(Reader["ID_USUARIO"].ToString()), Reader["NOMBRE_EJECUTIVO"].ToString(), Reader["APELLIDO_PATERNO_EJECUTIVO"].ToString(), Reader["APELLIDO_MATERNO_EJECUTIVO"].ToString(), Reader["EMAIL_EJECUTIVO"].ToString());
                        Proyecto.SupervisorMontaje = new Entities.UsuarioEntity(Convert.ToInt16(Reader["ID_USUARIO_SUPERVISOR"].ToString()), Reader["NOMBRE_SUPERVISOR"].ToString(), Reader["APELLIDO_PATERNO_SUPERVISOR"].ToString(), Reader["APELLIDO_MATERNO_SUPERVISOR"].ToString(), Reader["EMAIL_SUPERVISOR"].ToString());
                        Proyecto.FechaComprometida = Convert.ToDateTime(Reader["FECHA_COMPROMETIDA"].ToString());
                        Proyecto.FechaIngreso = Convert.ToDateTime(Reader["FECHA_INGRESO"].ToString());

                        if (!string.IsNullOrEmpty(Reader["FECHA_PLANIFICADA"].ToString()))
                            Proyecto.FechaPlanificada = Convert.ToDateTime(Reader["FECHA_PLANIFICADA"].ToString());

                        Proyecto.Cliente = new Entities.ClienteEntity(Convert.ToInt16(Reader["ID_CLIENTE"].ToString()), Reader["NOMBRE_FANTASIA"].ToString(), Reader["RAZON_SOCIAL"].ToString(), Reader["RUT"].ToString());
                        Proyecto.Direccion = Reader["DIRECCION"].ToString();
                        Proyecto.Comuna = Reader["COMUNA"].ToString();
                        Proyecto.Directorio = Reader["DIRECTORIO"].ToString();
                        Proyecto.TipoValor = new Entities.TipoValorEntity(Convert.ToInt16(Reader["ID_TIPOVALOR"].ToString()), Reader["SIGLA_TIPO_VALOR"].ToString(), Convert.ToDecimal(Reader["VALOR_TIPO_VALOR"].ToString()));
                        Proyecto.ValorTotal = Convert.ToDecimal(Reader["VALOR_TOTAL_PROYECTO"].ToString());
                    }

                    return Proyecto;
                }
            }
        }

        public static Entities.ProyectoEntity GetCabeceraProyectoEliminado(int IDProyecto)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getCabeceraProyectoEliminado"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    Entities.ProyectoEntity Proyecto = null;

                    while (Reader.Read())
                    {
                        Proyecto = new Entities.ProyectoEntity();
                        Proyecto.ID = Convert.ToInt16(Reader["ID_PROYECTO"].ToString());
                        Proyecto.CantidadTotalNP = Convert.ToInt16(Reader["CONT_NP"].ToString());
                        Proyecto.Nombre = Reader["NOMBRE_PROYECTO"].ToString();

                        if (!string.IsNullOrEmpty(Reader["PADRE_ID_PROYECTO"].ToString()))
                            Proyecto.Padre = new Entities.ProyectoEntity(Convert.ToInt16(Reader["PADRE_ID_PROYECTO"].ToString()), Reader["NOMBRE_PROYECTO_PADRE"].ToString());

                        Proyecto.EjecutivoComercial = new Entities.UsuarioEntity(Convert.ToInt16(Reader["ID_USUARIO"].ToString()), Reader["NOMBRE_EJECUTIVO"].ToString(), Reader["APELLIDO_PATERNO_EJECUTIVO"].ToString(), Reader["APELLIDO_MATERNO_EJECUTIVO"].ToString(), Reader["EMAIL_EJECUTIVO"].ToString());
                        Proyecto.FechaComprometida = Convert.ToDateTime(Reader["FECHA_COMPROMETIDA"].ToString());
                        Proyecto.FechaIngreso = Convert.ToDateTime(Reader["FECHA_INGRESO"].ToString());

                        if (!string.IsNullOrEmpty(Reader["FECHA_PLANIFICADA"].ToString()))
                            Proyecto.FechaPlanificada = Convert.ToDateTime(Reader["FECHA_PLANIFICADA"].ToString());

                        Proyecto.Cliente = new Entities.ClienteEntity(Convert.ToInt16(Reader["ID_CLIENTE"].ToString()), Reader["NOMBRE_FANTASIA"].ToString());
                        Proyecto.Direccion = Reader["DIRECCION"].ToString();
                        Proyecto.Comuna = Reader["COMUNA"].ToString();
                        Proyecto.Directorio = Reader["DIRECTORIO"].ToString();
                        Proyecto.TipoValor = new Entities.TipoValorEntity(Convert.ToInt16(Reader["ID_TIPOVALOR"].ToString()), Reader["SIGLA_TIPO_VALOR"].ToString(), Convert.ToDecimal(Reader["VALOR_TIPO_VALOR"].ToString()));
                        Proyecto.ValorTotal = Convert.ToDecimal(Reader["VALOR_TOTAL_PROYECTO"].ToString());
                        Proyecto.Motivo = Reader["MOTIVO"].ToString();
                    }

                    return Proyecto;
                }
            }
        }

        /// <summary>
        /// Actualiza los datos del proyecto
        /// </summary>
        /// <param name="IDProyecto"></param>
        /// <param name="NombreProyecto"></param>
        /// <param name="FechaComprometida"></param>
        /// <param name="FechaPlanificada"></param>
        /// <param name="IDEjecutivoComercial"></param>
        /// <param name="Direccion"></param>
        /// <param name="Comuna"></param>
        /// <param name="Edita"></param>
        /// <param name="IDNotificacionApp1">Retorna el ID de notificación de actualización de datos de proyecto (nombre, comuna o dirección).</param>
        /// <param name="IDNotificacionApp2">Retorna el ID de notificación de actualización de fecha comprometida del proyecto.</param>
        /// <param name="IDNotificacionApp3">Retorna el ID de notificación de actualización de fecha planificada del proyecto.</param>
        public static bool SetCabeceraProyecto(int IDProyecto, int IDUsuarioResponsable, string NombreProyecto, DateTime FechaComprometida, DateTime FechaPlanificada, int IDEjecutivoComercial, string Direccion, string Comuna, DataTable dtJefesProyecto, DateTime FechaEvento, ref int IDNotificacionApp1, ref int IDNotificacionApp2, ref int IDNotificacionApp3, ref int IDNotificacionApp4, int IDSupervisorMontaje, int IDCliente, decimal TiempoDiseño)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("setCabeceraProyecto"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@idUsuarioResponsable", SqlDbType.Int).Value = IDUsuarioResponsable;
                    Cmd.Parameters.Add("@nombreProyecto", SqlDbType.VarChar, 50).Value = NombreProyecto;
                    Cmd.Parameters.Add("@fechaComprometida", SqlDbType.DateTime).Value = FechaComprometida;
                    Cmd.Parameters.Add("@fechaPlanificada", SqlDbType.Date).Value = FechaPlanificada;
                    Cmd.Parameters.Add("@idEjecutivoComercial", SqlDbType.Int).Value = IDEjecutivoComercial;
                    Cmd.Parameters.Add("@direccion", SqlDbType.VarChar, 200).Value = Direccion;
                    Cmd.Parameters.Add("@comuna", SqlDbType.VarChar, 100).Value = Comuna;
                    Cmd.Parameters.Add("@listJefeProyecto", SqlDbType.Structured).Value = dtJefesProyecto;
                    Cmd.Parameters.Add("@fechaEvento", SqlDbType.DateTime, 100).Value = FechaEvento;
                    Cmd.Parameters.Add("@idSupervisorMontaje", SqlDbType.Int).Value = IDSupervisorMontaje;
                    Cmd.Parameters.Add("@idCliente", SqlDbType.Int).Value = IDCliente;
                    Cmd.Parameters.Add("@tiempo", SqlDbType.Decimal).Value = TiempoDiseño;


                    SqlParameter p_IDNotificacionApp1 = new SqlParameter("@idNotificacionApp1", SqlDbType.Int);
                    p_IDNotificacionApp1.Direction = ParameterDirection.InputOutput;
                    p_IDNotificacionApp1.Value = 0;
                    Cmd.Parameters.Add(p_IDNotificacionApp1);

                    SqlParameter p_IDNotificacionApp2 = new SqlParameter("@idNotificacionApp2", SqlDbType.Int);
                    p_IDNotificacionApp2.Direction = ParameterDirection.InputOutput;
                    p_IDNotificacionApp2.Value = 0;
                    Cmd.Parameters.Add(p_IDNotificacionApp2);

                    SqlParameter p_IDNotificacionApp3 = new SqlParameter("@idNotificacionApp3", SqlDbType.Int);
                    p_IDNotificacionApp3.Direction = ParameterDirection.InputOutput;
                    p_IDNotificacionApp3.Value = 0;
                    Cmd.Parameters.Add(p_IDNotificacionApp3);

                    SqlParameter p_IDNotificacionApp4 = new SqlParameter("@idNotificacionApp4", SqlDbType.Int);
                    p_IDNotificacionApp4.Direction = ParameterDirection.InputOutput;
                    p_IDNotificacionApp4.Value = 0;
                    Cmd.Parameters.Add(p_IDNotificacionApp4);

                    SqlParameter p_Edita = new SqlParameter("@edita", SqlDbType.Bit);
                    p_Edita.Direction = ParameterDirection.InputOutput;
                    p_Edita.Value = 0;
                    Cmd.Parameters.Add(p_Edita);

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    IDNotificacionApp1 = Convert.ToInt32(p_IDNotificacionApp1.Value);
                    IDNotificacionApp2 = Convert.ToInt32(p_IDNotificacionApp2.Value);
                    IDNotificacionApp3 = Convert.ToInt32(p_IDNotificacionApp3.Value);
                    IDNotificacionApp4 = Convert.ToInt32(p_IDNotificacionApp4.Value);

                    return Convert.ToBoolean(p_Edita.Value);
                }
            }
        }

        public static List<Entities.HistorialProyectoEntity> GetHistorial(int IDProyecto, string Sorting)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getHistorialProyecto"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@sorting", SqlDbType.VarChar, 4).Value = Sorting;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    List<Entities.HistorialProyectoEntity> Lst = new List<Entities.HistorialProyectoEntity>();

                    while (Reader.Read())
                        Lst.Add(new Entities.HistorialProyectoEntity(Reader["ICONO"].ToString(), Reader["NOMBRE_USUARIO"].ToString(), Reader["A_PATERNO_USUARIO"].ToString(), Reader["DESCRIPCION_HISTORIAL"].ToString(), Convert.ToDateTime(Reader["FECHA"].ToString())));

                    return Lst;
                }
            }
        }

        //public static void DelProyecto(int IDProyecto)
        //{
        //    using (SqlConnection Con = Conexion.GetConexionMakeUp())
        //    {
        //        using (SqlCommand Cmd = Conexion.GetCommand("delProyecto"))
        //        {
        //            Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;

        //            Con.Open();
        //            Cmd.ExecuteNonQuery();
        //        }
        //    }
        //}

        public static void DelProyecto(int IDProyecto, string Observacion)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("delProyecto"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@observacion", SqlDbType.VarChar, 500).Value = Observacion;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static bool GetProyectoEsCLP(int IDProyecto)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("SaldoPorFacturarProyecto"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@aproximarValor", SqlDbType.Bit).Direction = ParameterDirection.ReturnValue;

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    return Convert.ToBoolean(Cmd.Parameters["@aproximarValor"].Value);
                }
            }
        }

        public static List<Entities.UsuarioEntity> GetJefeProyecto(int IDProyecto)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getJefeProyecto"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    List<Entities.UsuarioEntity> Lst = new List<Entities.UsuarioEntity>();

                    while (Reader.Read())
                        Lst.Add(new Entities.UsuarioEntity { ID = Convert.ToInt16(Reader["ID_USUARIO"]), Nombre = Reader["NOMBRE"].ToString(), ApellidoPaterno = Reader["APELLIDO_PA"].ToString(), ApellidoMaterno = Reader["APELLIDO_MA"].ToString(), EsJefeProyecto = Convert.ToInt16(Reader["ES_JEFE_PROYECTO"]) != 0 ? true : false });

                    return Lst;
                }
            }
        }

        //public static List<Entities.UsuarioEntity> GetSupervisorMontaje(int IDProyecto)
        //{
        //    using (SqlConnection Con = Conexion.GetConexionMakeUp())
        //    {
        //        using (SqlCommand Cmd = Conexion.GetCommand("getSupervisorMontaje"))
        //        {
        //            Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;

        //            Con.Open();
        //            SqlDataReader Reader = Cmd.ExecuteReader();

        //            List<Entities.UsuarioEntity> Lst = new List<Entities.UsuarioEntity>();

        //            while (Reader.Read())
        //                Lst.Add(new Entities.UsuarioEntity { ID = Convert.ToInt16(Reader["ID_USUARIO"]), Nombre = Reader["NOMBRE"].ToString(), ApellidoPaterno = Reader["APELLIDO_PA"].ToString(), ApellidoMaterno = Reader["APELLIDO_MA"].ToString() });

        //            return Lst;
        //        }
        //    }
        //}

        public static void AddJefeProyecto(int IDProyecto, DataTable dtJefesProyecto, int IDUsuarioResponsable, DateTime FechaEvento)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("addJefeProyecto"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@listJefeProyecto", SqlDbType.Structured).Value = dtJefesProyecto;
                    Cmd.Parameters.Add("@idUsuarioResponsable", SqlDbType.Int).Value = IDUsuarioResponsable;
                    Cmd.Parameters.Add("@fechaEvento", SqlDbType.DateTime).Value = FechaEvento;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
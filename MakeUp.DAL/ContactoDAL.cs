﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.DAL
{
    public class ContactoDAL
    {
        private static DAO_Conexion Conexion = new DAO_Conexion();

        public static void AddContacto(int IDProyecto, int IDCliente, int IDUsuario, string Nombre, string Email, string Descripcion, string Fono, string Movil, DateTime FechaEvento, bool CompartirContacto, ref int IDNotificacionApp)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("addContacto"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = IDUsuario;
                    Cmd.Parameters.Add("@idCliente", SqlDbType.Int).Value = IDCliente;
                    Cmd.Parameters.Add("@nombre", SqlDbType.VarChar).Value = Nombre;
                    Cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = Email;
                    Cmd.Parameters.Add("@descripcion", SqlDbType.VarChar).Value = Descripcion;
                    Cmd.Parameters.Add("@fono", SqlDbType.VarChar).Value = Fono;
                    Cmd.Parameters.Add("@movil", SqlDbType.VarChar).Value = Movil;
                    Cmd.Parameters.Add("@fechaEvento", SqlDbType.DateTime).Value = FechaEvento;
                    Cmd.Parameters.Add("@compartirContacto", SqlDbType.Bit).Value = CompartirContacto;

                    SqlParameter p_IDNotificacionApp = new SqlParameter("@idNotificacionApp", SqlDbType.Int);
                    p_IDNotificacionApp.Direction = ParameterDirection.InputOutput;
                    p_IDNotificacionApp.Value = 0;
                    Cmd.Parameters.Add(p_IDNotificacionApp);

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                    
                    IDNotificacionApp = Convert.ToInt32(p_IDNotificacionApp.Value);
                }
            }
        }

        public static void SetVincularContactoProyecto(int IDProyecto, int IDContacto, int IDUsuario, byte Vincula, DateTime Fecha)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("setVincularContactoProyecto"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@idContacto", SqlDbType.Int).Value = IDContacto;
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = IDUsuario;
                    Cmd.Parameters.Add("@vincula", SqlDbType.Bit).Value = Vincula;
                    Cmd.Parameters.Add("@fecha", SqlDbType.DateTime).Value = Fecha;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        //public static List<Entities.ClienteEntity.ContactoEntity> GetContactoProyecto(int IDProyecto, int IDCliente, int IDRol)
        //{
        //    using (SqlConnection Con = Conexion.GetConexionMakeUp())
        //    {
        //        using (SqlCommand Cmd = Conexion.GetCommand("getContactoProyecto"))
        //        {
        //            Cmd.Parameters.Add("@idCliente", SqlDbType.Int).Value = IDCliente;
        //            Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;

        //            Con.Open();
        //            SqlDataReader Reader = Cmd.ExecuteReader();

        //            List<Entities.ClienteEntity.ContactoEntity> Lst = new List<Entities.ClienteEntity.ContactoEntity>();

        //            while (Reader.Read())
        //            {
        //                Entities.ClienteEntity.ContactoEntity Contacto = new Entities.ClienteEntity.ContactoEntity(Convert.ToInt16(Reader["ID_CONTACTO"].ToString()), Reader["NOMBRE_COMPLETO"].ToString(), Reader["EMAIL"].ToString(), Reader["DESCRIPCION"].ToString(), Reader["TELEFONO"].ToString(), Reader["MOVIL"].ToString(), Convert.ToBoolean(Reader["CONTACTO_COMPARTIDO"].ToString()), Reader["NOMBRE_RESPONSABLE"].ToString(), Reader["APELLIDO_PATERNO_RESPONSABLE"].ToString(), Reader["VINCULADO_EN_PROYECTO"].ToString() == "1" ? true : false);

        //                //Valida que si el rol del Usuario no es Administrador o Comercial, solo muestre los contactos compartidos
        //                if (IDRol == 1 || IDRol == 6 || IDRol == 0)
        //                {
        //                    Lst.Add(Contacto);

        //                }
        //                else
        //                {
        //                    Contacto.ContactoCompartido = true;
        //                    Lst.Add(Contacto);
        //                    //Lst.Add(new Entities.ClienteEntity.ContactoEntity(Convert.ToInt16(Reader["ID_CONTACTO"].ToString()), Reader["NOMBRE_COMPLETO"].ToString(), Reader["EMAIL"].ToString(), Reader["DESCRIPCION"].ToString(), Reader["TELEFONO"].ToString(), Reader["MOVIL"].ToString(), Convert.ToBoolean(Reader["CONTACTO_COMPARTIDO"].ToString()), Reader["NOMBRE_RESPONSABLE"].ToString(), Reader["APELLIDO_PATERNO_RESPONSABLE"].ToString(), Reader["VINCULADO_EN_PROYECTO"].ToString() == "1" ? true : false)); 
        //                }
        //            }
        //            return Lst;
        //        }
        //    }
        //}

        public static List<Entities.ClienteEntity.ContactoEntity> GetContactoProyecto(int IDProyecto, int IDCliente)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getContactoProyecto"))
                {
                    Cmd.Parameters.Add("@idCliente", SqlDbType.Int).Value = IDCliente;
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    List<Entities.ClienteEntity.ContactoEntity> Lst = new List<Entities.ClienteEntity.ContactoEntity>();

                    while (Reader.Read())
                    {
                        Entities.ClienteEntity.ContactoEntity Contacto = new Entities.ClienteEntity.ContactoEntity(Convert.ToInt16(Reader["ID_CONTACTO"].ToString()), Reader["NOMBRE_COMPLETO"].ToString(), Reader["EMAIL"].ToString(), Reader["DESCRIPCION"].ToString(), Reader["TELEFONO"].ToString(), Reader["MOVIL"].ToString(), Convert.ToBoolean(Reader["CONTACTO_COMPARTIDO"].ToString()), Reader["NOMBRE_RESPONSABLE"].ToString(), Reader["APELLIDO_PATERNO_RESPONSABLE"].ToString(), Reader["VINCULADO_EN_PROYECTO"].ToString() == "1" ? true : false);
                        Lst.Add(Contacto);
                    }
                    return Lst;
                }
            }
        }

        public static void SetContacto(int IDContacto, string NombreCompleto, string Email, string Descripcion, string Telefono, string Movil, bool ContactoCompartido)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("setContactoProyecto"))
                {
                    Cmd.Parameters.Add("@idContacto", SqlDbType.Int).Value = IDContacto;
                    Cmd.Parameters.Add("@nombreCompleto", SqlDbType.VarChar, 100).Value = NombreCompleto;
                    Cmd.Parameters.Add("@email", SqlDbType.VarChar, 100).Value = Email;
                    Cmd.Parameters.Add("@descripcion", SqlDbType.VarChar, 200).Value = Descripcion;
                    Cmd.Parameters.Add("@telefono", SqlDbType.VarChar, 20).Value = Telefono;
                    Cmd.Parameters.Add("@movil", SqlDbType.VarChar, 20).Value = Movil;
                    Cmd.Parameters.Add("@contactoCompartido", SqlDbType.Bit).Value = ContactoCompartido;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static void DelContacto(int IDContacto)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("delContacto"))
                {
                    Cmd.Parameters.Add("@idContacto", SqlDbType.Int).Value = IDContacto;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static int GetRol(int IDUsuario)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("dbo.Rol"))
                {
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = IDUsuario;
                    Cmd.Parameters.Add("@rol", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    int rol = Convert.ToInt16(Cmd.Parameters["@rol"].Value);

                    return rol;
                }
            }
        }
    }
}

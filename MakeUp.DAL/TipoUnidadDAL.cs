﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.DAL
{
    public class TipoUnidadDAL
    {
        private static DAO_Conexion Conexion = new DAO_Conexion();

        public static int AddTipoUnidad(string Nombre, string Sigla)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("addTipoUnidad"))
                {
                    int ID = 0;

                    Cmd.Parameters.Add("@nombre", SqlDbType.VarChar).Value = Nombre;
                    Cmd.Parameters.Add("@sigla", SqlDbType.VarChar).Value = Sigla;
                    Cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int)).Direction = ParameterDirection.ReturnValue; //variable recibe ID y además establecida como variable de retorno

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    ID = Convert.ToInt16(Cmd.Parameters["@ID"].Value);

                    return ID;
                }
            }
        }

        public static List<Entities.TipoUnidadEntity> GetTipoUnidad(int ID, string Nombre, string Sigla)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getTipoUnidad"))
                {
                    Cmd.Parameters.Add("@idTipoUnidad", SqlDbType.Int).Value = ID;
                    Cmd.Parameters.Add("@nombre", SqlDbType.VarChar).Value = Nombre;
                    Cmd.Parameters.Add("@sigla", SqlDbType.VarChar).Value = Sigla;

                    Con.Open();
                    List<Entities.TipoUnidadEntity> Lst = new List<Entities.TipoUnidadEntity>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                    {
                        Entities.TipoUnidadEntity TipoUnidad = new Entities.TipoUnidadEntity();

                        TipoUnidad.ID = Convert.ToInt16(Reader["id_unidad"].ToString());
                        TipoUnidad.Nombre = Reader["nombre"].ToString();
                        TipoUnidad.Sigla = Reader["sigla"].ToString();

                        Lst.Add(TipoUnidad);
                    }

                    return Lst;
                }
            }
        }

        public static void SetTipoUnidad(int ID, string Nombre, string Sigla)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("setTipoUnidad"))
                {
                    Cmd.Parameters.Add("@idTipoUnidad", SqlDbType.Int).Value = ID;
                    Cmd.Parameters.Add("@nombre", SqlDbType.VarChar).Value = Nombre;
                    Cmd.Parameters.Add("@sigla", SqlDbType.VarChar).Value = Sigla;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static void DelTipoUnidad(int ID)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("delTipoUnidad"))
                {
                    Cmd.Parameters.Add("@idTipoUnidad", SqlDbType.Int).Value = ID;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }
    }
}

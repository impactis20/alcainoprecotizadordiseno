﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.DAL
{
    public class NotaPedidoDAL
    {
        private static DAO_Conexion Conexion = new DAO_Conexion();

        public static int AddNPProyecto(int IDProyecto, int IDUsuario, int IDTipoUnidad, int IDTipoNP, int IDDocOC, decimal ValorUnitario, decimal Cantidad, string Descripcion, DateTime Fecha, string DecisionEP, ref int IDNotificacionApp)
        //public static int AddNPProyecto(int IDProyecto, int IDUsuario, int IDTipoUnidad, int IDDocOC, decimal ValorUnitario, decimal Cantidad, string Descripcion, DateTime Fecha, string DecisionEP, ref int IDNotificacionApp)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("addNPProyecto"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = IDUsuario;
                    Cmd.Parameters.Add("@idTipoUnidad", SqlDbType.Int).Value = IDTipoUnidad;
                    Cmd.Parameters.Add("@idTipoNP", SqlDbType.Int).Value = IDTipoNP;
                    Cmd.Parameters.Add("@idDocOC", SqlDbType.Int).Value = IDDocOC;
                    Cmd.Parameters.Add("@valorUnitario", SqlDbType.Decimal).Value = ValorUnitario;
                    Cmd.Parameters.Add("@cantidad", SqlDbType.Decimal).Value = Cantidad;
                    Cmd.Parameters.Add("@descripcion", SqlDbType.VarChar).Value = Descripcion;
                    Cmd.Parameters.Add("@decisionEP", SqlDbType.VarChar).Value = DecisionEP;
                    Cmd.Parameters.Add("@fecha", SqlDbType.DateTime).Value = Fecha;
                    Cmd.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    SqlParameter p_IDNotificacionApp = new SqlParameter("@idNotificacionApp", SqlDbType.Int);
                    p_IDNotificacionApp.Direction = ParameterDirection.InputOutput;
                    p_IDNotificacionApp.Value = 0;
                    Cmd.Parameters.Add(p_IDNotificacionApp);

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    //obtiene valor IDNotificacion
                    IDNotificacionApp = Convert.ToInt32(p_IDNotificacionApp.Value);

                    return Convert.ToInt16(Cmd.Parameters["@ID"].Value);
                }
            }
        }

        public static List<Entities.NotaPedidoEntity> GetNPCreada(int IDProyecto)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getNPCreada"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();
                    List<Entities.NotaPedidoEntity> Lst = new List<Entities.NotaPedidoEntity>();

                    while (Reader.Read())
                    {
                        Entities.NotaPedidoEntity NP = new Entities.NotaPedidoEntity(Convert.ToInt32(Reader["ID_NP"].ToString()), IDProyecto.ToString(), Reader["NRO_NP"].ToString(), Convert.ToInt32(Reader["ID_TIPONP"].ToString()), Reader["NOMBRE"].ToString());
                        //Entities.NotaPedidoEntity NP = new Entities.NotaPedidoEntity(Convert.ToInt32(Reader["ID_NP"].ToString()), IDProyecto.ToString(), Reader["NRO_NP"].ToString(), Convert.ToInt32(Reader["ID_TIPONP"].ToString()));
                        NP.Creacion = new Entities.CreacionEntity(Convert.ToInt16(Reader["ID_UNIDAD"].ToString()), Convert.ToInt16(Reader["ID_DOCUMENTOCOMERCIAL"].ToString()), Convert.ToDecimal(Reader["VALOR_UNITARIO"].ToString()), Convert.ToDecimal(Reader["CANTIDAD"].ToString()), Reader["DESCRIPCION"].ToString(), Convert.ToInt16(Reader["ID_TIPONP"].ToString()));

                        Lst.Add(NP);
                    }

                    return Lst;
                }
            }
        }


        public static List<Entities.NotaPedidoEntity> GetNPRegularizar(int IDProyecto, int IDNP)
        //public static int AddNPProyecto(int IDProyecto, int IDUsuario, int IDTipoUnidad, int IDDocOC, decimal ValorUnitario, decimal Cantidad, string Descripcion, DateTime Fecha, string DecisionEP, ref int IDNotificacionApp)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getRegularizarNotaPedido"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@idNP", SqlDbType.Int).Value = IDNP;
                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    List<Entities.NotaPedidoEntity> Lst = new List<Entities.NotaPedidoEntity>();

                    while (Reader.Read())
                    {
                        Entities.NotaPedidoEntity NPReg = new Entities.NotaPedidoEntity(Convert.ToInt16(Reader["ID_PROYECTO"].ToString()), Convert.ToDecimal(Reader["CANTIDAD_CREACION"].ToString()), Convert.ToDecimal(Reader["VALOR_UNITARIO_CREACION"].ToString()));
                        Lst.Add(NPReg);
                    }

                    return Lst;
                }
            }
        }

        public static void DelNPCreada(int IDNP, string DecisionEP)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("delNPCreada"))
                {
                    Cmd.Parameters.Add("@idNP", SqlDbType.Int).Value = IDNP;
                    Cmd.Parameters.Add("@decisionEP", SqlDbType.VarChar, 10).Value = DecisionEP;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static void DelNP(int IDNP, int IDUsuario, string DecisionEP, DateTime Fecha, ref int IDNotificacionApp)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("delNP"))
                {
                    Cmd.Parameters.Add("@idNP", SqlDbType.Int).Value = IDNP;
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = IDUsuario;
                    Cmd.Parameters.Add("@decisionEP", SqlDbType.VarChar, 10).Value = DecisionEP;
                    Cmd.Parameters.Add("@fecha", SqlDbType.DateTime).Value = Fecha;
                    SqlParameter p_IDNotificacionApp = new SqlParameter("@idNotificacionApp", SqlDbType.Int);
                    p_IDNotificacionApp.Direction = ParameterDirection.InputOutput;
                    p_IDNotificacionApp.Value = 0;
                    Cmd.Parameters.Add(p_IDNotificacionApp);

                    //obtiene valor IDNotificacion
                    IDNotificacionApp = Convert.ToInt32(p_IDNotificacionApp.Value);

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static void SetNPCreada(int IDProyecto, int IDNP, int IDUsuario, int IDTipoUnidad, int IDDocOC, decimal ValorUnitario, decimal Cantidad, string Descripcion, DateTime Fecha, string DecisionEP, int IDTipoNP)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("setNPCreada"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    Cmd.Parameters.Add("@idNP", SqlDbType.Int).Value = IDNP;
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = IDUsuario;
                    Cmd.Parameters.Add("@idTipoUnidad", SqlDbType.Int).Value = IDTipoUnidad;
                    Cmd.Parameters.Add("@idDocOC", SqlDbType.Int).Value = IDDocOC;
                    Cmd.Parameters.Add("@valorUnitario", SqlDbType.Decimal).Value = ValorUnitario;
                    Cmd.Parameters.Add("@cantidad", SqlDbType.Decimal).Value = Cantidad;
                    Cmd.Parameters.Add("@descripcion", SqlDbType.VarChar).Value = Descripcion;
                    Cmd.Parameters.Add("@fecha", SqlDbType.DateTime).Value = Fecha;
                    Cmd.Parameters.Add("@decisionEP", SqlDbType.VarChar, 10).Value = DecisionEP;
                    Cmd.Parameters.Add("@idTipoNP", SqlDbType.Int).Value = IDTipoNP;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static void SetNP(int IDNP, int IDUsuario, int IDTipoUnidad,int IDTipoNP, int IDDocOC, decimal ValorUnitario, decimal Cantidad, string Descripcion, string CampoModifica, DateTime Fecha, string DecisionEP, ref int IDNotificacionApp)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("setNP"))
                {
                    Cmd.Parameters.Add("@idNP", SqlDbType.Int).Value = IDNP;
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = IDUsuario;
                    Cmd.Parameters.Add("@idTipoUnidad", SqlDbType.Int).Value = IDTipoUnidad;
                    Cmd.Parameters.Add("@idTipoNP", SqlDbType.Int).Value = IDTipoNP;
                    Cmd.Parameters.Add("@idDocOC", SqlDbType.Int).Value = IDDocOC;
                    Cmd.Parameters.Add("@valorUnitario", SqlDbType.Decimal).Value = ValorUnitario;
                    Cmd.Parameters.Add("@cantidad", SqlDbType.Decimal).Value = Cantidad;
                    Cmd.Parameters.Add("@descripcion", SqlDbType.VarChar).Value = Descripcion;
                    Cmd.Parameters.Add("@campoModifica", SqlDbType.VarChar).Value = CampoModifica;
                    Cmd.Parameters.Add("@fecha", SqlDbType.DateTime).Value = Fecha;
                    Cmd.Parameters.Add("@decisionEP", SqlDbType.VarChar).Value = DecisionEP;
                    SqlParameter p_IDNotificacionApp = new SqlParameter("@idNotificacionApp", SqlDbType.Int);
                    p_IDNotificacionApp.Direction = ParameterDirection.InputOutput;
                    p_IDNotificacionApp.Value = 0;
                    Cmd.Parameters.Add(p_IDNotificacionApp);

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    //obtiene valor IDNotificacion
                    IDNotificacionApp = Convert.ToInt32(p_IDNotificacionApp.Value);
                }
            }
        }

        public static List<Entities.EstadoNPEntity> GetEstadoNP()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getEstadoNP"))
                {
                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    List<Entities.EstadoNPEntity> Lst = new List<Entities.EstadoNPEntity>();

                    while (Reader.Read())
                        Lst.Add(new Entities.EstadoNPEntity(Convert.ToInt16(Reader["ID_ESTADONP"].ToString()), Reader["DESCRIPCION"].ToString()));

                    return Lst;
                }
            }
        }

        public static List<Entities.NotaPedidoEntity> GetNP(int IDProyecto)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getNPProyecto"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    List<Entities.NotaPedidoEntity> Lst = new List<Entities.NotaPedidoEntity>();

                    while (Reader.Read())
                    {
                        Entities.NotaPedidoEntity NP = new Entities.NotaPedidoEntity(Convert.ToInt16(Reader["ID_NP"].ToString()), IDProyecto.ToString(), Reader["NRO_NP"].ToString(), Convert.ToInt16(Reader["ID_ESTADONP"].ToString()), Convert.ToBoolean(Reader["ESTADO_NP"].ToString()), Convert.ToInt16(Reader["TIPO_NP"].ToString()), Reader["NOMBRE_TIPONP"].ToString());
                        //Entities.NotaPedidoEntity NP = new Entities.NotaPedidoEntity(Convert.ToInt32(Reader["ID_NP"].ToString()), IDProyecto.ToString(), Reader["NRO_NP"].ToString(), Convert.ToInt32(Reader["ID_TIPONP"].ToString()), Reader["NOMBRE"].ToString());

                        if (NP.IDEstadoNP >= 1) //1 = 'Por ajustar'
                            NP.Creacion = new Entities.CreacionEntity(Convert.ToDecimal(Reader["CANTIDAD_CREACION"].ToString()), Reader["DESCRIPCION_CREACION"].ToString(), Convert.ToDecimal(Reader["VALOR_UNITARIO_CREACION"].ToString()), Convert.ToInt16(Reader["ID_TIPO_UNIDAD"].ToString()), Reader["NOMBRE_TIPO_UNIDAD"].ToString(), Reader["SIGLA_TIPO_UNIDAD"].ToString(), Convert.ToDateTime(Reader["FECHA_CREACION"].ToString()), Convert.ToDateTime(Reader["FECHA_ULTIMA_MODIFICACION_CREACION"].ToString()), Convert.ToInt16(Reader["ID_DOCUMENTOCOMERCIAL"].ToString()), Reader["DESCRIPCION_OC"].ToString(), Reader["NOMBRE_USUARIO_CREACION"].ToString(), Reader["A_PATERNO_USUARIO_CREACION"].ToString(), Convert.ToInt16(Reader["TIPO_NP"].ToString()));

                        if(NP.IDEstadoNP >= 2) //2 = 'Por valorizar'
                            NP.Ajuste = new Entities.AjusteEntity(Convert.ToDecimal(Reader["CANTIDAD_AJUSTE"].ToString()), Reader["DESCRIPCION_AJUSTE"].ToString(), Convert.ToDateTime(Reader["FECHA_REAL_ENTREGA_AJUSTE"].ToString()), Convert.ToDateTime(Reader["FECHA_AJUSTE"].ToString()), Reader["NOMBRE_USUARIO_AJUSTE"].ToString(), Reader["A_PATERNO_USUARIO_AJUSTE"].ToString());

                        if (NP.IDEstadoNP >= 3) // 3 o 4 traen los mismos datos, la forma de saber si la NP está facturada es que NP.IDEstadoNP = 4 o por pagar si NP.IDEstadoNP = 3
                            NP.Valorizacion = new Entities.ValorizacionEntity(Convert.ToDecimal(Reader["CANTIDAD_VALORIZACION"].ToString()), Reader["DESCRIPCION_VALORIZACION"].ToString(), Convert.ToDecimal(Reader["VALOR_UNITARIO_VALORIZACION"].ToString()), Convert.ToDateTime(Reader["FECHA_VALORIZACION"].ToString()), Reader["NOMBRE_USUARIO_VALORIZACION"].ToString(), Reader["A_PATERNO_USUARIO_VALORIZACION"].ToString());
                        
                        Lst.Add(NP);
                    }

                    return Lst;
                }
            }
        }

        public static void AddAjuste(int IDNP, int IDUsuario, decimal Cantidad, string Descripcion, DateTime FechaRealEntrega, DateTime FechaOperacion, string DecisionEP, ref int IDNotificacionApp)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("addAjuste"))
                {
                    Cmd.Parameters.Add("@idNP", SqlDbType.Int).Value = IDNP;
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = IDUsuario;
                    Cmd.Parameters.Add("@cantidad", SqlDbType.Decimal).Value = Cantidad;
                    Cmd.Parameters.Add("@desc", SqlDbType.VarChar).Value = Descripcion;
                    Cmd.Parameters.Add("@fechaRealEntrega", SqlDbType.Date).Value = FechaRealEntrega;
                    Cmd.Parameters.Add("@fecha", SqlDbType.DateTime).Value = FechaOperacion;
                    Cmd.Parameters.Add("@decisionEP", SqlDbType.VarChar).Value = DecisionEP;
                    SqlParameter p_IDNotificacionApp = new SqlParameter("@idNotificacionApp", SqlDbType.Int);
                    p_IDNotificacionApp.Direction = ParameterDirection.InputOutput;
                    p_IDNotificacionApp.Value = 0;
                    Cmd.Parameters.Add(p_IDNotificacionApp);

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    //obtiene valor IDNotificacion
                    IDNotificacionApp = Convert.ToInt32(p_IDNotificacionApp.Value);
                }
            }
        }

        public static void AddValorizacion(int IDNP, int IDUsuario, decimal Cantidad, decimal ValorUnitario, string Descripcion, DateTime FechaOperacion, string DecisionEP, ref int IDNotificacionApp)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("addValorizacion"))
                {
                    Cmd.Parameters.Add("@idNP", SqlDbType.Int).Value = IDNP;
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = IDUsuario;
                    Cmd.Parameters.Add("@valorUnitario", SqlDbType.Decimal).Value = ValorUnitario;
                    Cmd.Parameters.Add("@cantidad", SqlDbType.Decimal).Value = Cantidad;
                    Cmd.Parameters.Add("@desc", SqlDbType.VarChar).Value = Descripcion;
                    Cmd.Parameters.Add("@fecha", SqlDbType.DateTime).Value = FechaOperacion;
                    Cmd.Parameters.Add("@decisionEP", SqlDbType.VarChar).Value = DecisionEP;
                    SqlParameter p_IDNotificacionApp = new SqlParameter("@idNotificacionApp", SqlDbType.Int);
                    p_IDNotificacionApp.Direction = ParameterDirection.InputOutput;
                    p_IDNotificacionApp.Value = 0;
                    Cmd.Parameters.Add(p_IDNotificacionApp);

                    Con.Open();
                    Cmd.ExecuteNonQuery();

                    //obtiene valor IDNotificacion
                    IDNotificacionApp = Convert.ToInt32(p_IDNotificacionApp.Value);
                }
            }
        }

        public static List<Entities.NotaPedidoEntity> GetResumenNPProyecto(int IDProyecto)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getResumenNPProyecto"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    List<Entities.NotaPedidoEntity> Lst = new List<Entities.NotaPedidoEntity>();

                    while (Reader.Read())
                    {
                        Entities.NotaPedidoEntity NP = new Entities.NotaPedidoEntity(Convert.ToInt16(Reader["ID_NP"].ToString()), IDProyecto.ToString(), Reader["NRO_NP"].ToString(), Convert.ToInt16(Reader["ID_ESTADONP"].ToString()), Convert.ToBoolean(Reader["ESTADO_NP"].ToString()), Reader["NOMBRE"].ToString());

                        if (NP.IDEstadoNP >= 1)
                            NP.Creacion = new Entities.CreacionEntity(Convert.ToDecimal(Reader["CANTIDAD_CREACION"].ToString()), Reader["DESCRIPCION_CREACION"].ToString(), Convert.ToDecimal(Reader["VALOR_UNITARIO_CREACION"].ToString()), Convert.ToInt16(Reader["ID_TIPO_UNIDAD"].ToString()), Reader["NOMBRE_TIPO_UNIDAD"].ToString(), Reader["SIGLA_TIPO_UNIDAD"].ToString(), Convert.ToBoolean(Reader["CANTIDAD_MODIFICADA"]), Convert.ToBoolean(Reader["DESCRIPCION_MODIFICADA"]), Convert.ToBoolean(Reader["VALOR_UNITARIO_MODIFICADO"]));

                        if (NP.IDEstadoNP >= 2)
                            NP.Ajuste = new Entities.AjusteEntity(Convert.ToDecimal(Reader["CANTIDAD_AJUSTE"].ToString()), Reader["DESCRIPCION_AJUSTE"].ToString());

                        if (NP.IDEstadoNP >= 3)
                            NP.Valorizacion = new Entities.ValorizacionEntity(Convert.ToDecimal(Reader["CANTIDAD_VALORIZACION"].ToString()), Reader["DESCRIPCION_VALORIZACION"].ToString(), Convert.ToDecimal(Reader["VALOR_UNITARIO_VALORIZACION"].ToString()));

                        Lst.Add(NP);
                    }

                    return Lst;
                }
            }
        }
        public static List<Entities.TipoNPEntity> GetTipoNP()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getTipoNP"))
                {
                    Con.Open();
                    List<Entities.TipoNPEntity> Lst = new List<Entities.TipoNPEntity>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                    {
                        Entities.TipoNPEntity TipoNP = new Entities.TipoNPEntity();

                        TipoNP.ID = Convert.ToInt16(Reader["ID_TIPONP"].ToString());
                        TipoNP.Nombre = Reader["NOMBRE"].ToString();

                        Lst.Add(TipoNP);
                    }

                    return Lst;
                }
            }
        }
        public static List<Entities.NotaPedidoEntity> GetTrazabilidadNP(int IDNP)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getTrazabalidadNP"))
                {
                    Cmd.Parameters.Add("@idNP", SqlDbType.Int).Value = IDNP;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    List<Entities.NotaPedidoEntity> Lst = new List<Entities.NotaPedidoEntity>();

                    while (Reader.Read())
                    {
                        Entities.NotaPedidoEntity NP = new Entities.NotaPedidoEntity(IDNP, Convert.ToInt16(Reader["ID_ESTADONP"].ToString()));

                        switch(NP.IDEstadoNP)
                        {
                            case 1:
                                NP.Creacion = new Entities.CreacionEntity(Reader["SIGLA_UNIDAD"].ToString(), Convert.ToDecimal(Reader["CANTIDAD"].ToString()), Convert.ToDecimal(Reader["VALOR_UNITARIO"].ToString()), Reader["DESCRIPCION"].ToString(), Convert.ToDateTime(Reader["FECHA"].ToString()), Reader["NOMBRE_USUARIO"].ToString(), Reader["APELLIDO_PA_USUARIO"].ToString());
                                break;
                            case 2:
                                NP.Creacion = new Entities.CreacionEntity(Reader["SIGLA_UNIDAD"].ToString(), Convert.ToDecimal(Reader["VALOR_UNITARIO"].ToString()));
                                NP.Ajuste = new Entities.AjusteEntity(Convert.ToDecimal(Reader["CANTIDAD"].ToString()), Reader["DESCRIPCION"].ToString(), Convert.ToDateTime(Reader["FECHA"].ToString()), Reader["NOMBRE_USUARIO"].ToString(), Reader["APELLIDO_PA_USUARIO"].ToString());
                                break;
                            default:
                                NP.Creacion = new Entities.CreacionEntity(Reader["SIGLA_UNIDAD"].ToString());
                                NP.Valorizacion = new Entities.ValorizacionEntity(Convert.ToDecimal(Reader["CANTIDAD"].ToString()), Reader["DESCRIPCION"].ToString(), Convert.ToDecimal(Reader["VALOR_UNITARIO"].ToString()), Convert.ToDateTime(Reader["FECHA"].ToString()), Reader["NOMBRE_USUARIO"].ToString(), Reader["APELLIDO_PA_USUARIO"].ToString());
                                break;
                        }

                        Lst.Add(NP);
                    }

                    return Lst;
                }
            }
        }


    }
}
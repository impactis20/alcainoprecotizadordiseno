﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace MakeUp.DAL
{
   public class IntegracionDAL
    {
        private static DAO_Conexion Conexion = new DAO_Conexion();
        //Metodos para integración tabla LOG_z_ma_addCentroCosto     
        public static Entities.IntegracionEntity getDetalleIntegracion1(int Id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion1Detalle"))
                {
                    Cmd.Parameters.Add("@id_log", SqlDbType.Int).Value = Id_log1;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    Entities.IntegracionEntity inte = new Entities.IntegracionEntity();
                    while (Reader.Read())
                        inte = new Entities.IntegracionEntity(Convert.ToInt32(Reader["id_log"]), Convert.ToString(Reader["desc_sp_cont"]), Convert.ToString(Reader["desc_sp_call"]), Convert.ToString(Reader["desc_call"]), Convert.ToInt32(Reader["estado"]), Convert.ToString(Reader["excepcion"]), Convert.ToDateTime(Reader["fecha_Hora"]), Convert.ToString(Reader["p_codigocc"]), Convert.ToString(Reader["p_nombreproyecto"]), Convert.ToString(Reader["p_clase1"]), Convert.ToInt32(Reader["p_nivel2"]), Convert.ToInt32(Reader["p_nreguistCC"]), Convert.ToDecimal(Reader["p_yy"]));
                        return inte;
                }
            }
        }

        public static void setIntegracion1(string p1, string p2, string p3, int p4, int p5, decimal p6, int id_log2)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("z_ma_addCentroCosto"))
                {

                    Cmd.Parameters.Add("@codigo", SqlDbType.VarChar,11).Value = p1;
                    Cmd.Parameters.Add("@nombre", SqlDbType.VarChar,45).Value = p2;
                    Cmd.Parameters.Add("@clase1", SqlDbType.VarChar,40).Value = p3;
                    Cmd.Parameters.Add("@nivel2", SqlDbType.Int).Value = Convert.ToInt32(p4);
                    Cmd.Parameters.Add("@nreguist", SqlDbType.Int).Value = Convert.ToInt32(p5);
                    Cmd.Parameters.Add("@yy", SqlDbType.Decimal).Value = p6;
                    Cmd.Parameters.Add("@id_log1", SqlDbType.Int).Value = id_log2;
                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }

        }


        public static List<Entities.IntegracionEntity> getInteg1()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion1"))
                {
                    Con.Open();
                    List<Entities.IntegracionEntity> Lst = new List<Entities.IntegracionEntity>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                    {
                        Entities.IntegracionEntity inte1 = new Entities.IntegracionEntity();
                        inte1.Id_log1 = Convert.ToInt32(Reader["id_log"]);
                        inte1.Codigo = Convert.ToString(Reader["p_codigocc"]);
                        inte1.desc_sp_cont = Convert.ToString(Reader["desc_sp_cont"]);
                        inte1.desc_sp_call = Convert.ToString(Reader["desc_sp_call"]);
                        inte1.estado = Convert.ToInt32(Reader["estado"]);
                        inte1.excepcion = Convert.ToString(Reader["excepcion"]);
                        inte1.fecha_Hora = Convert.ToDateTime(Reader["fecha_Hora"]);
                        Lst.Add(inte1);
                    }

                    return Lst;
                }
            }
        }

        //Metodos para integración tabla Log_z_ma_addArticulo
        public static Entities.IntegracionEntity getDetalleIntegracion2(int Id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion2Detalle"))
                {
                    Cmd.Parameters.Add("@id_log", SqlDbType.Int).Value = Id_log1;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    Entities.IntegracionEntity inte = new Entities.IntegracionEntity();
                    while (Reader.Read())
                        inte = new Entities.IntegracionEntity(Convert.ToInt32(Reader["id_log"]), Convert.ToString(Reader["desc_sp_cont"]), Convert.ToString(Reader["desc_sp_call"]), Convert.ToString(Reader["desc_call"]), Convert.ToInt32(Reader["estado"]), Convert.ToString(Reader["excepcion"]), Convert.ToDateTime(Reader["fecha_Hora"]), Convert.ToString(Reader["p_nombreproyecto"]), Convert.ToString(Reader["p_idarticulomanager"]), Convert.ToDateTime(Reader["p_fechacreacion"]), Convert.ToString(Reader["p_codigocc"]));                 
                    return inte;
                }
            }
        }

        public static void setIntegracion2(string p1, string p2, DateTime p3, string p4,int id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {

                using (SqlCommand Cmd = Conexion.GetCommand("z_ma_addArticulo"))
                {
                    Cmd.Parameters.Add("@id_log3", SqlDbType.Int).Value = Convert.ToInt32(id_log1);
                    Cmd.Parameters.Add("@nombreProyecto", SqlDbType.VarChar).Value = p1;
                    Cmd.Parameters.Add("@codigoCC", SqlDbType.VarChar).Value = p2;
                    Cmd.Parameters.Add("@fechaCreacion", SqlDbType.DateTime).Value = p3;
                    Cmd.Parameters.Add("@idArticuloManager", SqlDbType.Int).Value = Convert.ToInt32(p4);

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static List<Entities.IntegracionEntity> getInteg2()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion2"))
                {
                    Con.Open();
                    List<Entities.IntegracionEntity> Lst = new List<Entities.IntegracionEntity>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                    {
                        Entities.IntegracionEntity inte1 = new Entities.IntegracionEntity();
                        inte1.Id_log1 = Convert.ToInt32(Reader["id_log"]);
                        inte1.Codigo = Convert.ToString(Reader["p_codigocc"]);
                        inte1.desc_sp_cont = Convert.ToString(Reader["desc_sp_cont"]);
                        inte1.desc_sp_call = Convert.ToString(Reader["desc_sp_call"]);
                        inte1.estado = Convert.ToInt32(Reader["estado"]);
                        inte1.excepcion = Convert.ToString(Reader["excepcion"]);
                        inte1.fecha_Hora = Convert.ToDateTime(Reader["fecha_Hora"]);
                        Lst.Add(inte1);
                    }

                    return Lst;
                }
            }
        }

        //Metodos para integración tabla LOG_z_ma_addArticuloMontaje

        public static Entities.IntegracionEntity getDetalleIntegracion3(int Id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion3Detalle"))
                {
                    Cmd.Parameters.Add("@id_log", SqlDbType.Int).Value = Id_log1;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    Entities.IntegracionEntity inte = new Entities.IntegracionEntity();
                    while (Reader.Read())
                        inte = new Entities.IntegracionEntity(Convert.ToInt32(Reader["id_log"]), Convert.ToString(Reader["desc_sp_cont"]), Convert.ToString(Reader["desc_sp_call"]), Convert.ToString(Reader["desc_call"]), Convert.ToInt32(Reader["estado"]), Convert.ToString(Reader["excepcion"]), Convert.ToDateTime(Reader["fecha_Hora"]), Convert.ToString(Reader["p_nombreproyecto"]), Convert.ToString(Reader["p_idarticulomanagermon"]),Convert.ToDateTime(Reader["p_fechacreacion"]),Convert.ToString(Reader["p_codigocc"]));
                        return inte;
                }
            }
        }

        public static void setIntegracion3(string p1, string p2,DateTime p3,string p4, int id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {

                using (SqlCommand Cmd = Conexion.GetCommand("z_ma_addArticuloMontaje"))
                {
                   
                    Cmd.Parameters.Add("@id_log5", SqlDbType.Int).Value = Convert.ToInt32(id_log1);
                    Cmd.Parameters.Add("@nombreProyectoM", SqlDbType.VarChar).Value = p1;
                    Cmd.Parameters.Add("@codigoCCM", SqlDbType.VarChar).Value = p2;
                    Cmd.Parameters.Add("@fechaCreacionM", SqlDbType.DateTime).Value = Convert.ToDateTime(p3);
                    Cmd.Parameters.Add("@idArticuloManagerMon", SqlDbType.Int).Value = Convert.ToInt32(p4);

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static List<Entities.IntegracionEntity> getInteg3()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion3"))
                {
                    Con.Open();
                    List<Entities.IntegracionEntity> Lst = new List<Entities.IntegracionEntity>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                    {
                        Entities.IntegracionEntity inte1 = new Entities.IntegracionEntity();
                        inte1.Id_log1 = Convert.ToInt32(Reader["id_log"]);
                        inte1.Codigo = Convert.ToString(Reader["p_codigocc"]);
                        inte1.desc_sp_cont = Convert.ToString(Reader["desc_sp_cont"]);
                        inte1.desc_sp_call = Convert.ToString(Reader["desc_sp_call"]);
                        inte1.estado = Convert.ToInt32(Reader["estado"]);
                        inte1.excepcion = Convert.ToString(Reader["excepcion"]);
                        inte1.fecha_Hora = Convert.ToDateTime(Reader["fecha_Hora"]);
                        Lst.Add(inte1);
                    }

                    return Lst;
                }
            }
        }

        //Metodos para integración tabla LOG_z_ma_addCentroCostoGarantia
        public static Entities.IntegracionEntity getDetalleIntegracion4(int Id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion4Detalle"))
                {
                    Cmd.Parameters.Add("@id_log", SqlDbType.Int).Value = Id_log1;

                    Con.Open();

                    SqlDataReader Reader = Cmd.ExecuteReader();

                    Entities.IntegracionEntity inte = new Entities.IntegracionEntity();

                    while (Reader.Read())
                        inte = new Entities.IntegracionEntity(Convert.ToInt32(Reader["id_log"]), Convert.ToString(Reader["desc_sp_cont"]), Convert.ToString(Reader["desc_sp_call"]), Convert.ToString(Reader["desc_call"]), Convert.ToInt32(Reader["estado"]), Convert.ToString(Reader["excepcion"]), Convert.ToDateTime(Reader["fecha_Hora"]), Convert.ToInt32(Reader["p_idproyecto"]));

                    return inte;
                }
            }
        }

        public static void setIntegracion4(int p1, int id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {

                using (SqlCommand Cmd = Conexion.GetCommand("z_ma_addCentroCostoGarantia"))
                {
                    Cmd.Parameters.Add("@idProyecto", SqlDbType.VarChar).Value = p1;
                    Cmd.Parameters.Add("@id_log2", SqlDbType.Int).Value = Convert.ToInt32(id_log1);

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static List<Entities.IntegracionEntity> getInteg4()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion4"))
                {
                    Con.Open();
                    List<Entities.IntegracionEntity> Lst = new List<Entities.IntegracionEntity>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                    {
                        Entities.IntegracionEntity inte1 = new Entities.IntegracionEntity();
                        inte1.Id_log1 = Convert.ToInt32(Reader["id_log"]);
                        inte1.Codigo = Convert.ToString(Reader["p_idproyecto"]);
                        inte1.desc_sp_cont = Convert.ToString(Reader["desc_sp_cont"]);
                        inte1.desc_sp_call = Convert.ToString(Reader["desc_sp_call"]);
                        inte1.estado = Convert.ToInt32(Reader["estado"]);
                        inte1.excepcion = Convert.ToString(Reader["excepcion"]);
                        inte1.fecha_Hora = Convert.ToDateTime(Reader["fecha_Hora"]);
                        Lst.Add(inte1);
                    }

                    return Lst;
                }
            }
        }

        //Metodos para integración tabla LOG_z_ma_addCliente

        public static Entities.IntegracionEntity getDetalleIntegracion5(int Id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion5Detalle"))
                {
                    Cmd.Parameters.Add("@id_log", SqlDbType.Int).Value = Id_log1;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    Entities.IntegracionEntity inte = new Entities.IntegracionEntity();
                    while (Reader.Read())
                        inte = new Entities.IntegracionEntity(Convert.ToInt32(Reader["id_log"]), Convert.ToString(Reader["desc_sp_cont"]), Convert.ToString(Reader["desc_sp_call"]), Convert.ToString(Reader["desc_call"]), Convert.ToInt32(Reader["estado"]), Convert.ToString(Reader["excepcion"]), Convert.ToDateTime(Reader["fecha_Hora"]), Convert.ToString(Reader["p_nombreFantasia"]), Convert.ToInt32(Reader["p_rut"]), Convert.ToString(Reader["p_digito_v"]), Convert.ToString(Reader["p_razon_social"]), Convert.ToString(Reader["p_giro"]), Convert.ToString(Reader["p_direccion"]), Convert.ToString(Reader["p_telefono"]), Convert.ToString(Reader["p_comuna"]), Convert.ToString(Reader["p_ciudad"]), Convert.ToInt32(Reader["p_nreguist"]));

                    return inte;
                }
            }
        }

        public static void setIntegracion5(string p1,int p2,string p3,string p4,string p5,string p6,string p7,string p8,string p9,int p10,int id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("z_ma_addCliente"))
                {

                    Cmd.Parameters.Add("@nombreFantasia", SqlDbType.VarChar).Value = p1;
                    Cmd.Parameters.Add("@rut", SqlDbType.Int).Value = Convert.ToInt32(p2);
                    Cmd.Parameters.Add("@dv", SqlDbType.Char).Value = Convert.ToChar(p3);
                    Cmd.Parameters.Add("@razonsocial", SqlDbType.VarChar).Value = p4;
                    Cmd.Parameters.Add("@giro", SqlDbType.VarChar).Value = p5;
                    Cmd.Parameters.Add("@direccion", SqlDbType.VarChar).Value = p6;
                    Cmd.Parameters.Add("@telefono", SqlDbType.Int).Value = p7;
                    Cmd.Parameters.Add("@comuna", SqlDbType.VarChar).Value = p8;
                    Cmd.Parameters.Add("@ciudad", SqlDbType.VarChar).Value = p9;
                    Cmd.Parameters.Add("@nreguist", SqlDbType.Int).Value = p10;
                    Cmd.Parameters.Add("@id_log", SqlDbType.Int).Value = id_log1;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static List<Entities.IntegracionEntity> getInteg5()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion5"))
                {
                    Con.Open();
                    List<Entities.IntegracionEntity> Lst = new List<Entities.IntegracionEntity>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                    {
                        Entities.IntegracionEntity inte1 = new Entities.IntegracionEntity();
                        inte1.Id_log1 = Convert.ToInt32(Reader["id_log"]);
                        inte1.desc_sp_cont = Convert.ToString(Reader["desc_sp_cont"]);
                        inte1.desc_sp_call = Convert.ToString(Reader["desc_sp_call"]);
                        inte1.estado = Convert.ToInt32(Reader["estado"]);
                        inte1.excepcion = Convert.ToString(Reader["excepcion"]);
                        inte1.fecha_Hora = Convert.ToDateTime(Reader["fecha_Hora"]);
                        Lst.Add(inte1);
                    }

                    return Lst;
                }
            }
        }

        //Metodos para integración tabla LOG_z_ma_addFactura

        public static Entities.IntegracionEntity GetDetalleIntegracion6(int Id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion6Detalle"))
                {
                    Cmd.Parameters.Add("@id_log", SqlDbType.Int).Value = Id_log1;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    Entities.IntegracionEntity inte1 = new Entities.IntegracionEntity();
                    while (Reader.Read())
                        inte1 = new Entities.IntegracionEntity(Convert.ToString(Reader["desc_sp_cont"]), Convert.ToString(Reader["desc_sp_call"]), Convert.ToString(Reader["desc_call"]), Convert.ToInt32(Reader["estado"]), Convert.ToString(Reader["excepcion"]), Convert.ToDateTime(Reader["fecha_Hora"]), Convert.ToInt32(Reader["p_idproyecto"]), Convert.ToInt32(Reader["p_idcliente"]), Convert.ToDateTime(Reader["p_fechacreacion"]), Convert.ToString(Reader["p_condicionpago"]), Convert.ToDateTime(Reader["p_fechavencimiento"]), Convert.ToString(Reader["p_nrooc"]), Convert.ToDateTime(Reader["p_fechaoc"]), Convert.ToString(Reader["p_nrohes"]), Convert.ToDateTime(Reader["p_fechahes"]), Convert.ToDouble(Reader["p_valortotalfactura"]), Convert.ToString(Reader["p_glosa"]), Convert.ToInt32(Reader["id_log"]), Convert.ToInt32(Reader["p_idfacturamanager"]));
                    return inte1;
                }
            }
        }
        public static void setIntegracion6(int p1,int p2, DateTime p3,string p4, DateTime p5,string p6, DateTime p7,string p8, DateTime p9,Double p10,string p11,int p12,int id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("z_ma_addFactura"))
                {

                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = p1;
                    Cmd.Parameters.Add("@idCliente", SqlDbType.Int).Value = p2;/***/
                    Cmd.Parameters.Add("@fechaIngreso", SqlDbType.DateTime).Value = p3;/***/
                    Cmd.Parameters.Add("@condicionPago", SqlDbType.VarChar,40).Value = p4;
                    Cmd.Parameters.Add("@fechaVencimiento", SqlDbType.DateTime).Value =p5;
                    Cmd.Parameters.Add("@nroOC", SqlDbType.Char,30).Value = p6;
                    Cmd.Parameters.Add("@fechaOC", SqlDbType.DateTime).Value = p7;
                    Cmd.Parameters.Add("@nroHes", SqlDbType.Char,30).Value = p8;
                    Cmd.Parameters.Add("@fechaHes", SqlDbType.DateTime).Value = p9;
                    Cmd.Parameters.Add("@valorTotalNeto", SqlDbType.Float).Value = p10;/***/
                    Cmd.Parameters.Add("@glosa", SqlDbType.VarChar).Value = Convert.ToString(p11);/***/
                    Cmd.Parameters.Add("@id_log", SqlDbType.Int).Value = id_log1;
                    Cmd.Parameters.Add("@idFacturaManager", SqlDbType.Int).Value = p12;/***/

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static List<Entities.IntegracionEntity> getInteg6()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion6"))
                {
                    Con.Open();
                    List<Entities.IntegracionEntity> Lst = new List<Entities.IntegracionEntity>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                    {
                        Entities.IntegracionEntity inte1 = new Entities.IntegracionEntity();
                        inte1.Id_log1 = Convert.ToInt32(Reader["id_log"]);
                        inte1.Codigo = Convert.ToString(Reader["p_idproyecto"]);
                        inte1.desc_sp_cont = Convert.ToString(Reader["desc_sp_cont"]);
                        inte1.desc_sp_call = Convert.ToString(Reader["desc_sp_call"]);
                        inte1.estado = Convert.ToInt32(Reader["estado"]);
                        inte1.excepcion = Convert.ToString(Reader["excepcion"]);
                        inte1.fecha_Hora = Convert.ToDateTime(Reader["fecha_Hora"]);
                        Lst.Add(inte1);
                    }

                    return Lst;
                }
            }
        }
      
        //Metodos para integración tabla LOG_z_ma_addNotaVenta

        public static Entities.IntegracionEntity getDetalleIntegracion7(int Id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion7Detalle"))
                {
                    Cmd.Parameters.Add("@id_log", SqlDbType.Int).Value = Id_log1;
                    Con.Open();
                    List<Entities.IntegracionEntity> Lst = new List<Entities.IntegracionEntity>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    Entities.IntegracionEntity inte = new Entities.IntegracionEntity();
                    while (Reader.Read())
                        inte = new Entities.IntegracionEntity(Convert.ToInt32(Reader["id_log"]), Convert.ToString(Reader["desc_sp_cont"]),Convert.ToString(Reader["desc_sp_call"]), Convert.ToString(Reader["desc_call"]),Convert.ToInt32(Reader["estado"]), Convert.ToString(Reader["excepcion"]), Convert.ToDateTime(Reader["fecha_Hora"]), Convert.ToInt32(Reader["p_idproyecto"]),Convert.ToInt32(Reader["p_idnotaventamanager"]), Convert.ToInt32(Reader["p_valortotalproyecto"]), Convert.ToString(Reader["p_rutcliente"]), Convert.ToInt32(Reader["p_idclientemanager"]),Convert.ToDateTime(Reader["p_fechacreacion"]),Convert.ToInt32(Reader["p_idtipovalor"]));

                    return inte;
                }
            }
        }

        public static void setIntegracion7(int p1, int p2, string p3, int p4,string p5,int p6,int p7,int id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("z_ma_addNotaVenta"))
                {

                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = p1;
                    Cmd.Parameters.Add("@idTipoValor", SqlDbType.Int).Value = p2;
                    Cmd.Parameters.Add("@fechaIngreso", SqlDbType.DateTime).Value = Convert.ToDateTime(p3).ToShortDateString();
                    Cmd.Parameters.Add("@idClienteManager", SqlDbType.Int).Value = p4;
                    Cmd.Parameters.Add("@rutCliente", SqlDbType.VarChar).Value = p5;
                    Cmd.Parameters.Add("@valorTotalProyecto", SqlDbType.Int).Value = p6;
                    Cmd.Parameters.Add("@idNotaVentaManager", SqlDbType.Int).Value = p7;
                    Cmd.Parameters.Add("@id_log1", SqlDbType.Int).Value = Convert.ToInt32(id_log1);

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static List<Entities.IntegracionEntity> getInteg7()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion7"))
                {
                    Con.Open();
                    List<Entities.IntegracionEntity> Lst = new List<Entities.IntegracionEntity>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                    {
                        Entities.IntegracionEntity inte1 = new Entities.IntegracionEntity();
                        inte1.Id_log1 = Convert.ToInt32(Reader["id_log"]);
                        inte1.desc_sp_cont = Convert.ToString(Reader["desc_sp_cont"]);
                        inte1.desc_sp_call = Convert.ToString(Reader["desc_sp_call"]);
                        inte1.estado = Convert.ToInt32(Reader["estado"]);
                        inte1.excepcion = Convert.ToString(Reader["excepcion"]);
                        inte1.fecha_Hora = Convert.ToDateTime(Reader["fecha_Hora"]);
                        Lst.Add(inte1);
                    }

                    return Lst;
                }
            }
        }

        //Metodos para integración tabla LOG_z_ma_addOrdenDeProduccion
        public static Entities.IntegracionEntity getDetalleIntegracion8(int Id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion8Detalle"))
                {
                    Cmd.Parameters.Add("@id_log", SqlDbType.Int).Value = Id_log1;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    Entities.IntegracionEntity inte = new Entities.IntegracionEntity();

                    while (Reader.Read())
                        inte = new Entities.IntegracionEntity(Convert.ToInt32(Reader["id_log"]), Convert.ToString(Reader["desc_sp_cont"]), Convert.ToString(Reader["desc_sp_call"]), Convert.ToString(Reader["desc_call"]), Convert.ToInt32(Reader["estado"]), Convert.ToString(Reader["excepcion"]), Convert.ToDateTime(Reader["fecha_Hora"]), Convert.ToString(Reader["p_ordenprd"]), Convert.ToString(Reader["p_nombreproyecto"]), Convert.ToString(Reader["p_nreguistcc"]), Convert.ToInt32(Reader["p_idclientemanager"]), Convert.ToDateTime(Reader["p_fechacomprometida"]), Convert.ToDateTime(Reader["p_fechacreacion"]), Convert.ToString(Reader["p_idarticulomanager"]));

                    return inte;
                }
            }
        }
        public static void setIntegracion8(string p1,string p2,DateTime p3, DateTime p4,int p5,string p6,int id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("z_ma_addOrdenDeProduccion"))
                {

                    Cmd.Parameters.Add("@OrdenPRD", SqlDbType.Int).Value = Convert.ToInt32(p1);
                    Cmd.Parameters.Add("@nreguist_art", SqlDbType.Int).Value = Convert.ToInt32(p2);
                    Cmd.Parameters.Add("@fechaCreacion", SqlDbType.DateTime).Value = p3;
                    Cmd.Parameters.Add("@fechaComprometida", SqlDbType.DateTime).Value = p4;
                    Cmd.Parameters.Add("@idCliente", SqlDbType.Int).Value = Convert.ToInt32(p5);
                    Cmd.Parameters.Add("@centroCostro", SqlDbType.Float).Value = Convert.ToDouble(p1);
                    Cmd.Parameters.Add("@id_log4", SqlDbType.Int).Value = id_log1;
                    Cmd.Parameters.Add("@nombreProyecto", SqlDbType.VarChar).Value = Convert.ToString(p6);

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }
        public static List<Entities.IntegracionEntity> getInteg8()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion8"))
                {
                    Con.Open();
                    List<Entities.IntegracionEntity> Lst = new List<Entities.IntegracionEntity>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                    {
                        Entities.IntegracionEntity inte1 = new Entities.IntegracionEntity();
                        inte1.Id_log1 = Convert.ToInt32(Reader["id_log"]);
                        inte1.Codigo = Convert.ToString(Reader["p_ordenprd"]);
                        inte1.desc_sp_cont = Convert.ToString(Reader["desc_sp_cont"]);
                        inte1.desc_sp_call = Convert.ToString(Reader["desc_sp_call"]);
                        inte1.estado = Convert.ToInt32(Reader["estado"]);
                        inte1.excepcion = Convert.ToString(Reader["excepcion"]);
                        inte1.fecha_Hora = Convert.ToDateTime(Reader["fecha_Hora"]);
                        Lst.Add(inte1);
                    }

                    return Lst;
                }
            }
        }



        //Metodos para integración tabla LOG_z_ma_addOrdenDeProduccionMontaje

        public static Entities.IntegracionEntity getDetalleIntegracion9(int Id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion9Detalle"))
                {
                    Cmd.Parameters.Add("@id_log", SqlDbType.Int).Value = Id_log1;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    Entities.IntegracionEntity inte = new Entities.IntegracionEntity();
                    while (Reader.Read())
                        inte = new Entities.IntegracionEntity(Convert.ToInt32(Reader["id_log"]), Convert.ToString(Reader["desc_sp_cont"]), Convert.ToString(Reader["desc_sp_call"]), Convert.ToString(Reader["desc_call"]), Convert.ToInt32(Reader["estado"]), Convert.ToString(Reader["excepcion"]), Convert.ToDateTime(Reader["fecha_Hora"]), Convert.ToString(Reader["p_ordenprdm"]), Convert.ToString(Reader["p_nombrem"]), Convert.ToString(Reader["p_nreguistcc"]), Convert.ToInt32(Reader["p_idclientemanager"]), Convert.ToDateTime(Reader["p_fechacomprometida"]), Convert.ToDateTime(Reader["p_fechacreacion"]), Convert.ToString(Reader["p_idarticulomanagermon"]));

                    return inte;
                }
            }
        }

        public static void setIntegracion9(string p1,string p2, DateTime p3, DateTime p4,int p5,int id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                
                using (SqlCommand Cmd = Conexion.GetCommand("z_ma_addOrdenDeProduccionMontaje"))
                {

                    Cmd.Parameters.Add("@OrdenPRDM", SqlDbType.Int).Value = Convert.ToInt32(p1);
                    Cmd.Parameters.Add("@nreguist_artM", SqlDbType.Int).Value = Convert.ToInt32(p2);
                    Cmd.Parameters.Add("@fechaCreacionM", SqlDbType.DateTime).Value = p3;
                    Cmd.Parameters.Add("@fechaComprometidaM", SqlDbType.DateTime).Value = p4;
                    Cmd.Parameters.Add("@idCliente", SqlDbType.Int).Value = p5;
                    Cmd.Parameters.Add("@centroCostro", SqlDbType.Float).Value = Convert.ToDouble(p1); ;
                    Cmd.Parameters.Add("@id_log6", SqlDbType.Int).Value = id_log1;
                    Cmd.Parameters.Add("@nombreProyectoM", SqlDbType.VarChar).Value = Convert.ToString(p2);

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }
        public static List<Entities.IntegracionEntity> getInteg9()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion9"))
                {
                    Con.Open();
                    List<Entities.IntegracionEntity> Lst = new List<Entities.IntegracionEntity>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                    {
                        Entities.IntegracionEntity inte1 = new Entities.IntegracionEntity();
                        inte1.Id_log1 = Convert.ToInt32(Reader["id_log"]);
                        inte1.Codigo = Convert.ToString(Reader["p_ordenprdm"]);
                        inte1.desc_sp_cont = Convert.ToString(Reader["desc_sp_cont"]);
                        inte1.desc_sp_call = Convert.ToString(Reader["desc_sp_call"]);
                        inte1.estado = Convert.ToInt32(Reader["estado"]);
                        inte1.excepcion = Convert.ToString(Reader["excepcion"]);
                        inte1.fecha_Hora = Convert.ToDateTime(Reader["fecha_Hora"]);
                        Lst.Add(inte1);
                    }

                    return Lst;
                }
            }
        }


        //Metodos para integración tabla LOG_z_ma_setCliente

        public static Entities.IntegracionEntity getDetalleIntegracion10(int Id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion10Detalle"))
                {
                    Cmd.Parameters.Add("@id_log", SqlDbType.Int).Value = Id_log1;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    Entities.IntegracionEntity inte = new Entities.IntegracionEntity();
                    while (Reader.Read())
                        inte = new Entities.IntegracionEntity(Convert.ToInt32(Reader["id_log"]), Convert.ToString(Reader["desc_sp_cont"]), Convert.ToString(Reader["desc_sp_call"]), Convert.ToString(Reader["desc_call"]), Convert.ToInt32(Reader["estado"]), Convert.ToString(Reader["excepcion"]), Convert.ToDateTime(Reader["fecha_Hora"]), Convert.ToString(Reader["p_nombreFantasia"]), Convert.ToInt32(Reader["p_rut"]), Convert.ToString(Reader["p_dv"]), Convert.ToString(Reader["p_razonsocial"]), Convert.ToString(Reader["p_giro"]), Convert.ToString(Reader["p_direccion"]), Convert.ToString(Reader["p_telefono"]), Convert.ToString(Reader["p_comuna"]), Convert.ToString(Reader["p_ciudad"]), Convert.ToInt32(Reader["p_nreguist"]));

                    return inte;
                }
            }
        }

        public static void setIntegracion10(string p1,int p2,string p3,string p4,string p5,string p6,string p7,string p8,string p9,int p10,int id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("z_ma_setCliente"))
                {

                    Cmd.Parameters.Add("@nombreFantasia", SqlDbType.VarChar).Value = p1;
                    Cmd.Parameters.Add("@rut", SqlDbType.Int).Value = Convert.ToInt32(p2);/***/
                    Cmd.Parameters.Add("@dv", SqlDbType.Char).Value = p3;/***/
                    Cmd.Parameters.Add("@razonsocial", SqlDbType.VarChar).Value = p4;
                    Cmd.Parameters.Add("@giro", SqlDbType.VarChar).Value = p5;
                    Cmd.Parameters.Add("@direccion", SqlDbType.VarChar).Value = p6;
                    Cmd.Parameters.Add("@telefono", SqlDbType.VarChar).Value = p7;
                    Cmd.Parameters.Add("@comuna", SqlDbType.VarChar).Value = p8;
                    Cmd.Parameters.Add("@ciudad", SqlDbType.VarChar).Value = p9;
                    Cmd.Parameters.Add("@nreguist", SqlDbType.Int).Value = p10;
                    Cmd.Parameters.Add("@id_log", SqlDbType.Int).Value = id_log1;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static List<Entities.IntegracionEntity> getInteg10()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion10"))
                {
                    Con.Open();
                    List<Entities.IntegracionEntity> Lst = new List<Entities.IntegracionEntity>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                    {
                        Entities.IntegracionEntity inte1 = new Entities.IntegracionEntity();
                        inte1.Id_log1 = Convert.ToInt32(Reader["id_log"]);
                        inte1.desc_sp_cont = Convert.ToString(Reader["desc_sp_cont"]);
                        inte1.desc_sp_call = Convert.ToString(Reader["desc_sp_call"]);
                        inte1.estado = Convert.ToInt32(Reader["estado"]);
                        inte1.excepcion = Convert.ToString(Reader["excepcion"]);
                        inte1.fecha_Hora = Convert.ToDateTime(Reader["fecha_Hora"]);
                        Lst.Add(inte1);
                    }

                    return Lst;
                }
            }
        }


        //Metodos para integración tabla LOG_z_ma_setCodigoFactura

        public static Entities.IntegracionEntity getDetalleIntegracion11(int Id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion11Detalle"))
                {
                    Cmd.Parameters.Add("@id_log", SqlDbType.Int).Value = Id_log1;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    Entities.IntegracionEntity inte = new Entities.IntegracionEntity();
                    while (Reader.Read())
                        inte = new Entities.IntegracionEntity(Convert.ToInt32(Reader["id_log"]), Convert.ToString(Reader["desc_sp_cont"]), Convert.ToString(Reader["desc_sp_call"]), Convert.ToString(Reader["desc_call"]), Convert.ToInt32(Reader["estado"]), Convert.ToString(Reader["excepcion"]), Convert.ToDateTime(Reader["fecha_Hora"]), Convert.ToInt32(Reader["p_idproyecto"]));

                    return inte;
                }
            }
        }

        public static void setIntegracion11(int p1,int id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("z_ma_setCodigoFactura"))
                {

                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value =p1;
                    Cmd.Parameters.Add("@id_log2", SqlDbType.Int).Value = id_log1;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }
        public static List<Entities.IntegracionEntity> getInteg11()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion11"))
                {
                    Con.Open();
                    List<Entities.IntegracionEntity> Lst = new List<Entities.IntegracionEntity>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                    {
                        Entities.IntegracionEntity inte1 = new Entities.IntegracionEntity();
                        inte1.Id_log1 = Convert.ToInt32(Reader["id_log"]);
                        inte1.Codigo = Convert.ToString(Reader["p_idproyecto"]);
                        inte1.desc_sp_cont = Convert.ToString(Reader["desc_sp_cont"]);
                        inte1.desc_sp_call = Convert.ToString(Reader["desc_sp_call"]);
                        inte1.estado = Convert.ToInt32(Reader["estado"]);
                        inte1.excepcion = Convert.ToString(Reader["excepcion"]);
                        inte1.fecha_Hora = Convert.ToDateTime(Reader["fecha_Hora"]);
                        Lst.Add(inte1);
                    }

                    return Lst;
                }
            }
        }



        //Metodos para integración tabla LOG_z_ma_setNotaVenta

        public static Entities.IntegracionEntity getDetalleIntegracion12(int Id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion12Detalle"))
                {
                    Cmd.Parameters.Add("@id_log", SqlDbType.Int).Value = Id_log1;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    Entities.IntegracionEntity inte = new Entities.IntegracionEntity();
                    while (Reader.Read())
                        inte = new Entities.IntegracionEntity(Convert.ToInt32(Reader["id_log"]), Convert.ToString(Reader["desc_sp_cont"]), Convert.ToString(Reader["desc_sp_call"]), Convert.ToString(Reader["desc_call"]), Convert.ToInt32(Reader["estado"]), Convert.ToString(Reader["excepcion"]), Convert.ToDateTime(Reader["fecha_Hora"]), Convert.ToInt32(Reader["p_idproyecto"]));

                    return inte;
                }
            }
        }

        public static void setIntegracion12(int p1,int id_log1)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("z_ma_setNotaVenta"))
                {

                    Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = p1;  //variable de parametro SP
                    Cmd.Parameters.Add("@id_log", SqlDbType.Int).Value = id_log1;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static List<Entities.IntegracionEntity> getInteg12()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getIntegracion12"))
                {
                    Con.Open();
                    List<Entities.IntegracionEntity> Lst = new List<Entities.IntegracionEntity>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                    {
                        Entities.IntegracionEntity inte1 = new Entities.IntegracionEntity();
                        inte1.Id_log1 = Convert.ToInt32(Reader["id_log"]);
                        inte1.desc_sp_cont = Convert.ToString(Reader["desc_sp_cont"]);
                        inte1.desc_sp_call = Convert.ToString(Reader["desc_sp_call"]);
                        inte1.estado = Convert.ToInt32(Reader["estado"]);
                        inte1.excepcion = Convert.ToString(Reader["excepcion"]);
                        inte1.fecha_Hora = Convert.ToDateTime(Reader["fecha_Hora"]);
                        Lst.Add(inte1);
                    }

                    return Lst;
                }
            }
        }
       
        //Metodos para integración tabla LOG_Z_MA_LISTA

        public static List<Entities.Index> getDetalleIndex()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getDetalleIndex"))
                {
                    Con.Open();
                    List<Entities.Index> Lst = new List<Entities.Index>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                    {
                        Entities.Index index = new Entities.Index();
                        index.id = Convert.ToInt32(Reader["id"]);
                        index.descripcion= Convert.ToString(Reader["descripcion"]);
                        index.procedimiento = Convert.ToString(Reader["procedimiento"]);
                        index.estado = Convert.ToInt32(Reader["estado"]);
                        index.errores = Convert.ToInt32(Reader["errores"]);
                        Lst.Add(index);
                    }

                    return Lst;
                }
            }
        }

        public static int setActualizaContador()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("LOG_z_ma_ActualizaContadorSP"))
                {
                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();
                    return 0;
                }
            }
        }
    }
}

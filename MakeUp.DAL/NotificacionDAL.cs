﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace MakeUp.DAL
{
    public class NotificacionDAL
    {
        private static DAO_Conexion _Conexion = new DAO_Conexion();

        public static List<Entities.NotificacionEntity> GetNotificacion()
        {
            using (SqlConnection Con = _Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = _Conexion.GetCommand("getNotificacion"))
                {
                    Con.Open();
                    List<Entities.NotificacionEntity> Lst = new List<Entities.NotificacionEntity>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                        Lst.Add(new Entities.NotificacionEntity(Convert.ToInt16(Reader["ID_NOTIFICACION"]), Reader["NOMBRE"].ToString(), Convert.ToBoolean(Reader["ESTADO"])));

                    return Lst;
                }
            }
        }

        public static List<Entities.RolPredeterminadoEntity> GetNotificarRol(int IDNotificacion)
        {
            using (SqlConnection Con = _Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = _Conexion.GetCommand("getNotificarRol"))
                {
                    Cmd.Parameters.Add("@idNotificacion", SqlDbType.Int).Value = IDNotificacion;
                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();
                    List<Entities.RolPredeterminadoEntity> Lst = new List<Entities.RolPredeterminadoEntity>();

                    while (Reader.Read())
                        Lst.Add(new Entities.RolPredeterminadoEntity(Convert.ToInt16(Reader["ID_ROLPREDETERMINADO"]), Reader["NOMBRE"].ToString(), Convert.ToBoolean(Reader["VINCULADO"])));
                    
                    return Lst;
                }
            }
        }

        public static List<Entities.FiltroNotificacionEntity> GetFiltroRolNotificacion(int IDNotificacion, int IDRolPredeterminado, ref bool FiltraRol)
        {
            using (SqlConnection Con = _Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = _Conexion.GetCommand("getFiltroRolNotificacion"))
                {
                    Cmd.Parameters.Add("@idNotificacion", SqlDbType.Int).Value = IDNotificacion;
                    Cmd.Parameters.Add("@idRolPredeterminado", SqlDbType.Int).Value = IDRolPredeterminado;
                    SqlParameter p_FiltraRol = new SqlParameter("@filtraRol", SqlDbType.Bit);
                    p_FiltraRol.Direction = ParameterDirection.InputOutput;
                    p_FiltraRol.Value = 0;
                    Cmd.Parameters.Add(p_FiltraRol);

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();
                    List<Entities.FiltroNotificacionEntity> Lst = new List<Entities.FiltroNotificacionEntity>();

                    while (Reader.Read())
                        Lst.Add(new Entities.FiltroNotificacionEntity(Convert.ToInt16(Reader["ID_FILTRO"]), Reader["NOMBRE"].ToString(), Convert.ToBoolean(Reader["FILTRO_SELECCIONADO"])));

                    Reader.Close();
                    
                    //Obtiene valor
                    FiltraRol = Convert.ToBoolean(p_FiltraRol.Value);

                    return Lst;
                }
            }
        }

        public static void SetNotificarRol(int IDNotificacion, DataTable dtRolEnNotificacion)
        {
            using (SqlConnection Con = _Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = _Conexion.GetCommand("setNotificarRol"))
                {
                    Cmd.Parameters.Add("@idNotificacion", SqlDbType.Int).Value = IDNotificacion;
                    Cmd.Parameters.Add("@listRol", SqlDbType.Structured).Value = dtRolEnNotificacion;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static void SetNotificacionesEnEstadoVista(int IDUsuario)
        {
            using (SqlConnection Con = _Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = _Conexion.GetCommand("setNotificacionesEnEstadoVista"))
                {
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = IDUsuario;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static void SetIngresoNotificacion(int IDNotificacionApp, int IDUsuario, bool Ingreso, DateTime FechaIngreso)
        {
            using (SqlConnection Con = _Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = _Conexion.GetCommand("setIngresoNotificacion"))
                {
                    Cmd.Parameters.Add("@idNotificacionApp", SqlDbType.Int).Value = IDNotificacionApp;
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = IDUsuario;
                    Cmd.Parameters.Add("@ingreso", SqlDbType.Bit).Value = Ingreso;
                    Cmd.Parameters.Add("@fechaIngreso", SqlDbType.DateTime).Value = FechaIngreso;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Obtiene las últimas N notificaciones, según parametro "cantidadNotificacionesApp" que se encuentra en Web.config
        /// </summary>
        /// <param name="IDUsuario"></param>
        /// <returns></returns>
        public static List<Entities.NotificacionAppEntity> GetNotificacionesAppUsuario(int IDUsuario, int CantidadFilas, ref int ContNotificacionesSinVer)
        {
            using (SqlConnection Con = _Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = _Conexion.GetCommand("getNotificacionesAppUsuario"))
                {
                    Cmd.Parameters.Add("@idUsuario", SqlDbType.Int).Value = IDUsuario;
                    Cmd.Parameters.Add("@CantidadFilas", SqlDbType.Int).Value = CantidadFilas;
                    SqlParameter p_ContNotificacionesSinVer = new SqlParameter("@contNotificacionesSinVer", SqlDbType.Int);
                    p_ContNotificacionesSinVer.Direction = ParameterDirection.InputOutput;
                    p_ContNotificacionesSinVer.Value = 0;
                    Cmd.Parameters.Add(p_ContNotificacionesSinVer);

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();
                    List<Entities.NotificacionAppEntity> Lst = new List<Entities.NotificacionAppEntity>();

                    while (Reader.Read())
                        Lst.Add(new Entities.NotificacionAppEntity(Convert.ToInt16(Reader["ID_NOTIFICACION_APP"]),
                                                                   IDUsuario,
                                                                   Reader["MENSAJE"].ToString(),
                                                                   Reader["URL"].ToString(),
                                                                   Convert.ToBoolean(Reader["INGRESO"]),
                                                                   Convert.ToDateTime(Reader["FECHA_EVENTO"]),
                                                                   Convert.ToInt16(Reader["ID_RESPONSABLE"]),
                                                                   Reader["NOMBRE_RESPONSABLE"].ToString(),
                                                                   Reader["APELLIDO_PATERNO_RESPONSABLE"].ToString()));

                    //Cierra el Reader para posterior leer los parámetros de salida.
                    Reader.Close();

                    //Obtiene el número de notificaciones sin ser vistas.
                    ContNotificacionesSinVer = Convert.ToInt32(p_ContNotificacionesSinVer.Value);

                    return Lst;
                }
            }
        }

        public static void AddPermiteFiltrarRolEnNotificacion(int IDRolPredeterminado, DataTable dtNotificacion)
        {
            using (SqlConnection Con = _Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = _Conexion.GetCommand("addPermiteFiltrarRolEnNotificacion"))
                {
                    Cmd.Parameters.Add("@idRolPredeterminado", SqlDbType.Int).Value = IDRolPredeterminado;
                    Cmd.Parameters.Add("@lstIdNotificacion", SqlDbType.Structured).Value = dtNotificacion;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static List<Entities.NotificacionEntity> GetFiltrarRolEnNotificacion(int IDRolPredeterminado)
        {
            using (SqlConnection Con = _Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = _Conexion.GetCommand("getFiltrarRolEnNotificacion"))
                {
                    Cmd.Parameters.Add("@idRolPredeterminado", SqlDbType.Int).Value = IDRolPredeterminado;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    List<Entities.NotificacionEntity> Lst = new List<Entities.NotificacionEntity>();

                    while (Reader.Read())
                        Lst.Add(new Entities.NotificacionEntity(Convert.ToInt16(Reader["ID_NOTIFICACION"]), Reader["NOMBRE"].ToString(), Convert.ToBoolean(Reader["VINCULADO"]))); //Variable Estado usada para determinar si el rol se filtra en la notificación

                    return Lst;
                }
            }
        }

        public static void SetFiltroRolNotificacion(int IDRolPredeterminado, int IDNotificacion, int IDFiltro)
        {
            using (SqlConnection Con = _Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = _Conexion.GetCommand("setFiltroRolNotificacion"))
                {
                    Cmd.Parameters.Add("@idRolPredeterminado", SqlDbType.Int).Value = IDRolPredeterminado;
                    Cmd.Parameters.Add("@idNotificacion", SqlDbType.Int).Value = IDNotificacion;
                    Cmd.Parameters.Add("@idFiltro", SqlDbType.Int).Value = IDFiltro;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

                    //#region Agrega notificaciones a eventos del sistema
                    //public static void AddNotificacionCreacionProyecto(int IDProyecto,int IDUsuarioResponsableNotificacion, DateTime FechaEvento)
                    //{
                    //    using (SqlConnection Con = _Conexion.GetConexionMakeUp())
                    //    {
                    //        using (SqlCommand Cmd = _Conexion.GetCommand("addNotificacionCreacionProyecto"))
                    //        {
                    //            Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    //            Cmd.Parameters.Add("@idUsuarioResponsableNotificacion", SqlDbType.Int).Value = IDUsuarioResponsableNotificacion;
                    //            Cmd.Parameters.Add("@fechaEvento", SqlDbType.DateTime).Value = FechaEvento;

                    //            Con.Open();
                    //            Cmd.ExecuteNonQuery();
                    //        }
                    //    }
                    //}

                    //public static void AddNotificacionFacturarEstadoPago(int IDProyecto, int IDEstadoPago, int IDUsuarioResponsableNotificacion, DateTime FechaEvento)
                    //{
                    //    using (SqlConnection Con = _Conexion.GetConexionMakeUp())
                    //    {
                    //        using (SqlCommand Cmd = _Conexion.GetCommand("addNotificacionFacturarEstadoPago"))
                    //        {
                    //            Cmd.Parameters.Add("@idProyecto", SqlDbType.Int).Value = IDProyecto;
                    //            Cmd.Parameters.Add("@idEstadoPago", SqlDbType.Int).Value = IDEstadoPago;
                    //            Cmd.Parameters.Add("@idUsuarioResponsableNotificacion", SqlDbType.Int).Value = IDUsuarioResponsableNotificacion;
                    //            Cmd.Parameters.Add("@fechaEvento", SqlDbType.DateTime).Value = FechaEvento;

                    //            Con.Open();
                    //            Cmd.ExecuteNonQuery();
                    //        }
                    //    }
                    //}
                    //#endregion
                }
}

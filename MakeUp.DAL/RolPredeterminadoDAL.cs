﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace MakeUp.DAL
{
    public class RolPredeterminadoDAL
    {
        private static DAO_Conexion Conexion = new DAO_Conexion();

        public static void AddRolPredeterminado(string Nombre, string Descripcion, DataTable dtPagina, DataTable dtControl)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("addRolPredeterminado"))
                {
                    Cmd.Parameters.Add("@nombre", SqlDbType.VarChar, 25).Value = Nombre;
                    Cmd.Parameters.Add("@descripcion", SqlDbType.VarChar, 150).Value = Descripcion;
                    Cmd.Parameters.Add("@listPagina", SqlDbType.Structured).Value = dtPagina;
                    Cmd.Parameters.Add("@listControl", SqlDbType.Structured).Value = dtControl;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static List<Entities.RolPredeterminadoEntity> GetListadoRolPredeterminadoCreado()
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getListadoRolPredeterminadoCreado"))
                {
                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    List<Entities.RolPredeterminadoEntity> Lst = new List<Entities.RolPredeterminadoEntity>();

                    while (Reader.Read())
                        Lst.Add(new Entities.RolPredeterminadoEntity(Convert.ToInt16(Reader["ID_ROLPREDETERMINADO"].ToString()), Reader["NOMBRE"].ToString()));

                    return Lst;
                }
            }
        }

        public static Entities.RolPredeterminadoEntity GetRolPredeterminado(int IDRolPredeterminado)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getRolPredeterminado"))
                {
                    Cmd.Parameters.Add("@idRolPredeterminado", SqlDbType.Int).Value = IDRolPredeterminado;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    Entities.RolPredeterminadoEntity Rol = new Entities.RolPredeterminadoEntity();

                    while (Reader.Read())
                        Rol = new Entities.RolPredeterminadoEntity(IDRolPredeterminado, Reader["NOMBRE"].ToString(), Reader["DESCRIPCION"].ToString());

                    Reader.NextResult();

                    while (Reader.Read())
                        Rol.Modulo.Add(new Entities.PaginaEntity(Convert.ToInt16(Reader["ID_PAGINA"].ToString()), Reader["NOMBRE"].ToString(), Reader["URI"].ToString(), Convert.ToBoolean(Reader["SELECCIONADO"]), Convert.ToBoolean(Reader["PERMITIDO_POR_DEFECTO"])));

                    Reader.NextResult();

                    while (Reader.Read())
                        Rol.Modulo.Find(x => x.ID == Convert.ToInt16(Reader["ID_PAGINA"].ToString())).Controles.Add(new Entities.PaginaEntity.Control(Convert.ToInt16(Reader["ID_CONTROL"].ToString()), Reader["CONTROL_ID"].ToString(), Reader["DESCRIPCION"].ToString(), Convert.ToBoolean(Convert.ToInt16(Reader["SELECCIONADO"].ToString()))));

                    return Rol;
                }
            }
        }

        public static void SetRolPredeterminado(int IDRolPredeterminado, string Nombre, string Descripcion, DataTable dtPagina, DataTable dtControl)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("setRolPredeterminado"))
                {
                    Cmd.Parameters.Add("@idRolPredeterminado", SqlDbType.Int).Value = IDRolPredeterminado;
                    Cmd.Parameters.Add("@nombre", SqlDbType.VarChar, 25).Value = Nombre;
                    Cmd.Parameters.Add("@descripcion", SqlDbType.VarChar, 150).Value = Descripcion;
                    Cmd.Parameters.Add("@listPagina", SqlDbType.Structured).Value = dtPagina;
                    Cmd.Parameters.Add("@listControl", SqlDbType.Structured).Value = dtControl;

                    Con.Open();
                    Cmd.ExecuteNonQuery();
                }
            }
        }

        public static List<Entities.RolPredeterminadoEntity> DelRolPredeterminado(int IDRolPredeterminado)
        {
            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("delRolPredeterminado"))
                {
                    Cmd.Parameters.Add("@idRolPredeterminado", SqlDbType.Int).Value = IDRolPredeterminado;

                    Con.Open();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    List<Entities.RolPredeterminadoEntity> Lst = new List<Entities.RolPredeterminadoEntity>();

                    while (Reader.Read())
                        Lst.Add(new Entities.RolPredeterminadoEntity(Convert.ToInt16(Reader["ID_ROLPREDETERMINADO"].ToString()), Reader["NOMBRE"].ToString()));

                    return Lst;
                }
            }
        }
    }
}

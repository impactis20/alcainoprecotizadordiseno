﻿using System.Configuration;
using System.Data.SqlClient;

namespace MakeUp.DAL
{
    public class DAO_Conexion
    {
        private SqlConnection _Conn { get; set; }
        private SqlCommand _Command { get; set; }

        public SqlConnection GetConexionMakeUp()
        {
            string Conexion = ConfigurationManager.ConnectionStrings["MakeUp2"].ToString();
            _Conn = new SqlConnection(Conexion);

            return _Conn;
        }

        public SqlCommand GetCommand(string Query)
        {
            _Command = new SqlCommand(Query, _Conn);
            _Command.CommandType = System.Data.CommandType.StoredProcedure;

            return _Command;
        }
    }
}

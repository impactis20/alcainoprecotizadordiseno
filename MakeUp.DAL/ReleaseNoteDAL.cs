﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeUp.DAL
{
    public class ReleaseNoteDAL
    {
        private static DAO_Conexion Conexion = new DAO_Conexion();

        public static List<Entities.ReleaseNoteEntity> GetRelease() {

            using (SqlConnection Con = Conexion.GetConexionMakeUp())
            {
                using (SqlCommand Cmd = Conexion.GetCommand("getReleaseNote"))
                {
                    Con.Open();
                    List<Entities.ReleaseNoteEntity> Lst = new List<Entities.ReleaseNoteEntity>();
                    SqlDataReader Reader = Cmd.ExecuteReader();

                    while (Reader.Read())
                    {
                        Entities.ReleaseNoteEntity ReleaseNote = new Entities.ReleaseNoteEntity();

                        ReleaseNote.ID = Convert.ToInt16(Reader["ID"].ToString());
                        ReleaseNote.FECHA = Convert.ToDateTime(Reader["FECHA"].ToString());
                        ReleaseNote.DESCRIPCION = Reader["DESCRIPCION"].ToString();
                        ReleaseNote.OBSERVACION = Reader["OBSERVACION"].ToString();

                        Lst.Add(ReleaseNote);
                    }

                    return Lst;
                }
            }
        }
    }
}

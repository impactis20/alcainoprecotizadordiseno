﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MakeUp.DAL;

namespace MakeUp.BL
{
    public class GenericoBL
    {
        public static List<string> GetAutocompletar(string Prefix, string Tabla, string Campo)
        {
            return GenericoDAL.GetAutocompletar(Prefix, Tabla, Campo);
        }
        
        public static List<Entities.GenericoEntity.Region> GetRegion()
        {
            return GenericoDAL.GetRegion();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using MakeUp.DAL.BancoCentral;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

using MakeUp.DAL;


namespace MakeUp.BL
{
    public class TipoValorBL
    {
        public static List<Entities.TipoValorEntity> GetTipoValor()
        {
            string user = "968243608";
            string password = "ef2BS7ld";
            //string frecuencyCode = "DAILY";
            //string[] seriesIds = new string[] { "F073.TCO.PRE.Z.D", "F072.CLP.EUR.N.O.D", "F073.UFF.PRE.Z.D" };

            DateTime Hoy = DateTime.Today;

            string firstDate = Hoy.ToString("yyyy-MM-dd");
            string lastDate = Hoy.ToString("yyyy-MM-dd");
            string[] fecha = lastDate.Split('-');
            int fechaID = Convert.ToInt32(fecha[0] + fecha[1] + fecha[2]);

            List<Entities.TipoValorEntity> Lst = TipoValorDAL.GetTipoValor();

            if (Lst[1].FechaUltimaActualizacion.ToShortDateString() != DateTime.Today.ToShortDateString() || Lst[1].FechaUltimaActualizacion == null)
            {
                try
                {

                    DataTable dt = new DataTable();
                    dt.Columns.Add("ID", typeof(int));
                    dt.Columns.Add("VALOR", typeof(decimal));
                    dt.Columns.Add("FECHA", typeof(DateTime));
                    //dt.Columns.Add("IDUNICO", typeof(int));
                

                    DateTime Fecha = DateTime.Now;
                    string[] seriesIds = new string[] { "F073.TCO.PRE.Z.D" };

                    using (SieteWS SieteWS = new SieteWS())
                    {

                        Respuesta respuesta = SieteWS.GetSeries(user, password, firstDate, lastDate, seriesIds);
                        DataRow dr = dt.NewRow();

                        //(Agrega CLP para obtenerlo en la función)

                        dr["ID"] = 1;
                        dr["VALOR"] = 1;
                        dr["FECHA"] = Fecha;
                        //dr["IDUNICO"] = 1 + fechaID;
                        dt.Rows.Add(dr);

                        foreach (fameSeries serie in respuesta.Series)
                        {
                            foreach (obs observacion in serie.obs)
                            {
                                double dolar= observacion.value;

                                //(Agrega Dolar para obtenerlo en la función)
                                dr = dt.NewRow();
                                dr["ID"] = 3;
                                dr["VALOR"] = Convert.ToDecimal(dolar);
                                dr["FECHA"] = Fecha;
                                //dr["IDUNICO"] = 3 + fechaID;
                                dt.Rows.Add(dr);
                            }
                        }

                        string[] seriesIds1 = new string[] { "F072.CLP.EUR.N.O.D" };
                        Respuesta respuesta1 = SieteWS.GetSeries(user, password, firstDate, lastDate, seriesIds1);
                        foreach (fameSeries serie in respuesta1.Series)
                        {
                            foreach (obs observacion in serie.obs)
                            {
                                //(Agrega Euro para obtenerlo en la función)
                                double euro = observacion.value;
                                dr = dt.NewRow();
                                dr["ID"] = 4;
                                dr["VALOR"] = Convert.ToDecimal(euro);
                                dr["FECHA"] = Fecha;
                                //dr["IDUNICO"] = 4 + fechaID;
                                dt.Rows.Add(dr);
                            }
                        }

                        string[] seriesIds2 = new string[] { "F073.UFF.PRE.Z.D" };
                        Respuesta respuesta2 = SieteWS.GetSeries(user, password, firstDate, lastDate, seriesIds2);
                        foreach (fameSeries serie in respuesta2.Series)
                        {
                            foreach (obs observacion in serie.obs)
                            {
                                //(Agrega Unidad de Fomento para obtenerlo en la función)
                                double UF = observacion.value;
                                dr = dt.NewRow();
                                dr["ID"] = 2;
                                dr["VALOR"] = Convert.ToDecimal(UF);
                                dr["FECHA"] = Fecha;
                                //dr["IDUNICO"] = 2 + fechaID;
                                dt.Rows.Add(dr);
                            }
                        }

                        TipoValorDAL.AddActualValorTipoValor(dt); //Agrega los valores del día actualizados
                        return TipoValorDAL.GetTipoValor();
                    }

                }
                catch
                {
                return Lst;
                }
            }

            return Lst;
        }

    public static decimal GetValorTipoValor(int IDTipoValor)
    {
        return TipoValorDAL.GetValorTipoValor(IDTipoValor);
    }

    public static float GetValorTipoValor(int IDTipoValor, DateTime Fecha)
    {
        return TipoValorDAL.GetValorTipoValor(IDTipoValor, Fecha);
    }

    }
}

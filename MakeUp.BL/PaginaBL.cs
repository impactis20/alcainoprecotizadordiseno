﻿using System.Collections.Generic;

using MakeUp.DAL;

namespace MakeUp.BL
{
    public class PaginaBL
    {
        public static List<Entities.PaginaEntity> GetAllPaginaControles()
        {
            return PaginaDAL.GetAllPaginaControles();
        }

        public static bool GetHabilitarControlPagina(int IDControl, int IDUsuario)
        {
            return PaginaDAL.GetHabilitarControlPagina(IDControl, IDUsuario);
        }
    }
}

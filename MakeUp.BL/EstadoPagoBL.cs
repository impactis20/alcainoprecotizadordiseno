﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MakeUp.DAL;

namespace MakeUp.BL
{
    public class EstadoPagoBL
    {
        public static bool AddEstadoPago(int IDProyecto, decimal Porcentaje, decimal Monto, string Glosa, int IDUsuarioCreador, DateTime Fecha)
        {
            return EstadoPagoDAL.AddEstadoPago(IDProyecto, Porcentaje, Monto, Glosa, IDUsuarioCreador, Fecha);
        }

        public static List<Entities.EstadoPagoEntity> GetEstadoPago(int IDProyecto)
        {
            return EstadoPagoDAL.GetEstadoPago(IDProyecto);
        }

        public static int DelEstadoPago(int IDEstadoPago)
        {
            return EstadoPagoDAL.DelEstadoPago(IDEstadoPago);
        }

        public static void SetEstadoPago(int IDProyecto, string DecisionEP)
        {
            EstadoPagoDAL.SetEstadoPago(IDProyecto, DecisionEP);
        }

        //public static void SetEstadoPagoFacturado(int IDProyecto)
        //{
        //    EstadoPagoDAL.SetEstadoPagoFacturado(IDProyecto);
        //}

        public static bool GetTieneEPAbiertos(int IDProyecto)
        {
            if (GetEstadoPago(IDProyecto).Where(x => x.FacturaEmitida == false).ToList().Count > 0)
                return true;
            else
                return false;
        }

        public static bool GetTieneEPEmitidos(int IDProyecto)
        {
            if (GetEstadoPago(IDProyecto).Where(x => x.FacturaEmitida == true).ToList().Count > 0)
                return true;
            else
                return false;
        }
    }
}

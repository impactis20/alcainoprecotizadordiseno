﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MakeUp.DAL;

namespace MakeUp.BL
{
    public class TipoUnidadBL
    {
        public static int AddTipoUnidad(string Nombre, string Sigla)
        {
            return TipoUnidadDAL.AddTipoUnidad(Nombre, Sigla);
        }

        public static List<Entities.TipoUnidadEntity> GetTipoUnidad(int ID, string Nombre, string Sigla, bool EditaRegistro)
        {
            List<Entities.TipoUnidadEntity> Lst = TipoUnidadDAL.GetTipoUnidad(ID, Nombre, Sigla);

            if (!EditaRegistro)
            {
                if (Nombre.Length > 0 || Sigla.Length > 0)
                {
                    foreach (Entities.TipoUnidadEntity TipoUnidad in Lst)
                    {
                        if (Nombre.Length > 0)
                            TipoUnidad.Nombre = TipoUnidad.Nombre.ToLower().Replace(Nombre.ToLower(), string.Format("<mark>{0}</mark>", Nombre.ToLower()));

                        if (Sigla.Length > 0)
                            TipoUnidad.Sigla = TipoUnidad.Sigla.ToLower().Replace(Sigla.ToLower(), string.Format("<mark>{0}</mark>", Sigla.ToLower()));
                    }
                }
            }

            return Lst;
        }

        public static void SetTipoUnidad(int ID, string Nombre, string Sigla)
        {
            TipoUnidadDAL.SetTipoUnidad(ID, Nombre, Sigla);
        }
        public static void DelTipoUnidad(int ID)
        {
            TipoUnidadDAL.DelTipoUnidad(ID);
        }
    }
}

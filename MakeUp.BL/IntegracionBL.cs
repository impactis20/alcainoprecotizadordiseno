﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MakeUp.DAL;
namespace MakeUp.BL
{
    public class IntegracionBL
    {
        //Metodos para integración tabla LOG_z_ma_addCentroCosto
        public static Entities.IntegracionEntity getDetalleIntegracion1(int id_log1)
        {
            return IntegracionDAL.getDetalleIntegracion1(id_log1);
        }
        public static void setIntegracion1(string p1, string p2, string p3, int p4, int p5, decimal p6, int id_log2)
        {
            IntegracionDAL.setIntegracion1(p1, p2, p3, p4, p5, p6, id_log2);

        }

        public static List<Entities.IntegracionEntity> getInteg1()
        {
            return IntegracionDAL.getInteg1();

        }

        //Metodos para integración tabla Log_z_ma_addArticulo
        public static Entities.IntegracionEntity getDetalleIntegracion2(int id_log1)
        {
            return IntegracionDAL.getDetalleIntegracion2(id_log1);
        }

        public static void setIntegracion2(string p1, string p2, DateTime p3, string p4, int id_log2)
        {
            IntegracionDAL.setIntegracion2(p1, p2, p3, p4, id_log2);

        }

        public static List<Entities.IntegracionEntity> getInteg2()
        {
            return IntegracionDAL.getInteg2();
        }

        //Metodos para integración tabla LOG_z_ma_addArticuloMontaje

        public static Entities.IntegracionEntity getDetalleIntegracion3(int Id_log1)
        {
            return IntegracionDAL.getDetalleIntegracion3(Id_log1);

        }

        public static void setIntegracion3(string p1, string p2, DateTime p3, string p4, int id_log2)
        {
            IntegracionDAL.setIntegracion3(p1, p2, p3, p4, id_log2);

        }

        public static List<Entities.IntegracionEntity> getInteg3()
        {
            return IntegracionDAL.getInteg3();
        }

        //Metodos para integración tabla LOG_z_ma_addCentroCostoGarantia

        public static Entities.IntegracionEntity getDetalleIntegracion4(int id_log1)
        {
            return IntegracionDAL.getDetalleIntegracion4(id_log1);
        }

        public static void setIntegracion4(int p1,int id_log2)
        {
            IntegracionDAL.setIntegracion4(p1, id_log2);

        }

        public static List<Entities.IntegracionEntity> getInteg4()
        {
            return IntegracionDAL.getInteg4();
        }

        //Metodos para integración tabla LOG_z_ma_addCliente
        public static Entities.IntegracionEntity getDetalleIntegracion5(int id_log1)
        {
            return IntegracionDAL.getDetalleIntegracion5(id_log1);
        }
        public static void setIntegracion5(string p1, int p2, string p3, string p4, string p5, string p6, string p7, string p8, string p9, int p10, int id_log1)
        {
            IntegracionDAL.setIntegracion5( p1,p2,p3,p4,p5, p6, p7, p8,p9, p10, id_log1);
        }

        public static List<Entities.IntegracionEntity> getInteg5()
        {
            return IntegracionDAL.getInteg5();
        }
        //Metodos para integración tabla LOG_z_ma_addFactura

        public static Entities.IntegracionEntity getDetalleIntegracion6(int Id_log1)
        {
            return IntegracionDAL.GetDetalleIntegracion6(Id_log1);

        }
        
        public static void setIntegracion6(int p1, int p2, DateTime p3, string p4, DateTime p5, string p6, DateTime p7, string p8, DateTime p9, Double p10, string p11, int p12, int id_log1)
        {
            IntegracionDAL.setIntegracion6(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, id_log1);
        }

        public static List<Entities.IntegracionEntity> getInteg6()
        {
            return IntegracionDAL.getInteg6();
        }


        //Metodos para integración tabla LOG_z_ma_addNotaVenta
        
        public static Entities.IntegracionEntity getDetalleIntegracion7(int id_log1)
        {
            return IntegracionDAL.getDetalleIntegracion7(id_log1);
        }

        public static void setIntegracion7(int p1, int p2, string p3, int p4, string p5, int p6, int p7, int id_log1)
        {
            IntegracionDAL.setIntegracion7(p1, p2, p3, p4, p5, p6, p7, id_log1);

        }
        public static List<Entities.IntegracionEntity> getInteg7()
        {
            return IntegracionDAL.getInteg7();
        }

        //Metodos para integración tabla LOG_z_ma_addNotaVenta

        public static Entities.IntegracionEntity getDetalleIntegracion8(int id_log1)
        {
            return IntegracionDAL.getDetalleIntegracion8(id_log1);
        }

        public static void setIntegracion8(string p1, string p2, DateTime p3, DateTime p4, int p5,string p6, int id_log1)
        {
            IntegracionDAL.setIntegracion8(p1, p2, p3, p4,  p5,p6, id_log1);

        }
        public static List<Entities.IntegracionEntity> getInteg8()
        {
            return IntegracionDAL.getInteg8();
        }


        //Metodos para integración tabla LOG_z_ma_addOrdenDeProduccionMontaje

        public static Entities.IntegracionEntity getDetalleIntegracion9(int id_log1)
        {
            return IntegracionDAL.getDetalleIntegracion9(id_log1);
        }
        public static void setIntegracion9(string p1, string p2, DateTime p3, DateTime p4, int p5, int id_log1)
        {
            IntegracionDAL.setIntegracion9(p1, p2, p3, p4, p5, id_log1);

        }

        public static List<Entities.IntegracionEntity> getInteg9()
        {
            return IntegracionDAL.getInteg9();
        }


        //Metodos para integración tabla LOG_z_ma_setCliente

        public static Entities.IntegracionEntity getDetalleIntegracion10(int id_log1)
        {
            return IntegracionDAL.getDetalleIntegracion10(id_log1);
        }

        public static void setIntegracion10(string p1, int p2, string p3, string p4, string p5, string p6, string p7, string p8, string p9, int p10, int id_log1)
        {
            IntegracionDAL.setIntegracion10(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, id_log1);

        }
        public static List<Entities.IntegracionEntity> getInteg10()
        {
            return IntegracionDAL.getInteg10();
        }


        //Metodos para integración tabla LOG_z_ma_setCodigoFactura

        public static Entities.IntegracionEntity getDetalleIntegracion11(int id_log1)
        {
            return IntegracionDAL.getDetalleIntegracion11(id_log1);
        }

        public static void setIntegracion11(int p1,int Id_log1)
        {
           IntegracionDAL.setIntegracion11(p1,Id_log1);

        }
        public static List<Entities.IntegracionEntity> getInteg11()
        {
            return IntegracionDAL.getInteg11();
        }
        //Metodos para integración tabla LOG_z_ma_setCodigoFactura
        public static Entities.IntegracionEntity getDetalleIntegracion12(int id_log1)
        {
            return IntegracionDAL.getDetalleIntegracion12(id_log1);
        }

        public static void setIntegracion12(int p1,int Id_log1)
        {
           IntegracionDAL.setIntegracion12(p1,Id_log1);

        }
        public static List<Entities.IntegracionEntity> getInteg12()
        {
            return IntegracionDAL.getInteg12();
        }
        




        public static List<Entities.Index> getDetalleIndex()
        {
            return IntegracionDAL.getDetalleIndex();

        }





       


        
       
        public static int setActualizaContador()
        {
            return IntegracionDAL.setActualizaContador();
        }
    }
}

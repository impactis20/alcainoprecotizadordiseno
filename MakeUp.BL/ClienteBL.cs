﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MakeUp.DAL;

namespace MakeUp.BL
{
    public class ClienteBL
    {
        public static int AddCliente(int RutSinDv, string DV, string NombreFantasia, string RazonSocial, string Giro, string Direccion, string Telefono, string Comuna, string Ciudad, int IDUsuario)
        {
            return ClienteDAL.AddCliente(RutSinDv, DV, NombreFantasia, RazonSocial, Giro, Direccion, Telefono, Comuna, Ciudad, IDUsuario);
        }

        public static List<Entities.ClienteEntity> GetCliente(int ID, string NombreFantasia, string RazonSocial,int Rut, bool EditaRegistro)
        {
            List<Entities.ClienteEntity> Lst = ClienteDAL.GetCliente(ID, NombreFantasia, RazonSocial, Rut);

            if (!EditaRegistro)
            {
                if (RazonSocial.Length > 0 || NombreFantasia.Length > 0)
                {
                    foreach (Entities.ClienteEntity Objeto in Lst)
                    {
                        if (RazonSocial.Length > 0)
                            Objeto.RazonSocial = Objeto.RazonSocial.ToLower().Replace(RazonSocial.ToLower(), string.Format("<mark>{0}</mark>", RazonSocial.ToLower()));

                        if(NombreFantasia.Length > 0)
                            Objeto.NombreFantasia = Objeto.NombreFantasia.ToLower().Replace(NombreFantasia.ToLower(), string.Format("<mark>{0}</mark>", NombreFantasia.ToLower()));
                    }
                }
            }

            return Lst;
        }

        public static void SetCliente(int IDCliente, int Rut, string DV, string NombreFantasia, string RazonSocial, string Giro, string Direccion, string Telefono, string Comuna, string Ciudad)
        {
            ClienteDAL.SetCliente(IDCliente, Rut, DV, NombreFantasia, RazonSocial, Giro, Direccion, Telefono, Comuna, Ciudad);
        }

        public static void DelCliente(int ID)
        {
            ClienteDAL.DelCliente(ID);
        }

        public static List<string> GetClienteJSON(string prefix)
        {
            return ClienteDAL.GetClienteJSON(prefix);
        }

        public static bool ClienteExiste(int RutSinDV)
        {
            return ClienteDAL.ClienteExiste(RutSinDV);
        }
    }
}

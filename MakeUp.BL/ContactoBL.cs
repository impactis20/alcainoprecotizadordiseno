﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MakeUp.DAL;

namespace MakeUp.BL
{
    public class ContactoBL
    {
        public static void AddContacto(int IDProyecto, int IDCliente, int IDUsuario, string Nombre, string Email, string Descripcion, string Fono, string Movil, bool CompartirContacto, ref int IDNotificacionApp)
        {
            ContactoDAL.AddContacto(IDProyecto, IDCliente, IDUsuario, Nombre, Email, Descripcion, Fono, Movil, DateTime.Now, CompartirContacto, ref IDNotificacionApp);
        }

        public static void SetVincularContactoProyecto(int IDProyecto, int IDContacto, int IDUsuario, bool Vincula)
        {
            ContactoDAL.SetVincularContactoProyecto(IDProyecto, IDContacto, IDUsuario, Convert.ToByte(Vincula), DateTime.Now);
        }

        //public static List<Entities.ClienteEntity.ContactoEntity> GetContactoProyecto(int IDProyecto, int IDCliente, int IDRol)
        //{
        //    return ContactoDAL.GetContactoProyecto(IDProyecto, IDCliente, IDRol);
        //}

        public static List<Entities.ClienteEntity.ContactoEntity> GetContactoProyecto(int IDProyecto, int IDCliente)
        {
            return ContactoDAL.GetContactoProyecto(IDProyecto, IDCliente);
        }

        public static void SetContacto(int IDContacto, string NombreCompleto, string Email, string Descripcion, string Telefono, string Movil, bool ContactoCompartido)
        {
            ContactoDAL.SetContacto(IDContacto, NombreCompleto, Email, Descripcion, Telefono, Movil, ContactoCompartido);
        }

        public static void DelContacto(int IDContacto)
        {
            ContactoDAL.DelContacto(IDContacto);
        }
        public static int GetRol(int IDUsuario)
        {
            return ContactoDAL.GetRol(IDUsuario);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MakeUp.DAL;

namespace MakeUp.BL
{
    public class UsuarioBL
    {
        public static int AddUsuario(string UserName, string Nombre, string APaterno, string AMaterno, string Email, bool VisualizaTodoContacto, int IDRol, List<Entities.PaginaEntity> LstPagina, List<Entities.PaginaEntity.Control> LstControl, string codigoManager)
        {
            return UsuarioDAL.AddUsuario(UserName, Nombre, APaterno, AMaterno, Email, VisualizaTodoContacto, IDRol, DateTime.Now, RolPredeterminadoBL.ListPaginaToDateTable(LstPagina), RolPredeterminadoBL.ListControlToDateTable(LstControl),codigoManager);
        }

        public static List<Entities.UsuarioEntity> GetUsuarioBaja()
        {
            return UsuarioDAL.GetUsuarioBaja();
        }

        public static List<Entities.UsuarioEntity> GetUsuario(int ID, string Username, string Nombre, string Email, bool? VisualizaTodoContacto, bool EditaRegistro)
        {
            List<Entities.UsuarioEntity> Lst = UsuarioDAL.GetUsuario(ID, Username, Nombre, Email, VisualizaTodoContacto);

            if (!EditaRegistro)
            {
                if (Username.Length > 0 || Nombre.Length > 0 || Email.Length > 0)
                {
                    foreach (Entities.UsuarioEntity us in Lst)
                    {
                        if (Username.Length > 0)
                            us.UserName = us.UserName.ToLower().Replace(Username.ToLower(), string.Format("<mark>{0}</mark>", Username.ToLower()));

                        if (Nombre.Length > 0)
                            us.Nombre = us.Nombre.ToLower().Replace(Nombre.ToLower(), string.Format("<mark>{0}</mark>", Nombre.ToLower()));

                        //if (Nombre.Length > 0)
                        //    us.ApellidoPaterno = us.ApellidoPaterno.ToLower().Replace(Nombre.ToLower(), string.Format("<mark>{0}</mark>", Nombre.ToLower()));

                        //if (Nombre.Length > 0)
                        //    us.ApellidoMaterno = us.ApellidoMaterno.ToLower().Replace(Nombre.ToLower(), string.Format("<mark>{0}</mark>", Nombre.ToLower()));

                        if (Email.Length > 0)
                            us.Email = us.Email.ToLower().Replace(Email.ToLower(), string.Format("<mark>{0}</mark>", Email.ToLower()));
                    }
                }
            }

            return Lst;
        }

        public static Entities.UsuarioEntity GetValidaUsername(string Username)
        {
            return UsuarioDAL.GetValidaUsername(Username);
        }

        public static List<string> GetNombreCompletoUsuarioJSON(string prefix)
        {
            return UsuarioDAL.GetNombreCompletoUsuarioJSON(prefix);
        }

        public static void SetUsuario(int IDUsuario, int IDRol, bool VisualizaTodoContacto, List<Entities.PaginaEntity> LstPagina, List<Entities.PaginaEntity.Control> LstControl)
        {
            UsuarioDAL.SetUsuario(IDUsuario, IDRol, VisualizaTodoContacto, RolPredeterminadoBL.ListPaginaToDateTable(LstPagina), RolPredeterminadoBL.ListControlToDateTable(LstControl));
        }

        public static void DelUsuario(int ID)
        {
            UsuarioDAL.DelUsuario(ID);
        }

        public static void SetDarAltaUsuario(int ID)
        {
            UsuarioDAL.SetDarAltaUsuario(ID);
        }

        public static bool GetEmailExiste(string Email)
        {
            return UsuarioDAL.GetEmailExiste(Email);
        }

        public static bool GetAccesoPagina(int IDUsuario, string Path)
        {
            return UsuarioDAL.GetAccesoPagina(IDUsuario, Path);
        }

        public static List<Entities.PaginaEntity.Control> GetControlAutorizado(int IDUsuario, string Path)
        {
            return UsuarioDAL.GetControlAutorizado(IDUsuario, Path);
        }

        public static List<Entities.PaginaEntity.Control> GetControlGridviewAutorizado(int IDUsuario, string Path, string IDGridview, short TipoControl)
        {
            return UsuarioDAL.GetControlGridviewAutorizado(IDUsuario, Path, IDGridview, TipoControl);
        }

        public static bool GetUsuarioValido(int IDUsuario)
        {
            return UsuarioDAL.GetUsuarioValido(IDUsuario);
        }

        public static Entities.RolPredeterminadoEntity GetRolUsuario(int IDUsuario)
        {
            return UsuarioDAL.GetRolUsuario(IDUsuario);
        }

        public static List<string> GetEjecutivoComercialJSON(string prefix)
        {
            return UsuarioDAL.GetEjecutivoComercialJSON(prefix);
        }

        public static List<string> GetSupervisorMontajeJSON(string prefix)
        {
            return UsuarioDAL.GetSupervisorMontajeJSON(prefix);
        }

        public static List<string> GetUserNameAD(string prefix)
        {
            return UsuarioDAL.GetUserNameAD(prefix);
        }

        public static List<Entities.UsuarioEntity> GetUsuarioAD(string UserName)
        {
            return UsuarioDAL.GetUsuarioAD(UserName).OrderBy(x => x.UserName).ToList();
        }

        public static bool AutenticarUsuario(string UserName, string Password)
        {
            return UsuarioDAL.AutenticarUsuario(UserName, Password);
        }
    }
}

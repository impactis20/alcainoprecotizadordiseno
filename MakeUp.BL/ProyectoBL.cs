﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MakeUp.DAL;

namespace MakeUp.BL
{
    public class ProyectoBL
    {
        public static int GetNuevoIDProyecto(int IdUsuario, DateTime Fecha)
        {
            return ProyectoDAL.GetNuevoIDProyecto(IdUsuario, Fecha);
        }

        public static double GetValorTotalProyecto(int IDProyecto)
        {
            return ProyectoDAL.GetValorTotalProyecto(IDProyecto);
        }

        public static double GetSaldoPorFacturarProyecto(int IDProyecto)
        {
            return ProyectoDAL.GetSaldoPorFacturarProyecto(IDProyecto);
        }

        public static int AddProyecto(int IDProyecto, string IDPadre, int IDCliente, int IDUsuarioCreador, int IDEjecutivo, int IDTipoValor, string Nombre, DateTime FechaIngreso, DateTime FechaComprometida, string Direccion, string Comuna, string Directorio)
        {
            int intIDPadre = IDPadre == string.Empty ? 0 : Convert.ToInt16(IDPadre);
            return ProyectoDAL.AddProyecto(IDProyecto, intIDPadre, IDCliente, IDUsuarioCreador, IDEjecutivo, IDTipoValor, Nombre, FechaIngreso, FechaComprometida, Direccion, Comuna, Directorio);
        }

        public static void AddFinalizarCreacion(int IDProyecto, int IDUsuarioCreador, ref int IDNotificacionApp)
        {
            ProyectoDAL.AddFinalizarCreacion(IDProyecto, IDUsuarioCreador, DateTime.Now, ref IDNotificacionApp);
        }

        public static List<Entities.TipoDocumentoEntity> GetTipoDocumento()
        {
            return ProyectoDAL.GetTipoDocumento();
        }

        public static string AddDocumentoComercial(int IDUsuario, int IDTipoDoc, int IDProyecto, string Nombre, DateTime FechaSubida, string Descripcion)
        {
            return ProyectoDAL.AddDocumentoComercial(IDUsuario, IDTipoDoc, IDProyecto, Nombre, FechaSubida, Descripcion);
        }

        public static List<Entities.DocumentoComercialEntity> GetDocumentoComercial(int IDProyecto)
        {
            return ProyectoDAL.GetDocumentoComercial(IDProyecto);
        }

        public static string DelDocumentoComercial(int IDDocComercial)
        {
            return ProyectoDAL.DelDocumentoComercial(IDDocComercial);
        }

        public static void SetDocumentoComercial(int IDDocComercial, int IDTipoDoc, string Descripcion)
        {
            ProyectoDAL.SetDocumentoComercial(IDDocComercial, IDTipoDoc, Descripcion);
        }

        public static void AddDocumentoProyecto(int IDProyecto, string Desc, string Url, DateTime Fecha)
        {
            ProyectoDAL.AddDocumentoProyecto(IDProyecto, Desc, Url, Fecha);
        }

        public static List<Entities.DocumentoProyectoEntity> GetDocumentoProyecto(int IDProyecto)
        {
            return ProyectoDAL.GetDocumentoProyecto(IDProyecto);
        }

        public static void DelDocumentoProyecto(int IDDocProyecto)
        {
            ProyectoDAL.DelDocumentoProyecto(IDDocProyecto);
        }

        public static List<Entities.DocumentoComercialEntity> GetOCProyecto(int IDProyecto)
        {
            return ProyectoDAL.GetOCProyecto(IDProyecto);
        }

        public static bool GetValidaCreacionProyecto(int IDProyecto)
        {
            if (ProyectoDAL.GetValidaCreacionProyecto(IDProyecto) > 0)
                return true;
            else
                return false;
        }

        public static List<Entities.ProyectoEntity> GetBuscarProyecto(int IDProyecto, string NombreProyecto, string NombreFantasia, List<Entities.EstadoNPEntity> LstEstadoNP, int controlUsuario, int rolID, int IDUsuario)
        {
            //Agrega Estados de NP seleccionadas a una Tabla
            DataTable dtEstadoNP = new DataTable();
            dtEstadoNP.Columns.Add("ID_ESTADONP", typeof(int));

            foreach (Entities.EstadoNPEntity EstadoNP in LstEstadoNP)
            {
                DataRow dr = dtEstadoNP.NewRow();
                dr["ID_ESTADONP"] = EstadoNP.ID;
                dtEstadoNP.Rows.Add(dr);
            }

            return ProyectoDAL.GetBuscarProyecto(IDProyecto, NombreProyecto, NombreFantasia, dtEstadoNP, controlUsuario, rolID, IDUsuario);
        }

        public static List<Entities.ProyectoEntity> GetBuscarProyectoPorRegularizar(int IDProyecto, string NombreProyecto, string NombreFantasia)
        {
            return ProyectoDAL.GetBuscarProyectoPorRegularizar(IDProyecto, NombreProyecto, NombreFantasia);
        }


        public static List<Entities.ProyectoEntity> GetBuscarProyectoEliminado(int IDProyecto, string NombreProyecto, string NombreFantasia)
        {
            return ProyectoDAL.GetBuscarProyectoEliminado(IDProyecto, NombreProyecto, NombreFantasia);
        }

        public static List<string> GetProyectoPadreJSON(string prefix)
        {
            return ProyectoDAL.GetProyectoPadreJSON(prefix);
        }

        public static Entities.ProyectoEntity GetCabeceraProyecto(int IDProyecto)
        {
            return ProyectoDAL.GetCabeceraProyecto(IDProyecto);
        }

        public static Entities.ProyectoEntity GetCabeceraProyectoEliminado(int IDProyecto)
        {
            return ProyectoDAL.GetCabeceraProyectoEliminado(IDProyecto);
        }

        public static bool SetCabeceraProyecto(int IDProyecto, int IDUsuarioResponsable, string NombreProyecto, DateTime FechaComprometida, DateTime FechaPlanificada, int IDEjecutivoComercial, string Direccion, string Comuna, List<Entities.UsuarioEntity> ListJefeProyecto, ref int IDNotificacionApp1, ref int IDNotificacionApp2, ref int IDNotificacionApp3, ref int IDNotificacionApp4, int IDSupervisorMontaje, int IDCliente, decimal TiempoDiseño)
        {
            //Agrega usuarios jefes de proyecto seleccionados a una Tabla
            DataTable dtJefesProyecto = new DataTable();
            dtJefesProyecto.Columns.Add("ID_USUARIO", typeof(int));

            ListJefeProyecto = ListJefeProyecto.OrderBy(x => x.ID).ToList();

            foreach (Entities.UsuarioEntity Usuario in ListJefeProyecto)
            {
                DataRow dr = dtJefesProyecto.NewRow();
                dr["ID_USUARIO"] = Usuario.ID;
                dtJefesProyecto.Rows.Add(dr);
            }

            return ProyectoDAL.SetCabeceraProyecto(IDProyecto, IDUsuarioResponsable, NombreProyecto, FechaComprometida, FechaPlanificada, IDEjecutivoComercial, Direccion, Comuna, dtJefesProyecto, DateTime.Now, ref IDNotificacionApp1, ref IDNotificacionApp2, ref IDNotificacionApp3, ref IDNotificacionApp4, IDSupervisorMontaje, IDCliente, TiempoDiseño);
        }

        public static List<Entities.HistorialProyectoEntity> GetHistorial(int IDProyecto, string Sorting)
        {
            return ProyectoDAL.GetHistorial(IDProyecto, Sorting);
        }

        public static void DelProyecto(int IDProyecto, string Observacion)
        {
            ProyectoDAL.DelProyecto(IDProyecto, Observacion);
        }

        //public static void DelProyecto(int IDProyecto)
        //{
        //    ProyectoDAL.DelProyecto(IDProyecto);
        //}

        public static bool GetProyectoEsCLP(int IDProyecto)
        {
            return ProyectoDAL.GetProyectoEsCLP(IDProyecto);
        }

        //public static void AddJefeProyecto(int IDProyecto, List<Entities.UsuarioEntity> ListJefeProyecto, int IDUsuarioResponsable)
        //{

        //    ProyectoDAL.AddJefeProyecto(IDProyecto, dtJefesProyecto, IDUsuarioResponsable, DateTime.Now);
        //}
        public static List<Entities.UsuarioEntity> GetJefeProyecto(int IDProyecto)
        {
            return ProyectoDAL.GetJefeProyecto(IDProyecto);
        }
        //public static List<Entities.UsuarioEntity> GetSupervisorMontaje(int IDProyecto)
        //{
        //    return ProyectoDAL.GetSupervisorMontaje(IDProyecto);
        //}
    }
}

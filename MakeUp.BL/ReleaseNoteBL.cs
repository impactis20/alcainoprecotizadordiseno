﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MakeUp.DAL;

namespace MakeUp.BL
{
    public class ReleaseNoteBL
    {

        public static List<Entities.ReleaseNoteEntity> GetRelease() {

           return ReleaseNoteDAL.GetRelease();
        }
    }
}

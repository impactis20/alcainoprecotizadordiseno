﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MakeUp.Entities;

using MakeUp.DAL;

namespace MakeUp.BL
{
    public static class FacturacionBL
    {
        public static List<Entities.ProyectoEntity> GetNPxFacturar()
        {
            return FacturacionDAL.GetNPxFacturar();
        }

        public static List<Entities.NotaPedidoEntity> GetNPParaFacturar(int IDProyecto)
        {
            return FacturacionDAL.GetNPParaFacturar(IDProyecto);
        }

        public static int AddFacturacionxNP(int IDProyecto, int IDUsuario, int IDCliente, decimal ValorTotalFactura, string CondicionPago, DateTime FechaVencimiento, string NroOC, DateTime FechaOC, string NroHes, DateTime FechaHes, string Glosa, List<Entities.NotaPedidoEntity> LstNP, ref int IDNotificacionApp)
        {
            //Agrega NP seleccionadas a una Tabla
            DataTable dtNP = new DataTable();
            dtNP.Columns.Add("ID_NP", typeof(int));

            foreach (Entities.NotaPedidoEntity NP in LstNP)
            {
                DataRow dr = dtNP.NewRow();
                dr["ID_NP"] = NP.ID;
                dtNP.Rows.Add(dr);
            }

            return FacturacionDAL.AddFacturacionxNP(IDProyecto, IDUsuario, IDCliente, ValorTotalFactura, CondicionPago, FechaVencimiento, NroOC, FechaOC, NroHes, FechaHes, Glosa, dtNP, DateTime.Now, ref IDNotificacionApp);
        }

        //convierte list a datatable (método propio de List<T>)
        private static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        public static List<Entities.ProyectoEntity> GetEPxFacturar()
        {
            return FacturacionDAL.GetEPxFacturar();
        }

        public static int AddFacturacionxEP(int IDProyecto, int IDUsuario, int IDCliente, int IDEstadoPago, decimal ValorTotalFactura, string CondicionPago, DateTime FechaVencimiento, string NroOC, DateTime FechaOC, string NroHes, DateTime FechaHes, string Glosa, ref int IDNotificacionApp)
        {
            return FacturacionDAL.AddFacturacionxEP(IDProyecto, IDUsuario, IDCliente, IDEstadoPago, ValorTotalFactura, CondicionPago, FechaVencimiento, NroOC, FechaOC, NroHes, FechaHes, Glosa, DateTime.Now, ref IDNotificacionApp);
        }

        public static List<Entities.FacturaEntity> GetFactura(int IDProyecto, string NombreProyecto, string NombreFantasia, List<Entities.EstadoDespachoFacturaEntity> LstEstadoDespachoFactura)
        {
            //Agrega estados de despacho de factura seleccionadas a una Tabla
            DataTable dtEstadoDespachoFactura = new DataTable();
            dtEstadoDespachoFactura.Columns.Add("ID_TIPOENVIO", typeof(int));

            foreach (Entities.EstadoDespachoFacturaEntity TipoEnvioFactura in LstEstadoDespachoFactura)
            {
                DataRow dr = dtEstadoDespachoFactura.NewRow();
                dr["ID_TIPOENVIO"] = TipoEnvioFactura.ID;
                dtEstadoDespachoFactura.Rows.Add(dr);
            }

            return FacturacionDAL.GetFactura(IDProyecto, NombreProyecto, NombreFantasia, dtEstadoDespachoFactura);
        }

        public static List<Entities.TipoEnvioFacturaEntity> GetTipoEnvioFactura()
        {
            return FacturacionDAL.GetTipoEnvioFactura();
        }

        public static List<Entities.EstadoDespachoFacturaEntity> GetEstadoDespachoFactura()
        {
            return FacturacionDAL.GetEstadoDespachoFactura();
        }

        public static Entities.FacturaEntity GetDatosDespachoFactura(int IDFactura)
        {
            return FacturacionDAL.GetDatosDespachoFactura(IDFactura);
        }

        public static void SetDespachoEnvioFactura(int NroFactura, int IDTipoEnvio, string EntregadoPor, DateTime FechaEnvio)
        {
            FacturacionDAL.SetDespachoEnvioFactura(NroFactura, IDTipoEnvio, EntregadoPor, FechaEnvio);
        }

        public static void SetDespachoRecepcionFactura(int NroFactura, int IDUsuarioResponsable, int RutReceptor, string RutReceptorDV, string RecibidoPor, DateTime FechaRecepcion, ref int IDNotificacionApp)
        {
            FacturacionDAL.SetDespachoRecepcionFactura(NroFactura, IDUsuarioResponsable, RutReceptor, RutReceptorDV, RecibidoPor, FechaRecepcion, DateTime.Now, ref IDNotificacionApp);
        }

        public static void SetEditarFactura(string proy, string NroFactura, decimal monto, string tipoValor)
        {
             FacturacionDAL.SetEditarFactura(proy, NroFactura,monto, tipoValor);
        }

        public static List<Entities.FacturaEntity> GetFacturaProyecto(int IDProyecto)
        {
            return FacturacionDAL.GetFacturaProyecto(IDProyecto);
        }
    }
}

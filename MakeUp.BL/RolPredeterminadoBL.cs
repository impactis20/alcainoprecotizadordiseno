﻿using System;
using System.Collections.Generic;
using System.Data;

using MakeUp.DAL;

namespace MakeUp.BL
{
    public class RolPredeterminadoBL
    {
        public static void AddRolPredeterminado(string Nombre, string Descripcion, List<Entities.PaginaEntity> LstPagina, List<Entities.PaginaEntity.Control> LstControl)
        {
            RolPredeterminadoDAL.AddRolPredeterminado(Nombre, Descripcion, ListPaginaToDateTable(LstPagina), ListControlToDateTable(LstControl));
        }

        public static List<Entities.RolPredeterminadoEntity> GetListadoRolPredeterminadoCreado()
        {
            return RolPredeterminadoDAL.GetListadoRolPredeterminadoCreado();
        }

        public static Entities.RolPredeterminadoEntity GetRolPredeterminado(int ID)
        {
            return RolPredeterminadoDAL.GetRolPredeterminado(ID);
        }

        public static void SetRolPredeterminado(int IDRolPredeterminado, string Nombre, string Descripcion, List<Entities.PaginaEntity> LstPagina, List<Entities.PaginaEntity.Control> LstControl)
        {
            RolPredeterminadoDAL.SetRolPredeterminado(IDRolPredeterminado, Nombre, Descripcion, ListPaginaToDateTable(LstPagina), ListControlToDateTable(LstControl));
        }

        public static DataTable ListPaginaToDateTable(List<Entities.PaginaEntity> LstPagina)
        {
            //Agrega las páginas (módulos) seleccionadas a una tabla
            DataTable dtPagina = new DataTable();
            dtPagina.Columns.Add("ID_PAGINA", typeof(int));

            foreach (Entities.PaginaEntity Pagina in LstPagina)
            {
                DataRow dr = dtPagina.NewRow();
                dr["ID_PAGINA"] = Pagina.ID;
                dtPagina.Rows.Add(dr);
            }

            return dtPagina;
        }

        public static DataTable ListControlToDateTable(List<Entities.PaginaEntity.Control> LstControl)
        {
            //Agrega los controles seleccionados a una tabla
            DataTable dtControl = new DataTable();
            dtControl.Columns.Add("ID_CONTROL", typeof(int));

            foreach (Entities.PaginaEntity.Control Control in LstControl)
            {
                DataRow dr = dtControl.NewRow();
                dr["ID_CONTROL"] = Control.ID;
                dtControl.Rows.Add(dr);
            }

            return dtControl;
        }

        public static List<Entities.RolPredeterminadoEntity> DelRolPredeterminado(int IDRolPredeterminado)
        {
            return RolPredeterminadoDAL.DelRolPredeterminado(IDRolPredeterminado);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MakeUp.DAL;

namespace MakeUp.BL
{
    public class NotificacionBL
    {
        public static List<Entities.NotificacionEntity> GetNotificacion()
        {
            List<Entities.NotificacionEntity> Lst = NotificacionDAL.GetNotificacion();

            foreach (Entities.NotificacionEntity Notificacion in Lst)
                Notificacion.RolNotificado = NotificacionDAL.GetNotificarRol(Notificacion.ID);

            return Lst;
        }

        public static void SetNotificarRol(int IDNotificacion, List<Entities.RolPredeterminadoEntity> LstRolEnNotificacion)
        {
            //Agrega estados de despacho de factura seleccionadas a una Tabla
            DataTable dtRolEnNotificacion = new DataTable();
            dtRolEnNotificacion.Columns.Add("ID_ROLPREDETERMINADO", typeof(int));

            foreach (Entities.RolPredeterminadoEntity RolEnNotificacion in LstRolEnNotificacion)
            {
                DataRow dr = dtRolEnNotificacion.NewRow();
                dr["ID_ROLPREDETERMINADO"] = RolEnNotificacion.ID;
                dtRolEnNotificacion.Rows.Add(dr);
            }

            NotificacionDAL.SetNotificarRol(IDNotificacion, dtRolEnNotificacion);
        }

        public static List<Entities.FiltroNotificacionEntity> GetFiltroRolNotificacion(int IDNotificacion, int IDRolPredeterminado, ref bool FiltraRol)
        {
            return NotificacionDAL.GetFiltroRolNotificacion(IDNotificacion, IDRolPredeterminado, ref FiltraRol);
        }

        public static void SetNotificacionesEnEstadoVista(int IDUsuario)
        {
            NotificacionDAL.SetNotificacionesEnEstadoVista(IDUsuario);
        }

        public static void SetIngresoNotificacion(int IDNotificacionApp, int IDUsuario, bool Ingreso)
        {
            NotificacionDAL.SetIngresoNotificacion(IDNotificacionApp, IDUsuario, Ingreso, DateTime.Now);
        }

        public static List<Entities.NotificacionAppEntity> GetNotificacionesAppUsuario(int IDUsuario, int CantidadFilas, ref int ContNotificacionesSinVer)
        {
            return NotificacionDAL.GetNotificacionesAppUsuario(IDUsuario, CantidadFilas, ref ContNotificacionesSinVer);
        }

        public static List<Entities.NotificacionEntity> GetFiltrarRolEnNotificacion(int IDRolPredeterminado)
        {
            return NotificacionDAL.GetFiltrarRolEnNotificacion(IDRolPredeterminado);
        }

        public static void AddPermiteFiltrarRolEnNotificacion(int IDRolPredeterminado, List<Entities.NotificacionEntity> LstNotificacion)
        {
            //Agrega las notificaciones que puede filtrar el rol
            DataTable dtNotificacion = new DataTable();
            dtNotificacion.Columns.Add("ID", typeof(int));

            foreach (Entities.NotificacionEntity Notificacion in LstNotificacion)
            {
                DataRow dr = dtNotificacion.NewRow();
                dr["ID"] = Notificacion.ID;
                dtNotificacion.Rows.Add(dr);
            }

            NotificacionDAL.AddPermiteFiltrarRolEnNotificacion(IDRolPredeterminado, dtNotificacion);
        }

        public static void SetFiltroRolNotificacion(int IDRolPredeterminado, int IDNotificacion, int IDFiltro)
        {
            NotificacionDAL.SetFiltroRolNotificacion(IDRolPredeterminado, IDNotificacion, IDFiltro);
        }
    }
}

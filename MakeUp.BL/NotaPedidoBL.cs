﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MakeUp.DAL;

namespace MakeUp.BL
{
    public class NotaPedidoBL
    {
        public static int AddNPProyecto(int IDProyecto, int IDUsuario, int IDTipoUnidad, int IDTipoNP, int IDDocOC, decimal ValorUnitario, decimal Cantidad, string Descripcion, DateTime Fecha, string DecisionEP, ref int IDNotificacionApp)
        //public static int AddNPProyecto(int IDProyecto, int IDUsuario, int IDTipoUnidad, int IDDocOC, decimal ValorUnitario, decimal Cantidad, string Descripcion, DateTime Fecha, string DecisionEP, ref int IDNotificacionApp)
        {
            //return NotaPedidoDAL.AddNPProyecto(IDProyecto, IDUsuario, IDTipoUnidad, IDDocOC, ValorUnitario, Cantidad, Descripcion, Fecha, DecisionEP, ref IDNotificacionApp);
            return NotaPedidoDAL.AddNPProyecto(IDProyecto, IDUsuario, IDTipoUnidad, IDTipoNP, IDDocOC, ValorUnitario, Cantidad, Descripcion, Fecha, DecisionEP, ref IDNotificacionApp);
        }

        public static List<Entities.NotaPedidoEntity> GetNPCreada(int IDProyecto)
        {
            return NotaPedidoDAL.GetNPCreada(IDProyecto);
        }

        public static void DelNPCreada(int IDNP, string DecisionEP)
        {
            NotaPedidoDAL.DelNPCreada(IDNP, DecisionEP);
        }

        public static void DelNP(int IDNP, int IDUsuario, string DecisionEP, ref int IDNotificacionApp)
        {
            NotaPedidoDAL.DelNP(IDNP, IDUsuario, DecisionEP, DateTime.Now, ref IDNotificacionApp);
        }

        public static void SetNPCreada(int IDProyecto, int IDNP, int IDUsuario, int IDTipoUnidad, int IDDocOC, decimal ValorUnitario, decimal Cantidad, string Descripcion, DateTime Fecha, string DecisionEP,int IDTipoNP)
        {
            NotaPedidoDAL.SetNPCreada(IDProyecto, IDNP, IDUsuario, IDTipoUnidad, IDDocOC, ValorUnitario, Cantidad, Descripcion, Fecha, DecisionEP, IDTipoNP);
        }

        public static void SetNP(int IDNP, int IDUsuario, int IDTipoUnidad,int IDTipoNP, int IDDocOC, decimal ValorUnitario, decimal Cantidad, string Descripcion, string CampoModifica, DateTime Fecha, string DecisionEP, ref int IDNotificacionApp)
        {
            NotaPedidoDAL.SetNP(IDNP, IDUsuario, IDTipoUnidad, IDTipoNP, IDDocOC, ValorUnitario, Cantidad, Descripcion, CampoModifica, Fecha, DecisionEP, ref IDNotificacionApp);
        }

        public static List<Entities.EstadoNPEntity> GetEstadoNP()
        {
            return NotaPedidoDAL.GetEstadoNP();
        }

        public static List<Entities.NotaPedidoEntity> GetNP(int IDProyecto)
        {
            return NotaPedidoDAL.GetNP(IDProyecto);
        }

        public static void AddAjuste(int IDNP, int IDUsuario, decimal Cantidad, string Descripcion, DateTime FechaRealEntrega, DateTime FechaOperacion, string DecisionEP, ref int IDNotificacionApp)
        {
            NotaPedidoDAL.AddAjuste(IDNP, IDUsuario, Cantidad, Descripcion, FechaRealEntrega, FechaOperacion, DecisionEP, ref IDNotificacionApp);
        }

        public static List<Entities.NotaPedidoEntity> GetNPRegularizar(int ID,int IDNP) {
            return NotaPedidoDAL.GetNPRegularizar(ID,IDNP);
        }

        public static void AddValorizacion(int IDNP, int IDUsuario, decimal Cantidad, decimal ValorUnitario, string Descripcion, DateTime FechaOperacion, string DecisionEP, ref int IDNotificacionApp)
        {
            NotaPedidoDAL.AddValorizacion(IDNP, IDUsuario, Cantidad, ValorUnitario, Descripcion, FechaOperacion, DecisionEP, ref IDNotificacionApp);
        }

        public static List<Entities.NotaPedidoEntity> GetResumenNPProyecto(int IDProyecto)
        {
            return NotaPedidoDAL.GetResumenNPProyecto(IDProyecto); ;
        }

        public static List<Entities.NotaPedidoEntity> GetTrazabilidadNP(int IDNP)
        {
            return NotaPedidoDAL.GetTrazabilidadNP(IDNP);
        }
        public static List<Entities.TipoNPEntity> GetTipoNP()
        {
            return NotaPedidoDAL.GetTipoNP();
        }
    }
}
